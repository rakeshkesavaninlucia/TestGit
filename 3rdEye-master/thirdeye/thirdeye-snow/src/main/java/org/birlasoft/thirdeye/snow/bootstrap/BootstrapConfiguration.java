/**
 * 
 */
package org.birlasoft.thirdeye.snow.bootstrap;

import org.birlasoft.thirdeye.snow.config.SnowWebConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author shaishav.dixit
 *
 */
@Configuration
@Import({SnowWebConfig.class})
public class BootstrapConfiguration {

}
