package org.birlasoft.thirdeye.snow.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.birlasoft.thirdeye.repositories.UserRepository;
import org.birlasoft.thirdeye.snow.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * UserServiceImpl.java - Service Implementation class for user
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class UserServiceImpl implements UserService {
	
	private UserRepository userRepository;
	
	/**
	 * 
	 * @param userRepository
	 */
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User findUserByEmail(String email) {		
		return userRepository.findUserByEmail(email);
	}

	@Override
	public User findUserById(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User createUserObject(User newUser, User currentUser) {
		newUser.setCreatedBy(currentUser.getId());
		newUser.setUpdatedBy(currentUser.getId());
		newUser.setCreatedDate(new Date());
		newUser.setUpdatedDate(new Date());
		return newUser;
	}

	@Override
	public User updateUserObject(User userToUpdate, User currentUser) {
		User userFromDB = findUserById(userToUpdate.getId());
		userFromDB.setFirstName(userToUpdate.getFirstName());
		userFromDB.setLastName(userToUpdate.getLastName());
		userFromDB.setEmailAddress(userToUpdate.getEmailAddress());
		if(userToUpdate.getPassword() != null && !userToUpdate.getPassword().isEmpty()){
			userFromDB.setPassword(userToUpdate.getPassword());
		}
		userFromDB.setUpdatedBy(currentUser.getId());
		userFromDB.setUpdatedDate(new Date());
		return userFromDB;
	}

	@Override
	public Map<Integer, Integer> createMapForUserRoles(Set<UserRole> userRoles) {
		Map<Integer, Integer> mapOfRoleIds = new HashMap<>();
		for (UserRole userRole : userRoles) {
			mapOfRoleIds.put(userRole.getRole().getId(), userRole.getRole().getId());
		}
		return mapOfRoleIds;
	}

	@Override
	public List<User> findByDeleteStatus(Boolean deleteStatus) {
		return userRepository.findByDeleteStatus(deleteStatus);
	}

	@Override
	public User deleteUser(Integer idOfUser) {
		User userById = findUserById(idOfUser);
		userById.setDeleteStatus(Constants.DELETE_STATUS_TRUE);
		return userById;
	}

	@Override
	public List<User> findByDeleteStatusAndAccountExpiredAndAccountLocked(Boolean deleteStatus, Boolean accountExpired, Boolean accountLocked) {
		return userRepository.findByDeleteStatusAndAccountExpiredAndAccountLocked(deleteStatus, accountExpired, accountLocked);
	}
	
	@Override
	public List<User> findByFirstNameContainingOrLastNameContaining(String search1,String search2){
		return userRepository.findByFirstNameContainingOrLastNameContaining(search1,search2);
	}
}
