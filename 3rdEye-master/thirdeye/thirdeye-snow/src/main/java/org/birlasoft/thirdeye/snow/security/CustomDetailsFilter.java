package org.birlasoft.thirdeye.snow.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

/**
 * This is effectively the login form details coming in.
 * 
 * @author shaishav.dixit
 *
 */
public class CustomDetailsFilter extends GenericFilterBean {

	private static final String TENANT_CODE = "tenantCode";

	@Autowired
	private TenantRepository tenantRepository;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		Map<String, String> detailMap = new HashMap<>();
		detailMap.put(TENANT_CODE, request.getParameter(TENANT_CODE));
		
		if (!StringUtils.isEmpty(request.getParameter(TENANT_CODE))){
			Tenant tenant = tenantRepository.findByTenantAccountId(StringUtils.trimAllWhitespace(request.getParameter(TENANT_CODE)));
			
			if (tenant != null && !StringUtils.isEmpty(tenant.getTenantAccountId()) && !StringUtils.isEmpty(tenant.getTenantUrlId())){
				
				// Setup the request attributes so that going forward for this request the 
				// correct tenant db is hit
				RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
				if ( requestAttributes == null || requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER", RequestAttributes.SCOPE_REQUEST) == null){
					RequestContextHolder.setRequestAttributes(new DispatcherServletWebRequest((HttpServletRequest) request));
					requestAttributes = RequestContextHolder.getRequestAttributes();
				}
				
				requestAttributes.setAttribute("CURRENT_TENANT_IDENTIFIER", tenant.getTenantUrlId(), RequestAttributes.SCOPE_REQUEST);
				
			} else {
				logger.info("No record found for account ID: " + request.getParameter(TENANT_CODE));
				throw new BadCredentialsException("No record found for account ID: " + request.getParameter(TENANT_CODE));
			}
		} else {
			logger.info("Account ID is empty");
		}
		chain.doFilter(request, response);
		
	}

}

