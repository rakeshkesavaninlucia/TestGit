package org.birlasoft.thirdeye.snow;

import javax.servlet.Filter;

import org.birlasoft.thirdeye.snow.bootstrap.BootstrapConfiguration;
import org.birlasoft.thirdeye.snow.config.TenantConfiguration;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**Class for application initialization of root, servlet configuration and mappings and filters.
 * @author shaishav.dixit
 *
 */
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class [] {BootstrapConfiguration.class, TenantConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}
	
	@Override 
	protected Filter[] getServletFilters() { 
		return new Filter[]{ new DelegatingFilterProxy("springSecurityFilterChain")}; 
	}

}
