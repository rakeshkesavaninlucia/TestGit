/**
 * 
 */
package org.birlasoft.thirdeye.snow.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.birlasoft.thirdeye.config.EntityManagers;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.snow.config.packages.EntityConfig;
import org.birlasoft.thirdeye.snow.config.packages.RepositoryConfig;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author shaishav.dixit
 *
 */
@Configuration
@EnableJpaRepositories(
		entityManagerFactoryRef = EntityManagers.TENANTENTITYMANAGER,
        transactionManagerRef = TransactionManagers.TENANTTRANSACTIONMANAGER,
        basePackages = {"org.birlasoft.thirdeye.repositories"})
@ComponentScan({"org.birlasoft.thirdeye.**.service","org.birlasoft.thirdeye.**.controller"})
@Import({EntityConfig.class, RepositoryConfig.class, SnowSecurityConfig.class})
@EnableTransactionManagement
public class TenantConfiguration {
	
	@Bean
    public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setShowSql(true);
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");
		return vendorAdapter;
    }


    @Bean(name = EntityManagers.TENANTENTITYMANAGER)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                           MultiTenantConnectionProvider connectionProvider,
                                                           CurrentTenantIdentifierResolver tenantResolver) {
        LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
        emfBean.setDataSource(dataSource);
        emfBean.setPackagesToScan("org.birlasoft.thirdeye.entity");
        emfBean.setJpaVendorAdapter(jpaVendorAdapter());

        Map<String, Object> properties = new HashMap<>();
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT_CONNECTION_PROVIDER, connectionProvider);
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, tenantResolver);
		
        emfBean.setJpaPropertyMap(properties);
        return emfBean;
    }

    @Bean(name = TransactionManagers.TENANTTRANSACTIONMANAGER)
    public JpaTransactionManager transactionManager(EntityManagerFactory tenantEntityManager){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(tenantEntityManager);
        return transactionManager;
    }

}
