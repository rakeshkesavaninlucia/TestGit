package org.birlasoft.thirdeye.search.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterValueTypeBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.birlasoft.thirdeye.search.service.ParameterEvaluatorSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParameterEvaluatorSearchServiceImpl implements ParameterEvaluatorSearchService {
	
	@Autowired
	private Client client;
	@Autowired
	private FilterSearchService filterSearchService;

	@Override
	public AssetParameterWrapper getParameterValueForAssets(SearchConfig searchConfig) {
		int size = 10;
		AssetParameterWrapper assetParameterWrapper = new AssetParameterWrapper();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForAssetParameterValue(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForAssetParameterValue(searchConfig, tenant, size);
			}

			assetParameterWrapper = extractAssetParameterWrapper(searchConfig, response);
		}
		return assetParameterWrapper;
	}

	private SearchResponse getResponseForAssetParameterValue(SearchConfig searchConfig, String tenant, int size) {
		QueryBuilder postFilterQuery = QueryBuilders.boolQuery()
				.must(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace()));
				//.must(QueryBuilders.termsQuery("id", searchConfig.getListOfIds()));
		if(!searchConfig.getListOfIds().isEmpty()) {
			postFilterQuery =  QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("id", searchConfig.getListOfIds())).must(postFilterQuery);
		}
		if (searchConfig.getFilterMap() != null && !searchConfig.getFilterMap().isEmpty()) {
			postFilterQuery = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap()).must(postFilterQuery);
		}
		
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.nestedQuery(IndexAssetTags.PARAMETERS.getTagKey(), QueryBuilders.boolQuery()
						.must(QueryBuilders.matchQuery("parameters.id", searchConfig.getParameterId()))
						.must(QueryBuilders.nestedQuery("parameters.values", QueryBuilders.matchQuery("parameters.values.questionnaireId", searchConfig.getQuestionnaireId()), ScoreMode.Avg)), ScoreMode.Avg))
				.setPostFilter(postFilterQuery)
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}
	
	private AssetParameterWrapper extractAssetParameterWrapper(SearchConfig searchConfig, SearchResponse response) {
		AssetParameterWrapper assetParameterWrapper = new AssetParameterWrapper();
		List<AssetParameterBean> listOfAssetParameterBeans = new ArrayList<>();

		for (SearchHit oneHit : response.getHits()) {
			AssetParameterBean assetParameterBean = new AssetParameterBean();
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			assetParameterBean.setAssetName(indexAssetBean.getName());
			assetParameterBean.setAssetId(indexAssetBean.getId());
			for (IndexParameterBean oneParameterBean : indexAssetBean.getParameters()) {
				if(oneParameterBean.getId().equals(searchConfig.getParameterId())){
					assetParameterWrapper.setParameter(oneParameterBean.getDisplayName());
					extractRequiredParameterValue(searchConfig, assetParameterBean, oneParameterBean);
				}
			}
			listOfAssetParameterBeans.add(assetParameterBean);
		}
		assetParameterWrapper.setValues(listOfAssetParameterBeans);
		assetParameterWrapper.setParameterId(searchConfig.getParameterId());
		return assetParameterWrapper;
	}

	private void extractRequiredParameterValue(SearchConfig searchConfig, AssetParameterBean assetParameterBean,
			IndexParameterBean oneParameterBean) {
		for (IndexParameterValueTypeBean oneParameterValue : oneParameterBean.getValues()) {
			if(oneParameterValue.getQuestionnaireId().equals(searchConfig.getQuestionnaireId())) {							
				assetParameterBean.setParameterValue(oneParameterValue.getValue());
				assetParameterBean.setColor(oneParameterValue.getColor());
				assetParameterBean.setDescription(oneParameterValue.getColorDesc());
			}
		}
	}

}
