package org.birlasoft.thirdeye.search.service;

import java.util.Map;

import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

public interface QuestionResponseSearchService {

	public Map<Integer, QuestionResponseSearchWrapper> getQuestionValue(SearchConfig searchConfig);

}
