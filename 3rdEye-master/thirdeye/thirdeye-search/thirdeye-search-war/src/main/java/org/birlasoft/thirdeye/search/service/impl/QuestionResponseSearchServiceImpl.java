/**
 * 
 */
package org.birlasoft.thirdeye.search.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionValueTypeBean;
import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.QuestionResponseSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shaishav.dixit
 *
 */
@Service
public class QuestionResponseSearchServiceImpl implements QuestionResponseSearchService {
	
	@Autowired
	private Client client;
	
	@Override
	public Map<Integer, QuestionResponseSearchWrapper> getQuestionValue(SearchConfig searchConfig) {
		int size = 10;
		Map<Integer, QuestionResponseSearchWrapper> mapOfQuestionAndValue = new HashMap<>();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForQuestionValue(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForQuestionValue(searchConfig, tenant, size);
			}

			extractQuestionResponseSearchWrapper(searchConfig, response, mapOfQuestionAndValue);
		}
		return mapOfQuestionAndValue;
		
	}

	private void extractQuestionResponseSearchWrapper(SearchConfig searchConfig, SearchResponse response, Map<Integer, QuestionResponseSearchWrapper> mapOfQuestionAndValue) {

		for (SearchHit oneHit : response.getHits()) {
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			for (IndexQuestionBean oneQuestion : indexAssetBean.getQuestions()) {
				prepareMapWithQuestionValue(searchConfig, mapOfQuestionAndValue, indexAssetBean, oneQuestion);
			}
		}
	}

	/**
	 * @param searchConfig
	 * @param mapOfQuestionAndValue
	 * @param indexAssetBean
	 * @param oneQuestion
	 */
	private void prepareMapWithQuestionValue(SearchConfig searchConfig,
			Map<Integer, QuestionResponseSearchWrapper> mapOfQuestionAndValue, IndexAssetBean indexAssetBean,
			IndexQuestionBean oneQuestion) {
		QuestionResponseSearchWrapper questionResponseSearchWrapper;
		for (IndexQuestionValueTypeBean oneQuestionValue : oneQuestion.getValues()) {
			if (oneQuestionValue.getQuestionnaireId().equals(searchConfig.getQuestionnaireId()) && oneQuestionValue.getQuestionnaireParameterId().equals(searchConfig.getQpId())) {
				if (mapOfQuestionAndValue.containsKey(oneQuestion.getId())) {
					questionResponseSearchWrapper = mapOfQuestionAndValue.get(oneQuestion.getId());
				} else {
					questionResponseSearchWrapper = new QuestionResponseSearchWrapper();
					questionResponseSearchWrapper.setDisplayName(oneQuestion.getDisplayName());
					questionResponseSearchWrapper.setQuestionId(oneQuestion.getId());
				}
				questionResponseSearchWrapper.addAssetEvaluation(indexAssetBean.getId(), oneQuestionValue.getValue());
				mapOfQuestionAndValue.put(oneQuestion.getId(), questionResponseSearchWrapper);
			}
		}
	}

	private SearchResponse getResponseForQuestionValue(SearchConfig searchConfig, String tenant, int size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.nestedQuery("questions", QueryBuilders.nestedQuery("questions.values", QueryBuilders.boolQuery()
						.must(QueryBuilders.matchQuery("questions.values.questionnaireId", searchConfig.getQuestionnaireId()))
						.must(QueryBuilders.matchQuery("questions.values.questionnaireParameterId", searchConfig.getQpId())), ScoreMode.Avg), ScoreMode.Avg))
				.setPostFilter(QueryBuilders.boolQuery()
						.must(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace()))
						.must(QueryBuilders.termsQuery("id", searchConfig.getListOfIds())))
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}

}
