/**
 * 
 */
package org.birlasoft.thirdeye.search.controller;

import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.ParameterEvaluatorSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shaishav.dixit
 *
 */
@RestController
public class ParameterEvaluatorSearchController extends BaseSearchController {
	
	@Autowired
	private ParameterEvaluatorSearchService parameterEvaluatorSearchService;
	
	@RequestMapping( value = "/param/value", method = RequestMethod.POST )
	public AssetParameterWrapper searchHome(@RequestBody SearchConfig searchConfig){		
		return parameterEvaluatorSearchService.getParameterValueForAssets(searchConfig);
	}

}
