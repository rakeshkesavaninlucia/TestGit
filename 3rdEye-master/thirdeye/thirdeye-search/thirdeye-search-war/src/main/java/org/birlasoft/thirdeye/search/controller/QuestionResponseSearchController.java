/**
 * 
 */
package org.birlasoft.thirdeye.search.controller;

import java.util.Map;

import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.QuestionResponseSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shaishav.dixit
 *
 */
@RestController
public class QuestionResponseSearchController extends BaseSearchController {
	
	@Autowired
	private QuestionResponseSearchService questionResponseSearchService;
	
	@RequestMapping( value = "/ques/value", method = RequestMethod.POST )
	public Map<Integer, QuestionResponseSearchWrapper> getQuestionValue(@RequestBody SearchConfig searchConfig){
		return questionResponseSearchService.getQuestionValue(searchConfig);
	}

}
