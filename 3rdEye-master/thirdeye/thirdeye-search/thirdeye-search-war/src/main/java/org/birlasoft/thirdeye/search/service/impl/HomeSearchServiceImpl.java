package org.birlasoft.thirdeye.search.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterBean;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.constant.ElasticSearchTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.birlasoft.thirdeye.search.service.HomeSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.InternalNested;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.metrics.avg.InternalAvg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeSearchServiceImpl implements HomeSearchService {

	private static final String PARAMETERS_ID = "parameters.id";

	private static final String PARAMETER_DISPLAY_NAME = "parameters.displayName";
	
	@Autowired
	private Client client;
	@Autowired
	private FilterSearchService filterSearchService;

	@Override
	public AssetParameterWrapper getAssetValueForParameter(SearchConfig searchConfig) {
		int size = 10;
		AssetParameterWrapper assetParameterWrapper=new AssetParameterWrapper();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForAssetParameterValue(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForAssetParameterValue(searchConfig, tenant, size);
			}

			assetParameterWrapper = extractAssetParameterWrapper(searchConfig.getParameterId(), response);
		}
		return assetParameterWrapper;
	}

	/**
	 * Get parameter current value for all the assets. Parameters should be filtered on the
	 * basis of workspace id.
	 * @param parameterId
	 * @param tenant
	 * @param size
	 * @param workspaceId
	 * @return
	 */
	private SearchResponse getResponseForAssetParameterValue(SearchConfig searchConfig, String tenant, int size) {
		QueryBuilder query = QueryBuilders.nestedQuery(IndexAssetTags.PARAMETERS.getTagKey(), QueryBuilders.matchQuery(PARAMETERS_ID, searchConfig.getParameterId()), ScoreMode.Avg);
		if (!searchConfig.getFilterMap().isEmpty()) {
			query = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap()).must(query);
		}
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setFetchSource(new String[]{"id","name",PARAMETER_DISPLAY_NAME,PARAMETERS_ID,"parameters.currentValue.value"}, null)
				.setQuery(query)
				.setPostFilter(QueryBuilders.constantScoreQuery(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace())))
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}

	/**
	 * Prepare {@link AssetParameterWrapper} from search response
	 * @param parameterId
	 * @param response
	 * @return
	 */
	private AssetParameterWrapper extractAssetParameterWrapper(Integer parameterId, SearchResponse response) {
		AssetParameterWrapper assetParameterWrapper = new AssetParameterWrapper();
		List<AssetParameterBean> listOfAssetParameterBeans = new ArrayList<>();
		
		for (SearchHit oneHit : response.getHits()) {
			AssetParameterBean assetParameterBean = new AssetParameterBean();
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			assetParameterBean.setAssetName(indexAssetBean.getName());
			assetParameterBean.setAssetId(indexAssetBean.getId());
			for (IndexParameterBean oneParameterBean : indexAssetBean.getParameters()) {
				if(oneParameterBean.getId().equals(parameterId)){
					assetParameterBean.setParameterValue(oneParameterBean.getCurrentValue().getValue());
					assetParameterWrapper.setParameter(oneParameterBean.getDisplayName());
				}
			}
			listOfAssetParameterBeans.add(assetParameterBean);
		}
		assetParameterWrapper.setValues(listOfAssetParameterBeans);
		return assetParameterWrapper;
	}

	@Override
	public ParameterSearchBean getParameterAggregateValue(SearchConfig searchConfig) {
		ParameterSearchBean parameterSearchBean = new ParameterSearchBean();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForParameterAggregate(searchConfig, tenant);

			extractParameterSearchBeanForAggregate(searchConfig, parameterSearchBean, response);
		}
		return parameterSearchBean;
	}

	/**
	 * Prepare {@link ParameterSearchBean} from elasticsearch aggregate response
	 * @param searchConfig
	 * @param parameterSearchBean
	 * @param response
	 */
	private void extractParameterSearchBeanForAggregate(SearchConfig searchConfig, ParameterSearchBean parameterSearchBean,
			SearchResponse response) {
		InternalNested internalNested = response.getAggregations().get(IndexAssetTags.PARAMETERS.getTagKey());
		LongTerms longTerms = internalNested.getAggregations().get("popular_parameters");
		for (Bucket oneBucket : longTerms.getBuckets()) {
			if(oneBucket.getKeyAsNumber().intValue() == searchConfig.getParameterId().intValue()){
				InternalNested internalNested1 = oneBucket.getAggregations().get("avg_val");
				InternalAvg internalAvg = internalNested1.getAggregations().get("myavg");
				parameterSearchBean.setId(oneBucket.getKeyAsNumber().intValue());
				parameterSearchBean.setAggregate(BigDecimal.valueOf((Double) internalAvg.getValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
			}
		}
	}

	/**
	 * Get average of all the assets for a parameter
	 * @param searchConfig
	 * @param tenant
	 * @return
	 */
	private SearchResponse getResponseForParameterAggregate(SearchConfig searchConfig, String tenant) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(extractQueryForHome(searchConfig))
				.addAggregation(AggregationBuilders.nested(IndexAssetTags.PARAMETERS.getTagKey(), IndexAssetTags.PARAMETERS.getTagKey())/*.path(IndexAssetTags.PARAMETERS.getTagKey())*/
						.subAggregation(AggregationBuilders.terms("popular_parameters").field(PARAMETERS_ID).size(50)
								.subAggregation(AggregationBuilders.nested("avg_val", "parameters.currentValue")/*.path("parameters.currentValue")*/
										.subAggregation(AggregationBuilders.avg("myavg").field("parameters.currentValue.value")))))
				.execute()
				.actionGet();
	}

	@Override
	public AssetClassSearchBean getAssetClassDetailsForHomeScreen(SearchConfig searchConfig) {
		int size = 10;
		AssetClassSearchBean assetClassSearchBean=new AssetClassSearchBean();

		for(String tenant: searchConfig.getTenantURLs()){		
			SearchResponse response = getResponseForAssetClass(tenant, size, searchConfig);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForAssetClass(tenant, size, searchConfig);
			}
			assetClassSearchBean = extractAssetClassDetailsBean(searchConfig.getSearchURL(), response);		
		}
		return assetClassSearchBean;
	}

	/**
	 * Prepare {@link AssetClassSearchBean} from elasticsearch response
	 * @param assetClass
	 * @param response
	 * @return
	 */
	private AssetClassSearchBean extractAssetClassDetailsBean(String assetClass, SearchResponse response) {
		AssetClassSearchBean assetClassSearchBean = new AssetClassSearchBean();
		assetClassSearchBean.setName(assetClass);
		assetClassSearchBean.setCount(response.getHits().getTotalHits());
		Set<ParameterSearchBean> setOfParameterSearchBeans =  new HashSet<>();

		for (SearchHit oneHit : response.getHits()) {
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			for (IndexParameterBean oneParameterBean : indexAssetBean.getParameters()) {
				ParameterSearchBean parameterSearchBean = new ParameterSearchBean();
				parameterSearchBean.setId(oneParameterBean.getId());
				parameterSearchBean.setName(oneParameterBean.getDisplayName());
				setOfParameterSearchBeans.add(parameterSearchBean);
			}
		}
		assetClassSearchBean.setParameters(setOfParameterSearchBeans);
		return assetClassSearchBean;
	}

	/**
	 * Get all the parametes for a asset class
	 * @param tenant
	 * @param size
	 * @param assetClass
	 * @param workspaceId
	 * @return
	 */
	private SearchResponse getResponseForAssetClass(String tenant, int size, SearchConfig searchConfig) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setFetchSource(new String[] {PARAMETERS_ID,PARAMETER_DISPLAY_NAME}, null)
				.setQuery(extractQueryForHome(searchConfig))// Query
				.setPostFilter(QueryBuilders.constantScoreQuery(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace())))
				.setSize(size)
				.execute()
				.actionGet();
	}

	/**
	 * Extract home search query with or without filter. Filter query is based of a parameter
	 * filer map in {@link SearchConfig}.
	 * @param searchConfig
	 * @return
	 */
	private QueryBuilder extractQueryForHome(SearchConfig searchConfig) {
		if (searchConfig.getFilterMap().isEmpty()) {			
			return QueryBuilders.matchQuery(ElasticSearchTags.ASSETCLASS.getTagKey(), searchConfig.getSearchURL());
		} else {
			return filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap()).must(QueryBuilders.matchQuery(ElasticSearchTags.ASSETCLASS.getTagKey(), searchConfig.getSearchURL()));
		}
	}

}
