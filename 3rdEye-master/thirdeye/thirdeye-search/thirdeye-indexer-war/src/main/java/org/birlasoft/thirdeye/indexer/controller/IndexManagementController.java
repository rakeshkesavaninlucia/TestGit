package org.birlasoft.thirdeye.indexer.controller;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.indexer.service.IndexAdminHelper;
import org.birlasoft.thirdeye.search.api.beans.IndexRequestBean;
import org.birlasoft.thirdeye.search.api.beans.IndexResponseBean;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/api")
public class IndexManagementController {

	@Autowired
	IndexAdminHelper indexAdminHelper;
	
	@Autowired
	CurrentTenantIdentifierResolver identifierResolver;
	
	/**
	 * This method will be used to build the complete index for a tenant based on the 
	 * @param tenantURL
	 * @param workspaceIds
	 */
	@RequestMapping( value = "/buildFullIndex/tenant/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  buildIndexByWorkspace(@RequestBody IndexRequestBean indexRequestBean){
		
		String tenantURL = indexRequestBean.getTenantURL();
		List<Integer> listOfWorkspaceId = extractWorkspaces(indexRequestBean.getWsIds());
		IndexResponseBean responseBean = new IndexResponseBean(); 
		// Some basic security - however this ties us to the hibernate implementation
		if (identifierResolver.resolveCurrentTenantIdentifier().equals(tenantURL)){
			indexAdminHelper.buildIndexForTenantWorkspace(tenantURL, listOfWorkspaceId);
			responseBean.setMessage("Index created successfully");
		} else {
			// this is an illegal operation
			responseBean.setMessage("Index creation failed");
			
		}
		return new ResponseEntity<>(responseBean,HttpStatus.OK);
	}

	private List<Integer> extractWorkspaces(String[] arrayOfWorkspaceIds) {
		List<Integer> listOfWorkspaceId = new ArrayList<>();
		for (String oneWorkspaceAsString : arrayOfWorkspaceIds){
				listOfWorkspaceId.add(Integer.parseInt(oneWorkspaceAsString.trim()));
	    }
		return listOfWorkspaceId;
	}
}
