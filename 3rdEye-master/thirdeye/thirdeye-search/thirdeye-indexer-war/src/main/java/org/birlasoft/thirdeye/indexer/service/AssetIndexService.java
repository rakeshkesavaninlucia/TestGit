/**
 * 
 */
package org.birlasoft.thirdeye.indexer.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.Workspace;

/**
 * @author shaishav.dixit
 *
 */
public interface AssetIndexService {

	public void buildAssetIndex(List<Workspace> setOfWorkspaces, String tenantId);
}
