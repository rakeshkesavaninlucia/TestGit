package org.birlasoft.thirdeye.indexer.converter;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.search.api.constant.ElasticSearchTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAidBlockTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAidSubBlockTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAidTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetParentChildTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.api.constant.IndexDataTags;
import org.birlasoft.thirdeye.search.api.constant.IndexParameterTags;
import org.birlasoft.thirdeye.search.api.constant.IndexParameterValueTags;
import org.birlasoft.thirdeye.search.api.constant.IndexQuestionTags;
import org.birlasoft.thirdeye.search.api.constant.IndexQuestionValueTags;
import org.birlasoft.thirdeye.search.api.constant.IndexRelationshipAssetDataTags;
import org.birlasoft.thirdeye.search.api.constant.IndexRelationshipAssetTags;
import org.birlasoft.thirdeye.search.api.constant.IndexTemplateColsValueTags;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AssetConverter implements JSONConverter<IndexAssetBean>{

	private static Logger logger = LoggerFactory.getLogger(AssetConverter.class);
	
	@Override
	public Optional<XContentBuilder> convertForWrite(IndexAssetBean oneAsset) {
		
		try {
			XContentBuilder contentBuilder = jsonBuilder().startObject();
			
			contentBuilder.startObject(ElasticSearchTags.TEMPLATECOLS.getTagKey());
			contentBuilder.endObject();
			
			contentBuilder.field(ElasticSearchTags.WORKSPACE.getTagKey(), oneAsset.getWorkspaceId());
			contentBuilder.field(ElasticSearchTags.ASSETCLASS.getTagKey(), oneAsset.getAssetClass());
			
			// Build parameter list for the index
			// Build the quality gates for the index
			// Any other date for update / putting into the system.
			
			contentBuilder.endObject();
			
			return Optional.of(contentBuilder);
			
		} catch (IOException e) {
			logger.error("Unable to convert asset bean " , e);
		}
		
		return Optional.empty();
	}
	
	
	@Override
	public XContentBuilder generateMappingForObject(IndexAssetBean t) {
		// This method is to given the JSOn so that you can 
		// create the put mapping into the asset type index.
		
		XContentBuilder contentBuilder = null;
		try {
			contentBuilder = jsonBuilder().startObject();
			contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
			
			contentBuilder.startObject(ElasticSearchTags.UID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
			contentBuilder.startObject(ElasticSearchTags.ASSETCLASS.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
			contentBuilder.startObject(ElasticSearchTags.WORKSPACE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
			
			
			insertTemplateColumnsMapping(t, contentBuilder);
			
			//prepare parameter mapping
			insertParametersMapping(contentBuilder);
			//prepare cost structure mapping
			insertCostStructureMapping(contentBuilder);
			
			//prepare Question mapping
			insertQuestionsMapping(contentBuilder);
			
			//prepare AID mapping
			insertAidMapping(contentBuilder);
			
			// Prepare Relationship Mapping
			insertRelationshipMapping(contentBuilder);
			
			contentBuilder.endObject();			
			contentBuilder.endObject();
			if(logger.isInfoEnabled()) {				
				logger.info("Creating new mapping : " + contentBuilder.string());
			}
		} catch (IOException e) {
			logger.error("Unable to create the mapping JSON for the asset " , e);
			return null;
		}
		
		
		return contentBuilder;
	}


	private void insertRelationshipMapping(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexAssetTags.RELATIONSHIP.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		
		insertParentAssetRelationshipMapping(contentBuilder,IndexAssetParentChildTags.PARENTS.getTagKey());
		insertParentAssetRelationshipMapping(contentBuilder,IndexAssetParentChildTags.CHILDREN.getTagKey());
		contentBuilder.endObject();
		contentBuilder.endObject();
	}


	private void insertParentAssetRelationshipMapping(XContentBuilder contentBuilder, String parentChildKey) throws IOException {
		contentBuilder.startObject(parentChildKey);
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		
		contentBuilder.startObject(IndexRelationshipAssetTags.ASSETID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetTags.ASSETNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetTags.ASSETSTYLE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		
		insertAssetRelationshipDataMapping(contentBuilder,IndexRelationshipAssetTags.RELATIONSHIPASSETDATA.getTagKey());
		
		contentBuilder.endObject();
		contentBuilder.endObject();
	}
	
	private void insertAssetRelationshipDataMapping(XContentBuilder contentBuilder, String relationshipDataKey) throws IOException {
		contentBuilder.startObject(relationshipDataKey);
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		
		contentBuilder.startObject(IndexRelationshipAssetDataTags.RELATIONSHIPNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetDataTags.DATATYPE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetDataTags.FREQUENCY.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetDataTags.RELATIONSHIPDISPLAYNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexRelationshipAssetDataTags.DIRECTION.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		
		contentBuilder.endObject();
		contentBuilder.endObject();
	}


	private void insertAidMapping(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexAssetTags.AIDS.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		
		contentBuilder.startObject(IndexAidTags.ID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexAidTags.NAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexAidTags.DESCRIPTION.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();	
		contentBuilder.startObject(IndexAidTags.AIDTEMPLATE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		
		insertAidBlocksMapping(contentBuilder,IndexAidTags.BLOCKS.getTagKey());
		contentBuilder.endObject();
		contentBuilder.endObject();
	}


	private void insertAidBlocksMapping(XContentBuilder contentBuilder, String blockKey) throws IOException {
	   contentBuilder.startObject(blockKey);
	   contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
	   contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
	   
	   contentBuilder.startObject(IndexAidBlockTags.ID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
	   contentBuilder.startObject(IndexAidBlockTags.TITLE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
	   contentBuilder.startObject(IndexAidBlockTags.SEQUENCENUMBER.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();

	   insertAidSubBlocksMapping(contentBuilder,IndexAidBlockTags.SUBBLOCKS.getTagKey());
	   
	   contentBuilder.endObject();
	   contentBuilder.endObject();
	}


	private void insertAidSubBlocksMapping(XContentBuilder contentBuilder, String subBlockKey) throws IOException {
	   contentBuilder.startObject(subBlockKey);
	   contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
	   contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
	   
	   contentBuilder.startObject(IndexAidSubBlockTags.SUBBLOCKNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
	   contentBuilder.startObject(IndexAidSubBlockTags.TITLE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
	   contentBuilder.startObject(IndexAidSubBlockTags.CONFIGVALUE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
	   contentBuilder.endObject();
	   contentBuilder.endObject();
	}


	private void insertTemplateColumnsMapping(IndexAssetBean t, XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(ElasticSearchTags.TEMPLATECOLS.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		
		
		for (String oneCol : t.getTemplateCols().keySet()) {
			getForColumnAndDataType(contentBuilder, t.getTemplateCols().get(oneCol));
		}
		
		
		contentBuilder.endObject();
		contentBuilder.endObject();
	}
	
	/**
	 * method to prepare mapping for Asset Parameters
	 * @param contentBuilder
	 * @throws IOException
	 */
	
	private void insertParametersMapping(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexAssetTags.PARAMETERS.getTagKey());
		prepareParameterMapping(contentBuilder);
		contentBuilder.endObject();
	}
	
	/**
	 * method to prepare mapping for Asset Cost Structure
	 * @param contentBuilder
	 * @throws IOException
	 */
	
	private void insertCostStructureMapping(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexAssetTags.COSTSTRUCTURES.getTagKey());
		prepareParameterMapping(contentBuilder);
		contentBuilder.endObject();
	}


	private void prepareParameterMapping(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());

		contentBuilder.startObject(IndexParameterTags.ID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexParameterTags.DISPLAYNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexParameterTags.UNIQUENAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();	
		contentBuilder.startObject(IndexParameterTags.TYPE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();	
		insertParameterValueTypeMapping(contentBuilder,IndexParameterTags.VALUES.getTagKey());	
		insertParameterValueTypeMapping(contentBuilder,IndexParameterTags.CURRENTVALUE.getTagKey());

		contentBuilder.endObject();
	}
	
	private void insertQuestionsMapping(XContentBuilder contentBuilder) throws IOException {
		
		   contentBuilder.startObject(IndexAssetTags.QUESTIONS.getTagKey());
		   contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		   contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		   
		   //prepare mapping for one question
		   contentBuilder.startObject(IndexQuestionTags.ID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		   contentBuilder.startObject(IndexQuestionTags.DISPLAYNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		   insertQuestionValueTypeMapping(contentBuilder,IndexQuestionTags.VALUES.getTagKey());		   
		   
		   contentBuilder.endObject();
		   contentBuilder.endObject();
		
	}
	

	private void insertParameterValueTypeMapping(XContentBuilder contentBuilder,String colName)	throws IOException {
		
		   contentBuilder.startObject(colName);
		   contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		   contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
		   
		   contentBuilder.startObject(IndexParameterValueTags.VALUEDATE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DATE.getTagKey()).field(IndexDataTags.FORMAT.getTagKey(),"yyyy-MM-dd HH:mm:ss").endObject();
		   contentBuilder.startObject(IndexParameterValueTags.QUESTIONNAIREID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		   contentBuilder.startObject(IndexParameterValueTags.VALUE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DOUBLE.getTagKey()).endObject();		
		   contentBuilder.startObject(IndexParameterValueTags.COLOR.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();		
		   contentBuilder.startObject(IndexParameterValueTags.COLORDESC.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();	
		
		   contentBuilder.endObject();
		   contentBuilder.endObject();
	}
	
	private void insertQuestionValueTypeMapping(XContentBuilder contentBuilder,String colName)	throws IOException {
		
		   contentBuilder.startObject(colName);
		   contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		   contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());	
		   
		   contentBuilder.startObject(IndexQuestionValueTags.VALUEDATE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DATE.getTagKey()).field(IndexDataTags.FORMAT.getTagKey(),IndexDataTags.DATE_FORMATE_SS.getTagKey()).endObject();
		   contentBuilder.startObject(IndexQuestionValueTags.QUESTIONNAIREID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		   contentBuilder.startObject(IndexQuestionValueTags.QUESTIONNAIREPARAMETERID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		   contentBuilder.startObject(IndexQuestionValueTags.VALUE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DOUBLE.getTagKey()).endObject();		
		   contentBuilder.startObject(IndexQuestionValueTags.COLOR.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();		
		   contentBuilder.startObject(IndexQuestionValueTags.COLORDESC.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();	
		
		   contentBuilder.endObject();
		   contentBuilder.endObject();
	}
	
	@Override
	public Optional<List<IndexAssetBean>> readFromResponse(SearchResponse searchResponse) {
		return Optional.empty();
	}
	
	private XContentBuilder getForColumnAndDataType (XContentBuilder contentBuilder, TemplateColumnBean oneCol) throws IOException{
		contentBuilder.startObject(oneCol.getName());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
	    contentBuilder.startObject(IndexTemplateColsValueTags.SEQUENCENUMBER.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
	    contentBuilder.startObject(IndexTemplateColsValueTags.FILTERABLE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
	    contentBuilder.startObject(IndexTemplateColsValueTags.COLUMNDATATYPE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
		switch (oneCol.getColumnDataType()){
		case DATE:
			contentBuilder.startObject(IndexTemplateColsValueTags.VALUE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DATE.getTagKey()).field(IndexDataTags.FORMAT.getTagKey(),IndexDataTags.DATE_FORMATE.getTagKey()).endObject();
			break;
		case NUMBER:
			contentBuilder.startObject("value").field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.DOUBLE.getTagKey()).endObject();
			break;
		case TEXT:		
			contentBuilder.startObject("value").field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
			break;
		}
		
		contentBuilder.endObject();
		contentBuilder.endObject();
		return contentBuilder;
	}
	
	
	
}
