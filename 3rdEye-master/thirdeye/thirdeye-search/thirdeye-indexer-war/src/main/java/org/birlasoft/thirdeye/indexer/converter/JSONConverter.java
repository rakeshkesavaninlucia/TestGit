package org.birlasoft.thirdeye.indexer.converter;

import java.util.List;
import java.util.Optional;

import org.birlasoft.thirdeye.search.api.beans.AbstractProtectedEntityBean;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;

/**
 * The converters are used marshall objects for the usage by Elasticsearch.
 * They are also capable of creating the mappings for the objects in the elastic search which is required
 * before the index for the data type can be created.
 * 
 * @author tej.sarup
 *
 * @param <T>
 */
//TODO we should be able to register the converters in some place post construct
// Can we use spring converter factory
public interface JSONConverter <T extends AbstractProtectedEntityBean> {

	public Optional<XContentBuilder> convertForWrite(T t);
	
	public XContentBuilder generateMappingForObject(T t);
	
	public Optional<List<T>> readFromResponse(SearchResponse searchResponse);
	
	
	
}
