package org.birlasoft.thirdeye.indexer.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionMapper;
import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionValueMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.aid.AIDCentralAssetWrapperBean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock1Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock2Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock3Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock4Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlockConfig;
import org.birlasoft.thirdeye.beans.aid.JSONAIDSubBlockConfig;
import org.birlasoft.thirdeye.beans.aid.RelatedAssetBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.colorscheme.service.Colorizer;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.ParameterQualityGate;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.indexer.converter.AssetConverter;
import org.birlasoft.thirdeye.indexer.service.AssetIndexService;
import org.birlasoft.thirdeye.indexer.service.IndexAdministratorService;
import org.birlasoft.thirdeye.repositories.ParameterQualityGateRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidSubBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetParentChildBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterValueTypeBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionValueTypeBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetDataBean;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implementation class of AssetIndexService for  buildAssetIndex , getAllQuestionsForAssetIndex ,
 * getAllParametersForAssetIndex ,fetchParameterValueBean ,fetchQuestionValueBean,getQuestionResponseScore,
 * and insertDataIntoIndex
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetIndexServiceImpl implements AssetIndexService {
	
	private static Logger logger = LoggerFactory.getLogger(AssetIndexServiceImpl.class);


	@Autowired
	private Client client;

	@Autowired
	private IndexAdministratorService adminService;


	@Autowired
	private AssetTemplateService assetTemplateService;

	@Autowired
	private AssetConverter assetConverter;
	
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	
	@Autowired
	private ParameterService pService;
	
	@Autowired
	private Colorizer colorizer;
	
	@Autowired
	private QuestionnaireQuestionService qqservice;
	
	@Autowired
	private AIDService aidService;

	@Autowired
	private RelationshipTemplateService relationshipTemplateService;
	
	@Autowired
	private AssetService assetService;
	
	@Autowired
	private ParameterQualityGateRepository parameterQualityGateRepository;
	
	@Autowired
	private QualityGateService qualityGateService;
	
	public static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
	public static final int NA = -1;
	public static final BigDecimal NA_BIGDECIMAL = new BigDecimal(NA);

	@Override
	public void buildAssetIndex(List<Workspace> listOfWorkspace, String tenantId) {

		// Find all the assets in the templates and load them one at time.
		List<AssetTemplate> listOfTemplates = assetTemplateService.findByWorkspaceIn(listOfWorkspace);
		
		// Fetch all aids in listOfWorkspace
		List<Aid> listOfAid = aidService.findByWorkspaceIn(new HashSet<Workspace>(listOfWorkspace));

		for (AssetTemplate oneTemplate : listOfTemplates) {		
			List<IndexAssetBean> listForIndexing = new ArrayList<>();
			
			boolean setTypeMapping = true;
			Set<Asset> allAssetsForTheTemplate = oneTemplate.getAssets();

			for (Asset oneAsset : allAssetsForTheTemplate){
				IndexAssetBean oneBean = new IndexAssetBean(tenantId, oneAsset);
				//get all parameter for Asset to index
				List<IndexParameterBean> allParametersForAsset = getAllParametersForAssetIndex(oneAsset);
				oneBean.setParameters(allParametersForAsset);	
				
				//get all cost structure for Asset to index
				List<IndexParameterBean> allCostStructuresForAsset = getAllCostStructureForAssetIndex(oneAsset);
				oneBean.setCostStuctures(allCostStructuresForAsset);	
				
				//get all question for Asset to index
				Set<IndexQuestionBean> allQuestionsForAsset = getAllQuestionsForAssetIndex(oneAsset);
				oneBean.setQuestions(allQuestionsForAsset);
				
				//get all aids for Asset to index
				List<IndexAidBean> allAidsForAsset = getAllAidsForAssetIndex(oneAsset, listOfAid);
				oneBean.setAids(allAidsForAsset);
				
				// Get relationship for Asset to index
				oneBean.setRelationship(getAssetParentChildForAssetIndex(oneAsset));
							
				if (setTypeMapping){
					String assetTypeName = generateAssetTypeName(oneBean);
					adminService.createIndexAndMapping(tenantId, assetTypeName, assetConverter.generateMappingForObject(oneBean));
					setTypeMapping = false;
				}

				listForIndexing.add(oneBean);
			}
			if(!listForIndexing.isEmpty()){
			insertDataIntoIndex(listForIndexing);
			}
		}
	}

    
	private IndexAssetParentChildBean getAssetParentChildForAssetIndex(Asset oneAsset) {
		IndexAssetParentChildBean assetParentChildBean = new IndexAssetParentChildBean();
		
		// Create parent assets for Asset
		assetParentChildBean.setParents(getParentChildRelationshipIndex(relationshipTemplateService.fetchInBoundRelationships(oneAsset)));
		
		// Create child assets for Asset
		assetParentChildBean.setChildren(getParentChildRelationshipIndex(relationshipTemplateService.fetchOutBoundRelationships(oneAsset)));
		
		return assetParentChildBean;
	}

	private List<IndexRelationshipAssetBean> getParentChildRelationshipIndex(List<RelatedAssetBean> relationshipAssetData) {
		List<IndexRelationshipAssetBean> parentChildRel = new ArrayList<>();
		for (RelatedAssetBean rab : relationshipAssetData) {
			IndexRelationshipAssetBean parentcChildRelationshipAssetBean = new IndexRelationshipAssetBean();
			parentcChildRelationshipAssetBean.setAssetId(rab.getAsset().getId());
			parentcChildRelationshipAssetBean.setAssetName(rab.getAsset().getShortName());
			parentcChildRelationshipAssetBean.setAssetStyle(rab.getAsset().getAssetStyle());
			List<IndexRelationshipAssetDataBean> relationshipAssetDataBeans = new ArrayList<>();
			for (RelationshipAssetDataBean assetDataBean : rab.getListOfRelationshipAssetData()) {
				IndexRelationshipAssetDataBean relationshipAssetDataBean = new IndexRelationshipAssetDataBean();
				relationshipAssetDataBean.setRelationshipName(assetDataBean.getDisplayName());
				relationshipAssetDataBean.setDataType(assetDataBean.getDataType());
				relationshipAssetDataBean.setFrequency(assetDataBean.getFrequency());
				setRelationshipDirectionAndDisplayName(assetDataBean, relationshipAssetDataBean);
				relationshipAssetDataBeans.add(relationshipAssetDataBean);
			}
			parentcChildRelationshipAssetBean.setRelationshipAssetData(relationshipAssetDataBeans);
			parentChildRel.add(parentcChildRelationshipAssetBean);
		}
		return parentChildRel;
	}


	/** Method to set display Name and direction for a relationship to be set in indexing.
	 * @param assetDataBean
	 * @param relationshipAssetDataBean
	 */
	private void setRelationshipDirectionAndDisplayName(RelationshipAssetDataBean assetDataBean,
            IndexRelationshipAssetDataBean relationshipAssetDataBean) {
     Integer relationshipAssetId = assetDataBean.getAssetIdOfRelationship();
     if( relationshipAssetId != null){
            Asset relationshipAsset = assetService.findOne(relationshipAssetId);
            if(!relationshipAsset.getAssetTemplate().getRelationshipTemplates().isEmpty() ){
                  RelationshipType rType = new ArrayList<RelationshipTemplate>(relationshipAsset.getAssetTemplate().getRelationshipTemplates()).get(0).getRelationshipType();
                  relationshipAssetDataBean.setDirection(rType.getDirection());
                  relationshipAssetDataBean.setRelationshipDisplayName(rType.getDisplayName());
            }
     }
}



	private List<IndexAidBean> getAllAidsForAssetIndex(Asset oneAsset, List<Aid> listOfAid) {
		List<IndexAidBean> aids = new ArrayList<>();
		
		if(null != listOfAid && !listOfAid.isEmpty()){
			for (Aid aid : listOfAid) {
				// Compare asset template id in asset and aid  
				if(aid.getAssetTemplate().getId().equals(oneAsset.getAssetTemplate().getId())){
					IndexAidBean indexAidBean = new IndexAidBean();
					indexAidBean.setId(aid.getId());
					indexAidBean.setName(aid.getName());
					indexAidBean.setDescription(aid.getDescription());
					indexAidBean.setAidTemplate(aid.getAidTemplate());
					// Create blocks in aid
					Map<Integer, Map<Integer, BigDecimal>> mapOfAssetIdParamIdAndParamValue = aidService.prepareAssetParameterMap(aid, oneAsset);
					AIDCentralAssetWrapperBean aidAssetBean = aidService.getCentralAssetForDisplay(aid, oneAsset, mapOfAssetIdParamIdAndParamValue.get(oneAsset.getId()));
					List<IndexAidBlockBean> listOfAidBlockBeans = new ArrayList<>();
					List<JSONAIDBlockConfig> blockConfigs  = aidAssetBean.getListOfBlocksInMainApp();
					// Load it in one block at a time
					for (Object obj : blockConfigs) {
						JSONAIDBlockConfig aidBlock = (JSONAIDBlockConfig) obj;
						IndexAidBlockBean indexAidBlockBean = new IndexAidBlockBean();
						indexAidBlockBean.setId(aidBlock.getId());
						indexAidBlockBean.setTitle(aidBlock.getBlockTitle());
						indexAidBlockBean.setAidBlockType(aidBlock.getAidBlockType().toString());
						indexAidBlockBean.setSequenceNumber(aidBlock.getSequenceNumber());
						listOfAidBlockBeans.add(getUpdatedIndexAidBlockBean(obj, indexAidBlockBean));
					}
					indexAidBean.setBlocks(listOfAidBlockBeans);
					aids.add(indexAidBean);
				}
			}
		}
		
		return aids;
	}
	
	private IndexAidBlockBean getUpdatedIndexAidBlockBean(Object obj, IndexAidBlockBean indexAidBlockBean){
		if(obj instanceof JSONAIDBlock1Bean){
			JSONAIDBlock1Bean jsonaidBlock1Bean = (JSONAIDBlock1Bean) obj;
			indexAidBlockBean.setSubBlocks(getListOfIndexAidSubBlocks(jsonaidBlock1Bean.getListOfBlocks()));
		}else if(obj instanceof JSONAIDBlock2Bean){
			JSONAIDBlock2Bean jsonaidBlock2Bean = (JSONAIDBlock2Bean) obj;
			indexAidBlockBean.setSubBlocks(getListOfIndexAidSubBlocks(jsonaidBlock2Bean.getListOfBlocks()));
		} else if(obj instanceof JSONAIDBlock3Bean){
			JSONAIDBlock3Bean jsonaidBlock3Bean = (JSONAIDBlock3Bean) obj;
			indexAidBlockBean.setSubBlocks(getListOfIndexAidSubBlocks(jsonaidBlock3Bean.getListOfBlocks()));
		} else if(obj instanceof JSONAIDBlock4Bean){
			JSONAIDBlock4Bean jsonaidBlock4Bean = (JSONAIDBlock4Bean) obj;
			indexAidBlockBean.setSubBlocks(getListOfIndexAidSubBlocks(jsonaidBlock4Bean.getListOfBlocks()));
		}
		return indexAidBlockBean;
	}


	private List<IndexAidSubBlockBean> getListOfIndexAidSubBlocks(List<JSONAIDSubBlockConfig> subBlockConfigs) {
		List<IndexAidSubBlockBean> subBlocks = new ArrayList<>();
		for (JSONAIDSubBlockConfig jsonaidSubBlockConfig : subBlockConfigs) {
			IndexAidSubBlockBean indexAidSubBlockBean = new IndexAidSubBlockBean();
			indexAidSubBlockBean.setTitle(jsonaidSubBlockConfig.getBlockTitle());
			indexAidSubBlockBean.setSubBlockName(jsonaidSubBlockConfig.getSubBlockIdentifier());
			indexAidSubBlockBean.setConfigValue(jsonaidSubBlockConfig.getDisplayString());
			subBlocks.add(indexAidSubBlockBean);
		}
		return subBlocks;
	}


	private Set<IndexQuestionBean> getAllQuestionsForAssetIndex(Asset oneAsset) {
		Map<Integer , IndexQuestionBean> listOfQuestionForAsset = new HashMap<>();
		List<QuestionnaireAsset> listOfQA = questionnaireAssetService.findByAsset(oneAsset);
		Set<QuestionnaireQuestion>  setOfQQ = qqservice.findByQuestionnaireAssetIn(new HashSet<QuestionnaireAsset>(listOfQA));
		for(QuestionnaireQuestion oneQQ : setOfQQ){	
			IndexQuestionBean questionBean ;
			if(listOfQuestionForAsset.containsKey(oneQQ.getQuestion().getId())){
				questionBean = listOfQuestionForAsset.get(oneQQ.getQuestion().getId());
				IndexQuestionValueTypeBean value = fetchQuestionValueBean(oneQQ);
				if(!value.getValue().equals(NA_BIGDECIMAL)){
				  questionBean.getValues().add(value);	
				}
			}
			else{
			    questionBean = new IndexQuestionBean();
			    questionBean.setId(oneQQ.getQuestion().getId());
			    questionBean.setDisplayName(oneQQ.getQuestion().getDisplayName());
			    IndexQuestionValueTypeBean value = fetchQuestionValueBean(oneQQ);			   
			    if(!value.getValue().equals(NA_BIGDECIMAL)){
			      List<IndexQuestionValueTypeBean> values = new ArrayList<>();
				  values.add(value);
				  questionBean.setValues(values);	
			      listOfQuestionForAsset.put(oneQQ.getQuestion().getId(), questionBean);
			    }
			}			
		}
		
		Set<IndexQuestionBean> questionsToReturn = new HashSet<>();
		for (IndexQuestionBean pb : listOfQuestionForAsset.values()){
			if (pb!=null){
				questionsToReturn.add(pb);
			}
		}
		
		return questionsToReturn;
	}



	private List<IndexParameterBean> getAllParametersForAssetIndex(Asset oneAsset) {	
		//Get questionnaires for Asset from QuestionnairesAsset
		List<Questionnaire> questionnaires = getQuestionnaireForAsset(oneAsset,QuestionnaireType.DEF.toString());
		return getParameterToReturns(oneAsset, questionnaires);
	}

	private List<IndexParameterBean> getAllCostStructureForAssetIndex(Asset oneAsset) {	
		//Get questionnaires for Asset from QuestionnairesAsset
		List<Questionnaire> questionnaires = getQuestionnaireForAsset(oneAsset,QuestionnaireType.TCO.toString());
		return getParameterToReturns(oneAsset, questionnaires);
	}

	private List<IndexParameterBean> getParameterToReturns(Asset oneAsset, List<Questionnaire> questionnaires) {
		Map<Integer, IndexParameterBean> mapOfParameterForAsset = getMapOfParameterForAsset(oneAsset, questionnaires);
		
		List<IndexParameterBean> parametersToReturn = new ArrayList<>();
		for (IndexParameterBean pb : mapOfParameterForAsset.values()){
			if (pb!=null){
				parametersToReturn.add(pb);
			}
		}		
		return parametersToReturn;
	}


	private Map<Integer, IndexParameterBean> getMapOfParameterForAsset(Asset oneAsset,List<Questionnaire> questionnaires) {
		Map<Integer , IndexParameterBean> mapOfParameterForAsset = new HashMap<>();
		for(Questionnaire oneQuestionnaire : questionnaires){
			List<ParameterEvaluationResponse> listOfParameterResponses = getListOfParameterResponses(oneAsset,oneQuestionnaire);
			for(ParameterEvaluationResponse oneEvaluationResponse: listOfParameterResponses){			
				IndexParameterBean parameterBean;
				if(mapOfParameterForAsset.containsKey(oneEvaluationResponse.getParameterBean().getId())){
					   parameterBean  = mapOfParameterForAsset.get(oneEvaluationResponse.getParameterBean().getId());						   
					   IndexParameterValueTypeBean parameterValueTypeBean = fetchParameterValueBean(oneAsset, oneQuestionnaire, oneEvaluationResponse);
					   parameterBean.getValues().add(parameterValueTypeBean);
					   validateDate(oneQuestionnaire, parameterBean,parameterValueTypeBean);
				}else{				
					   parameterBean = new IndexParameterBean();
					   parameterBean.setId(oneEvaluationResponse.getParameterBean().getId());
					   parameterBean.setDisplayName(oneEvaluationResponse.getParameterBean().getDisplayName());
					   parameterBean.setUniqueName(oneEvaluationResponse.getParameterBean().getUniqueName());
					   parameterBean.setType(oneQuestionnaire.getQuestionnaireType());
					   //Prepare current Value for QE
					   IndexParameterValueTypeBean currentValue =  fetchParameterValueBean(oneAsset, oneQuestionnaire, oneEvaluationResponse);					  
					   parameterBean.setCurrentValue(currentValue);
					   List<IndexParameterValueTypeBean> values = new ArrayList<>();
					   values.add(currentValue);
					   parameterBean.setValues(values);
					   mapOfParameterForAsset.put(oneEvaluationResponse.getParameterBean().getId(), parameterBean);
				}
								
			}		
		}
		return mapOfParameterForAsset;
	}


	private List<ParameterEvaluationResponse> getListOfParameterResponses(Asset oneAsset,
			Questionnaire oneQuestionnaire) {
		List<QuestionnaireParameter>listOfQP = questionnaireParameterService.findByQuestionnaire(oneQuestionnaire);
		
		Set<Integer> idsOfParameters = new HashSet<>();
		for(QuestionnaireParameter oneQP :listOfQP){
		  idsOfParameters.add(oneQP.getParameterByParameterId().getId());
		}			
		//TODO::Need to discuss evaluteParameter method need thirdEye assetBean which is extended by CSSBean
		Set<AssetBean> assetBeans = new HashSet<>();
		assetBeans.add(new AssetBean(oneAsset));
		
		List<ParameterEvaluationResponse> listOfParameterResponses = new ArrayList<>();
		
		if(oneQuestionnaire.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.DEF.toString())){
		  listOfParameterResponses = pService.evaluateParameters(idsOfParameters, oneQuestionnaire.getId(), assetBeans);
		}else if(oneQuestionnaire.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.TCO.toString())){
		  listOfParameterResponses = pService.evaluateTcoParameters(oneQuestionnaire.getId(), assetBeans);
		}
		return listOfParameterResponses;
	}


	private void validateDate(Questionnaire oneQuestionnaire,
			IndexParameterBean parameterBean,
			IndexParameterValueTypeBean parameterValueTypeBean) {
		try {
			if(df.parse(parameterBean.getCurrentValue().getValueDate()).before(oneQuestionnaire.getCreatedDate())){
				   parameterBean.setCurrentValue(parameterValueTypeBean);
			   }
		   } catch (ParseException e) {
			  logger.error("Exception in AssetIndexServiceImpl.getAllParametersForAssetIndex()"+e);
		   }
	}
	
	/**
	 * method to set data in ParameterValueTypeBean
	 * @param oneAsset
	 * @param oneQuestionnaire
	 * @param oneEvaluationResponse
	 * @return
	 */

	private IndexParameterValueTypeBean fetchParameterValueBean(Asset oneAsset,Questionnaire oneQuestionnaire, ParameterEvaluationResponse oneEvaluationResponse) {
		IndexParameterValueTypeBean parameterValueTypeBean = new IndexParameterValueTypeBean();
		
		parameterValueTypeBean.setValueDate(df.format(oneQuestionnaire.getCreatedDate()));		
		parameterValueTypeBean.setQuestionnaireId(oneQuestionnaire.getId());
		parameterValueTypeBean.setValue( oneEvaluationResponse.fetchParamValueForAsset(new AssetBean(oneAsset)));
		List<AssetBean> listOfAssetBeans = (List<AssetBean>) colorizer.getParameterColor(oneEvaluationResponse.getParameterBean(), oneEvaluationResponse.getAssetResponse());
		if(!listOfAssetBeans.isEmpty()){
		   AssetBean oneAssetBean =   listOfAssetBeans.get(0);
		   parameterValueTypeBean.setColor(oneAssetBean.getHexColor());
		}
		parameterValueTypeBean.setColorDesc(getParameterColorDescription(parameterValueTypeBean.getColor(), oneEvaluationResponse.getParameterBean().getId()));
		return parameterValueTypeBean;
	}
	
	private String getParameterColorDescription(String color, Integer parameterId) {
		String colorDescription = "";
		
		ParameterQualityGate parameterQualityGate = parameterQualityGateRepository.findByParameter(pService.findOne(parameterId));
		if(parameterQualityGate != null) {
			QualityGate qg = qualityGateService.findOne(parameterQualityGate.getQualityGate().getId());
			String colorDescJSON = qg.getColorDescription();
			if(colorDescJSON != null) {
				JSONQualityGateDescriptionMapper descriptionMapper = Utility.convertJSONStringToObject(colorDescJSON, JSONQualityGateDescriptionMapper.class);
				List<JSONQualityGateDescriptionValueMapper> descriptionValueMappers = descriptionMapper.getDescription();
				if(descriptionValueMappers != null) {
					for (JSONQualityGateDescriptionValueMapper jsonQualityGateDescriptionValueMapper : descriptionValueMappers) {
						if(jsonQualityGateDescriptionValueMapper.getColor().equals(color)) {
							colorDescription = jsonQualityGateDescriptionValueMapper.getDescription();
							break;
						}
					}
				}
			}
		}
		
		return colorDescription;
	}
	
	/**
	 * method to set data in QuestionValueTypeBean 
	 * @param oneQQ
	 * @return
	 */
	
	private IndexQuestionValueTypeBean fetchQuestionValueBean(QuestionnaireQuestion oneQQ) {
		IndexQuestionValueTypeBean questionValuetypeBean = new IndexQuestionValueTypeBean();
		questionValuetypeBean.setValueDate(df.format(oneQQ.getQuestionnaire().getCreatedDate()));
		questionValuetypeBean.setQuestionnaireId(oneQQ.getQuestionnaire().getId());
		questionValuetypeBean.setQuestionnaireParameterId(oneQQ.getQuestionnaireParameter().getId());
		//set question response value	
		questionValuetypeBean.setValue(getQuestionResponseScore(oneQQ));
		return questionValuetypeBean;
	}

/**
 * method to get question response for QuestionnaireQuestion
 * @param oneQQ
 * @return score
 */
	private BigDecimal getQuestionResponseScore(QuestionnaireQuestion oneQQ) {
		ResponseData responseData ;
		List<ResponseData> responseDatas = new ArrayList<>(oneQQ.getResponseDatas());
		BigDecimal value = new BigDecimal(NA);
		if(!responseDatas.isEmpty()){
		  responseData = responseDatas.get(0);
		  String queType = oneQQ.getQuestion().getQuestionType();			
			if(queType.equals(QuestionType.TEXT.toString())){
				 JSONTextResponseMapper jsonTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONTextResponseMapper.class);
				 value = jsonTextResponseMapper.fetchQuantifiableResponse();
				
			}else if(queType.equals(QuestionType.PARATEXT.toString())){
				JSONParaTextResponseMapper jsonParaTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONParaTextResponseMapper.class);
				value = jsonParaTextResponseMapper.fetchQuantifiableResponse();
				
			}else if(queType.equals(QuestionType.DATE.toString())){
				JSONDateResponseMapper jsonDateResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONDateResponseMapper.class);
				value = jsonDateResponseMapper.fetchQuantifiableResponse();			
			}else if(queType.equals(QuestionType.MULTCHOICE.toString())){
				if(oneQQ.getQuestion().getBenchmark() == null) {
					JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONMultiChoiceResponseMapper.class);
					value = jsonMultiChoiceResponseMapper.fetchQuantifiableResponse();
				} else {
					JSONBenchmarkMultiChoiceResponseMapper jsonBenchmarkMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONBenchmarkMultiChoiceResponseMapper.class);
					value = jsonBenchmarkMultiChoiceResponseMapper.fetchQuantifiableResponse();
				}
				
			}else if(queType.equals(QuestionType.NUMBER.toString())){
				JSONNumberResponseMapper jsonNumberResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
				value = jsonNumberResponseMapper.fetchQuantifiableResponse();				
			}
			else if(queType.equals(QuestionType.CURRENCY.toString())){
				JSONNumberResponseMapper jsonNumberResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
				value = jsonNumberResponseMapper.fetchQuantifiableResponse();				
			}
		}
		return value;
	}


  /**
   * method to return questionnaires for asset from QuestionnaireAsset
   * @param oneAsset
   * @return
   */
	private List<Questionnaire> getQuestionnaireForAsset(Asset oneAsset,String questionnaireType) {
		List<Questionnaire> questionnaires = new ArrayList<>();
		List<QuestionnaireAsset> listOfQA = questionnaireAssetService.findByAsset(oneAsset);
		//prepare list of questionnaires to fetch Q_P		
		for(QuestionnaireAsset oneQA :listOfQA){
			if(questionnaireType.equals(oneQA.getQuestionnaire().getQuestionnaireType()))
			   questionnaires.add(oneQA.getQuestionnaire());
		}
		return questionnaires;
	}


	private String generateAssetTypeName(IndexAssetBean oneAssetBean) {
		return oneAssetBean.getAssetTemplateName().replaceAll(" ", "_");
		
	}

	/**
	 * method to insert tenant data into index.
	 * @param listOfAssets
	 */
	public void insertDataIntoIndex(List<IndexAssetBean> listOfAssets) { 

		BulkRequestBuilder bulkRequest = client.prepareBulk();

		for (IndexAssetBean oneAsset : listOfAssets) {
			
			// instance a json mapper
			ObjectMapper mapper = new ObjectMapper(); // create once, reuse

			// generate json
			byte[] json;
			try {
				json = mapper.writeValueAsBytes(oneAsset);
				bulkRequest.add(client.prepareIndex(oneAsset.getTenantURL().toLowerCase(), generateAssetTypeName(oneAsset), String.valueOf(oneAsset.getId()))
						/*.setSource(assetConverter.convertForWrite(oneAsset).get())*/
						.setSource(json)
						);
			} catch (JsonProcessingException e) {
				logger.error("Exception in AssetIndexServiceImpl.insertDataIntoIndex()"+e);
			}

		}
		
		BulkResponse bulkResponse = bulkRequest.get();
        logger.info(Boolean.toString(bulkResponse.hasFailures()));
	}

}
