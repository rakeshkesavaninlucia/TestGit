package org.birlasoft.thirdeye.indexer.service;

import java.util.List;

public interface IndexAdminHelper {

	void buildIndexForTenantWorkspace(String tenantURL, List<Integer> workspaceIds);

}
