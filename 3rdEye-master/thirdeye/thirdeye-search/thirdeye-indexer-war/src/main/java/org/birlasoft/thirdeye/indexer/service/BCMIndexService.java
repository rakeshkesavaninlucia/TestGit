package org.birlasoft.thirdeye.indexer.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.Workspace;

/** Service Class for BCM Indexing
 * @author dhruv.sood
 *
 */
public interface BCMIndexService {

	/**Method for BCM Indexing
	 * @author dhruv.sood
	 * @param listOfWorkspace
	 * @param tenantId
	 */
	public void buildBCMIndex(List<Workspace> listOfWorkspace, String tenantId);
}
