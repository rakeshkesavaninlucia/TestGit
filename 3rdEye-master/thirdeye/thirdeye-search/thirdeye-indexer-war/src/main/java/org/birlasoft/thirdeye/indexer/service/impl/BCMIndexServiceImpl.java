package org.birlasoft.thirdeye.indexer.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterEvaluator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.indexer.converter.BCMConverter;
import org.birlasoft.thirdeye.indexer.service.BCMIndexService;
import org.birlasoft.thirdeye.indexer.service.IndexAdministratorService;
import org.birlasoft.thirdeye.repositories.FunctionalMapDataRepository;
import org.birlasoft.thirdeye.repositories.ParameterConfigRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMAssetValueBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMLevelBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMParameterBean;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Implementation class of BCMIndexService 
 * @author dhruv.sood
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BCMIndexServiceImpl  implements BCMIndexService{

	private static Logger logger = LoggerFactory.getLogger(BCMIndexServiceImpl.class);
	
	@Autowired
	private Client client;
	
	@Autowired
	private IndexAdministratorService adminService;	

	@Autowired
	private BCMConverter bcmConverter;

	@Autowired
	private BCMService bcmService;
	
	@Autowired
	private BCMLevelService bcmLevelService;
	
	@Autowired
    private BCMReportService bCMReportService;
	
	@Autowired
	private FunctionalMapService functionalMapService;
	
	@Autowired
	private ParameterConfigRepository parameterConfigRepository;
	
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private FunctionalMapDataRepository functionalMapDataRepository;
	
	
	@Override
	public void buildBCMIndex(List<Workspace> listOfWorkspace, String tenantId) {	
		
		Set<Workspace> setOfWorkspace = new HashSet<>(listOfWorkspace);
		List<IndexBCMBean> listForBCMIndexing = new ArrayList<>();
		List<Bcm> listOfBcms = bcmService.findByWorkspaceIn(setOfWorkspace, false);
		
		logger.info("Preparing the BCM data");
		
		for (Bcm oneBCM : listOfBcms) {		
			Set<Workspace> setOfWorkspaceForOneBCM = new HashSet<>();
			setOfWorkspaceForOneBCM.add(oneBCM.getWorkspace());
		    Map<IndexBCMLevelBean, Set<IndexBCMParameterBean>> mapOfBcmLevelAndSetOfQeParam = new HashMap<>(); 
		    Map<Questionnaire, Set<Parameter>> mapOfQeAndFcParams = bCMReportService.createMapOfQeAndSetOfFcParam(setOfWorkspaceForOneBCM);
		    
		    for (Entry<Questionnaire, Set<Parameter>>  entry : mapOfQeAndFcParams.entrySet()) {
				Questionnaire questionnaire = entry.getKey();
				Set<Parameter> setOfFcParams = entry.getValue();
				// Get list of AssetBean
				for (Parameter parameter : setOfFcParams) {
					List<AssetBean> listOfAssetBeans = getListOfAssetBean(parameter);
					evaluateFcParamWithQeAssets(mapOfBcmLevelAndSetOfQeParam, questionnaire, listOfAssetBeans, parameter, oneBCM);
				}
			}
			IndexBCMBean  indexBCMBean = new IndexBCMBean(tenantId, oneBCM);
			indexBCMBean.setBcmLevels(updateListOfIndexBCMLevelBeanByMap(oneBCM, mapOfBcmLevelAndSetOfQeParam));
			
			logger.info("Creating Mapping for BCM data");
			
			//Creating the BCM Mapping for the index.BCM mapping has to be done only once
			adminService.createIndexAndMapping(tenantId, oneBCM.getBcmName().replaceAll(" ", "_"), bcmConverter.generateMappingForObject(indexBCMBean));
			listForBCMIndexing.add(indexBCMBean);
		}	
		
		//Now, the data has been prepared and mapping has been done, now push it for indexing
		if(!listForBCMIndexing.isEmpty()){
			logger.info("Inserting BCM data into index");
			insertDataIntoIndex(listForBCMIndexing);
		}
	}

	private void evaluateFcParamWithQeAssets(Map<IndexBCMLevelBean, Set<IndexBCMParameterBean>> mapOfBcmLevelAndSetOfQeParam,
			Questionnaire questionnaire, List<AssetBean> listOfAssetBeans,
			Parameter parameter, Bcm oneBCM) {
		int dataAtlevelNumber = 0;
		FunctionalCoverageParameterConfigBean fcParameterConfigBean = new FunctionalCoverageParameterConfigBean();
		List<ParameterConfig> listOfParameterConfigs = parameterConfigRepository.findByParameter(parameter);
		if(listOfParameterConfigs.size() == 1){
			String parameterConfig = listOfParameterConfigs.get(0).getParameterConfig();
			fcParameterConfigBean = Utility.convertJSONStringToObject(parameterConfig, FunctionalCoverageParameterConfigBean.class);
			List<FunctionalMapData> fmDataList = functionalMapDataRepository.findByFunctionalMapAndQuestionnaire(functionalMapService.findOne(fcParameterConfigBean.getFunctionalMapId(), false), questionnaire);
			if(!fmDataList.isEmpty())
				dataAtlevelNumber = fmDataList.get(0).getBcmLevel().getLevelNumber();
		}
		List<Parameter> listOfParameters = new ArrayList<>();			
		listOfParameters.add(parameter);
		for (int level = dataAtlevelNumber; level >= 1; level--){
			fcParameterConfigBean.setFunctionalMapLevelType(functionalMapService.getLevelNumberValue(Integer.parseInt(String.valueOf(level))));
			FunctionalCoverageParameterEvaluator fcParamEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, fcParameterConfigBean, questionnaire);
			List<BcmResponseBean> listOfBcmResponseBeans = fcParamEvaluator.evaluateBcmLevel(listOfAssetBeans, parameter);
			Set<Integer> levelIn = new HashSet<>();
			levelIn.add(level);
			List<BcmLevel> listOfBcmLevels = bcmLevelService.findByBcmAndLevelNumberIn(oneBCM, levelIn, false);
			if(listOfBcmResponseBeans.size() != listOfBcmLevels.size()){
				listOfBcmResponseBeans.addAll(createRemainingBcmResponse(listOfAssetBeans, listOfBcmResponseBeans, listOfBcmLevels));
			}
			for (BcmResponseBean bcmResponseBean : listOfBcmResponseBeans) {
				BcmLevelBean bcmLevelBean = bcmResponseBean.getBcmLevelBean();
				// Create IndexBCMLevelBean object
				IndexBCMLevelBean indexBCMLevelBean = createNewIndexBCMLevelBeanObject(bcmLevelBean);
				// Create IndexBCMParameterBean object
				IndexBCMParameterBean indexBCMParameterBean = createNewIndexBCMParameterBeanObject(questionnaire, parameter, bcmResponseBean, listOfAssetBeans);
				// Create Map of IndexBCMLevelBean as a key
				createMapOfIndexBCMLevelBeanAndSetOfIndexBCMParameterBean(mapOfBcmLevelAndSetOfQeParam, indexBCMLevelBean, indexBCMParameterBean);
			}
		}
	}

	private List<BcmResponseBean> createRemainingBcmResponse(List<AssetBean> listOfAssetBeans, List<BcmResponseBean> listOfBcmResponseBeans,
			List<BcmLevel> listOfBcmLevels) {
		Map<Integer, BcmResponseBean> mapBcmResponse = createMapForBcmResponses(listOfBcmResponseBeans);
		List<BcmResponseBean> listOfRemainingBcmResponseBeans = new ArrayList<>();
		for (BcmLevel bcmLevel : listOfBcmLevels) {
			if(bcmLevel.getId() != null && !mapBcmResponse.containsKey(bcmLevel.getId())){
				BcmResponseBean bcmResponseBean = new BcmResponseBean();
				bcmResponseBean.setBcmLevelBean(new BcmLevelBean(bcmLevel));
				for (AssetBean assetBean : listOfAssetBeans)
					bcmResponseBean.addAssetEvaluation(assetBean, new BigDecimal(-1));
				listOfRemainingBcmResponseBeans.add(bcmResponseBean);
			}
		}
		return listOfRemainingBcmResponseBeans;
	}

	private Map<Integer, BcmResponseBean> createMapForBcmResponses(List<BcmResponseBean> listOfBcmResponseBeans) {
		Map<Integer, BcmResponseBean> mapBcmResponse = new HashMap<>();
		for (BcmResponseBean bcmResponseBean : listOfBcmResponseBeans) {
			mapBcmResponse.put(bcmResponseBean.getBcmLevelBean().getId(), bcmResponseBean);
		}
		return mapBcmResponse;
	}

	private List<IndexBCMLevelBean> updateListOfIndexBCMLevelBeanByMap(Bcm oneBCM,
			Map<IndexBCMLevelBean, Set<IndexBCMParameterBean>> mapOfBcmLevelAndSetOfQeParam) {
		List<IndexBCMLevelBean> listOfIndexBCMLevelBean = getListOfIndexBCMLevelBeans(bcmLevelService.findByBcm(oneBCM, true));	
		for (IndexBCMLevelBean indexBCMLevelBean : listOfIndexBCMLevelBean) {
			if(mapOfBcmLevelAndSetOfQeParam.containsKey(indexBCMLevelBean)){
				indexBCMLevelBean.setBcmEvaluatedValue(mapOfBcmLevelAndSetOfQeParam.get(indexBCMLevelBean));
			}
		}
		return listOfIndexBCMLevelBean;
	}

	private void createMapOfIndexBCMLevelBeanAndSetOfIndexBCMParameterBean(Map<IndexBCMLevelBean, Set<IndexBCMParameterBean>> mapOfBcmLevelAndSetOfQeParam,
			IndexBCMLevelBean indexBCMLevelBean,
			IndexBCMParameterBean indexBCMParameterBean) {
		if(mapOfBcmLevelAndSetOfQeParam.containsKey(indexBCMLevelBean)){
			Set<IndexBCMParameterBean> setOfIndexBCMParameterBeans = mapOfBcmLevelAndSetOfQeParam.get(indexBCMLevelBean);
			setOfIndexBCMParameterBeans.add(indexBCMParameterBean);
			mapOfBcmLevelAndSetOfQeParam.put(indexBCMLevelBean, setOfIndexBCMParameterBeans);
		} else {
			Set<IndexBCMParameterBean> setOfIndexBCMParameterBeans =  new HashSet<>();
			setOfIndexBCMParameterBeans.add(indexBCMParameterBean);
			mapOfBcmLevelAndSetOfQeParam.put(indexBCMLevelBean, setOfIndexBCMParameterBeans);
		}
	}

	private IndexBCMParameterBean createNewIndexBCMParameterBeanObject(Questionnaire questionnaire, Parameter parameter,
			BcmResponseBean bcmResponseBean, List<AssetBean> listOfAssetBeans) {
		IndexBCMParameterBean indexBCMParameterBean = new IndexBCMParameterBean();
		indexBCMParameterBean.setParameterId(parameter.getId());
		indexBCMParameterBean.setQuestionnaireId(questionnaire.getId());
		indexBCMParameterBean.setDisplayName(parameter.getDisplayName());
		indexBCMParameterBean.setUniqueName(parameter.getUniqueName());
		indexBCMParameterBean.setType(parameter.getType());
		Set<IndexBCMAssetValueBean> setOfIndexBCMAssetValueBeans = createSetOfIndexBCMAssetValueBean(bcmResponseBean, listOfAssetBeans);
		indexBCMParameterBean.setValues(setOfIndexBCMAssetValueBeans);
		return indexBCMParameterBean;
	}

	private Set<IndexBCMAssetValueBean> createSetOfIndexBCMAssetValueBean(BcmResponseBean bcmResponseBean, List<AssetBean> listOfAssetBeans) {
		Set<IndexBCMAssetValueBean> setOfIndexBCMAssetValueBeans =  new HashSet<>();
		for (AssetBean assetBean : listOfAssetBeans) {
			// Create IndexBCMAssetValueBean object
			setOfIndexBCMAssetValueBeans.add(createNewIndexBCMAssetValueBeanObject(bcmResponseBean, assetBean));
		}
		return setOfIndexBCMAssetValueBeans;
	}

	private IndexBCMAssetValueBean createNewIndexBCMAssetValueBeanObject(BcmResponseBean bcmResponseBean, AssetBean assetBean) {
		IndexBCMAssetValueBean indexBCMAssetValueBean = new IndexBCMAssetValueBean();
		indexBCMAssetValueBean.setAssetid(assetBean.getId());
		indexBCMAssetValueBean.setShortName(assetBean.getShortName());
		BigDecimal evaluatedValue = bcmResponseBean.fetchParamValueForAsset(assetBean);
		if(evaluatedValue != null)
			indexBCMAssetValueBean.setEvaluatedPercentage(evaluatedValue.toString());
		else
			indexBCMAssetValueBean.setEvaluatedPercentage(String.valueOf(ParameterBean.NA));
		return indexBCMAssetValueBean;
	}

	private IndexBCMLevelBean createNewIndexBCMLevelBeanObject(BcmLevelBean bcmLevelBean) {
		IndexBCMLevelBean indexBCMLevelBean = new IndexBCMLevelBean();
		indexBCMLevelBean.setBcmLevelId(bcmLevelBean.getId());
		indexBCMLevelBean.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		indexBCMLevelBean.setCategory(bcmLevelBean.getCategory());
		indexBCMLevelBean.setLevelNumber(bcmLevelBean.getLevelNumber());
		if(bcmLevelBean.getParent() != null){
			indexBCMLevelBean.setParentBCMLevelId(bcmLevelBean.getParent().getId());
			indexBCMLevelBean.setParentBcmLevelName(bcmLevelBean.getParent().getBcmLevelName());
		} else if (bcmLevelBean.getSuperNodeBcmLevel() != null) {
			indexBCMLevelBean.setParentBCMLevelId(bcmLevelBean.getSuperNodeBcmLevel().getId());
			indexBCMLevelBean.setParentBcmLevelName(bcmLevelBean.getSuperNodeBcmLevel().getBcmLevelName());			
		}
		indexBCMLevelBean.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		indexBCMLevelBean.setDefaultBCMColor(Constants.DEFAULT_COLOR);
		return indexBCMLevelBean;
	}
	
	private List<AssetBean> getListOfAssetBean(Parameter parameter){
		List<AssetBean> listOfAssetBeans = new ArrayList<>();
		List<ParameterConfig> listOfParameterConfigs = parameterConfigRepository.findByParameter(parameter);
		if(listOfParameterConfigs.size() == 1){
			String parameterConfig = listOfParameterConfigs.get(0).getParameterConfig();
			FunctionalCoverageParameterConfigBean fcParameterConfigBean = Utility.convertJSONStringToObject(parameterConfig, FunctionalCoverageParameterConfigBean.class);
			FunctionalMap functionalMap = functionalMapService.findOne(fcParameterConfigBean.getFunctionalMapId(), true);
			functionalMap.getAssetTemplate().getAssets().size();
			for (Asset asset : functionalMap.getAssetTemplate().getAssets()) {
				listOfAssetBeans.add(new AssetBean(asset));
			}
		}
		return listOfAssetBeans;
	}
	
	private List<IndexBCMLevelBean> getListOfIndexBCMLevelBeans(List<BcmLevel> listOfbcmLevels) {
		List<IndexBCMLevelBean> listOfIndexBCMLevelBean = new ArrayList<>();
		for (BcmLevel oneBcmLevel : listOfbcmLevels) {
			IndexBCMLevelBean bcmLevelBean = new IndexBCMLevelBean(oneBcmLevel);
			listOfIndexBCMLevelBean.add(bcmLevelBean);
		}		
		return listOfIndexBCMLevelBean;
	}
	
	/** Method to insert tenant data into index.
	 * @author dhruv.sood
	 * @param listOfBCM
	 */
	public void insertDataIntoIndex(List<IndexBCMBean> listOfBCM) { 
		
		BulkRequestBuilder bulkRequest = client.prepareBulk();
		for (IndexBCMBean oneBCM : listOfBCM) {
			
			// Instantiate a json mapper
			ObjectMapper mapper = new ObjectMapper(); // create once, reuse
	
			// generate json
			byte[] json;
			try {
				json = mapper.writeValueAsBytes(oneBCM);
				bulkRequest.add(client.prepareIndex(oneBCM.getTenantURL().toLowerCase(),oneBCM.getBcmName().replaceAll(" ", "_"), String.valueOf(oneBCM.getId())).setSource(json));
			} catch (JsonProcessingException e) {
				logger.error("Exception in BCMIndexServiceImpl.insertDataIntoIndex()"+e);
			}

		}		
		BulkResponse bulkResponse = bulkRequest.get();
        logger.info(Boolean.toString(bulkResponse.hasFailures()));
	}
}
