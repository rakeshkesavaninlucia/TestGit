package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexAssetTags {

	/**
	 * Used to mark the parameter array associated to an asset
	 */
	PARAMETERS("parameters"),
	QUESTIONS("questions"),
	TEMPLATECOLS("templateCols"),
	COSTSTRUCTURES("costStuctures"),
	AIDS("aids"),
	RELATIONSHIP("relationship");
	
	String tagKey;
	
	IndexAssetTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
