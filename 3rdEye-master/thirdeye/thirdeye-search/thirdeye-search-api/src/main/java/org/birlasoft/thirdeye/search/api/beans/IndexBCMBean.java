package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.Bcm;

/**This is the bean class for the  bcm Levels for search.
 * @author dhruv.sood
 *
 */
public class IndexBCMBean extends AbstractProtectedEntityBean {

	private Integer bcmId;
    private String  bcmName;
    private List<IndexBCMLevelBean> bcmLevels =  new ArrayList<>();
    
      
	public IndexBCMBean(String tenantURL,Bcm bcm) {
		super(tenantURL, bcm.getWorkspace().getId());
		this.bcmId = bcm.getId();
		this.bcmName = bcm.getBcmName();
	}
	
	
	public IndexBCMBean() {
		// TODO Auto-generated constructor stub
	}


	public Integer getBcmId() {
		return bcmId;
	}
	public void setBcmId(Integer bcmId) {
		this.bcmId = bcmId;
	}
	public String getBcmName() {
		return bcmName;
	}
	public void setBcmName(String bcmName) {
		this.bcmName = bcmName;
	}
	public List<IndexBCMLevelBean> getBcmLevels() {
		return bcmLevels;
	}


	public void setBcmLevels(List<IndexBCMLevelBean> bcmLevels) {
		this.bcmLevels = bcmLevels;
	}


	
}
