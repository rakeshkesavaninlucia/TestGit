package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The core search configuration class which is used for creating the 
 * searches to the elastic search.
 * 
 * @author tej.sarup
 *
 */
public class SearchConfig {

	private String searchURL;
	private String parameterName;
	private Integer parameterId;
	private Integer questionnaireId;
	private Integer qpId;
	private List<String> tenantURLs;
	private int wsID;
	private List<Integer> listOfWorkspace;
	private List<Integer> listOfIds;
	private Integer bcmId;
	private Integer assetId;
	private Integer aidId;
	private Map<String,List<String>> filterMap = new HashMap<>();
	
	public int getWsID() {
		return wsID;
	}

	public void setWsID(int wsID) {
		this.wsID = wsID;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public Integer getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public Integer getQpId() {
		return qpId;
	}

	public void setQpId(Integer qpId) {
		this.qpId = qpId;
	}

	public String getSearchURL() {
		return searchURL;
	}

	public void setSearchURL(String searchURL) {
		this.searchURL = searchURL;
	}

	public List<String> getTenantURLs() {
		return tenantURLs;
	}

	public void setTenantURLs(List<String> tenantURLs) {
		this.tenantURLs = tenantURLs;
	}
	
	public List<Integer> getListOfWorkspace() {
		return listOfWorkspace;
	}

	public void setListOfWorkspace(List<Integer> listOfWorkspace) {
		this.listOfWorkspace = listOfWorkspace;
	}

	public List<Integer> getListOfIds() {
		return listOfIds;
	}

	public void setListOfIds(List<Integer> listOfIds) {
		this.listOfIds = listOfIds;
	}

	public SearchConfig() {
		
	}

	protected SearchConfig(String searchURL, List<String> tenantURL){
		this.searchURL = searchURL;
		this.tenantURLs = tenantURL;
	}

	/**
	 * Prepare the search configuration for a particular tenant.
	 * 
	 * @param searchURL
	 * @param tenantURL
	 */
	public SearchConfig(String searchURL, String tenantURL) {
		this.searchURL = searchURL;
		tenantURLs = new ArrayList<>();
		tenantURLs.add(tenantURL);
	}

	public Integer getBcmId() {
		return bcmId;
	}

	public void setBcmId(Integer bcmId) {
		this.bcmId = bcmId;
	}	
	
	public Integer getAssetId() {
		return assetId;
	}
	
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	
	public Integer getAidId() {
		return aidId;
	}
	
	public void setAidId(Integer aidId) {
		this.aidId = aidId;
	}
	
	public Map<String, List<String>> getFilterMap() {
		return filterMap;
	}

	public void setFilterMap(Map<String, List<String>> filterMap) {
		this.filterMap = filterMap;
	}	
}
