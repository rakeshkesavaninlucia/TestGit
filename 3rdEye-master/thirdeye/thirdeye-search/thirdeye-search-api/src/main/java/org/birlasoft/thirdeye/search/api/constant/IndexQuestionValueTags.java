package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexQuestionValueTags {

	VALUEDATE("valueDate"),
	QUESTIONNAIREID("questionnaireId"),	
	QUESTIONNAIREPARAMETERID("questionnaireParameterId"),
	VALUE("value"),
	COLOR("color"),
	COLORDESC("colorDesc");
	
	String tagKey;
	
	IndexQuestionValueTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
