package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class IndexQuestionValueTypeBean {
	
	private Integer questionnaireId;
	private Integer questionnaireParameterId;
	private String valueDate;
	private BigDecimal value;
	private String color;
	private String colorDesc;
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}	
	public Integer getQuestionnaireParameterId() {
		return questionnaireParameterId;
	}
	public void setQuestionnaireParameterId(Integer questionnaireParameterId) {
		this.questionnaireParameterId = questionnaireParameterId;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getColorDesc() {
		return colorDesc;
	}
	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}  

}
