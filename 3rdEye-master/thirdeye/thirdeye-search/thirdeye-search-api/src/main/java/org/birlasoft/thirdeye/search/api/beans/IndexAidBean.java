package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean class for Aid indexing
 * @author samar.gupta
 *
 */
public class IndexAidBean {
	
	private Integer id;
	private String name;
	private String description;
	private String aidTemplate;
	private List<IndexAidBlockBean> blocks = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAidTemplate() {
		return aidTemplate;
	}
	public void setAidTemplate(String aidTemplate) {
		this.aidTemplate = aidTemplate;
	}
	public List<IndexAidBlockBean> getBlocks() {
		return blocks;
	}
	public void setBlocks(List<IndexAidBlockBean> blocks) {
		this.blocks = blocks;
	}
}
