package org.birlasoft.thirdeye.search.api.beans;

import java.util.HashSet;
import java.util.Set;

import org.birlasoft.thirdeye.entity.BcmLevel;

/**Bean class for the  BCM Levels for indexing.
 * @author dhruv.sood
 *
 */
public class IndexBCMLevelBean {
	

	private Integer bcmLevelId;
	private String bcmLevelName;
	private Integer levelNumber;
	private Integer parentBCMLevelId;
	private String parentBcmLevelName;
	private String category;
	private String  defaultBCMColor;	
	private Integer sequenceNumber;	
	private Set<IndexBCMParameterBean> bcmEvaluatedValue =  new HashSet<>();
	
	/**
	 * @param bcmLevel
	 */
	public IndexBCMLevelBean(BcmLevel bcmLevel) {		
		this.bcmLevelId = bcmLevel.getId();
		this.bcmLevelName = bcmLevel.getBcmLevelName();
		this.levelNumber = bcmLevel.getLevelNumber();
		this.category = bcmLevel.getCategory();
		this.sequenceNumber = bcmLevel.getSequenceNumber();
		this.defaultBCMColor = "#ffffff";
		if( bcmLevel.getBcmLevel()!=null){
			this.parentBCMLevelId = bcmLevel.getBcmLevel().getId();
			this.parentBcmLevelName = bcmLevel.getBcmLevel().getBcmLevelName();
		}
	}
	 
	/**
	 * Empty constructor
	 */
	public IndexBCMLevelBean() {
		super();
	}

	public Integer getBcmLevelId() {
		return bcmLevelId;
	}

	public void setBcmLevelId(Integer bcmLevelId) {
		this.bcmLevelId = bcmLevelId;
	}

	public String getBcmLevelName() {
		return bcmLevelName;
	}

	public void setBcmLevelName(String bcmLevelName) {
		this.bcmLevelName = bcmLevelName;
	}

	public Integer getLevelNumber() {
		return levelNumber;
	}

	public void setLevelNumber(Integer levelNumber) {
		this.levelNumber = levelNumber;
	}

	public Integer getParentBCMLevelId() {
		return parentBCMLevelId;
	}

	public void setParentBCMLevelId(Integer parentBCMLevelId) {
		this.parentBCMLevelId = parentBCMLevelId;
	}

	public String getParentBcmLevelName() {
		return parentBcmLevelName;
	}

	public void setParentBcmLevelName(String parentBcmLevelName) {
		this.parentBcmLevelName = parentBcmLevelName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDefaultBCMColor() {
		return defaultBCMColor;
	}

	public void setDefaultBCMColor(String defaultBCMColor) {
		this.defaultBCMColor = defaultBCMColor;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Set<IndexBCMParameterBean> getBcmEvaluatedValue() {
		return bcmEvaluatedValue;
	}

	public void setBcmEvaluatedValue(Set<IndexBCMParameterBean> bcmEvaluatedValue) {
		this.bcmEvaluatedValue = bcmEvaluatedValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bcmLevelId == null) ? 0 : bcmLevelId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndexBCMLevelBean other = (IndexBCMLevelBean) obj;
		if (bcmLevelId == null) {
			if (other.bcmLevelId != null)
				return false;
		} else if (!bcmLevelId.equals(other.bcmLevelId))
			return false;
		return true;
	}
}
