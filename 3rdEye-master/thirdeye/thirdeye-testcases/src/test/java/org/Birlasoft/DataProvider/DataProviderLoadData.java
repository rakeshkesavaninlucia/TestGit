package org.Birlasoft.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.Birlasoft.Utility.ExcelReadWrite;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;

public class DataProviderLoadData {
	
	
	
	@DataProvider(name="dp_validLogin")
	public static Iterator<Object[]> getValidLoginData() throws IOException
	{
		
		return commomDataLoadMethod("Login","validlogin");
	}
	
	@DataProvider(name="dp_createTemplate")
	public static Iterator<Object[]> InvalidLogin() throws IOException
	{
		
		return commomDataLoadMethod("TemplateManagement","CreateTemplate");
	}
	
	
	@DataProvider(name="dp_addasset")
	public static Iterator<Object[]> addAsset() throws IOException
	{
		
		return commomDataLoadMethod("TemplateManagement","CreateTemplate");
	}
	
	
	
	@DataProvider(name="dp_Create")
	public static Iterator<Object[]> createTemplate() throws IOException
	{
		return commomDataLoadMethod("Sheet1","AddAttributeexcel");
	}
	
	

	@DataProvider(name="dp_Dashboard")
	public static Iterator<Object[]> createDashboard() throws IOException
	{
		return commomDataLoadMethod("Sheet5","Dashboard2");
	}
	
	@DataProvider(name="dp_Type")
	public static Iterator<Object[]> createConfigurtaion() throws IOException
	{
		return commomDataLoadMethod("Configuration","Relationship_2");
	}
	
	@DataProvider(name="dp_Usviewasset")
	public static Iterator<Object[]> usAssetPageviewTC1() throws IOException
	{
		return commomDataLoadMethod("ViewAssetpageUS","export");
	}
	
	
	//Common method for laoding data
	public static Iterator<Object[]> commomDataLoadMethod(String Sheetname,String sname) throws IOException
	{	
		//Reading the excelfrom given path by using userdefinded method
		//ExcelReadwrite is the user defined method to read excel . Please verify in ExcelReadwrite class
		//ExcelReadWrite xl = new ExcelReadWrite(System.getProperty("user.dir"+"\\TestData.xlsx"));//null pointer exception
		String userDir = System.getProperty("user.dir");
		String filePath = userDir+"\\TestData.xlsx";
		ExcelReadWrite xl = new ExcelReadWrite(filePath);
		
		int Rowcount=xl.getRow(Sheetname);
		int Columncount=xl.getColum(Sheetname);
		
		List<Object[]> arraylist =new ArrayList<Object[]>();
		
		for(int i=1;i<=Rowcount;i++)
		{
			//excuteflag and scriptname are column name
			String Execute_Flag=xl.readCell(Sheetname, i, 2);
			String Script_name=xl.readCell(Sheetname, i, 3);
			
			if((Execute_Flag.equalsIgnoreCase("Y"))&&(Script_name.equalsIgnoreCase(sname)))
			{
				Object[] x = new Object[1];
				Map<String,String> dMap = new HashMap<String,String>();
				
				for(int j=0;j<Columncount;j++)
				{
					String Skey  = xl.readCell(Sheetname, 0, j);
					String Value = xl.readCell(Sheetname, i, j);
					dMap.put(Skey, Value);
					
					
				}
				
				x[0]=dMap;
				arraylist.add(x);
				
			}
		}
		
		return arraylist.iterator();
	}
	
}
