package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Color_Scheme_QualityGate {
	
	public  Create_Color_Scheme_QualityGate(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
	
	
	//color_scheme
    @FindBy(xpath = "//a[@class='btn btn-primary']")
    public WebElement Create_Quality_Gate_Page_Button;
    
    @FindBy(xpath="//*[@id='name']")
  	public WebElement Quality_Gate_Name;
    
    @FindBy(xpath = "//input[@class='btn btn-primary']")
	 public WebElement Create_Quality_Gate_Page_Button1;
	

  

 public void  Create_Quality_Gate_Page_Button ()
 {
 	Util.fn_CheckVisibility_Button( Create_Quality_Gate_Page_Button);
 	
}
 
 
 public void get_Text(String Quality_Gate_Text) {
	 Quality_Gate_Name.sendKeys(Quality_Gate_Text);
 }
 
 public void  Create_Quality_Gate_Page_Button1 ()
 {
 	Util.fn_CheckVisibility_Button( Create_Quality_Gate_Page_Button1);
 	
}
 
}
	
