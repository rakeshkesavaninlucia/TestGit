package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Configuration_Relationship_Type extends Util {
	public  Configuration_Relationship_Type(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			 
		}	
	

	//Relationship_Type
	
    
   @FindBy(xpath = "//span[contains(text(),'Relationship Types')]")
   public WebElement verify_page_title;    
	
    @FindBy(xpath = "//a[contains(text(),'Create Relationship Type')]")
	public WebElement Create_Relationship_Type;
    
    @FindBy(xpath="//*[@id='workspaceId']")
	public WebElement Workspace_Dropdown;
    
//  @FindBy(xpath="//*[@id='workspaceId']/option")
//	public WebElement Workspace_Dropdown1;
	
    @FindBy(xpath="//*[@id='parentDescriptor']")
    public WebElement Parent_Descriptor;
	
	@FindBy(xpath="//*[@id='childDescriptor']")
    public WebElement Child_Descriptor;
	
    @FindBy(xpath = "//*[@id='direction']")
	public WebElement Direction_Dropdown;
	
    @FindBy(xpath="//*[@id='createRelationshipTypeForm']/div/input")
 	 public WebElement Relationship_Type_Button;
    
    @FindBy(xpath = "//a[@class='editRelationshipType fa fa-pencil-square-o']")
   	public WebElement Edit_Button;
    
    @FindBy(xpath = "//*[@id='direction']")
   	public WebElement Direction_Dropdown1;

    @FindBy(xpath = "//*[@id='relationshipTypeList_filter']/label/input")
    public WebElement Click_Search_Question;
   
    @FindBy(xpath = "//*[@id='createRelationshipTypeForm']/div/input")
   	public WebElement Edit_Relationship_Button;

  

  
  
  //calling all objects
    
    public String get_PageTitle() {
   	   
   	   String pageTitle = verify_page_title.getText();
  		
  		return pageTitle ;
  		
      }
    
    
 //To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
		try{
			String expectedTitle ="Relationship Types";
			if(expectedTitle.equals(get_PageTitle()))
			{
				System.out.println("Page Title is "+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
		}
      
    
    
    public void Create_Relationship_Type ()
    {
    	Util.fn_CheckVisibility_Button(Create_Relationship_Type);
    	
    }
  
  
	public void Workspace_Dropdown(String value) {
		Dropdownselect(Workspace_Dropdown,value);
	}
    
    
    public void get_Parent(String Descriptor) {
    	Parent_Descriptor.sendKeys(Descriptor);
    }
    
	 
    public void get_Child(String Descriptor1) {
    	Child_Descriptor.sendKeys(Descriptor1);
    }
    
    public void Direction_Dropdown(String value) {
		Dropdownselect(Direction_Dropdown,value);
	}

    
    
	public void  Relationship_Type_Button()
	    {
	    
		Util.fn_CheckVisibility_Button( Relationship_Type_Button);
	    	
	   }
		
	  public void Edit_Button ()
	    {
	    	Util.fn_CheckVisibility_Button(Edit_Button);
	    	
	    }
	  
	  //To search the question
	   	
      public void Click_Input_Search(String Search) throws InterruptedException
       {
    	   Webtable_Search(Click_Search_Question,Search);
    	   
       }
    	
      public void Direction_Dropdown1(String value) {
  		Dropdownselect(Direction_Dropdown1,value);
  	}

      
      public void Edit_Relationship_Button ()
      {
      	Util.fn_CheckVisibility_Button(Edit_Relationship_Button );
      	
      }
  
      
 
}



