package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Map;

import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Template_ViewTemplate_Pageobject;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class US_AssetPageview_AddAsset extends Util
{
	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	
	@Test(dataProvider="dp_Usviewasset",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void CreateTemplate(Map<String,String> Userstoryaddasset) throws InterruptedException, AWTException, IOException
	{	// getting all the column name from excel i.e (key)
		String Username=Userstoryaddasset.get("Username");
		String Password=Userstoryaddasset.get("Password");
		String AccountId=Userstoryaddasset.get("Tenant_ID");
		String Order_ID=Userstoryaddasset.get("Order_Set");
		String TC_ID=Userstoryaddasset.get("TC_ID");
		String AssetTemplateName=Userstoryaddasset.get("AssetTemplateName");
		String ActionColumnElementName=Userstoryaddasset.get("ActionColumnElementName");
		String ActionType=Userstoryaddasset.get("ActionType");
		String BeforeDownloadExcelname=Userstoryaddasset.get("BeforeDownloadExcelname");
		//String Template_Name=Userstoryaddasset.get("Template_Name");
		//String Description=Userstoryaddasset.get("Description");
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		//Template_CreateTemplate_PageObject createtemplatepg = new Template_CreateTemplate_PageObject(driver);
		Template_ViewTemplate_Pageobject viewpage = new Template_ViewTemplate_Pageobject(driver);
		
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		loginpage.Login_Password(Password);
		loginpage.Login_Accountid1(AccountId);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		//2.navigate to create template page
		homepage.Homepage_Configuration();
		homepage.Homepage_TemplateManagement();
		homepage.Homepage_TemplateManagement_ViewTemplates();
		Thread.sleep(2000);
		//3.creating a template
		//createtemplatepg.CreateTemplate_template_name(Template_Name);
		//createtemplatepg.CreateTemplate_template_description(Description);
		//Thread.sleep(2000);
		//createtemplatepg.CreateTemplate_Assettype_click(Asset_Type);
		//Thread.sleep(2000);
		//createtemplatepg.CreateTemplate_template_save();
		viewpage.excelBeforeDownloadFolderVerify(BeforeDownloadExcelname);
		Thread.sleep(2000);
		viewpage.viewTemplateActioncolumn(AssetTemplateName, ActionColumnElementName, ActionType);
		Thread.sleep(2000);
		
		viewpage.excelAfterDownloadFolderVerif(BeforeDownloadExcelname);
		viewpage.ImportActivity(BeforeDownloadExcelname);
		
		
	}
	
	//@AfterMethod
	public void teardown()
	{
		driver.quit();
		
		
	}

}
