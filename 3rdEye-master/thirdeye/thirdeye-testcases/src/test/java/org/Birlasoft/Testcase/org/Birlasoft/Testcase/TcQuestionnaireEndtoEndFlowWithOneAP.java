package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;

import org.Birlasoft.POM.Create_Question;
import org.Birlasoft.POM.Create_Questionnaire;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Select_Asset_Type;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Template_Inventory;
import org.Birlasoft.POM.Template_Inventory_AddAsset;
import org.Birlasoft.POM.Template_ViewTemplate_Pageobject;
import org.Birlasoft.POM.Template_Viewtemplate_Updatetemplate;
import org.Birlasoft.POM.View_Question;
import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 * @author manoj.shrivas
 *
 */


/**
 *Opening Browser and navigating to the required URL
 *
 */
public class TcQuestionnaireEndtoEndFlowWithOneAP extends Util {

	public String TempName = "Auto" + System.currentTimeMillis();
	public String QuestionName = "Auto" + System.currentTimeMillis();

	@BeforeClass(description = "navigating to URL")
	public void browserinit() throws IOException {
		// intialization of the browser
		browser();
		String URL = driver.getCurrentUrl();
		Assert.assertTrue(URL.contains("http://10.193.146.223:8080/thirdeye-web/login"));
		
	}

	
	
  @Test
  (priority = 0,description="Login to the application")
  public void LoggingIn() throws InterruptedException, TestLinkAPIException 
  {
	  String result = "";
      String exception = null;
      try
      {
    	//logging in Application
    	  Login_page_object obj1 = new Login_page_object(driver);
    	  obj1.verifyLogInPageTitle();
    	  Wait_sleep();
    	  obj1.Login_username("mann3_shri@birlasoft.com");
    	  Wait_sleep();
    	  obj1.Login_Password("welcome123#");
    	  Wait_sleep();
    	  obj1.Login_Accountid1("qadd");
    	  Wait_sleep();
    	  obj1.Login_Submit();
    	  //driver.navigate().to("http://130.1.4.252:8080/home"); 
    	  obj1.Login_ErrorMsg();
    	  Wait_sleep();

              ///tesstcase logic will come here

      result = TestLinkAPIResults.TEST_PASSED;
      accessTestLinkResult("3rdEye_TC_-1", null, result);
      } 
      catch (Exception e)
      {
           result = TestLinkAPIResults.TEST_FAILED;
           exception = e.getMessage();
           accessTestLinkResult("3rdEye_TC_-1", exception, result);
      }


  }
  
 
 /* @Test(priority =1,description="Clicking on Template management")
  public void create_template() throws InterruptedException
  {
	  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		obj2.Homepage_titleverify("Portfolio Home");
		obj2.Homepage_TemplateManagement();
		obj2.Homepage_TemplateManagement_CreateTemplate();
		//obj2.Homepage_TemplateManagement_ViewTemplates();
		
	} 
  
  
  @Test(priority = 2,description="Creating 'Application type' Asset template")
	public void Create_Asset_Template() throws InterruptedException
	{
		Template_CreateTemplate_PageObject obj3 = new Template_CreateTemplate_PageObject(driver);
		Wait_sleep();
		obj3.CreateTemplate_title_verify();
		obj3.CreateTemplate_Assettype_click("Application");
		obj3.CreateTemplate_template_name(TempName);
		System.out.println(TempName);
		obj3.CreateTemplate_template_description("Template Created through Automation on "+System.currentTimeMillis());
		obj3.CreateTemplate_template_save();
		
		
	}
  
  @Test(priority =3,description="Clicking on view template")
  public void View_template() throws InterruptedException
  {
	  Home_PageObjecet obj4 = new Home_PageObjecet(driver);
	  obj4.Homepage_TemplateManagement_ViewTemplates();
		
	}
  
  
  
  @Test(priority = 4,description="Checking created Template in datatable")
  
     public void View_Asset_Template() throws InterruptedException
	{
		Template_ViewTemplate_Pageobject obj5 = new Template_ViewTemplate_Pageobject(driver);
		Wait_sleep();
		obj5.pageetitle_verify();
		Wait_sleep();
		Wait_sleep();
		obj5.View_Template_Table_asset_verify(TempName);
		Wait_sleep();
		
		
		
	}
  
     //Checking the Update template flow
  
  @Test(priority = 5,description="Updating the created template")
	public void Update_Asset_Page() throws InterruptedException
	{
		
		Template_Viewtemplate_Updatetemplate obj6 = new Template_Viewtemplate_Updatetemplate(driver);
		obj6.pageetitle_verify();
		Wait_sleep();
		obj6.Update_template_name(TempName);
		Wait_sleep();
		obj6.Update_template_description("Asset Template Updated");
		Wait_sleep();
		obj6.Update_template_save();
		
	}
  
  /*
   * Clicking on inventory.
   * Checking in template created.
  
  
  @Test(priority =6,description="Clicking on inventory")
  public void navigate_Inventory()throws InterruptedException
  {
	  Home_PageObjecet obj7 = new Home_PageObjecet(driver);
	   obj7.Homepage_TemplateManagement_Inventory();
	   Wait_sleep();
	   obj7.Homepage_TemplateManagement_Inventorylist(TempName);
	   
		
	}*/
  
  
  /*
   * Navigating to "View Asset Template Data" page
   * 
   
  
  
  @Test(priority = 7,description="Clicking on add asset button ",dependsOnMethods={"navigate_Inventory"})
   public void Add_Asset_To_Inventory() throws InterruptedException 
  {
	   Template_Inventory obj7 = new Template_Inventory(driver);
	   obj7.Inventory_title();
	   obj7.Inventory_AddAsset_Btn_clk(); 
	   
  }*/
  
  /*
   * Adding asset properties
   * 
   
  
  
  
  @Test(priority = 8,description="Adding asset to inventory",dependsOnMethods={"Add_Asset_To_Inventory"})
  public static void Add_Asset_Page()
  {
	
	   Template_Inventory_AddAsset obj9 = new Template_Inventory_AddAsset(driver);
	   obj9.Inventory_addasset_title();
	   obj9.Inventory_addasset_textbox("John");
	   obj9.Inventory_addasset_submit_btn();
	   obj9.Inventory_addasset_textbox("Jack");
	   obj9.Inventory_addasset_submit_btn();
	   obj9.Inventory_addasset_back_btn(); 
	      
	   
	   
  }*/
  
  /*
   * Verifying the asset created
   * 
   
  
  
 @Test(priority = 9,description="verifyng asset",dependsOnMethods={"Add_Asset_Page"})
  
 public void Verfiy_Asset_Table() throws InterruptedException
  {
	   
	   Template_Inventory obj8 = new Template_Inventory(driver);
	   obj8.Inventory_title();
	   obj8.Inventory_table_Asset_check("John");
	   obj8.Inventory_table_Asset_check("jack");
	   
	   
}*/
 
 
 /*
  * Clicking on question management
  * 
  */ 
 
 
 /*@Test(priority = 11,description="Entering question management")
 public void QuestionManagement() throws InterruptedException
 {
	  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		obj2.Homepage_titleverify("Portfolio Home");
	    obj2.Homepage_QuestionManagement();
	    obj2.Homepage_QuestionManagement_CreateQuestion();
	    
		
 }*/
 
 
 /*
  * Creating questions
  * 
  */
 /*
           String txt ="It is a Text question?"+System.currentTimeMillis(); 
           String record=txt;
		   String num ="It is a Number question??"+System.currentTimeMillis();
		   String record1= num;
	       String para ="It is a paragraph question?"+System.currentTimeMillis();
	       String record2= para;
	       String date ="It is a date question?"+System.currentTimeMillis();
	       String record3= date;
	       String multi ="It is a multipltType question?"+System.currentTimeMillis();
	       String record4= multi;
 
 @Test(priority=12,description="Creating Question")
 public void Create() throws InterruptedException
 {

	 Create_Question obj4 = new Create_Question(driver);
	  for(int i=1;i<=5;i++)
	  {
	  obj4.getWrkSpc();
	  obj4.getWrkSpcSelect("AutomationWorkspace12Release");
	  obj4.getMode();
	  obj4.getMode1("MODIFIABLE");
	  obj4.getcategory();
	  obj4.getcategory1("Functional Assessment");
	  obj4.getdisName("Dis_name_"+java.time.LocalDateTime.now());
	  System.out.println("Dis_name_"+java.time.LocalDateTime.now());

	     if(i==1)
	       {
		     obj4.get_title(txt);
		     
	         obj4.getQuestion_type1("Text");
	         System.out.println("Text type question created is "+txt); 
	         obj4.complete_creation(); 
	         View_Question obj5 = new View_Question(driver);
	         obj5.Click_create();
	         }
		 else if(i==2){
		     obj4.get_title(num);
		     obj4.getQuestion_type1("Number");
	         System.out.println("Number type question created is "+num);
	         obj4.complete_creation(); 
	         View_Question obj6 = new View_Question(driver);
	         obj6.Click_create();
	         }
		 else if(i==3){
			 obj4.get_title(para);
			 obj4.getQuestion_type1("Paragraph text");
			 obj4.complete_creation(); 
			 System.out.println("Paragraph type question created is "+para);
			 View_Question obj7 = new View_Question(driver);
			 obj7.Click_create();
			 }
		 else if(i==4){
			 obj4.get_title(date);
			 obj4.getQuestion_type1("Date");
			 obj4.complete_creation();
			 System.out.println("date type question created is "+date);
			 View_Question obj8 = new View_Question(driver);
			 obj8.Click_create();
			 }
		 else if(i==5){
			 obj4.get_title(multi);
			 obj4.MCQ_Options("Three");
			 obj4.complete_creation();
			 System.out.println("Multiple choice type question created is "+multi);
			 View_Question obj8 = new View_Question(driver);
			 obj8.Click_create();

		 }
		 }
 }

	     //obj4.complete_creation(); 
	 
  
	       @Test(priority = 13,description="Entering question management and clicking on View Question")
	       public void QuestionManagementview() throws InterruptedException
	       {
	    	   Home_PageObjecet obj2 = new Home_PageObjecet(driver);
	    	   obj2.Homepage_titleverify("Portfolio Home");
	    	   obj2.Homepage_QuestionManagement();
	    	   obj2.Homepage_QuestionManagement_ViewQuestion();

	       }
 
	       @Test(priority=14,description="Searching created Question")
	       public void Check_Question_Availibility() throws InterruptedException
	       {

	    	   View_Question obj9 = new View_Question(driver);
	    	   obj9.get_PageTitle();
	    	   obj9.Search_question(record);
	    	   obj9.Search_question(record1);
	    	   obj9.Search_question(record2);
	    	   obj9.Search_question(record3);
	    	   obj9.Search_question(record4);



	       }
	       
	       
	      
	       
  
   /**
 * 
 * @AfterClass
  
  public void exitBrowser()
  {
	  
	 driver.quit(); 
	  
  }
 * 
 */

 


}
















