package org.Birlasoft.POM;

import java.util.List;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class Home_PageObjecet extends Util {
public  Home_PageObjecet(WebDriver driver1)
    {
		
		PageFactory.initElements(driver1,this);
		
	}

     //objects of Home Page

@FindBy(xpath = "//html/body/div/div/section[1]/h1/div/div/span")
public WebElement Homepage_opening_display;

@FindBy(xpath = "//span[contains(text(),'Dashboard')]")
public WebElement Homepage_Dashboard;
//span[contains(text(),'Add  a new dashboard')]

@FindBy(xpath = "//a[contains(text(),'Add a new dashboard')]")
public WebElement Homepage_Dashboard_subelement;

@FindBy(xpath = "//span[contains(text(),'User Management')]")
public WebElement Homepage_Usermanagement;

@FindBy(xpath = "//span[contains(text(),'Create User')]")
public WebElement Homepage_UserManagement_CreateUser;

@FindBy(xpath = "//span[contains(text(),'View Users')]")
public WebElement Homepage_Usermanagement_ViewUser;

@FindBy(xpath = "//span[contains(text(),'View User Roles')]")
public WebElement Homepage_Usermanagement_ViewUserRoles;

@FindBy(xpath = "//span[contains(text(),'Workspace Management')]")
public WebElement Homepage_WorkspaceManagement;

@FindBy(xpath = "//span[contains(text(),'Create Workspaces')]")
public WebElement Homepage_WorkspaceManagement_CreateWorkspaces;

@FindBy(xpath = "//span[contains(text(),'Workspace User Management')]")
public WebElement Homepage_WorkspaceManagement_WorkspaceUserManagement;


@FindBy(xpath = "//span[contains(text(),'Template Management')]")
public WebElement Homepage_TemplateManagement;

@FindBy(xpath = "//span[contains(text(),'Create Template')]")
public WebElement Homepage_TemplateManagement_CreateTemplate;

@FindBy(xpath = "//span[contains(text(),'View Templates')]")
public WebElement Homepage_TemplateManagement_ViewTemplates;

@FindBy(xpath = "//span[contains(text(),'Inventory')]")
public WebElement Homepage_TemplateManagement_Inventory;
//span[contains(text(),'treeview-menu menu-open')]
@FindBy(xpath = "//ul/li[5]/ul/li[@class='treeview active']/ul/li/a")
List <WebElement>Homepage_TemplateManagement_Inventorylist;

@FindBy(xpath = "//span[contains(text(),'Reports')]")
public WebElement Homepage_Reports;

@FindBy(xpath = "//span[contains(text(),'Asset Graphs')]")
public WebElement Homepage_Reports_AssetGraphs;

@FindBy(xpath = "//span[contains(text(),'Create Graph')]")
public WebElement Homepage_Reports_AssetGraphs_CreateGraph;

@FindBy(xpath = "//span[contains(text(),'View Graphs')]")
public WebElement Homepage_Reports_AssetGraphs_ViewGraphs;

@FindBy(xpath = "//span[contains(text(),'Scatter Graph')]")
public WebElement Homepage_Reports_ScatterGraph;

@FindBy(xpath = "//span[contains(text(),'CreateAID')]")
public WebElement Homepage_Reports_CreateAID;

@FindBy(xpath = "//span[contains(text(),'View AIDs')]")
public WebElement Homepage_Reports_ViewAIDs;

@FindBy(xpath = "//span[contains(text(),'Portfolio Health')]")
public WebElement Homepage_Reports_PortfolioHealth;

@FindBy(xpath = "//span[contains(text(),'Functional Redundancy')]")
public WebElement Homepage_Reports_FunctionalRedundancy;

@FindBy(xpath = "//span[contains(text(),'Functional BCM')]")
public WebElement Homepage_Reports_FunctionalBCM;

@FindBy(xpath = "//span[contains(text(),'Question Management')]")
public WebElement Homepage_QuestionManagement;

@FindBy(xpath = "//span[contains(text(),'Create Question')]")
public WebElement Homepage_QuestionManagement_CreateQuestion;

@FindBy(xpath = "//span[contains(text(),'View Question')]")
public WebElement Homepage_QuestionManagement_ViewQuestion;

@FindBy(xpath = "//span[contains(text(),'Parameter Management')]")
public WebElement Homepage_ParameterManagement;

@FindBy(xpath = "//span[contains(text(),'View Parameter')]")
public WebElement Homepage_ParameterManagement_ViewParameter;

@FindBy(xpath = "/html/body/div[1]/aside/section/ul/li[9]/a/span")
public WebElement Homepage_QuestionnaireManagement;

@FindBy(xpath = "//span[contains(text(),'Create Questionnaire')]") 
public WebElement Homepage_QuestionnaireManagement_CreateQuestionnaire;

@FindBy(xpath = "//span[contains(text(),'View Questionnaire')]")
public WebElement Homepage_QuestionnaireManagement_ViewQuestionnaire;

@FindBy(xpath = "//span[contains(text(),'Response Status')]")
public WebElement Homepage_QuestionnaireManagement_ResponseStatus;

@FindBy(xpath = "//span[contains(text(),'Submit Response')]")
public WebElement Homepage_QuestionnaireManagement_SubmitResponse;

@FindBy(xpath = "//span[contains(text(),'TCO Management')]")
public WebElement Homepage_TCO_Management;

@FindBy(xpath = "//span[contains(text(),'Cost Element')]")
public WebElement Homepage_TCO_Management_Cost_Element;

@FindBy(xpath = "//span[contains(text(),'Cost Structure')]")
public WebElement Homepage_TCO_Management_Cost_Structure;

@FindBy(xpath = "//ul/li[3]/a/span[contains(text(),'Chart of Account')]")
public WebElement Homepage_TCO_Management_Chart_Of_account;

@FindBy(xpath = "//span[contains(text(),'Submit Cost')]")
public WebElement Homepage_TCO_Management_Submit_Cost;

@FindBy(xpath = "//span[contains(text(),'Functional Maps')]")
public WebElement Homepage_FunctionalMaps;

@FindBy(xpath = "//a[contains(text(),'View BCM Templates')]")
public WebElement Homepage_FunctionalMaps_ViewBCMTemplates;

@FindBy(xpath = "//span[contains(text(),'Manage Workspace Capability Map')]")
public WebElement Homepage_FunctionalMaps_ManageWBCM;

@FindBy(xpath = "//span[contains(text(),'View Workspace BCM')]")
public WebElement Homepage_FunctionalMaps_viewWBCM;

@FindBy(xpath = "//ul/li[10]/ul/li[4]/a")
public WebElement Homepage_FunctionalMaps_Functionalmap;

@FindBy(xpath = "//span[contains(text(),'Configuration')]")
public WebElement Homepage_Configuration;

@FindBy(xpath = "//li/a[contains(text(),'Relationship Types')]")
public WebElement Homepage_Configuration_RelationshipTypes;

@FindBy(xpath = "//span[contains(text(),'Color Scheme')]")
public WebElement Homepage_ColorScheme;

@FindBy(xpath = "//li/a[contains(text(),'Quality Gates')]")
public WebElement Homepage_ColorScheme_QualityGates;

//@FindBy(xpath = "/html/body/div/aside[1]/section/ul/li[13]/ul/li[1]/a")
//public WebElement Homepage_ColorScheme_QualityGates;

@FindBy(xpath = "//span[contains(text(),'Index Creation')]")
public WebElement Homepage_IndexCreation;

@FindBy(xpath = "//span[contains(text(),'Create Index')]")
public WebElement Homepage_IndexCreation_CreateIndex;

@FindBy(xpath="//span[contains(text(),'Benchmark')]")
public WebElement Homepage_BenchMark;

@FindBy(xpath = "//li/a[contains(text(),'Benchmark')]")
public WebElement Homepage_Benchmark_ViewBenchMark;


//calling all objects

public void Homepage_titleverify(String title)
{
	
	Homepage_opening_display.getText().contains(title);
}


public void Homepage_Dashboard()
{
	Homepage_Dashboard.click();
}

public void Homepage_Dashboard_subelement()
{
	Homepage_Dashboard_subelement.click();
}

public void Homepage_Usermanagement()
{
	Homepage_Usermanagement.click();
}

public void Homepage_UserManagement_CreateUser()
 {
	 Homepage_UserManagement_CreateUser.click();
 }


public void Homepage_Usermanagement_ViewUser()
{
	Homepage_Usermanagement_ViewUser.click();
}

public void Homepage_Usermanagement_ViewUserRoles()
{
	Homepage_Usermanagement_ViewUserRoles.click();
}

public void Homepage_WorkspaceManagement()
{
	Homepage_WorkspaceManagement.click();
}

public void Homepage_WorkspaceManagement_CreateWorkspaces()
{
	Homepage_WorkspaceManagement_CreateWorkspaces.click();
}

public void Homepage_WorkspaceManagement_WorkspaceUserManagement()
{
	Homepage_WorkspaceManagement_WorkspaceUserManagement.click();
}

public void Homepage_TemplateManagement()
{
	Util.fn_CheckVisibility_Button(Homepage_TemplateManagement);
}

public void Homepage_TemplateManagement_CreateTemplate()
{
	Util.fn_CheckVisibility_Button(Homepage_TemplateManagement_CreateTemplate);
}

public void Homepage_TemplateManagement_ViewTemplates()
{
	Util.fn_CheckVisibility_Button(Homepage_TemplateManagement_ViewTemplates);
}

public void Homepage_TemplateManagement_Inventory()
{
	Util.fn_CheckVisibility_Button(Homepage_TemplateManagement_Inventory);
}

public void Homepage_TemplateManagement_Inventorylist(String assetname)
{
	
	
	int listcount=Homepage_TemplateManagement_Inventorylist.size();
	 for(int list=0;list<listcount;list++)
	 {
		 System.out.println("Number of List "+listcount);
		try
		{
		 String listtext=Homepage_TemplateManagement_Inventorylist.get(list).getText();
		 if(listtext.equals(assetname))
		  {
			 
			 Homepage_TemplateManagement_Inventorylist.get(list).click(); 
			 Thread.sleep(2000);
			 System.out.println("Value found"+listtext);
			 //Highlighter.highLightElement(driver,click);
		  }
		}catch(Exception e)
		  {
   		      e.printStackTrace();
			  System.out.println("value not found in this li");
			  throw new AssertionError("A clear description of the failure", e);
		  }
	 }
}

public void Homepage_Reports()
{
	Homepage_Reports.click();
}


public void Homepage_Reports_AssetGraphs()
{
	Homepage_Reports_AssetGraphs.click();
}

public void Homepage_Reports_AssetGraphs_CreateGraph()
{
	Homepage_Reports_AssetGraphs_CreateGraph.click();
}

public void Homepage_Reports_AssetGraphs_ViewGraphs()
{
	Homepage_Reports_AssetGraphs_ViewGraphs.click();
}

public void Homepage_Reports_ScatterGraph()
{
	Homepage_Reports_ScatterGraph.click();
}

public void Homepage_Reports_CreateAID()
{
	Homepage_Reports_CreateAID.click();
}


public void Homepage_Reports_ViewAIDs()
{
	Homepage_Reports_ViewAIDs.click();
}

public void Homepage_Reports_PortfolioHealth()
{
	Homepage_Reports_PortfolioHealth.click();
}



public void Homepage_Reports_FunctionalRedundancy()
{
	Homepage_Reports_FunctionalRedundancy.click();
}


public void Homepage_Reports_FunctionalBCM()
{
	Homepage_Reports_FunctionalBCM.click();
}


public void Homepage_QuestionManagement()
{
	if(Homepage_QuestionManagement.isDisplayed())
		//click on question
		click_Method(Homepage_QuestionManagement);
	
}


public void Homepage_QuestionManagement_CreateQuestion()
{
	
		//click on create question
		click_Method(Homepage_QuestionManagement_CreateQuestion);
}


public void Homepage_QuestionManagement_ViewQuestion()
{
	click_Method(Homepage_QuestionManagement_ViewQuestion);
}

public void Homepage_ParameterManagement()
{

	try
	{
		
		click_Method(Homepage_ParameterManagement);
		System.out.println("user navigate to parameter management page");
		
	}catch(Exception e)
	{

	    throw new AssertionError("A clear description of the failure", e);
	}
	
}

public void Homepage_parameterManagement_ViewParameter()
{
	try{
		
		click_Method(Homepage_ParameterManagement_ViewParameter);
		System.out.println("User navigate to view parameter page");
		
		
	    }catch(Exception e)
	    {

	      throw new AssertionError("A clear description of the failure", e);
		}
}


public void Homepage_QuestionnaireManagement()
{
	//click_Method(Homepage_QuestionnaireManagement);
	
	if(Homepage_QuestionnaireManagement.isDisplayed()){
		//click on questionnaire
		click_Method(Homepage_QuestionnaireManagement);
		System.out.println("click questionnaire");
	}
}

//public void Homepage_QuestionnaireManagement_CreateQuestionnaire()
//{
//	//click_Method(Homepage_QuestionnaireManagement_CreateQuestionnaire);
//	if(Homepage_QuestionnaireManagement_CreateQuestionnaire.isDisplayed()){
//		//click on questionnaire
//		click_Method(Homepage_QuestionnaireManagement_CreateQuestionnaire);
//		System.out.println("click on questionnaire");
//	}
//}

public void Homepage_QuestionnaireManagement_ViewQuestionnaire()
{
	click_Method(Homepage_QuestionnaireManagement_ViewQuestionnaire);
}

public void Homepage_QuestionnaireManagement_CreateQuestionnaire()
{
	click_Method(Homepage_QuestionnaireManagement_CreateQuestionnaire);
}

public void Homepage_QuestionnaireManagement_ResponseStatus()
{
	click_Method(Homepage_QuestionnaireManagement_ResponseStatus);
}


public void Homepage_QuestionnaireManagement_SubmitResponse()
{
	click_Method(Homepage_QuestionnaireManagement_SubmitResponse);
}

public void Homepage_TCOManagement()
{
	//click_Method(Homepage_QuestionnaireManagement);
	
	if(Homepage_TCO_Management.isDisplayed()){
		//click on questionnaire
		click_Method(Homepage_TCO_Management);
	}
}


public void Homepage_TCOManagement_CostElement()
{
	click_Method(Homepage_TCO_Management_Cost_Element);
	
}

public void Homepage_TCOManagement_CostStructure()
{
	click_Method(Homepage_TCO_Management_Cost_Structure);
}

public void Homepage_TCOManagement_ChartOfAccount()
{
	click_Method(Homepage_TCO_Management_Chart_Of_account);
}


public void Homepage_TCOManagement_SubmitCost()
{
	click_Method(Homepage_TCO_Management_Submit_Cost);
}



public void Homepage_FunctionalMaps()
{
	try
	{
		click_Method(Homepage_FunctionalMaps);
		System.out.println("User click on Functional Map");
	}
	catch(Exception e)
    {

	      throw new AssertionError("A clear description of the failure", e);
	}
}
public void Homepage_FunctionalMaps_ViewBCMTemplates()
{
	try
	{
		click_Method(Homepage_FunctionalMaps_ViewBCMTemplates);
		System.out.println("User navigate to ViewBCMTemplate");
		
	 }catch(Exception e)
	  {

	      throw new AssertionError("A clear description of the failure", e);
	 }
}

public void Homepage_FunctionalMaps_ManageWBCM()
{
	try
	{
		click_Method(Homepage_FunctionalMaps_ManageWBCM);
		System.out.println("User navigate to Workspace BCM ");
	}
	catch(Exception e)
	{
		throw new AssertionError("A clear description of the failure", e);
	}
}

public void Homepage_FunctionalMaps_viewWBCM()
{
	try
	{
		click_Method(Homepage_FunctionalMaps_viewWBCM);
		System.out.println("User navigate to Workspace View BCM ");
	}
	catch(Exception e)
	{
		throw new AssertionError("A clear description of the failure", e);
	}
}


public void Homepage_FunctionalMaps_Functionalmap() 
{
	try
	{
		click_Method(Homepage_FunctionalMaps_Functionalmap);
		System.out.println("User navigate to Fucntional Map[ ");
	}
	catch(Exception e)
	{
		throw new AssertionError("A clear description of the failure", e);
	}
}

public void Homepage_Configuration()
{
	Homepage_Configuration.click();
}


public void Homepage_Configuration_RelationshipTypes()
{
	Homepage_Configuration_RelationshipTypes.click();
}


public void Homepage_ColorScheme()
{
	Homepage_ColorScheme.click();
}


public void Homepage_ColorScheme_QualityGates()
{
	Homepage_ColorScheme_QualityGates.click();
}

public void Homepage_IndexCreation()
{
	Homepage_IndexCreation.click();
}

public void Homepage_IndexCreation_CreateIndex()
{
	Homepage_IndexCreation_CreateIndex.click();
}

public void Homepage_Workspace_change(String Change_Workspacename)
{
	try
	{
	
		Thread.sleep(1000);
		Change_Workspace(Change_Workspacename);
	}
	catch(Exception e)
	{
		throw new AssertionError("A clear description of the failure", e);
	}
	
}

public void HomePage_BenchMark()
{
	Util.fn_CheckVisibility_Button(Homepage_BenchMark);
}

public void HomePage_BenchMark_View()
{
	Util.fn_CheckVisibility_Button(Homepage_Benchmark_ViewBenchMark);
}



}
