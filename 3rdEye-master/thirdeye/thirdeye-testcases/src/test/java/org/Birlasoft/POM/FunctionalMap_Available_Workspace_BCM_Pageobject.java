package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FunctionalMap_Available_Workspace_BCM_Pageobject 
{

	public FunctionalMap_Available_Workspace_BCM_Pageobject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	//Workspace BCM Available template page
	@FindBy(xpath = "//span[contains(text(),'Available Workspace BCM Templates')]")
	public WebElement Available_Workspace_BCM_Template_Pagetitle; 
	
	
	//Search Box in the available template page
	@FindBy(xpath = "//div[@id='listOfWorkspaceBCM_filter']/label/input[@type='search']")
	public WebElement Available_Workspace_BCM_Template_SearchBox;
	
	
	//Edit button for the corresponidng workspace BCM
	@FindBy(xpath = "//table[@id='listOfWorkspaceBCM']/tbody/tr[1]/td[4]/a")
	public WebElement Available_Workspace_BCM_Template_Action_Edit;
	
		
	//Workspace BCM page link btn
	@FindBy(xpath = "//a[contains(text(),'Manage Workspace Capability Map')]")
	public WebElement Available_Workspace_BCM_Template_Manage_Worksapace_Map_Btn;
	
	
	//Workspace BCM table
	@FindBy(id = "listOfWorkspaceBCM")
	public WebElement Available_Workspace_BCM_Template_Table_Verify;
	
	
	//workspace BCM textbox 
	@FindBy(xpath = "//div[@class='form-group']/input[@type='text' and @id='bcmName']")
	public WebElement Available_Workspace_Popup_WorkspaceBCM_Template_Name_Txtbox;
	
	
	//select the BCM template from the dropdown
	@FindBy(xpath = "//select[@name='bcmTemplate']")
	public WebElement Available_Workspace_Popup_Dropdown_BCM_Template_Name;
	
	
	//Create WorkspaceBCM Btn
	@FindBy(xpath = "//input[@type='submit' and @value='Create Workspace BCM']")
	public WebElement Available_Workspace_BCM_Template_Name_SubmitBtn;
	

	//Workspace bCM popup title 
	@FindBy(xpath = "//h4[@class='modal-title' and  contains(text(),'Manage Workspace Capability Map')]")
	public WebElement Available_Workspace_BCM_Template_Popup_Title_verify;
	
	
	public String get_pagetitle()
	{
		String pageTitle = Available_Workspace_BCM_Template_Pagetitle.getText();
		
		return pageTitle ;
			
	}
	
	//To verify whether the user is landing on the proper page or not
	public void Available_Workspace_BCM_Template_Pagetitle_verify()
	{
			try{
				String expectedTitle = "Available Workspace BCM Templates";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	//click on "ManageWorkspace BCM" Btn
	public void Available_Workspace_BCM_Template_Create_Workspace_BCM_Btn()
	{
		Util.fn_CheckVisibility_Button(Available_Workspace_BCM_Template_Manage_Worksapace_Map_Btn);	
		
	}
	
	//Verify the Created Workspace BCM name on the table
	public void Available_Workspace_BCM_Template_Webtable_Verify(String Searchname) throws InterruptedException
	{
		Util.Webtable_Search(Available_Workspace_BCM_Template_SearchBox,Searchname);
		Util.Webtable_Element_check(Available_Workspace_BCM_Template_Table_Verify, Searchname);
		
	}
	
	//Verify and click on the Edit Icon
	public void Available_Workspace_BCM_Template_Edit_BCM(String BCM_Template_Searchname ) throws InterruptedException
	{
		Util.Webtable_Search(Available_Workspace_BCM_Template_SearchBox,BCM_Template_Searchname);
		Util.fn_CheckVisibility_Button(Available_Workspace_BCM_Template_Action_Edit);
	}
	
	//verify whether the user able to create workspace BCM or not
	public void Available_Workspace_BCM_Template_Popup_Create_BCM(String Workspace_BCM_Name,String BCM_Name ) throws InterruptedException
	
	{
		Thread.sleep(1000);
		Util. Popup_title_verify(Available_Workspace_BCM_Template_Popup_Title_verify,"Create BCM Template");
		Util.Popup_Set_text(Available_Workspace_Popup_WorkspaceBCM_Template_Name_Txtbox,Workspace_BCM_Name);
		Util.Dropdown_Text(Available_Workspace_Popup_Dropdown_BCM_Template_Name, BCM_Name);
		// try with this method , if it s not working plz start with modal popup
		Util.fn_CheckVisibility_Button(Available_Workspace_BCM_Template_Name_SubmitBtn);
		//Util.Popup_Error_verify();
	}
	
	
	
	
	
	
}

