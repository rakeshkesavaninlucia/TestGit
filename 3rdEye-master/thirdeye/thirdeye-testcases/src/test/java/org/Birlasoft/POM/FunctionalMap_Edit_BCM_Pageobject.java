package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FunctionalMap_Edit_BCM_Pageobject 
{
	
	public FunctionalMap_Edit_BCM_Pageobject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath = "//h1/div/span[contains(text(),'Edit Business Capability Map')]")
	public WebElement Edit_BCM_Pagetitle;
	
	@FindBy(xpath = "//input[@type='text' and @value='BCM_without_value chain']")
	public WebElement Edit_BCM_Textbox;
	
	@FindBy(id = "AddL0")
	public WebElement Edit_BCM_AddValuechain_Btn;
	
	@FindBy(xpath = "//a[@href = '/bcm/list' and contains(text(),'Back')]")
	public WebElement Edit_BCM_Back_Btn;
	
	@FindBy(xpath = "//h4[@id='exampleModalLabel']/span[ contains(text(),'Add Value Chain')]")
	public WebElement Edit_BCM_AddvalueChain_pagetitle;
	
	@FindBy(id = "level-name")
	public WebElement Edit_BCM_All_Level_popup_txtbx;
	
	@FindBy(xpath = "//input[@type='submit' and @value='Create Level']")
	public WebElement Edit_BCM_All_Level_popup_CreateLvlBtn;
	
	@FindBy(xpath = "//ul/li/span/i[@title='Add Business Function']")
	public WebElement Edit_BCM_L1_AddBussiness_Function_Icon;
	
	@FindBy(xpath = "//h4[@id='exampleModalLabel']/span[contains(text(),'Add Business Function')]")
	public WebElement Edit_BCM_AddBussiness_Function_popup_title;
	
	@FindBy(id = "category-name")
	public WebElement Edit_BCM_AddBussiness_Function_popup_category_txtbox;
	
	@FindBy(xpath = "//ul/li/span/i[@title='Add Business Process')]")
	public WebElement Edit_BCM_L2_AddBusiness_Process_Icon;
	
	@FindBy(xpath = "//h4[@id='exampleModalLabel']/span[contains(text(),'Add Business Process')]")
	public WebElement Edit_BCM_AddBussiness_Process_popup_title;
	
	@FindBy(xpath = "//ul/li/span/i[@title='Add Activity')]")
	public WebElement Edit_BCM_L3_Add_Activity_Icon;
	
	@FindBy(xpath = "//h4[@id='exampleModalLabel']/span[contains(text(),'Add Activity')]")
	public WebElement Edit_BCM_Add_Activity_popup_title;
	
	@FindBy(xpath = "//ul/li/span/i[@title='Delete')]")
	public WebElement Edit_BCM_All_Delete_Icon;
	
	@FindBy(xpath = "//ul/li/span/i[@title='Edit Name')]")
	public WebElement Edit_BCM_All_Edit_Icon;
	
	
	public String get_pagetitle()
	{
		String pageTitle = Edit_BCM_Pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void Edit_BCM_Pagetitle_verify()
	{
			try{
				String expectedTitle = "Edit Business Capability Map";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	public void Edit_BCM_L1_AddValue_chain_Creation(String Level1_name) throws InterruptedException
	{
		Util.fn_CheckVisibility_Button(Edit_BCM_AddValuechain_Btn);
		//need to verify the pop up Value chain popup title
		
		Util.Popup_title_verify(Edit_BCM_AddvalueChain_pagetitle,"Add Value Chain");
		
		Util.Popup_Set_text(Edit_BCM_All_Level_popup_txtbx, Level1_name);
		
		Util.fn_CheckVisibility_Button(Edit_BCM_All_Level_popup_CreateLvlBtn);
	}
	
	
	public void Edit_BCM_L2_Business_Function (String Level2_name,String Category_name) throws InterruptedException
	{
		
		Util.fn_CheckVisibility_Button(Edit_BCM_L1_AddBussiness_Function_Icon);
		
		Util.Popup_title_verify(Edit_BCM_AddBussiness_Function_popup_title,"Add Business Function");
		
		Util.Popup_Set_text(Edit_BCM_All_Level_popup_txtbx, Level2_name);
		
		Util.Popup_Set_text(Edit_BCM_AddBussiness_Function_popup_category_txtbox, Category_name);
		
		Util.fn_CheckVisibility_Button(Edit_BCM_All_Level_popup_CreateLvlBtn);
		
		
		
		
		
	}
	
	
	public void Edit_BCM_L3_Bussiness_Process(String Level3_Name) throws InterruptedException
	{
		Util.fn_CheckVisibility_Button(Edit_BCM_L2_AddBusiness_Process_Icon);
		
		Util.Popup_title_verify(Edit_BCM_AddBussiness_Process_popup_title,"Add Business Process");
		
		Util.Popup_Set_text(Edit_BCM_All_Level_popup_txtbx,Level3_Name);
		
		Util.fn_CheckVisibility_Button(Edit_BCM_All_Level_popup_CreateLvlBtn);
		
		
	}
	
	
	public void Edit_BCM_L4_Business_Activity(String Level4_name) throws InterruptedException
	{
		Util.fn_CheckVisibility_Button(Edit_BCM_L3_Add_Activity_Icon);
		
		Util.Popup_title_verify(Edit_BCM_Add_Activity_popup_title,"Add Activity");
		
		Util.Popup_Set_text(Edit_BCM_All_Level_popup_txtbx,Level4_name);
		
		Util.fn_CheckVisibility_Button(Edit_BCM_All_Level_popup_CreateLvlBtn);
	}
	
	
	
	
	

}

