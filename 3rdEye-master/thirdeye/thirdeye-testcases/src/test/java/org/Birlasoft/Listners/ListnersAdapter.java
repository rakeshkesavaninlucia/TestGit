package org.Birlasoft.Listners;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ListnersAdapter extends TestListenerAdapter{
	
	public void onTestStart(ITestResult result){
		
		System.out.println("Testcase has started and Testcase details are: "+result.getName());
		
	}

   public void onTestSuccess(ITestResult result){
		
		System.out.println("Testcase "+ result.getName()+ "has successfully ran .");
		
	}
	
   public void onTestSkipped(ITestResult result){
		
		System.out.println("Testcase "+ result.getName()+ "has been skipped.");
		
	}
   
   public void onTestFailure(ITestResult result){
		
		System.out.println("Testcase "+ result.getName()+ "is failed.");
		
	}
   
   public void onTestFailedButWithinSuccessPercentage(ITestResult result){
		
		System.out.println("Testcase "+ result.getName()+ "has failed but it is under defined success rate.");
		
	}
   
   
   
	
	
}
