package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;

import org.Birlasoft.POM.Dashboard_Add_New_Dashboard;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



/**
* @author meghna.agarwal
*
*/
public class TC_Dashboard  extends Util{
	
	@BeforeClass
	public void browserinit() throws IOException
	{
		//Initialization of the browser
		Util.browser();
		
	}
	
	 @Test(priority = 0, description="Login to the application")
	    public void f() throws InterruptedException  {
	   

			//login page
			
			Login_page_object objq = new Login_page_object(driver);
			objq.verifyLogInPageTitle();
			Util.Wait_sleep();
			objq.Login_username("meghna.agarwal@birlasoft.com");
			Util.Wait_sleep();
			objq.Login_Password("juhi@08");
			Util.Wait_sleep();
			objq.Login_Accountid1("qadd"); 
			Util.Wait_sleep();
			objq.Login_Submit();
	        objq.Login_ErrorMsg();
	        Util.Wait_sleep();			
			
		     }	
	 
	    //going to portfolio home page
	    @Test(priority = 1, description="Clicking on Dashboard")
	    public void Naviagate_To_Parameter()
		{
			 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			 obj2.Homepage_titleverify("Portfolio Home");
			 /***
		       * Validating the availability of the functionality Portfolio Home
		       *
		       * ***/
		           
			 obj2.Homepage_Dashboard();
			 obj2.Homepage_Dashboard_subelement();
		}
	  
	    
	    

	    //creating dashboard
	    @Test(priority = 2, description="navigating through Dashboard")
	    public void Naviagate_To_Dashboard() throws InterruptedException
		{
	    	
	    	Dashboard_Add_New_Dashboard obj3 = new Dashboard_Add_New_Dashboard(driver);
	    	 obj3.Dashboard_Name();
	    	 Util.Wait_sleep();			
             obj3.get_Text("Dashboard1");
             Util.Wait_sleep();			
			 obj3.Save_Button();
			 Util.Wait_sleep();			
			 obj3.Dropdown();
			 Util.Wait_sleep();			
		 	 obj3.Dropdown1();
		 	 Util.Wait_sleep();			
			 obj3.Add_widget_click();
			 Util.Wait_sleep();				    
			 obj3.get_name("Create Graph");
			 Util.Wait_sleep();	
			 obj3.Widget_popup_Raidobutton();
			 Util.Wait_sleep();			
//			 obj3.Edit_widget_click();
//			 Thread.sleep(2000);
//			 obj3.Delete_widget_click();
//			 Thread.sleep(2000);
			

    //adding widget	 
			 obj3.Add_Widget_Button1();
			 Util.Wait_sleep();			
			 obj3.Setting_Button();
			 Util.Wait_sleep();			
			 Util.Wait_sleep();			
		     obj3.Select_Questionnaire_Dropdown("QuestHunt");
		     Util.Wait_sleep();			
		     obj3.Select_Question_Dropdown("Is the application Custom Built or Packaged?");
		     Util.Wait_sleep();			
		     obj3.Select_Parameter_Dropdown("Drill1");
		     Util.Wait_sleep();			
		     obj3.finalmethod();
		     Util.Wait_sleep();			
			 obj3.Plot_Chart_Button();
			 Util.Wait_sleep();			
		}
	    
	
}
