package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Update_Questionnaire extends Util{
	public  Update_Questionnaire(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       @FindBy(xpath = "//div[@class='box-footer']/input")
		     public WebElement ClickNext;
	       
	       
	       
	       
	        // @FindBy(id = "questionList")
	        // public WebElement ViewQuestion_table;
	       
	      public void ClickOn_Done()
	       {
	    	  click_Method(ClickNext);
           }
	      
	     
}
