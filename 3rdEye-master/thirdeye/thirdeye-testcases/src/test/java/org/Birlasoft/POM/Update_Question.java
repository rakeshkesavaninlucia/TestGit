package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Update_Question extends Util{

	public  Update_Question(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       @FindBy(xpath = "//*[@id='mod-module-createQuestion-1']/form/div/input")
		     public WebElement ClickDone;
	       
	       
	       
	       
	        // @FindBy(id = "questionList")
	        // public WebElement ViewQuestion_table;
	       
	      public void ClickOn_Done()
	       {
	    	   click_Method(ClickDone);
           }
	      
	     
	
}
