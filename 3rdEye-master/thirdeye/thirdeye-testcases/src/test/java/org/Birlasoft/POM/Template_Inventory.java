package org.Birlasoft.POM;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class Template_Inventory extends Util {

	public Template_Inventory(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath = "//span[contains(text(),'View Asset Template Data')]")
	public WebElement Inventory_title;
	
	@FindBy(xpath = "//h1/small/div/span")
	public WebElement Inventory_Page_inventory_name;
	
	@FindBy(id = "assetList")
	public WebElement Inventory_Table;
	
	
	@FindBy(xpath = "//input[@type = 'search']")
	public WebElement Inventory_Search;
	
	@FindBy(xpath = "//a[contains(text(),'Add Asset')]")
	public WebElement Inventory_AddAsset_Btn_clk;
	
	//get the invnetory title
	public String get_inventory_title()
	{
        String pageTitle = Inventory_title.getText();
		
		return pageTitle ;
	}
	
	
	public void Inventory_title()
	{
		try{
			String expectedTitle = "View Asset Template Data";
			if(expectedTitle.equals(get_inventory_title()))
			{
				System.out.println("Page Title is"+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
	}
	
	//to get the inventory name
	public String get_invnetory_name()
	{
        String pageTitle = Inventory_Page_inventory_name.getText();
		
		return pageTitle ;
		
		
	}
	
	public void Inventory_Page_inventory_name(String Inventory_Pass_name)
	{
		try{
			String expectedTitle = Inventory_Pass_name;
			if(expectedTitle.equals(get_invnetory_name()))
			{
				System.out.println("Page Title is"+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
	}
	
	//To verify whether the created asset is present on the table or not
	public void Inventory_table_Asset_check(String assetname_created) throws InterruptedException
	
	{    
		Webtable_Search(Inventory_Search,assetname_created);
		Webtable_Element_check(Inventory_Table,assetname_created);
	}
	
	
	//To click on the create asset in the table
	public void Inventory_table_Asset_click(String assetname_created) throws InterruptedException
	{
		Webtable_Search(Inventory_Search,assetname_created);
		Webtable_element_click(Inventory_Table,assetname_created);
		
		
	}
	
	//To click on the add asset button to create asset for corresponding template
	public void Inventory_AddAsset_Btn_clk()
	{
		System.out.println("adding asset to the Inventory" +get_invnetory_name());
		click_Method(Inventory_AddAsset_Btn_clk);
	}
	
	
	
	
	/*********/
	
	/*
	 * Inventory  Page object Model
	 *
	*/
	
	
	
}
	

