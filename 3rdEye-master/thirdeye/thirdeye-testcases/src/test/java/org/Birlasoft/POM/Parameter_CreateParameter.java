package org.Birlasoft.POM;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Parameter_CreateParameter extends Util {

	
	public Parameter_CreateParameter(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
		
	}
	
	@FindBy(xpath = "//span[contains(text(),'Available Parameter')]")
	public WebElement Create_Parameter_Pageverify;
	
	@FindBy(xpath = "//a[contains(text(),'Create Parameter')]")
	public WebElement Create_Parameter_Btn;
	
	@FindBy(xpath = "//input[@type='search']")
	public WebElement Create_Parameter_search;
	
	@FindBy(xpath = "//tr[1]/td[5]/a[@title='Edit']")
	public WebElement Create_Parameter_pencilicon;
	
	@FindBy(xpath = "//tr[1]/td[5]/a[@title='Create Variant']")
	public WebElement Create_Parameter_createvariant;
	
	@FindBy(xpath = "//tr[1]/td[5]/a[@title='Tree View']")
	public WebElement Create_Parameter_Treeview;
	
	@FindBy(xpath = "//button[contains(text(),'Close')]")
	public WebElement Create_Parameter_Treeview_close;
	
	@FindBy(xpath = "//tr[1]/td[5]/a[@title='Delete']")
	public WebElement Create_Parameter_Delete;
	
	@FindBy(id ="DataTables_Table_0")
	public WebElement Create_Parameter_Table_asset_verify;
	
	public String get_pagetitle()
	{
		String pageTitle = Create_Parameter_Pageverify.getText();
		
		return pageTitle ;
		
		
	}
	
	
	//To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
			try{
				String expectedTitle = "Available Parameter";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	//To verify whether the parameter is present on the table or not
	public void Create_Parameter_Table_parameter_verify(String Templatename) throws InterruptedException
	{
		Webtable_Search(Create_Parameter_search,Templatename);
		Webtable_Element_check( Create_Parameter_Table_asset_verify,Templatename );
	}
	
	//click on the update/edit icon to update the template name
	public void Create_Parameter_Update_icon(String Templatename) throws InterruptedException
	{
		Webtable_Search(Create_Parameter_search,Templatename);
		click_Method(Create_Parameter_pencilicon);
	}
	
	//click on the update/edit icon to update the template name
	public void Create_Parameter_Variant_icon(String Templatename) throws InterruptedException
	{
		Webtable_Search(Create_Parameter_search,Templatename);
		click_Method(Create_Parameter_createvariant);
	}
	
	//click on the update/edit icon to update the template name
	public void Create_Parameter_Treeview_icon(String Templatename) throws InterruptedException
	{
		Webtable_Search(Create_Parameter_search,Templatename);
		click_Method(Create_Parameter_Treeview);
		Popup_table_Click(Create_Parameter_Treeview_close);
	}
	
	
	//click on the update/edit icon to update the template name
	public void Create_Parameter_Delete_icon(String Templatename) throws InterruptedException
	{
		Webtable_Search(Create_Parameter_search,Templatename);
		Popup_Delete_Alert(Create_Parameter_Delete);
		
		
	}
	
	public void Create_Parameter_CrtParamBtn()

	{
		if(Create_Parameter_Btn.isDisplayed())
			//calling click method
		click_Method(Create_Parameter_Btn);
		System.out.println("User click on Create Parameter button");
	}
	
	
	
	
}

