package org.Birlasoft.Utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Util 
{

	public static WebDriver driver;
	
	public static WebDriver browser()

	{
		System.out.println("Chrome browser");		
			//to handle the developer mode extension popup
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("incognito");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			Map<String, Object> prefs = new HashMap<String, Object>();
			options.addArguments("chrome.switches","--disable-extensions");
			options.addArguments("disable-infobars");
			prefs.put("credentials_enable_service", false);
			options.setExperimentalOption("prefs", prefs);
            System.setProperty("webdriver.chrome.driver","C://Working Folder//software//chromedriver_win32//chromedriver.exe");
			driver = new ChromeDriver(options);
			driver.navigate().to("http://10.193.146.223:8080/thirdeye-web/login");
			driver.manage().window().maximize();
			return driver;
		}

		
	      



	public void elementHighlight(WebElement element) throws InterruptedException 
	{
		for (int i = 0; i < 3; i++) 
		{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: red; border: 3px solid red;");
			Thread.sleep(2000);
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "");
		}
	}

	
	
	public static WebDriver AjaxAuto_select(WebElement autoOptions, String value)
	{
		WebDriverWait wait;
		wait = new WebDriverWait(driver, 5);
		
		
		
		wait.until(ExpectedConditions.visibilityOf(autoOptions));

		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
		for(WebElement option : optionsToSelect)
		{
			try 
			{
		        
				if(option.getText().equals(value)) 
	        	{
					System.out.println("Trying to select: "+value);
					Thread.sleep(3000);
					option.click();
					Thread.sleep(2000);
					return driver; 
	        	} 
			}catch(Exception e)	
			{
		      throw new AssertionError("A clear description of the failure", e);
			}
        }
		return driver; 
		
    }
	
	
	public static WebDriver Set_Text(WebElement settext,String value)
    
    {
		try
           {
        			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
                               .pollingEvery(10,TimeUnit.SECONDS)
                               .ignoring(NoSuchElementException.class);
        			waitforelement.until(ExpectedConditions.visibilityOf(settext));
        			WebElement SETTEXT= settext;
        			if(SETTEXT.getText().isEmpty())
        			{
        				SETTEXT.sendKeys(value);
        				System.out.println("Entered value in textbox is" +value );
        			}
            
        			else
        			{
        				String Existingtest=SETTEXT.getText();
        				System.out.println("Text displayed in textbox is " +Existingtest);
        				SETTEXT.clear();
        				SETTEXT.sendKeys(value);
        				System.out.println("Entered value in textbox is" +value );
        			}
           }catch(NoSuchElementException e)
			{
           		    e.printStackTrace();
           			new AssertionError("A clear description of the failure", e);
                  
			}
           return driver;
           
    }
	
	
	public static WebDriver click_Method(WebElement clickable)
	{
		try
		{
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.elementToBeClickable(clickable));
			WebElement CLICK= clickable;
			CLICK.click();
			
		}catch(NoSuchElementException e)
	    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
		
		}
		
		
		return driver;
	
	}
	

	public static WebDriver Right_click_Method(WebElement clickable)
	{
		try
		{
				FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.elementToBeClickable(clickable));
				WebElement CLICK= clickable;
				Actions act = new Actions(driver);
				act.contextClick(CLICK).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			
		}catch(NoSuchElementException e)
	    {
		  e.printStackTrace();
		  throw new AssertionError("A clear description of the failure", e);
		
		}
		return driver;
	
		
	}
	
	public static WebDriver QuestionType_operation(WebElement AjaxElement,WebElement Min_Number, String value,int MinNumber)
    {
           WebDriverWait wait;
           wait = new WebDriverWait(driver, 5);
           wait.until(ExpectedConditions.visibilityOf(AjaxElement));
           List<WebElement> optionsToSelect = AjaxElement.findElements(By.tagName("li"));
           for(WebElement option : optionsToSelect)
           {
             try {       
            	 if(option.getText().equals(value)) 
            	 	{
            		 System.out.println("Trying to select: "+value);
            		 option.click();
                     System.out.println("Trying to select: "+value);
                     WebElement SETNUMBER= Min_Number;
                     int i = Double.valueOf(MinNumber).intValue();
         	         SETNUMBER.sendKeys(String.valueOf(i));                     
                     break;
                    }
            	 return driver; 
             	} 
      
             catch(Exception e)
	   	   		{
		          throw new AssertionError("A clear description of the failure", e);
	             
	   	   		}
           }

        return driver; 
      
    }
	
	
	public static WebDriver Webtable_Search(WebElement search,String value) throws InterruptedException 
	{
		   try
			{
				FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.visibilityOf(search));
		        WebElement SETTEXT= search;
		        //Highlighter.highLightElement(driver, SETTEXT);

		        	String Existingtest=SETTEXT.getText();
		        	System.out.println("Text displayed in textbox is " +Existingtest);
		        	Thread.sleep(1000);
		        	SETTEXT.clear();
		        	SETTEXT.sendKeys(value);
		        	System.out.println("Entered value in textbox is" +value );
		        
			}catch(NoSuchElementException e)
			    {
				e.printStackTrace();
				throw new AssertionError("A clear description of the failure", e);
				
				}
		
		return driver;
	          
	          
	}
	
	/*
	 * Method to stop execution for 2 secs.
	 * */
	
	public static void Wait_sleep() throws InterruptedException 
	{
		   
        		Thread.sleep(2000);
			}


	public static WebDriver Webtable_element_click(WebElement Elementtobeclick,String value ) throws InterruptedException
	{
		try
		{
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver)
                                         .withTimeout(60, TimeUnit.SECONDS)
                                         .pollingEvery(10,TimeUnit.SECONDS)
                                         .ignoring(NoSuchElementException.class);
                           waitforelement.until(ExpectedConditions.visibilityOf( Elementtobeclick));
            WebElement mytable =  Elementtobeclick;
                     //Highlighter.highLightElement(driver, mytable);
            List<WebElement> rows_table=mytable.findElements(By.tagName("tr"));
            int row_count=rows_table.size();
            for(int row=0;row<row_count;row++)
            {
            	List<WebElement> columns_row=rows_table.get(row).findElements(By.tagName("td"));
                int column_count=columns_row.size();
                System.out.println("Number of cells In Row "+row+" are "/*+column_count*/);
                for(int column=0;column<column_count;column++)
                {
                	String celtext=columns_row.get(column).getText();//System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
                    System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
                    
                    try 
                    {
                    	if(celtext.equals(value))
                    	{
                    		System.out.println("update name is "+value);
                    		JavascriptExecutor javascript = (JavascriptExecutor) driver; 
                    		javascript.executeScript("alert('Created Name is found on datatable');"); 
                    		Thread.sleep(2000);
                    		driver.switchTo().alert().accept();
                            break;
                            
                                
                    	}
                            
                    }catch(NoSuchElementException e)
                    {
                    	e.printStackTrace();
                        throw new AssertionError("A clear description of the failure", e);
                        
                    }
                         
                }
                     
            }
              
		}catch (NoSuchElementException exc) 
        {
			exc.printStackTrace();
			throw new AssertionError("A clear description of the failure", exc);
			
        }
		
		return driver;
          
   }


  

  //Webtable_Element_check       

	public static WebDriver Webtable_Element_check(WebElement Elementtobecheck , String value ) throws InterruptedException
	{
		try
		  {
              
              FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver)
                           .withTimeout(60, TimeUnit.SECONDS)
                           .pollingEvery(10,TimeUnit.SECONDS)
                           .ignoring(NoSuchElementException.class);
              waitforelement.until(ExpectedConditions.visibilityOf( Elementtobecheck));
              WebElement mytable =  Elementtobecheck;
       
              List<WebElement> rows_table=mytable.findElements(By.tagName("tr"));
             int row_count=rows_table.size();
             for(int row=0;row<row_count;row++)
             {
              //String rowtext=rows_table.get(row).getText();
                  List<WebElement> columns_row=rows_table.get(row).findElements(By.tagName("td"));
                  int column_count=columns_row.size();
                  System.out.println("Number of cells In Row "+row+" are "+column_count);
           for(int column=0;column<column_count;column++)
              {
                  String celtext=columns_row.get(column).getText();//System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
                  System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);

              try 
              {      
                if(celtext.equals(value))
                  {
                     
                     System.out.println("update name is "+value);
                     JavascriptExecutor javascript = (JavascriptExecutor) driver; 
                     javascript.executeScript("alert('Created Name is found on datatable');"); 
                     Thread.sleep(2000);
                     driver.switchTo().alert().accept();
                     break;
              
                  }
              }
              catch(NoSuchElementException e)
                        {
                         e.printStackTrace();
                         throw new AssertionError("A clear description of the failure", e);
                     
                        }
                   }
              }
		}
		              catch (NoSuchElementException exc) 
                      {
                        exc.printStackTrace();
                        throw new AssertionError("A clear description of the failure", exc);
                      }
		return driver;
	}

		//Web element click

	public static WebDriver Webtable_Element_click(WebElement ElementLocated , String value ) throws InterruptedException
	{	
		try
		{
		
		FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver)
				.withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(10,TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		 waitforelement.until(ExpectedConditions.visibilityOf(ElementLocated));
			WebElement mytable = ElementLocated;
	//Highlighter.highLightElement(driver, mytable);
	List<WebElement> rows_table=mytable.findElements(By.tagName("tr"));
	int row_count=rows_table.size();
	for(int row=0;row<row_count;row++)
	  {
		//String rowtext=rows_table.get(row).getText();
		List<WebElement> columns_row=rows_table.get(row).findElements(By.tagName("td"));
	    int column_count=columns_row.size();
	    System.out.println("Number of cells In Row "+row+" are "/*+column_count*/);
	    for(int column=0;column<column_count;column++)
	     {
	    	
	    	
	    	String celtext=columns_row.get(column).getText();//System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
	    	System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
	    	
	    	   try 
			   {	
	    	      if(celtext.equals(value))
	    	           {
	    		          columns_row.get(column).click();
	    		          break;
	    	           }
	    	    } 
	    	     catch(Exception e) 
	    	             {
	    		            e.printStackTrace();
				            System.out.println("Table not found in this row");
				break;
				          }

	           }	  
	      }
		}
		             catch (NoSuchElementException exc) 
	                          {
                      exc.printStackTrace();
	                           } 
		                 catch (WebDriverException e)
			                         {
		               e.printStackTrace();
	                                  }
		   return driver;

	}

	public static WebDriver Dropdown_text(WebElement selectoption,String Value)
	{
		try
		   {
	          FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS)
				.pollingEvery(5,TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
	         waitforelement.until(ExpectedConditions.elementToBeClickable(selectoption));	
	         Select dropdown1 = new Select((selectoption));
	         dropdown1.selectByVisibleText(Value);
		   }
		   catch(NoSuchElementException e)
	         {
	           System.out.println("Dropdown value is not visible");
	           e.printStackTrace();
	         }
	return driver;
	
	}


	public static void Popup_table_close(WebElement Elementtobeclick) throws InterruptedException 
	{
       
       try
           {
              Thread.sleep(1000);
    	   driver.switchTo().activeElement().click();
                  
           }
       catch(NoSuchElementException e)
               {
                  e.printStackTrace();
                  throw new AssertionError("A clear description of the failure", e);
               }
                 return ;
	}



	public static void Popup_title_verify(WebElement title,String Expectedtitle) throws InterruptedException
	{
       Thread.sleep(1000);
       driver.switchTo().activeElement();
        WebElement Error_name = title ; 
        String popuptitle = Error_name.getText();
        
        try
        {
        if(popuptitle.equals(Expectedtitle))
           {
           System.out.println("User landed on proper page"+popuptitle);
           }
        
        }
        catch(Exception e)
        {
            throw new AssertionError("A clear description of the failure", e);

        }
       
        fn_Driverobj();
	}


	public static void  Popup_Table_CheckBox(List<WebElement> popup_row,String Cost_Element_Name)
	{
	
	List<WebElement> rows = popup_row;

	for (WebElement row : rows) 
	 {
	    WebElement secondColumn = row.findElement(By.xpath(".//td[2]"));
	    if(secondColumn.getText().equals(Cost_Element_Name))
	    {
	        WebElement checkbox = row.findElement(By.xpath(".//td[1]/input"));
	        checkbox.click();
	        break;      
	    }   
	 }
	
	}



	public static void  Web_Table_Click(List<WebElement> popup_row,String Cost_Element_Name1) throws InterruptedException
	{
	
			List<WebElement> rows = popup_row;

		for (WebElement row : rows) {
		    WebElement secondColumn = row.findElement(By.xpath(".//td[1]/a"));
		    if(secondColumn.getText().equals(Cost_Element_Name1))
		    {
		        WebElement checkbox = row.findElement(By.xpath(".//td[4]/a"));
		        checkbox.click();
		        break;      
		    }   
		}
	}

	public static void  Web_Table_Click_edit(List<WebElement> popup_row,String Cost_Element_Name1) throws InterruptedException
	{
	
			List<WebElement> rows = popup_row;

		for (WebElement row : rows) {
		    WebElement secondColumn = row.findElement(By.xpath(".//td[1]"));
		    if(secondColumn.getText().equals(Cost_Element_Name1))
		    {
		        WebElement checkbox = row.findElement(By.xpath(".//td[4]/a[@title='Edit']"));
		        checkbox.click();
		        break;      
		    }   
		}
	}

	public static void  Web_Table_Click_importExport(List<WebElement> popup_row,String Cost_Element_Name1) throws InterruptedException
	{
	
			List<WebElement> rows = popup_row;

		for (WebElement row : rows) {
		    WebElement secondColumn = row.findElement(By.xpath(".//td[1]"));
		    if(secondColumn.getText().equals(Cost_Element_Name1))
		    {
		        WebElement checkbox = row.findElement(By.xpath(".//td[4]/a[@title='Export/Import']"));
		        checkbox.click();
		        break;      
		    }   
		}
	}

	public static void  Web_Table_Click_link(List<WebElement> popup_row,String Cost_Element_Name1) throws InterruptedException
	{
	
			List<WebElement> rows = popup_row;

		for (WebElement row : rows) {
		    WebElement secondColumn = row.findElement(By.xpath(".//td[1]/a"));
		    if(secondColumn.getText().equals(Cost_Element_Name1))
		    {
		        secondColumn.click();
		        break;      
		    }   
		}
		
	
	
	}

	public static WebDriver Popup_table_Search(WebElement search,String value) throws InterruptedException 
	{
	  try
		{
		    driver.switchTo().activeElement();
			//FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
			//		.pollingEvery(10,TimeUnit.SECONDS)
			//		.ignoring(NoSuchElementException.class);
			//waitforelement.until(ExpectedConditions.visibilityOf(search));
			
			Thread.sleep(3000);
	        WebElement SETTEXT= search;
	        //Highlighter.highLightElement(driver, SETTEXT);
	        
	        SETTEXT.sendKeys(value);
		  }
	       catch(NoSuchElementException e)
		  {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
		  }
	           return driver;
	 }

	public static void WebTable_ActionColumn_click(List<WebElement> Action_EditIcon,String Cost_Element_Name)
	{
	
	List<WebElement> rows = Action_EditIcon;

	for (WebElement row : rows)
	 {
	    WebElement secondColumn = row.findElement(By.xpath(".//td[1]"));
	    if(secondColumn.getText().equals(Cost_Element_Name))
	    {
	        WebElement checkbox = row.findElement(By.xpath(".//td[5]/a"));
	        checkbox.click();
	        break;      
	    }   
	 }
	}



	public static void Dropdownselect(WebElement Elementtobecheck , String value)
	{
	       Select SelectDropdown = new Select (Elementtobecheck);
           List<WebElement> oSize =  SelectDropdown.getOptions();
           int iListSize = oSize.size();

       // Setting up the loop to print all the options
          for(int i =0; i < iListSize ; i++)
            {
	     // Storing the value of the option	
	      String sValue = SelectDropdown.getOptions().get(i).getText();
	    // Printing the stored value
	        System.out.println(sValue);
	        System.out.println(i);
	   // Putting a check on each option that if any of the option is equal to 'Africa" then select it 
	     if(sValue.equals(value))
	   		   {
		 SelectDropdown.selectByIndex(i);
		 break;
	   		   }
            }
    }

    public static WebDriver Radiobutton_click(List<WebElement> Radio_clickable)
    {
	     List<WebElement> options = Radio_clickable ;
	     Random random = new Random();
	     int index = random.nextInt(options.size());
	     options.get(index).click();   
	     
	     return driver;
	}

    public static WebDriver Set_Number(WebElement settext,String value)
    {
    	try
	      {
		    FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(10,TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		    waitforelement.until(ExpectedConditions.visibilityOf(settext));
		    WebElement SETTEXT= settext;
		
		   int i = Double.valueOf(value).intValue();
		   SETTEXT.sendKeys(String.valueOf(i));
           if(SETTEXT.getText().isEmpty())
          	  {
           SETTEXT.sendKeys(value);
           System.out.println("Entered value in textbox is" +value );
          	  }
           }
	     catch(NoSuchElementException e)
	        {
		 e.printStackTrace();
		 throw new AssertionError("A clear description of the failure", e);
		     }
       
	return driver;
    }
    
    public static WebDriver Dropdown_Text(WebElement dropdown,String visibletext)
    {
    	try
		{
    		
		   FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS)
					.pollingEvery(5,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
		   waitforelement.until(ExpectedConditions.elementToBeClickable(dropdown));	
		   Select dropdown1 = new Select(dropdown);
		   dropdown1.selectByVisibleText(visibletext);
		
		  }
	     catch(NoSuchElementException e)
		    {
		 System.out.println("Dropdown value is not visible");
		 e.printStackTrace();
		 throw new AssertionError("error is"+e);
		    }
	   return driver;
    }

    public static WebDriver Popup_table_Click(WebElement clickable1) throws InterruptedException
    {
    	try
    	{
			WebElement CLICK= clickable1;
			driver.switchTo().activeElement();
			Thread.sleep(3000);
			CLICK.click();
 
    	}
	    catch(NoSuchElementException e)
	    {
	    	e.printStackTrace();
	    	throw new AssertionError("A clear description of the failure", e);
		}
		  
    	return driver;
    }

    public static WebDriver Popup_Delete_Alert(WebElement Delete_click) throws InterruptedException
    {
    	
    	try
    	{
    		FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.elementToBeClickable(Delete_click));
			WebElement CLICK= Delete_click;
			//Highlighter.highLightElement(driver, CLICK);
			CLICK.click();
			Thread.sleep(3000);
			driver.switchTo().alert().accept();    

	               
    	}
    	catch(NoSuchElementException e)	           
    	{
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
				   
    	}
		return driver;
    }
    
    public static void Change_Workspace(String Workspace_Name) throws InterruptedException
    {	   
    	WebElement workspace_click=driver.findElement(By.xpath("//ul/li/a/span[@class='active-workspacename']"));
	    fn_CheckVisibility_Button(workspace_click);
	    System.out.println("User clicked on workspace management");
	    Thread.sleep(1000);
	    List<WebElement> liList2 = driver.findElements(By.xpath("//ul/li/a[@data-type='workspaceSwitch']/h4")); 
		int listcount1=liList2.size();
		for(int list=0;list<listcount1;list++)
			{
			System.out.println("Number of List "+listcount1);
			try
			    {
				
				String listtext=liList2.get(list).getText();
				if(listtext.equals(Workspace_Name))
					{
				 
					liList2.get(list).click(); 
					System.out.println("Value found"+listtext);
				 
					Alert alert1=driver.switchTo().alert();      
            
		            // Capturing alert message.    
		            String alertMessage1=driver.switchTo().alert().getText();        
		                     
		            // Displaying alert message     
		            System.out.println(alertMessage1);           
		                     
		            // Accepting alert      
		            alert1.accept();    
		            break;
		            }
			    }
			catch(Exception e)
				{
				
	    		e.printStackTrace();
				System.out.println("value not found in this li");
				break;
				}
			}  
      }


    public static void fn_CheckVisibility_Button(WebElement ElementToClick)
      {
    	fn_WaitForVisibility(ElementToClick, 50);
		boolean status=ElementToClick.isEnabled();
		if(status==true)
		   {
			ElementToClick.click();	
		   }
		else
			{
			System.out.println("Button is Not Enable");
			}	
      }

    public static void fn_WaitForVisibility(WebElement element, int Timeout)
    {
    	WebDriverWait WaitObj=new WebDriverWait(fn_Driverobj(),Timeout);
		WaitObj.until(ExpectedConditions.visibilityOf(element));
    }

    public static WebDriver fn_Driverobj()
    {
    	return  driver;
    }
    
    public static WebDriver closeBrowser()
	
	  {
	   try{
		driver.close();
	      }catch (NoSuchElementException exc) 
     {
		exc.printStackTrace();
		throw new AssertionError("A clear description of the failure", exc);
     }
	return driver;
	 }

    public static WebDriver QuitBrowser()
    
	  {
	   try{
		driver.quit();
	      }catch (NoSuchElementException exc) 
     {
		exc.printStackTrace();
		throw new AssertionError("A clear description of the failure", exc);
     }
	return driver;
		
	 }
    
    public static WebDriver Popup_Set_text(WebElement settext,String value)
    {
 	   try
 		{
 		    driver.switchTo().activeElement();
 			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
 					.pollingEvery(10,TimeUnit.SECONDS)
 					.ignoring(NoSuchElementException.class);
 			waitforelement.until(ExpectedConditions.visibilityOf(settext));
 	        WebElement SETTEXT= settext;

 	        	String Existingtest=SETTEXT.getText();
 	        	System.out.println("Text displayed in textbox is " +Existingtest);
 	        	SETTEXT.clear();
 	        	SETTEXT.sendKeys(value);
 	        	System.out.println("Entered value in textbox is" +value );
 	        
 		}catch(NoSuchElementException e)
 		    {
 			e.printStackTrace();
 			throw new AssertionError("A clear description of the failure", e);
 			
 			}
 	       
 		return driver;
    }

    public static void Unordered_List_Click(List<WebElement> List_name,String Listname)
    {
 		 List<WebElement> liList1 =List_name;
 		 
 		 int listcount=liList1.size();
 		 try{
 		 for(int list=0;list<listcount;list++)
 		 {
 			//System.out.println("Number of List "+listcount);
 			
 			
 			 String listtext=liList1.get(list).getText();
 			 if(listtext.equals(Listname))
 			 {
 				 
 				 liList1.get(list).click(); 
 				 //Thread.sleep(2000);
 				 System.out.println("Value found "+listtext);
 				 System.out.println("Number of List "+listcount);
 			 }
 			           
 		  }
 		 
 		 }catch(Exception e){
 	    		e.printStackTrace();
 				System.out.println("value not found in this li");
 				
 				}
 		 }
 	   
    
    public static void div_List_Click(String X ,String Y, WebElement a, WebElement b)
    {
    	
    	String z = a.getText();
    	String q = b.getText();
    	
    	  System.out.println("The facets available for SERVER assets are "+z+" and "+q);
    	  Assert.assertEquals(X, z);
          Assert.assertEquals(Y, q);
 		 
 	}
    
    public static void Facet_List_Click(String X ,String Y, WebElement a, WebElement b)
    {
    	
    	String z = a.getText();
    	String q = b.getText();
    	
    	  System.out.println("The data  available for SERVER assets are "+z+" and "+q);
    	  Assert.assertEquals(X, z);
          Assert.assertEquals(Y, q);
 		 
 	}
 	
 		 
 		 
 	public static void AutoIt_FileUpload(WebElement ElementToClick, String wordpath ) throws IOException, InterruptedException
    {
   	 fn_CheckVisibility_Button( ElementToClick);
   	Process P = Runtime.getRuntime().exec(wordpath);
   	 P.waitFor();
    }

    public static void setClipboardData(String string) 
    {
   	    StringSelection stringSelection = new StringSelection(string);
   	    Toolkit.getDefaultToolkit().getSystemClipboard()
   	             .setContents(stringSelection, null);
    }

    public static void File_Upload_Page(String filename,String foldername) throws AWTException
    {
    	
    	String userDir = System.getProperty("user.home");
		String downloadPath=userDir+"\\"+foldername+"\\"+filename;
		System.out.println("path"+downloadPath);
   	 setClipboardData(downloadPath);//Ctrl+c =//

   	 
   	  try {
   		  Robot robot = new Robot();
   		  robot.keyPress(KeyEvent.VK_CONTROL);
   		  robot.keyPress(KeyEvent.VK_V); 
   		  robot.keyRelease(KeyEvent.VK_V); 
   		  robot.keyRelease(KeyEvent.VK_CONTROL); 
   		  robot.keyPress(KeyEvent.VK_ENTER); 
   		  robot.keyRelease(KeyEvent.VK_ENTER); 
   		  
   		  } catch (AWTException e) 
   	  {
   		         e.printStackTrace();
   	  }
   	 
   	 
    }
    
    public void checkTitle()
    {
    	   	
        String title= driver.getTitle();
        System.out.println("Page title is " +title);
         
    }
    
    
      public void pageHeader(WebElement contenttitle)
    {
         
    	String head =contenttitle.getText();
    	System.out.println("Page header is " +head);	
         
    }
    
      public void Delet_rentry(WebElement webfrpage,String data)
      {
    	  WebElement table = webfrpage;
    	  
    	  table.clear();
    	  System.out.println("data is deleted");
    	  table.sendKeys(data);
    	  System.out.println("data is entered");
    	  
      }
      
     
      
      /*
       * To select desirable element from webtable .
       * 
       * */
      

      public static WebDriver tableCheckBoxValidation(String nameSearch,int nameColumnNo) throws InterruptedException
      {
    	  Wait_sleep();
    	  WebElement dropdownLocator= driver.findElement(By.xpath("//div/label/select"));
    	  //prior we used specific xpath for dropdown //select[@name='questionListInModal_length']
    	  new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(dropdownLocator));
    	  Select SelectDropdown = new Select(dropdownLocator);
    	  List<WebElement> dropdownList =  SelectDropdown.getOptions();
    	  int dropdownListSize= dropdownList.size();
    	  System.out.println("Dropdown Count is"+dropdownListSize);
    	  for(int count= 0;count<=dropdownListSize-1;count++)
    	  {
    		  Wait_sleep();
    		  dropdownLocator.click();
    		  SelectDropdown.selectByIndex(count);
    		  System.out.println("Selected value from the dropdown is"+count);
    		  Wait_sleep();
    		  List<WebElement> allPages = driver.findElements(By.xpath("//div/ul[@class='pagination']/li/a"));
    		  // prior we used specific xpath for pagination //*[@id='questionListInModal_paginate']/ul/li/a
    		  int pagecount=allPages.size();
    		  System.out.println("Total pages :" +pagecount);
    		  for(int i=0; i<=pagecount-1; i++)
    		  {
    			  List<WebElement> tableRow = driver.findElements(By.xpath("//table[@role='grid']/tbody/tr"));       
    			  List<WebElement> rows = tableRow;
    			  System.out.println("Entered into the loop"+i);
    			  try
    			  {
    				  for (WebElement row : rows) 
    				  {
    					  WebElement secondColumn = row.findElement(By.xpath(".//td["+nameColumnNo+"]"));
    					  if(secondColumn.getText().equals(nameSearch))
    					  {

    						  WebElement checkbox = row.findElement(By.xpath(".//td[1]/input"));
    						  if(!checkbox.isSelected())
    						  {
    							  checkbox.click();
    							  System.out.println("Name found in the table is"+nameSearch);
    							  return driver;
    						  }
    						  else
    						  {
    							  System.out.println("Already Item is selected");
    						  }         
    					  }  
    				  }
    			  }catch(Exception e)
    			  {

    			  }
    			  allPages = driver.findElements(By.xpath("//div/ul[@class='pagination']/li/a"));
    			  Wait_sleep();
    			  allPages.get(i).click();
    		  }
    	  }
    	  return driver;
      }
             
         
      /*
       * 
       * To select desirable element from webtable action column.
       * */ 
      
      
      public static WebDriver tableActionColumnValidation(String nameSearch, String actionColumnElementName,
    		  int nameColumnNo, String actionColumnType) throws InterruptedException {

    	  Wait_sleep();
    	  WebElement dropdownLocator = driver.findElement(By.xpath("//div/label/select"));
    	  // prior we used specific xpath for dropdown
    	  // //select[@name='questionListInModal_length']
    	  new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(dropdownLocator));
    	  Select SelectDropdown = new Select(dropdownLocator);
    	  List<WebElement> dropdownList = SelectDropdown.getOptions();
    	  int dropdownListSize = dropdownList.size();
    	  System.out.println("Dropdown Count is" + dropdownListSize);
    	  try {
    		  for (int count = 0; count <= dropdownListSize - 1; count++) {
    			  Wait_sleep();
    			  dropdownLocator.click();
    			  SelectDropdown.selectByIndex(count);
    			  System.out.println("Selected value from the dropdown is" + count);
    			  Wait_sleep();
    			  List<WebElement> allPages = driver.findElements(By.xpath("//div/ul[@class='pagination']/li/a"));
    			  // prior we used specific xpath for pagination
    			  // //*[@id='questionListInModal_paginate']/ul/li/a
    			  int pagecount = allPages.size();
    			  System.out.println("Total pages :" + pagecount);
    			  for (int i = 0; i <= pagecount - 1; i++) {
    				  List<WebElement> tableRow = driver.findElements(By.xpath("//table[@role='grid']/tbody/tr"));
    				  List<WebElement> rows = tableRow;
    				  System.out.println("Entered into the loop" + i);
    				  try {
    					  for (WebElement row : rows) {

    						  switch (actionColumnType) {
    						  case "Non-Question":
    							  WebElement actionColumnName = row.findElement(By.xpath(".//td[" + nameColumnNo + "]"));
    							  if (actionColumnName.getText().equals(nameSearch)) {

    								  WebElement actionColumnOperation = row.findElement(
    										  By.xpath(".//td[last()]/a[@title='" + actionColumnElementName + "']"));
    								  //// td[last()]/a ->.//td[1]/input
    								  actionColumnOperation.click();
    								  System.out.println("Name found in the table is" + nameSearch);
    								  return driver;

    							  }
    							  break;

    						  case "Question":
    							  WebElement questionColumnName = row
    							  .findElement(By.xpath(".//td[" + nameColumnNo + "]"));
    							  if (questionColumnName.getText().equals(nameSearch)) {

    								  WebElement actionColumnOperation = row.findElement(By.xpath(
    										  ".//td[last()]/div/div/a[@title='" + actionColumnElementName + "']"));
    								  //// td[last()]/a ->.//td[1]/input
    								  actionColumnOperation.click();
    								  System.out.println("Name found in the table is :- " + nameSearch);
    								  return driver;

    							  }

    							  break;
    						  case "Search":
    							  WebElement questionColumnName1 = row
    							  .findElement(By.xpath(".//td[" + nameColumnNo + "]"));
    							  if (questionColumnName1.getText().equals(nameSearch)) {
    								  System.out.println("Name found in the table is :- " + nameSearch);
    								  JavascriptExecutor javascript = (JavascriptExecutor) driver;
    								  javascript.executeScript("alert('Question is found ');");
    								  Thread.sleep(2000);
    								  driver.switchTo().alert().accept();
    								  System.out.println("Question  found");
    								  return driver;
    							  }
    							  break;

    						  }
    					  }
    				  } catch (Exception e) {

    				  }
    				  allPages = driver.findElements(By.xpath("//div/ul[@class='pagination']/li/a"));
    				  Wait_sleep();
    				  allPages.get(i).click();
    			  }
    		  }
    	  } catch (Exception u) {

    	  }
    	  JavascriptExecutor javascript = (JavascriptExecutor) driver;
    	  javascript.executeScript("alert('Question not found');");
    	  Thread.sleep(2000);
    	  driver.switchTo().alert().accept();
    	  System.out.println("Question not found");
    	  return driver;

      }
            

      /*
       * 
       * Giving weightage values to questions .
       * 
       * */
      
      
             public static void setNumber(WebElement textBox,String numericData){
                    FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
                                 .pollingEvery(10,TimeUnit.SECONDS)
                                 .ignoring(NoSuchElementException.class);
                    waitforelement.until(ExpectedConditions.visibilityOf(textBox));
              WebElement SETTEXT= textBox;
                    //Changing the string value to integer 
                    //int iTest = Integer.parseInt(value);
                    //Convert string (with decimal or a point) to a integer Ex:1.0 to 1
                    int i = Double.valueOf(numericData).intValue();
                    SETTEXT.sendKeys(String.valueOf(i));
                    
             
             }
      	   
      	   
             /*
              * 
              * Question weightage.
              * 
              * */
      	   
             
             public static void tableWeightColumn(List<WebElement> questionInParameter,String nameSearch,String questionWeight) throws InterruptedException
             {
                    
                    List<WebElement> tableRow = questionInParameter;
                    for (WebElement row : tableRow) 
                     {
                        WebElement secondColumn = row.findElement(By.xpath(".//td[2]"));
                        if(secondColumn.getText().equals(nameSearch))
                        {
                           
                                  WebElement questionweightcolumn = row.findElement(By.xpath(".//td[3]/input"));
                                  setNumber(questionweightcolumn,questionWeight);
                                   System.out.println("successfully enter the weight to the"+nameSearch+"");
                                   break;
                                        
                        }
                    
                     }  
             }
      
      
	public static void createFiveQuestions(List<WebElement> questionInParameter, String nameSearch,
			String questionWeight) throws InterruptedException {

		List<WebElement> tableRow = questionInParameter;
		for (WebElement row : tableRow) {
			WebElement secondColumn = row.findElement(By.xpath(".//td[2]"));
			if (secondColumn.getText().equals(nameSearch)) {

				WebElement questionweightcolumn = row.findElement(By.xpath(".//td[3]/input"));
				setNumber(questionweightcolumn, questionWeight);
				System.out.println("successfully enter the weight to the" + nameSearch + "");
				break;

			}

		}
	}
	
	public static void checkingText()
	{
		
		
		
	}
	
	public void accessTestLinkResult(String testCase, String exception, String result) throws TestLinkAPIException 
    {
       
       //Enter your project API key here. 
	String DEVKEY="1cdde5f905bd7a1d8754b91f24832f7e"; 
      
     //Enter your Test Link URL here
     //String URL= "http://10.193.146.223:81";
     String URL= "http://10.193.146.223:81/lib/api/xmlrpc/v1/xmlrpc.php";
     
     
     //Enter your Test Project Name here 
    String PROJECT_NAME="Third_Eye";
    
     //Enter your Test Plan here
    String PLAN_NAME="Test_Plan_Sprint_16";
    
     //Enter your Test build here
    String BUILD_NAME="Questionnaire_build";
       
    TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(DEVKEY,URL);
    testlinkAPIClient.reportTestCaseResult(PROJECT_NAME, PLAN_NAME, testCase, BUILD_NAME, exception, result);

    }
	
	
	public boolean excelBeforeDownloadFolderVerify(String fileName)
	{	
		String userDir = System.getProperty("user.home");
		String downloadPath=userDir+"\\Downloads";
		System.out.println("path"+downloadPath);
		File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();
		  int a = dirContents.length;
		  	System.out.println(""+a);
		  	for (int i = 0; i < dirContents.length; i++) 
		  	{	
		  		//System.out.println(""+dirContents[i].getName().equals(fileName));
		      if (dirContents[i].getName().equals(fileName)) 
		      {
		          // File has been found, it can now be deleted:
		    	  System.out.println(""+dirContents[i].getName());
		          dirContents[i].delete();
		          System.out.println("name found and deleted");
		          return true;
		      }
		    }
		return false;
		
	}
	
	public boolean excelAfterDownloadFolderVerify(String fileName)
	{	
		String userDir = System.getProperty("user.home");
		String downloadPath=userDir+"\\Downloads";
		System.out.println("path"+downloadPath);
		File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();
		  int a = dirContents.length;
		  	System.out.println(""+a);
		  	for (int i = 0; i < dirContents.length; i++) 
		  	{	
		  		//System.out.println(""+dirContents[i].getName().equals(fileName));
		      if (dirContents[i].getName().equals(fileName)) 
		      {
		          // File has been found, it can now be deleted:
		    	  System.out.println("Download Successfully"+dirContents[i].getName());
		          return true;
		      }
		    }
		return false;
		
	}
	
	public void addAssetthroughExcel(String filename,String Sheetname,String Value1) throws IOException
	{
		
		String userDir = System.getProperty("user.home");
		String filePath = userDir+"\\"+filename+".xlsx";
		//FileInputStream path = new FileInputStream(filePath);
		ExcelReadWrite path = new ExcelReadWrite(filePath);
		
		int row = path.getRow(Sheetname);
		int Columncount=path.getColum(Sheetname);
		//List<Object[]> listof =new ArrayList<Object[]>();
		
		for(int i=0;i<=row;i++)
		{
			//Object[] excelasset = new Object[1];
			//Map<gString,String> testmap = new HashMap<String,String>();
			
			for(int j=0;j<Columncount;j++)
			{
			
				String Skey  = path.readCell(Sheetname, 0, j);
				String Svalue = path.writeCell(Sheetname, i, j,Value1);
				
			}
		}
		
	}
	
	
	public void addexceladdasset(String filename,String Sheetname,String columnname,String Value) throws IOException
	{
		String userDir = System.getProperty("user.home");
		String filePath = userDir+"\\Downloads"+"\\"+filename+".xlsx";
		FileInputStream path = new FileInputStream(filePath);
		
		XSSFWorkbook wb= new XSSFWorkbook(path);
		
		XSSFSheet sheet = wb.getSheet(Sheetname);
		
		int Rowcount = sheet.getLastRowNum();
		
		int columncount= sheet.getRow(0).getLastCellNum();
		
		for(int i=0;i<=Rowcount;i++)
		{
			for(int j=0;j<columncount;j++)
			{
			XSSFCell cell = sheet.getRow(i).getCell(j);//get the value from cell
			String celltext="";
			
			if(cell.getCellType()==Cell.CELL_TYPE_STRING)
			{
			celltext=cell.getStringCellValue();
			if(celltext.equals(columnname))
			{
				
				//sheet.getRow(i).createCell(j).setCellValue(Value);
				//sheet.getRow(i).getCell(j).;
				break;
				
			}
			}
			else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC)
			celltext=String.valueOf(cell.getNumericCellValue());
				
			else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
			celltext="";
			
			break;
			
		}
	
		}
		
		XSSFRow newrow=sheet.createRow(Rowcount++);
		//newrow.createCell(1).setCellValue(Value);
		
		
		FileOutputStream fos =new FileOutputStream(filePath);
		wb.write(fos);
		path.close();
		fos.close();
	}
	
	


    
}

       

            
               

	

