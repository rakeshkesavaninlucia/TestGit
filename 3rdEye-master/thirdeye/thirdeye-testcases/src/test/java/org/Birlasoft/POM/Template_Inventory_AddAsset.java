package org.Birlasoft.POM;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Template_Inventory_AddAsset extends Util 
{
	public Template_Inventory_AddAsset(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}

	
	@FindBy(xpath = "//span[contains(text(),'Asset Data Entry')]")
	public WebElement Inventory_addasset_pagetitle;
	
	@FindBy(xpath = "//h1/small/div/span")
	public WebElement Inventory_addasset_Page_subtitle;
	
	@FindBy(xpath = "//input[@type='TEXT']")
	public WebElement Inventory_addasset_textbox;
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement Inventory_addasset_submit_btn;
	
	@FindBy(xpath = "//a[@class='fa fa-chevron-left btn btn-default']")
	public WebElement Inventory_addasset_back_btn;
	
	//get the invnetory title
		public String get_inventory_addasset_pagetitle()
		{
	        String pageTitle = Inventory_addasset_pagetitle.getText();
			
			return pageTitle ;
		}
		
		
		public void Inventory_addasset_title()
		{
			try{
				String expectedTitle = "Asset Data Entry";
				if(expectedTitle.equals(get_inventory_addasset_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
		}
		
		
		public String get_invnetory_name()
		{
	        String pageTitle = Inventory_addasset_Page_subtitle.getText();
			
			return pageTitle ;
			
			
		}
		
		public void Inventory_Page_inventory_name(String Inventory_Pass_name)
		{
			try{
				String expectedTitle = Inventory_Pass_name;
				if(expectedTitle.equals(get_invnetory_name()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
		}
		
		
		public void Inventory_addasset_textbox(String AssetName)
		{
			
			Set_Text(Inventory_addasset_textbox,AssetName);
			
		}
		
		public void Inventory_addasset_submit_btn()
		{
			click_Method(Inventory_addasset_submit_btn);
			
		}
		
		public void Inventory_addasset_back_btn()
		{
			click_Method(Inventory_addasset_back_btn);
		}
}

