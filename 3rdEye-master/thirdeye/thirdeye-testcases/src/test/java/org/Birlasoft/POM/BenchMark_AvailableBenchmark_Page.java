package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BenchMark_AvailableBenchmark_Page {

	public  BenchMark_AvailableBenchmark_Page(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
	
	
	//objects of BenchMark
	

		
	@FindBy(xpath = "//*[@id='benchmarkView']/tbody")
    public WebElement BenchMark_AvailableBenchmark_Page;
    
    @FindBy(xpath="//a[contains(text(),'Create Benchmark')]")
    public WebElement Create_BenchMark;

    @FindBy(xpath="//a[@class='fa fa-pencil-square-o']")
    public WebElement Edit_BenchMark;
    
    
    
 //calling all objects

    
     
 

    public BenchMark_AvailableBenchmark_Page ()
    {
    	Util.fn_CheckVisibility_Button(BenchMark_AvailableBenchmark_Page);
    }
	
    public void Create_BenchMark ()
    {
    	Util.fn_CheckVisibility_Button(Create_BenchMark);
    	
    }

    public void Edit_BenchMark ()
    {
    	Util.fn_CheckVisibility_Button(Edit_BenchMark);
    }

	
    
    
    
    
    
}
