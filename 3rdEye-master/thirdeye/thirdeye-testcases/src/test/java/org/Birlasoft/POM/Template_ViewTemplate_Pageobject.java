package org.Birlasoft.POM;


import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Template_ViewTemplate_Pageobject extends Util {
	
	public Template_ViewTemplate_Pageobject ( WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	

	@FindBy(xpath = "//span[contains(text(),'Available Asset Template')]")
	public WebElement View_Template_Pageverify;
	
	@FindBy(id = "templateColumnsofview")
	public WebElement View_Template_Table_asset_verify;
	
	@FindBy(xpath ="//input[@type='search']")
	public WebElement View_Template_search;
	
	@FindBy(xpath ="//tr[1]/td[4]/a[1]")
	public WebElement View_Template_edit_icon;
	
	@FindBy(xpath = "//tr[1]/td[4]/a[2]")
	public WebElement View_Template_export_icon;
	
	@FindBy(xpath = "//a[contains(text(),'Create Template')]")
	public WebElement View_Template_createtemp_btn;
	
	
	
	public String get_pagetitle()
	{
		String pageTitle = View_Template_Pageverify.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
		try{
			String expectedTitle = "Available Asset Template";
			if(expectedTitle.equals(get_pagetitle()))
			{
				System.out.println("Page Title is"+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
		
	}
	
	//To verify whether the template is present on the table or not
	public void View_Template_Table_asset_verify(String Templatename) throws InterruptedException
	{
		Webtable_Search(View_Template_search,Templatename);
		Webtable_Element_check(View_Template_Table_asset_verify,Templatename );
	}
	
	//To click on the template in the table
	public void View_Template_Table_asset_click(String Templatename ) throws InterruptedException
	{
		Webtable_Search(View_Template_search,Templatename);
		Webtable_element_click(View_Template_Table_asset_verify,Templatename);
	}
	
	//click on the update/edit icon to update the template name
	public void View_Template_Update_icon(String Templatename) throws InterruptedException
	{
		Webtable_Search(View_Template_search,Templatename);
		click_Method(View_Template_edit_icon);
	}
	
	/*
	 * need to handle the creation of asset through excel
	 * creation of asset through excel is pending
	 */
	
	@FindBy(xpath="//li/a[contains(text(),'Export')]")
	public WebElement View_Template_Exporttab;
	
	@FindBy(xpath="//div/a[contains(text(),'Export')]")
	public WebElement View_Template_Export_Btn;
	
	@FindBy(xpath="//li/a[contains(text(),'Import')]")
	public WebElement View_Template_Importtab;
	
	@FindBy(xpath="//input[@name='fileImport']")
	public WebElement View_Template_Import_ChooseFile;
	
	@FindBy(xpath="//input[@value='Upload']")
	public WebElement View_Template_Import_upload_Btn;
	
	
	
	public void viewTemplateActioncolumn(String AssetName,String ActionColumnElementName,String ActionType) throws InterruptedException
	{
		tableActionColumnValidation(AssetName,ActionColumnElementName,1,ActionType);
	}
	
	public void beforeDownloadExcelverify(String fileName)
	{
		excelBeforeDownloadFolderVerify(fileName);
	}
	
	
	public void ImportActivity(String filename) throws InterruptedException, AWTException, IOException
	{
		click_Method(View_Template_Export_Btn);
		Thread.sleep(2000);
		click_Method(View_Template_Importtab);
		Thread.sleep(2000);
		addexceladdasset("DB_Test_template","AssetTemplate","Name","autotemp");
		//click_Method(View_Template_Import_ChooseFile);
		Thread.sleep(2000);
		//File_Upload_Page(filename);
		Thread.sleep(2000);
		click_Method(View_Template_Import_upload_Btn);
	}
	
	
	public void excelAfterDownloadFolderVerif(String filename)
	{
		 excelAfterDownloadFolderVerify(filename);
	}

}

