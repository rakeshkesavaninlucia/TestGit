package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Question_BenchMark extends Util{

	
	public  Create_Question_BenchMark(WebDriver driver) 
	{
		
		PageFactory.initElements(driver,this);
		
	}
	

        //objects of Question Page
	
	
       @FindBy(id = "select2-userWorkSpaceId-container")
	     public WebElement workspace;
       
       @FindBy(xpath = "//*[@id='select2-userWorkSpaceId-results']")
		 WebElement workspace_1;
     
       @FindBy(id = "select2-questionMode-container")
		 WebElement mode;
       
       @FindBy(id = "select2-questionMode-results")
		 WebElement mode1;
     
       @FindBy(xpath = "//span[starts-with(@id,'select2-questionCategory-')]")
		 WebElement category;
       
       @FindBy(xpath = "//ul[starts-with(@id,'select2-questionCategory-')]")
		 WebElement category1;
       
              
       @FindBy(xpath = "//*[@id='displayName']")
		 WebElement displayName;
       
       @FindBy(xpath = "//*[@id='title']")
		 WebElement title;
       
       @FindBy(xpath = "//*[@id='helpText']")
		 WebElement helpText;
       
       @FindBy(xpath = "//*[@id='select2-questionType-container']")
		 WebElement Question_type;
       
   
       @FindBy(id="select2-questionType-results")
		 WebElement Question_type1;
       
       
       @FindBy(xpath = "//*[@id='divCheckbox']/label")
     	public WebElement BenchMark_CheckBox;
       		 
       
       @FindBy(xpath = "//*[@id='getFragment']/div/select")
    	public WebElement Select_Box;
      		 
       
       @FindBy(xpath = "//*[@id='getFragment']/div[2]/div/input")
		public WebElement BenchMark_CheckBox2;
       
       @FindBy(xpath = "//*[@id='mod-module-createQuestion-1']/form/div/input")
       public WebElement Done_Button;

       
       @FindBy(xpath = "//section[@class='content-header']")	
       public WebElement Create_a_new_question_title_verify1;
        
     
       
     //calling methods
       public void getWrkSpc() 
       {
     		
     		click_Method(workspace);
     		
        }
       
    
	public void getWrkSpcSelect(String workspace) 
	{
		AjaxAuto_select(workspace_1,workspace); 
		//workspace_1.click();
    		
    }
     
       public void getMode() {
    	   click_Method(mode);
    	   //mode.click();
   		
   }
       
       public void getMode1(String mode) {
    	   AjaxAuto_select(mode1,mode); 
    	   //mode.click();
      		
      }
       
       
       public void getcategory() {
    	   click_Method(category);
      		//category.click();
      		
      }
       
     
      	 public void getcategory1(String category) {
    	   AjaxAuto_select(category1,category); 
    	   //mode.click();
       }
       
       
       public void getdisName(String Display_Name) {
    		 displayName.sendKeys(Display_Name);
    		
    }
       
       public void get_title(String Question_title) {
   		title.sendKeys(Question_title);
   		
   }
       
       public void getHelpText(String Question_Title) {
      		helpText.sendKeys(Question_Title);
      		
      }
       
       public void getQuestion_type() {
     		Question_type.click();
     		
     }
      
    	  public void MCQ_Options(){
    		  Question_type.click();
    		   AjaxAuto_select(Question_type1,"Multiple choice" );
    		  
    	  }
    	   
    	 
          public void ClickOnCheckbox() throws InterruptedException
          {
      		
          	click_Method(BenchMark_CheckBox);
      	}
      	

		 public void Select_Box() {
				click_Method(Select_Box);
	  		}
			
			public void Select_Box(String value) {
				Dropdownselect(Select_Box,value);
	  		}
       
			
			
			  public void ClickOnCheckbox2() throws InterruptedException
	          {
	      		
	          	click_Method(BenchMark_CheckBox2);
	      	}
	     
			  
		       public void Done_Button ()
		       {
		       	Util.fn_CheckVisibility_Button(Done_Button);
		       	System.out.println("Click on the button");
		       	
		       }
          
//		       public void Create_a_new_question_title_verify()
//		       {
//		       	
//		       	checkTitle();
//		       	pageHeader(Create_a_new_question_title_verify1);	
//		       	
//		       }
 }

