package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Questionnaire extends Util{

	public  Create_Questionnaire(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
//	
//	       @FindBy(xpath = "//span[contains(text(),'Create a New Questionnaire')]")
//	         public WebElement Create_Questionnaire_Pageverify;
//	       
	       @FindBy(xpath = "//*[@id='name']")
		     public WebElement Questionnaire_name;
	       
	       @FindBy(xpath = "//*[@id='description']")
			 WebElement Description;
	       
	       @FindBy(xpath = "//*[@id='assetType.id']")
			 WebElement select_asset_type;
	       
	       @FindBy(xpath = "//div[@class='box-footer']/input")
			 WebElement click_next;
	       
	       
	     
//	       public String get_PageTitle() {
//	    	   
//	    	   String pageTitle = Create_Questionnaire_Pageverify.getText();
//	   		
//	   		return pageTitle ;
//	   		
//	       }
//	       
//	       
//	    //To verify whether the user is landing on the proper page or not
//	   	public void pageetitle_verify()
//	   	{
//	   		try{
//	   			String expectedTitle = "Create a New Questionnaire";
//	   			if(expectedTitle.equals(get_PageTitle()))
//	   			{
//	   				System.out.println("Page Title is"+expectedTitle);
//	   			}
//	   			
//	   		}catch(Exception e){
//
//	   	      throw new AssertionError("A clear description of the failure", e);
//	   		}
//	   		
//	   	}
//	       
	       
	       
	       public void get_Name(String Questionnaire_title) {
	    	   Questionnaire_name.sendKeys(Questionnaire_title);
	       }
	  
	       public void get_Desc(String Questionnaire_Desc) {
	    	   Description.sendKeys(Questionnaire_Desc);
	       }
	       
	       public void select_asset_type() {
				click_Method(select_asset_type); 
			} 
		  
			public void select_asset_type(String value) {
				Dropdownselect(select_asset_type,value);
	   		}
		    
	       public void get_Next() {
	    	   click_Method(click_next);
	       }
	
}
