package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.util.Map;

import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TcCheckWhetherUserAbleToCreateTemplate extends Util
{
	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	@Test(dataProvider="dp_Create",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void CreateTemplate(Map<String,String> createtemplate) throws InterruptedException
	{	// getting all the column name from excel i.e (key)
		String Username=createtemplate.get("Username");
		String Password=createtemplate.get("Password");
		String AccountId=createtemplate.get("Tenant_ID");
		String Order_ID=createtemplate.get("Order_Set");
		String TC_ID=createtemplate.get("TC_ID");
		String Asset_Type=createtemplate.get("Asset_Type");
		String Template_Name=createtemplate.get("Template_Name");
		String Description=createtemplate.get("Description");
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		Template_CreateTemplate_PageObject createtemplatepg = new Template_CreateTemplate_PageObject(driver);
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		loginpage.Login_Password(Password);
		loginpage.Login_Accountid1(AccountId);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		//2.navigate to create template page
		homepage.Homepage_Configuration();
		homepage.Homepage_TemplateManagement();
		homepage.Homepage_TemplateManagement_CreateTemplate();
		Thread.sleep(2000);
		//3.creating a template
		createtemplatepg.CreateTemplate_template_name(Template_Name);
		createtemplatepg.CreateTemplate_template_description(Description);
		Thread.sleep(2000);
		createtemplatepg.CreateTemplate_Assettype_click(Asset_Type);
		Thread.sleep(2000);
		createtemplatepg.CreateTemplate_template_save();
		
		
	}
	
	@AfterMethod
	public void teardown()
	{
		driver.quit();
		
		
	}
	
	
}

