package org.Birlasoft.POM;
import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectAssetType_Questionnaire extends Util{
	
	public  SelectAssetType_Questionnaire (WebDriver driver) 
	{
		
	PageFactory.initElements(driver,this);
	
}
//objects of Question Page


 
	@FindBy(xpath = "//*[@id='mod-module-questionnaireAsset-1']/form/div[1]/div/section/div/table/tbody/tr/td[1]/span")
    public WebElement page_title;
	
	@FindBy(xpath = "//*[@id='assetList_filter']/label/input")
    public WebElement Select_Asset;

    @FindBy(xpath = "//*[@id='assetList']/tbody/tr/td[1]/input")
	  public WebElement Click_Asset1;
    
    @FindBy(xpath = "//*[@id='assetList_filter']/label/input") 
    public WebElement Delete_Asset;
    
    @FindBy(xpath = "//*[@id='assetList_filter']/label/input")
    public WebElement Select_Asset_again;

//    @FindBy(xpath = "//*[@id='assetList']/tbody/tr/td[1]/input")
//	  public WebElement Select_Asset1_again;

    @FindBy(xpath = "//*[@id='assetList']/tbody/tr/td[1]/input")
	  public WebElement Click_Asset2;
    
    @FindBy(xpath = "//*[@id='assetList_filter']/label/input") 
    public WebElement Delete_Asset1;
    
    @FindBy(xpath = "//*[@id='mod-module-questionnaireAsset-1']/form/div[2]/input")
		public WebElement Select_Next;
     
     
  //To verify whether the user is landing on the proper page or not
  	public void pagetitle_verify()
  	{
  		try{
  			String expectedTitle = "Selected Asset Type :";
  			if(expectedTitle.equals(get_PageTitle_SAT()))
  			{
  				System.out.println("Page Title is"+expectedTitle);
  			}
  			
  		}catch(Exception e){

  	      throw new AssertionError("A clear description of the failure", e);
  		}
  		
  	}
    
    
public void Select_Search(String Select_Sea) 
{
Select_Asset.sendKeys(Select_Sea);
}

public void Select_Click() 
{
	click_Method(Click_Asset1);
  }

public void Delete_Click() 
{
	Delete_Asset.clear();
	
	}


public void Select_Search1(String Select_Sea) 
{
Select_Asset_again.sendKeys(Select_Sea);
}


public void Select_Click1() 
{
	click_Method(Click_Asset2);
  }


public void Delete_Click1() 
{
	Delete_Asset1.clear();
	
	}

public void Click_Next() 
{
	click_Method(Select_Next);
  
}
 
   
 
   public String get_PageTitle_SAT() {
	   
	   String pageTitle = page_title.getText();
		
		return pageTitle ;
		
   }
   
   
	
	
   
 
   
}
