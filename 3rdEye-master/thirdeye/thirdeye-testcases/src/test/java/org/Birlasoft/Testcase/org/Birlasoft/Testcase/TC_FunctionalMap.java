package org.Birlasoft.Testcase.org.Birlasoft.Testcase;



import java.io.IOException;

import org.Birlasoft.POM.FunctionalMap_Available_BCM_Template_Pageobject;
import org.Birlasoft.POM.FunctionalMap_Edit_BCM_Pageobject;

import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
//import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_FunctionalMap extends Util 

{
	
	//public WebDriver driver;
	@BeforeClass
	public void browserinit() throws IOException
	{
		//intialization of the browser
		browser();
		
	}
	
	
	   @Test(priority = 0)
	   public void f() throws InterruptedException  {
			
			  
			
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			
			Thread.sleep(1000);
			
			obj1.Login_username("rakesh1.k@birlasoft.com");
			
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
			
			
	}
	
	 @Test(priority = 1)
	public void Naviagate_To_Parameter() throws InterruptedException
	{
			 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			 Thread.sleep(1000);
			 obj2.Homepage_titleverify("Portfolio Home");
			 obj2.Homepage_FunctionalMaps();
			 obj2.Homepage_FunctionalMaps_ViewBCMTemplates();
			
	}
	
	
	@Test(priority = 2)
	public void Create_BCM() throws InterruptedException
	{
		FunctionalMap_Available_BCM_Template_Pageobject obj1 = new FunctionalMap_Available_BCM_Template_Pageobject(driver);
		obj1.Available_BCM_Template_pagetitle_verify();
		//obj1.Available_BCM_Template_Add_new_BCMBtn();
		//obj1.Available_BCM_Template_Popup_Create_BCM("Auto_test_BCM");
		obj1.Available_BCM_Template_Webtable_Verify("again_te");
		//obj1.Popup_Error_verify();
		obj1.Available_BCM_Template_Edit_BCM("again_te");
		
	
	}
	
	@Test(priority = 3)
	public void Edit_BCM() throws InterruptedException
	{
		FunctionalMap_Edit_BCM_Pageobject obj4 = new FunctionalMap_Edit_BCM_Pageobject(driver);
		
		obj4.Edit_BCM_Pagetitle_verify();
		obj4.Edit_BCM_L1_AddValue_chain_Creation("Auto_L2");
		obj4.Edit_BCM_L2_Business_Function("auto_l2","Cat_struct" );
		obj4.Edit_BCM_L3_Bussiness_Process("Auto_L3");
		
	}
	
	@AfterClass(alwaysRun=true)
	public void Parameter_CloseBrowser()
	{
		
		//closeBrowser();
		//QuitBrowser();
	}
	
	
	

}
