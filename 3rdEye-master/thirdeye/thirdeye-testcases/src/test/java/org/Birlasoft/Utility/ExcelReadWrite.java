package org.Birlasoft.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilePermission;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReadWrite {
	
	FileInputStream fis;
	XSSFWorkbook wb;
	
	//Excel Readwrite is a constructor
	
	
	public ExcelReadWrite(String filepath) throws IOException 
	{

		//Create an inputstream to get data from external source(ex.xl)
		 fis= new FileInputStream(filepath);
		 
		//workbook object
		//load the input stream to a workbook object 
		 wb= new XSSFWorkbook(fis);
		
	}
	
	
	
	// getting the rowcount from excel
	
	public  int getRow(String Sheetname)
	{
		//setting the sheetpath
		XSSFSheet sheet=wb.getSheet(Sheetname);
		return sheet.getLastRowNum();
		
	}
	
	

	// getting the column count from excel
	public  int getColum(String Sheetname)
	{
		//setting the sheetpath
		XSSFSheet sheet=wb.getSheet(Sheetname);
		return sheet.getRow(0).getLastCellNum();
	}
	
	
	
	//Reading the data from excel
	public String readCell(String Sheetname,int row,int col)
	{
		//setting the sheetpath
		XSSFSheet sheet = wb.getSheet(Sheetname);
		XSSFCell cell   = sheet.getRow(row).getCell(col);
		
		String celltext="";
		
		if(cell.getCellType()==Cell.CELL_TYPE_STRING)
		celltext=cell.getStringCellValue();
		
		else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC)
		celltext=String.valueOf(cell.getNumericCellValue());
			
		else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
		celltext="";
		
		return celltext;
	}
	
	//Writing the data to excel
	public String writeCell(String Sheetname,int row,int col,String Value)
	{
		
		//setting the sheetpath
		XSSFSheet sheet = wb.getSheet(Sheetname);
		
		sheet.getRow(row).getCell(col).setCellValue(Value);
		return Value;
	}
	
	//save and closing the excel
	public void saveAndClose(String fpath) throws IOException
	{	
		//output stream is an output stream for writing data to a File
		FileOutputStream fos =new FileOutputStream(fpath);
		wb.write(fos);
		fis.close();
		fos.close();		
	}
	


}
