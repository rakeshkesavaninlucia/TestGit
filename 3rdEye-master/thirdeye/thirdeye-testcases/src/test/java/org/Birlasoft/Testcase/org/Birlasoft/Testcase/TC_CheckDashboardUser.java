package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.util.Map;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Dashboard_Add_New_Dashboard;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_CheckDashboardUser extends Util {

	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	@Test(dataProvider="dp_Dashboard",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void CreateTemplate(Map<String,String> createtemplate) throws InterruptedException
	{	// getting all the column name from excel i.e (key)
		String Username=createtemplate.get("Username");
		String Password=createtemplate.get("Password");
		String AccountId=createtemplate.get("Tenant_ID");
		String Dashboard_Name=createtemplate.get("Dashboard_Name");
		String Widget_Name=createtemplate.get("Widget_Name");
		String Questionnaire_Name=createtemplate.get("Questionnaire_Name");
		String Select_question=createtemplate.get("Select_question");
		String Select_Parameter=createtemplate.get("Select_Parameter");
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		Dashboard_Add_New_Dashboard createdashboardpg = new Dashboard_Add_New_Dashboard(driver);
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		loginpage.Login_Password(Password);
		loginpage.Login_Accountid1(AccountId);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		
		//2.navigate to create dashboard page
		homepage.Homepage_Dashboard();
		Thread.sleep(2000);
		homepage.Homepage_Dashboard_subelement();
		Thread.sleep(4000);
		
		//3.creating a dashboard
		createdashboardpg.get_Text(Dashboard_Name);
		Thread.sleep(2000);
		createdashboardpg.Save_Button();
		Thread.sleep(2000);
		createdashboardpg.Dropdown();
		Thread.sleep(2000);
		createdashboardpg.Dropdown1();
		Thread.sleep(2000);
		createdashboardpg.Add_widget_click();
		Thread.sleep(2000);
		
		//4.Add a widget to your dashboard
		createdashboardpg.get_name(Widget_Name);
		Thread.sleep(2000);

		createdashboardpg.Add_Widget_Button1();
		Thread.sleep(2000);
		createdashboardpg.Setting_Button();
		Thread.sleep(2000);
		createdashboardpg.Select_Questionnaire_Dropdown(Questionnaire_Name);
		Thread.sleep(2000);

		createdashboardpg.Select_Question_Dropdown(Select_question);
		Thread.sleep(2000);

		createdashboardpg.Select_Parameter_Dropdown(Select_Parameter);
		Thread.sleep(2000);
		createdashboardpg.finalmethod();
		Thread.sleep(2000);
		createdashboardpg.Save_Widget_Button();
		Thread.sleep(2000);
		createdashboardpg.Plot_Chart_Button();
		Thread.sleep(2000);
	
		
	}
	
	
	@AfterMethod
	public void teardown()
	{
		driver.quit();
	}
	
	
}	

