package org.birlasoft.thirdeye.service.impl;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionnaireFCParameterBean;
import org.birlasoft.thirdeye.beans.bcm.BCMValueWrapper;
import org.birlasoft.thirdeye.beans.bcm.BCMWrapper;
import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterEvaluator;
import org.birlasoft.thirdeye.colorscheme.service.Colorizer;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.FunctionalMapType;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.repositories.FunctionalMapDataRepository;
import org.birlasoft.thirdeye.repositories.ParameterConfigRepository;
import org.birlasoft.thirdeye.repositories.QuestionnaireParameterRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMAssetValueBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMLevelBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMParameterBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * This service class for BCM reporting.
 * @author samar.gupta
 *
 */
@Service
@Transactional(value = TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BCMReportServiceImpl implements BCMReportService {

	private static Logger logger = LoggerFactory.getLogger(BCMReportServiceImpl.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private BCMLevelService bcmLevelService;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private FunctionalMapService functionalMapService;
	@Autowired
	private ParameterConfigRepository parameterConfigRepository;
	@Autowired
	private Colorizer colorizer;
	@Autowired
	private QualityGateService qualityGateService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private QuestionnaireParameterRepository questionnaireParameterRepository;
	@Autowired
	private FunctionalMapDataRepository functionalMapDataRepository;
	@Autowired
	private AssetRepository assetRepository;
	@Autowired
	private AssetService assetService;
	@Autowired
	private BCMService  bcmService;


	@Override
	public BCMWrapper getBCMWrapperForView(Integer bcmId, Integer parameterId, Integer qeId) {
		Map<Integer, String> bcmColorMap = new HashMap<>();
		if(parameterId != null)			
			bcmColorMap = getColorForBCMLevels(parameterId, qeId);

		IndexBCMBean indexBCMLevelBeans = getBcmFromElasticsearch(bcmId);
		BCMWrapper wrapper = new BCMWrapper();
		List<String> l1List = new ArrayList<>();
		Set<String> l2CategoryList = new HashSet<>();
		Set<BCMValueWrapper> l2List = new HashSet<>();
		for (IndexBCMLevelBean indexBCMLevelBean : indexBCMLevelBeans.getBcmLevels()) {
			int levelNumber = indexBCMLevelBean.getLevelNumber();
			switch (levelNumber) {
			case 1:
				l1List.add(indexBCMLevelBean.getBcmLevelName());
				break;
			case 2:
				l2CategoryList.add(indexBCMLevelBean.getCategory());
				BCMValueWrapper valueWrapper = createNewBcmValueWrapper(indexBCMLevelBean);
				if (parameterId != null && bcmColorMap.containsKey(indexBCMLevelBean.getBcmLevelId())){						
					valueWrapper.setHexColor(bcmColorMap.get(indexBCMLevelBean.getBcmLevelId()));					
				} else {
					valueWrapper.setHexColor("#ffffff");
				}
				l2List.add(valueWrapper);
				break;

			default:
				break;
			}
		}
		wrapper.setL2List(l2List);
		wrapper.setL1List(l1List);
		wrapper.setL2CategoryList(l2CategoryList);

		return wrapper;
	}

	/**
	 * Create new Bcm Value wrapper.
	 * @param indexBCMLevelBean
	 * @return {@code BCMValueWrapper}
	 */
	private BCMValueWrapper createNewBcmValueWrapper(IndexBCMLevelBean indexBCMLevelBean) {
		BCMValueWrapper valueWrapper = new BCMValueWrapper();
		valueWrapper.setCategoryDesc(indexBCMLevelBean.getCategory());
		valueWrapper.setL1Desc(indexBCMLevelBean.getParentBcmLevelName());
		valueWrapper.setL1Id(indexBCMLevelBean.getParentBCMLevelId());
		valueWrapper.setL2Id(indexBCMLevelBean.getBcmLevelId());
		valueWrapper.setValue(indexBCMLevelBean.getBcmLevelName());
		valueWrapper.setCategoryId(1);
		valueWrapper.setSequenceNumber(indexBCMLevelBean.getSequenceNumber());
		return valueWrapper;
	}

	/**
	 * method to return map of bcm Color.
	 * @param parameterId
	 * @return bcmColorMap
	 */
	private Map<Integer, String> getColorForBCMLevels(Integer parameterId, Integer qeId) {
		Parameter param = parameterService.findOne(parameterId);
		Questionnaire questionnaire = questionnaireService.findOne(qeId);
		List<BcmResponseBean> bcmResponseBeansList =bcmLevelFCParamEvaluator(param, questionnaire);
		Map<BcmLevelBean,BigDecimal> mapOfBCMlevelValue  = prepareAssetAvgForBCMColor(bcmResponseBeansList);
		List<BcmLevelBean> listOfBcmLevelBean = (List<BcmLevelBean>) colorizer.getParameterColor(new ParameterBean(param),mapOfBCMlevelValue);
		//TODO: Call Color service and prepare  bcmColorMap.
		Map<Integer, String> bcmColorMap = new HashMap<>();
		if(!listOfBcmLevelBean.isEmpty()){
			for(BcmLevelBean oneBean : listOfBcmLevelBean){
				bcmColorMap.put(oneBean.getId(), oneBean.getHexColor());
			}
		}		
		return bcmColorMap;

	}

	/**
	 * method to prepare map of BCMlevel and asset avg Value.
	 * @param bcmResponseBeansList
	 * @return mapOfBCMlevelValue
	 */
	private Map<BcmLevelBean, BigDecimal> prepareAssetAvgForBCMColor(List<BcmResponseBean> bcmResponseBeansList) {
		Map<BcmLevelBean, BigDecimal> mapOfBCMlevelValue = new HashMap<>();
		for(BcmResponseBean oneBcmResponseBean : bcmResponseBeansList){
			BigDecimal eValue = new BigDecimal(0);
			int divisor = 0;
			for(AssetBean oneAssetBean : oneBcmResponseBean.getAssets()){
				if(!oneBcmResponseBean.fetchParamValueForAsset(oneAssetBean).equals(ParameterBean.NA_BIGDECIMAL)){
					eValue = eValue.add(oneBcmResponseBean.fetchParamValueForAsset(oneAssetBean));
					divisor++;
				}
			}
			if (divisor == 0){
				divisor = 1;
			}			
			eValue = eValue.divide(new BigDecimal(divisor),2,RoundingMode.HALF_UP);
			mapOfBCMlevelValue.put(oneBcmResponseBean.getBcmLevelBean(), eValue);
		}
		return mapOfBCMlevelValue;
	}

	/**
	 * method to evaluate BCMLevel for FC Parameter
	 * @author sunil1.gupta
	 * @param parameterId
	 */
	private List<BcmResponseBean> bcmLevelFCParamEvaluator(Parameter param, Questionnaire questionnaire) {
		List<Parameter> listOfParameters = new ArrayList<>();
		listOfParameters.add(param);
		List<AssetBean> assetBeans = getAssetsForFCParam(param, questionnaire);
		FunctionalCoverageParameterEvaluator fcParamEvaluator = new FunctionalCoverageParameterEvaluator(
				listOfParameters, parameterService, functionalMapService,bcmLevelService, questionnaire);		
		return fcParamEvaluator.evaluateBcmLevel(new ArrayList<AssetBean>(assetBeans), param);
	}

	/**
	 * method to provide set of assetBeans from FC parameter
	 * @author sunil1.gupta
	 * @param param
	 * @return set of assetBeans
	 */
	private List<AssetBean> getAssetsForFCParam(Parameter param, Questionnaire questionnaire) {
		List<AssetBean> assetBeans = new ArrayList<>();
		List<ParameterConfig> paramConfigList = parameterConfigRepository.findByParameter(param);
		if (!paramConfigList.isEmpty()) {
			// only one Parameter Config for one FC expected as per discuss with samar.
			ParameterConfig oneParamConfig = paramConfigList.get(0);
			FunctionalCoverageParameterConfigBean fcConfigBean = Utility.convertJSONStringToObject(oneParamConfig.getParameterConfig(),FunctionalCoverageParameterConfigBean.class);
			if (fcConfigBean != null) {
				FunctionalMap fm = functionalMapService.findOne(fcConfigBean.getFunctionalMapId(), true);
				// fetch functional map data by functional map
				List<FunctionalMapDataBean> fmDataBeans = functionalMapService.fetchFunctionalMapData(fm, questionnaire);
				// pull all distinct assets from list of functional map data
				assetBeans = prepareAssetBeans(fmDataBeans);
			}
		}
		return assetBeans;
	}

	/**
	 * method to prepare AssetBeans 
	 * from assets
	 * @param functionalMapDataBeans
	 * @return {@code Set<AssetBean>}
	 */
	private List<AssetBean> prepareAssetBeans(List<FunctionalMapDataBean> functionalMapDataBeans) {
		List<AssetBean> assetBeans = new ArrayList<>();
		for (FunctionalMapDataBean fmdb : functionalMapDataBeans) {
			assetBeans.add(fmdb.getAssetBean());
		}
		return assetBeans;
	}

	/**
	 * method to get Asset by bcmLevel from function map data table
	 * @param bcmLevel
	 * @return assetBeans
	 */
	@Override
	public List<AssetBean> getBcmLevelAssets(Integer bcmLevelId, Integer fcparameterId, Integer qualityGateId, Integer qeId) {


		//Get BCM Level Asset From Elastic Search
		
		List<Integer> listOfAssetIds =getBCMLevelAssetFromElasticsearch(bcmLevelId,fcparameterId,qeId);
		//get list of Asset from list of asset IDs.
		List<Asset> listOfAssetsfromAssetIds = assetService.findListOfAssetsByIds(listOfAssetIds);
		
		List<AssetBean> assetBeans = assetService.getListOfAssetBean(listOfAssetsfromAssetIds);
		if (qualityGateId != null){			
			return (List<AssetBean>) colorizer.getQualityGateColor(qualityGateService.findOne(qualityGateId), assetBeans);
		}
		return  assetBeans;
	}


	@Override
	public IndexBCMBean getBcmFromElasticsearch(Integer bcmId) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/bcm";

		IndexBCMBean indexBCMBean = new IndexBCMBean();

		RestTemplate restTemplate = new RestTemplate();

		SearchConfig searchConfig = setSearchConfig(bcmId);


		//Firing a rest request to elastic search server
		try {
			indexBCMBean = restTemplate.postForObject(uri, searchConfig, IndexBCMBean.class);
		} catch (RestClientException e) {				
			logger.error("Exception occured in BCMReportServiceImpl :: getBcmFromElasticsearch() :", e);
		}
		return indexBCMBean;
	}

	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return {@code SearchConfig}
	 */
	private SearchConfig setSearchConfig(Integer bcmId) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		searchConfig.setBcmId(bcmId);
		return searchConfig;
	}

	/**
	 * 
	 * @param parameterId
	 * @return {@list QualityGateID}
	 */
	@Override
	public Map<Integer, String> getMapOfQGOneParam(Integer parameterId) {
		Map<Integer, String> mapOfAllQualityGate = new HashMap<>();

		//get all QualityGate within a workspace
		List<QualityGate> qg = qualityGateService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());

		//get all default QualityGate from parameterQualityGate
		List<QualityGate> pqg = parameterService.findByQualityGateIn(qg);
		List<QualityGate> allQG = new ArrayList<>(qg);

		//remove all QualityGate, from set of QualityGate within a workspace, which is not in set of default QualityGate set from parameterQualityGate
		allQG.removeAll(pqg);

		if(!allQG.isEmpty()){
			for(QualityGate oneQG : allQG){
				mapOfAllQualityGate.put(oneQG.getId(), oneQG.getName());
			}}

		return mapOfAllQualityGate; 
	}	

	@Override
	public Map<Questionnaire, Set<Parameter>> createMapOfQeAndSetOfFcParam(Set<Workspace> setOfWorkspaces){
		return createMapOfQeAndSetOfFcParam(questionnaireService.findByWorkspaceInAndQuestionnaireType(setOfWorkspaces, QuestionnaireType.DEF.name()));
	}

	private Map<Questionnaire, Set<Parameter>> createMapOfQeAndSetOfFcParam(List<Questionnaire> listOfQuestionnaires) {
		Set<Questionnaire> setOfQuestionnaires = createSetOfQeFromFunctionalMapData(listOfQuestionnaires);
		// Put FC parameter for each Qe in map.
		Map<Questionnaire, Set<Parameter>> mapOfQeAndSetOfFcParam = new HashMap<>();
		List<QuestionnaireParameter> listOfQuestionnaireParameters = new ArrayList<>();
		if(!setOfQuestionnaires.isEmpty())
			listOfQuestionnaireParameters = questionnaireParameterRepository.findByQuestionnaireIn(new ArrayList<>(setOfQuestionnaires));
		for (QuestionnaireParameter qp : listOfQuestionnaireParameters) {
			if(mapOfQeAndSetOfFcParam.containsKey(qp.getQuestionnaire())){
				Set<Parameter> setOfFCParams = mapOfQeAndSetOfFcParam.get(qp.getQuestionnaire());
				if(qp.getParameterByParameterId().getType().equals(ParameterType.FC.name()))
					setOfFCParams.add(qp.getParameterByParameterId());
				mapOfQeAndSetOfFcParam.put(qp.getQuestionnaire(), setOfFCParams);
			} else {
				Set<Parameter> setOfFCParams = new HashSet<>();
				if(qp.getParameterByParameterId().getType().equals(ParameterType.FC.name()))
					setOfFCParams.add(qp.getParameterByParameterId());
				mapOfQeAndSetOfFcParam.put(qp.getQuestionnaire(), setOfFCParams);
			}
		}
		return mapOfQeAndSetOfFcParam;
	}

	private Set<Questionnaire> createSetOfQeFromFunctionalMapData(List<Questionnaire> listOfQuestionnaires) {
		// Create set of Qe from functional map data.
		Set<Questionnaire> setOfQuestionnaires = new HashSet<>();
		List<FunctionalMapData> listOfFunctionalMapDatas = new ArrayList<>();
		if(!listOfQuestionnaires.isEmpty())
			listOfFunctionalMapDatas = functionalMapDataRepository.findByQuestionnaireIn(listOfQuestionnaires);
		for (FunctionalMapData fmData : listOfFunctionalMapDatas) {
			if(fmData.getQuestionnaire() != null)
				setOfQuestionnaires.add(fmData.getQuestionnaire());
		}
		return setOfQuestionnaires;
	}

	@Override
	public List<QuestionnaireFCParameterBean> getListOfQuestionnaireFCParameterBean(Set<Workspace> setOfWorkspaces){
		List<QuestionnaireFCParameterBean> listOfQeFCParameterBeans = new ArrayList<>();

		Map<Questionnaire, Set<Parameter>> mapOfQeAndSetOfFcParam = createMapOfQeAndSetOfFcParam(setOfWorkspaces);
		for (Map.Entry<Questionnaire, Set<Parameter>> entry : mapOfQeAndSetOfFcParam.entrySet()) {
			Questionnaire qe = entry.getKey();
			Set<Parameter> setOfFCParams = entry.getValue();
			for (Parameter parameter : setOfFCParams) {
				QuestionnaireFCParameterBean qeFcParam = new QuestionnaireFCParameterBean();
				qeFcParam.setQuestionnaireId(qe.getId());
				qeFcParam.setQuestionnaireName(qe.getName());
				qeFcParam.setFcParameterId(parameter.getId());
				qeFcParam.setFcParameterName(parameter.getDisplayName());
				listOfQeFCParameterBeans.add(qeFcParam);
			}
		}		
		return listOfQeFCParameterBeans;
	}

	/** Method To get Bcm Level Asset from Elastic Search
	 * @param bcmLevelId
	 * @param fcparameterId
	 * @param qeId
	 * @return
	 */
	private List<Integer> getBCMLevelAssetFromElasticsearch(Integer bcmLevelId, Integer fcparameterId, Integer qeId){


		BcmLevel bcmLevel = bcmLevelService.findOneLoadedBcmLevel(bcmLevelId);

		IndexBCMBean indexBCMBean = getBcmFromElasticsearch(bcmLevel.getBcm().getId());
		// TODO :Code to be optimize
		List<Integer> listOfBCMLevelAsset =  new ArrayList<Integer>();
		for(IndexBCMLevelBean indexBCMLevelBean : indexBCMBean.getBcmLevels()){
			if (indexBCMLevelBean.getBcmLevelId().equals(bcmLevelId))
				for(IndexBCMParameterBean indexBCMParamBean : indexBCMLevelBean.getBcmEvaluatedValue()){
					if(indexBCMParamBean.getParameterId()==fcparameterId && indexBCMParamBean.getQuestionnaireId() == qeId)
						for(IndexBCMAssetValueBean indexBcmAssetValueBean :indexBCMParamBean.getValues()){
							BigDecimal assetvalue = new BigDecimal(indexBcmAssetValueBean.getEvaluatedPercentage());
							if(assetvalue.intValue()>-1){
								listOfBCMLevelAsset.add(indexBcmAssetValueBean.getAssetId());
							}
						}
				}
		}
		return listOfBCMLevelAsset;
	}
	
	@Override
	public List<Integer> getListOfAssetBasedOnLevelIdsFromElasticSearch(Integer[] bcmLevelIds){
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());	
		List<Bcm> getListOfBcm = bcmService.findByWorkspaceInAndStatus(listOfWorkspaces, true);
		Integer bcmId = null;
		for(Bcm getBcmId : getListOfBcm){
			bcmId = getBcmId.getId();
		}
		IndexBCMBean indexBCMBean = getBcmFromElasticsearch(bcmId);
		List<Integer> listOfBCMLevelAsset =  new ArrayList<>();
		for(Integer bcmlevelid : bcmLevelIds){
			for(IndexBCMLevelBean indexBCMLevelBean : indexBCMBean.getBcmLevels()){
				if(indexBCMLevelBean.getBcmLevelId().equals(bcmlevelid)){
					for(IndexBCMParameterBean indexBCMParamBean : indexBCMLevelBean.getBcmEvaluatedValue()){

						for(IndexBCMAssetValueBean indexassetbean: indexBCMParamBean.getValues()){
							BigDecimal assetvalue = new BigDecimal(indexassetbean.getEvaluatedPercentage());
							if(assetvalue.intValue()>-1){
								listOfBCMLevelAsset.add(indexassetbean.getAssetId());
							}

						}

					}
				}

			}

		}

		return listOfBCMLevelAsset;
}
}