package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.beans.tco.CostElementBean;
import org.birlasoft.thirdeye.beans.tco.CostStructureBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TcoResponseServiceImpl implements TcoResponseService {
	
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private ParameterService parameterService;
	
	@Override
	public List<ChartOfAccountBean> extractChartOfAccounts(Questionnaire questionnaire) {
		List<QuestionnaireParameter> listOfQp = questionnaireParameterService.getListOfRootQuestionnaireParameters(questionnaire);
		List<ParameterBean> listOfParameterBean = new ArrayList<>();
		for (QuestionnaireParameter questionnaireParameter : listOfQp) {
			Parameter parameter = parameterService.findFullyLoaded(questionnaireParameter.getParameterByParameterId().getId());
			ParameterBean parameterBean = parameterService.generateParameterTree(parameter);
			listOfParameterBean.add(parameterBean);
		}
		
		return this.prepareListOfChartOfAccounts(listOfParameterBean, questionnaire);
	}
	
	private List<ChartOfAccountBean> prepareListOfChartOfAccounts(
			List<ParameterBean> listOfParameterBean, Questionnaire questionnaire) {
		List<ChartOfAccountBean> listToReturn = new ArrayList<>();
		
		Set<QuestionnaireAsset> setOfQa = questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire);
		for (QuestionnaireAsset questionnaireAsset : setOfQa) {
			ChartOfAccountBean accountBean = new ChartOfAccountBean();
			accountBean.setAsset(new AssetBean(questionnaireAsset.getAsset()));
			for (ParameterBean oneParam : listOfParameterBean) {
				CostStructureBean costStructureBean = prepareCostStructureBeanFromParamBean(oneParam, questionnaireAsset);
				accountBean.addCostStructure(costStructureBean);
			}
			listToReturn.add(accountBean);
		}
		
		return listToReturn;
	}

	private CostStructureBean prepareCostStructureBeanFromParamBean(ParameterBean oneParam, QuestionnaireAsset questionnaireAsset) {
		CostStructureBean costStructureBean = new CostStructureBean();
		costStructureBean.setDisplayName(oneParam.getDisplayName());
		//add child cost structure
		for (ParameterBean oneChildParam : oneParam.getChildParameters()) {
			costStructureBean.addChildCostStructure(prepareCostStructureBeanFromParamBean(oneChildParam, questionnaireAsset));
		}
		
		//add child cost element
		for (QuestionBean oneChildQuestion : oneParam.getChildQuestions()) {
			costStructureBean.addChildCostElement(prepareCostElementBeanFromQuestionBean(oneChildQuestion, questionnaireAsset));
		}
		return costStructureBean;
	}

	private CostElementBean prepareCostElementBeanFromQuestionBean(QuestionBean question, QuestionnaireAsset questionnaireAsset) {
		CostElementBean costElementBean = new CostElementBean();
		costElementBean.setTitle(question.getTitle());
		List<QuestionnaireParameter> listOfQuestionnaireParameters = questionnaireParameterService.findByQuestionnaireAndParameter(questionnaireAsset.getQuestionnaire(), 
				parameterService.findOne(question.getParentParameter().getId()));
				
		QuestionnaireQuestion qq = questionnaireQuestionService.getQqForQuestion(question, questionnaireAsset, listOfQuestionnaireParameters);
		costElementBean.setQqBean(new QuestionnaireQuestionBean(qq));
		return costElementBean;
	}
}
