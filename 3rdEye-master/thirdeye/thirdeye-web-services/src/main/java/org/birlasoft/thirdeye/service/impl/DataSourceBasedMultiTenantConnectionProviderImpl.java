/**
 * 
 */
package org.birlasoft.thirdeye.service.impl;

import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * Waits for the application context to be setup and prepares the mapping for tenant
 * data sources
 * 
 * @author shaishav.dixit
 *
 */
@Component
@Transactional(value=TransactionManagers.MASTERTRANSACTIONMANAGER, readOnly = true)
public class DataSourceBasedMultiTenantConnectionProviderImpl extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl implements ApplicationListener<ContextRefreshedEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6581981487465L;

	public static final String DEFAULT_TENANT_ID = "default_tenant";

	@Autowired
	private Environment env;

	@Autowired
	private TenantRepository tenantRepository;

	private Map<String, DataSource> map;
	
	private static Logger logger = LoggerFactory.getLogger(DataSourceBasedMultiTenantConnectionProviderImpl.class);	

	@PostConstruct
	public void load() {
		map = new HashMap<>();
	}

	/**
	 * Loads all the tenant datasources from the master database
	 */
	
	
	public void init() throws PropertyVetoException {
		for (Tenant tenant : tenantRepository.findAll()) {
			ComboPooledDataSource dataSource = new ComboPooledDataSource();
			dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
			dataSource.setJdbcUrl(tenant.getTenantDbUrl() + "?noAccessToProcedureBodies=true");
			dataSource.setUser(tenant.getTenantDbUserName());
			dataSource.setPassword(tenant.getTenantDbPassword());
			dataSource.setPreferredTestQuery("SELECT 1");
			dataSource.setTestConnectionOnCheckout(true);
			map.put(tenant.getTenantUrlId(), dataSource);
		}
	}

	@Override
	protected DataSource selectAnyDataSource() {
		return map.get(DEFAULT_TENANT_ID);
	}

	@Override
	protected DataSource selectDataSource(String tenantIdentifier) {
		return map.get(tenantIdentifier);
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)  {
		try {
			init();
		} catch (PropertyVetoException e) {
			logger.error("Unacceptable Value",e); 
		}
	}


}
