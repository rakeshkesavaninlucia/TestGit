package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.AssetTypeStyle;
import org.birlasoft.thirdeye.repositories.AssetTypeStyleRepository;
import org.birlasoft.thirdeye.service.AssetTypeStyleService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTypeStyleServiceImpl implements AssetTypeStyleService {

	@Autowired
	private AssetTypeStyleRepository assetTypeStyleRepository;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Override
	public String getAssetStyle(AssetType assetType) {
		AssetTypeStyle assetTypeStyle = assetTypeStyleRepository.findByWorkspaceAndAssetType(customUserDetailsService.getActiveWorkSpaceForUser(), assetType);
		if (assetTypeStyle == null){
			assetTypeStyle = assetTypeStyleRepository.findByWorkspaceIsNullAndAssetType(assetType);
		}
		return assetTypeStyle.getAssetColour();
	}

}
