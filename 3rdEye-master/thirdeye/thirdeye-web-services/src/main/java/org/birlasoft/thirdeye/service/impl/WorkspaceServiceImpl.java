package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.WorkspaceRepository;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * WorkspaceServiceImpl.java - Service Implementation class for Workspace
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class WorkspaceServiceImpl implements WorkspaceService {
	
	private WorkspaceRepository workspaceRepository; 
	
	/**
	 * 
	 * @param workspaceRepository
	 */
	@Autowired
	public WorkspaceServiceImpl(WorkspaceRepository workspaceRepository) {
		this.workspaceRepository = workspaceRepository;
	}

	@Override
	public Workspace findOne(Integer id) {
		return workspaceRepository.findOne(id);
	}
	
	@Override
	public Workspace save(Workspace workSpace) {
		return workspaceRepository.save(workSpace);
	}
	
	@Override
	public List<Workspace> findAll() {
		return workspaceRepository.findAll();
	}
	
	@Override
	public Set<Workspace> getUserWorkspaceList(User user) {
		Set<Workspace> workspacesInUserWorkspaces = new HashSet<>();
		Set<UserWorkspace> userWorkspaces = user.getUserWorkspaces();
		for(UserWorkspace userWorkspace : userWorkspaces){
			workspacesInUserWorkspaces.add(userWorkspace.getWorkspace());
		}
		return workspacesInUserWorkspaces;
	}
	
	@Override
	public Workspace findOneLoaded(Integer id, boolean loaded) {
		Workspace workspace = workspaceRepository.findOne(id);
		
		if(loaded){
			workspace.getQuestionnaires().size();
			workspace.getUserWorkspaces().size();
		}
		return workspace;
	}
	
	@Override
	public Workspace createWorkspaceObject(Workspace workSpace, User currentUser) {
		workSpace.setUserByCreatedBy(currentUser);
		workSpace.setUserByUpdatedBy(currentUser);
		workSpace.setCreatedDate(new Date());
		workSpace.setUpdatedDate(new Date());
		return workSpace;
	}

	@Override
	public List<User> getUserListByWorkspace(Workspace oneWorkspace) {
		List<User> userList = new ArrayList<>();
		Set<UserWorkspace> userWorkspace = oneWorkspace.getUserWorkspaces();
		for (UserWorkspace oneUserWorkspace : userWorkspace) {
			userList.add(oneUserWorkspace.getUser());
		}
		return userList;
	}

	@Override
	public List<User> getUserListNotInWorkspace(List<User> userList, List<User> allUser) {
		for(User user : userList){
			 if(allUser.contains(user)){
				allUser.remove(user);
		   }
		}
		return allUser;
	}

	@Override
	public Workspace findByIdLoadedUserWorspaces(Integer id) {
		return workspaceRepository.findByIdLoadedUserWorspaces(id);
	}
	
	@Override
	public Map<Integer,String> getUserMapMatchingActiveUsers(List<User> matchedUserList, List<User> userNotIn) {
		List<User> filteredUsers=new ArrayList<>();
		for(User user : matchedUserList){
			 if(userNotIn.contains(user)){
				 filteredUsers.add(user);
		   }
		}
		//Now converting to HashMap
		Map<Integer,String> map=new HashMap<>();
		for (User oneUser : filteredUsers){
			map.put(oneUser.getId(), oneUser.getFirstName()+" "+oneUser.getLastName());	
			}
		return map;
	}
}
