package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 *	Service interface for workspace.
 */
public interface WorkspaceService {
	/**
	 * Get Only One {@code Workspace} with given Id
	 * @param id
	 * @return {@code Workspace} Object
	 */
	public Workspace findOne(Integer id);
	/**
	 * save {@code Workspace} Object
	 * @param workSpace
	 * @return {@code Workspace}
	 */
	public Workspace save(Workspace workSpace);
	/**
	 * Get List of all {@code Workspace}
	 * @return List{@code <Workspace>} Object
	 */
	public List<Workspace> findAll();
	
	/**
	 * Retrieve all the workspaces linked to a User.
	 * @param user
	 * @return Set{@code<Workspace>}
	 */
	public Set<Workspace> getUserWorkspaceList(User user);
	/**
	 * Find one workspace with loaded or not by id.
	 * @param id
	 * @param loaded
	 * @return {@code Workspace}
	 */
	public Workspace findOneLoaded(Integer id, boolean loaded);
	/**
	 * Create {@code new} {@code Workspace} Object.
	 * @param workSpace
	 * @param currentUser
	 * @return {@code Workspace}
	 */
	public Workspace createWorkspaceObject(Workspace workSpace, User currentUser);
	/** Get {@code User List} by {@code workspace}
	 * @param oneWorkspace
	 * @return {@code List<User>}
	 */
	public List<User> getUserListByWorkspace(Workspace oneWorkspace);
	/**
	 * Get user list not in workspace.
	 * @param userList
	 * @param allUser
	 * @return {@code List<User>}
	 */
	public List<User> getUserListNotInWorkspace(List<User> userList, List<User> allUser);
	/**
	 * find By Id Loaded User Worspaces
	 * @param id
	 * @return {@code Workspace}
	 */
	public Workspace findByIdLoadedUserWorspaces(Integer id);
	
	/**
	 * Get Map Matching User list not in workspace
	 * @param userList
	 * @param userNotIn
	 * @return {@code Workspace}
	 */
	public Map<Integer,String> getUserMapMatchingActiveUsers(List<User> userList,List<User> userNotIn);
}
