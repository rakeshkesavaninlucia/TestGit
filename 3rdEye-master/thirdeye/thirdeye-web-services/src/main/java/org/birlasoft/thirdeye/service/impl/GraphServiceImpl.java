package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.add.ADDWrapper;
import org.birlasoft.thirdeye.beans.add.AddDependsOnBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.GraphType;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.GraphAssetTemplateRepository;
import org.birlasoft.thirdeye.repositories.GraphRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetDataBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.GraphService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * Implementation of service class for Graph
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class GraphServiceImpl implements GraphService{
	
	private static Logger logger = LoggerFactory.getLogger(GraphServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	private GraphRepository graphRepository;	
	
	@Autowired
	private GraphAssetTemplateRepository graphAssetTemplateRepository; 
	@Autowired
	private AssetTemplateService assetTemplateService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	/**
	 * Constructor for initializing services
	 * @param graphRepository
	 */
	@Autowired 
	public GraphServiceImpl(GraphRepository graphRepository) {
		this.graphRepository = graphRepository;
	}
	
	
	@Override
	public Graph findOne(Integer id, boolean loaded) {
		Graph g = graphRepository.findOne(id);
		
		if (loaded && g.getId() > 0 ){
			g.getWorkspace().getAssetTemplates().size();
		}
		
		return g;
	}

	@Override
	public Graph save(Graph graph) {
		return graphRepository.save(graph);
	}
	
	@Override
	public List<Graph> findAll() {
		return graphRepository.findAll();
	}

	@Override
	public List<Graph> findByWorkspaceIn(Set<Workspace> workspaces) {
		return graphRepository.findByWorkspaceIn(workspaces);
	}

	@Override
	public List<Graph> findByWorkspaceAndGraphType(Workspace workspace,String graphType) {
		return graphRepository.findByWorkspaceAndGraphType(workspace, graphType);
	}

	@Override
	public Graph createNewGraphObject(Graph graph, User currentUser, Workspace activeWorkspace) {
		graph.setUserByCreatedBy(currentUser);
		graph.setUserByUpdatedBy(currentUser);
		graph.setWorkspace(activeWorkspace);
		graph.setCreatedDate(new Date());
		graph.setUpdatedDate(new Date());
		// Set graph type Network (NW) by default
		graph.setGraphType(GraphType.NW.toString());
		return graph;
	}
	
	@Override
	public List<GraphAssetTemplate> saveGraphAssetTemplates(List<GraphAssetTemplate> graphAssetTemplates) {
		return graphAssetTemplateRepository.save(graphAssetTemplates);
	}
	
	@Override
	public List<GraphAssetTemplate> findByGraph(Graph graph, boolean loaded) {
		List<GraphAssetTemplate> graphAssetTemplates = graphAssetTemplateRepository.findByGraph(graph);
		
		if(loaded){
			for (GraphAssetTemplate graphAssetTemplate : graphAssetTemplates) {
				graphAssetTemplate.getAssetTemplate().getId();
				graphAssetTemplate.getAssetTemplate().getAssetTemplateName();
				graphAssetTemplate.getAssetTemplate().getAssetType().getAssetTypeName();
			}
		}
		
		return graphAssetTemplates;
	}
	
	@Override
	public Graph updateGraphObject(Graph graph, User currentUser, Workspace activeWorkSpaceForUser) {
		Graph graphFromDB = findOne(graph.getId(), false);
		graphFromDB.setGraphName(graph.getGraphName());
		graphFromDB.setUserByUpdatedBy(currentUser);
		graphFromDB.setDescription(graph.getDescription());
		graphFromDB.setUpdatedDate(new Date());
		return graphFromDB;
	}
	
	@Override
	public void deleteInBatch(List<GraphAssetTemplate> graphAssetTemplates) {
		graphAssetTemplateRepository.deleteInBatch(graphAssetTemplates);
	}
	
	@Override
	public Set<ADDWrapper> getAddGraph(Graph graph, Map<String, List<String>> filterMap) {
		// So that hibernate gets it in its session
		Graph g = graphRepository.findOne(graph.getId());
			
		// See which templates are we trying to show
		g.getGraphAssetTemplates().size();
		
		// Get a list of Assets quickly
		List<Integer> listOfAssetIds = new ArrayList<>();
		for (GraphAssetTemplate oneJoin : g.getGraphAssetTemplates()){
			oneJoin.getAssetTemplate().getAssets().forEach(asset -> listOfAssetIds.add(asset.getId()));
		}
		
		return new HashSet<>(getAssetDetailsForAdd(listOfAssetIds, filterMap, new HashSet<>()));
	}

	@Override
	public List<ADDWrapper> getAssetDetailsForAdd(List<Integer> listOfAssetIds, Map<String, List<String>> filterMap, Set<Integer> allAssets) {
		List<ADDWrapper> addWrappers =  new ArrayList<>();
		List<IndexAssetBean> list = getAssetsRelationshipFromElasticsearch(listOfAssetIds, filterMap);
		
		Set<Integer> setOfdependencies = new HashSet<>();
		Set<Integer> duplicateAssets = new HashSet<>();
		
		list.forEach(asset -> {
			ADDWrapper addWrapper = extractAddWrapper(asset, setOfdependencies);
			allAssets.add(asset.getId());
			addWrappers.add(addWrapper);
		});
		
		setOfdependencies.forEach(a -> {
			if(allAssets.contains(a)){
				duplicateAssets.add(a);
			}
		});
		
		setOfdependencies.removeAll(duplicateAssets);
		
		if(!setOfdependencies.isEmpty()) {
			addWrappers.addAll(getAssetDetailsForAdd(new ArrayList<>(setOfdependencies), new HashMap<>(), allAssets));
		}
		
		return addWrappers;
		
	}

	/**
	 * Extract Add warpper from {@link IndexAssetBean}.
	 * @param asset
	 * @param setOfChildren 
	 * @return
	 */
	private ADDWrapper extractAddWrapper(IndexAssetBean asset, Set<Integer> setOfChildren) {
		ADDWrapper addWrapper = new ADDWrapper();
		addWrapper.setName(asset.getName());
		addWrapper.setType(asset.getAssetClass());
		addWrapper.setId(asset.getId());
		
		List<AddDependsOnBean> depends = new ArrayList<>();
		List<IndexRelationshipAssetBean> childrenRelationshipAssetBeans = asset.getRelationship().getChildren();
		for (IndexRelationshipAssetBean dependsRelationshipAssetBean : childrenRelationshipAssetBeans) {
			AddDependsOnBean addDependsOnBean = new AddDependsOnBean();
			addDependsOnBean.setAsset(dependsRelationshipAssetBean.getAssetName());
			setDirectionandRelationshipName(dependsRelationshipAssetBean,addDependsOnBean);
			depends.add(addDependsOnBean);
			setOfChildren.add(dependsRelationshipAssetBean.getAssetId());
		}
		addWrapper.setDepends(depends);
		
		List<String> dependedOnBy = new ArrayList<>();
		List<IndexRelationshipAssetBean> parentsRelationshipAssetBeans = asset.getRelationship().getParents();
		for (IndexRelationshipAssetBean dependsOnByRelationshipAssetBean : parentsRelationshipAssetBeans) {
			dependedOnBy.add(dependsOnByRelationshipAssetBean.getAssetName());
			setOfChildren.add(dependsOnByRelationshipAssetBean.getAssetId());
		}
		addWrapper.setDependedOnBy(dependedOnBy);
		
		addWrapper.setImageUrl(getAssetImage(asset.getAssetClass()));
		return addWrapper;
	}

	/**
	 * method to set direction,reltionshipName and relationshipDisplayName
	 * @param dependsRelationshipAssetBean
	 * @param addDependsOnBean
	 */
	private void setDirectionandRelationshipName(IndexRelationshipAssetBean dependsRelationshipAssetBean, AddDependsOnBean addDependsOnBean){
		
		List<IndexRelationshipAssetDataBean> listOfRelationshipData = dependsRelationshipAssetBean.getRelationshipAssetData();
		for (IndexRelationshipAssetDataBean oneRelationshipData : listOfRelationshipData){
			addDependsOnBean.setDirection(oneRelationshipData.getDirection());
			if(oneRelationshipData.getRelationshipName() != null){
				addDependsOnBean.setRelationshipName(oneRelationshipData.getRelationshipName());
			} else if (oneRelationshipData.getRelationshipDisplayName() != null){
				addDependsOnBean.setRelationshipDisplayName(oneRelationshipData.getRelationshipDisplayName());
			}
		}
	}
	/**
	 * method to get image for Asset
	 * @param assetType
	 * @return {@code String}
	 */
	private String getAssetImage(String assetType) {
		String imageurl ;
		switch (assetType) {
		case "Server":
			imageurl = "static/img/server.svg";
			break;
		case "Database": 
			imageurl =  "static/img/database.svg";
			break;
		case "Person": 
			imageurl =  "static/img/person.svg";
			break;
		case "Laptop":
			imageurl =  "static/img/laptop.svg";
			break;
		case "IT Services":
			imageurl =  "static/img/ITservices.svg";
			break;
		case "Desktop": 
			imageurl =  "static/img/desktop.svg";
			break;
		case "DataCenter":
			imageurl =  "static/img/dataCenter.svg";
			break;
		case "Business Services":
			imageurl =  "static/img/businesServices.svg";
			break;
			
		default:
			imageurl =  "static/img/app.svg";
			break;
		}
		return imageurl;
	}

	@Override
	public void saveGraph(Graph graph, Set<Integer> assetTemplateIds) {
		Graph savedGraph;
		if(graph.getId() == null){
			savedGraph = save(createNewGraphObject(graph, customUserDetailsService.getCurrentUser(), customUserDetailsService.getActiveWorkSpaceForUser()));
		}else{
			savedGraph = save(updateGraphObject(graph, customUserDetailsService.getCurrentUser(), customUserDetailsService.getActiveWorkSpaceForUser()));
		}
		if(!assetTemplateIds.isEmpty() && savedGraph != null){
			if(savedGraph.getId() != null){
				List<GraphAssetTemplate> graphAssetTemplatesToDelete = findByGraph(savedGraph, false);
				deleteInBatch(graphAssetTemplatesToDelete);
			}
			List<GraphAssetTemplate> graphAssetTemplates = createListOfGraphAssetTemplate(assetTemplateIds, savedGraph);
			saveGraphAssetTemplates(graphAssetTemplates);
		}		
	}
	
	/**
	 * method to create list of GraphAssetTemplate.
	 * @param assetTemplateIds
	 * @param savedGraph
	 * @return {@code List<GraphAssetTemplate>}
	 */
	private List<GraphAssetTemplate> createListOfGraphAssetTemplate(Set<Integer> assetTemplateIds, Graph savedGraph) {
		List<GraphAssetTemplate> graphAssetTemplates = new ArrayList<>();
		for (Integer assetTemplateId : assetTemplateIds) {
			AssetTemplate assetTemplate = assetTemplateService.findOne(assetTemplateId);
			if(assetTemplate != null){
				GraphAssetTemplate graphAssetTemplate = new GraphAssetTemplate();
				graphAssetTemplate.setAssetTemplate(assetTemplate);
				graphAssetTemplate.setGraph(savedGraph);
				graphAssetTemplates.add(graphAssetTemplate);
			}
		}
		return graphAssetTemplates;
	}
	
	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return {@code SearchConfig}
	 */
	private SearchConfig setSearchConfig(List<Integer> listOfAssetIds) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		
		searchConfig.setListOfIds(listOfAssetIds);
		return searchConfig;
	}
	
	@Override
	public List<IndexAssetBean> getAssetsRelationshipFromElasticsearch(List<Integer> listOfAssetIds, Map<String, List<String>> filterMap) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetRelationship";
		
		List<IndexAssetBean> indexAssetParentChildBean = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		List<IndexAssetBean> list = new ArrayList<>();

		RestTemplate restTemplate = new RestTemplate();

		SearchConfig searchConfig = setSearchConfig(listOfAssetIds);
		searchConfig.setFilterMap(filterMap);
		
		
		//Firing a rest request to elastic search server
		try {
			indexAssetParentChildBean = (List<IndexAssetBean>) restTemplate.postForObject(uri, searchConfig, List.class);
			list = mapper.convertValue(indexAssetParentChildBean, new TypeReference<List<IndexAssetBean>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in GraphServiceImpl :: getAssetRelationshipFromElasticsearch() :", e);
		}
		return list;
	}


	@Override
	public List<GraphAssetTemplate> findGraphAssetTemplate(AssetTemplate assetTemplate) {
		return graphAssetTemplateRepository.findByAssetTemplate(assetTemplate);
	}

	@Override
	public List<Graph> findByGraphAssetTemplates(GraphAssetTemplate graphAssetTemplate) {
		return graphRepository.findByGraphAssetTemplates(graphAssetTemplate);
	}

}
