package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.User;

/**
 * Service interface for response.
 * @author samar.gupta
 */
public interface ResponseService {

	/**
	 * Find list of response by questionnaire.
	 * @param questionnaire
	 * @param loaded
	 * @return {@code List<Response>}
	 */
	public List<Response> findByQuestionnaire(Questionnaire questionnaire, boolean loaded);

	/**
	 * Save response object.
	 * @param response
	 * @return {@code Response}
	 */
	public Response save(Response response);

	/**
	 * find one response object by id.
	 * @param responseId
	 * @return {@code Response}
	 */
	public Response findOne(Integer responseId);

	/**
	 * Create new response object.
	 * @param qe
	 * @param currentUser
	 * @return {@code Response}
	 */
	public Response createNewResponseObject(Questionnaire qe, User currentUser);
	
	/**
	 * Get map for completion percentage of questionnaire.
	 * @param listOfquestionnaires
	 * @return {@code Map<Integer, Integer>}
	 */
	public Map<Integer, Integer> getMapForCompletionPercentageOfQe(List<Questionnaire> listOfquestionnaires);
	
	/** Find list of response data by response.
	 * @param response
	 * @return {@code List<ResponseData>}
	 */
	public List<ResponseData> findByResponse(Response response);
	/**
	 * Prepare QQs list for NQRQ. 
	 * @param qe
	 * @return {@code List<QuestionnaireQuestionBean>}
	 */
	public List<QuestionnaireQuestionBean> prepareQQsListForNQRQ(Questionnaire qe);
	/**
	 * find list of Response by  Questionnaire
	 * @param qes
	 * @return {@code List<Response>}
	 */
	public List<Response> findByQuestionnaireIn(List<Questionnaire> qes);
	/**
	 * find Response By Id with Loaded Response Datas
	 * @param id
	 * @return {@code Response}
	 */
	public Response findByIdLoadedResponseDatas(Integer id);

	/**
	 * Insert response into response data.
	 * @param newValuesToBeStored
	 * @param idOfQQ
	 * @param alreadyRespondedData
	 * @param otherOptionText 
	 * @return {@code ResponseData}
	 */
	public List<ResponseData> getListOfResponseData(String[] newValuesToBeStored, Integer qq, ResponseData alreadyRespondedData, String otherOptionText);

}
