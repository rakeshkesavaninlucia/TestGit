package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 *	Service interface for user workspace.
 */
public interface UserWorkspaceService {
	/** 
	 * Delete {@code User Workspace}
	 * @param userWorkspace
	 */
	public void delete(UserWorkspace userWorkspace);
	/**
	 * Save user workspace object.
	 * @param userworkspace
	 * @return {@code UserWorkspace}
	 */
	public UserWorkspace save(UserWorkspace userworkspace);
	/**
	 * delete user from workspace.
	 * @param workspace
	 * @param idOfUser
	 */
	public void deleteUserFromWorkspace(Workspace workspace, Integer idOfUser);
	/**
	 * Create user workspace object.
	 * @param user
	 * @param workspace
	 * @return {@code UserWorkspace}
	 */
	public UserWorkspace createNewUserWorkspaceObject(User user, Workspace workspace);
	/**
	 * Find by user.
	 * @param user
	 * @return {@code List<UserWorkspace>}
	 */
	public List<UserWorkspace> findByUser(User user);
}
