package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;


/**
 * This object will be used for JSON serialization
 * 
 * @author samar.gupta
 *
 */
public class JSONDateResponseMapper implements QuantifiableResponse{

	private String responseDate;
	private Double score;

	public JSONDateResponseMapper() {
		super();
	}

	public String getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		
		if (score != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(score));
		}
		
		return valueToBeReturned;
	}
}
