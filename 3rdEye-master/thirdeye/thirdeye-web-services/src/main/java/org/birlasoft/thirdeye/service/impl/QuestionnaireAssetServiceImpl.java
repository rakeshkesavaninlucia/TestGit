package org.birlasoft.thirdeye.service.impl;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.repositories.QuestionnaireAssetRepository;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service class for Questionnaire Asset
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireAssetServiceImpl implements QuestionnaireAssetService {
	@Autowired
	private QuestionnaireAssetRepository questionnaireAssetRepository;

	@Override
	public QuestionnaireAsset save(QuestionnaireAsset questionnaireAsset) {
		return questionnaireAssetRepository.save(questionnaireAsset);
	}
	
	@Override
	public void deleteInBatch(Iterable<QuestionnaireAsset> assetsToBeDeleted) {
		questionnaireAssetRepository.deleteInBatch(assetsToBeDeleted);
	}
	
	@Override
	public List<QuestionnaireAsset> save  (Iterable<QuestionnaireAsset> assetsToBeSaved){
		return questionnaireAssetRepository.save(assetsToBeSaved);
	}


	@Override
	public QuestionnaireAsset findOne(Integer id) {
		return questionnaireAssetRepository.findOne(id);
	}


	@Override
	public List<QuestionnaireAsset> findByQuestionnaire(Questionnaire questionnaire) {
		return questionnaireAssetRepository.findByQuestionnaire(questionnaire);
	}

	@Override
	public Set<QuestionnaireAsset> findByQuestionnaireLoadedAsset(Questionnaire qe) {
		return questionnaireAssetRepository.findByQuestionnaireLoadedAsset(qe);
	}

	@Override
	public List<QuestionnaireAsset> saveAndFlush(
			List<QuestionnaireAsset> questionnaireAssetForAddition) {
		return questionnaireAssetRepository.save(questionnaireAssetForAddition);
	}

	@Override
	public List<QuestionnaireAsset> findByAsset(Asset asset) {		
		return questionnaireAssetRepository.findByAsset(asset);
	}

	@Override
	public Set<QuestionnaireAsset> findByQuestionnaireAndAssetIn(Questionnaire qe, Set<Asset> setOfAsset) {
		return questionnaireAssetRepository.findByQuestionnaireAndAssetIn(qe, setOfAsset);
	}
	
	@Override
	public Set<QuestionnaireAsset> findByQuestionnaireAndAsset(Questionnaire qe, Asset idOfAsset) {
		return questionnaireAssetRepository.findByQuestionnaireAndAsset(qe, idOfAsset);
	}
}
