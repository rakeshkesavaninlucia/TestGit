package org.birlasoft.thirdeye.service.impl;

import java.util.Iterator;
import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.repositories.AssetTypeRepository;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTypeServiceImpl implements AssetTypeService {
	
	
	@Autowired
	private AssetTypeRepository assetTypeRepository;
	
	public AssetTypeServiceImpl() {
		//Default constructor
	}
	
	@Override
	public List<AssetType> findAll() {
		return assetTypeRepository.findAll();
	}
	
	@Override
    public AssetType save(AssetType assetType) {
        return assetTypeRepository.save(assetType);
    }

	@Override
	public AssetType findOne(Integer id) {							
		return assetTypeRepository.findOne(id);
	}

	@Override
	public List<AssetType> extractAssetTypes(AssetTypes[] assetTypes) {
		List<AssetType> listOfAssetTypes = findAll();
		
		Iterator<AssetType> iter = listOfAssetTypes.iterator();

			while (iter.hasNext()) {
				AssetType assetType = iter.next();
				for (AssetTypes assetTypeEnum : assetTypes) {
					if(assetType.getAssetTypeName().equalsIgnoreCase(assetTypeEnum.name())){
						iter.remove();
						break;
					}
				}
			}
		
		return listOfAssetTypes;
	}

	@Override
	public AssetType findByAssetTypeName(String assetTypeName) {
		return assetTypeRepository.findByAssetTypeName(assetTypeName);
	}
}
