/**
 * 
 */
package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author shaishav.dixit
 *
 */
public class CellDataLengthValidator implements ExcelCellValidator {

	private int length;
	private boolean flag;
	private static final String CELL_LENGTH_ERROR = "Data cannot be more than ";
	private static final String CELL_TYPE_ERROR = "Not a valid data type";
	
	public CellDataLengthValidator(int length) {
		this.length = length;
	}

	@Override
	public boolean validate(Cell cell, List<String> errors) {
		flag = false;
		
		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			if(cell.getStringCellValue().length() > length) {
				errors.add(CELL_LENGTH_ERROR + length);
			} 
			flag = true;
		}
		
		return flag;
	}

}
