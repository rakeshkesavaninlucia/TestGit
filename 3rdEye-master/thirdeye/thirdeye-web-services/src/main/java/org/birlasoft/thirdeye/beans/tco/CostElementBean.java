package org.birlasoft.thirdeye.beans.tco;

import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;

public class CostElementBean {
	
	private String title;
	private QuestionnaireQuestionBean qqBean;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public QuestionnaireQuestionBean getQqBean() {
		return qqBean;
	}
	
	public void setQqBean(QuestionnaireQuestionBean qqBean) {
		this.qqBean = qqBean;
	}

}
