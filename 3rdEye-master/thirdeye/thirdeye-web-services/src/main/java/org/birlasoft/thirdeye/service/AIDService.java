package org.birlasoft.thirdeye.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.aid.AIDCentralAssetWrapperBean;
import org.birlasoft.thirdeye.beans.aid.AidSearchWrapper;
import org.birlasoft.thirdeye.constant.aid.AIDBlockType;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.interfaces.SubBlockUser;

/**
 * Service Interface for Aid.
 */

public interface AIDService {

	/**
	 * fetch Aid 
	 * @param aidReportId
	 * @param b
	 * @return {@link Aid}
	 */
	public Aid fetchOne(Integer aidReportId, boolean b);

	/**
	 * Adds a new block of the given type to a aid report structure
	 * @param oneAid
	 * @param blockType
	 */
	public void addBlockToReport(Aid oneAid, AIDBlockType blockType);

	/**
	 * fetch {@link AidBlock} by {@code blockId}
	 * @param blockId
	 * @param b
	 * @return {@link AidBlock} 
	 */
	public AidBlock fetchOneBlock(Integer blockId, boolean b);

	/**
	 * Save {@code List} of {@link AidBlock}
	 * @param asList
	 */
	public void saveBlocks(List<AidBlock> asList);

	/**
	 * Load SubBlocks Configuration Strings.
	 * @param subBlockUser
	 */
	public void loadSubBlocksConfigurationStrings(List<? extends SubBlockUser> subBlockUser);
	
	/**
	 * Delete {@code List} of {@link AidBlock}
	 * @param listOfBlocksForDelete
	 */
	void deleteBlocks(List<AidBlock> listOfBlocksForDelete);

	/**
	 * load SubBlock ResponseStrings.
	 * @param subBlockUsers
	 * @param oneAsset
	 * @param paramEvalMap
	 */
	public void loadSubBlockResponseStrings(List<? extends SubBlockUser> subBlockUsers, Asset oneAsset, Map<Integer, BigDecimal> paramEvalMap);
	
	/**
	 * Save Aid object.
	 * @param aid
	 * @return {@code Aid}
	 */
	public Aid save(Aid aid);
	/**
	 * Create new Aid object.
	 * @param newAid
	 * @param currentUser
	 * @param workspace
	 * @param assetTemplate 
	 * @return
	 */
	public Aid createNewAidObject(Aid newAid, User currentUser, Workspace workspace, AssetTemplate assetTemplate);	
	/**
	 * Find list of Aids by workspace In.
	 * @param workspaces
	 * @return {@code List<Aid>}
	 */
	public List<Aid> findByWorkspaceIn(Set<Workspace> workspaces);
	
	/**
	 * Extracts the details for the parameters that are used as a part of the AID.<br>
	 * The map returned by this method is designed for consumption for the AID specifically.
	 * @param asset
	 * @param aidInDisplay 
	 * @return a map of Assetid : Param id : Param Value
	 */

	public Map<Integer, Map<Integer, BigDecimal>> prepareAssetParameterMap(Aid aidInDisplay, Asset asset);
	/**
	 * method to get Central Asset For Display.
	 * @param oneAid
	 * @param oneAsset
	 * @param paramEvalMap
	 * @return {@code AIDCentralAssetWrapperBean}
	 */
	public AIDCentralAssetWrapperBean getCentralAssetForDisplay(Aid oneAid, Asset oneAsset, Map<Integer, BigDecimal> paramEvalMap);
	/**
	 * Get asset aid from elasticsearch
	 * @param assetId
	 * @param aidId
	 * @return {@code AidSearchWrapper}
	 */
	public AidSearchWrapper getAssetAidFromElasticsearch(Integer assetId, Integer aidId);
	
	
	/**
	 * Get asset ids list from elasticsearch
	 * @param aidId
	 * @param filterMap
	 * @return {@code Set<Integer>}
	 */
	public Set<Integer> getAssetIdsFromElasticSearch(Integer aidId,  Map<String, List<String>> filterMap); 
	
	
	/**
	 * Get aid from assetTemplateId
	 * @param assetTemplateId
	 * @return
	 */
	public List<Aid> findByAssetTemplate(AssetTemplate assetTemplate);
}
