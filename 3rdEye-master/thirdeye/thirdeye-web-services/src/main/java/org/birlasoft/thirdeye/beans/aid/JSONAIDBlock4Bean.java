package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidSubBlockBean;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JSONAIDBlock4Bean extends JSONAIDBlockConfig {

	private enum Block4_SubBlocks {subBlock1, subBlock2}
	
	private JSONAIDSubBlockConfig subBlock1 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock2 = new JSONAIDSubBlockConfig();
	
	public JSONAIDBlock4Bean(){
		super();
	}
	
	public JSONAIDBlock4Bean(AidBlock oneEntity){
		super(oneEntity);
		
		
		if (!StringUtils.isEmpty(oneEntity.getBlockJsonconfig())){
			JSONAIDBlock1Bean deserialized = Utility.convertJSONStringToObject(oneEntity.getBlockJsonconfig(), JSONAIDBlock1Bean.class);
			if (deserialized != null){
				this.subBlock1 = deserialized.getSubBlock1();
				subBlock1.setSubBlockIdentifier(Block4_SubBlocks.subBlock1.name());
				this.subBlock2 = deserialized.getSubBlock2();
				subBlock2.setSubBlockIdentifier(Block4_SubBlocks.subBlock2.name());
			}
		}
		
	}
	
	/**
	 * Constructor to configure values in BLOCK_4.
	 * @param indexAidBlockBean
	 */
	public JSONAIDBlock4Bean(IndexAidBlockBean indexAidBlockBean) {
		super(indexAidBlockBean);
		
		List<IndexAidSubBlockBean> listOfAidSubBlockBeans = indexAidBlockBean.getSubBlocks();
		for (IndexAidSubBlockBean indexAidSubBlockBean : listOfAidSubBlockBeans) {
			if(indexAidSubBlockBean.getSubBlockName() != null){
				Block4_SubBlocks subBlockId = Block4_SubBlocks.valueOf(indexAidSubBlockBean.getSubBlockName());
				switch (subBlockId) {
				case subBlock1:
					subBlock1.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock1.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock2:
					subBlock2.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock2.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				default:
					break;
				}
			}
		}
	}

	public JSONAIDSubBlockConfig getSubBlock1() {
		return subBlock1;
	}

	public void setSubBlock1(JSONAIDSubBlockConfig subBlock1) {
		this.subBlock1 = subBlock1;
	}

	public JSONAIDSubBlockConfig getSubBlock2() {
		return subBlock2;
	}

	public void setSubBlock2(JSONAIDSubBlockConfig subBlock2) {
		this.subBlock2 = subBlock2;
	}

	
	
	@Override
	public JSONAIDSubBlockConfig getSubBlockFromIdentifier(String subBlockIdentifier) {
		Block4_SubBlocks subBlockId = Block4_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			return subBlock1;
		case subBlock2:
			return subBlock2;
		default:
			return new JSONAIDSubBlockConfig();
		}
	}
	
	@Override
	public void setSubBlockFromIdentifier(String subBlockIdentifier, JSONAIDSubBlockConfig subBlockConfig) {
		Block4_SubBlocks subBlockId = Block4_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			this.subBlock1 = subBlockConfig;
			break;
		case subBlock2:
			this.subBlock2 = subBlockConfig;
			break;
		}
		
	}
	
	@JsonIgnore
	@Override
	public List<JSONAIDSubBlockConfig> getListOfBlocks() {
		List<JSONAIDSubBlockConfig> listOfSubBlocks = new ArrayList<JSONAIDSubBlockConfig>();
		
		for (Block4_SubBlocks subBlockId : Block4_SubBlocks.values()){
			listOfSubBlocks.add(getSubBlockFromIdentifier(subBlockId.name()));
		}
		return listOfSubBlocks;
	}
}
