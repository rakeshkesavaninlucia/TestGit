package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Bean class for parameter and its evaluated value for different assets
 * @author shaishav.dixit
 *
 */
public class ParameterEvaluationResponse {

	private ParameterBean parameterBean;
	private Map<AssetBean, BigDecimal> assetResponse = new HashMap<>();
	
	/**
	 * Add asset bean and its evaluated value to map
	 * @param ab
	 * @param d
	 */
	public void addAssetEvaluation (AssetBean ab, BigDecimal d){
		if (ab != null){
			assetResponse.put(ab, d);
		}
	}

	public Set<AssetBean> getAssets(){
		return assetResponse.keySet();
	}
	
	/**
	 * Fetch parameter value for an asset from the map
	 * @param ab
	 * @return
	 */
	public BigDecimal fetchParamValueForAsset(AssetBean ab){
		return assetResponse.get(ab);
	}
	
	public ParameterBean getParameterBean() {
		return parameterBean;
	}

	public void setParameterBean(ParameterBean parameterBean) {
		this.parameterBean = parameterBean;
	}

	
	/**
	 * Get map of asset id and its parameter value
	 * @return
	 */
	public Map<Integer, BigDecimal> fetchAssetMappedByAssetId(){
		 Map<Integer, BigDecimal> responseMap = new HashMap<>();
		 for (Entry<AssetBean, BigDecimal> entry : assetResponse.entrySet()) {
			 responseMap.put(entry.getKey().getId(), entry.getValue());
		}
		 
		 return responseMap;
	}

	public Map<AssetBean, BigDecimal> getAssetResponse() {
		return assetResponse;
	}
}
