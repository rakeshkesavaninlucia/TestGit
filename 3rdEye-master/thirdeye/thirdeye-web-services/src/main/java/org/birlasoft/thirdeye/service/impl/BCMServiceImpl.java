package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.BCMRepository;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for {@link  BCMService}
 *
 */
@Service("bcmService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BCMServiceImpl implements BCMService{
	
	@Autowired
	private BCMRepository bcmRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	
	@Override
	public Bcm saveOrUpdate(Bcm incomingBcm) {
		return bcmRepository.save(incomingBcm);
	}

	@Override
	public List<Bcm> findByWorkspaceIsNullAndBcmIsNull() {
		return bcmRepository.findByWorkspaceIsNullAndBcmIsNull();
	}

	@Override
	public Bcm findOne(Integer idOfBCMTemplate) {
		return bcmRepository.findOne(idOfBCMTemplate);
	}

	@Override
	public List<Bcm> findByWorkspaceIn(Set<Workspace> workspaces, boolean loaded) {
		List<Bcm> listOfBcm = bcmRepository.findByWorkspaceIn(workspaces);
		
		if(loaded){
			for (Bcm bcm : listOfBcm) {
				bcm.getBcm().getBcmName();
			}
		}
		
		return listOfBcm;
	}

	@Override
	public List<Bcm> saveAll(List<Bcm> bcms) {
		return bcmRepository.save(bcms);
	}

	@Override
	public List<Bcm> findByWorkspaceInAndStatus(Set<Workspace> workspaces, boolean status) {
		return bcmRepository.findByWorkspaceInAndStatus(workspaces, status);
	}

	@Override
	public List<Bcm> updateBcmActiveStatusInList(Integer idOfWorkspaceBcm) {
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Bcm> updatedListOfBcms = new ArrayList<>();
		
		if(!listOfWorkspaces.isEmpty()){
			List<Bcm> listOfBcms = this.findByWorkspaceIn(listOfWorkspaces, false);
			for (Bcm bcm : listOfBcms) {
				if(bcm.getId().equals(idOfWorkspaceBcm)) {
					bcm.setStatus(true);
				} else {
					bcm.setStatus(false);
				}
				updatedListOfBcms.add(bcm);
			}
		}
		return updatedListOfBcms;
	}

	@Override
	public boolean isStatusActiveToShowMessage(Integer idOfWorkspaceBcm) {
		List<Bcm> updatedListOfBcms = this.updateBcmActiveStatusInList(idOfWorkspaceBcm);
		
		boolean isStatusActive = false;
		if(!updatedListOfBcms.isEmpty()){
			List<Bcm> savedBcms = this.saveAll(updatedListOfBcms);
			if(savedBcms.size() == updatedListOfBcms.size()){
				isStatusActive = true;
			}
		}
		
		return isStatusActive;
	}
}
