package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.birlasoft.thirdeye.repositories.UserRoleRepository;
import org.birlasoft.thirdeye.service.RoleService;
import org.birlasoft.thirdeye.service.UserRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * UserRoleServiceImpl.java - Service implementation class for UserRole
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class UserRoleServiceImpl implements UserRoleService {
	
	private static Logger logger = LoggerFactory.getLogger(UserRoleServiceImpl.class);
	
	private UserRoleRepository userRoleRepository;
	private RoleService roleService;
		
	/**
	 * @param userRoleRepository
	 * @param roleService
	 */
	@Autowired
	public UserRoleServiceImpl(UserRoleRepository userRoleRepository,
			RoleService roleService) {
		this.userRoleRepository = userRoleRepository;
		this.roleService = roleService;
	}

	@Override
	public UserRole save(UserRole userRole) {
		return userRoleRepository.save(userRole);
	}

	@Override
	public List<UserRole> save(List<UserRole> userRoles) {
		return userRoleRepository.save(userRoles);
	}

	@Override
	public List<UserRole> findByUser(User user) {
		return userRoleRepository.findByUser(user);
	}

	@Override
	public void deleteInBatch(List<UserRole> userRoles) {
		userRoleRepository.deleteInBatch(userRoles);
	}

	@Override
	public List<UserRole> createListOfNewUserRoleObjects(String[] roles, User savedUser) {
		List<UserRole> userRoles = new ArrayList<>();
		for (String roleId : roles) {
			try{
				UserRole userRole = new UserRole();
				userRole.setUser(savedUser);
				userRole.setRole(roleService.findOne(Integer.parseInt(roleId)));
				userRoles.add(userRole);
			   }catch(NumberFormatException nfe){
				   logger.error("Exception when parse parameter:" +roleId , nfe);
			   }
		}
		return userRoles;
	}
}
