package org.birlasoft.thirdeye.beans.parameter;


public class ParameterBaseConfigBean {
	
	private String parameterType;

	public String getParameterType() {
		return parameterType;
	}

	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}
	
}
