package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionAnswerMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.birlasoft.thirdeye.repositories.UserRepository;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 
 * UserServiceImpl.java - Service Implementation class for user
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class UserServiceImpl implements UserService {
	
	private UserRepository userRepository;
	
	/**
	 * 
	 * @param userRepository
	 */
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User findUserByEmail(String email) {		
		return userRepository.findUserByEmail(email);
	}

	@Override
	public User findUserById(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User createUserObject(User newUser, User currentUser) {
		newUser.setCreatedBy(currentUser.getId());
		newUser.setUpdatedBy(currentUser.getId());
		newUser.setCreatedDate(new Date());
		newUser.setUpdatedDate(new Date());
		return newUser;
	}
	
	@Override
	public User updateUserObjectForUserProfile(User currentUser,List<JSONQuestionAnswerMapper> questionAnswermapper) {
		String questionAnswer = currentUser.getQuestionAnswer();
		String jSONString = null;
		if(questionAnswer != null || questionAnswermapper != null)
			jSONString = createJSONString(questionAnswermapper);
		currentUser.setQuestionAnswer(jSONString);
		return currentUser;
	}
	
	/**Method to save question answer in json form 
	 * @param questionAnswermapper
	 * @return jsonString
	 */
	private String createJSONString(List<JSONQuestionAnswerMapper> questionAnswermapper) {
		String jSONString = null;
		// Assume quantifier length = encoded String Array
		if(questionAnswermapper!=null){
				jSONString = Utility.convertObjectToJSONString(questionAnswermapper);
		}
		return jSONString;
	}

	@Override
	public User updateUserObject(User userToUpdate, User currentUser) {
		User userFromDB = findUserById(userToUpdate.getId());
		userFromDB.setFirstName(userToUpdate.getFirstName());
		userFromDB.setLastName(userToUpdate.getLastName());
		userFromDB.setEmailAddress(userToUpdate.getEmailAddress());
		if(userToUpdate.getPassword() != null && !userToUpdate.getPassword().isEmpty()){
			userFromDB.setPassword(userToUpdate.getPassword());
		}
		userFromDB.setUpdatedBy(currentUser.getId());
		userFromDB.setUpdatedDate(new Date());
		return userFromDB;
	}

	@Override
	public Map<Integer, Integer> createMapForUserRoles(Set<UserRole> userRoles) {
		Map<Integer, Integer> mapOfRoleIds = new HashMap<>();
		for (UserRole userRole : userRoles) {
			mapOfRoleIds.put(userRole.getRole().getId(), userRole.getRole().getId());
		}
		return mapOfRoleIds;
	}

	@Override
	public List<User> findByDeleteStatus(Boolean deleteStatus) {
		return userRepository.findByDeleteStatus(deleteStatus);
	}

	@Override
	public User deleteUser(Integer idOfUser) {
		User userById = findUserById(idOfUser);
		userById.setDeleteStatus(Constants.DELETE_STATUS_TRUE);
		return userById;
	}

	@Override
	public List<User> findByDeleteStatusAndAccountExpiredAndAccountLocked(Boolean deleteStatus, Boolean accountExpired, Boolean accountLocked) {
		return userRepository.findByDeleteStatusAndAccountExpiredAndAccountLocked(deleteStatus, accountExpired, accountLocked);
	}
	
	@Override
	public List<User> findByFirstNameContainingOrLastNameContaining(String search1,String search2){
		return userRepository.findByFirstNameContainingOrLastNameContaining(search1,search2);
	}
	
	@Override
    public User updateUserPassword(User userToUpdate) {
    
    	userRepository.save(userToUpdate);
        return userToUpdate;
    }
}
