package org.birlasoft.thirdeye.beans.tco;

import java.util.ArrayList;
import java.util.List;

public class TcoCombiGraphBean {
	
	private String key;
	private boolean bar;
	private String color;
	private List<List<Object>> values = new ArrayList<>();
	private List<Object> assets = new ArrayList<>();
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public boolean isBar() {
		return bar;
	}
	public void setBar(boolean bar) {
		this.bar = bar;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public List<List<Object>> getValues() {
		return values;
	}
	public void setValues(List<List<Object>> values) {
		this.values = values;
	}
	public List<Object> getAssets() {
		return assets;
	}
	public void setAssets(List<Object> assets) {
		this.assets = assets;
	}

}
