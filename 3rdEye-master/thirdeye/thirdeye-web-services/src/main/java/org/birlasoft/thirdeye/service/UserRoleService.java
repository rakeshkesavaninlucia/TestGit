package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;

/**
 *	Service interface for user role.
 */
public interface UserRoleService {
	
	/**
	 * Save user role object. 
	 * @param userRole
	 * @return {@code UserRole}
	 */
	public UserRole save(UserRole userRole);

	/**
	 * Save list of user role in batch.
	 * @param userRoles
	 * @return {@code List<UserRole>}
	 */
	public List<UserRole> save(List<UserRole> userRoles);

	/**
	 * Find list of user role by user.
	 * @param user
	 * @return {@code List<UserRole>}
	 */
	public List<UserRole> findByUser(User user);

	/**
	 * Delete list of user role in batch.
	 * @param userRoles
	 */
	public void deleteInBatch(List<UserRole> userRoles);

	/**
	 * Create List Of New User Role Objects.
	 * @param roles
	 * @param savedUser
	 * @return {@code List<UserRole>}
	 */
	public List<UserRole> createListOfNewUserRoleObjects(String[] roles, User savedUser);
}
