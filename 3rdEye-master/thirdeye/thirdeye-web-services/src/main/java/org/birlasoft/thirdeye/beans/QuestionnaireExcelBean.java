package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;

/**
 * @author dhruv.sood
 *
 */
public class QuestionnaireExcelBean {

	private QuestionnaireAsset questionnaireAsset;
	private Question question;
	private QuestionnaireParameter questionnaireParameter;
	private String responseData;
	
	
	
	public QuestionnaireParameter getQuestionnaireParameter() {
		return questionnaireParameter;
	}
	public void setQuestionnaireParameter(QuestionnaireParameter questionnaireParameter) {
		this.questionnaireParameter = questionnaireParameter;
	}
	public QuestionnaireAsset getQuestionnaireAsset() {
		return questionnaireAsset;
	}
	public void setQuestionnaireAsset(QuestionnaireAsset questionnaireAsset) {
		this.questionnaireAsset = questionnaireAsset;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getResponseData() {
		return responseData;
	}
	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}
	
}
