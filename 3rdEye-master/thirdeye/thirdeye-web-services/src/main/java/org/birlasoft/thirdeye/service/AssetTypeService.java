package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.AssetType;

/**
 * Service interface for asset type.
 * @author samar.gupta
 */
public interface AssetTypeService {
	/**
	 * List all AssetType
	 * @return List{@code <AssetType>} Object
	 */
    public List<AssetType> findAll();
    /**
     * Only One AssetType  with given Id
     * @param id
     * @return {@code AssetType} Object
     */
    public AssetType findOne(Integer id);
    /**
     * save AssetType Object
     * @param assettype
     * @return {@code AssetType} Object
     */
    public AssetType save(AssetType assettype);
    /**
     * Extract asset type from whole list of asset classes. 
     * @param assetTypes
     * @return {@code List<AssetType>}
     */
    public List<AssetType> extractAssetTypes(AssetTypes[] assetTypes);
    /**
     * find By AssetType Name.
     * @param assetTypeName
     * @return {@code AssetType}
     */
    public AssetType findByAssetTypeName(String assetTypeName);
}
