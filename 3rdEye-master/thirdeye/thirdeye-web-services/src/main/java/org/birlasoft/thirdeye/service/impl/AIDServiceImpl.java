package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.QuestionHelper;
import org.birlasoft.thirdeye.beans.aid.AIDBlockHelper;
import org.birlasoft.thirdeye.beans.aid.AIDCentralAssetWrapperBean;
import org.birlasoft.thirdeye.beans.aid.AIDSubBlockBean;
import org.birlasoft.thirdeye.beans.aid.AidSearchCentralAssetWrapperBean;
import org.birlasoft.thirdeye.beans.aid.AidSearchWrapper;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlockConfig;
import org.birlasoft.thirdeye.beans.aid.JSONAIDSubBlockConfig;
import org.birlasoft.thirdeye.beans.aid.RelatedAssetSearchBean;
import org.birlasoft.thirdeye.beans.aid.RelationshipAssetSearchDataBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.constant.aid.AIDBlockType;
import org.birlasoft.thirdeye.constant.aid.AIDTemplate;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.interfaces.SubBlockUser;
import org.birlasoft.thirdeye.repositories.AIDBlockRepository;
import org.birlasoft.thirdeye.repositories.AIDRepository;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetParentChildBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexRelationshipAssetDataBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ScatterGraphService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implementation class for Aid Service.
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AIDServiceImpl implements AIDService {
	private static Logger logger = LoggerFactory.getLogger(AIDServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	@Autowired
	AIDRepository aidRepository;
	
	@Autowired
	AIDBlockRepository aidBlockRepository;
	
	@Autowired
	QuestionService questionService;
	
	@Autowired
	ParameterService paramService;
	
	@Autowired 
	QuestionnaireService questionnaireService;
	
	@Autowired
	AssetTemplateColumnService templateColumnService;

	@Autowired
	AssetService assetService;

	@Autowired
	ScatterGraphService scatterService;

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Override
	public Aid fetchOne(Integer aidReportId, boolean fullyLoaded) {
		Aid toReturn = aidRepository.findOne(aidReportId);
		if (fullyLoaded){
			List<Integer> listOfAssetId = new ArrayList<>();
			
			for (Asset a : toReturn.getAssetTemplate().getAssets()){
				listOfAssetId.add(a.getId());
			}
			
			if(!listOfAssetId.isEmpty())
				assetRepository.findMany(listOfAssetId);
		}
		
		return toReturn;
	}
	
	@Override
	public void addBlockToReport(Aid oneAid, AIDBlockType blockType) {
		AidBlock newBlock = new AidBlock();
		newBlock.setAid(oneAid);
		newBlock.setAidBlockType(blockType.name());
		newBlock.setSequenceNumber(oneAid.getAidBlocks().size() +1);
		newBlock.setBlockJsonconfig("");
		
		aidBlockRepository.save(newBlock);
	}
	
	
	@Override
	public AidBlock fetchOneBlock(Integer blockId, boolean loaded) {
		AidBlock oneBlock = aidBlockRepository.findOne(blockId);
		
		if (loaded) { 
			oneBlock.getAid().getName();
		}
				
		return oneBlock;
	}
	
	@Override
	public void saveBlocks(List<AidBlock> asList) {
		aidBlockRepository.save(asList);
	}
	
	@Override
	public void loadSubBlocksConfigurationStrings (List<? extends SubBlockUser> subBlockUsers){
		// Load it in one block at a time
		for (SubBlockUser oneSubBlockUser : subBlockUsers){
			for (JSONAIDSubBlockConfig oneSubBlock : oneSubBlockUser.getListOfBlocks()){
				setSubBlockVariable(oneSubBlock);
			}
			
		}
	}

	/**
	 * Set SubBlock Variable.
	 * @param oneSubBlock
	 */
	private void setSubBlockVariable(JSONAIDSubBlockConfig oneSubBlock) {
		if(oneSubBlock.usesParameter()){
			Parameter p = paramService.findOne(oneSubBlock.getParamId());
			oneSubBlock.setParameterString(p.getDisplayName());
		} else if (oneSubBlock.usesQuestion()){
			Questionnaire qe = questionnaireService.findOne(oneSubBlock.getQuestionQEId());
			Question q = questionService.findOne(oneSubBlock.getQuestionId());
			for (QuestionnaireQuestion qq : q.getQuestionnaireQuestions()){
				if (qq.getQuestionnaire().equals(qe)){
					oneSubBlock.setQuestionString(q.getTitle());
					break;
				}
			}
		} else if (oneSubBlock.usesTemplateColumn()){
			AssetTemplateColumn oneCol = templateColumnService.findOne(oneSubBlock.getTemplateColumnId());
			oneSubBlock.setTemplateColumnString(oneCol.getAssetTemplateColName());
		}
	}
	
	@Override
	public void deleteBlocks(List<AidBlock> listOfBlocksForDelete) {
		aidBlockRepository.delete(listOfBlocksForDelete);
	}
	
	private void setQuestionResponse(Asset oneAsset, JSONAIDSubBlockConfig oneSubBlock) {
		Questionnaire qe = questionnaireService.findOne(oneSubBlock.getQuestionQEId());
		Question q = questionService.findOne(oneSubBlock.getQuestionId());
		
		// Load the qq - we can optimize this alot more by firing a query 
		q.getQuestionnaireQuestions().size();
		for (QuestionnaireQuestion qq : q.getQuestionnaireQuestions()){
			// Find the QQ which is in the same questionnaire and the asset 
			// Here we dont care which param this is for
			if (qq.getQuestionnaire().equals(qe) && qq.getQuestionnaireAsset().getAsset().equals(oneAsset)){
				
				// Get the response(s) which are available and then 
				// convert to Strings
				oneSubBlock.setQuestionString(QuestionHelper.fetchResponseAsString(qq.getResponseDatas(), QuestionType.valueOf(q.getQuestionType())));
				break;
			}
		}
	}


	@Override
	public Aid save(Aid aid) {
		return aidRepository.save(aid);
	}

	@Override
	public Aid createNewAidObject(Aid newAid, User currentUser, Workspace workspace, AssetTemplate assetTemplate) {
		newAid.setUserByCreatedBy(currentUser);
		newAid.setUserByUpdatedBy(currentUser);
		newAid.setCreatedDate(new Date());
		newAid.setUpdatedDate(new Date());
		newAid.setWorkspace(workspace);
		newAid.setAssetTemplate(assetTemplate);
		newAid.setAidTemplate(AIDTemplate.DEFAULT.toString());
		return newAid;
	}

	@Override
	public List<Aid> findByWorkspaceIn(Set<Workspace> workspaces) {
		return aidRepository.findByWorkspaceIn(workspaces);
	}
	
	@Override
	public void loadSubBlockResponseStrings(List<? extends SubBlockUser> subBlockUsers, Asset oneAsset , Map<Integer, BigDecimal> paramEvalMap) {

		// Load it in one block at a time
		for (SubBlockUser oneSubBlockUser : subBlockUsers){
			for (JSONAIDSubBlockConfig oneSubBlock : oneSubBlockUser.getListOfBlocks()){
				if(oneSubBlock.usesParameter()){
						setParameterValue(oneSubBlock, paramEvalMap.get(oneSubBlock.getParamId()));

				} else if (oneSubBlock.usesQuestion()){
					setQuestionResponse(oneAsset, oneSubBlock);
				} else if (oneSubBlock.usesTemplateColumn()){
					setColumnFromInventory(oneAsset, oneSubBlock);
				}
			}
			
		}
	}
	
	
	@Override
	public Map<Integer, Map<Integer, BigDecimal>> prepareAssetParameterMap(Aid aidInDisplay, Asset asset) {
		
		Set<AssetBean> assets = new HashSet<>();
		assets.add(new AssetBean(asset));
		
		Set<AIDSubBlockBean> setOfSubBlock = fetchSubBlockConfig(aidInDisplay);
		
		// Now that we have the parameter and asset list. Quickly evaluate it
		return evaluateParameters(assets, setOfSubBlock);
	}
	
	
	private Set<AIDSubBlockBean> fetchSubBlockConfig(Aid aidInDisplay) {
		Set<AIDSubBlockBean> required = new HashSet<>();
		for (AidBlock oneBlock : aidInDisplay.getAidBlocks()){
			
			JSONAIDBlockConfig oneConfig =  AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock);
			
			for (JSONAIDSubBlockConfig oneSubBlock : oneConfig.getListOfBlocks()){
				if(oneSubBlock.usesParameter()){
					AIDSubBlockBean block = new AIDSubBlockBean();
					block.setParameterId(oneSubBlock.getParamId());
					block.setQuestionaireId(oneSubBlock.getParamQEId());
					required.add(block);
				}
			}
		}
		
		return required;
	}	
	
	private Map<Integer,Map<Integer, BigDecimal>> evaluateParameters(Set<AssetBean> assetList, Set<AIDSubBlockBean> setOfSubBlock) {
		
		/*This is a map of Asset Id, Parameter id and the value. This is specifically for the
		easy use for the AID block which require the information for all the parameters in an asset*/
		Map<Integer,Map<Integer, BigDecimal>> paramMappedById = new HashMap<>();
		
		try {
			List<ParameterEvaluationResponse> responses = new ArrayList<>();
			for (AIDSubBlockBean oneBlock : setOfSubBlock) {
				Set<Integer> setOfParameters = new HashSet<>();
				setOfParameters.add(oneBlock.getParameterId());
				responses.addAll(paramService.evaluateParameters(setOfParameters, oneBlock.getQuestionaireId(), assetList));
			} 
					
			for (ParameterEvaluationResponse oneParamSeries : responses){
				for(Integer assetID : oneParamSeries.fetchAssetMappedByAssetId().keySet()){
					Map<Integer, BigDecimal> innerParamMap = setParamMap(
							paramMappedById, assetID);
					
					innerParamMap.put(oneParamSeries.getParameterBean().getId(), oneParamSeries.fetchAssetMappedByAssetId().get(assetID));
				}
			}
			
		} catch (Exception e){
			logger.error("Unable to evaluate parameter for assets",e);
		}
				
		return paramMappedById;
	}

	private Map<Integer, BigDecimal> setParamMap(
			Map<Integer, Map<Integer, BigDecimal>> paramMappedById,
			Integer assetID) {
		Map<Integer, BigDecimal> innerParamMap ;
		if (paramMappedById.containsKey(assetID)){
			innerParamMap = paramMappedById.get(assetID);
		} else {
			innerParamMap = new HashMap<>();
			paramMappedById.put(assetID, innerParamMap);
		}
		return innerParamMap;
	}

	private void setParameterValue(JSONAIDSubBlockConfig oneSubBlock, BigDecimal paramValue) {
		
		String parameterString = "N/A";
		
		if (paramValue != null){
			parameterString = String.valueOf(paramValue);
		}
				
		oneSubBlock.setParameterString(parameterString);
	}

	private void setColumnFromInventory(Asset oneAsset, JSONAIDSubBlockConfig oneSubBlock) {
		Asset assetReload = assetService.findOne(oneAsset.getId());
		
		for (AssetData onePieceOfData : assetReload.getAssetDatas()){
			if (onePieceOfData.getAssetTemplateColumn().getId().equals(oneSubBlock.getTemplateColumnId())){
				oneSubBlock.setTemplateColumnString(onePieceOfData.getData());
				break;
			}
		}
	}

	@Override
	public AIDCentralAssetWrapperBean getCentralAssetForDisplay(Aid oneAid, Asset oneAsset, Map<Integer, BigDecimal> paramEvalMap) {
		AIDCentralAssetWrapperBean oneWrapper = new AIDCentralAssetWrapperBean();
		oneWrapper.setCentralAssetBean(new AssetBean(oneAsset));
		oneWrapper.setId(oneAid.getId());
		
		for (AidBlock oneBlock : oneAid.getAidBlocks()){
			oneWrapper.addBlock(AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock));
		}		
		this.loadSubBlockResponseStrings(oneWrapper.getListOfBlocksInMainApp(), oneAsset , paramEvalMap);
		oneWrapper.prepareForDisplay();
	
		return oneWrapper;
	}

	@Override
	public AidSearchWrapper getAssetAidFromElasticsearch(Integer assetId, Integer aidId) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetAid";
		
		 AidSearchWrapper aidSearchWrapper = new AidSearchWrapper();

		RestTemplate restTemplate = new RestTemplate();

		SearchConfig searchConfig = setSearchConfig(assetId, aidId);
		
		IndexAssetBean indexAssetBean = new IndexAssetBean();
		//Firing a rest request to elastic search server
		try {
			indexAssetBean = restTemplate.postForObject(uri, searchConfig, IndexAssetBean.class);
		} catch (RestClientException e) {				
			logger.error("Exception occured in AIDServiceImpl :: getAssetAidFromElasticsearch() :", e);
		}
		
		
		IndexAssetParentChildBean relationship = indexAssetBean.getRelationship();
		List<RelatedAssetSearchBean> listOfInboundAssets = new ArrayList<>();
		List<RelatedAssetSearchBean> listOfOutboundAssets = new ArrayList<>();
		if(relationship != null){
			List<IndexRelationshipAssetBean> parents = relationship.getParents();
			for (IndexRelationshipAssetBean indexRelationshipAssetBean : parents) {
				RelatedAssetSearchBean relatedAssetSearchBean = extractIndexRelationshipAssetBean(indexRelationshipAssetBean);
				listOfInboundAssets.add(relatedAssetSearchBean);
			}
			
			List<IndexRelationshipAssetBean> children = relationship.getChildren();
			for (IndexRelationshipAssetBean indexRelationshipAssetBean : children) {
				RelatedAssetSearchBean relatedAssetSearchBean = extractIndexRelationshipAssetBean(indexRelationshipAssetBean);
				listOfOutboundAssets.add(relatedAssetSearchBean);
			}
		}
		
		aidSearchWrapper.setCentralAsset(getAidSearchCentralAssetWrapperBean(indexAssetBean, aidId));
		aidSearchWrapper.setListOfInboundAssets(listOfInboundAssets);
		aidSearchWrapper.setListOfOutboundAssets(listOfOutboundAssets);
		
		return aidSearchWrapper;
	}

	private AidSearchCentralAssetWrapperBean getAidSearchCentralAssetWrapperBean(IndexAssetBean indexAssetBean, Integer aidId) {
		AidSearchCentralAssetWrapperBean aidSearchCentralAssetWrapperBean = new AidSearchCentralAssetWrapperBean();
		aidSearchCentralAssetWrapperBean.setCentralAssetBean(indexAssetBean.getName());
		aidSearchCentralAssetWrapperBean.setId(indexAssetBean.getId());
		
		List<IndexAidBean> listOfAidBeans = indexAssetBean.getAids();
		for (IndexAidBean indexAidBean : listOfAidBeans) {
			if(indexAidBean.getId().equals(aidId)){
				List<IndexAidBlockBean> listOfAidBlockBeans = indexAidBean.getBlocks();
				for (IndexAidBlockBean indexAidBlockBean : listOfAidBlockBeans) {
					aidSearchCentralAssetWrapperBean.addBlock(AIDBlockHelper.getAIDBlockBeanFromIndexAidBlockBean(indexAidBlockBean));
				}
			}
		}
		aidSearchCentralAssetWrapperBean.prepareForDisplay();
		return aidSearchCentralAssetWrapperBean;
	}
	
	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return {@code SearchConfig}
	 */
	private SearchConfig setSearchConfig(Integer assetId, Integer aidId) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		
		searchConfig.setAssetId(assetId);
		searchConfig.setAidId(aidId);
		return searchConfig;
	}
	
	private RelatedAssetSearchBean extractIndexRelationshipAssetBean(IndexRelationshipAssetBean indexRelationshipAssetBean) {
		RelatedAssetSearchBean relatedAssetSearchBean = new RelatedAssetSearchBean();
		relatedAssetSearchBean.setAssetId(indexRelationshipAssetBean.getAssetId());
		relatedAssetSearchBean.setAsset(indexRelationshipAssetBean.getAssetName());
		relatedAssetSearchBean.setAssetStyle(indexRelationshipAssetBean.getAssetStyle());
		List<RelationshipAssetSearchDataBean> listOfRelationshipAssetData = new ArrayList<>();
		List<IndexRelationshipAssetDataBean> listOfIndexRelationshipAssetDataBeans = indexRelationshipAssetBean.getRelationshipAssetData();
		for (IndexRelationshipAssetDataBean indexRelationshipAssetDataBean : listOfIndexRelationshipAssetDataBeans) {
			RelationshipAssetSearchDataBean relationshipAssetSearchDataBean  = new RelationshipAssetSearchDataBean();
			relationshipAssetSearchDataBean.setDisplayName(indexRelationshipAssetDataBean.getRelationshipName());
			relationshipAssetSearchDataBean.setDataType(indexRelationshipAssetDataBean.getDataType());
			relationshipAssetSearchDataBean.setFrequency(indexRelationshipAssetDataBean.getFrequency());
			relationshipAssetSearchDataBean.setRelationshipTypeDisplayName(indexRelationshipAssetDataBean.getRelationshipDisplayName());
			relationshipAssetSearchDataBean.setDirection(indexRelationshipAssetDataBean.getDirection());
			listOfRelationshipAssetData.add(relationshipAssetSearchDataBean);
		}
		relatedAssetSearchBean.setListOfRelationshipAssetData(listOfRelationshipAssetData);
		return relatedAssetSearchBean;
	}


	@Override
	public Set<Integer> getAssetIdsFromElasticSearch(Integer aidId, Map<String, List<String>> filterMap) {

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetAidId";

		RestTemplate restTemplate = new RestTemplate();
		Set<Integer> aidAssetIds = new HashSet<>();
		Set<Integer> assetids = new HashSet<>();
		ObjectMapper mapper = new ObjectMapper();

		SearchConfig searchConfig = setSearchConfigForAsset(aidId, filterMap);

		try {
			aidAssetIds = restTemplate.postForObject(uri, searchConfig, Set.class);
			assetids = mapper.convertValue(aidAssetIds, new TypeReference<Set<Integer>>() {
			});

		} catch (RestClientException e) {
			logger.error("Exception occured in AIDServiceImpl :: getAssetAidFromElasticsearch() :", e);
		}

		return assetids;
	}

	private SearchConfig setSearchConfigForAsset(Integer aidId, Map<String, List<String>> filterMap) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig = new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String> list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		searchConfig.setFilterMap(filterMap);
		searchConfig.setAidId(aidId);

		return searchConfig;
	}

	@Override
	public List<Aid> findByAssetTemplate(AssetTemplate assetTemplate) {

		return aidRepository.findByAssetTemplate(assetTemplate);
	}

	
}
