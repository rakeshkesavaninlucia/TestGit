package org.birlasoft.thirdeye.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.beans.fr.FunctionalRedundancyWrapper;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * 
 * Service class for functional map
 *
 */
public interface FunctionalMapService {

	/**
	 * Method to create functional map
	 * @param functionalMapBean
	 * @param currentUser
	 * @return
	 */
	public FunctionalMap createFunctionalMapObject(FunctionalMapBean functionalMapBean, User currentUser);

	/**
	 * Method to save functional map
	 * @param fm
	 * @return
	 */
	public FunctionalMap save(FunctionalMap fm);

	/**
	 * Method to find functional map by workspace
	 * @param activeWorkSpaceForUser
	 * @param loaded
	 * @return
	 */
	public List<FunctionalMap> findByWorkspace(Workspace activeWorkSpaceForUser, boolean loaded);

	/**
	 * Method to find functional map based on id
	 * @param idOfFunctionalMap
	 * @param loaded
	 * @return
	 */
	public FunctionalMap findOne(Integer idOfFunctionalMap, boolean loaded);

	/**
	 * Method to update functional map 
	 * @param functionalMapBean
	 * @param currentUser
	 * @return
	 */
	public FunctionalMap updateFunctionalMapObject(FunctionalMapBean functionalMapBean, User currentUser);
	
	/**
	 * Method to export functional map template
	 * @param response
	 * @param fm
	 */
	public void exportFmTemplate(HttpServletResponse response, FunctionalMap fm);	

	/**
	 * Method to import functional map template
	 * @param workbook
	 * @param fm
	 * @param listOfErrorCells
	 * @param questionnaireId
	 */
	public void importFunctionalMapExcel(Workbook workbook, FunctionalMap fm,List<ExcelCellBean> listOfErrorCells, Integer questionnaireId);

	/**
	 * Method to get functional map of level types
	 * @param functionalMap
	 * @return
	 */
	public Map<Integer, String> getMapOfLevelTypes(FunctionalMap functionalMap);

	/**
	 * Method to fetch functional map data by bcm level
	 * @param bcmLevel
	 * @return
	 */
	public List<FunctionalMapDataBean> fetchFunctionalMapDataByBcmLevel(BcmLevel bcmLevel);
	
	/**
	 * Method to get map of functional map by bcm level for active workspace
	 * @param activeWorkspace
	 * @return
	 */
	public Map<Bcm, List<FunctionalMap>> getMapOfFunctionalMapsByBcmForActiveWorkspace(Workspace activeWorkspace);
	/**
	 * create set Of AssetBeans
	 * @param functionalMapDataBeans
	 * @return {@code Set<AssetBean>}
	 */
	public Set<AssetBean> createSetOfAssetBeans(List<FunctionalMapDataBean> functionalMapDataBeans);
	/**
	 * Get level type by level number
	 * @param levelType
	 * @param levelNumber
	 * @return {@code int}
	 */
	public String getLevelNumberValue(int levelNumber);
	/**
	 * Get functional redundancy wrapper to view FR report.
	 * @param bcmLevel
	 * @param fcParameter
	 * @param questionnaire
	 * @param mode
	 * @param filterMap
	 * @return {@code FunctionalRedundancyWrapper}
	 */
	public FunctionalRedundancyWrapper getFunctionalRedundancyWrapper(BcmLevel bcmLevel, Parameter fcParameter, Questionnaire questionnaire, String mode, Map<String, List<String>> filterMap);
	
	/**
	 * Method to get list of functional map based of workspace and levelTypes
	 * @param workspace
	 * @param levelTypes
	 * @param loaded
	 * @return
	 */
	public List<FunctionalMap> findByWorkspaceAndTypeNotIn(Workspace workspace, Set<String> levelTypes, boolean loaded);

	/**
	 * Method to get bcm response at a level
	 * @param bcmLevel
	 * @param fcParameter
	 * @param value
	 * @param questionnaire
	 * @return
	 */
	public List<BcmResponseBean> getBcmResponsesAtLevel(BcmLevel bcmLevel,Parameter fcParameter, Integer value, Questionnaire questionnaire);

	/**
	 * <ethod to set excel errors
	 * @param listOfErrorCells
	 * @param errors
	 * @param cell
	 */
	public void setExcelErrors(List<ExcelCellBean> listOfErrorCells,List<String> errors, Cell cell);

	/**
	 * Method to export log record based on hash value
	 * @param workbook
	 * @return
	 */
	public ExportLog getExportLogByHashValue(Workbook workbook);

	/**
	 * Method to validate date 
	 * @param timeDown
	 * @param updatedDate
	 * @return
	 */
	public boolean validateDate(Date timeDown, Date updatedDate);

	/**
	 * Method to get response data
	 * @param queTypeText
	 * @param stringCellValue
	 * @return
	 */
	public String getResponseData(String queTypeText, String stringCellValue);
	/**
	 * method to get functional map data list
	 * @param fm
	 * @param questionnaire
	 * @return {@code List<FunctionalMapDataBean>}
	 */
	public List<FunctionalMapDataBean> fetchFunctionalMapData(FunctionalMap fm, Questionnaire questionnaire);
	/**
	 * Method to get response data for benchmark MCQ
	 * @param queTypeText
	 * @param stringCellValue
	 * @return
	 */
	public String getResponseDataForBenchmarkMCQ(String queTypeText, String stringCellValue);
	
}
