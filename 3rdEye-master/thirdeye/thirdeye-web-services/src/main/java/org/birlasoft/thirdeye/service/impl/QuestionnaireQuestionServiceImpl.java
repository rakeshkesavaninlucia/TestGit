package org.birlasoft.thirdeye.service.impl;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.MandatoryQuestionBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.repositories.ParameterConfigRepository;
import org.birlasoft.thirdeye.repositories.QuestionnaireParameterRepository;
import org.birlasoft.thirdeye.repositories.QuestionnaireQuestionRepository;
import org.birlasoft.thirdeye.repositories.ResponseDataRepository;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service class for Questionnaire Question. 
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireQuestionServiceImpl implements QuestionnaireQuestionService {
	@Autowired
	private QuestionnaireQuestionRepository questionnaireQuestionRepository;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private ResponseDataRepository responseDataRepository;
	@Autowired
	private ParameterFunctionService parameterFunctionService;
	@Autowired
	private ResponseService responseService; 
	@Autowired
	private QuestionnaireParameterRepository questionnaireParameterRepository;
	@Autowired
	private ParameterConfigRepository parameterConfigRepository;
	@Autowired
	private QuestionnaireAssetService qaService;
	@Autowired
	private QuestionnaireParameterService qpService;
	@Autowired
	private AssetRepository assetRepository;


	@Override
	public List<QuestionnaireQuestion> findByQuestionnaire(Questionnaire questionnaire, boolean loaded){
		if(loaded){
			List<QuestionnaireQuestion> listOfQQ = questionnaireQuestionRepository.findByQuestionnaireLoaded(questionnaire);
			loadAssetsForQQ(listOfQQ);		
			for (QuestionnaireQuestion oneQQ : listOfQQ){
				oneQQ.getQuestion().getTitle();
				oneQQ.getQuestion().getQuestionType();
				oneQQ.getQuestionnaireAsset().getAsset();
				oneQQ.getQuestionnaireParameter().getParameterByParameterId().getUniqueName();
				oneQQ.getResponseDatas().size();
			}
			return listOfQQ;
		} else {
			return questionnaireQuestionRepository.findByQuestionnaire(questionnaire);
		}

	}

	@Override
	public void deleteInBatch(Collection<QuestionnaireQuestion> listOfQuestionnaireQuestion) {
		// delete all associated response data first
		for (QuestionnaireQuestion oneQQ : listOfQuestionnaireQuestion){
			responseDataRepository.deleteInBatch(oneQQ.getResponseDatas());
		}		
		questionnaireQuestionRepository.deleteInBatch(listOfQuestionnaireQuestion);
	}

	/**
	 * method to save list Of QuestionnaireQuestion
	 * @param listOfQuestionnaireQuestion
	 * @return {@code List<QuestionnaireQuestion>}
	 */

	@Override
	public List<QuestionnaireQuestion> save(List<QuestionnaireQuestion> listOfQuestionnaireQuestion) {
		return questionnaireQuestionRepository.save(listOfQuestionnaireQuestion);
	}

	@Override
	public void generateNewQuestionnaireQuestions(Questionnaire qe) {
		// Generate new QQ based on the basic logic
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(qe);

		List<QuestionBean> allQuestionsToDisplay = new ArrayList<>();
		for (ParameterBean oneRootParameter : listOfRootParameters){
			allQuestionsToDisplay.addAll(oneRootParameter.fetchListOfAllQuestionsForParameter());
		}
		// Load the questionnaire with the assets
		List<QuestionnaireQuestion> qqListForSaving = new ArrayList<>();
		int sequenceNumber = 1;
		List<QuestionnaireAsset> allAssetsToBeShown = getListOfAssetsInQuestionnaire(qe);
		for (QuestionnaireAsset oneAsset : allAssetsToBeShown){
			for (QuestionBean oneQuestion : allQuestionsToDisplay){
				QuestionnaireQuestion newQQ = new QuestionnaireQuestion();				
				newQQ.setQuestionnaireAsset(oneAsset);
				newQQ.setQuestion(questionService.findOne(oneQuestion.getId()));
				Integer parentParameterId = oneQuestion.getParentParameter().getParent().getId();
				Integer parameterId = oneQuestion.getParentParameter().getId();				
				Parameter param = parameterService.findOne(parameterId);
				Parameter parentParam = parameterService.findOne(parentParameterId);
				newQQ.setQuestionnaireParameter(questionnaireParameterRepository.findByQuestionnaireAndParameterByParameterIdAndParameterByParentParameterId(qe,param,parentParam));
				newQQ.setQuestionnaire(qe);
				newQQ.setSequenceNumber(sequenceNumber);

				qqListForSaving.add(newQQ);

				sequenceNumber ++;
			}
		}

		this.save(qqListForSaving);
	}

	/**
	 * This method will simply refresh the QQ on the QE. This is a non destructive method that will maintain the responses 
	 * for the existing QQ and resolve the new QQ to be added and then make the insert.
	 * 
	 * It will make the entry only if there are valid params and assets on the questionnaire.
	 * 
	 * @param qe
	 */
	@Override
	public void refreshQuestionnaireQuestions(Questionnaire qe){
		// If there are Q_A and Q_P refresh the QQ ->
		if (qe.getQuestionnaireAssets() != null && ! qe.getQuestionnaireAssets().isEmpty() && 
				qe.getQuestionnaireParameters() != null && ! qe.getQuestionnaireParameters().isEmpty()){
			// You dont have to worry about the removal of QQ now.			
			// Find out the QA and QP for which there is no QQ entry			
			// Insert the new QQ into the table			
			// Done!
			Map<QuestionnaireParameter, List<ParameterFunction>> mapOfQpAndPf = new HashMap<>();
			Set<QuestionnaireParameter> setOfQP = qe.getQuestionnaireParameters();
			Set<QuestionnaireParameter> newSetOfLeafQpOnQe = new HashSet<>();

			for (QuestionnaireParameter questionnaireParameter : setOfQP) {
				List<ParameterFunction> listOfPf = parameterFunctionService.findByParameterByParentParameterIdAndParameterByChildparameterIdIsNull(questionnaireParameter.getParameterByParameterId());
				mapOfQpAndPf.put(questionnaireParameter, listOfPf);
				if (listOfPf != null && !listOfPf.isEmpty()){
					newSetOfLeafQpOnQe.add(questionnaireParameter);
				}
			}

			List<QuestionnaireQuestion> listOfQq = this.findByQuestionnaire(qe, true);
			Set<QuestionnaireParameter> setOfQpAlreadyInQq = new HashSet<>();
			Set<QuestionnaireAsset> setOfQaAlreadyInQq = new HashSet<>();
			int maxSequenceNumber = getMaxSequenceNumber(listOfQq,setOfQpAlreadyInQq, setOfQaAlreadyInQq);

			newSetOfLeafQpOnQe.removeAll(setOfQpAlreadyInQq);

			Set<QuestionnaireAsset> setOfQaOnQe = qe.getQuestionnaireAssets();
			Set<QuestionnaireAsset> newSetOfQaOnQe = new HashSet<>();
			newSetOfQaOnQe.addAll(setOfQaOnQe);
			newSetOfQaOnQe.removeAll(setOfQaAlreadyInQq);

			List<QuestionnaireQuestion> qqListForSaving = new ArrayList<>();
			if (!newSetOfQaOnQe.isEmpty()){
				maxSequenceNumber = generateListOfNewQQ(qe, mapOfQpAndPf,
						mapOfQpAndPf.keySet(), newSetOfQaOnQe,
						maxSequenceNumber, qqListForSaving);
			}

			if (!newSetOfLeafQpOnQe.isEmpty()){
				generateListOfNewQQ(qe, mapOfQpAndPf,newSetOfLeafQpOnQe, setOfQaAlreadyInQq,maxSequenceNumber, qqListForSaving);
			}

			this.save(qqListForSaving);

		}
	}

	private int getMaxSequenceNumber(List<QuestionnaireQuestion> listOfQq,
			Set<QuestionnaireParameter> setOfQpAlreadyInQq,
			Set<QuestionnaireAsset> setOfQaAlreadyInQq) {
		int maxSequenceNumber = 0;
		for (QuestionnaireQuestion questionnaireQuestion : listOfQq) {
			setOfQpAlreadyInQq.add(questionnaireQuestion.getQuestionnaireParameter());
			setOfQaAlreadyInQq.add(questionnaireQuestion.getQuestionnaireAsset());
			if (questionnaireQuestion.getSequenceNumber().intValue() > maxSequenceNumber){
				maxSequenceNumber = questionnaireQuestion.getSequenceNumber().intValue();
			}
		}
		maxSequenceNumber++;
		return maxSequenceNumber;
	}

	/**
	 * method to generate List Of New QuestionnaireQuestion
	 * @param qe
	 * @param mapOfQpAndPf
	 * @param setOfLeafQp
	 * @param setOfQa
	 * @param sequenceNumber
	 * @param qqListForSaving
	 * @return
	 */
	private int generateListOfNewQQ( Questionnaire qe, Map<QuestionnaireParameter,
			List<ParameterFunction>> mapOfQpAndPf, Set<QuestionnaireParameter> setOfLeafQp,
			Set<QuestionnaireAsset> setOfQa, int sequenceNumber, List<QuestionnaireQuestion> qqListForSaving) {
		int seqNumber = sequenceNumber;
		for (QuestionnaireAsset questionnaireAsset : setOfQa) {
			for (QuestionnaireParameter questionnaireParameter : setOfLeafQp) {
				for(ParameterFunction onePf : mapOfQpAndPf.get(questionnaireParameter)){
					QuestionnaireQuestion newQQ = new QuestionnaireQuestion();					
					newQQ.setQuestionnaireAsset(questionnaireAsset);
					newQQ.setQuestion(onePf.getQuestion());
					newQQ.setQuestionnaireParameter(questionnaireParameter);
					newQQ.setQuestionnaire(qe);					
					newQQ.setSequenceNumber(seqNumber);
					qqListForSaving.add(newQQ);
					seqNumber ++;
				}
			}
		}
		return seqNumber;
	}

	/**
	 * method to remove assets from Questionnaire
	 * @param assetsForRemoval
	 */
	@Override
	public void handleRemovalOfAssetsFromQE(Set<QuestionnaireAsset> assetsForRemoval) {
		if (assetsForRemoval != null && !assetsForRemoval.isEmpty()){
			// Find the QQ that need to be deleted 
			Set<QuestionnaireQuestion> qqAlreadyPresent = questionnaireQuestionRepository.findByQuestionnaireAssetIn(assetsForRemoval);

			// Remove the QQ for the assets (basically everything)
			this.deleteInBatch(qqAlreadyPresent);

			// Remove the QA for the assets (basically everything)
			qaService.deleteInBatch(assetsForRemoval);
		}
	}

	/**
	 * method to remove Of Params From Questionnaire
	 * @param paramsForRemoval
	 */
	@Override
	public void handleRemovalOfParamsFromQE(Set<QuestionnaireParameter> paramsForRemoval) {
		if (paramsForRemoval != null && !paramsForRemoval.isEmpty()){
			// Find the QQ that need to be deleted 
			Set<QuestionnaireQuestion> qqAlreadyPresent = questionnaireQuestionRepository.findByQuestionnaireParameterIn(paramsForRemoval);

			// Remove the QQ for the parameter (basically everything)
			this.deleteInBatch(qqAlreadyPresent);

			// Remove the QP for the parameter (basically everything)
			qpService.deleteInBatch(paramsForRemoval);
		}
	}

	/**
	 * method to prepare list Of Questions For Questionnaire
	 * @param qe
	 * @return {@code List<QuestionnaireQuestionBean> }
	 */
	@Override
	public List<QuestionnaireQuestionBean> prepareListOfQuestionsForQuestionnaire(Questionnaire qe) {

		List<QuestionnaireQuestion> questionnaireQuestions = this.findByQuestionnaire(qe, true);

		List<QuestionnaireQuestionBean> qqbeanListForGUI = new ArrayList<>();
		for (QuestionnaireQuestion qq : questionnaireQuestions){
			qqbeanListForGUI.add(new QuestionnaireQuestionBean(qq));
		}
		Collections.sort(qqbeanListForGUI, new SequenceNumberComparator());
		return qqbeanListForGUI;
	}

	/**
	 * method to get List Of Assets from Questionnaire
	 * @param qe
	 * @return {@code List<QuestionnaireAsset> }
	 */
	private List<QuestionnaireAsset> getListOfAssetsInQuestionnaire(Questionnaire qe) {
		List<QuestionnaireAsset> listOfAssetsToBeShown = new ArrayList<>();

		if(qe == null){
			return listOfAssetsToBeShown;
		}

		Set<QuestionnaireAsset> listOfQuestionnaireAssets = qe.getQuestionnaireAssets();
		for (QuestionnaireAsset qa : listOfQuestionnaireAssets){
			listOfAssetsToBeShown.add(qa);
		}
		return listOfAssetsToBeShown;
	}

	/**
	 * find QuestionnaireQuestion by id
	 * @param idOfQQ
	 * @param b
	 * @return {@code QuestionnaireQuestion }
	 */
	@Override
	public QuestionnaireQuestion findOne(Integer idOfQQ, boolean b) {
		QuestionnaireQuestion qq = questionnaireQuestionRepository.findOne(idOfQQ);
		if (qq != null && b ){
			qq.getQuestion().getTitle();
			qq.getQuestionnaire().getId();
			qq.getQuestionnaireAsset().getAsset();
			qq.getQuestionnaireAsset().getAsset().getId();
			qq.getQuestionnaireParameter().getParameterByParameterId();
			qq.getQuestionnaireParameter().getParameterByParameterId().getDisplayName();
		}
		return qq;
	}

	/**
	 * method to find list of QuestionnaireQuestion By Parameter, Question And Questionnaire
	 * @param parameterBean
	 * @param questionBean
	 * @param questionnaire
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	@Override
	public List<QuestionnaireQuestion> findByParameterAndQuestionAndQuestionnaire(ParameterBean parameterBean, QuestionBean questionBean, Questionnaire questionnaire) {
		List<QuestionnaireQuestion> listToReturn = new ArrayList<>();
		if (questionnaire == null && parameterBean != null && questionBean != null) {
			List<QuestionnaireParameter> qpList = questionnaireParameterRepository.findByQuestionnaireAndParameterByParameterId(questionnaire, parameterService.findOne(parameterBean.getId()));
			for(QuestionnaireParameter qp : qpList){				
				listToReturn.addAll(questionnaireQuestionRepository.findByQuestionnaireParameterAndQuestionLoaded(qp,questionService.findOne(questionBean.getId())));
			}
		} else if (parameterBean == null) {
			listToReturn = questionnaireQuestionRepository.findByQuestionAndQuestionnaireLoaded(questionService.findOne(questionBean.getId()),questionnaire);
		}else{
			List<QuestionnaireParameter> qpList = questionnaireParameterRepository.findByQuestionnaireAndParameterByParameterId(questionnaire, parameterService.findOne(parameterBean.getId()));
			for(QuestionnaireParameter qp : qpList){
				if(questionBean != null)
					listToReturn.addAll(questionnaireQuestionRepository.findByQuestionnaireParameterAndQuestionAndQuestionnaireLoaded(qp, questionService.findOne(questionBean.getId()),questionnaire));
			}
		}
		// Now that we have a list of QQ, next is to pull out all the assets.
		// Doing this so that we can load all assets in one shot.
		loadAssetsForQQ(listToReturn);		
		// Load all the necessary child fields of the QQs
		for (QuestionnaireQuestion oneQQ : listToReturn){
			oneQQ.getQuestion().getTitle();
			oneQQ.getQuestionnaireAsset().getAsset();
			oneQQ.getQuestionnaireParameter().getParameterByParameterId().getUniqueName();
			oneQQ.getResponseDatas().size();
		}		
		return listToReturn;
	}

	/**
	 * method to load asset for QuestionnaireQuestion
	 * @param listToReturn
	 */
	private void loadAssetsForQQ(List<QuestionnaireQuestion> listToReturn) {
		Set<Integer> assetsToPull = new HashSet<>();
		for (QuestionnaireQuestion oneQQ : listToReturn){
			if (oneQQ.getQuestionnaireAsset() != null){
				assetsToPull.add(oneQQ.getQuestionnaireAsset().getAsset().getId());
			}
		}
		if(!assetsToPull.isEmpty())
			assetRepository.findMany(assetsToPull);
	}

	/**
	 * method to get list of quetion and asset based of QuestionnaireQuestion
	 * @param questionsToBeDisplayed
	 * @return {@code  List<Map<String, Object>>}
	 */
	@Override
	public List<Map<String, Object>> getQQListForOrderQuestions(List<QuestionnaireQuestion> questionsToBeDisplayed) {
		// Display the form
		List<Map<String,Object>> qqList = new ArrayList<>();
		for (QuestionnaireQuestion oneQQ : questionsToBeDisplayed){
			Map<String,Object> oneRowMap = new HashMap<>();
			oneRowMap.put("question", new QuestionBean(oneQQ.getQuestion(), new ParameterBean(oneQQ.getQuestionnaireParameter().getParameterByParameterId())));
			oneRowMap.put("asset", new AssetBean(oneQQ.getQuestionnaireAsset().getAsset()));
			qqList.add(oneRowMap);
		}
		return qqList;
	}

	/**
	 * Find list of QQs for multichoice and checkboxes question type.
	 * @param questionnaireId
	 * @return {@code Set<QuestionnaireQuestionBean>}
	 */
	@Override
	public Set<QuestionBean> findQuestionsOfMCQorRadioType(Questionnaire questionnaire) {
		Set<QuestionBean> questionsToReturn = new HashSet<>();

		if(questionnaire == null){
			return questionsToReturn;
		}		
		// TODO: You can optimize by pulling the distinct questions from the QQ table.
		List<QuestionnaireQuestion> questionnaireQuestions = this.findByQuestionnaire(questionnaire, true);	

		for (QuestionnaireQuestion questionnaireQuestion : questionnaireQuestions) {
			String questionType = questionnaireQuestion.getQuestion().getQuestionType();
			if(questionType.equals(QuestionType.MULTCHOICE.toString())){
				questionsToReturn.add(new QuestionBean(questionnaireQuestion.getQuestion()));
			}
		}		
		return questionsToReturn;
	}

	/**
	 * Find questions by questionnaire.
	 * @param q
	 * @return {@code Set<QuestionBean>}
	 */
	@Override
	public Set<QuestionBean> findQuestions(Questionnaire q) {
		Set<QuestionBean> questionsToReturn = new HashSet<>();		
		if(q == null){
			return questionsToReturn;
		}		
		List<QuestionnaireQuestion> questionnaireQuestions = this.findByQuestionnaire(q, true);		
		for (QuestionnaireQuestion questionnaireQuestion : questionnaireQuestions) {
			questionsToReturn.add(new QuestionBean(questionnaireQuestion.getQuestion()));
		}		
		return questionsToReturn;
	}

	/**
	 * Create map for mandatory question
	 * @param questionnaireQuestionBeans
	 * @param parameterFunctions
	 * @return {@code Map<Integer, Integer>}
	 */
	@Override
	public Map<Integer, Integer> createMapForMandatoryQuestion(List<QuestionnaireQuestionBean> questionnaireQuestionBeans,
			List<ParameterFunction> parameterFunctions) {
		Map<Integer, Integer> mapOfMandatoryQuestion = new HashMap<>();
		for (QuestionnaireQuestionBean qqBean : questionnaireQuestionBeans) {
			for (ParameterFunction pf : parameterFunctions) {
				if(qqBean.getQuestion().getId().equals(pf.getQuestion().getId()) && qqBean.getQuestion().getParentParameter().getId().equals(pf.getParameterByParentParameterId().getId())){
					mapOfMandatoryQuestion.put(qqBean.getQuestionnaireQuestionId(), qqBean.getQuestionnaireQuestionId());
					break;
				}
			}
		}
		return mapOfMandatoryQuestion;
	}

	/**
	 * Create map for answered mandatory question.
	 * @param listOfQuestionnaires
	 * @return {@code Map<Integer, MandatoryQuestionBean>}
	 */
	@Override
	public Map<Integer, MandatoryQuestionBean> createMapForAnsweredMandatoryQuestion(List<Questionnaire> listOfQuestionnaires) {

		Map<Integer, MandatoryQuestionBean> answeredMandatoryQuestion = new HashMap<>();

		List<ParameterFunction> pfs = parameterFunctionService.findByMandatoryQuestion(true);

		for (Questionnaire qe : listOfQuestionnaires) {
			List<QuestionnaireQuestionBean> questionnaireQuestionBeans = this.prepareListOfQuestionsForQuestionnaire(qe);
			Map<Integer,Integer> mapForMandatoryQuestion = this.createMapForMandatoryQuestion(questionnaireQuestionBeans, pfs);
			List<Response> responses = responseService.findByQuestionnaire(qe, true);
			for (Response response : responses) {
				int answeredMandatoryQuestionCount = countMandatoryQuestion(mapForMandatoryQuestion, response);
				MandatoryQuestionBean mqBean = new MandatoryQuestionBean();
				mqBean.setAnswered(answeredMandatoryQuestionCount);
				mqBean.setTotal(mapForMandatoryQuestion.size());
				answeredMandatoryQuestion.put(qe.getId(), mqBean);
				break;
			}
		}		
		return answeredMandatoryQuestion;
	}

	/**
	 * count of MandatoryQuestion
	 * @param mapForMandatoryQuestion
	 * @param response
	 * @return {@code integer}
	 */
	private int countMandatoryQuestion(
			Map<Integer, Integer> mapForMandatoryQuestion, Response response) {
		Set<ResponseData> responseDatas = response.getResponseDatas();
		int answeredMandatoryQuestionCount = 0;
		for (ResponseData responseData : responseDatas) {
			if(mapForMandatoryQuestion.containsKey(responseData.getQuestionnaireQuestion().getId())){
				answeredMandatoryQuestionCount++;
			}
		}
		return answeredMandatoryQuestionCount;
	}

	/**
	 * find by Questionnaire Loaded ResponseDatas.
	 * @param qe
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	@Override
	public List<QuestionnaireQuestion> findByQuestionnaireLoadedResponseDatas(Questionnaire qe) {
		return questionnaireQuestionRepository.findByQuestionnaireLoadedResponseDatas(qe);
	}

	/**
	 * method to find Set of QuestionnaireQuestion based on
	 * Parameter And Questionnaire
	 * @param parameter
	 * @param questionnaire
	 * @return {@code List<QuestionnaireParameter>}
	 */
	@Override
	public Set<QuestionnaireQuestion> findByParameterAndQuestionnaire(ParameterBean parameter, Questionnaire questionnaire) {
		List<QuestionnaireParameter> qp = questionnaireParameterRepository.findByQuestionnaireAndParameterByParameterId(questionnaire,parameterService.findOne(parameter.getId()));
		return questionnaireQuestionRepository.findByQuestionnaireAndQuestionnaireParameterIn(questionnaire, qp);

	}

	/**
	 * method to find set of QuestionnaireQuestion by QuestionnaireAsset
	 * @param questionnaireAssets
	 * @return {@code Set<QuestionnaireQuestion>}
	 */
	@Override
	public Set<QuestionnaireQuestion> findByQuestionnaireAssetIn(Set<QuestionnaireAsset> questionnaireAssets) {

		return questionnaireQuestionRepository.findByQuestionnaireAssetIn(questionnaireAssets);
	}

	/**@author dhruv.sood
	 * Get the map of FmId and Parameter name
	 * @param questionnaire
	 */
	@Override
	public Map<Integer,String> getFcParameterList(Questionnaire questionnaire){
		List<Parameter> parameterList = parameterService.findByQuestionnaire(questionnaire,true) ;			
		Map<Integer,String> fcFmMap = new HashMap<>();

		if(!parameterList.isEmpty()){
			for(Parameter oneParameter:parameterList){
				if(oneParameter.getType().equals(ParameterType.FC.name())){
					List<ParameterConfig> paramConfigList = parameterConfigRepository.findByParameter(oneParameter);					  
					int fmId = Utility.convertJSONStringToObject(paramConfigList.get(0).getParameterConfig(), FunctionalCoverageParameterConfigBean.class).getFunctionalMapId();
					fcFmMap.put(fmId, oneParameter.getDisplayName());
				}						
			}			
		}		
		return fcFmMap;
	}

	@Override
	public List<QuestionnaireQuestion> getQuestionsForQuestionnaire(Questionnaire qe) {

		List<QuestionnaireQuestion> questionnaireQuestionList =  findByQuestionnaire(qe,true);		
		Collections.sort(questionnaireQuestionList, new SequenceNumberComparator());
		return questionnaireQuestionList.stream().collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(QuestionnaireQuestion::getId))),ArrayList::new));

	}

	@Override
	public QuestionnaireQuestion getQqForQuestion(QuestionBean question, QuestionnaireAsset questionnaireAsset,
			List<QuestionnaireParameter> listOfQuestionnaireParameters) {
		return questionnaireQuestionRepository.findByQuestionnaireAndQuestionAndQuestionnaireAssetAndQuestionnaireParameterIn(questionnaireAsset.getQuestionnaire(),
				questionService.findOne(question.getId()), questionnaireAsset, listOfQuestionnaireParameters);
	}

	@Override
	public Set<QuestionnaireQuestion> findByQuestionnaireParameterInAndQuestionnaireAssetIn(List<QuestionnaireParameter> listOfQp, Set<QuestionnaireAsset> listOfQa) {
		return questionnaireQuestionRepository.findByQuestionnaireParameterInAndQuestionnaireAssetIn(listOfQp, listOfQa);
	}

	@Override
	public QuestionnaireQuestion findByQuestionAndQuestionnaireParameterAndQuestionnaireAsset(Question question, QuestionnaireParameter questionnaireParameter,	QuestionnaireAsset questionnaireAsset) {
		return questionnaireQuestionRepository.findByQuestionAndQuestionnaireParameterAndQuestionnaireAsset(question, questionnaireParameter, questionnaireAsset);
	}

	/**
	 * method to get list Of Questions For Questionnaire excluding duplicate question,questionnnaireAsset group 
	 * @param qe
	 * @return {@code List<QuestionnaireQuestionBean> }
	 */
	@Override
	public List<QuestionnaireQuestionBean> listOfQuestionsForQuestionnaire(Questionnaire qe) {

		List<QuestionnaireQuestion> questionnaireQuestions = this.getByQuestionnaireId(qe, true);

		List<QuestionnaireQuestionBean> qqbeanListForGUI = new ArrayList<>();
		for (QuestionnaireQuestion qq : questionnaireQuestions){
			qqbeanListForGUI.add(new QuestionnaireQuestionBean(qq));
		}
		Collections.sort(qqbeanListForGUI, new SequenceNumberComparator());
		return qqbeanListForGUI;
	}

	private List<QuestionnaireQuestion> getByQuestionnaireId(Questionnaire questionnaire, boolean loaded){
		if(loaded){
			// get qq excluding duplicate questionId for each QuestionnnaireAsset
			List<QuestionnaireQuestion> listOfQQ = questionnaireQuestionRepository.findByQuestionnaireIdloaded(questionnaire);
			loadAssetsForQQ(listOfQQ);		
			for (QuestionnaireQuestion oneQQ : listOfQQ){
				oneQQ.getQuestion().getTitle();
				oneQQ.getQuestion().getQuestionType();
				oneQQ.getQuestionnaireAsset().getAsset();
				oneQQ.getQuestionnaireParameter().getParameterByParameterId().getUniqueName();
				oneQQ.getResponseDatas().size();
			}

			return listOfQQ;
		} else {
			return questionnaireQuestionRepository.findByQuestionnaireId(questionnaire);
		}

	}
	
	/**
	 * method to get list Of QuestionnaireQuestion by questionId and QuestionnaireAssetId
	 * @param questionId
	 * @param questionnaireAssetId
	 * @return {@code List<QuestionnaireQuestion>}
	 */

	@Override
	public List<QuestionnaireQuestion> findByQuestionIdAndQuestionnaireAssetId(QuestionnaireAsset questionnaireAssetId, Question questionId) {
		return questionnaireQuestionRepository.findByQuestionIdAndQuestionnaireAssetId(questionId, questionnaireAssetId);
	}
}


