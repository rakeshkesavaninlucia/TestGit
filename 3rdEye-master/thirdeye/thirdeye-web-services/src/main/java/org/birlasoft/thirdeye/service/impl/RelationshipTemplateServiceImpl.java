package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.beans.aid.RelatedAssetBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.repositories.RelationshipAssetDataRepository;
import org.birlasoft.thirdeye.repositories.RelationshipTemplateRepository;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.AssetTypeStyleService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} for {@link RelationshipTemplate}, {@link RelationshipAssetData} to save, update, find and 
 * delete.
 *
 */
@Service("relationshipTemplateService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class RelationshipTemplateServiceImpl implements RelationshipTemplateService{
	
	@Autowired
	private RelationshipTemplateRepository relationshipTemplateRepository;
	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	@Autowired
	private RelationshipAssetDataRepository relationshipAssetDataRepository;
	@Autowired
	private AssetTypeStyleService assetTypeStyleService;
	@Autowired
	private AssetService assetService;
	
	@Override
	public RelationshipTemplate saveOrUpdate(RelationshipTemplate incomingRelationshipTemplate) {
		return relationshipTemplateRepository.save(incomingRelationshipTemplate);
	}
	@Override
	public RelationshipTemplate findOne(Integer idOfRelationshipTemplate) {
		return relationshipTemplateRepository.findOne(idOfRelationshipTemplate);
	}
	@Override
	public List<RelationshipTemplate> findAll() {
		return relationshipTemplateRepository.findAll();
	}
	@Override
	public RelationshipTemplate createNewRelationshipTemplateObject(AssetTemplateBean assetTemplateBean, AssetTemplate assetTemplate) {
		RelationshipTemplate relationshipTemplate = new RelationshipTemplate();
		relationshipTemplate.setAssetTemplate(assetTemplate);
		AssetType childAssetType = assetTypeService.findOne(assetTemplateBean.getChildAssetTypeId());
		relationshipTemplate.setAssetTypeByChildAssetTypeId(childAssetType);
		AssetType parentAssetType = assetTypeService.findOne(assetTemplateBean.getParentAssetTypeId());
		relationshipTemplate.setAssetTypeByParentAssetTypeId(parentAssetType);
		RelationshipType relationshipType = relationshipTypeService.findOne(assetTemplateBean.getRelationshipTypeId());
		relationshipTemplate.setRelationshipType(relationshipType);
		relationshipTemplate.setHasAttribute(assetTemplateBean.isHasTemplateColumn());
		relationshipTemplate.setDisplayName(parentAssetType.getAssetTypeName() + " " + relationshipType.getDisplayName() + " " + childAssetType.getAssetTypeName());
		return relationshipTemplate;
	}
	@Override
	public List<RelationshipTemplate> findByAssetTemplate(AssetTemplate assetTemplate) {
		return relationshipTemplateRepository.findByAssetTemplate(assetTemplate);
	}
	@Override
	public RelationshipTemplate updateRelationshipTemplateObject(AssetTemplateBean assetTemplateBean) {
		RelationshipTemplate relationshipTemplate = this.findOne(assetTemplateBean.getRelationshipTemplateId());
		AssetType parentAssetType = assetTypeService.findOne(assetTemplateBean.getParentAssetTypeId());
		relationshipTemplate.setAssetTypeByParentAssetTypeId(parentAssetType);
		AssetType childAssetType = assetTypeService.findOne(assetTemplateBean.getChildAssetTypeId());
		relationshipTemplate.setAssetTypeByChildAssetTypeId(childAssetType);
		RelationshipType relationshipType = relationshipTypeService.findOne(assetTemplateBean.getRelationshipTypeId());
		relationshipTemplate.setRelationshipType(relationshipType);
		relationshipTemplate.setHasAttribute(assetTemplateBean.isHasTemplateColumn());
		relationshipTemplate.setDisplayName(parentAssetType.getAssetTypeName() + " " + relationshipType.getDisplayName() + " " + childAssetType.getAssetTypeName());
		return relationshipTemplate;
	}
	@Override
	public AssetTemplateBean setRelationshipIntoBean(AssetTemplate assetTemplate, AssetTemplateBean assetTemplateBean) {
		List<RelationshipTemplate> relationshipTemplates = this.findByAssetTemplate(assetTemplate);
		if(relationshipTemplates.size() == 1){
			RelationshipTemplate relationshipTemplate = relationshipTemplates.get(0);
			assetTemplateBean.setParentAssetTypeId(relationshipTemplate.getAssetTypeByParentAssetTypeId().getId());
			assetTemplateBean.setChildAssetTypeId(relationshipTemplate.getAssetTypeByChildAssetTypeId().getId());
			assetTemplateBean.setRelationshipTypeId(relationshipTemplate.getRelationshipType().getId());
			assetTemplateBean.setHasTemplateColumn(relationshipTemplate.isHasAttribute());
			assetTemplateBean.setRelationshipTemplateId(relationshipTemplate.getId());
		}
		return assetTemplateBean;
	}
	@Override
	public List<RelationshipTemplate> findByAssetTemplateIn(Set<AssetTemplate> setOfAssetTemplate) {
		return relationshipTemplateRepository.findByAssetTemplateIn(setOfAssetTemplate);
	}
	@Override
	public RelationshipAssetData saveRelationshipAssetData(RelationshipAssetData relationshipAssetData) {
		return relationshipAssetDataRepository.save(relationshipAssetData);
	}
	@Override
	public RelationshipAssetData createNewRelationshipAssetDataObject(Asset savedAsset, Asset parentAsset, Asset childAsset) {
		RelationshipAssetData relationshipAssetData = new RelationshipAssetData();
		relationshipAssetData.setAssetByAssetId(savedAsset);
		relationshipAssetData.setAssetByParentAssetId(parentAsset);
		relationshipAssetData.setAssetByChildAssetId(childAsset);
		
		return this.saveRelationshipAssetData(relationshipAssetData);
	}
	@Override
	public Set<RelationshipAssetData> findByAssetByAssetIdIn(Set<Asset> assets) {
		Set<RelationshipAssetData> relationshipAssetDatas = relationshipAssetDataRepository.findByAssetByAssetIdIn(assets);
		
		for (RelationshipAssetData relationshipAssetData : relationshipAssetDatas) {
			relationshipAssetData.getAssetByParentAssetId().getAssetTemplate().getAssetTemplateName();
			relationshipAssetData.getAssetByParentAssetId().getAssetDatas().size();
			relationshipAssetData.getAssetByChildAssetId().getAssetTemplate().getAssetTemplateName();
			relationshipAssetData.getAssetByChildAssetId().getAssetDatas().size();
		}
		
		return relationshipAssetDatas;
	}
	@Override
	public boolean isHasTemplateColumn(AssetTemplate assetTemplate) {
		boolean hasTemplateColumn = true;
		if(assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			List<RelationshipTemplate> listOfRelationshipTemplates = this.findByAssetTemplate(assetTemplate);
			if(listOfRelationshipTemplates.size() == 1){
				RelationshipTemplate relationshipTemplate = listOfRelationshipTemplates.get(0);
				hasTemplateColumn = relationshipTemplate.isHasAttribute();
			}
		}
		return hasTemplateColumn;
	}
	
	@Override
	public List<RelationshipAssetData> findByAssetByParentAssetId(Asset asset) {
		return relationshipAssetDataRepository.findByAssetByParentAssetId(asset);
	}

	@Override
	public List<RelationshipAssetData> findByAssetByChildAssetId(Asset asset) {
		return relationshipAssetDataRepository.findByAssetByChildAssetId(asset);
	}
	
	@Override
	public List<RelatedAssetBean> fetchInBoundRelationships(Asset asset) {
		Asset sameAsset = assetService.findOne(asset.getId());
		// Load the cases where is receiving from other assets
		sameAsset.getRelationshipAssetDatasForChildAssetId().size();

		Map<Asset, List<RelationshipAssetDataBean>> relationshipMapByAsset = new HashMap<>();

		for (RelationshipAssetData oneRelationship : sameAsset.getRelationshipAssetDatasForChildAssetId()){
			List<RelationshipAssetDataBean> otherRelationshipsWithSameAsset;
			RelationshipAssetDataBean relationshipBeanToInsert = new RelationshipAssetDataBean(oneRelationship);
			extractRelationshipDetails(relationshipBeanToInsert);

			if (relationshipMapByAsset.containsKey(oneRelationship.getAssetByParentAssetId())){
				otherRelationshipsWithSameAsset = relationshipMapByAsset.get(oneRelationship.getAssetByParentAssetId());
				otherRelationshipsWithSameAsset.add(relationshipBeanToInsert);
			} else { 
				otherRelationshipsWithSameAsset = new ArrayList<>();
				otherRelationshipsWithSameAsset.add(relationshipBeanToInsert);
				relationshipMapByAsset.put(oneRelationship.getAssetByParentAssetId(), otherRelationshipsWithSameAsset);
			}
		}

		return generateRelatedAssetsBeanForAid(relationshipMapByAsset);
	}
	
	@Override
	public List<RelatedAssetBean> fetchOutBoundRelationships(Asset asset) {
		Asset sameAsset = assetService.findOne(asset.getId());
		// Load the cases where is receiving from other assets
		sameAsset.getRelationshipAssetDatasForParentAssetId().size();

		Map<Asset, List<RelationshipAssetDataBean>> relationshipMapByAsset = new HashMap<>();

		for (RelationshipAssetData oneRelationship : sameAsset.getRelationshipAssetDatasForParentAssetId()){
			List<RelationshipAssetDataBean> otherRelationshipsWithSameAsset;
			RelationshipAssetDataBean relationshipBeanToInsert = new RelationshipAssetDataBean(oneRelationship);
			extractRelationshipDetails(relationshipBeanToInsert);

			if (relationshipMapByAsset.containsKey(oneRelationship.getAssetByChildAssetId())){
				otherRelationshipsWithSameAsset = relationshipMapByAsset.get(oneRelationship.getAssetByChildAssetId());
				otherRelationshipsWithSameAsset.add(relationshipBeanToInsert);
			} else { 
				otherRelationshipsWithSameAsset = new ArrayList<>();
				otherRelationshipsWithSameAsset.add(relationshipBeanToInsert);
				relationshipMapByAsset.put(oneRelationship.getAssetByChildAssetId(), otherRelationshipsWithSameAsset);
			}
		}

		return generateRelatedAssetsBeanForAid(relationshipMapByAsset);
	}
	
	/**
	 * Get a {@code List} of {@link RelatedAssetBean} for AID graphs
	 * @param relationshipMapByAsset
	 * @return
	 */
	private List<RelatedAssetBean> generateRelatedAssetsBeanForAid(Map<Asset, List<RelationshipAssetDataBean>> relationshipMapByAsset) {
		List<RelatedAssetBean> relatedAssets = new ArrayList<>();
		for (Entry<Asset, List<RelationshipAssetDataBean>> entry : relationshipMapByAsset.entrySet()){
			RelatedAssetBean oneOutBoundAssetBean = new RelatedAssetBean();
			AssetBean oneAssetBean = new AssetBean(entry.getKey());
			oneAssetBean.setAssetStyle(assetTypeStyleService.getAssetStyle(entry.getKey().getAssetTemplate().getAssetType()));
			oneOutBoundAssetBean.setAsset(oneAssetBean);
			oneOutBoundAssetBean.setListOfRelationshipAssetData(entry.getValue());
			relatedAssets.add(oneOutBoundAssetBean);
		}
		return relatedAssets;
	}
	
	/**
	 * Extract Relationship name, data type and frequency
	 * @param relationshipBeanToInsert
	 */
	private void extractRelationshipDetails(RelationshipAssetDataBean relationshipAssetDataBean) {
		for (TemplateColumnBean oneAttribute : relationshipAssetDataBean.getRelationshipAsset().getAssetDatas()){
			if (oneAttribute.getName().equalsIgnoreCase(Constants.DEFAULT_COLUMN_NAME)){
				relationshipAssetDataBean.setDisplayName(oneAttribute.getData());
			} else if ("Data Type".equalsIgnoreCase(oneAttribute.getName()) || "DataType".equalsIgnoreCase(oneAttribute.getName())){
				relationshipAssetDataBean.setDataType(oneAttribute.getData());
			} else if ("Frequency".equalsIgnoreCase(oneAttribute.getName())){
				relationshipAssetDataBean.setFrequency(oneAttribute.getData());
			}
		}
	}
	
	@Override
	public List<RelationshipAssetDataBean> getRootRelationshipsOfAsset(Asset asset) {
		List<RelationshipAssetData> listOfRelationships = relationshipAssetDataRepository.findByAssetByParentAssetIdOrAssetByChildAssetId(asset, asset);
		List<RelationshipAssetDataBean> listToReturn = new ArrayList<>();
		Set<RelationshipType> setOfRelationshipType = new HashSet<>();

		for (RelationshipAssetData relationshipAsset : listOfRelationships){
			//get relationship type from relationship asset data
			setOfRelationshipType.add(new ArrayList<RelationshipTemplate>(relationshipAsset.getAssetByAssetId().getAssetTemplate().getRelationshipTemplates()).get(0).getRelationshipType());
		}
		
		for (RelationshipType relationshipType : setOfRelationshipType) {
			RelationshipAssetDataBean aBean = new RelationshipAssetDataBean();
			List<AssetBean> listOfAssetBean = new ArrayList<>();
			for (RelationshipAssetData reAsset : listOfRelationships) {
				if(relationshipType.equals(new ArrayList<RelationshipTemplate>(reAsset.getAssetByAssetId().getAssetTemplate().getRelationshipTemplates()).get(0).getRelationshipType())){
					AssetBean assetBean = reAsset.getAssetByParentAssetId().equals(asset) ? new AssetBean(reAsset.getAssetByChildAssetId()) : new AssetBean(reAsset.getAssetByParentAssetId());
					assetBean.setRelationshipAssetId(reAsset.getId());
					listOfAssetBean.add(assetBean);
				}
			}
			aBean.setListOfAssets(listOfAssetBean);
			aBean.setDisplayName(relationshipType.getDisplayName());
			listToReturn.add(aBean);
		}
		
		return listToReturn;
	}

	@Override
	public void deleteInBatch(List<RelationshipTemplate> deleRelationshipTemplates) {
		relationshipTemplateRepository.deleteInBatch(deleRelationshipTemplates);

	}

}
