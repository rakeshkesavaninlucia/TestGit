package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.QuestionnaireRepository;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for create {@code question} , save {@code question} , list of all {@code question} ,
 * edit {@code question} , view {@code question} .
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireServiceImpl implements QuestionnaireService {
	@Autowired
	private QuestionnaireRepository questionnaireRepositoy;
	@Autowired
	private QuestionnaireQuestionService qqService;
	@Autowired
	private QuestionnaireAssetService qaService;
	@Autowired
	private WorkspaceService workspaceService;
	@Autowired
	private AssetTemplateService assetTemplateService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private QuestionnaireParameterService qpService;
	@Autowired
	private ResponseDataService responseDataService;
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private AssetTypeService assetTypeService;
	
	private static Logger logger = LoggerFactory.getLogger(QuestionnaireServiceImpl.class);	
	
	@Override
	public Questionnaire save(Questionnaire questionnaire) {
		return questionnaireRepositoy.save(questionnaire);
	}
	
	@Override
	public List<Questionnaire> findByWorkspaceIn(Set<Workspace> workspace) {
		return questionnaireRepositoy.findByWorkspaceIn(workspace);
	}

	@Override
	public Questionnaire findOne(Integer id) {
		return questionnaireRepositoy.findOne(id);
	}
	
	@Override
	public Questionnaire findOneFullyLoaded(Integer id) {
		Questionnaire qe = findOne( id);
		
		// Load the questionnaire assets
		qe.getQuestionnaireAssets().size();
		qe.getQuestionnaireParameters().size();
		qe.getWorkspace().getWorkspaceName();
		qe.getAssetType().getAssetTypeName();
		return qe;
	}

	@Override
	public List<Questionnaire> findByStatus(String status, boolean loaded) {
		List<Questionnaire> listOfResponse = questionnaireRepositoy.findByStatus(status);
		
		if (loaded){
			for (Questionnaire qe : listOfResponse){
				qe.getResponses().size();
			}
		}
		
		return listOfResponse;
	}

	@Override
	public List<Questionnaire> findAll() {
		return questionnaireRepositoy.findAll();
	}
	
	@Override
	public void updateQuestionnaireEntity(Questionnaire qe, Set<Asset> updatedAssetSet, Set<Parameter> updatedParameterList ){
		// Warning you will delete all assets if you are not submitting any assets
		if (updatedAssetSet != null) {
			modifyAssetLinks(qe, updatedAssetSet);
		}
		
		if (updatedParameterList != null){
			// Must reload the Questionnaire because of the assets modification
			Questionnaire questionnaire  = questionnaireRepositoy.findOne(qe.getId());
			modifyParameterLinks(questionnaire,updatedParameterList);
			// Check if you have any params on the QE
			// See what is to be deleted
			// Add the new entries to QP: call Sunils method to add the QP
		}
		
		// See if there are any parameters on the QE
		// See what is to be added and what is to be deleted
		// For the deletions find the QQ / Responses to be deleted
		
	}
	private void modifyParameterLinks(Questionnaire qe, Set<Parameter> updatedParameterSet) {
		
		Set<QuestionnaireParameter> setOfQp = qpService.findByParameterByRootParameterIdInAndQuestionnaire(updatedParameterSet, qe);
		qqService.handleRemovalOfParamsFromQE(setOfQp);
		// Add the new questionnaire Parameter

	}
	
	private void modifyAssetLinks(Questionnaire qe, Set<Asset> updatedAssetSet) {
		// Find the changes in assets
		Set<Asset> assetOnQe = new HashSet<>();
		Map<Asset, QuestionnaireAsset> cacheOfQA = new HashMap<>();
		Set<QuestionnaireAsset> questionnaireAssets = qaService.findByQuestionnaireLoadedAsset(qe);
		for (QuestionnaireAsset qa : questionnaireAssets){
			assetOnQe.add(qa.getAsset());
			cacheOfQA.put(qa.getAsset(), qa);
		}
		
		handleAssetRemoval(updatedAssetSet, assetOnQe, cacheOfQA);
		
		// Add the new questionnaire Assets
		handleAssetAddition(updatedAssetSet, assetOnQe,qe);
		
	}
	
	
	private void handleAssetAddition(Set<Asset> updatedAssetSet, Set<Asset> assetOnQe, Questionnaire qe) {
		// First lets find out what is to be added
		Set<Asset> assetsToBeAdded = new HashSet<>();
		assetsToBeAdded.addAll(assetOnQe);
		updatedAssetSet.removeAll(assetsToBeAdded);
		//In QuestionnaireAsset, implementation of equal and hash 
		// is based on Q_A id and here Q_A id is null. so we are using List instead of Set.
		List<QuestionnaireAsset> questionnaireAssetForAddition = new ArrayList<>();
		for (Asset oneAsset : updatedAssetSet){
			questionnaireAssetForAddition.add(new QuestionnaireAsset(oneAsset, qe));
		}
		qaService.saveAndFlush(questionnaireAssetForAddition);
	}

	private void handleAssetRemoval(Set<Asset> updatedAssetSet, Set<Asset> assetOnQe, Map<Asset, QuestionnaireAsset> cacheOfQA) {
		// First lets find out what is to be deleted
		Set<Asset> assetForRemoval = new HashSet<>();
		assetForRemoval.addAll(assetOnQe);
		assetForRemoval.removeAll(updatedAssetSet);
		
		Set<QuestionnaireAsset> questionnaireAssetForRemoval = new HashSet<>();
		for (Asset oneAsset : assetForRemoval){
			questionnaireAssetForRemoval.add(cacheOfQA.get(oneAsset));
		}
		// Remove all QQ and QE related to the assets
		qqService.handleRemovalOfAssetsFromQE(questionnaireAssetForRemoval);
	}
	
	
	@Override
	public Questionnaire createQuestionnaireObject(Questionnaire questionnaire, User currentUser, String questionnaireType ) {
		questionnaire.setStatus(QuestionnaireStatusType.EDITABLE.toString());
		questionnaire.setQuestionnaireType(questionnaireType);
		questionnaire.setUserByCreatedBy(currentUser);
		questionnaire.setUserByUpdatedBy(currentUser);
		questionnaire.setCreatedDate(new Date());
		questionnaire.setUpdatedDate(new Date());
		return questionnaire;
	}

	@Override
	public Questionnaire updateQuestionnaireObject(Questionnaire questionnaire, User currentUser) {
		Questionnaire questionnaireFromDB = findOneFullyLoaded(questionnaire.getId());
		questionnaire.setUserByUpdatedBy(currentUser);
		questionnaire.setUpdatedDate(new Date());
		questionnaire.setUserByCreatedBy(questionnaireFromDB.getUserByCreatedBy());
		questionnaire.setCreatedDate(questionnaireFromDB.getCreatedDate());
		return questionnaire;
	}

	@Override
	public Map<Integer, Integer> getMapOfSelectedAssets(Questionnaire questionnaire) {
		Map<Integer, Integer> mapsOfSelectedAsset = new HashMap<>();
		List<QuestionnaireAsset> questionnaireAssets = qaService.findByQuestionnaire(questionnaire);
		for (QuestionnaireAsset questionnaireAsset : questionnaireAssets) {
			mapsOfSelectedAsset.put(questionnaireAsset.getAsset().getId(), questionnaireAsset.getAsset().getId());
		}
		return mapsOfSelectedAsset;
	}

	@Override
	public Map<String, List<AssetBean>> getMapOfAssetTypeAndAssetBeans(Workspace ws) {
		Map<String, List<AssetBean>> mapOfAssets = new HashMap<>();
		
		if (ws == null){
			return mapOfAssets;
		}
		
		Workspace workspace = workspaceService.findOne(ws.getId());
		
		Set<AssetTemplate> assetTemplates = workspace.getAssetTemplates();
		for (AssetTemplate assetTemplate : assetTemplates) {

			List<AssetBean> assetBeansOfOneType ;
			String assetTypeName = assetTemplate.getAssetType().getAssetTypeName();
			if (mapOfAssets.containsKey(assetTypeName)){
				assetBeansOfOneType = mapOfAssets.get(assetTypeName);
			} else {
				assetBeansOfOneType = new ArrayList<>();
				mapOfAssets.put(assetTypeName, assetBeansOfOneType);
			}
			
			// Add all the asset of the type to the list
			assetTemplate = assetTemplateService.findFullyLoadedAssetTemplate(assetTemplate.getId());
			for (Asset asset : assetTemplate.getAssets()) {
				AssetBean assetBean = new AssetBean(asset);
				assetBeansOfOneType.add(assetBean);
			}
		}
		return mapOfAssets;
	}

	@Override
	public List<Asset> getNewAssetsToBeLinked(String[] assetParameter) {
		List<Asset> newAssetsToBeLinked = new ArrayList<>();
		
		if (assetParameter == null){
			return newAssetsToBeLinked;
		}
		
		for(String s : assetParameter){
			
			try {
				Asset  assetObject = assetService.findOne(Integer.parseInt(s));
				newAssetsToBeLinked.add(assetObject);
			} catch (NumberFormatException nfe){
				logger.error("Unable to parse parameter:" +s , nfe);
			}
		}
		return newAssetsToBeLinked;
	}

	@Override
	public List<Questionnaire> findByQuestionnaireTypeAndStatusAndWorkspaceIn(String type, String status, Set<Workspace> workspaces, boolean loaded) {
		List<Questionnaire> listOfResponse = questionnaireRepositoy.findByQuestionnaireTypeAndStatusAndWorkspaceIn(type, status, workspaces);
		
		if (loaded){
			for (Questionnaire qe : listOfResponse){
				qe.getResponses().size();
			}
		}
		return listOfResponse;
	}
	

	@Override
	public String saveQQResponseData(HttpServletRequest request,Integer responseId) {
		
		String queType ="";
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		for(String oneParam : requestParameterNames){
			if (oneParam.startsWith("qq_")){
				// Find out which qq the user is answering
				String cleansedParam = oneParam.replaceAll("qq_", "");
				// The cleansed param is an Integer
				
				Integer idOfQQ = Integer.parseInt(cleansedParam);
				String scoreValue = request.getParameter(oneParam);				
				QuestionnaireQuestion qq = qqService.findOne(idOfQQ, false);
				Response response = responseService.findOne(responseId);
				ResponseData responseData = responseDataService.findByQuestionnaireQuestionAndResponse(qq, response, true);
				queType = responseData.getQuestionnaireQuestion().getQuestion().getQuestionType();
				if(validateScore(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null)){
					return "error"+"_"+queType;
				}
				prepareResponseData(queType, scoreValue, responseData);
				responseDataService.save(responseData);
			}
		}
		return "success"+"_"+queType;
		
	}

	
	private void prepareResponseData(String queType, String scoreValue,	ResponseData responseData) {
		if(queType.equals(QuestionType.TEXT.toString())){
			JSONTextResponseMapper jsonTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONTextResponseMapper.class);
			jsonTextResponseMapper.setScore(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null);
			responseData.setResponseData(Utility.convertObjectToJSONString(jsonTextResponseMapper));
		}else if(queType.equals(QuestionType.PARATEXT.toString())){
			JSONParaTextResponseMapper jsonParaTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONParaTextResponseMapper.class);
			jsonParaTextResponseMapper.setScore(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null);
			responseData.setResponseData(Utility.convertObjectToJSONString(jsonParaTextResponseMapper));
		}else if(queType.equals(QuestionType.DATE.toString())){
			JSONDateResponseMapper jsonDateResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONDateResponseMapper.class);
			jsonDateResponseMapper.setScore(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null);
			responseData.setResponseData(Utility.convertObjectToJSONString(jsonDateResponseMapper));
		}else if(queType.equals(QuestionType.MULTCHOICE.toString())){
			JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONMultiChoiceResponseMapper.class);
			jsonMultiChoiceResponseMapper.setScore(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null);
			responseData.setResponseData(Utility.convertObjectToJSONString(jsonMultiChoiceResponseMapper));
		}else if(queType.equals(QuestionType.NUMBER.toString())){
			JSONNumberResponseMapper jsonNumberResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
			jsonNumberResponseMapper.setQuantifier(!scoreValue.isEmpty() ? Double.parseDouble(scoreValue) : null);
			responseData.setResponseData(Utility.convertObjectToJSONString(jsonNumberResponseMapper));
		}
	}
	
	/**
	 * method to validate score
	 * @param score
	 * @return boolean
	 */
	private boolean validateScore(Double score) {
		if (score != null && score != -1) {
			if (score < 1 || score > 10) {
				return true;
			}
		} else {
			if (score == null)
				return true;
		}
		return false;
	}
	
	@Override
	public Set<AssetType> getAssetType(Workspace ws) {

		Set<AssetType> listOfAssetType = new HashSet<>();
		if (ws == null) {
			return listOfAssetType;
		}
		Workspace workspace = workspaceService.findOne(ws.getId());

		Set<AssetTemplate> assetTemplates = workspace.getAssetTemplates();
		for (AssetTemplate assetTemplate : assetTemplates) {
			if (!assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name()))
				listOfAssetType.add(assetTemplate.getAssetType());
		}
		return listOfAssetType;
	}

	@Override
	public AssetType getAssetTypeByAssetTypename(String assetTypeName) {		
		return assetTypeService.findByAssetTypeName(assetTypeName);
	}

	@Override
	public Map<String, List<AssetBean>> getMapOfAssetByAssetType(Workspace workspace, String assetType) {
		return getMapOfAssetTypeAndAssetBeans(workspace).entrySet().stream().filter(p-> p.getKey().equals(assetType))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));	
	}

	@Override
	public List<Questionnaire> findByWorkspaceInAndQuestionnaireType(Set<Workspace> workspace,String questionnaireType) {
		return questionnaireRepositoy.findByWorkspaceInAndQuestionnaireType(workspace, questionnaireType);
	}
	
	@Override
	public List<Questionnaire> findByClosedStatus(String status){
		return questionnaireRepositoy.findByStatus(status);
	
	}

}
