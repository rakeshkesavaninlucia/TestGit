package org.birlasoft.thirdeye.beans.fm;

import org.birlasoft.thirdeye.constant.FunctionalMapType;
import org.birlasoft.thirdeye.entity.FunctionalMap;

public class FunctionalMapBean {
	
	private Integer id;
	private Integer assetTemplateId;
	private Integer bcmId;
	private Integer questionId;
	private Integer workspaceId;
	private FunctionalMapType type;
	private String name;
	
	public FunctionalMapBean(FunctionalMap fm) {
		this.id = fm.getId();
		this.assetTemplateId = fm.getAssetTemplate().getId();
		this.bcmId = fm.getBcm().getId();
		this.questionId = fm.getQuestion().getId();
		this.workspaceId = fm.getWorkspace().getId();
		this.type = FunctionalMapType.valueOf(fm.getType());
		this.name = fm.getName();
	}
	
	public FunctionalMapBean() {}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getAssetTemplateId() {
		return assetTemplateId;
	}
	
	public void setAssetTemplateId(Integer assetTemplateId) {
		this.assetTemplateId = assetTemplateId;
	}
	
	public Integer getBcmId() {
		return bcmId;
	}
	
	public void setBcmId(Integer bcmId) {
		this.bcmId = bcmId;
	}
	
	public Integer getQuestionId() {
		return questionId;
	}
	
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	public Integer getWorkspaceId() {
		return workspaceId;
	}
	
	public void setWorkspaceId(Integer workspaceId) {
		this.workspaceId = workspaceId;
	}

	public FunctionalMapType getType() {
		return type;
	}

	public void setType(FunctionalMapType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
