package org.birlasoft.thirdeye.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service interface for questionnaire response excel.
 * @author dhruv.sood
 */
public interface QuestionnaireExcelService {
	
	/**@author dhruv.sood
	 * @param response
	 * @param questionnaire
	 */	
	
	public void exportQuestionnaireTemplate(HttpServletResponse response, Questionnaire questionnaire, Response qResponse);
	
	/**@author dhruv.sood
	 * @param workbook
	 * @param questionnaire
	 * @param listOfErrorCells
	 * @return 
	 */
	public void importQuestionnaireResponseExcel(XSSFWorkbook workbook, Questionnaire questionnaire,List<ExcelCellBean> listOfErrorCells);

	/**Method to export chart of account cost response excel
	 * @author dhruv.sood
	 * @param qResponse
	 * @param questionnaire
	 * @param response
	 */
	public void exportCOATemplate(HttpServletResponse response, Questionnaire questionnaire, Response qResponse);
	/**
	 * Method to check file exist or not
	 * @param file
	 * @param listOfErrorCells
	 * @return {@code boolean}
	 */
	public boolean isFileExist(MultipartFile file,List<ExcelCellBean> listOfErrorCells);
	
}
