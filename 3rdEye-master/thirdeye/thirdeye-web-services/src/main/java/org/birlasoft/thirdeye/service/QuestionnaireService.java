package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
/**
 * This is {@code service} interface for questionnaire.
 * @author sanjeev.mishra
 */
public interface QuestionnaireService {
	
	/**
	 * save {@code questionnaire} 
	 * @param questionnaire
	 * @return {@code Questionnaire}
	 */
	public Questionnaire save(Questionnaire questionnaire);
	
	/**
	 * List of all {@code questionnaire} within a {@code user} {@code workspace}
	 * @param workspace
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByWorkspaceIn(Set<Workspace> workspace);
	
	/**
	 * find One {@code questionnaire} for given id.
	 * @param id
	 * @return {@code Questionnaire}
	 */
	 public Questionnaire findOne(Integer id);
	 
	 /**
	 * Fine one fully loaded questionnaire object by id. 
	 * @param id
	 * @return {@code Questionnaire}
	 */
	 public Questionnaire findOneFullyLoaded(Integer id);
	 
	 /**
	 * Find list of questionnaire with loaded or not by publish status. 
	 * @param status
	 * @param loaded
	 * @return {@code List<Questionnaire>}
	 */
	 public List<Questionnaire> findByStatus(String status, boolean loaded);
	 
	 /**
	 * Find all list of questionnaire.
	 * @return {@code List<Questionnaire>}
	 */
	 public List<Questionnaire> findAll();
	 
	 /**
	 * Create a {@code QuestionnaireObject}.
	 * @param questionnaire
	 * @param currentUser
	 * @param questionnairetype
	 * @return {@code Questionnaire}
	 */
	public Questionnaire createQuestionnaireObject(Questionnaire questionnaire, User currentUser, String questionnairetype);
	/**
	 * Update a {@code QuestionnaireObject}  
	 * @param questionnaire
	 * @param currentUser
	 * @return {@code Questionnaire}
	 */
	public Questionnaire updateQuestionnaireObject(Questionnaire questionnaire, User currentUser);
	/** Get Map of selected assets. 
	 * @param questionnaire
	 * @return {@code Map<Integer, Integer>}
	 */
	public Map<Integer, Integer> getMapOfSelectedAssets(Questionnaire questionnaire);
	/** Get map of asset type string and list of asset beans.
	 * @param workspace
	 * @return {@code Map<String, List<AssetBean>>}
	 */
	public Map<String, List<AssetBean>> getMapOfAssetTypeAndAssetBeans(Workspace workspace);
	/**
	 * Get new assets to be linked.
	 * @param assetParameter
	 * @return {@code List<Asset>}
	 */
	public List<Asset> getNewAssetsToBeLinked(String[] assetParameter);
	/**
	 * Find list of questionnaire by publish status and in active workspaces. 
	 * @param type
	 * @param status 
	 * @param workspaces
	 * @param loaded
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByQuestionnaireTypeAndStatusAndWorkspaceIn(String type, String status, Set<Workspace> workspaces, boolean loaded);

	/**
	 * update Questionnaire Entity
	 * @param qe
	 * @param updatedAssetSet
	 * @param updatedParameterList
	 */
	public void updateQuestionnaireEntity(Questionnaire qe, Set<Asset> updatedAssetSet, Set<Parameter> updatedParameterList );

	/**
	 * Save QQ Response Data
	 * @param request
	 * @param responseId
	 * @return {@code String}
	 */
	public String saveQQResponseData(HttpServletRequest request,Integer responseId);

	/**
	 * method to get set of AssetType for given workSpace.
	 * @param ws
	 * @return {@code List<String>}
	 */
	public Set<AssetType> getAssetType(Workspace ws);

	
	/**
	 * method to get AssetType for given assetTypeName.
	 * @param assetTypeName
	 * @return {@code AssetType} Object
	 */
	public AssetType getAssetTypeByAssetTypename(String assetTypeName);

	/** Get map of asset type string and list of asset beans based on given asset type.
	 * @param workspace
	 * @param assetType
	 * @return {@code Map<String, List<AssetBean>>}
	 */
	public Map<String, List<AssetBean>> getMapOfAssetByAssetType(Workspace workspace,String assetType);
	
	/**
	 * method to get list of Questionnaire by given status.
	 * @param status
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByClosedStatus(String status);	
	/**
	 * List of all {@code questionnaire} within a {@code user} {@code workspace} and  {@code questionnaireType}
	 * @param workspace
	 * @param questionnaireType
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByWorkspaceInAndQuestionnaireType(Set<Workspace> workspace, String questionnaireType);
	
}
