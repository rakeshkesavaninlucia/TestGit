package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.widgets.BaseWidgetJSONConfig;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Widget;
/**
 * 
 * Service class for dashboard
 *
 */
public interface DashboardService {

	/**
	 * 
	 * @param idOfDashboard
	 * @return
	 */
	public Dashboard findOne (int idOfDashboard);
	
	/**
	 * 
	 * @param activeDashboard
	 * @param baseWidgetConfig
	 * @return
	 */
	public List<BaseWidgetJSONConfig> addNewWidgetToDashboard(Dashboard activeDashboard, BaseWidgetJSONConfig baseWidgetConfig);
	
	/**
	 * 
	 * @param idOfWidget
	 * @param loadDashboard
	 * @return
	 */
	public Widget findOneWidget(Integer idOfWidget, boolean loadDashboard);
	
	/**
	 * 
	 * @param w
	 * @return
	 */
	public Widget saveWidget(Widget w);
	
	/**
	 * 
	 * @param incomingDashboard
	 * @return
	 */
	public Dashboard saveOrUpdate(Dashboard incomingDashboard);
	
	/**
	 * 
	 * @return
	 */
	public Set<Dashboard> listDashboardForActiveWorkspace();
	
	/**
	 * 
	 * @param listOfDelete
	 */
	public void delete(List<Dashboard> listOfDelete);
	
	/**
	 * 
	 * @param asList
	 */
	public void deleteWidget(List<Widget> asList);
	
	/**
	 * 
	 * @param activeDashboard
	 * @return
	 */
	public List<BaseWidgetJSONConfig> viewDashboard(Dashboard activeDashboard);
	
}
