package org.birlasoft.thirdeye.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.entity.Workspace;


/**
 * Service Interface for Benchmark.
 */

public interface BenchmarkService {
	/**
	 * Fetch list of Benchmark by workspaces 
	 * @param setOfWorkspace
	 * @return {@code List<Benchmark>}
	 */
	public List<Benchmark> findByWorkspaceIn(Set<Workspace> setOfWorkspace);
	/**
	 * Find one Benchmark by id.
	 * @param benchmarkId
	 * @return {@code Benchmark}
	 */
	public Benchmark findOne(Integer benchmarkId);
	/**
	 * Fetch list of Benchmark item by benchmark id. 
	 * @param benchmark
	 * @return {@code List<BenchmarkItem>}
	 */
	public List<BenchmarkItem> findByBenchmark(Benchmark benchmark);
	/**
	 * Fetch list of Benchmark item by benchmark item ids.
	 * @param setOfBenchmarkItemIds
	 * @return {@code List<BenchmarkItem>}
	 */
	public List<BenchmarkItem> findByIdIn(Set<Integer> setOfBenchmarkItemIds);
	/**
	 * save update Bench mark
	 * @param benchmark
	 * @return {@code Benchmark}
	 */
	public Benchmark saveUpdateBenchmark(Benchmark benchmark);
	/**
	 * Fetch list of benchmark
	 * @param benchmarkId
	 * @return {@code List<Benchmark>}
	 */
	public List<Benchmark> findAllBenchmark(Integer benchmarkId);
	
	/**
	 * save Bench mark
	 * @param benchmark
	 * @return {@code Benchmark}
	 */
	public Benchmark save(Benchmark benchmark);
	/**
	 * fetch list of Benchmark
	 * @param listOfWorkspaces
	 * @return {@code List<Benchmark> }
	 */
	public List<Benchmark> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> listOfWorkspaces);
	/**
	 * find fully loaded benchmark
	 * @param idOfBenchMark
	 * @return {@code Benchmark}
	 */
	public Benchmark findfullyLoadedBenchmark(Integer idOfBenchMark);
	/**
	 * delete benchmark
	 * @param idOfBenchmark
	 */
	public void deleteBenchmark(Integer idOfBenchmark);
	/**
	 * Find item score by benchmark item and current date.
	 * @param benchmarkItem
	 * @param currentDate
	 * @return {@code List<BenchmarkItemScore>}
	 */
	public List<BenchmarkItemScore> findByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date currentDate);
	/**
	 * Fetch system level benchmark
	 * @return {@code List<Benchmark>}
	 */
	public List<Benchmark> findByWorkspaceIsNull();
}
