package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;


/**
 * This object will be used for JSON serialization
 * 
 * @author samar.gupta
 *
 */
public class JSONTextResponseMapper implements QuantifiableResponse{

	private String responseText;
	private Double score;

	public JSONTextResponseMapper() {
		super();
	}

	public String getResponseText() {
		return responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		
		if (score != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(score));
		}
		
		
		return valueToBeReturned;
	}
}
