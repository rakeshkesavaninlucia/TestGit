package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

public class ScatterSeries {

	private String seriesName;
	private List<DataCoordinate> dataPoints = new ArrayList<DataCoordinate>();
	private String seriesColor;
	
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public List<DataCoordinate> getDataPoints() {
		return dataPoints;
	}
	public void setDataPoints(List<DataCoordinate> dataPoints) {
		this.dataPoints = dataPoints;
	}
	
	public void addDataPoint(DataCoordinate dc){
		this.dataPoints.add(dc);
	}
	
	public String getSeriesColor() {
		return seriesColor;
	}
	public void setSeriesColor(String seriesColor) {
		this.seriesColor = seriesColor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((seriesName == null) ? 0 : seriesName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScatterSeries other = (ScatterSeries) obj;
		if (seriesName == null) {
			if (other.seriesName != null)
				return false;
		} else if (!seriesName.equals(other.seriesName))
			return false;
		return true;
	}
	
	
}
