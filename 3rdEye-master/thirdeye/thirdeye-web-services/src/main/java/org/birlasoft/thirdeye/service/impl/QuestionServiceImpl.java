package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.entity.Category;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.QuestionRepository;
import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.BenchmarkItemScoreService;
import org.birlasoft.thirdeye.service.BenchmarkItemService;
import org.birlasoft.thirdeye.service.BenchmarkService;
import org.birlasoft.thirdeye.service.CategoryService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**This is the {@code service implements class} to actually save and find questions 
 * @author sanjeev.mishra
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionServiceImpl implements QuestionService {
	
	private static Logger logger = LoggerFactory.getLogger(QuestionServiceImpl.class);
	
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private WorkspaceService workspaceService;
	@Autowired
	private ParameterFunctionService parameterFunctionService;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService; 
	@Autowired
	private BenchmarkService benchmarkService;
	@Autowired
	private BenchmarkItemService benchmarkItemService;
	@Autowired
	private BenchmarkItemScoreService benchmarkItemScoreService;
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;

	@Override
	public Question save(Question question) {
		return questionRepository.save(question);
	}

	@Override
	public List<Question> findByWorkspaceIn(Set<Workspace> workspaces) {
		return questionRepository.findByWorkspaceIn(workspaces);
	}

	@Override
	public List<Question> findQuestionsByWorkspaceNull() {
		return questionRepository.findQuestionsByWorkspaceNull();
	}

	@Override
	public Question findOne(Integer id) {
		return questionRepository.findOne(id);
	}

	@Override
	public List<Question> findByWorkspaceIsNullOrWorkspace(Workspace workspace) {
		return questionRepository.findByWorkspaceIsNullOrWorkspace(workspace);
	}

	@Override
	public Question updateQuestionObject(Question question, List<Double> quantifiers, User currentUser, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean) {
		Question questionFromDB = findOne(question.getId());
		String queTypeText = question.getQueTypeText();
		String jSONString = null;
		if(queTypeText != null || numberQuestionMapper != null || !bqBean.getBenchmarkItem().isEmpty())
			jSONString = createJSONString(quantifiers, queTypeText, question.getQuestionType(), otherOption, numberQuestionMapper, bqBean);
		question.setQueTypeText(jSONString);
		question.setUserByUpdatedBy(currentUser);
		question.setUpdatedDate(new Date());
		question.setUserByCreatedBy(questionFromDB.getUserByCreatedBy());
		question.setCreatedDate(questionFromDB.getCreatedDate());
		question.setBenchmark(questionFromDB.getBenchmark());
		return question;
	}
	
	@Override
	public Question createQuestionObject(Question question,	List<Double> quantifiers, User currentUser, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean) {
		String queTypeText = question.getQueTypeText();
		String jSONString = null;
		if(queTypeText != null || numberQuestionMapper != null || !bqBean.getBenchmarkItem().isEmpty())
			jSONString = createJSONString(quantifiers, queTypeText, question.getQuestionType(), otherOption, numberQuestionMapper, bqBean);
		question.setQueTypeText(jSONString);
		question.setUserByCreatedBy(currentUser);
		question.setUserByUpdatedBy(currentUser);
		question.setCreatedDate(new Date());
		question.setUpdatedDate(new Date());
		if(bqBean.getBenchmarkId() != null)
			question.setBenchmark(benchmarkService.findOne(bqBean.getBenchmarkId()));
		return question;
	}
	
	/**
	 * Create JSON string.
	 * @param quantifiers
	 * @param queTypeText
	 * @param queType
	 * @param otherOption
	 * @param numberQuestionMapper
	 * @param bqBean
	 * @return {@code String}
	 */
	private String createJSONString(List<Double> quantifiers, String queTypeText, String queType, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean) {
		String[] trimedQueTypeTextArray = {};
		if(queTypeText != null){
			String[] queTypeTextArray = queTypeText.split(",");
			List<String> newQueTypeTextArray = new ArrayList<>();
			for (String string : queTypeTextArray) {				
				newQueTypeTextArray.add(string.replaceAll("&#44;", ","));
			}
			queTypeTextArray = newQueTypeTextArray.toArray(new String[newQueTypeTextArray.size()]);
			trimedQueTypeTextArray = StringUtils.trimArrayElements(queTypeTextArray);
		}
		String jSONString = null;
		// Assume quantifier length = encoded String Array
		if(QuestionType.MULTCHOICE.toString().equals(queType)){
			if(!bqBean.isBenchmarkCheckboxMCQ()){
				JSONMultiChoiceQuestionMapper oneQuestionData = new JSONMultiChoiceQuestionMapper();
				int ctr = 0;
				int seqNo = 1;
				for(Double oneQuant : quantifiers){
					JSONQuestionOptionMapper oneOption = new JSONQuestionOptionMapper();
					oneOption.setQuantifier(oneQuant);
					oneOption.setSequenceNumber(seqNo);
					oneOption.setText(trimedQueTypeTextArray[ctr]);
					
					oneQuestionData.addOption(oneOption);
					ctr ++;
					seqNo++;
				}
				
				oneQuestionData.setOtherOption(otherOption);
				jSONString = Utility.convertObjectToJSONString(oneQuestionData);
			} else {
				JSONBenchmarkMultiChoiceQuestionMapper oneQuestionData = new JSONBenchmarkMultiChoiceQuestionMapper();
				List<BenchmarkItem> listOfBenchmarkItems = benchmarkService.findByIdIn(bqBean.getBenchmarkItem());
				int seqNo = 1;
				for (BenchmarkItem benchmarkItem : listOfBenchmarkItems) {
					JSONBenchmarkQuestionOptionMapper oneOption = new JSONBenchmarkQuestionOptionMapper();
					oneOption.setBenchmarkItemId(benchmarkItem.getId());
					oneOption.setText(benchmarkItem.getDisplayName());
					oneOption.setSequenceNumber(seqNo);
					
					// fetch benchmark item score to save in question option
					List<BenchmarkItemScore> listOfBenchmarkItemScores = benchmarkService.findByBenchmarkItemAndCurrentDate(benchmarkItem, new Date());
					if(listOfBenchmarkItemScores.size() == 1){
						BenchmarkItemScore benchmarkItemScore = listOfBenchmarkItemScores.get(0);
						oneOption.setBenchmarkItemScoreId(benchmarkItemScore.getId());
						if(benchmarkItemScore.getScore() != null)
							oneOption.setQuantifier(benchmarkItemScore.getScore().doubleValue());
					} else {
						oneOption.setQuantifier(Constants.NA_DOUBLE);
					}
					
					oneQuestionData.addOption(oneOption);
					
					seqNo++;
				}
				jSONString = Utility.convertObjectToJSONString(oneQuestionData);
			}
		}else if(QuestionType.NUMBER.toString().equals(queType) && numberQuestionMapper != null){
				jSONString = Utility.convertObjectToJSONString(numberQuestionMapper);
		}
		return jSONString;
	}

	@Override
	public List<Question> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces) {
		return questionRepository.findByWorkspaceIsNullOrWorkspaceIn(workspaces);
	}

	@Override
	public List<Question> getQuestionListByUser(User user) {
		  List<Question> listOfquestion = new ArrayList<>();
		  Set<Workspace> usersWorkspaces = workspaceService.getUserWorkspaceList(user);
		  if(!usersWorkspaces.isEmpty()){
			  listOfquestion = findByWorkspaceIsNullOrWorkspaceIn(usersWorkspaces);
		  }
		 return listOfquestion;
	}

	@Override
	public List<JSONQuestionOptionMapper> getQuestionOptions(Question question) {
		String queTypeText = question.getQueTypeText();
		String queType = question.getQuestionType();
		// Use the Options to send at JSP.
		List<JSONQuestionOptionMapper> options = null;
		if(queTypeText != null && queType.equals(QuestionType.MULTCHOICE.toString())){
				JSONMultiChoiceQuestionMapper mappedObject = getJSONMultiChoiceQuestionMapper(queTypeText); 
				options = mappedObject.getOptions().subList(0, mappedObject.getOptions().size()-1);				
		}
		return options;
	}
	
	@Override
	public JSONMultiChoiceQuestionMapper getJSONMultiChoiceQuestionMapper(String questionType) {
		JSONMultiChoiceQuestionMapper mappedObject = null;
		if (questionType != null) {
			mappedObject = Utility.convertJSONStringToObject(questionType, JSONMultiChoiceQuestionMapper.class); 
		}
		return mappedObject;
	}
	
	private JSONBenchmarkMultiChoiceQuestionMapper getJSONBenchmarkMultiChoiceQuestionMapper(String questionType) {
		JSONBenchmarkMultiChoiceQuestionMapper mappedObject = null;
		if (questionType != null) {
			mappedObject = Utility.convertJSONStringToObject(questionType, JSONBenchmarkMultiChoiceQuestionMapper.class); 
		}
		return mappedObject;
	}

	@Override
	public QuestionnaireQuestionBean createQQBeanForViewQuestion(Question question, String shortName, Integer qqId) {
		AssetBean ab = new AssetBean();
		ab.setShortName(shortName);
		QuestionBean questionBean = new QuestionBean(question, null);
		String queType = questionBean.getType();
		if(queType.equals(QuestionType.MULTCHOICE.toString())){
			if(question.getBenchmark() == null)
				questionBean.setJsonMultiChoiceQuestionMapper(getJSONMultiChoiceQuestionMapper(question.getQueTypeText()));
			else
				questionBean.setJsonMultiChoiceQuestionMapper(this.convertBenchmarkMCQintoNormalMCQ(getJSONBenchmarkMultiChoiceQuestionMapper(question.getQueTypeText())));
		}
		return new QuestionnaireQuestionBean(qqId, questionBean, ab);		
	}

	@Override
	public JSONMultiChoiceQuestionMapper convertBenchmarkMCQintoNormalMCQ(JSONBenchmarkMultiChoiceQuestionMapper benchmarkMultiChoiceQuestionMapper) {
		JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = new JSONMultiChoiceQuestionMapper();
		for (JSONBenchmarkQuestionOptionMapper jsonBenchmarkQuestionOptionMapper : benchmarkMultiChoiceQuestionMapper.getOptions()) {
			JSONQuestionOptionMapper jsonQuestionOptionMapper = new JSONQuestionOptionMapper();
			jsonQuestionOptionMapper.setText(jsonBenchmarkQuestionOptionMapper.getText());
			jsonQuestionOptionMapper.setQuantifier(getBenchmarkItemScore(jsonBenchmarkQuestionOptionMapper.getBenchmarkItemId()));
			jsonQuestionOptionMapper.setSequenceNumber(jsonBenchmarkQuestionOptionMapper.getSequenceNumber());
			jsonMultiChoiceQuestionMapper.addOption(jsonQuestionOptionMapper);
		}
		return jsonMultiChoiceQuestionMapper;
	}
	
	/**
	 * Get latest benchmark Item Score
	 * @param benchmarkItemId
	 * @return score
	 */
	private Double getBenchmarkItemScore(Integer benchmarkItemId) {	
	    Double score ; 
	    BenchmarkItemScore benchmarkItemScore = benchmarkItemScoreService.findByBenchmarkItemAndCurrentDate(benchmarkItemService.findById(benchmarkItemId), new Date());
		
		if(benchmarkItemScore !=  null && benchmarkItemScore.getScore() != null){
			score =benchmarkItemScore.getScore().doubleValue();
	    } else {
	    	score = Constants.NA_DOUBLE;
	    }
		return score;
	}

	@Override
	public List<Question> getExtractedListOfQuestions(Questionnaire qe, Integer parentParameterId) {
		List<Question> listOfQuestions = findByWorkspaceIsNullOrWorkspace(workspaceService.findOne(qe.getWorkspace().getId()));
		// In this list of questions remove those questions which are already a part of the parent parameter.
		if(parentParameterId > 0){
			List<ParameterFunction> parameterFunctions = parameterFunctionService.findByParameterByParentParameterId(parameterService.findOne(parentParameterId));
			for (ParameterFunction parameterFunction : parameterFunctions) {
				if (parameterFunction.getQuestion() != null){
					listOfQuestions.remove(parameterFunction.getQuestion());
				}
			}
		}
		return listOfQuestions;
	}

	@Override
	public Set<QuestionBean> getSetOfQuestionBean(Parameter parameter, List<ParameterFunction> listOfFunctionsToBeSaved) {
		Set<QuestionBean> setOfSavedQuestions = new HashSet<>();
		for (ParameterFunction pf : listOfFunctionsToBeSaved){
			setOfSavedQuestions.add(new QuestionBean(pf.getQuestion(), new ParameterBean(parameter)));
		}
		return setOfSavedQuestions;
	}

	@Override
	public Question createVariantQuestion(Question question) {
		Question newQuestion = new Question();
		newQuestion.setCategory(question.getCategory());
		newQuestion.setHelpText(question.getHelpText());
		newQuestion.setLevel(question.getLevel());
		newQuestion.setTitle(question.getTitle());
		newQuestion.setQuestionMode(question.getQuestionMode());
		newQuestion.setQuestionType(question.getQuestionType());
		newQuestion.setQueTypeText(question.getQueTypeText());
		newQuestion.setQuestion(question);
		if(question.getBenchmark() != null)
			newQuestion.setBenchmark(question.getBenchmark());
		return newQuestion;
	}

	@Override
	public List<Question> findByWorkspaceAndDisplayName(Workspace workspace, String displayName) {
		return questionRepository.findByWorkspaceAndDisplayName(workspace, displayName);
	}

	@Override
	public Question prepareQuestionWithError(Question question, List<Double> quantifiers, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean) {
		String queTypeText = question.getQueTypeText(); 
    	String jSONString = null;
		if(queTypeText != null || !bqBean.getBenchmarkItem().isEmpty())
			jSONString = createJSONString(quantifiers, queTypeText, question.getQuestionType(), otherOption, numberQuestionMapper, bqBean);
		question.setQueTypeText(jSONString);
		if(question.getWorkspace() != null){
			question.setWorkspace(workspaceService.findOne(question.getWorkspace().getId()));
		}
		if(question.getCategory() != null){
			question.setCategory(categoryService.findOne(question.getCategory().getId()));
		}
		return question;
	}

	@Override
	public List<Question> findByIdIn(Set<Integer> setOfQuestionIds) {
		return questionRepository.findByIdIn(setOfQuestionIds);
	}
	

	@Override
	public Question findFullyLoaded(Integer id) {
		Question question= questionRepository.findOne(id);
		if(question.getBenchmark() != null)
			question.getBenchmark().getName();
		loadQuestionParameterDetails(question);
		return question;
	}
	
	private void loadQuestionParameterDetails(Question question) {
	
		for (ParameterFunction oneFunction : question.getParameterFunctions()){
			// in which case the child is either a question or 
			if (oneFunction.getQuestion() != null){
				oneFunction.getQuestion().getTitle();
			}
			if (oneFunction.getParameterByChildparameterId() != null){
				oneFunction.getParameterByChildparameterId().getUniqueName();
			}
		}
		
		// Load the cases where it is the child parameter
		for (ParameterFunction oneFunction : question.getParameterFunctions()){
			if (oneFunction.getQuestion() != null){
				oneFunction.getQuestion().getTitle();
			}
			if (oneFunction.getParameterByChildparameterId() != null){
				oneFunction.getParameterByChildparameterId().getUniqueName();
			}
		}
	}

	@Override
	public List<Question> findByWorkspace(Workspace workspace) {
		return questionRepository.findByWorkspace(workspace);
	}

	@Override
	public List<Question> findByWorkspaceAndQuestionType(
			Workspace workspace, String quesType) {
		return questionRepository.findByWorkspaceIsNullOrWorkspaceAndQuestionType(workspace, quesType);
	}

	@Override
	public Map<Category, List<Question>> getMapOfQuestionByCategory(Workspace workspace) {
		Map<Category, List<Question>> mapToReturn = new HashMap<>();
		List<Question> listOfQuestions = questionRepository.findByWorkspaceIsNullOrWorkspaceAndQuestionTypeAndBenchmarkIsNull(workspace, QuestionType.MULTCHOICE.toString());
		Set<Category> setOfCategory = new HashSet<>();
		for (Question ques : listOfQuestions) {
			setOfCategory.add(ques.getCategory());
		}
		for (Category category : setOfCategory) {
			List<Question> listToBeAdded = new ArrayList<>();
			for (Question question : listOfQuestions) {
				if(category == null && question.getCategory() == null){
					listToBeAdded.add(question);
				}
				if(category != null && question.getCategory() != null && question.getCategory().equals(category)){
					listToBeAdded.add(question);
				}
			}
			mapToReturn.put(category, listToBeAdded);
		}
		return mapToReturn;
	}

	@Override
	public Category createCategoryIfNotExist(String categoryName) {
		Category category =categoryService.getCategoryFromName(categoryName);		
		//if category does'nt exist, then create new category
		if(category==null){
			category=new Category();
			category.setName(categoryName);
			category.setCreatedDate(new Date());
			category.setUpdatedDate(new Date());
			category.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
			category.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
			categoryService.save(category);			
		}
		return category;
	}
	
	
	/**@author dhruv.sood
     * Find list of question as per question type and corresponding workspace
     * @param workspace
     * @return {@code List<Question>}
     */
	@Override
	public List<Question> findQuestionListByWorkspaceAndQuestionType(Workspace workspace, String questionType) {
		return questionRepository.findByWorkspaceAndQuestionType(workspace, questionType);
	}	
	
	

	/**@author dhruv.sood
     * Find list of question as per question type and corresponding workspace
     * @param setOfWorkspaces
     * @param category
     * @return {@code List<Question>}
     */
	@Override
	public List<Question> findByWorkspaceIsNullOrWorkspaceInAndCategoryNot(Set<Workspace> setOfWorkspaces, Category category) {
		return questionRepository.findByWorkspaceIsNullOrWorkspaceInAndCategoryNot(setOfWorkspaces, category);
	}

	@Override
	public List<Question> findByWorkspaceInAndCategory(Set<Workspace> setOfWorkspaces, Category category) {
		return questionRepository.findByWorkspaceInAndCategory(setOfWorkspaces, category);
	}	
	
	@Override
	public Map<Integer, QuestionResponseSearchWrapper> getQuestionValueFromElasticsearch(Integer questionnaireId,
			QuestionnaireParameter qp, Set<Integer> assetIds) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/ques/value";

		Map<Integer, QuestionResponseSearchWrapper> mapOfQuestionAndValue = new HashMap<>();
		Map<Integer, QuestionResponseSearchWrapper> mapToReturn = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		
		SearchConfig searchConfig = setSearchConfig(questionnaireId, qp, assetIds);
		
		try {
			mapOfQuestionAndValue = restTemplate.postForObject(uri, searchConfig, Map.class);
			mapToReturn = mapper.convertValue(mapOfQuestionAndValue, new TypeReference<Map<Integer, QuestionResponseSearchWrapper>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in QuestionServiceImpl :: getQuestionValueFromElasticsearch() :", e);
		}
		return mapToReturn;
	}

	private SearchConfig setSearchConfig(Integer questionnaireId, QuestionnaireParameter qp, Set<Integer> assetIds) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		
		SearchConfig searchConfig=new SearchConfig();
		
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		
		searchConfig.setListOfIds(new ArrayList<>(assetIds));
		searchConfig.setQuestionnaireId(questionnaireId);
		searchConfig.setQpId(qp.getId());
		return searchConfig;
	}
}
