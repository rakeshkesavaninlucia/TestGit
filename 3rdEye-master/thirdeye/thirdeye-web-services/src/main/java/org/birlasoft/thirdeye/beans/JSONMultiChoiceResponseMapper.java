package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;

/**
 * This object will be used for JSON serialization
 * @author samar.gupta
 */
public class JSONMultiChoiceResponseMapper implements QuantifiableResponse{

	private List<JSONQuestionOptionMapper> options = new ArrayList<JSONQuestionOptionMapper>();
	private String otherOptionResponseText;
	private Double score;
	
	public JSONMultiChoiceResponseMapper() {
	}

	public List<JSONQuestionOptionMapper> getOptions() {
		return options;
	}

	public void setOptions(List<JSONQuestionOptionMapper> options) {
		this.options = options;
	}
	
	public void addOption (JSONQuestionOptionMapper oneOption){
		if (oneOption != null){
			options.add(oneOption);
		}
	}

	public String getOtherOptionResponseText() {
		return otherOptionResponseText;
	}

	public void setOtherOptionResponseText(String otherOptionResponseText) {
		this.otherOptionResponseText = otherOptionResponseText;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		if (options != null && !options.isEmpty()){
			for (JSONQuestionOptionMapper oneOption : options){
				// There will only be one option selected here
				valueToBeReturned = valueToBeReturned.add(new BigDecimal(oneOption.getQuantifier()));
			}
		} else if (score != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(score));
		}
		
		
		return valueToBeReturned;
	}
}
