package org.birlasoft.thirdeye.beans.gng;


import java.util.HashSet;
import java.util.Set;

/**
 * 
 * GNGWrapper class
 *
 */

public class GNGWrapper {

	private Set<GNGValueWrapper> assetsValues = new HashSet<>();

	public Set<GNGValueWrapper> getAssetsValues() {
		return assetsValues;
	}

	public void setAssetsValues(Set<GNGValueWrapper> assetsValues) {
		this.assetsValues = assetsValues;
	}


}
