package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.widgets.BaseWidgetJSONConfig;
import org.birlasoft.thirdeye.beans.widgets.WidgetHelper;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.WidgetTypes;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.DashboardRepository;
import org.birlasoft.thirdeye.repositories.WidgetRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Service Implementation class for DashboardController
 *
 */
@Service("dashboardService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class DashboardServiceImpl implements DashboardService{

	@Autowired
	DashboardRepository dashboardRepo;
	
	@Autowired
	WidgetRepository widgetRepo;
	
	@Autowired
	CustomUserDetailsService userDetailsService;
	
	@Autowired
	WorkspaceService workspaceService;
	
	@Override
	public Dashboard findOne (int idOfDashboard){
		return dashboardRepo.findOne(idOfDashboard);
	}
	
	@Override
	public List<BaseWidgetJSONConfig> addNewWidgetToDashboard(Dashboard activeDashboard, BaseWidgetJSONConfig newWidgetConfig) {
		
		// Although we know the widget type we make the initial insert
		// using the base config rather than the implementation classes
		BaseWidgetJSONConfig baseConfig = new BaseWidgetJSONConfig();
		baseConfig.setWidgetType(newWidgetConfig.getWidgetType() != null ? newWidgetConfig.getWidgetType() : WidgetTypes.FACETGRAPH);
		baseConfig.setTitle(newWidgetConfig.getTitle());
		baseConfig.setSequenceNumber(activeDashboard.getWidgets().size()+1);
				
		Widget newWidgetForDashboard = new Widget();
		newWidgetForDashboard.setDashboard(activeDashboard);
		newWidgetForDashboard.setSequenceNumber(baseConfig.getSequenceNumber());
		newWidgetForDashboard.setWidgetConfig(Utility.convertObjectToJSONString(baseConfig));
		newWidgetForDashboard.setWidgetType(baseConfig.getWidgetType().name());
		
		// Put the new widget into the model 
		WidgetHelper widgetHelper = new WidgetHelper();
		List<BaseWidgetJSONConfig> widgetConfigs = new ArrayList<>();
		widgetConfigs.add(widgetHelper.getWidgetBeanFromWidget(widgetRepo.saveAndFlush(newWidgetForDashboard)));
		
		return widgetConfigs;
	}
	
	
	@Override
	public Widget findOneWidget(Integer idOfWidget, boolean loadDashboard) {
		Widget w = widgetRepo.findOne(idOfWidget);
		
		if (loadDashboard){
			w.getDashboard().getDashboardName();
		}
		
		return w;
	}
	
	@Override
	public Widget saveWidget(Widget w) {
		return widgetRepo.save(w);
	}
	
	@Override
	public Dashboard saveOrUpdate(Dashboard incomingDashboard) {
		Dashboard toSaveOrUpdate;
		if (incomingDashboard.getId() == null){
			toSaveOrUpdate = new Dashboard(incomingDashboard.getDashboardName());
			toSaveOrUpdate.setWorkspace(userDetailsService.getActiveWorkSpaceForUser());
		} else {
			toSaveOrUpdate = findOne(incomingDashboard.getId());
			toSaveOrUpdate.setDashboardName(incomingDashboard.getDashboardName());
		}
		
		return dashboardRepo.save(toSaveOrUpdate);
	}
	
	@Override
	public Set<Dashboard> listDashboardForActiveWorkspace() {
		Workspace activeWorkspace = userDetailsService.getActiveWorkSpaceForUser();
		activeWorkspace = workspaceService.findOne(activeWorkspace.getId());
		activeWorkspace.getDashboards().size();
		
		
		return activeWorkspace.getDashboards();
	}
	
	@Override
	public void delete(List<Dashboard> listOfDelete) {
		for (Dashboard oneDashboard : listOfDelete){
			widgetRepo.delete(oneDashboard.getWidgets());
			
			dashboardRepo.delete(oneDashboard);
		}
	}
	
	@Override
	public void deleteWidget(List<Widget> asList) {
		widgetRepo.delete(asList);
	}

	@Override
	public List<BaseWidgetJSONConfig> viewDashboard(Dashboard activeDashboard) {
		// Put the new widget into the model 
		WidgetHelper widgetHelper = new WidgetHelper();
		List<BaseWidgetJSONConfig> listWidgetConfigs = new ArrayList<>();
		
		for (Widget oneWidget : activeDashboard.getWidgets()){
			listWidgetConfigs.add(widgetHelper.getWidgetBeanFromWidget(oneWidget));
		}
		
		// Sort By Sequence Number 
		if(!listWidgetConfigs.isEmpty())
			Collections.sort(listWidgetConfigs, new SequenceNumberComparator());
		
		return listWidgetConfigs;
		
	}
}
