package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.entity.Questionnaire;

public interface TcoResponseService {

	public List<ChartOfAccountBean> extractChartOfAccounts(Questionnaire questionnaire);

}
