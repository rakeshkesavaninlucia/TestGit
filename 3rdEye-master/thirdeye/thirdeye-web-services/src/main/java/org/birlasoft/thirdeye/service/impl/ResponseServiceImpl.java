package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.ResponseDataRepository;
import org.birlasoft.thirdeye.repositories.ResponseRepository;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ResponseServiceImpl implements ResponseService {
	
	@Autowired
	private ResponseRepository responseRepository;
	@Autowired
	private ResponseDataRepository responseDataRepository;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private QuestionService questionService;
	
	private ResponseData responseData;
	
	@Override
	public List<Response> findByQuestionnaire(Questionnaire qe, boolean loaded) {
		List<Response> listOfResponse = responseRepository.findByQuestionnaire(qe);
		
		if(loaded){
			for (Response response : listOfResponse){
				Set<QuestionnaireParameter> questionnaireParameters = response.getQuestionnaire().getQuestionnaireParameters();
				response.getQuestionnaire().getQuestionnaireParameters().size();
				for (QuestionnaireParameter qp : questionnaireParameters) {
					qp.getParameterByParameterId().getDisplayName();
				}
				response.getQuestionnaire().getQuestionnaireQuestions().size();
				Set<QuestionnaireQuestion> questionnaireQuestions = response.getQuestionnaire().getQuestionnaireQuestions();
				for (QuestionnaireQuestion questionnaireQuestion : questionnaireQuestions) {
					questionnaireQuestion.getQuestion().getQuestionType();
					questionnaireQuestion.getQuestion().getTitle();
				}
				response.getQuestionnaire().getQuestionnaireAssets().size();
				response.getQuestionnaire().getQuestionnaireQuestions().size();
				Set<QuestionnaireQuestion> qqs = response.getQuestionnaire().getQuestionnaireQuestions();
				for (QuestionnaireQuestion qq : qqs) {
					qq.getResponseDatas().size();
				}
			}	
		}
		return listOfResponse;
	}

	@Override
	public Response save(Response r) {
		return responseRepository.save(r);
	}
	
	@Override
	public Response findOne(Integer responseId) {
		return responseRepository.findOne(responseId);
	}

	@Override
	public Response createNewResponseObject(Questionnaire qe, User currentUser) {
		// Create a new response and save it
		Response newResponse = new Response();
		newResponse.setCreatedDate(new Date());
		newResponse.setUpdatedDate(new Date());
		newResponse.setUserByCreatedBy(currentUser);
		newResponse.setUserByUpdatedBy(currentUser);
		newResponse.setQuestionnaire(qe);
		return newResponse;
	}

	@Override
	public Map<Integer, Integer> getMapForCompletionPercentageOfQe(List<Questionnaire> listOfquestionnaires) {
		Map<Integer,Integer> completionPercentage = new HashMap<>();
		
		for (Questionnaire oneQe : listOfquestionnaires){
			int numberOfResponses = oneQe.getResponses().size();
			List<QuestionnaireQuestion> qq = questionnaireQuestionService.findByQuestionnaire(oneQe, true);
			int numberOfQQ = qq.size();
			
			int numberOfResponseData = 0;
			for (QuestionnaireQuestion oneQQ : qq){
				numberOfResponseData = numberOfResponseData + oneQQ.getResponseDatas().size();
			}
			
			int finalPercentage = 0;
			if (numberOfResponses>0 && numberOfQQ >0){
				finalPercentage = (numberOfResponseData*100)/(numberOfQQ * numberOfResponses);
			}
			
			completionPercentage.put(oneQe.getId(), finalPercentage);
		}
		return completionPercentage;
	}

	private ResponseData insertResponseIntoResponseData(String[] newValuesToBeStored, QuestionnaireQuestion qqAnswered, ResponseData alreadyRespondedData, String otherOptionText) {
		// Need to find out options that were selected
		
		String jSONStringOfQuestion = qqAnswered.getQuestion().getQueTypeText();

		String queType = qqAnswered.getQuestion().getQuestionType();
		if (jSONStringOfQuestion == null){
			if(queType.equals(QuestionType.TEXT.toString())){
				JSONTextResponseMapper jsonTextResponseMapper = new JSONTextResponseMapper();
				jsonTextResponseMapper.setResponseText(newValuesToBeStored[0]);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonTextResponseMapper));
			}else if (queType.equals(QuestionType.PARATEXT.toString())){
				JSONParaTextResponseMapper jsonParaTextResponseMapper = new JSONParaTextResponseMapper();
				jsonParaTextResponseMapper.setResponseParaText(newValuesToBeStored[0]);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonParaTextResponseMapper));
			}else if (queType.equals(QuestionType.DATE.toString())){
				JSONDateResponseMapper jsonDateResponseMapper = new JSONDateResponseMapper();
				jsonDateResponseMapper.setResponseDate(newValuesToBeStored[0]);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonDateResponseMapper));
			}
		}else if(jSONStringOfQuestion != null){
			if(queType.equals(QuestionType.MULTCHOICE.toString())){
				JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper;
				if(qqAnswered.getQuestion().getBenchmark() == null)
					jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONMultiChoiceQuestionMapper.class);
				else {
					jsonMultiChoiceQuestionMapper = questionService.convertBenchmarkMCQintoNormalMCQ(Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONBenchmarkMultiChoiceQuestionMapper.class));
				}
				JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = new JSONMultiChoiceResponseMapper();
				for (String oneVal : newValuesToBeStored){
						// We need to see which options were selected
						for (JSONQuestionOptionMapper oneOption : jsonMultiChoiceQuestionMapper.getOptions()){
							if (oneOption.getText().equals(oneVal)){
								jsonMultiChoiceResponseMapper.addOption(oneOption);
							}
						}
				}
				if(newValuesToBeStored[0].equalsIgnoreCase(Constants.OTHER_TEXT)){
						jsonMultiChoiceResponseMapper.setOtherOptionResponseText(otherOptionText);
				}
				
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonMultiChoiceResponseMapper));
			}
			else if(queType.equals(QuestionType.NUMBER.toString()) ){
				JSONNumberResponseMapper jsonNumberResponseMapper = new JSONNumberResponseMapper();
				//jsonNumberResponseMapper.setQuantifier(!newValuesToBeStored[0].isEmpty() ? Double.parseDouble(newValuesToBeStored[0]) : null);
				jsonNumberResponseMapper.setResponseNumber(!newValuesToBeStored[0].isEmpty() ? Double.parseDouble(newValuesToBeStored[0]) : null);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonNumberResponseMapper));
			}
			else if(queType.equals(QuestionType.CURRENCY.toString())){
				JSONNumberResponseMapper jsonNumberResponseMapper = new JSONNumberResponseMapper();
				jsonNumberResponseMapper.setQuantifier(!newValuesToBeStored[0].isEmpty() ? Double.parseDouble(newValuesToBeStored[0]) : null);
				jsonNumberResponseMapper.setResponseNumber(!newValuesToBeStored[0].isEmpty() ? Double.parseDouble(newValuesToBeStored[0]) : null);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonNumberResponseMapper));
			}
		}
		
		return alreadyRespondedData;
	}

	@Override
	public List<ResponseData> findByResponse(Response response) {
		return responseDataRepository.findByResponse(response);
	}

	@Override
	public List<QuestionnaireQuestionBean> prepareQQsListForNQRQ(Questionnaire qe) {
		List<QuestionnaireQuestionBean> qqbeanListForGUI = new ArrayList<>();
		List<Response> responses = this.findByQuestionnaire(qe, true);
		for (Response response : responses) {
			Set<ResponseData> responseDatas = response.getResponseDatas();
			for (ResponseData responseData : responseDatas) {
				qqbeanListForGUI.add(new QuestionnaireQuestionBean(responseData.getQuestionnaireQuestion()));
			}
		}
		
		Collections.sort(qqbeanListForGUI, new SequenceNumberComparator());
		return qqbeanListForGUI;
	}

	@Override
	public List<Response> findByQuestionnaireIn(List<Questionnaire> qes) {
		return responseRepository.findByQuestionnaireIn(qes);
	}

	@Override
	public Response findByIdLoadedResponseDatas(Integer id) {
		return responseRepository.findByIdLoadedResponseDatas(id);
	}
	
	@Override
	public List<ResponseData> getListOfResponseData(String[] newValuesToBeStored, Integer idOfQQ, ResponseData alreadyRespondedData, String otherOptionText){
		// get QuestionnaireQuestion
		QuestionnaireQuestion qq = questionnaireQuestionService.findOne(idOfQQ,true);

		// To set responseData for even the duplicate record (i.e duplicate questionId for one questionnaireassetId),
		// get all qq where group of questionId and questionnaireAssetId is same.  
		List<QuestionnaireQuestion> listOfqq = questionnaireQuestionService.findByQuestionIdAndQuestionnaireAssetId(qq.getQuestionnaireAsset(), qq.getQuestion());
		List<ResponseData> listOfResponseData =new ArrayList<>();
		
		for(QuestionnaireQuestion oneqq: listOfqq){
			responseData = responseDataRepository.findByQuestionnaireQuestionId(oneqq);
			if(responseData != null){
				//update responseData even for duplicate QuestionnaireQuestion
				responseData.setResponseData(alreadyRespondedData.getResponseData());
			}
			else{
				//Set response and questionnaireQuestion if responseData is not for duplicate questionnaireQuestion
				responseData = new ResponseData();
				responseData.setQuestionnaireQuestion(oneqq);
				responseData.setResponse(alreadyRespondedData.getResponse());
			}
			alreadyRespondedData =  insertResponseIntoResponseData(newValuesToBeStored, oneqq, responseData, otherOptionText );
			listOfResponseData.add(alreadyRespondedData);
		}
		return listOfResponseData;
	}
	
}
