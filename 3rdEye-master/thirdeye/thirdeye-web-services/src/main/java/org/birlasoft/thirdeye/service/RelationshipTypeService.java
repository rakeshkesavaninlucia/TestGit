package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.entity.Workspace;

public interface RelationshipTypeService {

	public RelationshipType saveOrUpdate(RelationshipType incomingRelationshipType);
	public RelationshipType findOne(Integer idOfRelationshipType);
	public List<RelationshipType> findAll();
	public List<RelationshipType> findByWorkspace(Workspace workspace);
	public List<RelationshipType> findByWorkspaceIsNullOrWorkspace(Workspace workspace);
	public RelationshipType createNewRelationshipType(RelationshipType incomingRelationshipType);
	public RelationshipType updateRelationshipType(RelationshipType incomingRelationshipType);
}
