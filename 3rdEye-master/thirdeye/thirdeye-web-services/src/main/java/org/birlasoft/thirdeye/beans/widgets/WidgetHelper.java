package org.birlasoft.thirdeye.beans.widgets;

import org.birlasoft.thirdeye.constant.WidgetTypes;
import org.birlasoft.thirdeye.entity.Widget;

/**
 * 
 * This is the class for Widget Helper
 *
 */
public class WidgetHelper {

	/**
	 * 
	 * @param w
	 * @return
	 */
	public <T> T getWidgetBeanFromWidget(Widget w){
		
		WidgetTypes widgetType = WidgetTypes.valueOf(w.getWidgetType());
		
		if(widgetType.equals(widgetType.FACETGRAPH)){
			FacetWidgetJSONConfig widget = new FacetWidgetJSONConfig(w);
			return (T) widget;
		}	
		return null;
	}	
}
