package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.beans.aid.RelatedAssetBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;

/**
 * Service {@code interface} for relationship
 *
 */
public interface RelationshipTemplateService {

	/**
	 * Save or update {@link RelationshipTemplate}
	 * @param incomingRelationshipTemplate
	 * @return {@code RelationshipTemplate}
	 */
	public RelationshipTemplate saveOrUpdate(RelationshipTemplate incomingRelationshipTemplate);
	/**
	 * Get {@link RelationshipTemplate} by relationship template id
	 * @param idOfRelationshipTemplate
	 * @return {@code RelationshipTemplate}
	 */
	public RelationshipTemplate findOne(Integer idOfRelationshipTemplate);
	/**
	 * Get all relationship templates
	 * @return {@code List<RelationshipTemplate>}
	 */
	public List<RelationshipTemplate> findAll();
	/**
	 * Create new relationship template object.
	 * @param assetTemplateBean
	 * @param assetTemplate
	 * @return {@code RelationshipTemplate}
	 */
	public RelationshipTemplate createNewRelationshipTemplateObject(AssetTemplateBean assetTemplateBean, AssetTemplate assetTemplate);
	/**
	 * Get {@code List} of {@link RelationshipTemplate} by asset template
	 * @param assetTemplate
	 * @return {@code List<RelationshipTemplate>}
	 */
	public List<RelationshipTemplate> findByAssetTemplate(AssetTemplate assetTemplate);
	/**
	 * Update relationship template object.
	 * @param assetTemplateBean
	 * @return {@code RelationshipTemplate}
	 */
	public RelationshipTemplate updateRelationshipTemplateObject(AssetTemplateBean assetTemplateBean);
	/**
	 * Set relationship template data into asset template bean.
	 * @param assetTemplate
	 * @param assetTemplateBean
	 * @return {@code AssetTemplateBean}
	 */
	public AssetTemplateBean setRelationshipIntoBean(AssetTemplate assetTemplate, AssetTemplateBean assetTemplateBean);
	/**
	 * Find relationship templates by asset templates.
	 * @param setOfAssetTemplate
	 * @return {@code List<RelationshipTemplate>}
	 */
	public List<RelationshipTemplate> findByAssetTemplateIn(Set<AssetTemplate> setOfAssetTemplate);
	
	//  methods for Relationship Asset Data 
	/**
	 * Save relationship asset data
	 * @param relationshipAssetData
	 * @return {@code RelationshipAssetData}
	 */
	public RelationshipAssetData saveRelationshipAssetData(RelationshipAssetData relationshipAssetData);
	/**
	 * Create new relationship asset data object.
	 * @param savedAsset
	 * @param parentAsset
	 * @param childAsset
	 * @return {@code RelationshipAssetData}
	 */
	public RelationshipAssetData createNewRelationshipAssetDataObject(Asset savedAsset, Asset parentAsset, Asset childAsset);
	/**
	 * Find parent/child relationship by asset.
	 * @param assets
	 * @return {@code Set<RelationshipAssetData>}
	 */
	public Set<RelationshipAssetData> findByAssetByAssetIdIn(Set<Asset> assets);
	/**
	 * Check relationship template have template column or not.
	 * @param assetTemplate
	 * @return {@code boolean}
	 */
	public boolean isHasTemplateColumn(AssetTemplate assetTemplate);
	/**
	 * Get {@code List} of {@link RelationshipAssetData} by parent asset
	 * @param asset
	 * @return {@code List<RelationshipAssetData>}
	 */
	public List<RelationshipAssetData> findByAssetByParentAssetId(Asset asset);
	
	/**
	 * Get {@code List} of {@link RelationshipAssetData} by child asset
	 * @param asset
	 * @return {@code List<RelationshipAssetData>}
	 */
	public List<RelationshipAssetData> findByAssetByChildAssetId(Asset asset);
	/**
	 * Get {@code List} of all inbound assets for AID graph
	 * @param asset
	 * @return
	 */
	public List<RelatedAssetBean> fetchInBoundRelationships(Asset asset);
	/**
	 * Get {@code List} of all outbound assets for AID graph
	 * @param asset
	 * @return
	 */
	public List<RelatedAssetBean> fetchOutBoundRelationships(Asset asset);
	
	/**
	 * Get root relationships of an asset for relationship tree
	 * @param asset
	 * @return
	 */
	public List<RelationshipAssetDataBean> getRootRelationshipsOfAsset(Asset asset);
	
	/**
	 * @param findByAssetTemplate
	 * @return
	 */
	public void deleteInBatch(List<RelationshipTemplate> findByAssetTemplate);
	
}
