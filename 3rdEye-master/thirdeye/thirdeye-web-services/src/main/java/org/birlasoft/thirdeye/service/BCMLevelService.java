package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Interface for BCMLevel Service.
 */

/**
 * @author shagun.sharma
 *
 */
public interface BCMLevelService {

	/**
	 * method to save {@link BcmLevel}
	 * @param bcmLevel
	 * @return {@link BcmLevel}
	 */
	public BcmLevel save(BcmLevel bcmLevel);
	
	/**
	 * Get {@code List} of {@link BcmLevel} by {@link BCm}
	 * @param bcm
	 * @param loaded
	 * @return {@code List<BcmLevel>}
	 */
	public List<BcmLevel> findByBcm(Bcm bcm, boolean loaded);
	
	/**
	 * Generate BCM tree from {@code List} of {@link BcmLevel}
	 * @param listOfLoadedBcmLevels
	 * @return {@code List<BcmLevelBean>}
	 */
	public List<BcmLevelBean> generateLevelTreeFromBcmLevels(List<BcmLevel> listOfLoadedBcmLevels);
	
	/**
	 * Find {@link BcmLevel} by {@code parentLevelId }
	 * @param parentLevelId
	 * @return {@link BcmLevel}
	 */
	public BcmLevel findOne(Integer parentLevelId);
	
	/**
	 * Get {@code List} of {@link BcmLevel} by parent {@link BcmLevel}
	 * @param parentBcmLevel
	 * @return {@code List<BcmLevel>}
	 */
	public List<BcmLevel> findByBcmLevel(BcmLevel parentBcmLevel);
	
	/**
	 * Delete BCM Level by {@code bcmId} and {@code levelId}
	 * @param bcmId
	 * @param levelId
	 */
	public void deleteLevel(Integer bcmId, Integer levelId);
	
	/**
	 * method to delete {@link BCMLevel}
	 * @param bl
	 */
	public void delete(BcmLevel bl);
	
	/**
	 * find {@link BcmLevel} by {@link BCM} , {@code levelNumber} 
	 * @param bcm
	 * @param levelNumber
	 * @return {@link BcmLevel}
	 */
	public BcmLevel findTopByBcmAndLevelNumberOrderBySequenceNumberDesc(Bcm bcm, Integer levelNumber);
	
	/**
	 * find {@link BcmLevel} by {@link BcmLevel} order by sequence number descending.
	 * @param bcmLevel
	 * @return {@link BcmLevel}
	 */
	public BcmLevel findTopByBcmLevelOrderBySequenceNumberDesc(BcmLevel bcmLevel);
	
	
	/**
	 * Save {@code List} of {@link BcmLevel}
	 * @param bcmLevels
	 * @return {@code List} of {@link BcmLevel}
	 */
	public List<BcmLevel> save(List<BcmLevel> bcmLevels);
	
	/**
	 * find {@link BcmLevel} by {@code bcmLevelId}
	 * @param bcmLevelId
	 * @return {@link BcmLevel}
	 */
	public BcmLevel findOneLoadedBcmLevel(Integer bcmLevelId);
	
	/**
	 * Create {@link BcmLevel}
	 * @param bcmLevels
	 * @param newBcm
	 */
	public void createBcmLevel(List<BcmLevel> bcmLevels, Bcm newBcm);
	
	/**
	 * find {@code List} of {@link BcmLevel} by {@link BCM} and {@code set} of {@code levelNumber}
	 * @param bcm
	 * @param levelNumber
	 * @param loaded
	 * @return {@code List} of {@link BcmLevel}
	 */
	public List<BcmLevel> findByBcmAndLevelNumberIn(Bcm bcm, Set<Integer> levelNumber, boolean loaded);
	
	/**
	 * method to save {@link BcmLevel}
	 * @param bcmLevelBean
	 * @param bcmId
	 * @param parentLevelId
	 * @return
	 */
	public BcmLevel saveBcmLevel(BcmLevelBean bcmLevelBean, Integer bcmId,Integer parentLevelId);

	/**
	 * Update Bcm level sequence and parent level
	 * @param bcmLevel
	 * @param parentBcmLevel
	 * @param position
	 */
	public void updateBcmSequence(BcmLevel bcmLevel, BcmLevel parentBcmLevel, Integer position);
	
	/** Find List Of BcmLevel1 By Active Bcm And BcmLevel1
	 * @param bcm
	 * @param levelNumber
	 * @return BcmLevel1
	 */
	public List<BcmLevel> findByBcmAndLevelNumberIn(List<Bcm> bcm,Integer levelNumber);
	
	/** find List Of Bcm Level By Multiple BcmLevelIDs
	 * @param parentLevelId
	 * @return ListOFLevelsByMultipleLevelIds
	 */
	public List<BcmLevel> findByBcmLevelIdIn(Integer[] parentLevelId);
	
	
	
}
