package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.JSONQuestionAnswerMapper;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;

/**
 *	Service interface for user.
 */
public interface UserService {
	/**
	 * Get {@code User} By {@code Email}
	 * @param email
	 * @return {@code User} Object
	 */
	public User findUserByEmail(String email);
	/**
	 * Get {@code User} By {@code Id}
	 * @param id
	 * @return {@code User}
	 */
	public User findUserById(Integer id);
	/**
	 * save {@code User} Object
	 * @param user
	 * @return
	 */
	public User save(User user);
	/**
	 * Find all list of user.
	 * @return {@code List<User>}
	 */
	public List<User> findAll();
	/**
	 * create new {@code user} object
	 * @param newUser
	 * @param currentUser
	 * @return {@code User}
	 */
	public User createUserObject(User newUser, User currentUser);
	/**
	 * Update user object.
	 * @param userToUpdate
	 * @param currentUser
	 * @return {@code User}
	 */
	public User updateUserObject(User userToUpdate, User currentUser);
	/** Create Map For User Roles.
	 * @param userRoles
	 * @return {@code Map<Integer, Integer>}
	 */
	public Map<Integer, Integer> createMapForUserRoles(Set<UserRole> userRoles);
	/** Get list of users, find By Delete Status.
	 * @param deleteStatus
	 * @return {@code List<User>}
	 */
	public List<User> findByDeleteStatus(Boolean deleteStatus);
	/**
	 * @param idOfUser
	 * @return {@code User}
	 */
	public User deleteUser(Integer idOfUser);
	/**
	 * Find list of users By DeleteStatus And AccountExpired And AccountLocked.
	 * @param deleteStatus
	 * @param accountExpired
	 * @param accountLocked
	 * @return {@code List<User>}
	 */
	public List<User> findByDeleteStatusAndAccountExpiredAndAccountLocked(Boolean deleteStatus, Boolean accountExpired, Boolean accountLocked);
	
	/**
	 * Find list of users By FirstName Containing Or LastName Containing search term.
	 * @param search1
	 * @param search2
	 * @return {@code List<User>}
	 */
	public List<User> findByFirstNameContainingOrLastNameContaining(String search1,String search2);

	/** update new {@code user} object for user profile
	 * @param newUser
	 * @param currentUser
	 * @param questionAnswermapper
	 * @return
	 */
	public User updateUserObjectForUserProfile(User currentUser,List<JSONQuestionAnswerMapper> questionAnswermapper);

	public User updateUserPassword(User userToUpdate);
}
