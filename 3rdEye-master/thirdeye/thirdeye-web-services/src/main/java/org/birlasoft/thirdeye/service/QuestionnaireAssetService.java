package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;

/**
 * Service interface for questionnaire asset.
 */
public interface QuestionnaireAssetService {
	
	/**
	 * Save questionnaire asset object.
	 * @param questionnaireAsset
	 * @return {@code QuestionnaireAsset}
	 */
	public QuestionnaireAsset save(QuestionnaireAsset questionnaireAsset);
	
	/**
	 * Delete list of questionnaire assets in batch.
	 * @param assetsToBeDeleted
	 */
	public void deleteInBatch(Iterable<QuestionnaireAsset> assetsToBeDeleted);
	
	/**
	 * Save list of questionnaire asset in batch.
	 * @param assetsToBeSaved
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public List<QuestionnaireAsset> save(Iterable<QuestionnaireAsset> assetsToBeSaved);
	
	/**
	 * Find one questionnaire asset object by id.
	 * @param id
	 * @return {@code QuestionnaireAsset}
	 */
	public QuestionnaireAsset findOne(Integer id);
	
	/**
	 * Find list of questionnaire asset by questionnaire object. 
	 * @param questionnaire
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public List<QuestionnaireAsset> findByQuestionnaire(Questionnaire questionnaire);	
	/**
	 * find By Questionnaire with Loaded Asset
	 * @param qe
	 * @return {@code Set<QuestionnaireAsset>}
	 */
	public Set<QuestionnaireAsset> findByQuestionnaireLoadedAsset(Questionnaire qe);
	/**
	 * Save list of Questionnaire Asset
	 * @param questionnaireAssetForAddition
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public List<QuestionnaireAsset> saveAndFlush(List<QuestionnaireAsset> questionnaireAssetForAddition);
    /**
     * Find list of questionnaire asset by asset
     * @author sunil1.gupta
     * @param asset
     * @return list of questionnaire asset
     */
	public List<QuestionnaireAsset> findByAsset(Asset asset);
	/**
	 * fetch all QA by QE and assets.
	 * @param qe
	 * @param setOfAsset
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public Set<QuestionnaireAsset> findByQuestionnaireAndAssetIn(Questionnaire qe, Set<Asset> setOfAsset);

	public Set<QuestionnaireAsset> findByQuestionnaireAndAsset(Questionnaire qe, Asset idOfAsset);
}
