package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ScatterWrapper;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Questionnaire;

/**
 * Service Interface for plot scatter graph.
 * 
 * @author samar.gupta
 *
 */

public interface ScatterGraphService {
	/**
	 * Plot scatter graph.
	 * @param questionnaireIds
	 * @param parameterIds
	 * @param idOfQualityGate
	 * @param filterMap 
	 * @return {@code ScatterWrapper}
	 */
	public ScatterWrapper plotScatterGraph(String[] questionnaireIds,
			String[] parameterIds, Integer idOfQualityGate, Map<String, List<String>> filterMap,Integer[] idsOfLevel1,Integer[] idsOfLevel2,Integer[] idsOfLevel3,Integer[]idsOfLevel4);
	
   /**
    * Get Questionnaire for scatter
    * @return  {@code List<Questionnaire>}
    */
	public List<Questionnaire> getQuestionnaireforScatter();
	
	/** GetBcmLevelsforScatter
	 * @return ListOFBcmLevel1
	 */
	public List<BcmLevel> getBcmLevelsforScatter();

}
