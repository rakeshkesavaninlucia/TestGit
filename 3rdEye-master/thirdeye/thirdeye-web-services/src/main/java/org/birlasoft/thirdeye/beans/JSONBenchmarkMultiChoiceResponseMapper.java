package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;

/**
 * This object will be used for JSON serialization
 * @author samar.gupta
 */
public class JSONBenchmarkMultiChoiceResponseMapper implements QuantifiableResponse{

	private List<JSONBenchmarkQuestionOptionMapper> options = new ArrayList<JSONBenchmarkQuestionOptionMapper>();
	private String otherOptionResponseText;
	private Double score;
	
	public JSONBenchmarkMultiChoiceResponseMapper() {
	}

	public List<JSONBenchmarkQuestionOptionMapper> getOptions() {
		return options;
	}

	public void setOptions(List<JSONBenchmarkQuestionOptionMapper> options) {
		this.options = options;
	}
	
	public void addOption (JSONBenchmarkQuestionOptionMapper oneOption){
		if (oneOption != null){
			options.add(oneOption);
		}
	}

	public String getOtherOptionResponseText() {
		return otherOptionResponseText;
	}

	public void setOtherOptionResponseText(String otherOptionResponseText) {
		this.otherOptionResponseText = otherOptionResponseText;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		if (options != null && !options.isEmpty()){
			for (JSONBenchmarkQuestionOptionMapper oneOption : options){
				// There will only be one option selected here
				valueToBeReturned = valueToBeReturned.add(new BigDecimal(oneOption.getQuantifier()));
			}
		} else if (score != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(score));
		}
		
		
		return valueToBeReturned;
	}
}
