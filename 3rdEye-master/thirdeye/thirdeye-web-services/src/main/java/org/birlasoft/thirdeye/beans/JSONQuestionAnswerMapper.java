package org.birlasoft.thirdeye.beans;

public class JSONQuestionAnswerMapper {

	private String question;
    private String answer;
    
    public JSONQuestionAnswerMapper(){
    	
    }
    
    public JSONQuestionAnswerMapper(String question, String answer) {
		super();
		this.question = question;
		this.answer = answer;
	}

    
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
