package org.birlasoft.thirdeye.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.asset.AssetBarChartWrapper;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Service interface for asset.
 * @author samar.gupta
 */
public interface AssetService {
	
	/**
	 * List of all {@code Assets}
	 * @return List{@code <Asset>} Object
	 */
    public List<Asset> findAll();
    
    /**
     * Only get One Asset with given Id
     * @param id
     * @return Asset Object
     */
    public Asset findOne(Integer id);
    
    /**
     * save the Asset Object 
     * @param asset
     * @return Asset Object
     */
    public Asset save(Asset asset);
    
    /**
     * find set of asset by not in set of assets ids in graph.
     * @param assetsIdsInGraph
     * @return {@code Set<Asset>}
     */
    public Set<Asset> findByIdNotIn(Set<Integer> assetsIdsInGraph);
    
    /**
     * find set of asset by asset template in set of template.
     * @param assetTemplates
     * @return {@code Set<Asset>}
     */
    public Set<Asset> findByAssetTemplateIn(Set<AssetTemplate> assetTemplates);

	/**
	 * Get set of assets which are not in graph.
	 * @param assetsInGraph
	 * @param assetTemplates
	 * @return Set{@code<Asset>}
	 */
	public List<AssetBean> getAssetBeansNotInGraph(List<AssetBean> assetsInGraph, Set<AssetTemplate> assetTemplates);
	
	/**
	 * Find list of asset by asset template.
	 * @param assetTemplate
	 * @return {@code Set<Asset>}
	 */
	public Set<Asset> findByAssetTemplate(AssetTemplate assetTemplate);
	
	/**
	 * Get list of asset bean from set of asset.
	 * @param assets
	 * @return {@code List<AssetBean>}
	 */
	public List<AssetBean> getAssetBeansFromAssets(Set<Asset> assets);
	
	/**
	 * Create new Asset data object.
	 * @param columnData
	 * @param parameterName
	 * @param asset
	 * @return AssetData
	 */
	public AssetData createAssetDataObject(String columnData, String parameterName, Asset asset);
	
	/**
	 * Get list of asset bean.
	 * @param assets
	 * @return {@code List<AssetBean>}
	 */
	public List<AssetBean> getListOfAssetBean(Collection<Asset> assets);

	/**
	 * Get {@code List} of all {@link AssetBean} by workspace
	 * @param activeWorkSpaceForUser
	 * @return
	 */
	public List<AssetBean> findAllAssetsInWorkspace(Workspace activeWorkSpaceForUser);
	
	/**
	 * Get {@code Set} of {@link AssetBean} by asset template id and questionnaire id
	 * @param idsOfAsset
	 * @param qeId
	 * @return
	 */
	public Set<AssetBean> getSetOfAssetBeansByIdsAndQeId(Set<Integer> idsOfAsset, Integer qeId);
	
	/**
	 * Get {@code set} of {@link Asset} by asset id and asset template id
	 * @param idsOfAsset
	 * @param assetTemplates
	 * @return
	 */
	public Set<Asset> findByIdInAndAssetTemplateIn(Set<Integer> idsOfAsset, Set<AssetTemplate> assetTemplates);
	/**
	 * Get {@code set} of {@link Asset} by workspace and asset type
	 * @param workspaceId
	 * @param assetTypeId
	 * @return
	 */
	public Set<Asset> findAssetsByWorkspaceIdAndAssetTypeId(Workspace workspaceId, AssetType assetTypeId);

	/**
	 * Save list of asset objects
	 * @param listOfAssets
	 * @return
	 */
	public List<Asset> save(List<Asset> listOfAssets);
	
    /**
     * Get {@link Asset} by UID
     * @param uid
     * @return
     */
    public Asset findByUid(String uid);
    /**
     * Find all assets by asset class with in workspace.
     * @param activeWorkSpaceForUsers
     * @param assetType
     * @return {@code List<AssetBean>}
     */
    public List<AssetBean> findAllAssetsInWorkspaceByAssetType(List<Workspace> activeWorkSpaceForUsers, AssetType assetType);
    /**
     * Create new {@link Asset} object.
     * @param assetTemplate
     * @param currentUser
     * @return {@code Asset}
     */
    public Asset createNewAssetObject(AssetTemplate assetTemplate, User currentUser);
    /**
	 * Get asset name to set UID in asset.
	 * @param asset
	 * @return {@code String}
	 */
    public String getAssetNameForUID(Asset asset);
	 /**
    * Find all assets by asset class with in workspace.
    * @param activeWorkSpaceForUsers
    * @param assetType
    * @return {@code List<Asset>}
    */
   public List<Asset> findAssetsInWorkspaceByAssetType(List<Workspace> activeWorkSpaceForUsers, AssetType assetType);

   /**
    * method to save asset and there relation ship in relationship asset data.
    * @param listOfAssets
    * @return {@code List<Asset>}
    */
	public List<Asset> saveAssetAndRelationship(List<Asset> listOfAssets,AssetTemplate assetTemplate);
	/**
	 * Fetch parent child assets of relationship template.
	 * @param assetTemplate
	 * @param assets
	 * @return {@code Map<Integer, RelationshipAssetDataBean>}
	 */
	public Map<Integer, RelationshipAssetDataBean> getAssetsRelationship(AssetTemplate assetTemplate, Set<Asset> assets);

	/**
	 * method to get filtered asset id from elastic search
	 * @param filterMap
	 * @return get list of filtered assetid
	 */
	public List<Integer> getFilteredAssetIdFromElasticSearch(Map<String, List<String>> filterMap);
	
	/**method to get list of asset
	 * @param id
	 * @return
	 */
	public List<Asset> findListOfAssetsByIds(List<Integer> id);
	 

	/** method for update an asset
	 * @param request
	 * @param assetTemplate
	 * @param assetid
	 * @return
	 */
	public Asset updateAsset(HttpServletRequest request, AssetTemplate assetTemplate, Integer assetid);
	
	/**method to get parameter name and value for sprider bar chart
	 * @param idOfAsset
	 * @return
	 */
	public List<AssetBarChartWrapper> getAssetBarChartWrapper(Integer idOfAsset);
	 
	
}
