package org.birlasoft.thirdeye.beans.parameter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.constant.FunctionalMapType;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.interfaces.ParameterEvaluator;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.util.Utility;

/**
 * Evaluator class for functional coverage parameter.
 * @author samar.gupta
 *
 */
public class FunctionalCoverageParameterEvaluator implements ParameterEvaluator {

	private List<Parameter> listOfParameters;
	private ParameterService parameterService;
	private FunctionalMapService functionalMapService;
	private BCMLevelService bcmLevelService;
	private FunctionalCoverageParameterConfigBean fcParameterConfigBean;
	private final Questionnaire questionnaire;
	
	/**
	 * Constructor to initialize {@code FunctionalCoverageParameterEvaluator}
	 * @param listOfParameters
	 * @param parameterService
	 * @param functionalMapService
	 * @param bcmLevelService
	 * @param questionnaire
	 */
	public FunctionalCoverageParameterEvaluator(
			List<Parameter> listOfParameters,
			ParameterService parameterService,
			FunctionalMapService functionalMapService,
			BCMLevelService bcmLevelService,
			Questionnaire questionnaire){
		this.listOfParameters = listOfParameters;
		this.parameterService = parameterService;
		this.functionalMapService = functionalMapService;
		this.bcmLevelService = bcmLevelService;
		this.questionnaire = questionnaire;
	}
	
	/**
	 * Constructor to initialize {@code FunctionalCoverageParameterEvaluator}
	 * @param listOfParameters
	 * @param parameterService
	 * @param functionalMapService
	 * @param bcmLevelService
	 * @param fcParameterConfigBean
	 * @param questionnaire
	 */
	public FunctionalCoverageParameterEvaluator(
			List<Parameter> listOfParameters,
			ParameterService parameterService,
			FunctionalMapService functionalMapService,
			BCMLevelService bcmLevelService,
			FunctionalCoverageParameterConfigBean fcParameterConfigBean,
			Questionnaire questionnaire){
		this.listOfParameters = listOfParameters;
		this.parameterService = parameterService;
		this.functionalMapService = functionalMapService;
		this.bcmLevelService = bcmLevelService;
		this.fcParameterConfigBean = fcParameterConfigBean;
		this.questionnaire = questionnaire;
	} 
	
	/**
	 * Evaluate functional coverage parameter
	 * @param onlyAssetsToEvaluate
	 * @return {@code List<BcmResponseBean>}
	 */
	@Override
	public List<ParameterEvaluationResponse> evaluateParameter(List<AssetBean> assets) {
		List<ParameterEvaluationResponse> listOfParameterEvaluationResponse = new ArrayList<>();
		for (Parameter oneParameter : listOfParameters) {
			List<BcmResponseBean> listOfBcmResponseBean = evaluateBcmLevel(assets, oneParameter);
			if(listOfBcmResponseBean != null && !listOfBcmResponseBean.isEmpty()){
				Map<AssetBean, BigDecimal> mapOfAssetScoreAtRollupLevel = calculateScoreAtRollupLevel(listOfBcmResponseBean);
				listOfParameterEvaluationResponse.add(createEvaluationResponseObject(mapOfAssetScoreAtRollupLevel, oneParameter));
			}
		}
		
		return listOfParameterEvaluationResponse;
	}
	
	/** 
	 * Evaluate bcm level by asset functional map data and parameter.
	 * @param assets
	 * @param oneParameter
	 * @return {@code List<BcmResponseBean>}
	 */
	public List<BcmResponseBean> evaluateBcmLevel(List<AssetBean> assets, Parameter oneParameter){
		List<BcmResponseBean> listOfBcmResponseBean = new ArrayList<>();
		if(oneParameter != null && oneParameter.getType().equals(ParameterType.FC.name())){
			ParameterConfig parameterConfig = parameterService.findParameterConfigByParameter(oneParameter);
			FunctionalCoverageParameterConfigBean fcConfigBean;
			fcConfigBean = Utility.convertJSONStringToObject(parameterConfig.getParameterConfig(), FunctionalCoverageParameterConfigBean.class);
			if(fcConfigBean != null){
				FunctionalMap fm = functionalMapService.findOne(fcConfigBean.getFunctionalMapId(), true);
				
				// fetch functional map data by functional map 
				List<FunctionalMapDataBean> fmDataBeans = functionalMapService.fetchFunctionalMapData(fm, questionnaire);
				
				// pull all distinct assets from list of functional map data 
				Set<AssetBean> assetBeans = createSetOfAssetBeans(fmDataBeans);
				
				// fetch only those assets which will be evaluated
				Set<AssetBean> onlyAssetsToEvaluate = fetchAssetsToEvaluate(assets, assetBeans);
				
				if(!onlyAssetsToEvaluate.isEmpty()){
					
					// generate bcm response bean by functional map data
					List<BcmResponseBean> responseBeans = generateBcmResponseBean(fmDataBeans);
					
					// filter list of bcm response bean and map with bcm level number
					Map<Integer, List<BcmResponseBean>> mapOfBcmResponseBeans = createMapOfLevelString(responseBeans);
					
					//prepare map of bcm level bean and level number as key
					Map<Integer, List<BcmLevelBean>> mapOfBcmLevels = prepareMapOfBcmLevel(fm);
					
					int rollUpLevel = extractRollupLevel(fcConfigBean);
					
					mapOfBcmResponseBeans = evaluateLevels(mapOfBcmResponseBeans, onlyAssetsToEvaluate, mapOfBcmLevels, rollUpLevel);
					
					listOfBcmResponseBean = mapOfBcmResponseBeans.get(rollUpLevel);
				}
			}
		}
		return listOfBcmResponseBean;
	}

	/**
	 * Returns map of bcm level number and list of bcm level 
	 * @param fm
	 * @return
	 */
	private Map<Integer, List<BcmLevelBean>> prepareMapOfBcmLevel(FunctionalMap fm) {
		// Find out the depth to which you need to go
		Set<Integer> levelNumbers = getLevelNumbers(fm.getType());
		
		// See what is available in the database for the depth that is required
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcmAndLevelNumberIn(fm.getBcm(), levelNumbers, false);
		
		// The roots of the BCM tree
		List<BcmLevelBean> rootBcmLevels = bcmLevelService.generateLevelTreeFromBcmLevels(listOfBcmLevel);
		
		// Make map for level number as key and list of bcm level value 
		return createMapOfBcmLevels(rootBcmLevels);
	}

	/**
	 * Extract roll up level for functional coverage parameter evaluation.
	 * @param fcConfigBean
	 * @return
	 */
	private int extractRollupLevel(FunctionalCoverageParameterConfigBean fcConfigBean) {
		int rollUpLevel = 0;
		if(this.fcParameterConfigBean == null){
			rollUpLevel = getLevelNumberKey(fcConfigBean.getFunctionalMapLevelType());
		}else if (this.listOfParameters.size() == 1 && this.fcParameterConfigBean != null){
			rollUpLevel = getLevelNumberKey(this.fcParameterConfigBean.getFunctionalMapLevelType());
		}
		return rollUpLevel;
	}
	
	private Map<Integer, List<BcmResponseBean>> evaluateLevels(Map<Integer, List<BcmResponseBean>> mapOfBcmResponseBeans,
			Set<AssetBean> assetsToEvaluate, Map<Integer, List<BcmLevelBean>> mapOfLevelNumbers, int rollUpLevel) {
		int minLevel = getMinimumLevelFromMap(mapOfBcmResponseBeans);
		boolean isRollup = false;
		//calculate immediate parent level
		evaluateParentLevel(mapOfBcmResponseBeans, assetsToEvaluate, mapOfLevelNumbers, minLevel, isRollup);
		
		int currentLevelEvaluated = minLevel - 1;
		isRollup = true;
		
		while (rollUpLevel < currentLevelEvaluated) {		
			evaluateParentLevel(mapOfBcmResponseBeans, assetsToEvaluate, mapOfLevelNumbers, currentLevelEvaluated, isRollup);
			currentLevelEvaluated--;
		}
		
		return mapOfBcmResponseBeans;
	}

	private void evaluateParentLevel(Map<Integer, List<BcmResponseBean>> mapOfBcmResponseBeans, Set<AssetBean> assetsToEvaluate,
			Map<Integer, List<BcmLevelBean>> mapOfLevelNumbers, int levelToEvaluate, boolean isRollup) {
		List<BcmResponseBean> listOfBcmResponseBean = new ArrayList<>();
		
		for (AssetBean assetBean : assetsToEvaluate) {
			if(mapOfLevelNumbers.get(Integer.valueOf(levelToEvaluate - 1)) != null){
				for (BcmLevelBean bcmLevelBean : mapOfLevelNumbers.get(Integer.valueOf(levelToEvaluate - 1))) {
					List<BcmLevelBean> listOfChildBcmLevel = bcmLevelBean.getChildLevels();
					if(!listOfChildBcmLevel.isEmpty()){
						BigDecimal avgLevelValue = calculateParentLevelValue(listOfChildBcmLevel, mapOfBcmResponseBeans.get(levelToEvaluate), assetBean, isRollup);
						listOfBcmResponseBean = addOrUpdateListOfBcmResponse(bcmLevelBean, assetBean, avgLevelValue, listOfBcmResponseBean);
					}
				}
			}
		}
		
		mapOfBcmResponseBeans.put(levelToEvaluate - 1, listOfBcmResponseBean);
	}

	private BigDecimal calculateParentLevelValue(List<BcmLevelBean> listOfChildBcmLevel, List<BcmResponseBean> listOfBcmResponse,
			AssetBean assetBean, boolean isRollup) {
		Map<BcmLevelBean, BigDecimal> mapOfBcmLevelValue = prepareMapOfBcmLevelAndValue(listOfChildBcmLevel, listOfBcmResponse, assetBean);

		BigDecimal valueToReturn = new BigDecimal(0);
		int divisor = 0;
		for (Entry<BcmLevelBean, BigDecimal> entry : mapOfBcmLevelValue.entrySet()) {
			if(isRollup) {				
				if(entry.getValue() != null && entry.getValue().doubleValue() > 0){
					valueToReturn = valueToReturn.add(entry.getValue());
					divisor++;
				}
			} else {
				if(entry.getValue() != null && !ParameterBean.NA_BIGDECIMAL.equals(entry.getValue())) {
					valueToReturn = valueToReturn.add(entry.getValue());
				}
			}
		}
		
		if(!isRollup){
			divisor = listOfChildBcmLevel.size();
		} 
		
		valueToReturn = extractDividedValueForParentLevel(valueToReturn, divisor);
		return valueToReturn;
	}

	private BigDecimal extractDividedValueForParentLevel(BigDecimal valueToReturn, int divisor) {
		BigDecimal returnValue;
		if(!valueToReturn.equals(new BigDecimal(0)) && divisor != 0){
			returnValue = valueToReturn.divide(new BigDecimal(divisor),2,RoundingMode.HALF_UP);
		} else {
			returnValue = valueToReturn.add(ParameterBean.NA_BIGDECIMAL);
		}
		return returnValue;
	}

	/**
	 * @param listOfChildBcmLevel
	 * @param listOfBcmResponse
	 * @param assetBean
	 * @return
	 */
	private Map<BcmLevelBean, BigDecimal> prepareMapOfBcmLevelAndValue(List<BcmLevelBean> listOfChildBcmLevel,
			List<BcmResponseBean> listOfBcmResponse, AssetBean assetBean) {
		Map<BcmLevelBean, BigDecimal> mapOfBcmLevelValue = new HashMap<>();
		for (BcmLevelBean bcmLevel : listOfChildBcmLevel) {
			if(listOfBcmResponse != null){
				for (BcmResponseBean bcmResponseBean : listOfBcmResponse) {
					if(bcmLevel.equals(bcmResponseBean.getBcmLevelBean())){
						mapOfBcmLevelValue.put(bcmLevel, bcmResponseBean.fetchParamValueForAsset(assetBean));
					}
				}
			}
		}
		return mapOfBcmLevelValue;
	}

	private Map<Integer, List<BcmLevelBean>> createMapOfBcmLevels(List<BcmLevelBean> rootBcmLevels) {
		Map<Integer, List<BcmLevelBean>> levelMap = new HashMap<>();
		
		addChildBcmLevelsToMap(levelMap, rootBcmLevels);
		
		return levelMap;
	}

	/**
	 * recursively add child to map
	 * @param levelMap
	 * @param rootBcmLevels
	 */
	private void addChildBcmLevelsToMap(Map<Integer, List<BcmLevelBean>> levelMap, List<BcmLevelBean> rootBcmLevels) {
		
		for (BcmLevelBean bcmLevel : rootBcmLevels) {
			if(levelMap.containsKey(bcmLevel.getLevelNumber())) {
				List<BcmLevelBean> listOfBcmLevelBean = levelMap.get(bcmLevel.getLevelNumber());
				listOfBcmLevelBean.add(bcmLevel);
				levelMap.put(bcmLevel.getLevelNumber(), listOfBcmLevelBean);
			} else {
				List<BcmLevelBean> listOfBcmLevelBean = new ArrayList<>();
				listOfBcmLevelBean.add(bcmLevel);
				levelMap.put(bcmLevel.getLevelNumber(), listOfBcmLevelBean);
			}
			
			if (!bcmLevel.getChildLevels().isEmpty()) {				
				addChildBcmLevelsToMap(levelMap, bcmLevel.getChildLevels());
			}
		}
		
	}

	/**
	 * calculate Score at rollup level.
	 * @param responseBeans
	 * @return {@code Map<AssetBean, Double>}
	 */
	private Map<AssetBean, BigDecimal> calculateScoreAtRollupLevel(List<BcmResponseBean> responseBeans){
		Map<AssetBean, BigDecimal> mapOfAssetScore = new HashMap<>();
		Map<AssetBean, Integer> mapOfNonZeroAsset = new HashMap<>(); 
		
		for (BcmResponseBean bcmResponseBean : responseBeans) {
			prepareMapOfScoreAndDivisor(mapOfAssetScore, mapOfNonZeroAsset, bcmResponseBean);
		}
		
		for (Entry<AssetBean, BigDecimal> entry : mapOfAssetScore.entrySet()) {
			BigDecimal avgScoreAtRollupLevel = new BigDecimal(0);
			if(mapOfNonZeroAsset.containsKey(entry.getKey())){
				if(!mapOfNonZeroAsset.get(entry.getKey()).equals(Integer.valueOf(0)))
					avgScoreAtRollupLevel = entry.getValue().divide(new BigDecimal (mapOfNonZeroAsset.get(entry.getKey())), 2, RoundingMode.HALF_UP);
				if(avgScoreAtRollupLevel.compareTo(new BigDecimal(0)) > 0){
					mapOfAssetScore.put(entry.getKey(), avgScoreAtRollupLevel);
				}else{
					mapOfAssetScore.put(entry.getKey(), ParameterBean.NA_BIGDECIMAL);
				}
			}
		}
		
		return mapOfAssetScore;
	}

	/**
	 * @param mapOfAssetScore
	 * @param mapOfNonZeroAsset
	 * @param bcmResponseBean
	 */
	private void prepareMapOfScoreAndDivisor(Map<AssetBean, BigDecimal> mapOfAssetScore,
			Map<AssetBean, Integer> mapOfNonZeroAsset, BcmResponseBean bcmResponseBean) {
		Set<AssetBean> assetBeans = bcmResponseBean.getAssets();
		for (AssetBean assetBean : assetBeans) {
			if(mapOfAssetScore.containsKey(assetBean)){
				if(bcmResponseBean.fetchParamValueForAsset(assetBean).doubleValue() > 0){
					BigDecimal assetScore = mapOfAssetScore.get(assetBean).add(bcmResponseBean.fetchParamValueForAsset(assetBean));
					mapOfAssetScore.put(assetBean, assetScore);
					mapOfNonZeroAsset.put(assetBean, mapOfNonZeroAsset.get(assetBean) + 1);
				}
			}else{
				if(bcmResponseBean.fetchParamValueForAsset(assetBean).doubleValue() > 0){
					mapOfNonZeroAsset.put(assetBean, 1);
					mapOfAssetScore.put(assetBean, bcmResponseBean.fetchParamValueForAsset(assetBean));
				}
				else{
					mapOfNonZeroAsset.put(assetBean, 0);
					mapOfAssetScore.put(assetBean, new BigDecimal(0));
				}
			}
		}
	}

	/**
	 * Fetch only those assets which will be evaluated. 
	 * @param assets
	 * @param assetBeans
	 * @return {@code Set<AssetBean>}
	 */
	private Set<AssetBean> fetchAssetsToEvaluate(List<AssetBean> assetsToEvaluate, Set<AssetBean> availableAssetsInFM) {
		Set<AssetBean> onlyAssetsToEvaluate = new HashSet<>();
		for (AssetBean assetBean : assetsToEvaluate) {
			if(availableAssetsInFM.contains(assetBean))
				onlyAssetsToEvaluate.add(assetBean);
		}
		
		return onlyAssetsToEvaluate;
	}
	
	/**
	 * create set Of AssetBeans
	 * @param functionalMapDataBeans
	 * @return {@code Set<AssetBean>}
	 */
	private Set<AssetBean> createSetOfAssetBeans(List<FunctionalMapDataBean> functionalMapDataBeans){
		Set<AssetBean> assetBeans = new HashSet<>();
		
		for (FunctionalMapDataBean fmdb : functionalMapDataBeans) {
			assetBeans.add(fmdb.getAssetBean());
		}
		
		return assetBeans;
	}
	
	/**
	 * Create Evaluation Response Object
	 * @param bcmResponseBean
	 * @param assets
	 * @return {@code ParameterEvaluationResponse}
	 */
	private ParameterEvaluationResponse createEvaluationResponseObject(Map<AssetBean, BigDecimal> mapOfAssetScoreAtRollupLevel, Parameter oneParameter){
		ParameterEvaluationResponse evaluationResponse = new ParameterEvaluationResponse();
		
		evaluationResponse.setParameterBean(new ParameterBean(oneParameter));
		for (Entry<AssetBean, BigDecimal> entry : mapOfAssetScoreAtRollupLevel.entrySet()) {
			evaluationResponse.addAssetEvaluation(entry.getKey(), entry.getValue());
		}
		
		return evaluationResponse;
	}
	
	/**
	 * Get set of level numbers by level type 
	 * @param type
	 * @return {@code Set<Integer>}
	 */
	private Set<Integer> getLevelNumbers(String type) {
		Set<Integer> levelNumbers = new HashSet<>();
		int targetDepth = FunctionalMapType.valueOf(type).getKey();
		for(int i=1;i <= targetDepth;i++){
			levelNumbers.add(i);
		}		
		return levelNumbers;
	}
	
	/**
	 * Get level number by level type
	 * @param levelType
	 * @return {@code int}
	 */
	private int getLevelNumberKey(String levelType){
		return FunctionalMapType.valueOf(levelType).getKey();
	}
	
	/**
	 * Generate bcm response beans by list of functional map dat abean
	 * @param fmDataBeans
	 * @return {@code List<BcmResponseBean>}
	 */
	private List<BcmResponseBean> generateBcmResponseBean(List<FunctionalMapDataBean> fmDataBeans){
		List<BcmResponseBean> responseBeans = new ArrayList<>();
		
		Map<BcmLevelBean, List<FunctionalMapDataBean>> mapOfLevelBeans = createMapOfBcmLevelBeans(fmDataBeans);
		
		for (Entry<BcmLevelBean, List<FunctionalMapDataBean>> entry : mapOfLevelBeans.entrySet()) {
			BcmResponseBean evaluationResponse = new BcmResponseBean();
			evaluationResponse.setBcmLevelBean(entry.getKey());
			List<FunctionalMapDataBean> listOfFmDataBeans = entry.getValue();
			for (FunctionalMapDataBean oneFmDataBean : listOfFmDataBeans) {
				evaluationResponse.addAssetEvaluation(oneFmDataBean.getAssetBean(), oneFmDataBean.getResponseValue());
			}
			
			responseBeans.add(evaluationResponse);
		}
		
		return responseBeans;
	}

	/**
	 * Create map of bcm level bean keys by list of functional
	 * map data bean
	 * @param fmDataBeans
	 * @return {@code Map<BcmLevelBean, List<FunctionalMapDataBean>>}
	 */
	private Map<BcmLevelBean, List<FunctionalMapDataBean>> createMapOfBcmLevelBeans(
			List<FunctionalMapDataBean> fmDataBeans) {
		Map<BcmLevelBean, List<FunctionalMapDataBean>> mapOfLevelBeans = new HashMap<>();
		
		for (FunctionalMapDataBean fmDataBean : fmDataBeans) {
			if(mapOfLevelBeans.containsKey(fmDataBean.getBcmLevelBean())){
				List<FunctionalMapDataBean> mapDataBeans = mapOfLevelBeans.get(fmDataBean.getBcmLevelBean());
				mapDataBeans.add(fmDataBean);
				mapOfLevelBeans.put(fmDataBean.getBcmLevelBean(), mapDataBeans);
			}else{
				List<FunctionalMapDataBean> mapDataBeans = new ArrayList<>();
				mapDataBeans.add(fmDataBean);
				mapOfLevelBeans.put(fmDataBean.getBcmLevelBean(), mapDataBeans);
			}
		}
		return mapOfLevelBeans;
	}
	
	/**
	 * Create map of level keys by list of bcm response bean.
	 * @param responseBeans
	 * @return {@code Map<Integer, List<BcmResponseBean>>}
	 */
	private Map<Integer, List<BcmResponseBean>> createMapOfLevelString(List<BcmResponseBean> responseBeans){
		Map<Integer, List<BcmResponseBean>> mapOfBcmResponses = new HashMap<>();
		
		for (BcmResponseBean bcmResponseBean : responseBeans) {
			if(mapOfBcmResponses.containsKey(bcmResponseBean.getBcmLevelBean().getLevelNumber())){
				List<BcmResponseBean> listOfResponseBeans = mapOfBcmResponses.get(bcmResponseBean.getBcmLevelBean().getLevelNumber());
				listOfResponseBeans.add(bcmResponseBean);
				mapOfBcmResponses.put(bcmResponseBean.getBcmLevelBean().getLevelNumber(), listOfResponseBeans);
			}else{
				List<BcmResponseBean> listOfResponseBeans = new ArrayList<>();
				listOfResponseBeans.add(bcmResponseBean);
				mapOfBcmResponses.put(bcmResponseBean.getBcmLevelBean().getLevelNumber(), listOfResponseBeans);
			}
		}
		
		return mapOfBcmResponses;
	}
	
	/**
	 * Add or update list of bcm response bean
	 * @param bcmLevelBean
	 * @param assetBean
	 * @param avgLevelValue
	 * @param listOfBcmResponseBean
	 * @return {@code List<BcmResponseBean>}
	 */
	private List<BcmResponseBean> addOrUpdateListOfBcmResponse(BcmLevelBean bcmLevelBean, AssetBean assetBean, BigDecimal avgLevelValue, List<BcmResponseBean> listOfBcmResponseBean){
		boolean isExist = false;
		for (BcmResponseBean bcmResponseBean : listOfBcmResponseBean) {
			if(bcmResponseBean.getBcmLevelBean().equals(bcmLevelBean)){
				bcmResponseBean.addAssetEvaluation(assetBean, avgLevelValue);
				isExist = true;
				break;
			}
		}
		
		if(!isExist){
			BcmResponseBean responseBean = new BcmResponseBean();
			responseBean.setBcmLevelBean(bcmLevelBean);
			responseBean.addAssetEvaluation(assetBean, avgLevelValue);
			listOfBcmResponseBean.add(responseBean);
		}
		return listOfBcmResponseBean;
	}

	/**
	 * Get minimum level from map
	 * @param mapOfBcmResponses
	 * @return {@code int}
	 */
	private int getMinimumLevelFromMap(Map<Integer, List<BcmResponseBean>> mapOfBcmResponses) {
		int minLevel = 4;
		for (Integer levelNumber : mapOfBcmResponses.keySet()) {
			if(levelNumber < minLevel){
				minLevel = levelNumber;
			}
		}
		return minLevel;
	}
}
