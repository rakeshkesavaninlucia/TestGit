/**
 * 
 */
package org.birlasoft.thirdeye.beans.parameter;

import java.math.BigDecimal;

/**
 * @author dhruv.sood
 *
 */
public class ParameterValueBoostBean {

	private Integer qeId;
	private Integer assetId;
	private Integer parameterId;
	private String parameterName;
	private BigDecimal evaluatedValue;
	private String boostPercentage;
	
	
	public Integer getQeId() {
		return qeId;
	}
	public void setQeId(Integer qeId) {
		this.qeId = qeId;
	}
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public Integer getParameterId() {
		return parameterId;
	}
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public BigDecimal getEvaluatedValue() {
		return evaluatedValue;
	}
	public void setEvaluatedValue(BigDecimal evaluatedValue) {
		this.evaluatedValue = evaluatedValue;
	}
	public String getBoostPercentage() {
		return boostPercentage;
	}
	public void setBoostPercentage(String boostPercentage) {
		this.boostPercentage = boostPercentage;
	}	
}
