package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Interface for BCM service
 */
public interface BCMService {

	/**
	 * Save or update {@link BCM}
	 * @param incomingBcm
	 * @return {@code Bcm}
	 */
	public Bcm saveOrUpdate(Bcm incomingBcm);
	/**
	 * find By {@link Workspace} Is Null And {@link BCM} Is Null.
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceIsNullAndBcmIsNull();
	
	/**
	 * find {@link BCM} for given bcmTemplate Id.
	 * @param idOfBCMTemplate
	 * @return {@code BCm}
	 */
	public Bcm findOne(Integer idOfBCMTemplate);
	/**
	 * Get {@code List} of {@link BCM} By @{@code set} of {@link Workspace}
	 * @param workspaces
	 * @param loaded
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceIn(Set<Workspace> workspaces, boolean loaded);
	/**
	 * Save list of Bcms.
	 * @param bcms
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> saveAll(List<Bcm> bcms);
	/**
	 * Fetch Bcms by workspaces and active status.
	 * @param workspaces
	 * @param status
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceInAndStatus(Set<Workspace> workspaces, boolean status);
	/**
	 * Update Bcm active status in List.
	 * @param idOfWorkspaceBcm
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> updateBcmActiveStatusInList(Integer idOfWorkspaceBcm);
	/**
	 * Is status active to show message.
	 * @param idOfWorkspaceBcm
	 * @return {@code boolean}
	 */
	public boolean isStatusActiveToShowMessage(Integer idOfWorkspaceBcm);
}
