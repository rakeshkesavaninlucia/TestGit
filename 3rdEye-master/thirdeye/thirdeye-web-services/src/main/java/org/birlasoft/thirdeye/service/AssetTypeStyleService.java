/**
 * 
 */
package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.entity.AssetType;

/**
 * @author shaishav.dixit
 *
 */
public interface AssetTypeStyleService {

	String getAssetStyle(AssetType assetType);

}
