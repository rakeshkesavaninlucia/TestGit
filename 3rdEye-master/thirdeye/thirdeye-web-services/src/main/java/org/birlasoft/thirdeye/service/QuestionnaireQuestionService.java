package org.birlasoft.thirdeye.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.MandatoryQuestionBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;

/**
 * Service interface for questionnaire question.
 * @author samar.gupta
 */
public interface QuestionnaireQuestionService {
	
	/**
	 * Find list of QQ with loaded or not by questionnaire.
	 * @param questionnaire
	 * @param loaded
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	public List<QuestionnaireQuestion> findByQuestionnaire (Questionnaire questionnaire, boolean loaded);
	
    /**
     * Save list of QQ in batch.
     * @param listOfQuestionnaireQuestion
     * @return {@code List<QuestionnaireQuestion>}
     */
    public List<QuestionnaireQuestion> save(List<QuestionnaireQuestion> listOfQuestionnaireQuestion);
    
    /**
     * Delete list of QQ in batch.
     * @param listOfQuestionnaireQuestion
     */    
    public void deleteInBatch(Collection<QuestionnaireQuestion> listOfQuestionnaireQuestion); 
    
    /**
     * Generate new QQs by questionnaire.
     * @param questionnaire
     */
    public void generateNewQuestionnaireQuestions(Questionnaire questionnaire);
    
    /**
     * Prepare list of QQ bean or questions for questionnaire.  
     * @param questionnaire
     * @return {@code List<QuestionnaireQuestionBean>}
     */
    public List<QuestionnaireQuestionBean> prepareListOfQuestionsForQuestionnaire(Questionnaire questionnaire);

	/**
	 * find one QQ object with loaded or not by id of QQ.
	 * @param idOfQQ
	 * @param loaded
	 * @return {@code QuestionnaireQuestion}
	 */
	public QuestionnaireQuestion findOne(Integer idOfQQ, boolean loaded);
	
	/**
	 * Find list of QQ by parameter and question and questionnaire.
	 * @param parameter
	 * @param question
	 * @param questionnaire
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	public List<QuestionnaireQuestion> findByParameterAndQuestionAndQuestionnaire(ParameterBean parameter, QuestionBean question, Questionnaire questionnaire);
	/**
	 * Get QQ list for order questions.
	 * @param questionsToBeDisplayed
	 * @return {@code List<Map<String, Object>>}
	 */
	public List<Map<String, Object>> getQQListForOrderQuestions(List<QuestionnaireQuestion> questionsToBeDisplayed);
	/**
	 * Find list of QQs for multichoice and checkboxes question type.
	 * @param questionnaireId
	 * @return {@code List<QuestionnaireQuestionBean>}
	 */
	public Set<QuestionBean> findQuestionsOfMCQorRadioType(Questionnaire questionnaire);
	/**
	 * Create map for mandatory question
	 * @param questionnaireQuestionBeans
	 * @param parameterFunctions
	 * @return {@code Map<Integer, Integer>}
	 */
	public Map<Integer, Integer> createMapForMandatoryQuestion(List<QuestionnaireQuestionBean> questionnaireQuestionBeans, List<ParameterFunction> parameterFunctions);
	/**
	 * Create map for answered mandatory question.
	 * @param listOfQuestionnaires
	 * @return {@code Map<Integer, MandatoryQuestionBean>}
	 */
	public Map<Integer, MandatoryQuestionBean> createMapForAnsweredMandatoryQuestion(List<Questionnaire> listOfQuestionnaires);
	/**
	 * Find questions by questionnaire.
	 * @param q
	 * @return {@code Set<QuestionBean>}
	 */
	public Set<QuestionBean> findQuestions(Questionnaire q);
	/**
	 * find by Questionnaire Loaded ResponseDatas.
	 * @param qe
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	public List<QuestionnaireQuestion> findByQuestionnaireLoadedResponseDatas(Questionnaire qe);

	/**
	 * find Set of QuestionnaireQuestion based on
	 * Parameter And Questionnaire
	 * @param parameter
	 * @param questionnaire
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public Set<QuestionnaireQuestion> findByParameterAndQuestionnaire(ParameterBean parameter, Questionnaire questionnaire);

	/**
	 * remove assets from Questionnaire
	 * @param assetsForRemoval
	 */
	public void handleRemovalOfAssetsFromQE(Set<QuestionnaireAsset> assetsForRemoval);
	
	/**
	 * This method will simply refresh the QQ on the QE. 
	 * @param qe
	 */
	
	public void refreshQuestionnaireQuestions(Questionnaire qe);
	
	/**
	 * remove Of Params From Questionnaire
	 * @param paramsForRemoval
	 */	
	public void handleRemovalOfParamsFromQE(Set<QuestionnaireParameter> paramsForRemoval);
	/**
	 * find set of QuestionnaireQuestion by QuestionnaireAsset
	 * @param questionnaireAssets
	 * @return {@code Set<QuestionnaireQuestion>}
	 */
	
	public Set<QuestionnaireQuestion> findByQuestionnaireAssetIn(Set<QuestionnaireAsset> questionnaireAssets);
	
	/**@author dhruv.sood
	 * Get the map of FmId and Parameter name
	 * @param questionnaire
	 * @return {@code Map<Integer,String> }
	 */
	public Map<Integer,String> getFcParameterList(Questionnaire questionnaire);

	/**
	 * method to get Questions For Questionnaire.
	 * @param qe
	 * @return {@code List<QuestionnaireQuestion> }
	 */
	public List<QuestionnaireQuestion> getQuestionsForQuestionnaire(Questionnaire qe);

	/**
	 * Get {@link QuestionnaireQuestion} based on {@code Question, QuestionnaireAsset and QuestionnaireParameter}
	 * @param question
	 * @param questionnaireAsset
	 * @param listOfQuestionnaireParameters
	 * @return
	 */
	public QuestionnaireQuestion getQqForQuestion(QuestionBean question, QuestionnaireAsset questionnaireAsset, List<QuestionnaireParameter> listOfQuestionnaireParameters);
	/**
	 * Fetch all QQs by QPs and QAs.
	 * @param listOfQp
	 * @param listOfQa
	 * @return {@code Set<QuestionnaireQuestion>}
	 */
	public Set<QuestionnaireQuestion> findByQuestionnaireParameterInAndQuestionnaireAssetIn(List<QuestionnaireParameter> listOfQp, Set<QuestionnaireAsset> listOfQa);
	
	
	/**Find QQ by q, qp, qa
	 * @author dhruv.sood
	 * @param question
	 * @param questionnaireParameter
	 * @param questionnaireAsset
	 * @return
	 */
	public QuestionnaireQuestion findByQuestionAndQuestionnaireParameterAndQuestionnaireAsset(Question question, QuestionnaireParameter questionnaireParameter, QuestionnaireAsset questionnaireAsset);

	/**
	 * method to get list Of Questions For Questionnaire excluding duplicate question,questionnnaireAsset group 
	 * @param qe
	 * @return {@code List<QuestionnaireQuestionBean> }
	 */
	public List<QuestionnaireQuestionBean> listOfQuestionsForQuestionnaire(Questionnaire qe);

	public List<QuestionnaireQuestion> findByQuestionIdAndQuestionnaireAssetId(QuestionnaireAsset questionnaireAssetId, Question questionId);
}
