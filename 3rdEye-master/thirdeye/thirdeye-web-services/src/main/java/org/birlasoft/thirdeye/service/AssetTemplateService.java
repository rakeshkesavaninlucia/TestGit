package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Service interface for asset template.
 */
public interface AssetTemplateService {
	
	/**
	 * save AssetTemplate Object 
	 * @param assetTemplate
	 * @return AssetTemplate Object
	 */
	public AssetTemplate save(AssetTemplate assetTemplate);
	/**
	 * Only get One AssetTemplate with given Id
	 * @param id
	 * @return AssetTemplate Object
	 */
	public AssetTemplate findOne(Integer id);
	/**
	 * Get all AssetTemplate for Current logged In User
	 * @return List{@code <AssetTemplate>} Object
	 */
	public List<AssetTemplate> findAll();
	/**
	 * Find fully loaded asset template.
	 * @param id
	 * @return {@code AssetTemplate}
	 */
	public AssetTemplate findFullyLoadedAssetTemplate(Integer id);	
	/**
	 * Create asset template.
	 * @param assetTemplateBean
	 * @param currentUser
	 * @return
	 */
	public AssetTemplate createNewAssetTemplateObject(AssetTemplateBean assetTemplateBean, User currentUser);
	/** 
	 * Find list of asset template by asset type and createdBy.
	 * @param assetType
	 * @param currentuser
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByAssetTypeAndUserByCreatedBy(AssetType assetType, User currentuser);
	
	/**
	 * Get {@code List} of {@link AssetTemplate} by workspace
	 * @param listOfWorkspaces
	 * @return
	 */
	public List<AssetTemplate> findByWorkspaceIn(List<Workspace> listOfWorkspaces);
	/**
	 * Get {@code List} of {@link AssetTemplate} by {@code set} of asset template id's
	 * @param idsOfAssetTemplate
	 * @return
	 */
	public List<AssetTemplate> findByIdIn(Set<Integer> idsOfAssetTemplate);
	/**
	 * Get {@code List} of {@link AssetTemplate} for active workspace
	 * @return
	 */
	public List<AssetTemplate> listTemplateForActiveWorkspace();
	/**
	 * Update {@link AssetTemplate} from {@link AssetTemplateBean}
	 * @param assetTemplateBean
	 * @param currentUser
	 * @return
	 */
	public AssetTemplate updateAssetTemplateObject(AssetTemplateBean assetTemplateBean, User currentUser);
	/**
	 * Get {@code List} of {@link AssetTemplate} by asset template name, asset type and user created
	 * @param assetTemplateName
	 * @param assetType
	 * @param currentuser
	 * @return
	 */
	public List<AssetTemplate> findByAssetTemplateNameAndAssetTypeAndUserByCreatedBy(String assetTemplateName, AssetType assetType, User currentuser);
	/**
	 * Get {@code Map} of {@link AssetType} and {@link AssetTemplate} for active 
	 * workspace
	 * @return
	 */
	public Map<AssetType, List<AssetTemplate>> getMapOfTemplateByTypeForActiveWorkspace();
	/**
	 * Get relationship loaded asset templates by workspace
	 * @param listOfWorkspaces
	 * @param loaded
	 * @return
	 */
	public List<AssetTemplate> findByWorkspaceIn(List<Workspace> listOfWorkspaces, boolean loaded);
	/**
	 * Check if relationship template has attributes or not
	 * @param oneTemplateForViewing
	 */
	public void checkForViewTemplateColumn(AssetTemplate oneTemplateForViewing);
	/**
	 * Get {@code List} of relationship {@link AssetTemplateBean} which have attributes
	 * @return
	 */
	public List<AssetTemplateBean> getListOfTemplateBeanWithHasColumn();
	/**
	 * Find all templates by asset class and in workspaces
	 * @param assetType
	 * @param listOfWorkspaces
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByAssetTypeAndWorkspaceIn(AssetType assetType, List<Workspace> listOfWorkspaces);
	/**
	 * template is expandable or not.
	 * @param templateToDelete
	 * @return {@code boolean}
	 */
	public boolean isTemplateExpandable(AssetTemplate templateToDelete);
	/**
	 * delete asset template.
	 * @param templateToDelete
	 * @return {@code AssetTemplate}
	 */
	public AssetTemplate deleteTemplate(AssetTemplate templateToDelete);
	/**
	 * Fetch list of relationship template.
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> listRelationshipTemplateForActiveWorkspace();
	
	/**
	 * Delete template by template id
	 * @param templateToDelete
	 */
	public void deleteByAssetTemplateId(AssetTemplate templateToDelete);
}
