package org.birlasoft.thirdeye.service.impl;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.RolePermission;
import org.birlasoft.thirdeye.repositories.RolePermissionRepository;
import org.birlasoft.thirdeye.repositories.RoleRepository;
import org.birlasoft.thirdeye.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class RoleServiceImpl implements RoleService {
	@Autowired
	public RoleRepository roleRepository;
	
	@Autowired
	public RolePermissionRepository rolePermissionRepository;

	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role findOne(Integer id) {
		return roleRepository.findOne(id);
	}
	
	@Override
	public Role save(Role oneRole) {
		return roleRepository.save(oneRole);
	}
	
	@Override
	public void delete(Set<RolePermission> listOfRolePermsToDelete) {
		rolePermissionRepository.deleteInBatch(listOfRolePermsToDelete);
	}
	
	@Override
	public void saveRolePermissions(Set<RolePermission> listOfRolePermsToDelete) {
		rolePermissionRepository.save(listOfRolePermsToDelete);
	}

	@Override
	public Role findByIdLoadedRolePermissions(Integer id) {
		return roleRepository.findByIdLoadedRolePermissions(id);
	}
}
