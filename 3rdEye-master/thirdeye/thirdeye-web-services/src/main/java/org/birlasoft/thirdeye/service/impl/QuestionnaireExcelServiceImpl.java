package org.birlasoft.thirdeye.service.impl;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.beans.QuestionnaireExcelBean;
import org.birlasoft.thirdeye.beans.tco.CostElementBean;
import org.birlasoft.thirdeye.beans.tco.CostStructureBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ExportPurpose;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.repositories.QuestionnaireQuestionRepository;
import org.birlasoft.thirdeye.repositories.ResponseRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ExcelWriter;
import org.birlasoft.thirdeye.service.ExportLogService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireExcelService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.birlasoft.thirdeye.util.ExcelUtility;
import org.birlasoft.thirdeye.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dhruv.sood
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireExcelServiceImpl implements QuestionnaireExcelService{
	
	
	private static Logger logger = LoggerFactory.getLogger(QuestionnaireExcelServiceImpl.class);
	private static final String CELL_RESPONSE = "Questions / Assets";	
	private static final String CELL_RESPONSE_TCO = "Cost Structure / Assets";
	private static final String SHEET_QUESTIONNAIRE = "Questionnaire";
	private static final String TEXT_FORMAT="@";
	private static final String NUMBER_FORMAT="0.0";
	private static final String HIDDEN_FORMAT=";;;";
	private static final String CELL_MODIFIED = "Cell Type has been modified.";
	private static final String HEADING_CELL_MODIFIED = "Heading cell has been modified.";
		
	private static final String SHEET_COA = "Chart Of Accounts";
	
	@Autowired
	private QuestionnaireQuestionRepository questionnaireQuestionRepository;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ResponseDataService responseDataService;
	@Autowired
	private ExportLogService exportLogService;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private ExcelWriter excelWriter;
	@Autowired
	private FunctionalMapService functionalMapService;
	@Autowired
	private ResponseDataService responseDataServiceImpl;
	@Autowired
	private QuestionnaireAssetService  questionnaireAssetService;
	@Autowired
	private ResponseRepository responseRepository;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private TcoResponseService tcoResponseService;
	
	/* 
	 * @author dhruv.sood
	 * 
	 */
	@Override
	public void exportQuestionnaireTemplate(HttpServletResponse response,Questionnaire questionnaire, Response qResponse) {
		String fileName = questionnaire.getName().replaceAll("\\s+", "_");
		String hash = Utility.getUniqueHash();
		XSSFWorkbook workbook = exportQuestionnaireExcel(questionnaire, hash, qResponse);
		if (workbook != null) {
			exportLogService.saveExportLog(questionnaire.getId(), hash,
					ExportPurpose.QE.toString());
		}
		excelWriter.writeExcel(response, workbook, fileName);
	}
	
	/**
	 * Generates the workbook for Questionnaire template
	 * @author dhruv.sood
	 * @param questionnaire
	 * @param hash
	 * @param qResponse
	 * @return
	 */
	private XSSFWorkbook exportQuestionnaireExcel(Questionnaire questionnaire, String hash, Response qResponse) {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet1 = workbook.createSheet(SHEET_QUESTIONNAIRE);

		// Setup the config sheet
		ExcelUtility.setupConfigSheet(hash, workbook);
		
		//set the response id on (0,1) of sheet 2
		XSSFSheet sheet2=workbook.getSheetAt(1);
		Cell row0Cell1 = sheet2.getRow(0).createCell(1);
		row0Cell1.setCellValue(qResponse.getId());
		row0Cell1.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));

		// Get the QQ
		List<QuestionnaireQuestion> questionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(questionnaire, true);
		Map<Question, Set<QuestionnaireAsset>> mapQQ = new HashMap<>();
		
		// Populating the Assets
		Map<Integer, Integer> colQAssetIdMap = populateAssets(workbook,questionnaireQuestions);
		
		// Get the response data
		Map<Integer, Map<Integer,String>> responseMap= getPreFilledResponseData(qResponse);
		
		//Populate questions, qId's , options and response
		populateRows(workbook, questionnaireQuestions, mapQQ, colQAssetIdMap, responseMap);

		Cell staticResponse = sheet1.getRow(1).createCell(1);
		staticResponse.setCellValue(CELL_RESPONSE);
		// Styling Cells for staticResponse
		CellStyle staticResponseCellStyle = ExcelUtility.getCellWithColor(workbook, HSSFColor.YELLOW.index);
		staticResponseCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		staticResponseCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		staticResponse.setCellStyle(staticResponseCellStyle);

		sheet1.createFreezePane(2, 2);
		sheet1.getRow(0).setZeroHeight(true);
		sheet1.setColumnHidden(0, true);
		return workbook;
	}

	/**This method populates the questions and responses
	 * @author dhruv.sood
	 * @param workbook
	 * @param questionnaireQuestions
	 * @param mapQQ
	 * @param colQAssetIdMap
	 * @param responseMap
	 */
	private void populateRows(XSSFWorkbook workbook, List<QuestionnaireQuestion> questionnaireQuestions, Map<Question, Set<QuestionnaireAsset>> mapQQ, Map<Integer, Integer> colQAssetIdMap, Map<Integer, Map<Integer, String>> responseMap) {
		int rowNum = 2;
		for (QuestionnaireQuestion oneQQ : questionnaireQuestions) {
			if (!mapQQ.containsKey(oneQQ.getQuestion().getId())) {
				Set<QuestionnaireAsset> questionnaireAssetSet = new HashSet<>();
				for (QuestionnaireQuestion qq : questionnaireQuestions) {
					if (oneQQ.getQuestion().getId() == qq.getQuestion().getId())
						questionnaireAssetSet.add(qq.getQuestionnaireAsset());
				}
				mapQQ.put(oneQQ.getQuestion(), questionnaireAssetSet);
			}
		}
		
		for (Map.Entry<Question, Set<QuestionnaireAsset>> entry : mapQQ.entrySet()) {
			Question oneQuestion = entry.getKey();
			Set<QuestionnaireAsset> qASet = entry.getValue();
			Row oneRow = workbook.getSheet(SHEET_QUESTIONNAIRE).createRow(rowNum);
			for (int cellNum = 0; cellNum < qASet.size() + 2; cellNum++) {
				Cell oneCell = oneRow.createCell(cellNum);
				if (cellNum == 0){
					oneCell.setCellValue(oneQuestion.getId());
					oneCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));					
				}else {
					if (cellNum == 1) {
						if (oneQuestion.getHelpText().equals("")) {
							oneCell.setCellValue(oneQuestion.getTitle());
						} else
							oneCell.setCellValue(oneQuestion.getTitle()+"("+oneQuestion.getHelpText()+")");
						oneCell.setCellStyle(ExcelUtility.getCellWithColor(workbook,HSSFColor.LEMON_CHIFFON.index));	
						workbook.getSheet(SHEET_QUESTIONNAIRE).setColumnWidth(cellNum, 7000);						
					} else {
						
						Integer qAId = colQAssetIdMap.get(oneCell.getColumnIndex());
						for (QuestionnaireAsset qA : qASet) {
							if (qA.getId() == qAId) {
								populateData(workbook.getSheet(SHEET_QUESTIONNAIRE), oneQuestion, rowNum, cellNum, oneCell);
								
								//populating the prefilled response
								Map<Integer,String> res = responseMap.get(qA.getId());
								String qres="";
								if(res!=null){
								qres = res.get(oneQuestion.getId());}
								
								if(oneQuestion.getQuestionType().equals(QuestionType.NUMBER.toString()) && !(qres==null || qres=="")){
									try {
										oneCell.setCellValue(NumberFormat.getInstance().parse(qres).doubleValue());
									} catch (ParseException e) {
										logger.error("Error in parsing", e);
									}
								}else if(oneQuestion.getQuestionType().equals(QuestionType.DATE.toString()) && !(qres==null || qres=="")){
									oneCell.setCellValue(ExcelUtility.stringToDate(qres));
								}
								else {
									oneCell.setCellValue(qres);
									}																			
							  }
						}
						workbook.getSheet(SHEET_QUESTIONNAIRE).autoSizeColumn(cellNum);
					}					
				}
			}
			rowNum++;
		}
	}

	
	/**This method returns the map of pre-filled response data
	 * @author dhruv.sood
	 * @param questionnaire
	 * @param qResponse
	 * @return
	 */
	private Map<Integer, Map<Integer,String>> getPreFilledResponseData(Response qResponse) {
		
		List<ResponseData> responseData = responseDataService.findByResponse(qResponse);			
		
		Map<Integer, Map<Integer,String>> responseMap = new HashMap<>();
		for(ResponseData rd:responseData)
		{
			 Map<Integer,String> questionMap = new HashMap<>();
			for(ResponseData rd2:responseData)
			{  
				if(getQuestionnaireAssetId(rd) == getQuestionnaireAssetId(rd2))
				{
					processResponseData(questionMap, rd2);	
					responseMap.put(getQuestionnaireAssetId(rd),questionMap);
				}
		    }			
		}	
		return responseMap;	
	}

	/**@author dhruv.sood
	 * @param rd
	 * @return
	 */
	private Integer getQuestionnaireAssetId(ResponseData rd) {
		return rd.getQuestionnaireQuestion().getQuestionnaireAsset().getId();
	}

	/**@author dhruv.sood
	 * @param questionMap
	 * @param rd2
	 */
	private void processResponseData(Map<Integer, String> questionMap, ResponseData rd2) {
		QuestionType questionType = QuestionType.valueOf(rd2.getQuestionnaireQuestion().getQuestion().getQuestionType());
		switch (questionType){
		
		case MULTCHOICE:
			if(rd2.getQuestionnaireQuestion().getQuestion().getBenchmark() == null) {
				JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONMultiChoiceResponseMapper.class);
				if (jsonMultiChoiceResponseMapper != null && !jsonMultiChoiceResponseMapper.getOptions().isEmpty()){				
					questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(),  jsonMultiChoiceResponseMapper.getOptions().get(0).getText()); 
				}
			} else {
				JSONBenchmarkMultiChoiceResponseMapper jsonBenchmarkMultiChoiceResponseMapper = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONBenchmarkMultiChoiceResponseMapper.class);
				if (jsonBenchmarkMultiChoiceResponseMapper != null && !jsonBenchmarkMultiChoiceResponseMapper.getOptions().isEmpty()){				
					questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(),  jsonBenchmarkMultiChoiceResponseMapper.getOptions().get(0).getText()); 
				}
			}
			break;
		case TEXT:
			JSONTextResponseMapper jsonTextResponseMapper = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONTextResponseMapper.class);
			if (jsonTextResponseMapper != null){
				questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(),  jsonTextResponseMapper.getResponseText()); 
			 }
			break;
		case PARATEXT:
			JSONParaTextResponseMapper jsonParaTextResponseMapper = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONParaTextResponseMapper.class);
			if (jsonParaTextResponseMapper != null){
				questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(),  jsonParaTextResponseMapper.getResponseParaText()); 
			 }	
			break;
		case NUMBER:
			JSONNumberResponseMapper jsonNumberResponse = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONNumberResponseMapper.class);
			if (jsonNumberResponse != null){
				questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(), String.valueOf(jsonNumberResponse.getResponseNumber())); 
			 }
			break;
		case DATE:
			JSONDateResponseMapper jsonDateResponseMapper = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONDateResponseMapper.class);
			if (jsonDateResponseMapper != null){
				questionMap.put(rd2.getQuestionnaireQuestion().getQuestion().getId(), ExcelUtility.convertDateFormat(jsonDateResponseMapper.getResponseDate())); 
			 }	
			break;
		default:
			break;
		}
	}

	/**
	 * @author dhruv.sood
	 * @param sheet1
	 * @param oneQuestion
	 * @param rowNum
	 * @param cellNum
	 * @param oneCell
	 */
	private void populateData(XSSFSheet sheet1, Question oneQuestion, int rowNum, int cellNum, Cell oneCell) {
		String queType = oneQuestion.getQuestionType();
		
		if (queType.equals(QuestionType.MULTCHOICE.toString())) {
			int optionsArrayIndex = 0;
			String [] optionsArray;
			if(oneQuestion.getBenchmark() == null){
				JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(oneQuestion.getQueTypeText(),JSONMultiChoiceQuestionMapper.class);
				List<JSONQuestionOptionMapper> options = jsonMultiChoiceQuestionMapper.getOptions();
				optionsArray = new String[options.size()];
				for (JSONQuestionOptionMapper op : options) {
					optionsArray[optionsArrayIndex] = op.getText();
					optionsArrayIndex++;
				}
			} else {
				JSONBenchmarkMultiChoiceQuestionMapper jsonBenchmarkMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(oneQuestion.getQueTypeText(),JSONBenchmarkMultiChoiceQuestionMapper.class);
				List<JSONBenchmarkQuestionOptionMapper> options = jsonBenchmarkMultiChoiceQuestionMapper.getOptions();
				optionsArray = new String[options.size()];
				for (JSONBenchmarkQuestionOptionMapper op : options) {
					optionsArray[optionsArrayIndex] = op.getText();
					optionsArrayIndex++;
				}
			}
			ExcelUtility.generateDropDown(sheet1, rowNum, rowNum, cellNum, cellNum, optionsArray);
			oneCell.setCellStyle(ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),TEXT_FORMAT));
			
		}else if (queType.equals(QuestionType.DATE.toString())) {
			oneCell.setCellStyle(ExcelUtility.getDateFormatCellStyle(sheet1.getWorkbook()));
		}else if (queType.equals(QuestionType.NUMBER.toString())) {
			oneCell.setCellStyle(ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),NUMBER_FORMAT));
		}else if ((queType.equals(QuestionType.TEXT.toString())) || (queType.equals(QuestionType.PARATEXT.toString())) ) {
			oneCell.setCellStyle(ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),TEXT_FORMAT));
		}
	}

	
	/**
	 * @author dhruv.sood
	 * @param workbook
	 * @param questionnaireQuestions
	 * @return
	 */
	private Map<Integer, Integer> populateAssets(XSSFWorkbook workbook,List<QuestionnaireQuestion> questionnaireQuestions) {
		int cellCount = 2;
		Map<Integer, Integer> colQAssetIdMap = new HashMap<>();
		Set<QuestionnaireAsset> questionnaireAssetSet = new HashSet<>();
		for (QuestionnaireQuestion qq : questionnaireQuestions) {
			questionnaireAssetSet.add(qq.getQuestionnaireAsset());
		}
		
		
		Row row0 = workbook.getSheet(SHEET_QUESTIONNAIRE).createRow(0);
		Row row1 = workbook.getSheet(SHEET_QUESTIONNAIRE).createRow(1);

		for (QuestionnaireAsset oneQuestionnaireAsset : questionnaireAssetSet) {
			Cell assetHiddenCell = row0.createCell(cellCount);
			Cell assetCell = row1.createCell(cellCount);
			assetHiddenCell.setCellValue(oneQuestionnaireAsset.getId());
			assetHiddenCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));
			assetCell.setCellValue(new AssetBean(oneQuestionnaireAsset.getAsset()).getShortName());
			assetCell.setCellStyle(ExcelUtility.getCellWithColor(workbook,HSSFColor.AQUA.index));
			colQAssetIdMap.put(assetHiddenCell.getColumnIndex(), oneQuestionnaireAsset.getId());						
			cellCount++;
		}		
		return colQAssetIdMap;
			
	}
	
	@Override
	public void importQuestionnaireResponseExcel(XSSFWorkbook workbook, Questionnaire questionnaire,List<ExcelCellBean> listOfErrorCells){
		
		List<QuestionnaireExcelBean> listQuestionnaireExcelBean = new ArrayList<>();		
		
		//get exportLog by  hashValue getting from workbook 
		ExportLog exportLog = functionalMapService.getExportLogByHashValue(workbook);
		
		// first validate questionnaire import excel sheet 2 for hash security
		boolean isValidate = validateQuestionnaireSheetSecurity(exportLog, questionnaire, listOfErrorCells);
		
		//Now, if excel hash security is valid  then only read questionnaire response excel
		if (isValidate) {			
			listQuestionnaireExcelBean =  readAndValidateQuestionnaireExcelStructureAndPrepareData(workbook.getSheetAt(0), questionnaire, listOfErrorCells);			
		}
		//get the response id stored in config sheet
		int responseId = Utility.convertToInt(workbook.getSheetAt(1).getRow(0).getCell(1).getNumericCellValue());
		
	    // save Questionnaire response data  into Response_Data table and update export log update timeup
		if(listOfErrorCells.isEmpty()){
			if(!listQuestionnaireExcelBean.isEmpty()){
			   List<ResponseData> listOfUpdatedResponseData = prepareUpdatedResponseDataList(questionnaire,listQuestionnaireExcelBean, responseId);
			   responseDataService.save(listOfUpdatedResponseData);
			   exportLogService.updateExportLog(exportLog);
			}else{
				  List<String> errors = new ArrayList<>();
			      errors.add("Uploaded Excel has no data.");
			      functionalMapService.setExcelErrors(listOfErrorCells, errors, null);
		    }
		}	
	}
	
	/**
	 * method will check the hash code security on uploaded workbook
	 * for sheet 2
	 * @author dhruv.sood
	 * @param workbook
	 * @param questionnaire
	 * @param listOfErrorCells
	 */
	private boolean validateQuestionnaireSheetSecurity(ExportLog exportLog,Questionnaire questionnaire, List<ExcelCellBean> listOfErrorCells) {
		boolean isValidate ;		
		if(exportLog !=null && (exportLog.getRefId() == questionnaire.getId()) && (exportLog.getUser().getId() == customUserDetailsService.getCurrentUser().getId()) && !functionalMapService.validateDate(exportLog.getTimeDown(),questionnaire.getUpdatedDate())){
			isValidate =true;
		}else{			
			ExcelCellBean excelCellBean = new ExcelCellBean();
			List<String>errors = new ArrayList<>();		
			errors.add("Uploaded file is not valid.");
			excelCellBean.setErrors(errors);
			listOfErrorCells.add(excelCellBean);
			isValidate =false;
		}
		return isValidate;
	}
	
	@Override
	public boolean isFileExist( MultipartFile file ,List<ExcelCellBean> listOfErrorCells)
	{
		
		boolean isValidate = true ;
		if(file.isEmpty()){
			ExcelCellBean excelCellBean = new ExcelCellBean();
			List<String>errors = new ArrayList<>();		
			errors.add("No file choosen");
			excelCellBean.setErrors(errors);
			listOfErrorCells.add(excelCellBean);
			isValidate =false;
		}
		
		return isValidate;
	}
	
	/**
	 * method to validate uploaded Questionnaire Response excel
	 * structure,and prepare Questionnaire Response data list to
	 * stored in Response_Data table.
	 * @author dhruv.sood
	 * @param firstSheet
	 * @param questionnaire
	 * @param listOfErrorCells
	 * @return listResponseDatas
	 */

	private List<QuestionnaireExcelBean> readAndValidateQuestionnaireExcelStructureAndPrepareData(XSSFSheet firstSheet , Questionnaire questionnaire, List<ExcelCellBean> listOfErrorCells) {
		
		//Get all the questions in the questionnaire
		List<QuestionnaireQuestion> listOfQuestionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(questionnaire, true);
				
		//Get all the assets in the questionnaire
		Set<QuestionnaireAsset> setOfQuestionnaireAssets = new HashSet<>();
		for(QuestionnaireQuestion qq:listOfQuestionnaireQuestions){
			setOfQuestionnaireAssets.add(qq.getQuestionnaireAsset());
		}
		
		//validate the heading cell text on the uploaded sheet
		validateHeadingCell(firstSheet,questionnaire.getQuestionnaireType(), listOfErrorCells);
		
		//validate Assets on Questionnaire uploaded sheet	
		Map<Integer,QuestionnaireAsset> mapOfColumnAndQuestionnaireAsset = validateAssets(firstSheet, setOfQuestionnaireAssets, listOfErrorCells);
		
		//validate Questions on Questionnaire uploaded sheet		
		Map<Integer,QuestionnaireParameter> mapOfRowAndQP = new HashMap<>();		
		Map<Integer,Question> mapOfRowAndQuestions  = new HashMap<>();
		if(questionnaire.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.DEF.toString())){
		  mapOfRowAndQuestions = validateQuestions(firstSheet, listOfQuestionnaireQuestions,mapOfRowAndQP,listOfErrorCells);	
		}else if(questionnaire.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.TCO.toString())){
		  mapOfRowAndQuestions =validateQuestionsForTCO(firstSheet, listOfQuestionnaireQuestions,mapOfRowAndQP,listOfErrorCells);
		}	       
	   //validate excel sheet data for upload and prepare functional map data	
	   return validateAndPrepareQuestionnaireResponseData(firstSheet, mapOfColumnAndQuestionnaireAsset, mapOfRowAndQuestions,mapOfRowAndQP, listOfErrorCells);

	}

	/**Method to validate the heading cell
	 * @author dhruv.sood
	 * @param firstSheet
	 * @param listOfErrorCells
	 */
	private void validateHeadingCell(XSSFSheet firstSheet,String questionnaireType,
			List<ExcelCellBean> listOfErrorCells) {
		List<String> errors = new ArrayList<>();
		
		switch(questionnaireType){
		case "DEF":
			if(firstSheet.getRow(1).getCell(1)!=null && !firstSheet.getRow(1).getCell(1).getStringCellValue().equals(CELL_RESPONSE)){
				errors.add(HEADING_CELL_MODIFIED);
				functionalMapService.setExcelErrors(listOfErrorCells, errors, firstSheet.getRow(1).getCell(1));
		 }
		break;
		case "TCO":
			if(firstSheet.getRow(1).getCell(1)!=null && !firstSheet.getRow(1).getCell(1).getStringCellValue().equals(CELL_RESPONSE_TCO)){
				errors.add(HEADING_CELL_MODIFIED);
				functionalMapService.setExcelErrors(listOfErrorCells, errors, firstSheet.getRow(1).getCell(1));
		 }
		break;
		default:
			errors.add(HEADING_CELL_MODIFIED);
			functionalMapService.setExcelErrors(listOfErrorCells, errors, firstSheet.getRow(1).getCell(1)); 
			break;		
		}
		
		
	}
	

	 /**
     * method to validate uploaded Questionnaire sheet questions
     * and return map of column and Questions.
     * @author dhruv.sood
     * @param firstSheet
     * @param setOfQuestionnaireQuestions
     * @param listOfErrorCells
     * @return mapOfRowAndQuestions
     */
	
	private Map<Integer,Question> validateQuestions(XSSFSheet firstSheet, List<QuestionnaireQuestion> listOfQuestionnaireQuestions,Map<Integer,QuestionnaireParameter> mapOfRowAndQP, List<ExcelCellBean> listOfErrorCells) {

		
		Map<Integer,Question> mapOfQuestions = new HashMap<>();
		Map<Integer,QuestionnaireParameter> mapOfQuestionIdAneQP = new HashMap<>();
		Map<Integer,Question> mapOfRowAndQuestions = new HashMap<>();		
		List<String> errors = new ArrayList<>();
		
		for(QuestionnaireQuestion oneqq :listOfQuestionnaireQuestions){				
			mapOfQuestions.put(oneqq.getQuestion().getId(), oneqq.getQuestion());
			mapOfQuestionIdAneQP.put(oneqq.getQuestion().getId(), oneqq.getQuestionnaireParameter());
		}
						
		for(int rowNum=2;rowNum<mapOfQuestions.values().size()+2;rowNum++){
			Row currentRow = firstSheet.getRow(rowNum);	
			if(currentRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC &&  currentRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && currentRow.getCell(0)!=null){
				String excelHeader;
				String helpText = mapOfQuestions.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())).getHelpText();
				if(helpText != null && !helpText.isEmpty())
					excelHeader = mapOfQuestions.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())).getTitle()+"("+helpText+")";
				else
					excelHeader = mapOfQuestions.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())).getTitle();
				if(excelHeader.equalsIgnoreCase(currentRow.getCell(1).getStringCellValue())){
					mapOfRowAndQuestions.put(currentRow.getCell(0).getRowIndex(), mapOfQuestions.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())));
					mapOfRowAndQP.put(currentRow.getCell(0).getRowIndex(),mapOfQuestionIdAneQP.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())));	
				}else{
					errors.add("Questions Column headers have been modified.");
					functionalMapService.setExcelErrors(listOfErrorCells, errors, currentRow.getCell(1));
				}				
			}else{
	        	errors.add(CELL_MODIFIED);
	        	functionalMapService.setExcelErrors(listOfErrorCells, errors, currentRow.getCell(1));
	         }
		}			
		return mapOfRowAndQuestions;
	}
	
	
	
	private Map<Integer,Question> validateQuestionsForTCO(XSSFSheet firstSheet, List<QuestionnaireQuestion> listOfQuestionnaireQuestions,Map<Integer,QuestionnaireParameter> mapOfRowAndQP, List<ExcelCellBean> listOfErrorCells) {

		Map<Integer,Set<String>> mapOfQuestionTitle = new HashMap<>();
		Map<Integer,Question> mapOfQuestions = new HashMap<>();
		Map<String,QuestionnaireParameter> mapOfQuestionTitleAneQP = new HashMap<>();
		Map<Integer,Question> mapOfRowAndQuestions = new HashMap<>();		
		List<String> errors = new ArrayList<>();
		
		for(QuestionnaireQuestion oneqq :listOfQuestionnaireQuestions){
			Set<String> listOfTitles  = new HashSet<>();			
		    if(mapOfQuestionTitle.get(oneqq.getQuestion().getId())!= null){
		    	listOfTitles = mapOfQuestionTitle.get(oneqq.getQuestion().getId());
		    	listOfTitles.add(getQuestionTitle(oneqq));
		    }else{
		    	listOfTitles.add(getQuestionTitle(oneqq));
		    }
		    
			mapOfQuestionTitle.put(oneqq.getQuestion().getId(), listOfTitles);
			mapOfQuestions.put(oneqq.getQuestion().getId(), oneqq.getQuestion());
			mapOfQuestionTitleAneQP.put(getQuestionTitle(oneqq), oneqq.getQuestionnaireParameter());
		}
						
		for(int rowNum=2;rowNum<mapOfQuestionTitleAneQP.values().size()+2;rowNum++){
			Row currentRow = firstSheet.getRow(rowNum);	
			if(currentRow.getCell(0)!=null && currentRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC &&  currentRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING ){
				if(mapOfQuestionTitle.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())).contains(currentRow.getCell(1).getStringCellValue())){
					mapOfRowAndQuestions.put(currentRow.getCell(0).getRowIndex(), mapOfQuestions.get(Utility.convertToInt(currentRow.getCell(0).getNumericCellValue())));
					mapOfRowAndQP.put(currentRow.getCell(0).getRowIndex(),mapOfQuestionTitleAneQP.get(currentRow.getCell(1).getStringCellValue()));
				}else{
					errors.add("Questions Column headers have been modified.");
					functionalMapService.setExcelErrors(listOfErrorCells, errors, currentRow.getCell(1));
				}				
			}else{
	        	errors.add(CELL_MODIFIED);
	        	functionalMapService.setExcelErrors(listOfErrorCells, errors, currentRow.getCell(1));
	         }
		}			
		return mapOfRowAndQuestions;
	}
	
	private String getQuestionTitle(QuestionnaireQuestion qq){
		String questionTitle = "";
		if(qq.getQuestionnaire().getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.DEF.toString())){
			questionTitle = qq.getQuestion().getTitle();
		}else if(qq.getQuestionnaire().getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.TCO.toString())){
			questionTitle = qq.getQuestionnaireParameter().getParameterByParentParameterId().getDisplayName() +"/"+
					        qq.getQuestionnaireParameter().getParameterByParameterId().getDisplayName() +"/"+
					        qq.getQuestion().getTitle();
		}
		return questionTitle;
	}
	
	/**
     * Method to validate uploaded Questionnaire sheet Assets
     * and return map of column and AssetBean.
     * @author dhruv.sood
     * @param firstSheet
     * @param setOfQuestionnaireAssets
     * @param listOfErrorCells
     * @return mapOfRowAndAssetBean
     */
	
	private Map<Integer,QuestionnaireAsset> validateAssets(XSSFSheet firstSheet, Set<QuestionnaireAsset> setOfQuestionnaireAssets, List<ExcelCellBean> listOfErrorCells) {
	
		Map<Integer,QuestionnaireAsset> mapOfQuestionnaireAsset = new HashMap<>();
		Map<Integer,QuestionnaireAsset> mapOfColumnAndQuestionnaireAsset = new HashMap<>();
		List<String> errors = new ArrayList<>();
		
		for(QuestionnaireAsset oneqa :setOfQuestionnaireAssets){
			mapOfQuestionnaireAsset.put(oneqa.getId(),oneqa);
		}
		
		for(int cellNum=2;cellNum<=setOfQuestionnaireAssets.size()+1;cellNum++){
			Row row0=firstSheet.getRow(0);
			Row row1=firstSheet.getRow(1);
			if(row0.getCell(cellNum).getCellType() == Cell.CELL_TYPE_NUMERIC &&  row1.getCell(cellNum).getCellType() == Cell.CELL_TYPE_STRING && row0.getCell(cellNum)!=null){
				AssetBean ab=new AssetBean(mapOfQuestionnaireAsset.get(Utility.convertToInt(row0.getCell(cellNum).getNumericCellValue())).getAsset());
				if(ab.getShortName().equalsIgnoreCase(row1.getCell(cellNum).getStringCellValue())){
					mapOfColumnAndQuestionnaireAsset.put(cellNum, mapOfQuestionnaireAsset.get(Utility.convertToInt(row0.getCell(cellNum).getNumericCellValue())));
				}else{
					errors.add("Assets have been modified.");
					functionalMapService.setExcelErrors(listOfErrorCells, errors, row1.getCell(cellNum));
				}				
			}else{
	        	errors.add(CELL_MODIFIED);
	        	functionalMapService.setExcelErrors(listOfErrorCells, errors, row1.getCell(cellNum));
	         }
		}		
		return mapOfColumnAndQuestionnaireAsset;
	}
	
	/**
	 * method to validate the excel data for upload in 
	 * Response_Data table
	 * @author dhruv.sood
	 * @param firstSheet
	 * @param questionnaire
	 * @param mapOfRowAndAssetBean
	 * @param mapOfColumnAndQuestions
	 * @param listOfErrorCells
	 * @return listQuestionnaireExcelBean
	 */
	private List<QuestionnaireExcelBean> validateAndPrepareQuestionnaireResponseData(XSSFSheet firstSheet, Map<Integer,QuestionnaireAsset> mapOfColumnAndQuestionnaireAsset, Map<Integer,Question> mapOfRowAndQuestions,Map<Integer,QuestionnaireParameter> mapOfRowAndQP, List<ExcelCellBean> listOfErrorCells) {
		
		List<QuestionnaireExcelBean> listQuestionnaireExcelBean =new ArrayList<>(); 
		List<String> errors = new ArrayList<>();
		
		String responseData ;
				
		//Going row wise		
		for(int rowNum:mapOfRowAndQuestions.keySet()){
			Question currentQuestion= mapOfRowAndQuestions.get(rowNum);//since one question for one row in excel
			QuestionnaireParameter 	questionQP = mapOfRowAndQP.get(rowNum);		
			//going per cell in a row
			for(Cell oneCell:firstSheet.getRow(rowNum)){
				
				if(oneCell.getColumnIndex()>1){
					
				QuestionType questionType = QuestionType.valueOf(currentQuestion.getQuestionType());
				switch(questionType){
						case MULTCHOICE:
						if(   oneCell.getCellType()==1 ||oneCell.getCellType()==3){
							boolean matched=false;
							String cellValue=oneCell.getStringCellValue();
							if(currentQuestion.getBenchmark() == null){
								JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(currentQuestion.getQueTypeText(), JSONMultiChoiceQuestionMapper.class);
								List<JSONQuestionOptionMapper>  listOfOptions=jsonMultiChoiceQuestionMapper.getOptions();
								if(!listOfOptions.isEmpty()){
									for(JSONQuestionOptionMapper oneOption:listOfOptions){
										if(oneOption.getText().equals(cellValue) || ("").equals(cellValue)){
											matched=true;
										}
									}
								}
								responseData = functionalMapService.getResponseData(currentQuestion.getQueTypeText(), oneCell.getStringCellValue());
							} else {
								JSONBenchmarkMultiChoiceQuestionMapper jsonBenchmarkMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(currentQuestion.getQueTypeText(), JSONBenchmarkMultiChoiceQuestionMapper.class);
								List<JSONBenchmarkQuestionOptionMapper>  listOfOptions=jsonBenchmarkMultiChoiceQuestionMapper.getOptions();
								if(!listOfOptions.isEmpty()){
									for(JSONBenchmarkQuestionOptionMapper oneOption:listOfOptions){
										if(oneOption.getText().equals(cellValue) || ("").equals(cellValue)){
											matched=true;
										}
									}
								}
								responseData = functionalMapService.getResponseDataForBenchmarkMCQ(currentQuestion.getQueTypeText(), oneCell.getStringCellValue());
							}
							if(!matched){
								errors.add("Option contains invalid value.");
								functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
							}
							QuestionnaireAsset questionnaireAsset= mapOfColumnAndQuestionnaireAsset.get(oneCell.getColumnIndex());
							fillQuestionnaireExcelBean(listQuestionnaireExcelBean, responseData, currentQuestion, questionnaireAsset,questionQP);							
							
						}else{
							errors.add(CELL_MODIFIED);
							functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
						}					
						break;
						case TEXT:
						if(oneCell.getCellType()==1){
							JSONTextResponseMapper jsonTextResponseMapper = new JSONTextResponseMapper();
							jsonTextResponseMapper.setResponseText(oneCell.getStringCellValue());
							responseData = Utility.convertObjectToJSONString(jsonTextResponseMapper);
			
							QuestionnaireAsset questionnaireAsset= mapOfColumnAndQuestionnaireAsset.get(oneCell.getColumnIndex());
							fillQuestionnaireExcelBean(listQuestionnaireExcelBean, responseData, currentQuestion, questionnaireAsset,questionQP);
							
						}else{
							errors.add(CELL_MODIFIED);
							functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
						}
						break;
						case PARATEXT:
						if(oneCell.getCellType()==1){
							JSONParaTextResponseMapper jsonParaTextResponseMapper = new JSONParaTextResponseMapper();
							jsonParaTextResponseMapper.setResponseParaText(oneCell.getStringCellValue());
							responseData = Utility.convertObjectToJSONString(jsonParaTextResponseMapper);
			
							QuestionnaireAsset questionnaireAsset= mapOfColumnAndQuestionnaireAsset.get(oneCell.getColumnIndex());
							fillQuestionnaireExcelBean(listQuestionnaireExcelBean, responseData, currentQuestion, questionnaireAsset,questionQP);
							
						}else{
							errors.add(CELL_MODIFIED);
							functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
						}	
						break;
						case CURRENCY:
						case NUMBER:
						if(oneCell.getCellType()==0){
						JSONNumberResponseMapper jsonNumberResponseMapper = new JSONNumberResponseMapper();
						jsonNumberResponseMapper.setResponseNumber(oneCell.getNumericCellValue());
						jsonNumberResponseMapper.setQuantifier(oneCell.getNumericCellValue());
						responseData = Utility.convertObjectToJSONString(jsonNumberResponseMapper);
		
						QuestionnaireAsset questionnaireAsset= mapOfColumnAndQuestionnaireAsset.get(oneCell.getColumnIndex());
						fillQuestionnaireExcelBean(listQuestionnaireExcelBean, responseData, currentQuestion, questionnaireAsset,questionQP);
						}else{
						  if(oneCell.getCellType()!=3){
							errors.add(CELL_MODIFIED);
							functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
						  }
						}
						break;						
						case DATE:
							try {
								JSONDateResponseMapper jsonDateResponseMapper = new JSONDateResponseMapper();
								jsonDateResponseMapper.setResponseDate(ExcelUtility.convertDateToString(oneCell.getDateCellValue()));
								responseData = Utility.convertObjectToJSONString(jsonDateResponseMapper);
								
								QuestionnaireAsset questionnaireAsset= mapOfColumnAndQuestionnaireAsset.get(oneCell.getColumnIndex());
								fillQuestionnaireExcelBean(listQuestionnaireExcelBean, responseData, currentQuestion, questionnaireAsset,questionQP);
								
							}catch (Exception e) {
								logger.error("Exception in validateAndPrepareQuestionnaireResponseData() :: "+e);
								errors.add("Date Type has been modified.");
								functionalMapService.setExcelErrors(listOfErrorCells, errors,oneCell);
							 }
							break;
							default:
							 break;	
					
		        }
			    }			
			
			}		
			
		}
		
		return listQuestionnaireExcelBean;
	}
		
		

	/**Filling the QuestionnaireExcelBean
	 * @author dhruv.sood
	 * @param listQuestionnaireExcelBean
	 * @param responseData
	 * @param currentQuestion
	 * @param questionnaireAsset
	 */
	private void fillQuestionnaireExcelBean(List<QuestionnaireExcelBean> listQuestionnaireExcelBean, String responseData, Question currentQuestion,	QuestionnaireAsset questionnaireAsset ,QuestionnaireParameter questionnaireParameter) {
		QuestionnaireExcelBean questionnaireExcelBean=new QuestionnaireExcelBean();
		questionnaireExcelBean.setQuestionnaireAsset(questionnaireAsset);
		questionnaireExcelBean.setQuestion(currentQuestion);
		questionnaireExcelBean.setQuestionnaireParameter(questionnaireParameter);
		questionnaireExcelBean.setResponseData(responseData);
		listQuestionnaireExcelBean.add(questionnaireExcelBean);		
	}
	
	/**Preparing the final response data that is to be saved to the database
	 * @author dhruv.sood
	 * @param questionnaire
	 * @param listQuestionnaireExcelBean
	 * @param responseId
	 * @return
	 */
	private List<ResponseData> prepareUpdatedResponseDataList(Questionnaire questionnaire, List<QuestionnaireExcelBean> listQuestionnaireExcelBean, int responseId){
		
		List<ResponseData> listOfUpdatedResponseData=new ArrayList<>();
		
		//get the response based on responseId
		Response response=responseRepository.findByIdLoadedResponseDatas(responseId);
		
		//get the original reponse_data on response
		List<ResponseData> originalRdList = responseDataServiceImpl.findByResponse(response);
		
		Iterator<QuestionnaireExcelBean> iterateExcelData= listQuestionnaireExcelBean.iterator();
				
		while(iterateExcelData.hasNext()){
			QuestionnaireExcelBean oneExcelBean = iterateExcelData.next();
			ResponseData rd=new ResponseData();			
			for(ResponseData oriData : originalRdList){
				if(oriData.getQuestionnaireQuestion().getQuestionnaireAsset().getId()==oneExcelBean.getQuestionnaireAsset().getId() 
						&& oriData.getQuestionnaireQuestion().getQuestion().getId()==oneExcelBean.getQuestion().getId()
						&& oriData.getQuestionnaireQuestion().getQuestionnaireParameter().getId()==oneExcelBean.getQuestionnaireParameter().getId()
						&& oriData.getResponse().getId()==response.getId()){
					rd.setId(oriData.getId());
				}
			}	
			Set<QuestionnaireQuestion> qqSet=questionnaireQuestionRepository.findByQuestionnaireAssetAndQuestionAndQuestionnaireAndQuestionnaireParameter(oneExcelBean.getQuestionnaireAsset(), oneExcelBean.getQuestion(), questionnaire,oneExcelBean.getQuestionnaireParameter());
			for(QuestionnaireQuestion oneqq:qqSet)
			{
				rd.setResponseData(oneExcelBean.getResponseData());
				rd.setQuestionnaireQuestion(oneqq);
				rd.setResponse(response);
				listOfUpdatedResponseData.add(rd);
				
			}			
		}	
		return listOfUpdatedResponseData;
	}
	
	
	
	/**Method to export chart of account cost response excel
	 * @author dhruv.sood
	 * @param qResponse
	 * @param questionnaire
	 * @param response
	 */
	@Override
	public void exportCOATemplate(HttpServletResponse response,Questionnaire questionnaire, Response qResponse) {
		String fileName = questionnaire.getName().replaceAll("\\s+", "_");
		String hash = Utility.getUniqueHash();
		XSSFWorkbook workbook = exportCOAExcel(questionnaire, hash, qResponse);
		if (workbook != null) {
			exportLogService.saveExportLog(questionnaire.getId(), hash, ExportPurpose.TCO.toString());
		}
		excelWriter.writeExcel(response, workbook, fileName);
	}
	
	/**
	 * Generates the workbook for Chart of Account Template
	 * @author dhruv.sood
	 * @param questionnaire
	 * @param hash
	 * @param qResponse
	 * @return
	 */
	private XSSFWorkbook exportCOAExcel(Questionnaire questionnaire, String hash, Response qResponse) {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet1 = workbook.createSheet(SHEET_COA);

		//Setup the config sheet
		ExcelUtility.setupConfigSheet(hash, workbook); 
		
		//set the response id on (0,1) of sheet 2
		XSSFSheet sheet2=workbook.getSheetAt(1);
		Cell row0Cell1 = sheet2.getRow(0).createCell(1);
		row0Cell1.setCellValue(qResponse.getId());
		row0Cell1.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));
					
		// Get the QQ
		List<QuestionnaireQuestion> questionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(questionnaire, true);
		
		
		// First, populating the Assets as Columns
		Map<Integer, Integer> colQAssetIdMap = populateTCOExcelAssets(workbook,questionnaireQuestions);
		
		//Cost Structure Bean which contains full tree
		List<CostStructureBean> listOfCostStructureBean = tcoResponseService.extractChartOfAccounts(questionnaire).get(0).getListOfCostStructure();
			
		int rowCount = 2;
		Map<Integer,Integer> mapOfRowAndQP = new HashMap<>();
		for (CostStructureBean costStructureBean : listOfCostStructureBean) {
				
		//Populating the Cost Structures and Cost Element names
		rowCount = populateCostStructures(workbook, costStructureBean , rowCount, costStructureBean.getDisplayName(), mapOfRowAndQP);
		}
		// Get the response data
		Map<Integer, Map<Integer,String>> responseMap= getTCOPreFilledResponseData(qResponse);
						
		//Populate questions, qqId's , costStructure Text and response
		populateTCORows(workbook, colQAssetIdMap, responseMap, rowCount,mapOfRowAndQP);	
		
		Cell costStructure = sheet1.getRow(1).createCell(1);
		costStructure.setCellValue(CELL_RESPONSE_TCO);
					
		// Styling Cells for costStructure
		CellStyle staticCostStructureCellStyle = ExcelUtility.getCellWithColor(workbook, HSSFColor.YELLOW.index);
		staticCostStructureCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		staticCostStructureCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		costStructure.setCellStyle(staticCostStructureCellStyle);
		
		sheet1.setColumnWidth(1, 7000);
		sheet1.createFreezePane(2, 1);
		sheet1.getRow(0).setZeroHeight(true);		
		sheet1.setColumnHidden(0, true);
		//sheet1.setColumnHidden(1, true);
		return workbook;
	}
	
	
	/**@author dhruv.sood
	 * @param workbook
	 * @param costStructureBean
	 * @param rowCount
	 * @param csName
	 * @return rowCount
	 */
	public int populateCostStructures(XSSFWorkbook workbook, CostStructureBean costStructureBean, Integer rowCount, String csName, Map<Integer,Integer>mapOfRowAndQP){
		
		//Check, If Child Cost Structure Exists?
		if(!costStructureBean.getChildCostStructure().isEmpty()){
			
			// If yes, then iterate it
			for(CostStructureBean oneChildCostStructureBean : costStructureBean.getChildCostStructure()){
				String newCsName = csName+"/"+oneChildCostStructureBean.getDisplayName();
				
				//If child cost structure is TCOL then populate it to excel
				if(!oneChildCostStructureBean.getChildCostElement().isEmpty()){
					rowCount = populateCostElement(workbook, oneChildCostStructureBean.getChildCostElement(), rowCount, newCsName,mapOfRowAndQP);
				}else{
					//Recursion for child cost Structures
					rowCount = populateCostStructures(workbook, oneChildCostStructureBean, rowCount, csName+"/"+oneChildCostStructureBean.getDisplayName(),mapOfRowAndQP);
				}					
			}
		}		
		//Now, same way check for cost element bean and populate it to excel
		else if(!costStructureBean.getChildCostElement().isEmpty()){			
			rowCount = populateCostElement(workbook, costStructureBean.getChildCostElement(), rowCount, csName , mapOfRowAndQP);			
		}
		return rowCount;
}


	/**@author dhruv.sood
	 * @param workbook
	 * @param listChildCostElementBean
	 * @param rowCount
	 * @param csName
	 * @return
	 */
	private int populateCostElement(XSSFWorkbook workbook, List<CostElementBean> listChildCostElementBean, int rowCount, String csName, Map<Integer,Integer>mapOfRowAndQP) {
		
		for(CostElementBean oneChildCostElementBean:listChildCostElementBean){		
	
			Row csRow = workbook.getSheet(SHEET_COA).createRow(rowCount);
			
			//Populate the cost element hidden cell
			Cell newRowCEHiddenCell = csRow.createCell(0);
			newRowCEHiddenCell.setCellValue(oneChildCostElementBean.getQqBean().getQuestion().getId());
			newRowCEHiddenCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));
			
			QuestionnaireQuestion qq = questionnaireQuestionService.findOne(oneChildCostElementBean.getQqBean().getQuestionnaireQuestionId(), true);
			mapOfRowAndQP.put(rowCount, qq.getQuestionnaireParameter().getId());
			
			//Populate the cost structure to row of excel
			Cell newRowCSCell = csRow.createCell(1);					
			newRowCSCell.setCellValue(csName+"/"+oneChildCostElementBean.getTitle());
			newRowCSCell.setCellStyle(ExcelUtility.getCellWithColor(workbook,HSSFColor.LEMON_CHIFFON.index));
				
			rowCount++;
		}		
		return rowCount;
	}
	
	
	/**This method populates the TCO assets on the excel 
	 * @author dhruv.sood
	 * @param workbook
	 * @param questionnaireQuestions
	 * @return
	 */
	private Map<Integer, Integer> populateTCOExcelAssets(XSSFWorkbook workbook,List<QuestionnaireQuestion> questionnaireQuestions) {
		int cellCount = 2;
		Map<Integer, Integer> colQAssetIdMap = new HashMap<>();
		Set<QuestionnaireAsset> questionnaireAssetSet = new HashSet<>();
		for (QuestionnaireQuestion qq : questionnaireQuestions) {
			questionnaireAssetSet.add(qq.getQuestionnaireAsset());
		}		
		
		List<QuestionnaireAsset> questionnaireAssetList = new ArrayList<>(questionnaireAssetSet);
		
		//Sorting the asset names in alphabetical order  		  		
		questionnaireAssetList.sort((obj1, obj2) -> (new AssetBean(obj1.getAsset()).getShortName()).compareTo(new AssetBean(obj2.getAsset()).getShortName()));
		
		Row row0 = workbook.getSheet(SHEET_COA).createRow(0);
		Row row1 = workbook.getSheet(SHEET_COA).createRow(1);

		for (QuestionnaireAsset oneQuestionnaireAsset : questionnaireAssetList) {
				Cell assetHiddenCell = row0.createCell(cellCount);
				Cell assetCell = row1.createCell(cellCount);
				assetHiddenCell.setCellValue(oneQuestionnaireAsset.getId());
				assetHiddenCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));
				assetCell.setCellValue(new AssetBean(oneQuestionnaireAsset.getAsset()).getShortName());
				assetCell.setCellStyle(ExcelUtility.getCellWithColor(workbook,HSSFColor.AQUA.index));
				colQAssetIdMap.put(assetHiddenCell.getColumnIndex(), oneQuestionnaireAsset.getId());
				workbook.getSheet(SHEET_COA).autoSizeColumn(cellCount);
				cellCount++;
		}		
		return colQAssetIdMap;	
	}
	
	/**This method returns the map of pre-filled response data for TCO
	 * @author dhruv.sood
	 * @param questionnaire
	 * @param qResponse
	 * @return
	 */
	private Map<Integer, Map<Integer,String>> getTCOPreFilledResponseData(Response qResponse) {
		
		List<ResponseData> responseData = responseDataService.findByResponse(qResponse);			
		
		Map<Integer, Map<Integer,String>> responseMap = new HashMap<>();
		for(ResponseData rd:responseData)
		{
			 Map<Integer,String> questionMap = new HashMap<>();
			for(ResponseData rd2:responseData)
			{  
				if(getQuestionnaireAssetId(rd) == getQuestionnaireAssetId(rd2))
				{
					processTCOResponseData(questionMap, rd2);	
					responseMap.put(getQuestionnaireAssetId(rd),questionMap);
				}
		    }			
		}	
		return responseMap;	
	}
	
	
	/**This method extracts the pre-filled response
	 * @author dhruv.sood
	 * @param questionMap
	 * @param rd2
	 */
	private void processTCOResponseData(Map<Integer, String> questionMap, ResponseData rd2) {
		
		JSONNumberResponseMapper jsonNumberResponse = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONNumberResponseMapper.class);
		if (jsonNumberResponse != null){
			questionMap.put(rd2.getQuestionnaireQuestion().getId(), String.valueOf(jsonNumberResponse.getResponseNumber())); 
		 }
	}
	
	/**This method populates the questions and responses to TCO excel
	 * @author dhruv.sood
	 * @param workbook
	 * @param questionnaireQuestions
	 * @param mapQQ
	 * @param colQAssetIdMap
	 * @param responseMap
	 */
	private void populateTCORows(XSSFWorkbook workbook, Map<Integer, Integer> colQAssetIdMap, Map<Integer, Map<Integer, String>> responseMap, int rowCount,Map<Integer,Integer>mapOfRowAndQP) {
		
		for(Map.Entry<Integer, Integer> oneEntry :colQAssetIdMap.entrySet()){
				int oneqaid = oneEntry.getValue();
				
				for(int respQaid:responseMap.keySet()){
					if(oneqaid==respQaid){
						for(int i=2;i<rowCount;i++){
							Row oneRow = workbook.getSheet(SHEET_COA).getRow(i);
							Double qIdXL = oneRow.getCell(0).getNumericCellValue();
							Integer qpId = mapOfRowAndQP.get(i);
							
							//Now, get the QQ
							QuestionnaireQuestion qq = questionnaireQuestionService.findByQuestionAndQuestionnaireParameterAndQuestionnaireAsset(questionService.findOne(qIdXL.intValue()) , questionnaireParameterService.findOne(qpId),questionnaireAssetService.findOne(oneqaid));
							
							for(Map.Entry<Integer, String> oneResponseEntry :responseMap.get(respQaid).entrySet()){
								if(qq.getId()==oneResponseEntry.getKey()){
									Cell oneCell = oneRow.createCell(oneEntry.getKey());								
									if(responseMap.get(respQaid).get(oneResponseEntry.getKey())!= "null")
									 oneCell.setCellValue(Double.parseDouble(responseMap.get(respQaid).get(oneResponseEntry.getKey())));									
									oneCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,NUMBER_FORMAT));
								}
							}							
						}
					}
				}
			}
			
		}		
		

}
