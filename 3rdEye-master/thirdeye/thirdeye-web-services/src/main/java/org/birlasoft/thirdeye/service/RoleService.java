package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.RolePermission;

/**
 * Service interface for role.
 */
public interface RoleService {
	/**
	 * List of all {@code Role}
	 * @return List{@code <Role>} Object
	 */
    public List<Role> findAll();
    /**
     * Find one role object by id.
     * @param id
     * @return {@code Role}
     */
    public Role findOne(Integer id);
    
	public Role save(Role oneRole);
	
	public void delete(Set<RolePermission> listOfRolePermsToDelete);
	
	public void saveRolePermissions(Set<RolePermission> newRolePermissionList);
	public Role findByIdLoadedRolePermissions(Integer id);
}
