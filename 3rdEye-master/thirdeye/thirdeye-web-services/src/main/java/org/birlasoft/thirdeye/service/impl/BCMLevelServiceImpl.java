package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.repositories.BCMLevelRepository;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of BCm Level service method.
 *
 */
@Service("bcmLevelService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BCMLevelServiceImpl implements BCMLevelService {

	
	@Autowired
	private BCMLevelRepository bcmLevelRepository;
	@Autowired
	private BCMService  bcmService;
	@Autowired
	CustomUserDetailsService customUserDetailsService;
   
	@Override
	public BcmLevel save(BcmLevel bcmLevel) {
		return bcmLevelRepository.save(bcmLevel);
	}

	@Override
	public List<BcmLevel> findByBcm(Bcm bcm, boolean loaded) {
		List<BcmLevel> bcmLevels = bcmLevelRepository.findByBcm(bcm);

		if (loaded) {
			for (BcmLevel bcmLevel : bcmLevels) {
				bcmLevel.getBcm().getBcmName();
				if (bcmLevel.getBcmLevel() != null)
					bcmLevel.getBcmLevel().getBcmLevelName();
			}
		}

		return bcmLevels;
	}
	
	@Override
	public List<BcmLevel> findByBcmAndLevelNumberIn(Bcm bcm, Set<Integer> levelNumber, boolean loaded) {
		List<BcmLevel> bcmLevels = bcmLevelRepository.findByBcmAndLevelNumberIn(bcm, levelNumber);
		
		if (loaded) {
			for (BcmLevel bcmLevel : bcmLevels) {
				bcmLevel.getBcm().getBcmName();
				
				// This is the parent!
				if (bcmLevel.getBcmLevel() != null)
					bcmLevel.getBcmLevel().getBcmLevelName();
			}
		}

		return bcmLevels;
	}
	
	@Override
	public List<BcmLevel> findByBcmAndLevelNumberIn(List<Bcm> bcm,Integer levelNumber){
		return bcmLevelRepository.findByBcmAndLevelNumberIn(bcm, levelNumber);
	}

	@Override
	public List<BcmLevelBean> generateLevelTreeFromBcmLevels(List<BcmLevel> listOfLoadedBcmLevels) {
		
		Map<Integer, BcmLevelBean> levelMap = new HashMap<>();
		// Load the BCM level into the hashmap for quick access
		for (BcmLevel oneBcmLevel : listOfLoadedBcmLevels) {
			levelMap.put(oneBcmLevel.getId(), new BcmLevelBean(oneBcmLevel));
		}

		// Now create tree properly with the beans
		for (BcmLevel oneBcmLevel : listOfLoadedBcmLevels) {
			if (oneBcmLevel.getBcmLevel() != null) {
				// Link up the parameter bean to the parent
				BcmLevelBean parent = levelMap.get(oneBcmLevel.getBcmLevel().getId());
				BcmLevelBean child = levelMap.get(oneBcmLevel.getId());

				if (parent != null) {
					parent.addChildLevel(child);
					child.setParent(parent);
				}
			}
		}

		// Find the BCM root levels
		List<BcmLevelBean> listToBeReturned = new ArrayList<>();
		for (BcmLevelBean lb : levelMap.values()) {
			// This means that it is a root of the tree
			if (lb.getParent() == null) {
				listToBeReturned.add(lb);
			}
		}
		
		getSortedListOfBcmLevels(listToBeReturned);
		return listToBeReturned;
	}

	private void getSortedListOfBcmLevels(List<BcmLevelBean> listToBeReturned) {
		Collections.sort(listToBeReturned, new SequenceNumberComparator());
		for (BcmLevelBean bcmLevelBean : listToBeReturned) {
			if(bcmLevelBean.isParent()){
				getSortedListOfBcmLevels(bcmLevelBean.getChildLevels());
			}
		}
	}

	@Override
	public BcmLevel findOne(Integer parentLevelId) {
		return bcmLevelRepository.findOne(parentLevelId);
	}
	@Override
	public List<BcmLevel> findByBcmLevelIdIn(Integer[] parentLevelId){
		return bcmLevelRepository.findByBcmLevelIdIn(parentLevelId);
		
	}  	
	@Override
	public List<BcmLevel> findByBcmLevel(BcmLevel parentBcmLevel) {
		List<BcmLevel> bcmLevels = bcmLevelRepository
				.findByBcmLevel(parentBcmLevel);

		for (BcmLevel bcmLevel : bcmLevels) {
			if (bcmLevel.getBcmLevel() != null)
				bcmLevel.getBcmLevel().getId();
			bcmLevel.getBcm().getId();
		}
		return bcmLevels;
	}

	@Override
	public void deleteLevel(Integer bcmId, Integer levelId) {

		BcmLevel toBeDeleted = findOne(levelId);

		if (toBeDeleted.getBcm().getId() == bcmId) {
			List<BcmLevel> levelsForDelete = new ArrayList<>();
			levelsForDelete.add(toBeDeleted);
			deleteCompleteLevel(levelsForDelete);
		}
	}

	private void deleteCompleteLevel(List<BcmLevel> levelsForDelete) {

		Iterator<BcmLevel> iter = levelsForDelete.iterator();
		while (iter.hasNext()) {
			BcmLevel bl = iter.next();
			List<BcmLevel> bcmLevelsToBeDeleted = findByBcmLevel(bl);
			if (!bcmLevelsToBeDeleted.isEmpty()) {
				deleteCompleteLevel(bcmLevelsToBeDeleted);
			}
			delete(bl);
		}
	}

	@Override
	public void delete(BcmLevel bl) {
		bcmLevelRepository.delete(bl);
	}

	/**
	 * 
	 * @param bls
	 */
	public void deleteInBatch(List<BcmLevel> bls) {
		bcmLevelRepository.deleteInBatch(bls);
	}

	@Override
	public BcmLevel findTopByBcmAndLevelNumberOrderBySequenceNumberDesc(
			Bcm bcm, Integer levelNumber) {
		return bcmLevelRepository
				.findTopByBcmAndLevelNumberOrderBySequenceNumberDesc(bcm,
						levelNumber);
	}

	@Override
	public BcmLevel findTopByBcmLevelOrderBySequenceNumberDesc(BcmLevel bcmLevel) {
		return bcmLevelRepository
				.findTopByBcmLevelOrderBySequenceNumberDesc(bcmLevel);
	}

	@Override
	public List<BcmLevel> save(List<BcmLevel> bcmLevels) {
		return bcmLevelRepository.save(bcmLevels);
	}

	@Override
	public BcmLevel findOneLoadedBcmLevel(Integer bcmLevelId) {
		BcmLevel bcmLevel = findOne(bcmLevelId);
		if (bcmLevel.getBcmLevel() != null)
			bcmLevel.getBcmLevel().getBcmLevelName();
		return bcmLevel;
	}

	@Override
	public void createBcmLevel(List<BcmLevel> bcmLevels, Bcm newBcm) {
		for (BcmLevel oneLevel : bcmLevels) {
			BcmLevel copyLevel = new BcmLevel();
			if (oneLevel.getBcmLevel() == null) {
				copyLevel.setBcm(newBcm);
				copyLevel.setBcmLevelName(oneLevel.getBcmLevelName());
				copyLevel.setSequenceNumber(oneLevel.getSequenceNumber());
				copyLevel.setLevelNumber(oneLevel.getLevelNumber());
				copyLevel.setDescription(oneLevel.getDescription());
				copyLevel.setCategory(oneLevel.getCategory());
				copyLevel.setUserByCreatedBy(customUserDetailsService
						.getCurrentUser());
				copyLevel.setCreatedDate(new Date());
				copyLevel.setUserByUpdatedBy(customUserDetailsService
						.getCurrentUser());
				copyLevel.setUpdatedDate(new Date());
				bcmLevelRepository.save(copyLevel);

				List<BcmLevel> childBcms = findByBcmLevel(oneLevel);
				if (!childBcms.isEmpty()) {
					createChildBcm(copyLevel, childBcms, newBcm);
				}
			}
		}
	}

	/**
	 * Create a copy of BCM child levels
	 * 
	 * @param parentBcm
	 * @param childBcms
	 * @param newBcm
	 */
	private void createChildBcm(BcmLevel parentBcm, List<BcmLevel> childBcms,
			Bcm newBcm) {
		for (BcmLevel oneLevel : childBcms) {
			BcmLevel copyLevel = new BcmLevel();
			copyLevel.setBcm(newBcm);
			copyLevel.setBcmLevel(parentBcm);
			copyLevel.setBcmLevelName(oneLevel.getBcmLevelName());
			copyLevel.setSequenceNumber(oneLevel.getSequenceNumber());
			copyLevel.setLevelNumber(oneLevel.getLevelNumber());
			copyLevel.setDescription(oneLevel.getDescription());
			copyLevel.setCategory(oneLevel.getCategory());
			copyLevel.setUserByCreatedBy(customUserDetailsService
					.getCurrentUser());
			copyLevel.setCreatedDate(new Date());
			copyLevel.setUserByUpdatedBy(customUserDetailsService
					.getCurrentUser());
			copyLevel.setUpdatedDate(new Date());

			bcmLevelRepository.save(copyLevel);

			List<BcmLevel> oneLevelChildBcms = findByBcmLevel(oneLevel);
			if (!oneLevelChildBcms.isEmpty()) {
				createChildBcm(copyLevel, oneLevelChildBcms, newBcm);
			}
		}
	}

	@Override
	public BcmLevel saveBcmLevel(BcmLevelBean bcmLevelBean, Integer bcmId,
			Integer parentLevelId) {
		BcmLevel bcmLevel ;
		if(bcmLevelBean.getId() == null){
			bcmLevel = createBcmlevelObject(bcmLevelBean, bcmId, parentLevelId);
		}else{
			bcmLevel = updateBcmlevelObject(bcmLevelBean);
		}		
		return bcmLevelRepository.save(bcmLevel) ;
	
	}
	
	/**
	 * create BCM level object.
	 * @param bcmLevelBean
	 * @param bcmId
	 * @param parentLevelId
	 * @return {@code BcmLevel}
	 */
	private BcmLevel createBcmlevelObject(BcmLevelBean bcmLevelBean,
			Integer bcmId, Integer parentLevelId) {
		Bcm bcm = bcmService.findOne(bcmId); 
		BcmLevel bcmLevel = new BcmLevel();
		bcmLevel.setBcmLevelName(bcmLevelBean.getBcmLevelName().trim());
		bcmLevel.setCategory(bcmLevelBean.getCategory() != null ? bcmLevelBean.getCategory().trim() : Constants.EMPTY_STRING);
		bcmLevel.setBcm(bcm);
		bcmLevel.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmLevel.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmLevel.setCreatedDate(new Date());
		bcmLevel.setUpdatedDate(new Date());
		bcmLevel.setDescription(bcmLevelBean.getDescription());
		if(parentLevelId == null){
			bcmLevel.setLevelNumber(1);
			BcmLevel topblByMaxSequenceNo = bcmLevelRepository.findTopByBcmAndLevelNumberOrderBySequenceNumberDesc(bcm,1);
			if(topblByMaxSequenceNo == null){
				bcmLevel.setSequenceNumber(1);
			}else{
				bcmLevel.setSequenceNumber(topblByMaxSequenceNo.getSequenceNumber()+1);
			}
		}else{
			BcmLevel parentBcmLevel = bcmLevelRepository.findOne(parentLevelId);
			bcmLevel.setLevelNumber(parentBcmLevel.getLevelNumber()+1);
			BcmLevel topblByMaxSequenceNo = bcmLevelRepository.findTopByBcmLevelOrderBySequenceNumberDesc(parentBcmLevel);
			if(topblByMaxSequenceNo == null){
				bcmLevel.setSequenceNumber(1);
			}else{
				bcmLevel.setSequenceNumber(topblByMaxSequenceNo.getSequenceNumber()+1);
			}
			bcmLevel.setBcmLevel(parentBcmLevel);
			updateParent(parentBcmLevel);
		}
		return bcmLevel;
	}
	
	/**
	 * update BCm level object
	 * @param bcmLevelBean
	 * @return {@code BcmLevel}
	 */
	private BcmLevel updateBcmlevelObject(BcmLevelBean bcmLevelBean) {
		BcmLevel bcmLevel = bcmLevelRepository.findOne(bcmLevelBean.getId());
		bcmLevel.setBcmLevelName(bcmLevelBean.getBcmLevelName().trim());
		if(bcmLevelBean.getCategory() != null)
			bcmLevel.setCategory(bcmLevelBean.getCategory().trim());
		bcmLevel.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmLevel.setUpdatedDate(new Date());
		bcmLevel.setDescription(bcmLevelBean.getDescription());
		bcmLevel.getBcm().setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		updateParent(bcmLevel);
		return bcmLevel;
	}
	
	@Override
	public void updateBcmSequence(BcmLevel bcmLevel, BcmLevel parentBcmLevel, Integer position) {
		//If new parent is same as old, update only the sequence number
		if(bcmLevel.getBcmLevel()!=null && bcmLevel.getBcmLevel().getId().equals(parentBcmLevel.getId())){			
			updateSequenceNumberWithinSameParent(bcmLevel, parentBcmLevel, position);
		}
		if(bcmLevel.getBcmLevel()!=null && !bcmLevel.getBcmLevel().getId().equals(parentBcmLevel.getId())){
			//update level number
			if(bcmLevel.getBcmLevel()!=null && bcmLevel.getBcmLevel().getLevelNumber() != parentBcmLevel.getLevelNumber()){
				updateLevelNumber(bcmLevel, parentBcmLevel);
			}
			//update sequence number
			updateSequenceNumberInOldParent(bcmLevel);
			updateSequenceNumberForParentChange(bcmLevel, parentBcmLevel, position);
			
			//update parent BCM level
			bcmLevel.setBcmLevel(parentBcmLevel);
			bcmLevelRepository.save(bcmLevel);
		}
		// if there is no parent for bcmLevel
		if(bcmLevel.getBcmLevel()==null){
			updateLevelNumber(bcmLevel, parentBcmLevel);
			updateSequenceNumberForParentChange(bcmLevel, parentBcmLevel, position);
			bcmLevel.setBcmLevel(parentBcmLevel);
			bcmLevelRepository.save(bcmLevel);
		}
	}

	/**
	 * Update sequence number of nodes in old parent. When a node is removed, all nodes with preceding sequence number should be moved one position back.
	 * @param bcmLevel
	 */
	private void updateSequenceNumberInOldParent(BcmLevel bcmLevel) {
		Integer oldPosition = bcmLevel.getSequenceNumber();
		
		List<BcmLevel> listOfChild = this.findByBcmLevel(bcmLevel.getBcmLevel());
		for (BcmLevel oneBcmLevel : listOfChild) {
			int childPosition = oneBcmLevel.getSequenceNumber();
			if(childPosition >= oldPosition){
				oneBcmLevel.setSequenceNumber(childPosition-1);
			}
		}
		updateParent(bcmLevel);
		bcmLevelRepository.save(listOfChild);
	}

	/**
	 * Update sequence number of BCM Level when its parent is changes.
	 * @param bcmLevel
	 * @param parentBcmLevel
	 * @param position
	 */
	private void updateSequenceNumberForParentChange(BcmLevel bcmLevel, BcmLevel parentBcmLevel, Integer position) {
		List<BcmLevel> listOfChild = this.findByBcmLevel(parentBcmLevel);
		for (BcmLevel oneBcmLevel : listOfChild) {
			int childPosition = oneBcmLevel.getSequenceNumber();
			if(childPosition >= position){
				oneBcmLevel.setSequenceNumber(childPosition+1);
			}
		}
		updateParent(parentBcmLevel);
		bcmLevelRepository.save(listOfChild);
		bcmLevel.setSequenceNumber(position);
		
	}

	/**
	 * Update level number of BCM level and its associated children
	 * @param bcmLevel
	 * @param parentBcmLevel
	 */
	private void updateLevelNumber(BcmLevel bcmLevel, BcmLevel parentBcmLevel) {
		bcmLevel.setLevelNumber(parentBcmLevel.getLevelNumber()+1);
		List<BcmLevel> listOfChild = this.findByBcmLevel(bcmLevel);
		if(!listOfChild.isEmpty()){
			for (BcmLevel oneChild : listOfChild) {
				updateLevelNumber(oneChild, bcmLevel);
			}
			bcmLevelRepository.save(listOfChild);
		}
	}

	/**
	 * Update sequence number of BCM level.
	 * @param bcmLevel
	 * @param parentBcmLevel
	 * @param position
	 */
	private void updateSequenceNumberWithinSameParent(BcmLevel bcmLevel, BcmLevel parentBcmLevel, Integer position) {
		List<BcmLevel> listOfChild = this.findByBcmLevel(parentBcmLevel);
		for (BcmLevel oneBcmLevel : listOfChild) {
			int childPosition = oneBcmLevel.getSequenceNumber();
			if(childPosition >= position && childPosition < bcmLevel.getSequenceNumber()){
				oneBcmLevel.setSequenceNumber(childPosition+1);
			}
			if (oneBcmLevel.getId().equals(bcmLevel.getId())){
				oneBcmLevel.setSequenceNumber(position);
			}
		}
		updateParent(bcmLevel);
		bcmLevelRepository.save(listOfChild);
	}
	
	public void updateParent(BcmLevel bcmLevel){	
			bcmLevel.setUpdatedDate(new Date());
			bcmLevel.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
			BcmLevel level = this.save(bcmLevel);
			//parentbcmLevel 
			if(level.getBcmLevel() != null && level.getBcmLevel().getId()!=null){
				updateParent(level.getBcmLevel());
			}
			level.setUpdatedDate(new Date());
			level.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
			level.getBcm().setUpdatedDate(new Date());
	}

	
}
