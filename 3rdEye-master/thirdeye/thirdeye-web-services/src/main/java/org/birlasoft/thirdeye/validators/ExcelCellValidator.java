/**
 * 
 */
package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author shaishav.dixit
 *
 */
public interface ExcelCellValidator {
	
	public boolean validate(Cell cell, List<String> errors);

}
