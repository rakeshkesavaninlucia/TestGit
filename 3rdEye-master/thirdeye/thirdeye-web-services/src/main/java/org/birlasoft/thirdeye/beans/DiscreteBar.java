package org.birlasoft.thirdeye.beans;

public class DiscreteBar {

	private String label;
	private Double value;
	
	public DiscreteBar(){}
	
	
	public DiscreteBar(String label, Double value) {
		super();
		this.label = label;
		this.value = value;
	}
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
	
	
}
