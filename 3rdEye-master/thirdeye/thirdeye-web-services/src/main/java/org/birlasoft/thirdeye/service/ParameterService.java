package org.birlasoft.thirdeye.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Service interface for parameter.
 * @author samar.gupta
 */
public interface ParameterService {
    /**
     * save the Parameter Object 
     * @param parameter
     * @return Parameter Object
     */
    public Parameter save(Parameter parameter);
    /**
     * Find all parameter.
     * @return {@code List<Parameter>}
     */
    public List<Parameter> findAll();
    /**
     * Find one parameter object by id.
     * @param id
     * @return {@code Parameter}
     */
    public Parameter findOne(Integer id);
    /**
     * Find fully loaded parameter object by id.
     * @param id
     * @return {@code Parameter}
     */
    public Parameter findFullyLoaded(Integer id);
    /**
     * Find list of parameter with loaded or not by questionnaire.
     * @param qe
     * @param loaded
     * @return {@code List<Parameter>}
     */
    public List<Parameter> findByQuestionnaire(Questionnaire qe, boolean loaded);
   
    /**
     * Create object with required fields.
     * @param parameter
     * @param questionnaireId
     * @param currentUser
     * @return {@code Parameter}
     */
    public Parameter createParameterObject(Parameter parameter, Integer questionnaireId, User currentUser);
    /** Get parameter bean list.
     * @param savedParam
     * @return {@code List<ParameterBean>}
     */
    public List<ParameterBean> getParameterBeanList(List<Parameter> savedParam);
    
    /**
     * Get {@code List} of {@link Parameter} by {@code set of Id's}
     * @param idsOfParameter
     * @return
     */
    public List<Parameter> findByIdIn(Set<Integer> idsOfParameter);
    
    /**
     * Get root parameter bean from questionnaire
     * @param questionnaire
     * @return
     */
    public List<ParameterBean> getRootParametersOfQuestionnaire(Questionnaire questionnaire);
    
	/**
	 * Get parameter by set of workspace or system level parameters
	 * @param workspaces
	 * @return
	 */
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces);
	
	/**
	 * Get parameter by workspace or system level parameters
	 * @param workspace
	 * @return
	 */
	public List<Parameter> findByWorkspaceIsNullOrWorkspace(Workspace workspace);
	
	/**
	 * Get system level parameters
	 * @return
	 */
	public List<Parameter> findByWorkspaceIsNull();
	
	/**
	 * Get {@code List} of {@link Parameter} for a {@link Workspace}
	 * @param workspace
	 * @return
	 */
	public List<Parameter> findByWorkspace(Workspace workspace);
	
	/**
	 * Generate the parameter tree. Pulls all the child parameters of a parameter
	 * @param oneParameter
	 * @return Map of all leaf and aggregate parameters
	 */
	public Map<ParameterType, List<ParameterBean>> getAllParametersInTree(Parameter oneParameter);
	
	/**
	 * Evaluate value of parameter
	 * @param idsOfParameters
	 * @param idOfQuestionnaire
	 * @param assetBeans
	 * @return
	 */
	public List<ParameterEvaluationResponse> evaluateParameters(Set<Integer> idsOfParameters, Integer idOfQuestionnaire, Set<AssetBean> assetBeans);
	
	/**
	 * Generate complete parameter tree. It should contain all child parameters and questions.
	 * @param rootLoadedParam
	 * @return
	 */
	public ParameterBean generateParameterTree (Parameter rootLoadedParam);
	
	/**
	 * Get {@code List} of root {@link Parameter} by {@link Questionnaire}
	 * @param questionnaire
	 * @param loaded
	 * @return
	 */
	public List<Parameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(Questionnaire questionnaire, boolean loaded);
	/**
	 * Get the {@code List} of root parameters on a questionnaire
	 * @author sunil1.gupta
	 * @param paramList
	 * @return
	 */
	public List<ParameterBean> getParametersOfQuestionnaire(List<Parameter> paramList);
	
	/**
	 * Get fully loaded {@code List} of {@link Parameter} by id's
	 * @param idsOfParameter
	 * @return
	 */
	public List<Parameter> findFullyLoadedParametersByIds(Set<Integer> idsOfParameter);
	/**
	 * Get {@code set} of {@link Parameter} by parent parameter
	 * @param parentParameter
	 * @return
	 */
	public Set<Parameter> findByParameter(Parameter parentParameter);
	
	/**
	 * Delete {@link Parameter} by id
	 * @param parameterId
	 */
	public void deleteParameter(Integer parameterId);
	
	/**
	 * Create a new variant parameter
	 * @param parameter
	 * @return
	 */
	public Parameter createVariantParameter(Parameter parameter);
	/**
	 * Get variant parameter and map of its child entity and weight
	 * @param listOfVariants
	 * @return
	 */
	public Map<Integer, Map<Integer, BigDecimal>> createVariantAndChildEntitiesMapping(Set<Parameter> listOfVariants);
	/**
	 * Get parameters on {@link Questionnaire}
	 * @return
	 */
	public Map<Integer, Integer> getParamsOnQuestionnaire();
	
	/**
	 * Get {@code set} of all variant parameters and child parameters
	 * @param listOfParameter
	 * @return
	 * 
	 * @author samar.gupta
	 */
	public Set<Integer> createExtendedParameters(Set<Parameter> listOfParameter);
	
	/**
	 * Fetch {@link Parameter} by id and update based on {@link ParameterBean}
	 * @param parameterBean
	 * @return
	 */
	public Parameter updateParameterObject(ParameterBean parameterBean);
	
	/**
	 * Create new {@link Parameter} entity from {@link ParameterBean}
	 * @param parameterBean
	 * @return
	 */
	public Parameter createNewParameter(ParameterBean parameterBean);
	
	// methods for parameter config entity
	/**
	 * Save {@link ParameterConfig}
	 * @param parameterConfig
	 * @return
	 */
	public ParameterConfig saveParameterConfig(ParameterConfig parameterConfig);
	
	/**
	 * Get {@code List} of {@link ParameterConfig} by {@link Parameter}
	 * @param parameter
	 * @return
	 */
	public List<ParameterConfig> findParameterConfigListByParameter(Parameter parameter);
	
	/**
	 * Delete {@link ParameterConfig}
	 * @param parameterConfig
	 */
	public void deleteParameterConfig(ParameterConfig parameterConfig);
	
	/**
	 * Delete bulk {@link ParameterConfig}
	 * @param parameterConfigs
	 */
	public void deleteParameterConfigs(List<ParameterConfig> parameterConfigs);
	
	/**
	 * Get {@link ParameterConfig} by parameter config id
	 * @param idOfParameterConfig
	 * @return
	 */
	public ParameterConfig findOneParameterConfig(Integer idOfParameterConfig);
	
	/**
	 * create new ParameterConfig object
	 * @param functionalMapLevelType
	 * @param savedParameter
	 * @param fcBean
	 * @return {@code ParameterConfig}
	 */
	public ParameterConfig createNewParameterConfigObject(String functionalMapLevelType, Parameter savedParameter, FunctionalCoverageParameterConfigBean fcBean);
	
	/**Get Quality gate for a parameter. 
	 * @param idOfParam
	 * @return
	 */
	public QualityGate getQualityGate(Integer idOfParam);
	
	/**
	 * Get parameter by workspace and type
	 * @param listOfWorkspaces
	 * @param type
	 * @return
	 */
	public Set<Parameter> findByWorkspaceInAndType(Set<Workspace> listOfWorkspaces, String type);
	
	/**
	 * Get parameter by workspace/system level and type
	 * @param activeWorkSpaceForUser
	 * @param string
	 * @return
	 */
	public List<Parameter> findByWorkspaceIsNullOrWorkspaceAndType(	Workspace activeWorkSpaceForUser, List<String> string);
	
	/**
	 * Get system level parameters by type
	 * @param string
	 * @return
	 */
	public List<Parameter> findByWorkspaceIsNullAndType(List<String> string);
	
	/**
	 * Delete quality gate associated to the parameter.
	 * @param parameter
	 */
	public void deleteParameterQualityGate(Parameter parameter);
	
	/**
	 * Generate {@link FunctionalCoverageParameterConfigBean} from {@link ParameterConfig} 
	 * @param parameterConfigs
	 * @return
	 */
	public FunctionalCoverageParameterConfigBean getFcConfigBeanFromParameteConfig(List<ParameterConfig> parameterConfigs);
	
	/**
	 * Extract the set of child questions and mandatory questions
	 * @param parameterFunctions
	 * @param map
	 * @param questions
	 * @param mandatoryQuestions
	 */
	public void extractSetOfQuestionsAndMandatoryQuestions(List<ParameterFunction> parameterFunctions, 
			Map<Integer, Double> map, Set<Question> questions, Set<Integer> mandatoryQuestions);
	
	/**
	 * Get {@link ParameterConfig} by {@link Parameter}
	 * @param oneParameter
	 * @return ParameterConfig
	 */
	public ParameterConfig findParameterConfigByParameter(Parameter oneParameter);
	
	public Set<Parameter> findParameterIdByClosedStatus(String Status);
	
	public List<QuestionnaireParameter> getQuestionnaireByParam(Parameter parameter);	
	
	public boolean getStatusOfParam(Parameter parameter);
	/**
	 * Evaluate value for tco parameter
	 * @param idsOfParameters
	 * @param idOfQuestionnaire
	 * @param assetBeans
	 * @return {@code List<ParameterEvaluationResponse>}
	 */
	public List<ParameterEvaluationResponse> evaluateTcoParameters(Integer idOfQuestionnaire, Set<AssetBean> assets);
	/**
	 * Fetch set of parameters by workspace is null or active workspace
	 * and parameter types
	 * @param workspaces
	 * @param types
	 * @return {@code Set<Parameter>}
	 */
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceInAndTypeIn(Set<Workspace> workspaces, List<String> types);
	/**
	 * Method to get Root cost Structure
	 * @param listOfWorkspaces
	 * @param type
	 * @return {@code Set<Parameter>}
	 */
	public Set<Parameter> getCostStructureByWorkspaceInAndType(Set<Workspace> listOfWorkspaces, String type);
	public List<QualityGate> findByQualityGateIn(List<QualityGate> qg);
	
}
