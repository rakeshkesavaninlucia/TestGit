package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.DataCoordinate;
import org.birlasoft.thirdeye.beans.DataCoordinate.GraphCoordinate;
import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionValueMapper;
import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.beans.ScatterSeries;
import org.birlasoft.thirdeye.beans.ScatterWrapper;
import org.birlasoft.thirdeye.colorscheme.service.Colorizer;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.ScatterGraphService;
import org.birlasoft.thirdeye.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation service class for plotting scatter graph.
 * @author samar.gupta
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ScatterGraphServiceImpl implements ScatterGraphService {

	private static Logger logger = LoggerFactory.getLogger(ScatterGraphServiceImpl.class);
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private AssetRepository assetRepository;
	@Autowired
	private QualityGateService qualityGateService;
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private Colorizer colorizerService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ResponseService responseService;
	@Autowired
	private BCMService  bcmService;
	@Autowired
	private BCMLevelService  bcmLevelService;
	@Autowired
	private BCMReportService  bcmReportService;
	@Autowired
	private AssetService  assetService;

	@Override
	public ScatterWrapper plotScatterGraph(String[] questionnaireIds,
			String[] parameterIds, Integer idOfQualityGate, Map<String, List<String>> filterMap,Integer[] idsOfLevel1,Integer[] idsOfLevel2,Integer[] idsOfLevel3,Integer[]idsOfLevel4){
		Set<Integer> parameterIdSet = extractSetOfParamIds(parameterIds);
		List<QuestionnaireParameter> listOfQP = populateQP(questionnaireIds, parameterIds);
		Set<AssetBean> setOfAssetBeans = new HashSet<>();
		List<AssetParameterWrapper> listOfAssetParameterWrapper = new ArrayList<>();
		for (QuestionnaireParameter questionnaireParameter : listOfQP) {
			Set<AssetBean> assetBeans = getQuestionnaireAssets(questionnaireParameter.getQuestionnaire());
			listOfAssetParameterWrapper.add(parameterSearchService.getParameterValueFromElasticsearch(questionnaireParameter.getParameterByParameterId().getId(), questionnaireParameter.getQuestionnaire().getId(), assetBeans, filterMap));
			setOfAssetBeans.addAll(assetBeans);
		}
		
		List<AssetBean> assetBeans = getAssetIdsBasedOnLevelIdsFromElasticSearch(idsOfLevel1, idsOfLevel2,  idsOfLevel3,idsOfLevel4);
		
		if(idsOfLevel1!=null || idsOfLevel2!=null || idsOfLevel3!=null || idsOfLevel4!=null){
	    	getFilteredAssetIds(assetBeans, listOfAssetParameterWrapper);
		}	
		// Find out which is the X, Y and Z axis 
		EnumMap<GraphCoordinate,AssetParameterWrapper> parametersEvaluated = new EnumMap<>(GraphCoordinate.class);
		// find out which one of the parameter is matching
		sortOutResponsePerAxis(parameterIdSet, listOfAssetParameterWrapper, parametersEvaluated);

		// Find out common assets
		Set<AssetParameterBean> assetForDisplay = isolateCommonAssets(parametersEvaluated);

		// This is the final map being prepared. Every asset will have a data coordinate
		Map<AssetParameterBean, DataCoordinate> coordinateMap = createCoordinateMapForAssets(parametersEvaluated, assetForDisplay);

		ScatterWrapper wrapper = new ScatterWrapper();
        if(assetBeans.isEmpty() && (idsOfLevel1!=null || idsOfLevel2!=null || idsOfLevel3!=null || idsOfLevel4!=null)){
        	wrapper.setmessage("No Functional Mapping done for this BCM Level");
        }
		if (parametersEvaluated.containsKey(GraphCoordinate.X)){
			wrapper.setxAxisLabel(parametersEvaluated.get(GraphCoordinate.X).getParameter());
		}
		if (parametersEvaluated.containsKey(GraphCoordinate.Y)){
			wrapper.setyAxisLabel(parametersEvaluated.get(GraphCoordinate.Y).getParameter());
		}
		if (parametersEvaluated.containsKey(GraphCoordinate.Z)){
			wrapper.setzAxisLabel(parametersEvaluated.get(GraphCoordinate.Z).getParameter());
		}

		// sort series based on asset health
		QualityGate qualityGate = null;
		Map<Integer, String> assetIdColor = new HashMap<>(); 
		if(idOfQualityGate != null){
			qualityGate = qualityGateService.findOne(idOfQualityGate);
			assetIdColor = getAssetColorFromQualityGate(qualityGate, setOfAssetBeans);
		}
		createDataSeriesBasedOnAssetHealth(coordinateMap, wrapper, qualityGate, assetIdColor);
		return wrapper;
	}

	/**Method to get the Asset ids From Elastic Search Based on the Levelids
	 * @param idsOfLevel1
	 * @param idsOfLevel2
	 * @param idsOfLevel3
	 * @param idsOfLevel4
	 * return List Of AssetBean 
	 */
	private List<AssetBean> getAssetIdsBasedOnLevelIdsFromElasticSearch(Integer[] idsOfLevel1, Integer[] idsOfLevel2,
			Integer[] idsOfLevel3,Integer[] idsOfLevel4) {
		List<AssetBean> assetBeans = new ArrayList<>();
		if(idsOfLevel1!=null && idsOfLevel2==null && idsOfLevel3 == null){
			List<Integer> listOfAssestIds = bcmReportService.getListOfAssetBasedOnLevelIdsFromElasticSearch(idsOfLevel1);
			//get list of Asset from list of asset IDs.
			List<Asset> listOfAssetsfromAssetIds = assetService.findListOfAssetsByIds(listOfAssestIds);
			 assetBeans = assetService.getListOfAssetBean(listOfAssetsfromAssetIds);
			
		}else if(idsOfLevel2!=null && idsOfLevel3 == null){
			List<Integer> listOfAssestIds = bcmReportService.getListOfAssetBasedOnLevelIdsFromElasticSearch(idsOfLevel2);
			List<Asset> listOfAssetsfromAssetIds = assetService.findListOfAssetsByIds(listOfAssestIds);
			 assetBeans = assetService.getListOfAssetBean(listOfAssetsfromAssetIds);
			
		}else if(idsOfLevel3!=null && idsOfLevel4==null ){
			List<Integer> listOfAssestIds = bcmReportService.getListOfAssetBasedOnLevelIdsFromElasticSearch(idsOfLevel3);
			List<Asset> listOfAssetsfromAssetIds = assetService.findListOfAssetsByIds(listOfAssestIds);
			 assetBeans = assetService.getListOfAssetBean(listOfAssetsfromAssetIds);
		}
		else if(idsOfLevel4!=null){
			List<Integer> listOfAssestIds = bcmReportService.getListOfAssetBasedOnLevelIdsFromElasticSearch(idsOfLevel4);
			List<Asset> listOfAssetsfromAssetIds = assetService.findListOfAssetsByIds(listOfAssestIds);
			 assetBeans = assetService.getListOfAssetBean(listOfAssetsfromAssetIds);	
		}
		return assetBeans;
	}

	/** Method to Get The Filtered AssetIDs 
	 * @param assetBeans
	 * @param listOfAssetParameterWrapper
	 * Set Values In Asset Parameter Wrapper On the Basis Of Comparison Of Bcm Level AssetMapping And QP AssetMapping.
	 */ 
	private void getFilteredAssetIds(List<AssetBean> assetBeans, List<AssetParameterWrapper> listOfAssetParameterWrapper){
	
		for (AssetParameterWrapper assetParameterWrapper : listOfAssetParameterWrapper) {
			List<AssetParameterBean> assetParameterBeans = assetParameterWrapper.getValues().stream()
																.filter(e -> isAssetPresentInBcm(e, assetBeans))
																.collect(Collectors.toList());
			assetParameterWrapper.setValues(assetParameterBeans);
		}
	}
	
	/** Method to check the assets present in Bcm 
	 * @param assetParameterBean
	 * @param assetBeans
	 * @return boolean-> true in Case bcmAssetids match with QPAssetids And False if Failed
	 */
	private boolean isAssetPresentInBcm(AssetParameterBean assetParameterBean, List<AssetBean> assetBeans){
		for (AssetBean assetBean : assetBeans) {
			if(assetBean.getId().equals(assetParameterBean.getAssetId())){
				return true;
			}
		}		
		return false;
	}
	
	
	private void createDataSeriesBasedOnAssetHealth(Map<AssetParameterBean, DataCoordinate> coordinateMap, ScatterWrapper wrapper,
			QualityGate qualityGate, Map<Integer, String> assetIdColorMap) {
		Map<String,ScatterSeries> seriesMap = new HashMap<>();

		for(Map.Entry<AssetParameterBean, DataCoordinate> oneCoordinate : coordinateMap.entrySet()){
			String assetColor;
			if(qualityGate != null && null != assetIdColorMap.get(oneCoordinate.getKey().getAssetId())){
				assetColor = assetIdColorMap.get(oneCoordinate.getKey().getAssetId());
			}else {
				assetColor = Constants.DEFAULT_COLOR;
			}
			if (seriesMap.containsKey(assetColor)){
				seriesMap.get(assetColor).addDataPoint(oneCoordinate.getValue());
			} else {
				ScatterSeries newSeries = new ScatterSeries();
				if(!"#99ddff".equals(assetColor)){
				  newSeries.setSeriesName(getQualityGateColor(qualityGate, assetColor));
				} else {
				  newSeries.setSeriesName(Constants.DEFAULT_COLOR_DESCRIPTION);
				}
				newSeries.setSeriesColor(assetColor);
				newSeries.addDataPoint(oneCoordinate.getValue());
				seriesMap.put(assetColor, newSeries);
			}
		}
		for (ScatterSeries oneDataSeries: seriesMap.values()){
			wrapper.addSeries(oneDataSeries);
		}		
	}

	private String getQualityGateColor(QualityGate qualityGate,
			String assetColor) {
		String description = assetColor;
        QualityGateBean qb = qualityGateService.createQGBean(qualityGate);
        for(JSONQualityGateDescriptionValueMapper oneDescription: qb.getDescription()){
            if(oneDescription.getColor().equalsIgnoreCase(assetColor)){
            	description = oneDescription.getDescription();
            	break;
            }
        }
		return description;
	}

	private Map<Integer, String> getAssetColorFromQualityGate(QualityGate qualityGate,Set<AssetBean> setOfAssetBeans) {
		Map<Integer, String> mapOfAssetBeans = new HashMap<>();
		List<AssetBean> qualityGateColor = new ArrayList<>();
		try {
			qualityGateColor = (List<AssetBean>) colorizerService.getQualityGateColor(qualityGate, new ArrayList<>(setOfAssetBeans));
		} catch (Exception e) {
			logger.error(" Insufficient Data Exception " + e);
		}

		List<AssetBean> listOfAssetBeans = qualityGateColor;
		for (AssetBean assetBean : listOfAssetBeans) {
			mapOfAssetBeans.put(assetBean.getId(), assetBean.getHexColor());		  
		}
		return mapOfAssetBeans;
	}

	private Set<AssetBean> getQuestionnaireAssets(Questionnaire questionnaire) {
		Set<AssetBean> assets = new HashSet<>();
		Set<QuestionnaireAsset> qa = questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire);
		Set<Integer> assetIdsToLoad = new HashSet<>();
		for (QuestionnaireAsset oneQuestionnaireAsset : qa) {
			assetIdsToLoad.add(oneQuestionnaireAsset.getAsset().getId());
		}

		Set<Asset> setOfAssets = assetRepository.findMany(assetIdsToLoad);

		for (Asset oneAsset : setOfAssets) {
			assets.add(new AssetBean(oneAsset));
		}
		return assets;
	}

	private List<QuestionnaireParameter> populateQP(String[] questionnaireIds, String[] parameterIds) {
		List<QuestionnaireParameter> listOfQP = new ArrayList<>();
		for (int i = 0; i<questionnaireIds.length; i++) {
			if("-1".equals(questionnaireIds[i])){
				continue;
			}
			listOfQP.addAll(questionnaireParameterService.findByQuestionnaireAndParameter(questionnaireService.findOne(Integer.valueOf(questionnaireIds[i])), parameterService.findOne(Integer.valueOf(parameterIds[i]))));
		}
		return listOfQP;
	}

	private Map<AssetParameterBean, DataCoordinate> createCoordinateMapForAssets(
			Map<GraphCoordinate, AssetParameterWrapper> parametersEvaluated,
			Set<AssetParameterBean> assetForDisplay) {
		Map<AssetParameterBean, DataCoordinate> coordinateMap = new HashMap<>();
		for (AssetParameterBean ab : assetForDisplay){
			DataCoordinate dc = new DataCoordinate();
			dc.setLabel(ab.getAssetName()); 
			if (parametersEvaluated.containsKey(GraphCoordinate.X)){
				dc.setCoordinate(GraphCoordinate.X, parametersEvaluated.get(GraphCoordinate.X).getValues().stream().filter(x-> ab.getAssetId().equals(x.getAssetId())).findFirst().get().getParameterValue().doubleValue());
			}
			if (parametersEvaluated.containsKey(GraphCoordinate.Y)){
				dc.setCoordinate(GraphCoordinate.Y, parametersEvaluated.get(GraphCoordinate.Y).getValues().stream().filter(y-> ab.getAssetId().equals(y.getAssetId())).findFirst().get().getParameterValue().doubleValue());
			}
			if (parametersEvaluated.containsKey(GraphCoordinate.Z)){
				dc.setCoordinate(GraphCoordinate.Z, parametersEvaluated.get(GraphCoordinate.Z).getValues().stream().filter(z-> ab.getAssetId().equals(z.getAssetId())).findFirst().get().getParameterValue().doubleValue());
			}

			coordinateMap.put(ab, dc);
		}
		return coordinateMap;
	}


	private Set<AssetParameterBean> isolateCommonAssets(	Map<GraphCoordinate, AssetParameterWrapper> parametersEvaluated) {
		EnumMap<GraphCoordinate,Set<AssetParameterBean>> listOfAssetsToShow = new EnumMap<>(GraphCoordinate.class);
		populateAssetMap(parametersEvaluated, GraphCoordinate.X, listOfAssetsToShow);
		populateAssetMap(parametersEvaluated, GraphCoordinate.Y, listOfAssetsToShow);
		populateAssetMap(parametersEvaluated, GraphCoordinate.Z, listOfAssetsToShow);

		return Utility.findCommonValues(listOfAssetsToShow);		
	}


	private void sortOutResponsePerAxis(
			Set<Integer> parameterIdSet,
			List<AssetParameterWrapper> responses,
			EnumMap<GraphCoordinate, AssetParameterWrapper> parametersEvaluated) {
		for (AssetParameterWrapper oneParameterResponse : responses){

			int i = 0;
			for (Integer oneId : parameterIdSet){
				if (oneParameterResponse.getParameterId().equals(oneId)){
					switch (i) {
					case 0:
						parametersEvaluated.put(GraphCoordinate.X, oneParameterResponse);
						break;
					case 1:
						parametersEvaluated.put(GraphCoordinate.Y, oneParameterResponse);
						break;
					case 2:
						parametersEvaluated.put(GraphCoordinate.Z, oneParameterResponse);
						break;
					default : 
						break;
					}

				}
				i++;
			}
		}
	}
	

	private Set<Integer> extractSetOfParamIds(String[] parameterIds) {
		Set<Integer> parameterIdSet = new LinkedHashSet<>();
		for (String oneParamID : parameterIds){
			try{
				parameterIdSet.add(Integer.parseInt(oneParamID));
			} catch (NumberFormatException nfe){
				logger.error("NumberFormatException in populateParamEvalConfig() :: " + nfe); 
			}
		}
		return parameterIdSet;
	}

	private void populateAssetMap(Map<GraphCoordinate, AssetParameterWrapper> parametersEvaluated, GraphCoordinate gc,
			EnumMap<GraphCoordinate, Set<AssetParameterBean>> listOfAssetsToShow) {
		if (parametersEvaluated.containsKey(gc)){
			listOfAssetsToShow.put(gc, new HashSet<AssetParameterBean>(parametersEvaluated.get(gc).getValues()));
		}
	}

	@Override
	public List<Questionnaire> getQuestionnaireforScatter() {
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();

		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		List<Questionnaire> questionnairesInWorkspaces = questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString());
	   
		// TODO We can make better using one query to fetch qes which do not have responses.
		List<Questionnaire> questionnairesToDisplay = new ArrayList<>();
		if(!questionnairesInWorkspaces.isEmpty()){
			List<Response> responses = responseService.findByQuestionnaireIn(questionnairesInWorkspaces);
			for (Response response : responses) {
				questionnairesToDisplay.add(response.getQuestionnaire());
			}
		}
		return questionnairesToDisplay;
	}
	@Override
	public List<BcmLevel> getBcmLevelsforScatter() {
		     Set<Workspace> listOfWorkspaces = new HashSet<> ();
		    listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());	
		    List<Bcm> listOfActiveBcm = bcmService.findByWorkspaceInAndStatus(listOfWorkspaces, true);
			List<BcmLevel> listOFBcmLevel = bcmLevelService.findByBcmAndLevelNumberIn(listOfActiveBcm, 1);
	    
			return listOFBcmLevel;
		
	}
	

}
