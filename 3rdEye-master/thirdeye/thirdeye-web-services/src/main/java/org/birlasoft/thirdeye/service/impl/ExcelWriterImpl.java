package org.birlasoft.thirdeye.service.impl;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.service.ExcelWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} to write excel file and download it on browser
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ExcelWriterImpl implements ExcelWriter {
	
	private static Logger logger = LoggerFactory.getLogger(ExcelWriterImpl.class);

	@Override
	public void writeExcel(HttpServletResponse response, XSSFWorkbook workbook, String fileName) {
		
		if (workbook != null){
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "inline;filename=" + fileName + ".xlsx");
			
			// get output stream of the response
			try {
				OutputStream outStream = response.getOutputStream();
				workbook.write(outStream);
				outStream.close();
				workbook.close();
			} catch (IOException e) {
				logger.error("Error while writing excel", e);
			}
		}
		
	}

}
