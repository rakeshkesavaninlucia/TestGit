package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.search.api.beans.FacetBean;
import org.birlasoft.thirdeye.search.api.beans.FilterBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service implementation class for Filter Service
 * @author dhruv.sood
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class FilterServiceImpl implements FilterService{
	
	private static Logger logger = LoggerFactory.getLogger(FilterServiceImpl.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
		
	private static final String INITIATING_REST_CALL="Initiating a Rest Service call";

	
	
	@Override
	public List<FacetBean> getFacets(String tenantId, Integer activeWorkspaceId, Map<String,List<String>> filterMap){

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/filter/facet";

		RestTemplate restTemplate = new RestTemplate();
		
		List<FacetBean> listOfFacetBean = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		List<FacetBean> list = new ArrayList<>();

		//Setting the search config object  
		SearchConfig searchConfig = setSearchConfig(tenantId, activeWorkspaceId);
		searchConfig.setFilterMap(filterMap);

		logger.info(INITIATING_REST_CALL);
		
		//Firing a rest request to elastic search server
		try {
			listOfFacetBean = (List<FacetBean>) restTemplate.postForObject(uri,searchConfig,List.class);
			list = mapper.convertValue(listOfFacetBean, new TypeReference<List<FacetBean>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in FilterServiceImpl :: getFacets() :"+e);
		}
		
		int i=1;
		for (FacetBean facetBean : list) {
			facetBean.setFacetNumber("facet_"+i);
			i++;
		}
				
		return checkedStatus(filterMap, list);
	}

	

	/**@author dhruv.sood
	 * Checking which all filters are selected
	 * @param filterMap
	 * @param listOfFacetBean
	 * @return
	 */
	private List<FacetBean> checkedStatus(Map<String, List<String>> filterMap, List<FacetBean> listOfFacetBean) {
		for (FacetBean onefacetBean : listOfFacetBean) {			
			for(Map.Entry<String,List<String>> oneEntry: filterMap.entrySet()){				
				for(FilterBean oneFilterBean:onefacetBean.getListOfFilterBean()){
					if(oneEntry.getValue().contains(oneFilterBean.getName())){
						oneFilterBean.setChecked(true);
					}					
				}							
			}						
		}
		return listOfFacetBean;
	}

	/**@author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId	
	 * @return
	 */
	private SearchConfig setSearchConfig(String tenantId, Integer activeWorkspaceId) {
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		return searchConfig;
	}

}
