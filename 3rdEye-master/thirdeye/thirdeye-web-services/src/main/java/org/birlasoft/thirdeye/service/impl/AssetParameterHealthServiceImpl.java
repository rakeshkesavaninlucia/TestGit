package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartRequestParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartResponseParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetParameterHealthBean;
import org.birlasoft.thirdeye.beans.parameter.ParamDrillDownBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;
import org.birlasoft.thirdeye.service.AssetParameterHealthService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} to handle spider charts
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetParameterHealthServiceImpl implements AssetParameterHealthService {
	
	@Autowired
	private QuestionnaireAssetService qaService;
	@Autowired
	private QuestionnaireParameterService qpService;
	@Autowired
	private ParameterService pService;
	@Autowired
	private QuestionnaireService qService;
	@Autowired
	private ParameterFunctionService pfService;
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private QuestionService questionService;
	
	@Override
	public List<AssetParameterHealthBean> getAssetViewHealthParameter(Asset asset) {
		
		List<AssetParameterHealthBean> assetQParamBeanList = new ArrayList<>();
		//get List of QusetionnaireAsset by assetId from Qe_A table 
		List<QuestionnaireAsset> questionnaireAssetList = qaService.findByAsset(asset);
		List<Questionnaire> questionnaireList = new ArrayList<>();
		//prepare list Questionnaire  from QA list
		for(QuestionnaireAsset qa : questionnaireAssetList){
			if(qa.getQuestionnaire().getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.DEF.toString()))
			{
				questionnaireList.add(qa.getQuestionnaire());
			}
		}
		//get list of QuestionnaireParameter from Qe_P table where parent parameter id is null and given list of QuestionnaireParameter 
		List<QuestionnaireParameter> qpList = qpService.findByQuestionnaireInAndParameterByParentParameterIdIsNull(questionnaireList);
		//prepare the list of asset parameter health bean for asset
		for(QuestionnaireParameter oneQP : qpList ){			
			AssetParameterHealthBean aphBean = new AssetParameterHealthBean();
			aphBean.setAssetBean(new AssetBean(asset));
			aphBean.setParameterBean(new ParameterBean(oneQP.getParameterByParameterId()));
			aphBean.setQuestionnaire(oneQP.getQuestionnaire());
			//evaluate the root parameter value
			aphBean.setParameterValue(evaluateParamValue(oneQP,asset));
			assetQParamBeanList.add(aphBean);
		}		
		return assetQParamBeanList;
	}
	/**
	 * method to get evaluate Parameter Value
	 * @author sunil1.gupta
	 * @param oneQP
	 * @param asset
	 * @return evaluateParamValue
	 */
	
	private BigDecimal evaluateParamValue(QuestionnaireParameter oneQP,Asset asset) {
		Integer idOfQuestionnaire = oneQP.getQuestionnaire().getId();
		Set<AssetBean> assetBeans = new HashSet<>();
		assetBeans.add(new AssetBean(asset));
		AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(oneQP.getParameterByParameterId().getId(), idOfQuestionnaire, assetBeans, null);
		return assetParameterWrapper.fetchParamValueForAsset(asset.getId());
	}
	
	@Override
	public List<List<AssetChartResponseParamBean>> getAssetParamHealthData(AssetChartRequestParamBean chartRequestParamBean) {
		
		ParameterBean parameterBean =  prepareParameterBean(pService.findOne(chartRequestParamBean.getParameterId()));
		List<List<AssetChartResponseParamBean>> listOfResponseData = new ArrayList<>();
		 if (parameterBean.getParamType().equalsIgnoreCase(ParameterType.AP.toString())){
			 listOfResponseData = evaluateAssetParamHealthData(chartRequestParamBean);	 
		 }else if(parameterBean.getParamType().equalsIgnoreCase(ParameterType.LP.toString())){
			 listOfResponseData = evaluateAssetParamQuestionHealthData(chartRequestParamBean);
		 }		 
		 return listOfResponseData;
	}
	
	/**
	 * method to evaluate AssetParamQuestion Health Data
	 * @param chartRequestParamBean
	 * @return list of response data
	 */
	private List<List<AssetChartResponseParamBean>> evaluateAssetParamQuestionHealthData(AssetChartRequestParamBean chartRequestParamBean) {
		 
		 List<List<AssetChartResponseParamBean>> responseChartdata = new ArrayList<>();
	     
		 Map<Integer, QuestionResponseSearchWrapper> questionEvaluationResponseList = prepareQuestionEvaluationResponses(chartRequestParamBean);
		 //prepare response for Average portfolio Question
		 List<AssetChartResponseParamBean> avgPortfolioHealthData = prepareRespForAvgAssetQuestion(questionEvaluationResponseList);
		 //prepare response for Asset Question
		 List<AssetChartResponseParamBean> assetHealthResponseData = prepareResposeForAssetQuestion(chartRequestParamBean,questionEvaluationResponseList);	 
		
		 responseChartdata.add(assetHealthResponseData);//show Yellow	
		 responseChartdata.add(avgPortfolioHealthData);	//show Blue
	     return responseChartdata;
	}
	
	/**
	 * method to Evaluation Responses for question 
	 * @param chartRequestParamBean
	 * @return
	 */
	private Map<Integer, QuestionResponseSearchWrapper> prepareQuestionEvaluationResponses(
			AssetChartRequestParamBean chartRequestParamBean) {
		Map<Integer, QuestionResponseSearchWrapper> mapOfQuestionAndValue = new HashMap<>();
		Questionnaire questionnaire = qService.findOne(chartRequestParamBean.getQuestionnaireId());
		List<QuestionnaireParameter> qp = qpService.findByQuestionnaireAndParameter(questionnaire, pService.findOne(chartRequestParamBean.getParameterId()));

		// get list of QuestionnaireAsset based on Questionnaire
		List<QuestionnaireAsset> questionnaireAssetList = qaService.findByQuestionnaire(questionnaire);

		Set<Integer> assetIds = questionnaireAssetList.stream().map(qa -> qa.getAsset().getId()).collect(Collectors.toSet());
		for (QuestionnaireParameter oneQp : qp) {
			mapOfQuestionAndValue = questionService.getQuestionValueFromElasticsearch(questionnaire.getId(), oneQp, assetIds);
		}

		return mapOfQuestionAndValue;
	}
	
	/**
	 * method to prepare Respose For AssetQuestion
	 * @param chartRequestParamBean
	 * @param questionEvaluationResponseList
	 * @return
	 */
	private List<AssetChartResponseParamBean> prepareResposeForAssetQuestion(AssetChartRequestParamBean chartRequestParamBean,
			Map<Integer, QuestionResponseSearchWrapper> questionEvaluationResponseList) {
		List<AssetChartResponseParamBean> assetHealthResponseData = new ArrayList<>();
		for (Entry<Integer, QuestionResponseSearchWrapper> entry : questionEvaluationResponseList.entrySet()) {
			QuestionResponseSearchWrapper oneResponse = entry.getValue();
			AssetChartResponseParamBean aseetChartResponseParamBean = new AssetChartResponseParamBean();
			aseetChartResponseParamBean.setAxis(oneResponse.getDisplayName());
			aseetChartResponseParamBean.setValue(oneResponse.fetchQuestionValueForAsset(chartRequestParamBean.getAssetId()));
			assetHealthResponseData.add(aseetChartResponseParamBean);
		}
		return assetHealthResponseData;
	}
	
	/**
	 * method to prepare Response For Avg AssetQuestion
	 * @param questionEvaluationResponseList
	 * @return
	 */
	private List<AssetChartResponseParamBean> prepareRespForAvgAssetQuestion(
			Map<Integer, QuestionResponseSearchWrapper> questionEvaluationResponseList) {

		List<AssetChartResponseParamBean> avgPortfolioHealthData = new ArrayList<>();
		for (Entry<Integer, QuestionResponseSearchWrapper> entry : questionEvaluationResponseList.entrySet()) {
			QuestionResponseSearchWrapper oneResponse = entry.getValue();
			AssetChartResponseParamBean aseetChartResponseParamBean = new AssetChartResponseParamBean();
			aseetChartResponseParamBean.setAxis(oneResponse.getDisplayName());
			BigDecimal eValue = new BigDecimal(0);
			for (Integer oneAsset : oneResponse.getAssetResponse().keySet()) {
				eValue = eValue.add(oneResponse.fetchQuestionValueForAsset(oneAsset));
			}

			eValue = eValue.divide(new BigDecimal(oneResponse.getAssetResponse().size()), 2, RoundingMode.HALF_UP);

			aseetChartResponseParamBean.setValue(eValue);
			avgPortfolioHealthData.add(aseetChartResponseParamBean);
		}
		return avgPortfolioHealthData;
	}
	/**
	 * method to evaluate AssetParam Health Data
	 * @param chartRequestParamBean
	 * @return responseChartdata
	 */
	private List<List<AssetChartResponseParamBean>> evaluateAssetParamHealthData(AssetChartRequestParamBean chartRequestParamBean) {
		 
		 List<List<AssetChartResponseParamBean>> responseChartdata = new ArrayList<>();
		 // evaluate asset parameter value
		 List<AssetParameterWrapper> listOfAssetParameterWrapper = evaluteAssetParamValue(chartRequestParamBean);
		 //prepare list of bean for average portfolio 
		 List<AssetChartResponseParamBean> avgPortfolioHealthData = prepareRespForAvgPortFolio(listOfAssetParameterWrapper);
		 //prepare list of bean for asset 
		 List<AssetChartResponseParamBean> assetHealthResponseData = prepareResposeForAsset(chartRequestParamBean,listOfAssetParameterWrapper);	 
		 responseChartdata.add(assetHealthResponseData);// Yellow	
		 responseChartdata.add(avgPortfolioHealthData);	// Blue
	     return responseChartdata;
	}
	
	/**
	 * method to prepare response for Average portfolio health
	 * @param listOfAssetParameterWrapper
	 * @return avgPortfolioHealthData
	 */
	private List<AssetChartResponseParamBean> prepareRespForAvgPortFolio(List<AssetParameterWrapper> listOfAssetParameterWrapper) {
		List<AssetChartResponseParamBean> avgPortfolioHealthData =  new ArrayList<>();
		for(AssetParameterWrapper oneWrapper : listOfAssetParameterWrapper){
			 AssetChartResponseParamBean aseetChartResponseParamBean = new AssetChartResponseParamBean();			 
			 aseetChartResponseParamBean.setAxis(oneWrapper.getParameter());
	    	 BigDecimal eValue = new BigDecimal(0);
	    	 for(AssetParameterBean oneAsset : oneWrapper.getValues()){
	    		 eValue = eValue.add(oneAsset.getParameterValue());
	    	 }	
	    	 
			if (eValue.compareTo(BigDecimal.ZERO) != 0) {
				eValue = eValue.divide(new BigDecimal(oneWrapper.getValues().size()), 2, RoundingMode.HALF_UP);
				aseetChartResponseParamBean.setValue(eValue);
				avgPortfolioHealthData.add(aseetChartResponseParamBean);
			}
	     }
		return avgPortfolioHealthData;
	}
	
	/**
	 * method to prepare Response for asset
	 * @param chartRequestParamBean
	 * @param listOfAssetParameterWrapper
	 * @return
	 */
	private List<AssetChartResponseParamBean> prepareResposeForAsset(AssetChartRequestParamBean chartRequestParamBean,List<AssetParameterWrapper> listOfAssetParameterWrapper) {
		
		List<AssetChartResponseParamBean> assetHealthResponseData =  new ArrayList<>();
		for(AssetParameterWrapper oneWrapper : listOfAssetParameterWrapper){
			 AssetChartResponseParamBean aseetChartResponseParamBean = new AssetChartResponseParamBean();			 
			 aseetChartResponseParamBean.setAxis(oneWrapper.getParameter());
			 aseetChartResponseParamBean.setValue(oneWrapper.fetchParamValueForAsset(chartRequestParamBean.getAssetId()));
			 
	    	 assetHealthResponseData.add(aseetChartResponseParamBean);
	      }
		return assetHealthResponseData;
	}
	
	/**
	 * method to evalute asset parameter value
	 * @param chartRequestParamBean
	 * @return list of Parameter Evaluation Response
	 */
	private List<AssetParameterWrapper> evaluteAssetParamValue(AssetChartRequestParamBean chartRequestParamBean) {
		Set<AssetBean> assetBeans = new HashSet<>();
		// get list of QuestionnaireAsset from Questionnaire_Asset table by QuestionnaireId
		List<QuestionnaireAsset> questionnaireAssetList = qaService.findByQuestionnaire(qService.findOne(chartRequestParamBean.getQuestionnaireId()));
		// prepare list of asset bean
		for (QuestionnaireAsset oneQA : questionnaireAssetList) {
			assetBeans.add(new AssetBean(oneQA.getAsset()));
		}
		Set<Integer> idsOfParameters = new HashSet<>();
		// get list of parameter function by parent parameter id
		List<ParameterFunction> parameterFunctionList = pfService.findByParameterByParentParameterId(pService.findOne(chartRequestParamBean.getParameterId()));
		// prepare list of parameter
		for (ParameterFunction onepf : parameterFunctionList) {
			idsOfParameters.add(onepf.getParameterByChildparameterId().getId());
		}

		List<AssetParameterWrapper> assetParameterWrappers = new ArrayList<>();
		for (Integer oneParam : idsOfParameters) {
			AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(oneParam, chartRequestParamBean.getQuestionnaireId(), assetBeans, null);
			assetParameterWrappers.add(assetParameterWrapper);
		}
		return assetParameterWrappers;
	}	
	
	private ParameterBean prepareParameterBean(Parameter parameter) {
		ParameterBean parameterBean = new ParameterBean(parameter);
		for (ParameterFunction pf : parameter.getParameterFunctionsForParentParameterId()){
			if(pf.getParameterByChildparameterId() != null){
				parameterBean.addChildParameter(new ParameterBean(pf.getParameterByChildparameterId()));
				parameterBean.addParameterFunction(pf.getParameterByChildparameterId().getId(), pf);
			}
			if(pf.getQuestion() != null){
				parameterBean.addChildQuestion(new QuestionBean(pf.getQuestion()));
				parameterBean.addParameterFunction(pf.getQuestion().getId(), pf);
			}
		}
		return parameterBean;
	}
	
	@Override
	public List<ParamDrillDownBean> getAssetParamDrillDownData(AssetChartRequestParamBean chartRequestParamBean) {
		
		ParameterBean parameterBean =  prepareParameterBean(pService.findOne(chartRequestParamBean.getParameterId()));
		List<ParamDrillDownBean> listOfDrillDownResponseData = new ArrayList<>();
		 if (parameterBean.getParamType().equalsIgnoreCase(ParameterType.AP.toString())){
			 listOfDrillDownResponseData = evaluateAssetParamDrillDownData(chartRequestParamBean);	 
		 }else if(parameterBean.getParamType().equalsIgnoreCase(ParameterType.LP.toString())){
			 listOfDrillDownResponseData = evaluateAssetParamQuestionDrillDownData(chartRequestParamBean);
		 }		 
		 return listOfDrillDownResponseData;
	}
	
	private List<ParamDrillDownBean> evaluateAssetParamDrillDownData(AssetChartRequestParamBean chartRequestParamBean) {
		
		 List<AssetParameterWrapper> listOfAssetParameterWrapper = evaluteAssetParamValue(chartRequestParamBean);
		 return prepareResponseForDrillDownParam(chartRequestParamBean,listOfAssetParameterWrapper);                                                   
	}	
	
	private List<ParamDrillDownBean> prepareResponseForDrillDownParam(AssetChartRequestParamBean chartRequestParamBean,List<AssetParameterWrapper> listOfAssetParameterWrapper) {
	
		 List<ParamDrillDownBean> paramResponseDataList = new ArrayList<>();
		 for(AssetParameterWrapper oneWrapper : listOfAssetParameterWrapper){
			 ParamDrillDownBean drillDownBean = new ParamDrillDownBean();
			 drillDownBean.setParameterId(oneWrapper.getParameterId());
			 drillDownBean.setTitle(oneWrapper.getParameter());
			 drillDownBean.setRanges(Arrays.asList(3,6,10));
			 
			 drillDownBean.setMeasures(Arrays.asList(oneWrapper.fetchParamValueForAsset(chartRequestParamBean.getAssetId())));
			
			 BigDecimal eValue = new BigDecimal(0);
	    	 for(AssetParameterBean oneAsset : oneWrapper.getValues()){
	    		 eValue = eValue.add(oneAsset.getParameterValue());
	    	 }	
	    	 eValue = eValue.divide(new BigDecimal(oneWrapper.getValues().size()),2,RoundingMode.HALF_UP);
	    	 drillDownBean.setMarkers(Arrays.asList(eValue));	  
	    	 paramResponseDataList.add(drillDownBean);
		 }
		return paramResponseDataList;
	}
	
	private List<ParamDrillDownBean> evaluateAssetParamQuestionDrillDownData(AssetChartRequestParamBean chartRequestParamBean) {
		 Map<Integer, QuestionResponseSearchWrapper> questionEvaluationResponseList = prepareQuestionEvaluationResponses(chartRequestParamBean);
		 return prepareResponseForDrillDownQuest(chartRequestParamBean,questionEvaluationResponseList);
	}
	
	private List<ParamDrillDownBean> prepareResponseForDrillDownQuest(AssetChartRequestParamBean chartRequestParamBean,Map<Integer, QuestionResponseSearchWrapper> questionEvaluationResponseList) {
		 List<ParamDrillDownBean> questResponseDataList =  new ArrayList<>();
		 for(Entry<Integer, QuestionResponseSearchWrapper> entry: questionEvaluationResponseList.entrySet()){
			 QuestionResponseSearchWrapper oneResponse = entry.getValue(); 
			 ParamDrillDownBean drillDownBean = new ParamDrillDownBean();
			 drillDownBean.setTitle(oneResponse.getDisplayName());
			 drillDownBean.setRanges(Arrays.asList(3,6,10));
			 
			 BigDecimal eValue = new BigDecimal(0);
	    	 for(Integer oneAsset : oneResponse.getAssetResponse().keySet()){
	    		 eValue = eValue.add(oneResponse.fetchQuestionValueForAsset(oneAsset));
	    	 }	    	 
	    	 drillDownBean.setMeasures(Arrays.asList(oneResponse.fetchQuestionValueForAsset(chartRequestParamBean.getAssetId())));
	    	 eValue = eValue.divide(new BigDecimal(oneResponse.getAssetResponse().size()),2,RoundingMode.HALF_UP);
	    	 drillDownBean.setMarkers(Arrays.asList(eValue));	  
	    	 questResponseDataList.add(drillDownBean);
	     }
		return questResponseDataList;
	}
	
	@Override
	public ParameterBean getParameterBean(AssetChartRequestParamBean chartRequestParamBean) {		
		ParameterBean parameterBean = pService.generateParameterTree(pService.findFullyLoaded(chartRequestParamBean.getRootParameterId()));	
		List<ParameterBean> parameterBeanList =parameterBean.fetchListOfAllParameters();
	    for(ParameterBean pb : parameterBeanList){
		  if((pb.getId().intValue() == chartRequestParamBean.getParameterId().intValue()) 
				  && (pb.getParent().getId().intValue() == chartRequestParamBean.getParentParameterId().intValue()) )
			  return pb;
		}		
		return parameterBean;
	}

}
