package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.QuestionnaireFCParameterBean;
import org.birlasoft.thirdeye.beans.bcm.BCMWrapper;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;

/**
 * @author sunil1.gupta
 *
 */
public interface BCMReportService {

	/**
	 * Get BCMWrapper for view.
	 * @param bcmId
	 * @param parameterId
	 * @param qeId
	 * @return {@code BCMWrapper}
	 */
	public BCMWrapper getBCMWrapperForView(Integer bcmId,Integer parameterId, Integer qeId);

	/**
	 * Get BcmLevel assets.
	 * @param bcmLevelId
	 * @param fcparameterId
	 * @param qualityGateId
	 * @param qeId
	 * @return {@code List<AssetBean>}
	 */
	public List<AssetBean> getBcmLevelAssets(Integer bcmLevelId,Integer fcparameterId,Integer qualityGateId, Integer qeId);
	/**
	 * Get Bcm from elastic search.
	 * @param bcmId
	 * @return {@code IndexBCMBean}
	 */
	public IndexBCMBean getBcmFromElasticsearch(Integer bcmId);

	/**
	 * Get map Of QG one parameter.
	 * @param parameterId
	 * @return {@code Map<Integer, String>}
	 */
	public Map<Integer, String> getMapOfQGOneParam(Integer parameterId);
	/**
	 * Get list Of QuestionnaireFCParameterBean by workspaces.
	 * @param setOfWorkspaces
	 * @return {@code List<QuestionnaireFCParameterBean>}
	 */
	public List<QuestionnaireFCParameterBean> getListOfQuestionnaireFCParameterBean(Set<Workspace> setOfWorkspaces);
	/**
	 * Create Map Of Qe And Set Of Fc Param.
	 * @param setOfWorkspaces
	 * @return {@code Map<Questionnaire, Set<Parameter>>}
	 */
	public Map<Questionnaire, Set<Parameter>> createMapOfQeAndSetOfFcParam(Set<Workspace> setOfWorkspaces);
	
	/** Get List Of AssetIDs Based On Multiple BcmLevelIDs From Elastic Search
	 * @param bcmLevelIds
	 * @return List OF AssetIDs From elastic Search
	 */
	public List<Integer> getListOfAssetBasedOnLevelIdsFromElasticSearch(Integer[] bcmLevelIds);
}
