package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ParameterEditableServiceImplTest {

	@Mock
	private QuestionnaireService questionnaireService;
	@Mock
	private QuestionnaireParameterService questionnaireParameterService;
	@InjectMocks
	private ParameterService parameterService = new ParameterServiceImpl();

	private Parameter dummyParameter;
	private Parameter dummyParameter2;
	private Set<Parameter> setOfQp = new HashSet<>();
	private List<QuestionnaireParameter> listOfQp = new ArrayList<>();
	private List<Questionnaire> listOfQuestionnaire = new ArrayList<>();
	private Questionnaire questionnaire;
	private Questionnaire questionnaire2;
	private Questionnaire questionnaire3;
	private Questionnaire questionnaire4; 
	private QuestionnaireParameter questionnaireParameter;
	private QuestionnaireParameter questionnaireParameter2;
	private QuestionnaireParameter questionnaireParameter3;
	private QuestionnaireParameter questionnaireParameter4;


	@Before
	public void setup() {
		dummyParameter = new Parameter();
		dummyParameter.setId(21);
		dummyParameter.setType("LP");
		dummyParameter.setUniqueName("Dummy UN");
		dummyParameter.setDisplayName("Dummy Parameter");

		dummyParameter2 = new Parameter();
		dummyParameter2.setId(30);
		dummyParameter2.setType("LP");
		dummyParameter2.setUniqueName("Dummy UN2");
		dummyParameter2.setDisplayName("Dummy Parameter2");

		// Set Questionnaire
		setupQuestionnaire();

		//Set QuestionnaireParameter
		setupQuestionnaireParam();
	}

	private void setupQuestionnaire() {
		questionnaire = new Questionnaire();
		questionnaire.setId(22);
		questionnaire.setName("DummyTestParamEdit");
		questionnaire.setStatus(QuestionnaireStatusType.PUBLISHED.toString());
		listOfQuestionnaire.add(questionnaire);

		questionnaire2 = new Questionnaire();
		questionnaire2.setId(23);
		questionnaire2.setName("DummyTestParamEdit2");
		questionnaire2.setStatus(QuestionnaireStatusType.CLOSED.toString());
		listOfQuestionnaire.add(questionnaire2);

		questionnaire3 = new Questionnaire();
		questionnaire3.setId(25);
		questionnaire3.setName("DummyTestParamEdit3");
		questionnaire3.setStatus(QuestionnaireStatusType.EDITABLE.toString());
		listOfQuestionnaire.add(questionnaire3);

		questionnaire4 = new Questionnaire();
		questionnaire4.setId(26);
		questionnaire4.setName("DummyTestParamEdit4");
		questionnaire4.setStatus(QuestionnaireStatusType.REVIEWED.toString());
		listOfQuestionnaire.add(questionnaire4);
	}

	private void setupQuestionnaireParam() {
		questionnaireParameter = new QuestionnaireParameter();
		questionnaireParameter.setId(21);
		questionnaireParameter.setQuestionnaire(questionnaire);
		questionnaireParameter.setParameterByParameterId(dummyParameter);
		listOfQp.add(questionnaireParameter);
		setOfQp.add(questionnaireParameter.getParameterByParameterId());

		questionnaireParameter2 = new QuestionnaireParameter();
		questionnaireParameter2.setId(24);
		questionnaireParameter2.setQuestionnaire(questionnaire2);
		questionnaireParameter2.setParameterByParameterId(dummyParameter);
		listOfQp.add(questionnaireParameter2);

		questionnaireParameter3 = new QuestionnaireParameter();
		questionnaireParameter3.setId(25);
		questionnaireParameter3.setQuestionnaire(questionnaire3);
		questionnaireParameter3.setParameterByParameterId(dummyParameter);
		listOfQp.add(questionnaireParameter3);

		questionnaireParameter4 = new QuestionnaireParameter();
		questionnaireParameter4.setId(26);
		questionnaireParameter4.setQuestionnaire(questionnaire4);
		questionnaireParameter4.setParameterByParameterId(dummyParameter2);
		listOfQp.add(questionnaireParameter4);
	}

	private void mockServiceClassMethods() {
		Mockito.when(questionnaireService.findByClosedStatus(QuestionnaireStatusType.CLOSED.toString())).thenReturn(listOfQuestionnaire);
		Mockito.when(questionnaireParameterService.findByQuestionnaireIn(listOfQuestionnaire)).thenReturn(setOfQp);	
		Mockito.when(questionnaireParameterService.findByParameterByParameterId(dummyParameter)).thenReturn(listOfQp);	
	}

	@Test
	public void testParameterWithClosedStatus() {
		mockServiceClassMethods();

		Set<Parameter> parameterId = parameterService.findParameterIdByClosedStatus(QuestionnaireStatusType.CLOSED.toString());
		Assert.assertEquals(1,parameterId.size());
	}

	@Test
	public void testParameterStatusAsClosed() {
		mockServiceClassMethods();

		List<QuestionnaireParameter> listQP = parameterService.getQuestionnaireByParam(dummyParameter);
		Assert.assertFalse(listQP.isEmpty());
	}

	@Test
	public void testParameterStatus() {
		mockServiceClassMethods();

		Boolean status = parameterService.getStatusOfParam(dummyParameter);
		Assert.assertTrue(status);
	}
}
