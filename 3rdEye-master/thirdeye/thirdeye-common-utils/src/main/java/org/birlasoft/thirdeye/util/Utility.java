package org.birlasoft.thirdeye.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.birlasoft.thirdeye.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class for {@code common} methods. 
 */
public class Utility {
	
	private static final String ENCODING = "UTF-8";
	private static Logger logger = LoggerFactory.getLogger(Utility.class);
	
	private Utility() {
		    throw new IllegalAccessError("Utility class");
    }

	/**
	 * Create semicolon separated string.
	 * @param queTypeText
	 * @return {@code String}
	 */
	public static String createSemicolonSeparatedString(String queTypeText) {
		if(queTypeText != null){
			String[] queTypeTextArray = queTypeText.split(",");
			String[] trimedQueTypeTextArray = StringUtils.trimArrayElements(queTypeTextArray);
			String[] encodedStringArray = encodeStringArray(trimedQueTypeTextArray);
			return StringUtils.arrayToDelimitedString(encodedStringArray, ";");	
		}
		return queTypeText;
	}
	
	/**
	 * Encode string Array.
	 * @param stringArrayToEncode
	 * @return {@code String[]}
	 */
	public static String[] encodeStringArray(String[] stringArrayToEncode){
		int stringArrayToEncodeLength = stringArrayToEncode.length;
		String[] encodedStringArray = new String[stringArrayToEncodeLength];
		for (int count = 0; count < stringArrayToEncodeLength; count++) {
			try {
				encodedStringArray[count] =	URLEncoder.encode(stringArrayToEncode[count], ENCODING);
			} catch (UnsupportedEncodingException e) {
				logger.error("Exception in encodeStringArray() :: "+e); 
			}
		}
		return encodedStringArray;
	}
	
	/**
	 * Decode string Array.
	 * @param stringArrayToDecode
	 * @return {@code String[]}
	 */
	public static String[] decodeStringArray(String[] stringArrayToDecode){
		int stringArrayToDecodeLength = stringArrayToDecode.length;
		String[] decodedStringArray = new String[stringArrayToDecodeLength];
		for (int count = 0; count < stringArrayToDecodeLength; count++) {
			try {
				decodedStringArray[count] =	URLDecoder.decode(stringArrayToDecode[count], ENCODING);
			} catch (UnsupportedEncodingException e) {
				logger.error("Exception in decodeStringArray() :: "+e); 
			}
		}
		return decodedStringArray;
	}
	
	/**
	 * Split and decode string.
	 * @param queTypeText
	 * @return {@code String[]}
	 */
	public static String[] splitAndDecodeString(String queTypeText) {
		String[] decodedArrayOfOption;
		String[] arrayOfOption = queTypeText.split(";");		
		decodedArrayOfOption = Utility.decodeStringArray(arrayOfOption);
		return decodedArrayOfOption;
	}
	
	/**
	 * Decode string Array.
	 * @param stringToDecode
	 * @return {@code String}
	 */
	public static String decodeStringArray(String stringToDecode){
		    String decodeString = "";
			try {
				decodeString = URLDecoder.decode(stringToDecode, ENCODING);
			} catch (UnsupportedEncodingException e) {
				logger.error("Exception in decodeStringArray() :: "+e); 
			}
		return decodeString;
	}
	
	/** To split the {@code String} with {@code ;}
	 * @param stringToSplit 
	 * @return {@code String[]}
	 */
	public static String[] splitStringSemicolon(String stringToSplit){
		return stringToSplit.split(";");
	}
	
	/**
	 * Encode String
	 * @param stringToEncode
	 * @return {@code String}
	 */
	public static String encode(String stringToEncode){
		String encodedString = "";
		try {
			encodedString = URLEncoder.encode(stringToEncode, ENCODING);
		} catch (UnsupportedEncodingException e) {
			logger.error("Exception in encode() :: "+e); 
		}
		return encodedString;
	}
	
	/**
	 * Decode String
	 * @param encodedStringToDecode
	 * @return {@code String}
	 */
	public static String decode(String encodedStringToDecode){
		String stringToReturn = "";
		if (!StringUtils.isEmpty(encodedStringToDecode)){
			try {
				stringToReturn = URLDecoder.decode(encodedStringToDecode, ENCODING);
			} catch (UnsupportedEncodingException e) {
				logger.error("Exception in decode() :: "+e); 
			}
		}
		return stringToReturn;
	}
	
	/**
	 * Generics method convert {@code object} to {@code json string}
	 * @param objectToBeSerialized
	 * @return {@code <T> String}
	 */
	public static <T> String convertObjectToJSONString (T objectToBeSerialized){
		ObjectMapper oneJSONMapper = new ObjectMapper();
		
		Writer writer = new StringWriter();
		try {
			oneJSONMapper.writeValue(writer, objectToBeSerialized);
		} catch (JsonGenerationException e) {
			logger.error("JsonGenerationException in convertObjectToJSONString() :: "+e); 
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException in convertObjectToJSONString() :: "+e); 
		} catch (IOException e) {
			logger.error("IOException in convertObjectToJSONString() :: "+e); 
		}		
		return writer.toString();
	}
	
	/**
	 * Generics method convert {@code json string} to {@code object} 
	 * @param jSONString
	 * @param classForSerialization
	 * @return {@code <T> T}
	 */
	public static <T> T convertJSONStringToObject(String jSONString, Class<T> classForSerialization) {
		T oneExtractedObject = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			oneExtractedObject = mapper.readValue(jSONString, classForSerialization);
		} catch (IOException e) {
			logger.error("IOException in convertObjectToJSONString() :: "+e); 
		}
		
		return oneExtractedObject;
	}
	
	/**
	 * Find the common values from the hashmap values
	 * 
	 * @param assetListForChildren
	 * @return
	 */
	public static <K,V> Set<V> findCommonValues(Map<K, Set<V>> assetListForChildren) {
		Set<V> assetSetToReturn = new HashSet<>();
		boolean initializeList = true;
		for (Set<V> oneSetForParameter : assetListForChildren.values()){
			// The first time you need to add all the assets to the list
			// then use the List API to do the checking.
			if (initializeList){
				assetSetToReturn.addAll(oneSetForParameter);
				initializeList = false;
			}
			
			assetSetToReturn.retainAll(oneSetForParameter);
		}
		
		return assetSetToReturn;
	}
	
	/**
	 * Find the unique values from the hashmap values
	 * 
	 * @param assetListForChildren
	 * @return
	 */
	public static <K,V> Set<V> findUniqueValues(Map<K, Set<V>> assetListForChildren) {
		Set<V> assetSetToReturn = new HashSet<>();
		for (Set<V> oneSetForParameter : assetListForChildren.values()){
			assetSetToReturn.addAll(oneSetForParameter);
		}
		
		return assetSetToReturn;
	}
	
	/**
	 * method to get title name
	 * @param levelNumber
	 * @return {@code String}
	 */
	public static String getTitleName(int levelNumber) {
		String addIconTitle ;
		switch (levelNumber) {
		case 1:
			addIconTitle = Constants.BCM_LEVEL_TITLE_BUSINESS_FUNCTION;
			break;
		case 2:
			addIconTitle = Constants.BCM_LEVEL_TITLE_BUSINESS_PROCESS;
			break;
		case 3:
			addIconTitle = Constants.BCM_LEVEL_TITLE_ACTIVITY;
			break;
		default:
			addIconTitle = Constants.BCM_LEVEL_TITLE_VALUE_CHAIN;
			break;
		}
		
		return addIconTitle;
	}
	
	/**
	 * method to edit title name
	 * @param levelNumber
	 * @return {@code String}
	 */
	public static String getTitleOnEdit(int levelNumber) {
		String titleOnEdit = null;
		switch (levelNumber) {
		case 1:
			titleOnEdit = Constants.BCM_LEVEL_TITLE_EDIT_VALUE_CHAIN;
			break;
		case 2:
			titleOnEdit = Constants.BCM_LEVEL_TITLE_EDIT_BUSINESS_FUNCTION;
			break;
		case 3:
			titleOnEdit = Constants.BCM_LEVEL_TITLE_EDIT_BUSINESS_PROCESS;
			break;
		case 4:
			titleOnEdit = Constants.BCM_LEVEL_TITLE_EDIT_ACTIVITY;
			break;
		default:
			break;
		}
		
		return titleOnEdit;
	}
	
	public static String getUniqueHash(){	
		StringBuilder sb = new StringBuilder();
		try {
			Random random = new Random();
			String passwordToHash = new BigInteger(130, random).toString(32);
			// Create MessageDigest instance for MD5
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			md.update(passwordToHash.getBytes());
			// Get the hash's bytes
			byte[] bytes = md.digest();
			// Convert it to hexadecimal format		
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
		} catch (NoSuchAlgorithmException e) {
			
			logger.error("NoSuchAlgorithmException in convertObjectToJSONString() :: "+e); 
		}
		// Add password bytes to digest
		return sb.toString();		
	}
	
	/**
	 * Validate date format (yyyy-MM-dd).
	 * @param inDate
	 * @return {@code boolean}
	 */
	public static boolean isValidDate(String inDate) {
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	  }
	
	/**
	 * method to generate Unique ID for Asset
	 * @param tenantId
	 * @param assetType
	 * @param assetTemplateId
	 * @param assetName
	 * @return {@code String}
	 */
	
	public static String generateUid(String tenantId,String assetType , Integer assetTemplateId, String assetName)
	{
		return tenantId + "::" + "Asset" + "::" +assetType + "::" + assetTemplateId + "::" +assetName;
	
	}
	
	/**
	 * method to convert doble value to int
	 * @param value
	 * @return {@code int}
	 */
	public static int convertToInt(double value){
		 Double doubleValue = new Double(value) ;
		 return doubleValue.intValue();
	}
}

