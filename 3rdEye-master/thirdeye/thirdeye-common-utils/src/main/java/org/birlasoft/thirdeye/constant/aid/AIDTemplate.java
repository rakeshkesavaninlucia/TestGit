package org.birlasoft.thirdeye.constant.aid;

public enum AIDTemplate {

	DEFAULT("Default template for Application Interface Diagrams in the system");
	
	private String aidTemplateDescription;

	AIDTemplate(String aidTemplateDescription){
		this.aidTemplateDescription = aidTemplateDescription;
	}

}
