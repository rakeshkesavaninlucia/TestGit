package org.birlasoft.thirdeye.constant;

public enum Permissions {

	APP_LOGIN("Root permission to allow the user to login to the application"),
	
	USER_MODIFY ("Ability to create / modify a user"),
	USER_VIEW ("Ability to view users in the system"),
	USER_ROLE_VIEW("Ability to view users roles in the system"),
	USER_ROLE_MODIFY("Ability to modify the permissions associated to the roles"),
	
	TEMPLATE_VIEW("Ability to VIEW asset templates in the system"),
	TEMPLATE_MODIFY("Ability to create asset templates in the system"),
	
	ASSET_FILLDETAILS("Allow the user to fill the asset details"),
	
	REPORT_ASSET_GRAPH("Allow the user to create and view asset graphs"),
	REPORT_SCATTER_GRAPH("Allow user to extract scatter graph reports"),
	REPORT_FACET_GRAPH("Allow user to extract reports on the facets of the questionnaire"),
	REPORT_PHP("Allow user to extract portfolio health on a page reports"),
	REPORT_FUNCTIONAL_REDUNDANCY("Allow user to extract functional redundancy on a page reports"),
	REPORT_WAVE_ANALYSIS("Allow user to extract Wave Analysis reports"),
	
	WORKSPACES_MODIFY("Allow the user to create and modify workspace access"),
	WORKSPACES_VIEW("Allow the user to view details about a workspace"),
	WORKSPACES_VIEW_DASHBOARD("View the dashboards in the workspace"),
	WORKSPACES_MODIFY_DASHBOARD("Add or Modify the dashboards in the workspace"),
	
	
	QUESTION_MODIFY("Allow the user to create and modify questions"),
	QUESTION_VIEW("Allow the user to view questions"),
	
	QUESTIONNAIRE_MODIFY("Allow the user to create and modify questionnaires"),
	QUESTIONNAIRE_VIEW("Allow the user to view questionnaires"),
	QUESTIONNAIRE_RESPOND("Allow the user to respond to questionnaires"),
	
	AID_MODIFY("Allow the user to create and modify Application Interface Diagram"),
	AID_VIEW("Allow the user to view Application Interface Diagram"),
	
	BCM_MODIFY("Allow the user to create and modify Business Capability Map"),
	BCM_VIEW("Allow the user to view Business Capability Map"),
	
	RELATIONSHIP_MODIFY("Allow the user to create and modify Relationship"),
	RELATIONSHIP_VIEW("Allow the user to view Relationship"),
	
	PARAMETER_MODIFY("Allow the user to create and modify parameters"),
	PARAMETER_VIEW("Allow the user to view parameters"),
	BOOST_EVALUATED_VALUE("Allow the user to boost parameter evaluated value"),
	
	FUNCTIONAL_MAP_MODIFY("Allow the user to create and modify functional maps"),
	FUNCTIONAL_MAP_VIEW("Allow the user to view functional maps"),
	
	COLOR_SCHEME_MODIFY("Allow the user to create and modify color schemes"),
	COLOR_SCHEME_VIEW("Allow the user to view color schemes"),
	
	INDEX_CREATION("Allow the user to create Elastic Search index"),
	
	VIEW_TCO("Allow the user to view TCO Parameter"),
	
	MODIFY_TCO("Allow the user to modify TCO Parameter"),
	
	BENCHMARK_MODIFY("Allow the user to modify Benchmark"),
	BENCHMARK_VIEW("Allow the user to view Benchmark"),

	SNOW_VIEW("Allow the user to view data from Service Now"),

	ADMIN_VIEW("Allow the user to view all the reports"),
	
	GNG_VIEW("Allow the user to view Asset GO/No-Go Analysis");

	private final String permissionDescription;
	
	Permissions(String description){
		this.permissionDescription = description;
	}

	public String getPermissionDescription() {
		return permissionDescription;
	}
	
	
}
