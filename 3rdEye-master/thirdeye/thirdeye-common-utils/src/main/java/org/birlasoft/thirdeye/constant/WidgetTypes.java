package org.birlasoft.thirdeye.constant;

/**
 * 
 * Enum Types for Widgets
 *
 */
public enum WidgetTypes {

	FACETGRAPH ("Show your data across various dimensions");
	
	String description;
	
	WidgetTypes(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
	
}


