/**
 * 
 */
package org.birlasoft.thirdeye.constant;

/**
 * @author shaishav.dixit
 *
 */
public enum TcoGraphTypes {
	
	SORTED("Sorted Bar Graph"), COMBI("Combi Graph");
	
	private final String value;       

    private TcoGraphTypes(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}

}
