package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for questionnaire asset.
 */
public interface QuestionnaireAssetRepository extends JpaRepository<QuestionnaireAsset, Serializable> {
	/**
	 * Find list of questionnaire asset by questionnaire object. 
	 * @param questionnaire
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public List<QuestionnaireAsset> findByQuestionnaire(Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireAsset.asset", type = EntityGraphType.LOAD)
	@Query("SELECT qa FROM QuestionnaireAsset qa WHERE qa.questionnaire = (:qe)")
	public Set<QuestionnaireAsset> findByQuestionnaireLoadedAsset(@Param("qe") Questionnaire qe);
	/**
	 * Find list of questionnaire asset by questionnaire object. 
	 * @param asset
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public List<QuestionnaireAsset> findByAsset(Asset asset);
	
	/**
	 * @param id
	 * @return
	 */
	public QuestionnaireAsset findById(Integer id);
	/**
	 * fetch all QA by QE and assets.
	 * @param qe
	 * @param setOfAsset
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public Set<QuestionnaireAsset> findByQuestionnaireAndAssetIn(Questionnaire qe, Set<Asset> setOfAsset);
	
	/**
	 * fetch all QA by QE and asset.
	 * @param qe
	 * @param idOfAsset
	 * @return {@code List<QuestionnaireAsset>}
	 */
	public Set<QuestionnaireAsset> findByQuestionnaireAndAsset(Questionnaire qe, Asset idOfAsset);
	
	
}
