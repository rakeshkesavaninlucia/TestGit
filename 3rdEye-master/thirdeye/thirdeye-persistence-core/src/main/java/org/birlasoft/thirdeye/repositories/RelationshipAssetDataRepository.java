package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository {@code interface} for {@link RelationshipAssetData}
 *
 */
public interface RelationshipAssetDataRepository extends JpaRepository<RelationshipAssetData, Serializable> {
	/**
	 * Find parent/child relationship by asset.
	 * @param assets
	 * @return {@code List<RelationshipAssetData>}
	 */
	public Set<RelationshipAssetData> findByAssetByAssetIdIn(Set<Asset> assets);
	
	/**
	 * Get {@code List} of {@link RelationshipAssetData} by parent asset
	 * @param asset
	 * @return
	 */
	public List<RelationshipAssetData> findByAssetByParentAssetId(Asset asset);
	
	/**
	 * Get {@code List} of {@link RelationshipAssetData} by child asset
	 * @param asset
	 * @return
	 */
	public List<RelationshipAssetData> findByAssetByChildAssetId(Asset asset);
	
	/**
	 * Get {@code List} of {@link RelationshipAssetData} by parent asset or child asset
	 * @param parentAsset
	 * @param childAsset
	 * @return
	 */
	public List<RelationshipAssetData> findByAssetByParentAssetIdOrAssetByChildAssetId(Asset parentAsset, Asset childAsset);
}
