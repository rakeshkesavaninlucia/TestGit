package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository interface for asset.
 * @author samar.gupta
 */
public interface AssetRepository extends JpaRepository<Asset, Serializable> {
	
	/**
     * find set of asset by graph assets not in set of graph asset.
     * @param assets
     * @return {@code Set<Asset>}
    */
//	public Set<Asset> findByGraphAssetsNotIn(Set<GraphAsset> assets);
	/**
     * find set of asset by not in set of assets ids in graph.
     * @param assetsIdsInGraph
     * @return {@code Set<Asset>}
     */
	public Set<Asset> findByIdNotIn(Set<Integer> assetsIdsInGraph);
	/**
     * find set of asset by asset template in set of template.
     * @param assetTemplates
     * @return {@code Set<Asset>}
     */
	public Set<Asset> findByAssetTemplateIn(Set<AssetTemplate> assetTemplates);
	/**
	 * Find Set of asset by asset template.
	 * @param assetTemplate
	 * @return {@code Set<Asset>}
	 */
	@EntityGraph(value = "Asset.fullyLoaded", type = EntityGraphType.LOAD)
	public Set<Asset> findByAssetTemplate(AssetTemplate assetTemplate);
	
	@Override
	@Query("SELECT a FROM Asset a LEFT JOIN FETCH a.assetDatas ad LEFT JOIN FETCH ad.assetTemplateColumn LEFT JOIN FETCH a.assetTemplate WHERE a.id= ?1")
	public Asset findOne (Serializable id);
	
	@Query("SELECT a FROM Asset a LEFT JOIN FETCH a.assetDatas ad LEFT JOIN FETCH ad.assetTemplateColumn LEFT JOIN FETCH a.assetTemplate WHERE a.id in ?1")
	public Set<Asset> findMany (Collection<Integer> id);
	
	public Set<Asset> findByIdInAndAssetTemplateIn(Set<Integer> idsOfAsset, Set<AssetTemplate> assetTemplates);
	
	@EntityGraph(value = "Asset.fullyLoaded", type = EntityGraphType.LOAD)
	@Query(value = "select a from Asset a where a.assetTemplate in (select at.id from AssetTemplate at where at.workspace = ?1 and at.assetType = ?2)")
	public Set<Asset> findAssetsByWorkspaceIdAndAssetTypeId(Workspace workspaceId, AssetType assetTypeId);
	
	public Asset findByUid(String uid);
	
	/** find list of asset by list of id's.
	 * @param id
	 * @return
	 */
	public List<Asset> findByIdIn(List<Integer> id);
	
}
