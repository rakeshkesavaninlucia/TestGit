package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.Widget;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WidgetRepository extends JpaRepository<Widget, Serializable> {
	
}
