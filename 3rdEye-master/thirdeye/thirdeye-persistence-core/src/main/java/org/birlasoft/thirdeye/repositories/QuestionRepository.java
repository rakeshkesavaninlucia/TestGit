package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Category;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/** 
 * {@code Repository} interface for question.
 */
public interface QuestionRepository extends JpaRepository<Question, Serializable> {
	/**
	 * List of {@code Question} with in {@code workspace}
	 * @param workspaces
	 * @return  {@code List<Question>}
	 */
	public List<Question> findByWorkspaceIn(Set<Workspace> workspaces);
	/**
	 * List of {@code Question} with workspace id is {@code NULL}
	 * @return {@code List<Question>}
	 */
	@Query("select q from Question q where q.workspace=null")
	public List<Question> findQuestionsByWorkspaceNull();
	/**
     * Find list of question by workspace is null or workspace.
     * @param workspace
     * @return {@code List<Question>}
     */
	public List<Question> findByWorkspaceIsNullOrWorkspace(Workspace workspace);
	/** Find list of question by workspace is null or in set of workspaces.
	 * @param workspaces
	 * @return {@code List<Question>}
	 */
	public List<Question> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces);
	
	public List<Question> findByWorkspaceAndDisplayName(Workspace workspace, String displayName);
	
	public List<Question> findByIdIn(Set<Integer> setOfQuestionIds);
	public List<Question> findByWorkspace(Workspace workspace);
	public List<Question> findByWorkspaceIsNullOrWorkspaceAndQuestionType(Workspace workspace, String quesType);
	
	/**Find list of question as per question type and corresponding workspace
     * @author dhruv.sood
     * @param workspace
     * @param questionType
     * @return {@code List<Question>}
     */
	public List<Question> findByWorkspaceAndQuestionType(Workspace workspace, String questionType);
	
	/**Find list of question as per category and corresponding workspace
     * @author dhruv.sood
     * @param setOfWorkspaces
     * @param category
     * @return {@code List<Question>}
     */
	public List<Question> findByWorkspaceIsNullOrWorkspaceInAndCategoryNot(Set<Workspace> setOfWorkspaces, Category category);
	/**
	 * Find by workspaceIn And Category.
	 * @param setOfWorkspaces
	 * @param category
	 * @return {@code List<Question>}
	 */
	public List<Question> findByWorkspaceInAndCategory(Set<Workspace> setOfWorkspaces, Category category);
	/**
	 * Fetch list of question by system level or active workspace
	 * and question type and benchmark is null.
	 * @param workspace
	 * @param quesType
	 * @return {@code List<Question>}
	 */
	public List<Question> findByWorkspaceIsNullOrWorkspaceAndQuestionTypeAndBenchmarkIsNull(Workspace workspace, String quesType);
}
