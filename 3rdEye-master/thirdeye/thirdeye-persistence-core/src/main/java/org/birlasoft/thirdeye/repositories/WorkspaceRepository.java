package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *	Repository interface for workspace.
 */
public interface WorkspaceRepository extends JpaRepository<Workspace, Serializable> {
	
	/**
	 * find By Id Loaded User Worspaces
	 * @param id
	 * @return {@code Workspace}
	 */
	@EntityGraph(value = "Workspace.userWorkspaces", type = EntityGraphType.LOAD)
	@Query("select w from Workspace w WHERE w.id = (:id)")
	public Workspace findByIdLoadedUserWorspaces(@Param("id") Integer id);
}
