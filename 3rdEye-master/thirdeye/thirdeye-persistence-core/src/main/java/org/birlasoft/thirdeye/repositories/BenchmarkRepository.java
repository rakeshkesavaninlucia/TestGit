package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for Benchmark.
 * @author samar.gupta
 */
public interface BenchmarkRepository extends JpaRepository<Benchmark, Serializable> {
	/**
	 * Fetch list of Benchmark by workspaces 
	 * @param setOfWorkspace
	 * @return {@code List<Benchmark>}
	 */
	public List<Benchmark> findByWorkspaceIn(Set<Workspace> setOfWorkspace);
    
	/**
	 * Fetch list of Benchmark by WorkspaceIsNull Or WorkspaceIn 
	 * @param listOfWorkspaces
	 * @return
	 */
	public List<Benchmark> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> listOfWorkspaces);
	/**
	 * Fetch system level benchmark
	 * @return {@code List<Benchmark>}
	 */
	public List<Benchmark> findByWorkspaceIsNull();
}
