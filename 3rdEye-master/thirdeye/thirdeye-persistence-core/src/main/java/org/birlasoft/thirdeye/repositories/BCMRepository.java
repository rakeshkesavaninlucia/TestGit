package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BCMRepository extends JpaRepository<Bcm, Serializable> {
	/**
	 * find By Workspace Is Null And Bcm Is Null.
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceIsNullAndBcmIsNull();
	/**
	 * find By Workspace In
	 * @param workspaces
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceIn(Set<Workspace> workspaces);
	/**
	 * Fetch Bcms by workspaces and active status.
	 * @param workspaces
	 * @param status
	 * @return {@code List<Bcm>}
	 */
	public List<Bcm> findByWorkspaceInAndStatus(Set<Workspace> workspaces, boolean status);
}
