package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *	Repository interface for user.
 */
public interface UserRepository extends JpaRepository<User, Serializable>{

	/**
	 * Get {@code User} By {@code Email}
	 * @param email
	 * @return {@code User} Object
	 */
	@Query("select u from User u LEFT JOIN FETCH u.userRoles ur LEFT JOIN FETCH ur.role r LEFT JOIN FETCH r.rolePermissions rp  LEFT JOIN FETCH u.userWorkspaces uw LEFT JOIN FETCH uw.workspace where u.emailAddress=?1")
	public User findUserByEmail(String email);
	
	/** Get list of users, find By Delete Status.
	 * @param deleteStatus
	 * @return {@code List<User>}
	 */
	public List<User> findByDeleteStatus(Boolean deleteStatus);
	/**
	 * Find list of users By DeleteStatus And AccountExpired And AccountLocked.
	 * @param deleteStatus
	 * @param accountExpired
	 * @param accountLocked
	 * @return {@code List<User>}
	 */
	public List<User> findByDeleteStatusAndAccountExpiredAndAccountLocked(Boolean deleteStatus, Boolean accountExpired, Boolean accountLocked);
	
	/**
	 * Find list of users By FirstName Containing Or LastName Containing search term.
	 * @param search1
	 * @param search2
	 * @return {@code List<User>}
	 */
	public List<User> findByFirstNameContainingOrLastNameContaining(String search1,String search2);
	
}
