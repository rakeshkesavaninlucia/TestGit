package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.Role;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for role.
 */
public interface RoleRepository extends JpaRepository<Role, Serializable> {
	
	@EntityGraph(value = "Role.rolePermissions", type = EntityGraphType.LOAD)
	@Query("select r from Role r WHERE r.id = (:id)")
	public Role findByIdLoadedRolePermissions(@Param("id") Integer id);
}
