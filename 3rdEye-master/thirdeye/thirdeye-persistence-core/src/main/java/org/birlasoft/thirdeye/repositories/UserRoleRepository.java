package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *	Repository interface for user role.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Serializable> {
	/**
	 * Find list of user role by user.
	 * @param user
	 * @return {@code List<UserRole>}
	 */
	public List<UserRole> findByUser(User user);
}
