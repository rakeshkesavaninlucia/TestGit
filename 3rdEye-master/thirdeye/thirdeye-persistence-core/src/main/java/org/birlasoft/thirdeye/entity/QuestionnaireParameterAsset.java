package org.birlasoft.thirdeye.entity;
// Generated Dec 14, 2016 11:00:22 AM by Hibernate Tools 5.1.0.Beta1

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * QuestionnaireParameterAsset generated by hbm2java
 */
@Entity
@Table(name = "questionnaire_parameter_asset", uniqueConstraints = @UniqueConstraint(columnNames = {
		"questionnaireId", "parameterId", "assetId" }))
public class QuestionnaireParameterAsset implements java.io.Serializable {

	private static final long serialVersionUID = 2384345284176762745L;
	private Integer id;
	private Asset asset;
	private Parameter parameter;
	private Questionnaire questionnaire;
	private User userByCreatedBy;
	private User userByUpdatedBy;
	private BigDecimal boostPercentage;
	private Date createdDate;
	private Date updatedDate;

	public QuestionnaireParameterAsset() {
	}

	public QuestionnaireParameterAsset(Asset asset, Parameter parameter, Questionnaire questionnaire,
			User userByCreatedBy, User userByUpdatedBy, BigDecimal boostPercentage, Date createdDate,
			Date updatedDate) {
		this.asset = asset;
		this.parameter = parameter;
		this.questionnaire = questionnaire;
		this.userByCreatedBy = userByCreatedBy;
		this.userByUpdatedBy = userByUpdatedBy;
		this.boostPercentage = boostPercentage;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "assetId", nullable = false)
	public Asset getAsset() {
		return this.asset;
	}

	public void setAsset(Asset asset) {
		this.asset = asset;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parameterId", nullable = false)
	public Parameter getParameter() {
		return this.parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "questionnaireId", nullable = false)
	public Questionnaire getQuestionnaire() {
		return this.questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdBy", nullable = false)
	public User getUserByCreatedBy() {
		return this.userByCreatedBy;
	}

	public void setUserByCreatedBy(User userByCreatedBy) {
		this.userByCreatedBy = userByCreatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updatedBy", nullable = false)
	public User getUserByUpdatedBy() {
		return this.userByUpdatedBy;
	}

	public void setUserByUpdatedBy(User userByUpdatedBy) {
		this.userByUpdatedBy = userByUpdatedBy;
	}

	@Column(name = "boostPercentage", nullable = false, precision = 5)
	public BigDecimal getBoostPercentage() {
		return this.boostPercentage;
	}

	public void setBoostPercentage(BigDecimal boostPercentage) {
		this.boostPercentage = boostPercentage;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
