package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RelationshipTypeRepository extends JpaRepository<RelationshipType, Serializable> {
	public List<RelationshipType> findByWorkspace(Workspace workspace);
	public List<RelationshipType> findByWorkspaceIsNullOrWorkspace(Workspace workspace);
}
