package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for response data.
 */
public interface ResponseDataRepository extends JpaRepository<ResponseData, Serializable> {
	/** Find list of response data by response.
	 * @param response
	 * @return {@code List<ResponseData>}
	 */
	public List<ResponseData> findByResponse(Response response);
	/**
	 * Find list of response data by questionnaire question.
	 * @param questionnaireQuestion
	 * @return {@code List<ResponseData>}
	 */
	
	@Query("SELECT rd FROM ResponseData rd LEFT JOIN FETCH rd.response WHERE rd.questionnaireQuestion = (:questionnaireQuestion)")
	public List<ResponseData> findByQuestionnaireQuestion(@Param("questionnaireQuestion") QuestionnaireQuestion questionnaireQuestion);
	
	@Query("SELECT rd FROM ResponseData rd LEFT JOIN FETCH rd.response WHERE rd.questionnaireQuestion in (:questionnaireQuestions)")
	public List<ResponseData> findByQuestionnaireQuestionIn(@Param("questionnaireQuestions") Collection<QuestionnaireQuestion> questionnaireQuestion);
	
	@Query("SELECT rd FROM ResponseData rd LEFT JOIN FETCH rd.response WHERE rd.id in (:qqIds)")
	public List<ResponseData> findAll(@Param("qqIds") Collection<Integer> ids);
	
	/**
	 * Find by questionnaire question and response.
	 * @param questionnaireQuestion
	 * @param response
	 * @return {@code ResponseData}
	 */
	
	public ResponseData findByQuestionnaireQuestionAndResponse(QuestionnaireQuestion questionnaireQuestion, Response response);
	
	@Query("SELECT distinct rd.questionnaireQuestion from ResponseData rd WHERE rd.questionnaireQuestion in (:questionnaireQuestions)")
	public List<QuestionnaireQuestion> findDistinctQuestionnaireQuestionByQuestionnaireQuestionId(@Param("questionnaireQuestions") Set<QuestionnaireQuestion> questionnaireQuestion);
		
	@Query("SELECT rd from ResponseData rd WHERE rd.questionnaireQuestion = (:questionnaireQuestion)")
	public ResponseData findByQuestionnaireQuestionId(@Param("questionnaireQuestion") QuestionnaireQuestion questionnaireQuestion);
}
