package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.AssetType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for asset type.
 * @author samar.gupta
 */
public interface AssetTypeRepository extends JpaRepository<AssetType, Serializable> {
	/**
     * find By AssetType Name.
     * @param assetTypeName
     * @return {@code AssetType}
     */
	public AssetType findByAssetTypeName(String assetTypeName);
}
