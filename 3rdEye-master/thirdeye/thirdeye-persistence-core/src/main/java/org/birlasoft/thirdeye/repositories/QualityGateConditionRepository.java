package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QualityGateConditionRepository extends JpaRepository<QualityGateCondition, Serializable> {

	public List<QualityGateCondition> findByQualityGate(QualityGate qualityGate);
	
	public List<QualityGateCondition> findByQualityGateId(Integer qualityGate);
	
}
