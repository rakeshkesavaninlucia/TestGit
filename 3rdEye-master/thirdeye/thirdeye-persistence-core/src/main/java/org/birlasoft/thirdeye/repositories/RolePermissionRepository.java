package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RolePermissionRepository extends JpaRepository<RolePermission, Serializable> {
	
}
