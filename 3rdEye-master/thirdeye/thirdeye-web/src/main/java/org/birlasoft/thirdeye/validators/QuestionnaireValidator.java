package org.birlasoft.thirdeye.validators;

import java.util.Set;

import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class QuestionnaireValidator implements Validator {
	
	private static final String QUESTIONNAIRE_NAME = "name";
	private static final String ASSET_TYPE = "assetType.id";
	@Autowired
	private Environment env;
	@Autowired
	private WorkspaceService workspaceService;
	
	@Autowired
	private CustomUserDetailsService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Questionnaire.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Questionnaire questionnaire = (Questionnaire)target;
		
		if(questionnaire.getName()!= null && questionnaire.getName().isEmpty()){
		  ValidationUtils.rejectIfEmptyOrWhitespace(errors, QUESTIONNAIRE_NAME, "error.questionnaire.name", env.getProperty("error.questionnaire.name"));
	    }else{
			  validateUniqueQuestionnaire(errors, questionnaire);
		 }
		 if(questionnaire.getName()!= null && questionnaire.getName().length() > 45){
			 errors.rejectValue(QUESTIONNAIRE_NAME, "error.questionnaire.name.length", env.getProperty("error.questionnaire.name.length")); 
		 }
		if(questionnaire.getAssetType().getId() <= 0 ){
			errors.rejectValue(ASSET_TYPE, "error.questionnaire.assetType", env.getProperty("error.questionnaire.assetType"));
		}
	}

	private void validateUniqueQuestionnaire(Errors errors, Questionnaire questionnaire) {
		Workspace workspaceOne = workspaceService.findOneLoaded(userService.getActiveWorkSpaceForUser().getId(), true);
		  Set<Questionnaire> questionnaires =  workspaceOne.getQuestionnaires();
		      for (Questionnaire oneQuestionnaire : questionnaires) {
				if(questionnaire.getId() != null && questionnaire.getId().equals(oneQuestionnaire.getId())){
			        break;
				}else
					if(oneQuestionnaire.getName().trim().equals(questionnaire.getName().trim())){
					 errors.rejectValue(QUESTIONNAIRE_NAME, "error.questionnaire.name.unique",env.getProperty("error.questionnaire.name.unique"));
					 break;
				    }
			  }
	}

	/**
	 * method to validate Assets
	 * @param errors
	 */
	public void validateAssets(Errors errors) {	
		errors.rejectValue(QUESTIONNAIRE_NAME, "error.questionnaire.asset", env.getProperty("error.questionnaire.asset"));
	}
}
