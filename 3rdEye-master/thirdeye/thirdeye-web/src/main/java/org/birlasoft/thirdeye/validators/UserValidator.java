package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class UserValidator implements Validator {
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String EMAIL = "emailAddress";
	private static final String USER_PASS = "password";
	private static final String ORGANIZATION_NAME ="organizationName";
	private static final String PHOTO = "photo";
	private static final String COUNTRY ="country";
	private static final String QUESTIONANSWER ="questionAnswer";

	@Autowired
	private Environment env;

	@Autowired
	private UserService userService; 

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIRST_NAME, "error.first.name", env.getProperty("error.first.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, LAST_NAME, "error.last.name", env.getProperty("error.last.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, EMAIL, "error.email", env.getProperty("error.email"));
		if(user.getId() == null){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, USER_PASS, "error.password", env.getProperty("error.password"));
		}
		List<User> userall = userService.findAll();
		for (User user2 : userall) {
			if(user.getId() !=null && user.getId().equals(user2.getId())){
				break;
			}else if(user.getEmailAddress() !=null && user.getEmailAddress().equalsIgnoreCase(user2.getEmailAddress())){
				errors.rejectValue(EMAIL, "error.email.exists", env.getProperty("error.email.exists")); 
			}
		}
	}

	public void validateUserProfile(Object target, Errors errors,MultipartFile fileUpload){
		User user = (User)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIRST_NAME, "error.first.name", env.getProperty("error.first.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, LAST_NAME, "error.last.name", env.getProperty("error.last.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, EMAIL, "error.email", env.getProperty("error.email"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ORGANIZATION_NAME, "error.organization.name", env.getProperty("error.organization.name"));
		if(fileUpload.getSize()>329384){
			errors.rejectValue(PHOTO, "error.photo", env.getProperty("error.photo"));
		}
		if(user.getCountry()!=null && "select".equalsIgnoreCase(user.getCountry())){
			errors.rejectValue(COUNTRY, "error.country", env.getProperty("error.country"));
		}
	}

	public void validateQuestionAnswer(String question,String answer,String question2,String answer2,Errors errors){

		if("-1".equals(question) || "-1".equals(question2) ){
			errors.rejectValue(QUESTIONANSWER, "error.question", env.getProperty("error.question"));
		} else{

			if(answer.isEmpty() && question != null || answer2.isEmpty() && question2 != null){
				errors.rejectValue(QUESTIONANSWER, "error.answer", env.getProperty("error.answer"));
			}
		}
		if(!("-1".equals(question)&& "-1".equals(question2)) && question.equalsIgnoreCase(question2) ){
			errors.rejectValue(QUESTIONANSWER, "error.question.same", env.getProperty("error.question.same"));
			
		}
		
	}
}
