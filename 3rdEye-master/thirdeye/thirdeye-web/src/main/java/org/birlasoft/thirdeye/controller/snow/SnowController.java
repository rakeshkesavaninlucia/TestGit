package org.birlasoft.thirdeye.controller.snow;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.ws.http.HTTPException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.birlasoft.thirdeye.beans.JSONSNOWDataMapper;
import org.birlasoft.thirdeye.beans.JSONServiceNowPullData;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.views.JSONNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/snow")
public class SnowController {

	@Autowired
	public CustomUserDetailsService customUserDetailsService;

	/**
	 * @return {@code String}
	 */
	@RequestMapping(value = "/snow", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'SNOW_VIEW'})")
	public String getSnowData(Model model) throws IOException, HTTPException {

		JSONSNOWDataMapper finalresponse;

		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());

		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(new AuthScope(new HttpHost("dev36824.service-now.com")),
				new UsernamePasswordCredentials("admin", "ServiceNow12#"));
		System.out.println(HttpClients.custom().setDefaultCredentialsProvider(credsProvider));
		HttpClientBuilder httpClientBuilder = HttpClients.custom().setDefaultCredentialsProvider(credsProvider);
		HttpHost proxy = new HttpHost("10.193.146.20", 80);
		httpClientBuilder.setProxy(proxy);
		CloseableHttpClient httpclient = httpClientBuilder.build();
		try {
			HttpGet httpget = new HttpGet("https://dev36824.service-now.com/api/83203/thirdeyescriptedapi");

			httpget.setHeader("Accept", "application/json");
			System.out.println("Executing request " + httpget.getRequestLine());
			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());
				String responseBody = EntityUtils.toString(response.getEntity());
				finalresponse = convertJSONStringToObject(responseBody, JSONSNOWDataMapper.class);
			} finally {
				response.close();
			}
		} finally {
			httpclient.close();
		}
		model.addAttribute("finalresponse", finalresponse);
		return "snow/snow";
	}

	private JSONSNOWDataMapper getJSONObject(String responseBody) {
		JSONSNOWDataMapper SNData = new JSONSNOWDataMapper();
		SNData.setResults(getSNResponse(responseBody));
		return SNData;
	}

	private List<JSONServiceNowPullData> getSNResponse(String responseBody) {
		List<JSONServiceNowPullData> jsonResponse = null;
		if (responseBody != null) {
			JSONSNOWDataMapper snData = getSNData(responseBody);
			jsonResponse = snData.getResults();
		}
		return jsonResponse;
	}

	private JSONSNOWDataMapper getSNData(String responseBody) {
		JSONSNOWDataMapper mappedObject = null;
		if (responseBody != null) {
			mappedObject = Utility.convertJSONStringToObject(responseBody, JSONSNOWDataMapper.class);
		}
		return mappedObject;
	}

	private <T> T convertJSONStringToObject(String jSONString, Class<T> classForSerialization) {
		T oneExtractedObject = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setPropertyNamingStrategy(new JSONNamingStrategy());
			oneExtractedObject = mapper.readValue(jSONString, classForSerialization);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return oneExtractedObject;
	}

}
