package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Bcm;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class BcmValidator implements Validator {
	private static final String BCM_NAME = "bcmName";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Bcm.class.equals(clazz);
	}
                
	public void validate(Object target, Errors errors, Integer idOfBCMTemplate) {
		Bcm newBcm = (Bcm) target;
		  
		  if (StringUtils.isEmpty(newBcm.getBcmName())) {
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_NAME, "bcm.errors.workspace.bcmname.empty");
		  }else if(idOfBCMTemplate == -1){
			  errors.rejectValue("bcm", "bcm.errors.select.bcmtemplate");
			}
	}

	@Override
	public void validate(Object target, Errors errors) {
		Bcm newBcm = (Bcm) target;
		  
		  if (StringUtils.isEmpty(newBcm.getBcmName())) {
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_NAME, "bcm.errors.bcmtemplatename.empty");
		  }
	}
}
