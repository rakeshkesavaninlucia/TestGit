package org.birlasoft.thirdeye.controller.tco;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for TCO response screen. 
 * @author shaishav.dixit
 *
 */
@Controller
@RequestMapping("/tco")
public class TcoResponseController {
	
	private static final String ACTIVE_RESPONSE_ID = "activeResponseId";
	
	private QuestionnaireService questionnaireService;
	private TcoResponseService tcoResponseService;
	private ResponseService responseService;
	private CustomUserDetailsService customUserDetailsService;
	
	/**
	 * Constructor autowiring for controller dependencies.
	 * @param questionnaireService
	 * @param tcoResponseService
	 * @param responseService
	 * @param customUserDetailsService
	 */
	@Autowired
	public TcoResponseController(QuestionnaireService questionnaireService,
			TcoResponseService tcoResponseService,
			ResponseService responseService,
			CustomUserDetailsService customUserDetailsService) {
		this.questionnaireService = questionnaireService;
		this.tcoResponseService = tcoResponseService;
		this.responseService = responseService;
		this.customUserDetailsService = customUserDetailsService;
	}
	
	/**
	 * Show TCO response page.
	 * @param questionnaireId
	 * @param model
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/{id}/response",  method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String responseTcoCostElements(@PathVariable(value = "id") Integer questionnaireId, Model model, HttpSession httpSession) {
		
		Questionnaire questionnaire = questionnaireService.findOne(questionnaireId);
		// Is there already any response for this qe. Currently only one response per qe.
		// we can add the ability to add more later.
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(questionnaire, false);
		
		if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
			Response newResponse = responseService.save(responseService.createNewResponseObject(questionnaire, customUserDetailsService.getCurrentUser()));
			// put it in the session
			httpSession.setAttribute(ACTIVE_RESPONSE_ID, newResponse.getId());
		} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
			// put the response id into the session because only one response is allowed
			// per questionnaire.
			httpSession.setAttribute(ACTIVE_RESPONSE_ID, listOfResponseAvailable.get(0).getId());
		} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
			// this is an error scenario in the given application
			return null;
		}
		
		List<ChartOfAccountBean> listOfCoa = tcoResponseService.extractChartOfAccounts(questionnaire);
		
		model.addAttribute("questionnaire", questionnaire);
		model.addAttribute("listOfCoa", listOfCoa);
		model.addAttribute("chartOfAccountId", questionnaire.getId());
		model.addAttribute("activeButton","Collapse");
		return "tco/tcoResponse";
	}
	
	@RequestMapping(value = "/response/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String listTcoResponse(Model model) {
		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Questionnaire> listOfQuestionnaires = questionnaireService.findByQuestionnaireTypeAndStatusAndWorkspaceIn(QuestionnaireType.TCO.toString(), QuestionnaireStatusType.PUBLISHED.toString(), listOfWorkspaces, true);
		
		model.addAttribute("questionnaireType", QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		model.addAttribute("listOfQuestionnaires", listOfQuestionnaires);
		model.addAttribute("completionPercentage", responseService.getMapForCompletionPercentageOfQe(listOfQuestionnaires));
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Submit Cost");
		return "questionnaire/listQuestionnaireSubmitResponse";
	}

}
