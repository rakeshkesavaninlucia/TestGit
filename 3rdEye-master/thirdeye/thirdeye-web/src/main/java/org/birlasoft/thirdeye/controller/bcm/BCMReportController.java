package org.birlasoft.thirdeye.controller.bcm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.QuestionnaireFCParameterBean;
import org.birlasoft.thirdeye.beans.bcm.BCMWrapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for view list of BCMs {@code String} ,view BCM Level Asset {@code} and
 *  view BCM Level Asset For QualityGate {@code String}
 */
@Controller
@RequestMapping(value="/bcm")
public class BCMReportController {
	
	private CustomUserDetailsService customUserDetailsService;
	private BCMService bcmService;
	private BCMLevelService bcmLevelService;
	@Autowired
    private BCMReportService bCMReportService;
	
	/**
	 * Constructor to initialize services
	 * @param customUserDetailsService
	 * @param bcmService
	 * @param bcmLevelService
	 */
	@Autowired
	public BCMReportController(CustomUserDetailsService customUserDetailsService,
			BCMService bcmService,
			BCMLevelService bcmLevelService) {
		this.customUserDetailsService = customUserDetailsService;
		this.bcmService = bcmService;
		this.bcmLevelService = bcmLevelService;
	}

	/**
	 * To view List of BCMs
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	public String listBCMsToView (Model model) {		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());		
		List<Bcm> listOfBcms = new ArrayList<>();
		List<QuestionnaireFCParameterBean> listOfParams = new ArrayList<>();
		
		if(!listOfWorkspaces.isEmpty()){
			listOfBcms = bcmService.findByWorkspaceInAndStatus(listOfWorkspaces, true);
			// Get the list of FC parameter from parameter for active workspaces 
			// only those FC parameter, which have function map data
			listOfParams = bCMReportService.getListOfQuestionnaireFCParameterBean(listOfWorkspaces);
		}
		
		model.addAttribute("listOfBcms", listOfBcms);
		model.addAttribute("listOfParams", listOfParams);
		model.addAttribute("mode", 0);
		model.addAttribute(Constants.PAGE_TITLE,"Business Capability Map");
		model.addAttribute("pageSubTitle","BCM Asset Mapping");
		return "bcm/bcmTemplate";
	}
	
	/**
	 * Get BCMWrapper for view
	 * @param bcmId
	 * @param parameterId
	 * @param qeId
	 * @return {@code BCMWrapper}
	 */
	@RequestMapping(value = "{id}/view", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	@ResponseBody
	public BCMWrapper viewBCM (@PathVariable(value = "id") Integer bcmId,
			@RequestParam (value="paramid", required = false) Integer parameterId,
			@RequestParam (value="qeid", required = false ) Integer qeId) {			

		return bCMReportService.getBCMWrapperForView(bcmId,parameterId,qeId);
		
	}
	
	/**
	 * method to View BCM Level Asset
	 * @param model
	 * @param fcParameterId
	 * @param bcmLevelId
	 * @param qeId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/viewBCMLevelAsset/{fcid}/qe/{qeid}/level/{levelid}", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	public String viewBCMLevelAsset (Model model, @PathVariable(value = "fcid") Integer fcParameterId,
			@PathVariable(value = "levelid") Integer bcmLevelId, @PathVariable(value = "qeid") Integer qeId){		
		BcmLevel bcmLevel = new BcmLevel();
		if(bcmLevelId != null ){			
		  bcmLevel = bcmLevelService.findOneLoadedBcmLevel(bcmLevelId);
	    }		
		model.addAttribute( "mapOfQualityGates",bCMReportService.getMapOfQGOneParam(fcParameterId));
		model.addAttribute("bcmLevel", bcmLevel);
		model.addAttribute("fcParameterId", fcParameterId);
		model.addAttribute("qeid",qeId);
	    return "bcm/viewBCMLevelAsset";
	}	
	
	/**
	 *  method to View BCM Level Asset for QualityGate
	 * @param model
	 * @param bcmLevelId
	 * @param fcParameterId
	 * @param qualityGateId
	 * @param qeId
	 * @return {@code String}
	 */
	@RequestMapping(value = "{id}/bcmAssetWithHealth", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	public String viewBCMLevelAssetForQualityGate (Model model, @PathVariable(value = "id") Integer bcmLevelId,@RequestParam(value = "fcParameterId") Integer fcParameterId, 
			                                       @RequestParam (value="qualityGateId", required = false ) Integer qualityGateId,
			                                       @RequestParam (value="qeId", required = false ) Integer qeId){
		List<AssetBean>  setOfBcmLevelAssets = new ArrayList<>();	
		  if(bcmLevelId !=null)
		     setOfBcmLevelAssets = bCMReportService.getBcmLevelAssets(bcmLevelId, fcParameterId, qualityGateId, qeId);
		  
		  model.addAttribute("listOfBcmLevelAsset", setOfBcmLevelAssets);	 
		  return "bcm/viewBCMLevelAsset :: bcmLevelAssets";	
	}	
	
}
