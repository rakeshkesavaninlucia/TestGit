package org.birlasoft.thirdeye.controller.add;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.birlasoft.thirdeye.beans.add.ADDWrapper;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.GraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.GraphValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code graph} , view {@code graph} , edit {@code graph} , save {@code graph} ,
 * list of all {@code graph} , view {@code asset} on {@code graph} and {@code asset} modify on {@code graph}.
 */
@Controller
@RequestMapping(value="/add")
public class AddController {
	
	private GraphService graphService;
	private CustomUserDetailsService customUserDetailsService;
	private GraphValidator graphValidator;
	private final WorkspaceSecurityService workspaceSecurityService;
	private AssetTemplateService assetTemplateService;
	private AssetService assetService;
	private static final String MAP_OF_CHECKED_TEMPLATE = "mapOfCheckedTemplate";
	private static final String CREATE_GRAPH = "assetGraph/createGraph";
	
	/**
	 * Constructor to initialize services and class variable. 
	 * @param graphService
	 * @param customUserDetailsService
	 * @param graphValidator
	 * @param workspaceSecurityService
	 * @param assetTemplateService
	 * @param assetService
	 */
	@Autowired
	public AddController(GraphService graphService,
						   CustomUserDetailsService customUserDetailsService,
						   GraphValidator graphValidator,
						   WorkspaceSecurityService workspaceSecurityService,
						   AssetTemplateService assetTemplateService,
						   AssetService assetService) {
		this.graphService = graphService;
		this.customUserDetailsService = customUserDetailsService;
		this.graphValidator = graphValidator;
		this.workspaceSecurityService = workspaceSecurityService;
		this.assetTemplateService = assetTemplateService;
		this.assetService = assetService;
	}
	
	/**
	 * View {@code graph} by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String viewGraphByID(Model model, @PathVariable(value = "id") Integer idOfGraph) {
		Graph graph = graphService.findOne(idOfGraph, false);		
		// Check if the user can access this graph.
		workspaceSecurityService.authorizeGraphAccess(graph);
		model.addAttribute("graphId", idOfGraph);
		return "add/viewGraph";
	}
	
	/**
	 * Create new {@code graph} within a workspace.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String createGraph(Model model) {
		setInitialsForGraphCreation(model);
		model.addAttribute(MAP_OF_CHECKED_TEMPLATE, new HashMap<Integer, Integer>());
		model.addAttribute("graph", new Graph());
		return CREATE_GRAPH;
	}

	/**
	 * method to Initials model For Graph Creation
	 * @param model
	 */
	private void setInitialsForGraphCreation(Model model) {
		// Fetch the user's active workspaces
		List<Workspace> listOfWorkspaces = new ArrayList<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		List<AssetTemplate> assetTemplates = assetTemplateService.findByWorkspaceIn(listOfWorkspaces);
		Map<String, List<AssetTemplate>> mapOfTemplate = createMapForTemplate(assetTemplates);
		model.addAttribute("mapOfTemplate", mapOfTemplate);
	}

	/**
	 * method to create map for Asset templates
	 * @param assetTemplates
	 * @return {@code Map<String, List<AssetTemplate>>}
	 */
	private Map<String, List<AssetTemplate>> createMapForTemplate(List<AssetTemplate> assetTemplates) {
		Map<String, List<AssetTemplate>> mapOfTemplate = new HashMap<>();
		for (AssetTemplate assetTemplate : assetTemplates) {
			String assetTypeName = assetTemplate.getAssetType().getAssetTypeName();
			if(mapOfTemplate.containsKey(assetTypeName)){
				mapOfTemplate.get(assetTypeName).add(assetTemplate);
			}else{
				List<AssetTemplate> listOfTemplate = new ArrayList<>();
				listOfTemplate.add(assetTemplate);
				mapOfTemplate.put(assetTypeName, listOfTemplate);
			}
		}
		return mapOfTemplate;
	}
	
	/**
	 * Save {@code graph}.
	 * @param graph
	 * @param result
	 * @param model
	 * @param assetTemplateIds
	 * @return {@code String}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String saveGraph(Graph graph, BindingResult result, Model model,
			@RequestParam(value="assetTemplatesCheckbox", required = false) Set<Integer> assetTemplateIds) {
		// TODO - Is assetTemplatesCheckbox required and validate assetTemplateIds.
		if(assetTemplateIds == null || assetTemplateIds.isEmpty()){
			ObjectError error = new ObjectError("assetTemplatesCheckbox", "Please check at least one asset template.");
			result.addError(error);
		}
		graphValidator.validate(graph, result);
		if(result.hasErrors()){
			Map<Integer, Integer> mapOfCheckedTemplate;
			if(graph.getId() == null){
				mapOfCheckedTemplate = new HashMap<>();
			}else{
				mapOfCheckedTemplate = createMapForCheckedTemplate(graph);
			}
			model.addAttribute(MAP_OF_CHECKED_TEMPLATE, mapOfCheckedTemplate);
			setInitialsForGraphCreation(model);
			return CREATE_GRAPH;
		}
		graphService.saveGraph(graph, assetTemplateIds);
		return "redirect:/add/list";
	}

	
	
	/**
	 * List of {@code graphs} that the user has access to {@code (based on the workspaces they are linked to)}.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String getAllGraphs(Model model) {		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());		
		List<Graph> graphsInUserWorkspaces = new ArrayList<>();		
		if(!listOfWorkspaces.isEmpty()){
			graphsInUserWorkspaces = graphService.findByWorkspaceIn(listOfWorkspaces);
		}		
		model.addAttribute("graphsInUserWorkspaces", graphsInUserWorkspaces);
		return "assetGraph/listGraph";
	}
	
	/**
	 * Edit  {@code graph} by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String editADD(Model model, @PathVariable(value = "id") Integer idOfGraph) {		
		// TODO Authorization necessary. The user should be linked to the workspace in which the graph is there.		
		Graph graph = graphService.findOne(idOfGraph,true);
		// Check if the user can access this graph.
		workspaceSecurityService.authorizeGraphAccess(graph);		
		if(graph != null){
			Map<Integer, Integer> mapOfCheckedTemplate = createMapForCheckedTemplate(graph);
			setInitialsForGraphCreation(model);
			model.addAttribute(MAP_OF_CHECKED_TEMPLATE, mapOfCheckedTemplate);
			model.addAttribute("graph", graph);
		}
		return CREATE_GRAPH;
	}

	/**
	 * method to create map of checked template.
	 * @param graph
	 * @return {@code Map<Integer, Integer>}
	 */
	private Map<Integer, Integer> createMapForCheckedTemplate(Graph graph) {
		List<GraphAssetTemplate> graphAssetTemplates = graphService.findByGraph(graph, true);
		Map<Integer, Integer> mapOfCheckedTemplate = new HashMap<>();
		for (GraphAssetTemplate graphAssetTemplate : graphAssetTemplates) {
			Integer idOfTemplate = graphAssetTemplate.getAssetTemplate().getId();
			mapOfCheckedTemplate.put(idOfTemplate, idOfTemplate);
		}
		return mapOfCheckedTemplate;
	}
	
	/**
	 * method to get ADD doc.
	 * @param model
	 * @param idOfAsset
	 * @return {@code String}
	 */
	@RequestMapping(value = "/getAddDoc/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String getAddDoc(Model model, @PathVariable(value = "id") Integer idOfAsset) {
		
		Asset asset = assetService.findOne(idOfAsset);
		ADDWrapper addWrapper = graphService.getAssetDetailsForAdd(Stream.of(asset.getId()).collect(Collectors.toList()), new HashMap<>(), new HashSet<>()).get(0);
		
		model.addAttribute("addWrapper", addWrapper);
		return "add/addDocFragments :: addDoc";
	}
}
