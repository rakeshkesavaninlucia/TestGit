package org.birlasoft.thirdeye.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ScatterWrapper;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ScatterGraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for {@code view graph} , get {@code root} parameter and {@code plot} {@code graph}
 */
@Controller
@RequestMapping(value="/graph/scatter")
public class ScatterGraphController {
	
	private static Logger logger = LoggerFactory.getLogger(ScatterGraphController.class);
	
	private QuestionnaireService questionnaireService;
	private ParameterService parameterService;
	private ScatterGraphService scatterGraphService;
	private CustomUserDetailsService customUserDetailsService;
	private final WorkspaceSecurityService workspaceSecurityService;
	
	private QuestionnaireQuestionService qqService;
	@Autowired
	private QualityGateService qualityGateService;
	@Autowired
	private BCMLevelService bcmLevelService;
     
	/**
	 * Constructor to initialize services
	 * @param questionnaireService
	 * @param parameterService
	 * @param scatterGraphService
	 * @param customUserDetailsService
	 * @param workspaceSecurityService
	 * @param responseService
	 * @param qqService
	 */
	@Autowired
	public ScatterGraphController(QuestionnaireService questionnaireService,
			ParameterService parameterService,
			ScatterGraphService scatterGraphService,
			CustomUserDetailsService customUserDetailsService,
			WorkspaceSecurityService workspaceSecurityService,		
			QuestionnaireQuestionService qqService) {
		this.questionnaireService = questionnaireService;
		this.parameterService = parameterService;
		this.scatterGraphService = scatterGraphService;
		this.customUserDetailsService = customUserDetailsService;
		this.workspaceSecurityService = workspaceSecurityService;	
		this.qqService = qqService;
		
	}

	/**
	 * View graph by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_SCATTER_GRAPH'})")
	public String viewGraphByID(Model model	) {
		List<Questionnaire> questionnairesToDisplay = scatterGraphService.getQuestionnaireforScatter();
		List<BcmLevel> level1ToGetDisplay = scatterGraphService.getBcmLevelsforScatter();
		model.addAttribute("listOfQuestionnaire", questionnairesToDisplay);
		model.addAttribute("bcmLevelList", level1ToGetDisplay);
		model.addAttribute("listOfQualityGate", qualityGateService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser()));
		return "scatter/viewScatterGraph";
	}
	

	/**
	 * View graph by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/viewWave", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_WAVE_ANALYSIS'})")
	public String viewWaveGraphByID(Model model	) {
		 
		List<Questionnaire> questionnairesToDisplay = scatterGraphService.getQuestionnaireforScatter();
		model.addAttribute("listOfQuestionnaire", questionnairesToDisplay);
		return "waveAnalysis/viewWaveAnalysis";
	}
	
	/**
	 * Get root parameter.
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value="/fetchRootParameters/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_SCATTER_GRAPH'})")
	public String getRootParameter(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(questionnaire);
		
		// You should validate that we only show a list of quantifiable parameters
		
		// Put the parameters into the modal
		model.addAttribute("rootParamList", listOfRootParameters);
		
		return "scatter/scatterGraphFragment :: optionList(listOfParameters=${rootParamList})";
	}
	
	@RequestMapping(value="/fetchbcmlevel/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_SCATTER_GRAPH'})")
	public String getBcmLevels(Model model, @PathVariable(value = "id") Integer[] idsOfBcmLevels){
		
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcmLevelIdIn(idsOfBcmLevels);
		model.addAttribute("levellist", listOfBcmLevel);
		return "scatter/scatterGraphFragment :: optionList(listOfLevels=${levellist})";
	}

	/**
	 * Plot scatter graph.
	 * @param idsOfParameter
	 * @param idsOfQuestionnaire
	 * @param idOfQualityGate
	 * @return {@code ScatterWrapper}
	 */
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_SCATTER_GRAPH'})")
	@ResponseBody
	public ScatterWrapper plotGraph(@RequestParam(value = "questionnaireIds") String idsOfQuestionnaire, @RequestParam(value = "parameterIds") String idsOfParameter,
			@RequestParam(value = "qualityGateId",required=false)Integer idOfQualityGate,@RequestParam(value = "idsOfLevel1",required=false) Integer[] idsOfLevel1,@RequestParam(value = "idsOfLevel2",required=false) Integer[] idsOfLevel2,@RequestParam(value = "idsOfLevel3",required=false) Integer[] idsOfLevel3, @RequestParam(value = "idsOfLevel4",required=false) Integer[] idsOfLevel4,HttpServletRequest request){
		
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			if (!"parameterIds".equals(oneEntry.getKey()) && !"questionnaireIds".equals(oneEntry.getKey()) && !"qualityGateId".equals(oneEntry.getKey()) && !"_csrf".equals(oneEntry.getKey()) && !"idsOfLevel1".equals(oneEntry.getKey()) && !"idsOfLevel2".equals(oneEntry.getKey()) && !"idsOfLevel3".equals(oneEntry.getKey()) && !"idsOfLevel4".equals(oneEntry.getKey()) )
				filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		
		ScatterWrapper scatterWrapper = new ScatterWrapper();
		String[] questionnaireIds = idsOfQuestionnaire.split(",");
		String[] parameterIds = idsOfParameter.split(",");
 		for (String qeIdInStr : questionnaireIds) {
			Integer qeId = Integer.parseInt(qeIdInStr);
			if(qeId > 0){
				Questionnaire questionnaire = questionnaireService.findOne(qeId);
				// Check if the user can access this questionnaire.
				workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);				
				// Check QQ size if QE does not have QQs.
				List<QuestionnaireQuestion> qqToCheckSize = qqService.findByQuestionnaire(questionnaire, false);
				if(qqToCheckSize.isEmpty()){
					return scatterWrapper;
				}
			}
		}
 		try {
 			scatterWrapper = scatterGraphService.plotScatterGraph(questionnaireIds, parameterIds, idOfQualityGate,filterMap,idsOfLevel1,idsOfLevel2,idsOfLevel3,idsOfLevel4);
		} catch (Exception e) {
			logger.info("Something went wrong in Scatter graph - " + e);
		}
		return scatterWrapper;
	
	}

}
