package org.birlasoft.thirdeye.controller;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for {@code pre}{@code publish} {@code questionnaire} and {@code publish} {@code questionnaire}.
 */
@Controller
@RequestMapping("/questionnaire")
public class QuestionnairePublishController {

	private QuestionnaireService questionnaireService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private ParameterFunctionService parameterFunctionService;
	
	/**
	 * Constructor to initialize services 
	 * @param questionnaireService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param parameterFunctionService
	 */
	@Autowired
	public QuestionnairePublishController(
			QuestionnaireService questionnaireService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService,
			ParameterFunctionService parameterFunctionService) {
		this.questionnaireService = questionnaireService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.parameterFunctionService = parameterFunctionService;
	}

	/**
	 * Create a {@code pre publish} {@code questionnaire}
	 * @param questionnaireId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/prepublish", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String prePublishQuestionnaire(@PathVariable(value = "id") Integer questionnaireId, Model model) {

		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		List<QuestionnaireQuestionBean> questionnaireQuestionBeans = questionnaireQuestionService.listOfQuestionsForQuestionnaire(qe);
		List<ParameterFunction> parameterFunctions = parameterFunctionService.findByMandatoryQuestion(true);
		// Make map for mandatory questions in questionnaire. 
		Map<Integer, Integer> mapOfMandatoryQuestion = questionnaireQuestionService.createMapForMandatoryQuestion(questionnaireQuestionBeans, parameterFunctions);
		
		model.addAttribute("mapOfMandatoryQuestion", mapOfMandatoryQuestion);		
		model.addAttribute("questionnaireId", questionnaireId);
		model.addAttribute("questionnaire", qe);
		model.addAttribute("listOfQuestions", questionnaireQuestionService.listOfQuestionsForQuestionnaire(qe));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.DEF.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Publish Questionnaire");
		
		
		return "questionnaire/questionnairePrePublish";
	}

	/**
	 * Create a {@code publish} {@code questionnaire}
	 * @param questionnaireId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/publish", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String publishQuestionnaire(@PathVariable(value = "id") Integer questionnaireId) {
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		qe.setStatus(QuestionnaireStatusType.PUBLISHED.name());
		
		questionnaireService.save(qe);
		
		return "redirect:/questionnaire/list";
	}
}
