package org.birlasoft.thirdeye.controller.aid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.aid.AIDBlockHelper;
import org.birlasoft.thirdeye.beans.aid.AIDCentralAssetWrapperBean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock1Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock2Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock3Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock4Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlockConfig;
import org.birlasoft.thirdeye.beans.aid.JSONAIDSubBlockConfig;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.constant.aid.AIDBlockType;
import org.birlasoft.thirdeye.constant.aid.SubBlockTabs;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.interfaces.SubBlockUser;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller to configureAid, to addBlockToAIDReport , moveBlockInTheReport,saveBlok,edit block and refreshAIDReportBlocks ,.
 *
 */
@Controller
@RequestMapping(value=AIDReportConfigurationController.BASEURL)
public class AIDReportConfigurationController {

	static final String SUB_BLOCK_EDIT_SAVE = "/subBlockEdit/save";

	static final String BLOCKEDIT_SAVE = "/blockedit/save";

	static final String BASEURL = "/aid/configure";
	
	@Autowired
	AIDService aidService;
	
	@Autowired
	AssetTemplateService assetTemplateService;
	
	@Autowired
	QuestionnaireService questionnaireService;
	
	@Autowired
	WorkspaceSecurityService workspaceSecurityService; 
	
	@Autowired
	QuestionnaireQuestionService questionnaireQuestionService;
	
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	ParameterService parameterService;
	
	/**
	 * Configure one application interface diagram report.
	 * 
	 * @param model
	 * @param aidReportId
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String configureAid(Model model, @PathVariable(value = "id") Integer aidReportId){
		// TODO: Check AID security and permissions for AID_MODIFY.
		
		model.addAttribute("block1", new JSONAIDBlock1Bean());
		model.addAttribute("block2", new JSONAIDBlock2Bean());
		model.addAttribute("block3", new JSONAIDBlock3Bean());
		model.addAttribute("block4", new JSONAIDBlock4Bean());
		Aid oneAid = aidService.fetchOne(aidReportId, false);
		insertCentralAssetToModel(model, oneAid);
		
		model.addAttribute("aidId", oneAid.getId());
		
		return "aid/aidReportConfiguration";
	}


	/** 
	 * Insert the central asset into the model so that it can be displayed.
	 * 
	 * @param model
	 * @param oneAid
	 */
	private void insertCentralAssetToModel(Model model, Aid oneAid) {
		// Fetch the existing blocks on the report to show on the right hand side of the page.
		// Put it into the model
		
		// TODO Check if the user has permission to edit the blocks and given
		// that we are in this controller - put in the edit buttons
		model.addAttribute("showEdit", true);
		
		AIDCentralAssetWrapperBean oneWrapper = new AIDCentralAssetWrapperBean();
		
		oneWrapper.setId(oneAid.getId());
		for (AidBlock oneBlock : oneAid.getAidBlocks()){
			oneWrapper.addBlock(AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock));
		}
		
		// TODO display the blocks on the JSP
		aidService.loadSubBlocksConfigurationStrings(oneWrapper.getListOfBlocksInMainApp());
		
		oneWrapper.prepareForDisplay();
		model.addAttribute("centralAsset", oneWrapper);
	}
	
	/** 
	 * Add one block to the report configuration. This will only insert details of the 
	 * new block that is being inserted into the diagram.
	 * @param aidReportId
	 * @param blockType
	 */
	@RequestMapping(value = "/{id}/addBlock", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void addBlockToAIDReport(@PathVariable(value = "id") Integer aidReportId, AIDBlockType blockType){
		
		Aid oneAid = aidService.fetchOne(aidReportId, false);
		
		aidService.addBlockToReport(oneAid, blockType);
	}
	
	/**
	 * This method is designed to move a block up or down in a report
	 * @param aidReportId
	 * @param blockId
	 * @param direction
	 */
	@RequestMapping(value = "/{aidReportId}/moveBlock/{blockId}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void moveBlockInTheReport(@PathVariable(value = "aidReportId") Integer aidReportId, @PathVariable(value = "blockId") Integer blockId , @RequestParam("direction") Integer direction){
		// TODO Security
		// TODO Can the user access the aid report
		
		Aid oneReport =  aidService.fetchOne(aidReportId, true);
		List<AidBlock> listOfBlocks = new ArrayList<>( oneReport.getAidBlocks());
		
		if (listOfBlocks.isEmpty()){
			return;
		}
		
		// Sort it in sequence
		Collections.sort(listOfBlocks, new SequenceNumberComparator());
		
		updateListWithSwap(blockId, direction, listOfBlocks);
		
		for (int newSequenceNumber = 0 ; newSequenceNumber < listOfBlocks.size(); newSequenceNumber ++){
			// Put in the new sequence number
			listOfBlocks.get(newSequenceNumber).setSequenceNumber(newSequenceNumber+1);
		}
		
		aidService.saveBlocks(listOfBlocks);
	}
	
	/**
	 * method to delete A Block From The Report.
	 * @param aidReportId
	 * @param blockId
	 */
	
	@RequestMapping(value = "/{aidReportId}/deleteBlock/{blockId}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteABlockFromTheReport(@PathVariable(value = "aidReportId") Integer aidReportId, @PathVariable(value = "blockId") Integer blockId ){
		// TODO Security data and action!		
		Aid oneReport =  aidService.fetchOne(aidReportId, true);		
		List<AidBlock> blocksForDeletion = new ArrayList<>();
		for (AidBlock oneBlock : oneReport.getAidBlocks()){
			if (oneBlock.getId().equals(blockId)){
				blocksForDeletion.add(oneBlock);
				break;
			}
		}		
		aidService.deleteBlocks(blocksForDeletion);
		
	}


	private void updateListWithSwap(Integer blockId, Integer direction,
			List<AidBlock> listOfBlocks) {
		for (int i = 0 ;  i < listOfBlocks.size() ; i++){
			AidBlock oneBlock = listOfBlocks.get(i);
			if (oneBlock.getId() ==  blockId){
				int swapWith = i;
				if (direction == 1) {
					swapWith = swapWith +1;
				} else if (direction == -1) {
					swapWith = swapWith -1;
				}
				
				if (swapWith >= 0 && swapWith < listOfBlocks.size()){
					Collections.swap(listOfBlocks, i, swapWith);
					break;
				}
			}
		}
	}
	
	/**
	 * Refresh the central asset configuration on the page
	 * 
	 * @param model
	 * @param aidReportId
	 * @return
	 */
	@RequestMapping(value = "/{id}/refreshReport", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String refreshAIDReportBlocks(Model model,@PathVariable(value = "id") Integer aidReportId ) {
		Aid oneAid = aidService.fetchOne(aidReportId, false);
		
		insertCentralAssetToModel(model,oneAid);
		
		return "aid/default/defaultTemplateFragments :: centralApp(aidWrapper=${centralAsset})";
	}
	
	/**
	 * Edit the configuration of a block in the AID. This will return the modal 
	 * for the edit of the block based on the type
	 * 
	 * @param model
	 * @param blockId
	 * @return
	 */
	@RequestMapping(value = "/blockedit/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	// TODO permission check to modify AID
	public String editBlockByItsId (Model model, @PathVariable(value = "id") Integer blockId ) {
		
		AidBlock oneBlock = aidService.fetchOneBlock(blockId, true);
		
		// TODO: Authorize access on the AID		
		
		AIDBlockType oneBlockType = AIDBlockType.valueOf(oneBlock.getAidBlockType());
		
		// Put the necessary form binding for the block which is being edited.
		JSONAIDBlockConfig oneBlockConfig = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock);
		
		SubBlockUser [] subBlockUser = {oneBlockConfig};
		aidService.loadSubBlocksConfigurationStrings(Arrays.asList(subBlockUser));
		
		model.addAttribute("blockEditForm", oneBlockConfig);
		
		
		
		return oneBlockType.getCompleteFragmentForEdit("blockEditFormBean");
	}
	
	
	/**
	 * This delegates the save to the respective block handler
	 * 
	 * @param model
	 * @param blockId
	 * @param refreshModal
	 * @return
	 */
	@RequestMapping(value = BLOCKEDIT_SAVE, method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String saveBlockEdit (@RequestParam(value="id") Integer blockId,@RequestParam(value = "refreshModal", required = false) Boolean refreshModal) {
		
		AidBlock oneBlock = aidService.fetchOneBlock(blockId, true);
		// TODO Authorize access on the AID		
		// Based on the refresh modal indicator - there is no change you just 
		// want to show the block edit modal again. Hence simply forward it to the necessary controller
		if (refreshModal != null && refreshModal){
			return "forward:/aid/configure/blockedit/" + oneBlock.getId();
		}
		
		// Fetch the block type and forward the request to the controller
		AIDBlockType oneBlockType = AIDBlockType.valueOf(oneBlock.getAidBlockType());
		
		// Forward request to the validator and save.
		return "forward:" + AIDReportConfigurationController.BASEURL + AIDReportConfigurationController.BLOCKEDIT_SAVE + "/" + oneBlockType.name();
		
	}
	
	
	/**
	 * A basic sub block contains information about the template column, question or parameter id.
	 * This is reused by various blocks to store the configuration.
	 *  
	 * @param model
	 * @param blockId
	 * @param subBlockIdentifier
	 * @return
	 */
	@RequestMapping(value = "/subBlockEdit/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String editSubBlock (Model model,@PathVariable(value = "id") Integer blockId, @RequestParam("subBlockIdentifier") String subBlockIdentifier) {
		
		AidBlock oneBlock = aidService.fetchOneBlock(blockId, true);		
		// TODO Authorize access on the AID
		Aid aid = oneBlock.getAid();		
		JSONAIDSubBlockConfig subBlockConfig = AIDBlockHelper.getSubBlockByIdentifier(oneBlock, subBlockIdentifier);
		subBlockConfig.setParentBlockId(blockId);		
		// Sub block identifier is a string representation of the property in the block
		subBlockConfig.setSubBlockIdentifier(subBlockIdentifier);		
		model.addAttribute("subBlockForm", subBlockConfig);		
		// Insert list of columns in the modal
		AssetTemplate oneTemplate = assetTemplateService.findOne(aid.getAssetTemplate().getId());		
		model.addAttribute("listOfTemplateColumns",oneTemplate.getAssetTemplateColumns());		
		// Put in list of available questionnaires
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();
		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString()));	
		// Put in list of available questions / parameters depending on the tab which is active i.e if the user has already selected something.
		if (subBlockConfig.getQuestionQEId() > 0 ){
			Questionnaire q = questionnaireService.findOne(subBlockConfig.getQuestionQEId());
			model.addAttribute("listOfQuestions", questionnaireQuestionService.findQuestions(q));
		} else if (subBlockConfig.getParamQEId() > 0){
			putInParameters(model, subBlockConfig.getParamQEId());
		}
		model.addAttribute("activeTab", getActiveTabBasedOnSubBlock(subBlockConfig));
		
		return "aid/default/defaultTemplateFragments :: subBlockConfiguration(subBlockForm=${subBlockForm})";
	}
	/**
	 * method to get Active Tab Based On SubBlock.
	 * @param subBlockConfig
	 * @return
	 */
	
	private SubBlockTabs getActiveTabBasedOnSubBlock(JSONAIDSubBlockConfig subBlockConfig) {
		if (subBlockConfig.getParamId() > 0){
			return SubBlockTabs.selectParameter;
		} else if (subBlockConfig.getQuestionId() > 0){
			return SubBlockTabs.selectQuestion;
		} else if (subBlockConfig.getTemplateColumnId() > 0){
			return SubBlockTabs.selectColumn;
		}
		
		return SubBlockTabs.selectQuestion;
	}


	/** 
	 * Delegate the save of the sub block to the respective Block handlers
	 * 
	 * @param oneSubBlockConfig
	 * @return
	 */
	@RequestMapping(value = SUB_BLOCK_EDIT_SAVE, method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String saveSubBlock (@ModelAttribute("subBlockForm") JSONAIDSubBlockConfig oneSubBlockConfig) {
		
		AidBlock oneBlock = aidService.fetchOneBlock(oneSubBlockConfig.getParentBlockId(), true);
		// TODO Authorize access on the AID		
		// Fetch the block type and forward the request to the controller
		AIDBlockType oneBlockType = AIDBlockType.valueOf(oneBlock.getAidBlockType());
		// Forward request to the validator and save.
		return "forward:" + AIDReportConfigurationController.BASEURL + AIDReportConfigurationController.SUB_BLOCK_EDIT_SAVE + "/" + oneBlockType.name();
		
	}
	/**
	 * fetch question based on questionnaire
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	
	@RequestMapping(value = "/subBlockEdit/fetchQuestions/{id}", method = RequestMethod.POST)
//	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String fetchQuestions(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		Questionnaire q = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(q);
		
		// Need to put a blank subblock config in:
		model.addAttribute("questionId", -1);
		
		model.addAttribute("listOfQuestions", questionnaireQuestionService.findQuestions(q));
		model.addAttribute("ajaxRequest", true);
		return "aid/default/defaultTemplateFragments :: questionList(listOfQuestions=${listOfQuestions})"; 
	}
	
	/**
	 * method to fetch Parameters
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value = "/subBlockEdit/fetchParameters/{id}", method = RequestMethod.POST)
//	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String fetchParameters(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		putInParameters(model, idOfQuestionnaire);
		
		// Need to put a blank subblock config in:
		model.addAttribute("paramId", -1);
		model.addAttribute("ajaxRequest", true);
		return "aid/default/defaultTemplateFragments :: parameterList(listOfParameters=${listOfParameters})"; 
	}

/**
 * 
 * @param model
 * @param idOfQuestionnaire
 */
	private void putInParameters(Model model, Integer idOfQuestionnaire) {
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(questionnaire);
		model.addAttribute("listOfParameters", listOfRootParameters);
	}
	
	
}
