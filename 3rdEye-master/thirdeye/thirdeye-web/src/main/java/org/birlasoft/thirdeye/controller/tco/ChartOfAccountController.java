package org.birlasoft.thirdeye.controller.tco;

import java.util.HashSet;
import java.util.Set;

import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.QuestionnaireValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code chartOfAccount} , save {@code chartOfAccount} , create {@code chartOfAccount} 
 * {@code asset} {@code link} , save {@code chartOfAccount} {@code asset} {@code link} , list of all
 * {@code chartOfAccount} , edit {@code chartOfAccount}.
 * 
 */
@Controller
@RequestMapping("/tco")
public class ChartOfAccountController {
	
	private WorkspaceService workspaceService;
	private CustomUserDetailsService customUserDetailsService;
	private QuestionnaireValidator questionnaireValidator; 
	private QuestionnaireService questionnaireService;
	
	private final WorkspaceSecurityService workspaceSecurityService;	

	
	/**
	 * Constructor to initialize services
	 * @param workspaceService
	 * @param customUserDetailsService
	 * @param questionnaireService
	 * @param questionnaireValidator
	 * @param workspaceSecurityService
	 */
	@Autowired
	public ChartOfAccountController(WorkspaceService workspaceService,
			CustomUserDetailsService customUserDetailsService,
			QuestionnaireService questionnaireService,
			QuestionnaireValidator questionnaireValidator,
			WorkspaceSecurityService workspaceSecurityService){
		this.workspaceService = workspaceService;
		this.customUserDetailsService = customUserDetailsService;
		this.questionnaireService = questionnaireService;
		this.questionnaireValidator = questionnaireValidator;
		this.workspaceSecurityService = workspaceSecurityService;
	}
	
	/**
	 * Create  {@code chartOfAccount} for current user 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String createChartOfAccount(Model model) {		
		model.addAttribute("questionnaireForm", new Questionnaire());
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Create Chart of Account");
		prepareModel(model);		
		return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;
	}
	
	/**
	 * Save {@code chartOfAccount} and {@code update} {@code chartOfAccount}
	 * @param chartOfAccountData
	 * @param result
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String saveChartOfAccount(@ModelAttribute("questionnaireForm") Questionnaire chartOfAccountData,BindingResult result,Model model) {
		
		Questionnaire questionnaire = chartOfAccountData;
		
		if(questionnaire.getId() != null){
			questionnaire = getChartOfAccountFromDB(questionnaire);
		}
		questionnaireValidator.validate(questionnaire, result);
	    if(result.hasErrors()){
			prepareModel(model);			
			return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;	
	    }
	    
	    Questionnaire savedChartOfAccount ;
	    if(questionnaire.getId() == null){
	    	questionnaire.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
	    	savedChartOfAccount = questionnaireService.save(questionnaireService.createQuestionnaireObject(questionnaire, customUserDetailsService.getCurrentUser(), QuestionnaireType.TCO.toString()));
	    }else{	    	
	    	savedChartOfAccount = questionnaireService.save(questionnaireService.updateQuestionnaireObject(questionnaire, customUserDetailsService.getCurrentUser()));
	    }
		return "redirect:/tco/"+savedChartOfAccount.getId()+"/assets";
	}
	/**
	 * Create {@code chartOfAccount}{@code asset} link.
	 * @param idOfChartOfAccount
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/assets", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String createChartOfAccountAssetLink(@PathVariable(value = "id") Integer idOfChartOfAccount, Model model) {
		// Security - make sure he can access this questionnaire based on his workspace.
		Questionnaire chartOfAccount = questionnaireService.findOneFullyLoaded(idOfChartOfAccount);
		// Check if the user can access this chartOfAccount.
		workspaceSecurityService.authorizeQuestionnaireType(chartOfAccount,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(chartOfAccount);		
		
		model.addAttribute("mapsOfSelectedAsset", questionnaireService.getMapOfSelectedAssets(chartOfAccount));
		model.addAttribute("mapByAssetType", questionnaireService.getMapOfAssetByAssetType(chartOfAccount.getWorkspace(),chartOfAccount.getAssetType().getAssetTypeName()));
		model.addAttribute("questionnaireId", idOfChartOfAccount);
		model.addAttribute("questionnaireAssetLink", chartOfAccount);
		model.addAttribute("questionnaireAssetType", chartOfAccount.getAssetType().getAssetTypeName());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Asset Selection for Chart Of Account");
		
		return "questionnaire/questionnaireAssetLinkTemplate";
	}


	/**
     * Save {@code assets} for a given {@code ChartOfAccount id}.
	 * @param chartOfAccount
	 * @param idOfQuestionnaire
	 * @param assetParameter
	 * @param model
	 * @param result
	 * @return @code String}
	 */
	@RequestMapping(value = "/{id}/assets/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String saveChartOfAccountAssetLink(@ModelAttribute("questionnaireAssetLink") Questionnaire chartOfAccount, @PathVariable(value = "id") Integer chartOfAccountId, @RequestParam(value="assetarray[]", required=false) String[] assetParameter, Model model, BindingResult result) {
		// security validate if user can access the Questionnaire.
		Questionnaire coa = questionnaireService.findOneFullyLoaded(chartOfAccountId);
		if (assetParameter == null){
			questionnaireValidator.validateAssets(result);
		}
		if (result.hasErrors()){
			model.addAttribute("mapsOfSelectedAsset", questionnaireService.getMapOfSelectedAssets(coa));
			model.addAttribute("mapByAssetType", questionnaireService.getMapOfAssetByAssetType(coa.getWorkspace(),coa.getAssetType().getAssetTypeName()));
			model.addAttribute("questionnaireId", chartOfAccountId);
			model.addAttribute("questionnaireAssetType", coa.getAssetType().getAssetTypeName());
			return "questionnaire/questionnaireAssetLinkTemplate";
		}
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		
		questionnaireService.updateQuestionnaireEntity(coa, new HashSet<Asset>(questionnaireService.getNewAssetsToBeLinked(assetParameter)), null);
		return "redirect:/tco/"+chartOfAccountId+"/coststructures";
	}

	/**
	 * List of all {@code chartOfAccount} for current user
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String listOfChartOfAccount(Model model) {
		// List those questionnaires which the user has access to in the active workspace
		Set<Workspace> listOfWorkspaces = new HashSet <>();		
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction() );
		
		if(!listOfWorkspaces.isEmpty()){
		    model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(listOfWorkspaces,QuestionnaireType.TCO.toString()));
		}
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Chart of Account - List Chart of Account");
		model.addAttribute("questionnaireType", QuestionnaireType.TCO.toString());
		return "questionnaire/listQuestionnaire";
	}
	
	
	/**
	 * edit {@code chartOfAccount}
	 * @param idOfChartOfAccount
	 * @param model
	 * @return {@code String}
	 */

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String editChartOfAccount(@PathVariable(value = "id") Integer idOfChartOfAccount, Model model){
		
		Questionnaire chartOfAccount = questionnaireService.findOneFullyLoaded(idOfChartOfAccount);
		
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(chartOfAccount,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(chartOfAccount);
		
		model.addAttribute("questionnaireForm", chartOfAccount);
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Chart of Account - Create Chart of Account");
		prepareModel(model);	
		return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;
	}
	
	/**
	 * method to prepare model
	 * @param model
	 */
	private void prepareModel(Model model) {
		model.addAttribute(QuestionnaireConstants.USER_WORKSPACE_LIST, workspaceService.getUserWorkspaceList(customUserDetailsService.getCurrentUser()));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ASSET_TYPES , questionnaireService.getAssetType(customUserDetailsService.getActiveWorkSpaceForUser()));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
	}

	/**
	 * method to get {@code ChartOfAccount} from Database by {@code questionnaireId}
	 * @param questionnaire
	 * @return {@code Questionnaire}
	 */
	private Questionnaire getChartOfAccountFromDB(Questionnaire chartOfAccount) {
		Questionnaire chartOfAccountFromDB = questionnaireService.findOne(chartOfAccount.getId());
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(chartOfAccountFromDB);
		
		chartOfAccountFromDB.setName(chartOfAccount.getName());
		chartOfAccountFromDB.setDescription(chartOfAccount.getDescription());
		chartOfAccountFromDB.setAssetType(chartOfAccount.getAssetType());
		return chartOfAccountFromDB;		
	}
	
	
}

