package org.birlasoft.thirdeye.controller.tco;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CostElementService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.validators.QuestionValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
/**
 * Controller for Cost Element
 * @author dhruv.sood
 *
 */
@Controller
@RequestMapping(value = "/costElement")
public class CostElementController {	

	@Autowired
	private CostElementService costElementService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private QuestionValidator questionValidator;
		
	
	/**Method to create cost element
	 * @author dhruv.sood	 
	 * @param model
	 * @param costElementId
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String createFunctionalMap(Model model,  @RequestParam(value="ceid",required=false) Integer costElementId){
	
		if(costElementId != null){
			Question question = questionService.findOne(costElementId);
			model.addAttribute("costElementForm", question);
		}else{
			model.addAttribute("costElementForm", new Question());
		}		
		return "tco/tcoFragment :: createNewCostElement";
	}
	
	
	/**Method to view list of cost elements
	 * @author dhruv.sood
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String viewCostElement(Model model) {
		
		//Get the active Workspace	
		Workspace activeWorkspace = customUserDetailsService.getActiveWorkSpaceForUser();
						
		//Finding all the currency type questions as per current user workspace
		List<Question> listCostElement = questionService.findQuestionListByWorkspaceAndQuestionType(activeWorkspace,QuestionType.CURRENCY.getValue());
				
		model.addAttribute("listCostElement", listCostElement);		
		return "tco/viewCostElement";
	}
	
	
	/** Method to save a cost element
	 * @author dhruv.sood
	 * @param newCostElement	
	 * @return {@code String}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public Object saveCostElement(@ModelAttribute("costElementForm") Question newCostElement,BindingResult result, HttpServletResponse response,Model model,@RequestParam(value="ceid",required=false) Integer costElementId) {

		questionValidator.validateCostElementModel(newCostElement, result);
		if(result.hasErrors()){
			//model.addAttribute(attributeName, attributeValue)
			return new ModelAndView("tco/tcoFragment :: createNewCostElement");
		}

		
		if(!newCostElement.getTitle().isEmpty()){
			Question question = costElementService.createCostElementObject(newCostElement);	
			List<Question> listOfQuestions = questionService.findByWorkspaceAndDisplayName(question.getWorkspace(), question.getDisplayName());
			//If there is question with same name in DB then abort.	
			if(!listOfQuestions.isEmpty()){
				return "redirect:view";
			}
			costElementService.saveCostElement(question);			
		}
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "costElement/view");
		return JSONView.render(jsonToReturn, response);
	}	
	
}
