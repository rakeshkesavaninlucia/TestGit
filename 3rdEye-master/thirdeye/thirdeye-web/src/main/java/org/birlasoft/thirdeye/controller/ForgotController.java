package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.JSONQuestionAnswerMapper;
import org.birlasoft.thirdeye.entity.Password;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.birlasoft.thirdeye.security.UserAuthenticationDetailsBean;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.ForgotValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;
   
@Controller
@RequestMapping("/forgot")
public class ForgotController {

	@Autowired
	private TenantRepository tenantRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private ForgotValidator forgotValidator;
	@Autowired
	public ForgotController(ForgotValidator forgotValidator)
	{
		this.forgotValidator = forgotValidator;
	}
	
	@RequestMapping(value = "/questionAnswer",method = RequestMethod.POST)
	public String change(@RequestParam(value ="tenantCode") String tenantCode, @RequestParam(value ="username") String username, HttpServletRequest request,Model model){
		
		tenantIdentifier(tenantCode,request); 
		User currentUser = userService.findUserByEmail(username); 
		List<JSONQuestionAnswerMapper> listOfQuestions = new ArrayList<JSONQuestionAnswerMapper>();
		listOfQuestions =listOfQuestionAnswer(currentUser.getQuestionAnswer());
		model.addAttribute("listOfQuestions",listOfQuestions);
		model.addAttribute("mapperObject", new JSONQuestionAnswerMapper());
		model.addAttribute("username",username);
		model.addAttribute("tenantCode",tenantCode);
		return "questionAnswer";
	}

	
	@RequestMapping(value = "/newPassword" ,method = RequestMethod.POST)
	public String newPass(@ModelAttribute("mapperObject") JSONQuestionAnswerMapper jsonMapper,BindingResult result,@RequestParam(value ="username", required = false) String username,@RequestParam(value ="tenantCode") String tenantCode,HttpServletRequest request,Model model)
	{	
		tenantIdentifier(tenantCode,request); 
		forgotValidator.validate1(jsonMapper, result,username);
		if(result.hasErrors())
		{
			User currentUser = userService.findUserByEmail(username); 
			List<JSONQuestionAnswerMapper> listOfQuestions = new ArrayList<JSONQuestionAnswerMapper>();
			listOfQuestions =listOfQuestionAnswer(currentUser.getQuestionAnswer());
			model.addAttribute("listOfQuestions",listOfQuestions);
			model.addAttribute("username",username);
			model.addAttribute("tenantCode",tenantCode);
			return "questionAnswer";
		}
		
		model.addAttribute("username",username);
		model.addAttribute("tenantCode",tenantCode);
		model.addAttribute("ChangePass", new Password());
		return "newPassword";

	}
	private void setPassword(User user) {
		if(user.getPassword() != null && !user.getPassword().isEmpty()){
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(hashedPassword);
		}
	}
	
	@RequestMapping(value = "/login" ,method = RequestMethod.POST)
	public String abc(Model model,@RequestParam(value ="username", required = false) String username,@RequestParam(value ="tenantCode")String tenantCode , HttpServletRequest request,@ModelAttribute("ChangePass") Password password,BindingResult result){
		tenantIdentifier(tenantCode,request); 
		User currentUser = userService.findUserByEmail(username);
		forgotValidator.validatePassword(password, result);
		if(result.hasErrors())
		{
			model.addAttribute("username",username);
			model.addAttribute("tenantCode",tenantCode);
		
			return "/newPassword";
		}
		currentUser.setPassword(password.getNewPassword());
        setPassword(currentUser);
		userService.updateUserPassword(currentUser);
		return "redirect:/home";
	}
	
	
	private List<JSONQuestionAnswerMapper> listOfQuestionAnswer(String quesAnsJson){
		
		return Arrays.asList(Utility.convertJSONStringToObject(quesAnsJson, JSONQuestionAnswerMapper[].class));
		
	}
	
	private void tenantIdentifier(String tenantCode,HttpServletRequest request){
		
		Tenant tenant = tenantRepository.findByTenantAccountId(StringUtils.trimAllWhitespace(tenantCode));
		if (tenant != null && !StringUtils.isEmpty(tenant.getTenantAccountId()) && !StringUtils.isEmpty(tenant.getTenantUrlId()))
		{
			// Setup the details for the session
			UserAuthenticationDetailsBean detailsBean = new UserAuthenticationDetailsBean(tenant.getTenantAccountId(), tenant.getTenantUrlId());
			// Setup the request attributes so that going forward for this request the 
			// correct tenant db is hit
			RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
			if ( requestAttributes == null || requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER", RequestAttributes.SCOPE_REQUEST) == null)
			{
				RequestContextHolder.setRequestAttributes(new DispatcherServletWebRequest(request));
				requestAttributes = RequestContextHolder.getRequestAttributes();
			}
			requestAttributes.setAttribute("CURRENT_TENANT_IDENTIFIER", tenant.getTenantUrlId(), RequestAttributes.SCOPE_REQUEST);
		} 
	}
	
}
	
	
	
	

	
	


