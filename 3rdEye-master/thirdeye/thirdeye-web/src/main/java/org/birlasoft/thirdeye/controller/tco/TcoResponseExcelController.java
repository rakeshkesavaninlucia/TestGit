/**
 * 
 */
package org.birlasoft.thirdeye.controller.tco;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireExcelService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**Controller class for TCO Excel upload Download
 * @author dhruv.sood
 *
 */
@Controller
@RequestMapping("/tco")
public class TcoResponseExcelController {
	
	private static Logger logger = LoggerFactory.getLogger(TcoResponseExcelController.class);
	

	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private QuestionnaireExcelService questionnaireExcelService;
	@Autowired
	private ResponseService responseService;
	
	private static final String CHART_OF_ACCOUNT_ID = "chartOfAccountId";

	
	/**
	 * @author dhruv.sood
	 * Show export import modal for TCO Excel
	 * @param model
	 * @param Id
	 * @return
	 */
	@RequestMapping(value = "{id}/exportImport/fetchModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String fetchModalForExportImport(Model model, @PathVariable(value = "id") Integer chartOfAccountId) {
		model.addAttribute("chartOfAccountId", chartOfAccountId);
		model.addAttribute("activeTab", "exportTab");
		model.addAttribute("activeButton", "Close");
		return "tco/tcoFragment :: exportImportCOAExcelModal";
	}
	
	
	/**
	 * Method to download TCO Chart Of Account response into Excel
	 * @author dhruv.sood
	 * @param questionnaireId
	 * @param response
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/response/exportCOA", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	@ResponseBody
	public void downloadRespondToCOAExcel(@PathVariable(value = "id") Integer questionnaireId, HttpServletResponse response) {
		
		Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(questionnaireId);
		
		// Check if the user can access this questionnaire.
		if(questionnaireId != null){
			workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		} 
		
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(questionnaire, false);		
		Response qResponse = checkResponse(questionnaire, listOfResponseAvailable);
		
		//Call the service to respond to questionnaire
		questionnaireExcelService.exportCOATemplate(response,questionnaire, qResponse);
		
	}
	
	/**
	 * @param questionnaire
	 * @param listOfResponseAvailable
	 * @param response
	 * @return
	 */
	private Response checkResponse(Questionnaire questionnaire,List<Response> listOfResponseAvailable) {
		Response response = new Response();
		if (isOnlyOneResponseAllowed(questionnaire)){
			// Currently we take only one.
			
			if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
				response = responseService.save(responseService.createNewResponseObject(questionnaire, customUserDetailsService.getCurrentUser()));
				
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
				response =listOfResponseAvailable.get(0);
								
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
				// this is an error scenario in the given application
				response= null;
			}
		} else {
			// We can put in a new response id into the database
			// and then put it in the session
		}
		return response;
	}
	
	/** 
	 * Check only one response allowed.
	 * @param qe
	 * @return {@code boolean}
	 */
	private boolean isOnlyOneResponseAllowed (Questionnaire qe) {
		//Check only one response is allowed for Questionnaire
		return true;
	}
	
	/**
	 * method to upload response for chart of Account from excel
	 * @auth
	 * @param model
	 * @param idOfQuestionnaire
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "{id}/import", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	public String uploadTcoResponseExcel(Model model, @PathVariable(value = "id") Integer chartOfAccountId, @RequestParam(value="fileImport") MultipartFile file) {
		try {			
			Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(chartOfAccountId);			
			// Check if the user can access this questionnaire.
			if(chartOfAccountId != null){
				workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
			}	
			List<ExcelCellBean> listOfErrorCells = new ArrayList<>();
			boolean isFileExist = questionnaireExcelService.isFileExist(file , listOfErrorCells);
			
			if(isFileExist){
			  InputStream in = file.getInputStream();			
			  XSSFWorkbook workbook = new XSSFWorkbook(in);	
			  questionnaireExcelService.importQuestionnaireResponseExcel(workbook, questionnaire, listOfErrorCells);
			  workbook.close();
			  in.close();	
			 }
			//set list of errors in model if exists
			model.addAttribute("listOfErrors", listOfErrorCells);
			model.addAttribute(CHART_OF_ACCOUNT_ID, chartOfAccountId);
			model.addAttribute("activeTab", "importTab");
			
		} catch (IOException e) {
			logger.info("Exception thrown in TCOResponseExcelController :: uploadTcoResponseExcel()"+e);			
		}
		return "forward:/tco/"+chartOfAccountId+"/response";
	}
	
}
