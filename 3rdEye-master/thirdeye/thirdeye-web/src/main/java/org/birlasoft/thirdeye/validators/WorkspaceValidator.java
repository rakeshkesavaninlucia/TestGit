package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class WorkspaceValidator implements Validator {
	
	private static final String WORKSPACE_NAME = "workspaceName";
	@Autowired
	private Environment environment;
	@Autowired
	private WorkspaceService workSpaceService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Workspace.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, WORKSPACE_NAME,"error.workspace.name",environment.getProperty("error.workspace.name"));
		
		Workspace workspace = (Workspace) target;
		if (workspace.getWorkspaceName() != null && !"".equals(workspace.getWorkspaceName())) {
			List<Workspace> listworkspace = workSpaceService.findAll();
			for (Workspace workspaceuniuqe : listworkspace) {
				if (workspaceuniuqe.getWorkspaceName().trim().equals(workspace.getWorkspaceName().trim())) {
					errors.rejectValue(WORKSPACE_NAME,"error.workspace.name.unique",environment.getProperty("error.workspace.name.unique"));
					break;
				}
			}
		}

	}

}
