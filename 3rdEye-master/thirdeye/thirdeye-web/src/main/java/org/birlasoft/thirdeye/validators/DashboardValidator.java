package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Dashboard;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class DashboardValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Dashboard.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
			Dashboard newDashboard = (Dashboard) target;
		  
		  if (StringUtils.isEmpty(newDashboard.getDashboardName())) {
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dashboardName", "dashboard.errors.dashboardname.empty");
		  }else {		  
			 // Currently we are not validating on the unique name for dashboards
		      
		  }
	}
}
