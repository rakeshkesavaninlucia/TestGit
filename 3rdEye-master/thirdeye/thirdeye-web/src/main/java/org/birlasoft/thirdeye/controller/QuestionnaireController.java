package org.birlasoft.thirdeye.controller;

import java.util.HashSet;
import java.util.Set;

import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.QuestionnaireValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code questionnaire} , save {@code questionnaire} , create {@code questionnaire} 
 * {@code asset} {@code link} , save {@code questionnaire} {@code asset} {@code link} , list of all
 * {@code questionnaire} , edit {@code questionnaire}.
 * 
 */
@Controller
@RequestMapping("/questionnaire")
public class QuestionnaireController {
	
	private WorkspaceService workspaceService;
	private CustomUserDetailsService customUserDetailsService;
	private QuestionnaireValidator questionnaireValidator; 
	private QuestionnaireService questionnaireService;
	
	private final WorkspaceSecurityService workspaceSecurityService;

	/**
	 * Constructor to initialize services
	 * @param workspaceService
	 * @param customUserDetailsService
	 * @param questionnaireService
	 * @param questionnaireValidator
	 * @param workspaceSecurityService
	 */
	@Autowired
	public QuestionnaireController(WorkspaceService workspaceService,
			CustomUserDetailsService customUserDetailsService,
			QuestionnaireService questionnaireService,
			QuestionnaireValidator questionnaireValidator,
			WorkspaceSecurityService workspaceSecurityService){
		this.workspaceService = workspaceService;
		this.customUserDetailsService = customUserDetailsService;
		this.questionnaireService = questionnaireService;
		this.questionnaireValidator = questionnaireValidator;
		this.workspaceSecurityService = workspaceSecurityService;
	}
	
	/**
	 * Create  {@code questionnaire} for current user 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String createQuestionnaire(Model model) {
		model.addAttribute("questionnaireForm", new Questionnaire());
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Create Questionnaire");
		prepareModel(model);		
		return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;
	}
	
	/**
	 * Save {@code questionnaire} and {@code update} {@code questionnaire}
	 * @param questionnaireData
	 * @param result
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String saveQuestionnaire(@ModelAttribute("questionnaireForm") Questionnaire questionnaireData,BindingResult result,Model model
			) {
		
		Questionnaire questionnaire = questionnaireData;
		
		if(questionnaire.getId() != null){
			questionnaire = getQuestionnaireFromDB(questionnaire);
		}
		questionnaireValidator.validate(questionnaire, result);
	    if(result.hasErrors()){	    	
			prepareModel(model);			
			return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;	
	    }
	    
	    Questionnaire savedQuestionnaire ;
	    if(questionnaire.getId() == null){
	    	questionnaire.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
	    	savedQuestionnaire = questionnaireService.save(questionnaireService.createQuestionnaireObject(questionnaire, customUserDetailsService.getCurrentUser(), QuestionnaireType.DEF.toString()));
	    }else{	    	
	    	savedQuestionnaire = questionnaireService.save(questionnaireService.updateQuestionnaireObject(questionnaire, customUserDetailsService.getCurrentUser()));
	    }
		return "redirect:/questionnaire/"+savedQuestionnaire.getId()+"/assets";
	}

	private void prepareModel(Model model) {
		model.addAttribute(QuestionnaireConstants.USER_WORKSPACE_LIST, workspaceService.getUserWorkspaceList(customUserDetailsService.getCurrentUser()));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ASSET_TYPES , questionnaireService.getAssetType(customUserDetailsService.getActiveWorkSpaceForUser()));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.DEF.getAction());
	}

	/**
	 * method to get {@code Questionnaire} from Database by {@code questionnaireId}
	 * @param questionnaire
	 * @return {@code Questionnaire}
	 */
	private Questionnaire getQuestionnaireFromDB(Questionnaire questionnaire) {
		Questionnaire questionnaireFromDB = questionnaireService.findOne(questionnaire.getId());
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaireFromDB);
		
		questionnaireFromDB.setName(questionnaire.getName());
		questionnaireFromDB.setDescription(questionnaire.getDescription());
		questionnaireFromDB.setAssetType(questionnaire.getAssetType());
		return questionnaireFromDB;		
	}
	
	/**
	 * Create {@code questionnaire}{@code asset} link.
	 * @param idOfQuestionnaire
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/assets", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String createQuestionnaireAssetLink(@PathVariable(value = "id") Integer idOfQuestionnaire, Model model) {
		// Security - make sure he can access this questionnaire based on his workspace.
		Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(questionnaire,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		
		model.addAttribute("mapsOfSelectedAsset", questionnaireService.getMapOfSelectedAssets(questionnaire));
		model.addAttribute("mapByAssetType", questionnaireService.getMapOfAssetByAssetType(questionnaire.getWorkspace(),questionnaire.getAssetType().getAssetTypeName()));
		model.addAttribute("questionnaireId", idOfQuestionnaire);
		model.addAttribute("questionnaireAssetLink", questionnaire);
		model.addAttribute("questionnaireAssetType", questionnaire.getAssetType().getAssetTypeName());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "Questionniare Management - Asset Selection for Questionnaire");
		
		return "questionnaire/questionnaireAssetLinkTemplate";
	}


	/**
     * Save {@code assets} for a given {@code Questionnaire id}.
	 * @param questionnaire
	 * @param idOfQuestionnaire
	 * @param assetParameter
	 * @param model
	 * @param result
	 * @return @code String}
	 */
	@RequestMapping(value = "/{id}/assets/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String saveQuestionnaireAssetLink(@ModelAttribute("questionnaireAssetLink") Questionnaire questionnaire, @PathVariable(value = "id") Integer idOfQuestionnaire, @RequestParam(value="assetarray[]", required=false) String[] assetParameter, Model model, BindingResult result) {
		// security validate if user can access the Questionnaire.
		Questionnaire qe = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);
		if (assetParameter == null){
			questionnaireValidator.validateAssets(result);
		}
		if (result.hasErrors()){
			model.addAttribute("mapsOfSelectedAsset", questionnaireService.getMapOfSelectedAssets(qe));
			model.addAttribute("mapByAssetType", questionnaireService.getMapOfAssetByAssetType(qe.getWorkspace(),qe.getAssetType().getAssetTypeName()));
			model.addAttribute("questionnaireId", idOfQuestionnaire);
			model.addAttribute("questionnaireAssetType", qe.getAssetType().getAssetTypeName());
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
			return "questionnaire/questionnaireAssetLinkTemplate";
		}
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		questionnaireService.updateQuestionnaireEntity(qe, new HashSet<Asset>(questionnaireService.getNewAssetsToBeLinked(assetParameter)), null);
		return "redirect:/questionnaire/"+idOfQuestionnaire+"/parameters";
	}

	/**
	 * List of all {@code questionnaire} for current user
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_VIEW'})")
	public String listOfQuestionnaire(Model model) {
		// List those questionnaires which the user has access to in the active workspace
		Set<Workspace> listOfWorkspaces = new HashSet <> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		model.addAttribute("questionnaireType", QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
		
		if(!listOfWorkspaces.isEmpty()){
		    model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(listOfWorkspaces,QuestionnaireType.DEF.toString()));
		}
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - View Questionnaire - List Questionnaire");
		return "questionnaire/listQuestionnaire";
	}
	
	
	/**
	 * edit {@code questionnaire}
	 * @param idOfQuestionnaire
	 * @param model
	 * @return {@code String}
	 */

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String editQuestionnaire(@PathVariable(value = "id") Integer idOfQuestionnaire, Model model){
		
		Questionnaire qe = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);		
		model.addAttribute("questionnaireForm", qe);	
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Managenment - Create Questionnaire");
		prepareModel(model);	
		return QuestionnaireConstants.QUESTIONNAIRE_TEMPLATE_URL;
	}
	
	
}

