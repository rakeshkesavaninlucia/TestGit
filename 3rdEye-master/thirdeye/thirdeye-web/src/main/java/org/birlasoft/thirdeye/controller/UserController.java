package org.birlasoft.thirdeye.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.JSONQuestionAnswerMapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.Permissions;
import org.birlasoft.thirdeye.entity.Password;
import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.RolePermission;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RoleService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserRoleService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.UserValidator;
import org.birlasoft.thirdeye.validators.ChangePasswordValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
/**
 * Controller for {@code create} {@code user} , save {@code user} , list of all {@code user}
 * edit {@code user} and delete {@code}.
 */
@Controller
@RequestMapping(value="/user")
public class UserController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	private UserService userService;
	private CustomUserDetailsService customUserDetailsService;
	private UserValidator userValidator;
	private ChangePasswordValidator changePasswordValidator;
	private RoleService roleService;
	private UserRoleService userRoleService;
	private WorkspaceService workspaceService;
	private UserWorkspaceService userWorkspaceService;
	private static final String MAP_FOR_CHECKED_ROLE_ID ="mapForCheckedRoleIds";
	private static final String ROLES = "roles";
	private static final String CREATE_USER = "userManagement/createUser";
	private static final String USER_PROFILE = "userManagement/userProfile";
	private static final String CHANGE_PASS = "userManagement/changePassword";
	
	/**
	 * 
	 * @param userService
	 * @param customUserDetailsService
	 * @param userValidator
	 * @param changePasswordValidator
	 * @param roleService
	 * @param userRoleService
	 * @param workspaceService
	 * @param userWorkspaceService 
	 */
	@Autowired
	public UserController(UserService userService,
			CustomUserDetailsService customUserDetailsService,
			UserValidator userValidator,
			ChangePasswordValidator changePasswordValidator, 
			RoleService roleService,
			UserRoleService userRoleService,
			WorkspaceService workspaceService, UserWorkspaceService userWorkspaceService){
		this.userService = userService;
		this.customUserDetailsService = customUserDetailsService;
		this.userValidator = userValidator;
		this.changePasswordValidator = changePasswordValidator;
		this.roleService = roleService;
		this.userRoleService = userRoleService;
		this.workspaceService = workspaceService;
		this.userWorkspaceService = userWorkspaceService;
	}
	
	/** Create new User
	 * {@code create} a new {@code User} form. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_MODIFY'})")
	public String createUser(Model model) {
		model.addAttribute(MAP_FOR_CHECKED_ROLE_ID, new HashMap<Integer, Integer>());
		model.addAttribute(ROLES, roleService.findAll());
		model.addAttribute("listOfWorkspaces",workspaceService.findAll());
		model.addAttribute("UserForm", new User());
		return CREATE_USER;
	}
	
	/**
	 * This Method {@code save} the {@code user} and also {@code update} the {@code user}
	 * @param user
	 * @param result
	 * @param model
	 * * @param roles
	 * @return {@code String}
	 */	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_MODIFY'})")
	public String saveUser(@ModelAttribute("UserForm") User user, @RequestParam(value="role", required=false) String[] roles, BindingResult result,Model model,
			@RequestParam(value = "workspaceId") Integer workspaceId) {
		
		userValidator.validate(user, result);
		//Setting password in user object
		setPassword(user);
		if(result.hasErrors()){
    	//Method to validate and create user
    	createNewUser(user, roles, model,workspaceId);
    	return CREATE_USER;	
	    }
	  
	    //Method to save a new User
	    saveNewUser(user, roles,workspaceId);
	    
		return "redirect:/user/list";
	}

	/**Method to save a new User
	 * @param user
	 * @param roles
	 */
	private void saveNewUser(User user, String[] roles, Integer workspaceId) {
		Workspace workspace = workspaceService.findOne(workspaceId);
		if(user.getId() == null){
	    	User savedUser = userService.save(userService.createUserObject(user, customUserDetailsService.getCurrentUser()));
	    	if(roles != null && roles.length > 0){
		    	List<UserRole> userRoles = userRoleService.createListOfNewUserRoleObjects(roles, savedUser);
				userRoleService.save(userRoles);
			}
			if (workspaceId != -1)
				userWorkspaceService.save(userWorkspaceService.createNewUserWorkspaceObject(userService.findUserById(user.getId()), workspace));
		} else {	
	    	User savedUser = userService.save(userService.updateUserObject(user, customUserDetailsService.getCurrentUser()));
	    	userRoleService.deleteInBatch(userRoleService.findByUser(savedUser));
	    	if(roles != null && roles.length > 0){
		    	List<UserRole> userRoles = userRoleService.createListOfNewUserRoleObjects(roles, savedUser);
		    	userRoleService.save(userRoles);
	    	}
			if (workspaceId != -1)
				userWorkspaceService.save(userWorkspaceService.createNewUserWorkspaceObject(userService.findUserById(user.getId()), workspace));
		}
	}

	/**
	 * @param user
	 * @param roles
	 * @param model
	 */
	private void createNewUser(User user, String[] roles, Model model,Integer workspaceId) {
		
		if(user.getId() == null ){
			Map<Integer, Integer> mapForCheckedRoleIds = new HashMap<>();	
			if(roles!=null){
			for (String roleId : roles) {
				mapForCheckedRoleIds.put(Integer.parseInt(roleId), Integer.parseInt(roleId));
			}
			}
			model.addAttribute(MAP_FOR_CHECKED_ROLE_ID, mapForCheckedRoleIds);
		}else if(user.getId() != null){
			User userFromDB = userService.findUserById(user.getId());
			model.addAttribute(MAP_FOR_CHECKED_ROLE_ID, userService.createMapForUserRoles(userFromDB.getUserRoles()));
		}
		model.addAttribute(ROLES, roleService.findAll());
		model.addAttribute("listOfWorkspaces",workspaceService.findAll());
	}

	/** Method to set the password to User object
	 * @param user
	 */
	private void setPassword(User user) {
		if(user.getPassword() != null && !user.getPassword().isEmpty()){
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(hashedPassword);
		}
	}
	
	
	/** Method to display the list of all users
	 * List of all {@code users} 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_VIEW'})")
	public String listOfUser(Model model) {
		  model.addAttribute("listOfUsers", userService.findByDeleteStatusAndAccountExpiredAndAccountLocked(Constants.DELETE_STATUS_FALSE, Constants.ACCOUNT_EXPIRED_FALSE, Constants.ACCOUNT_LOCKED_FALSE));
		  return "userManagement/listUsers";
	}
	
	/** Method to edit  User details
	 * @param idOfUser
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_MODIFY'})")
	public String editUser(@PathVariable(value = "id") Integer idOfUser, Model model){
		User userById = userService.findUserById(idOfUser);
		model.addAttribute(MAP_FOR_CHECKED_ROLE_ID, userService.createMapForUserRoles(new HashSet<UserRole>(userRoleService.findByUser(userById))));
		model.addAttribute(ROLES, roleService.findAll());
		model.addAttribute("listOfWorkspaces",workspaceService.findAll());
		model.addAttribute("UserForm", userById);
		return CREATE_USER;
	}
	
	/** Method to delete User details from the records
	 * @param idOfUser
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_MODIFY'})")
	public String deleteUser(@RequestParam(value="userId") Integer idOfUser) {
	        userService.save(userService.deleteUser(idOfUser));
		    return "redirect:/user/list";
	}
	
	/** Method to view all the User Roles
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/roles/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_ROLE_VIEW'})")
	public String listOfUserRoles( Model model) {

		model.addAttribute("listOfRoles", roleService.findAll());
		return "userManagement/listRoles";
	}
	
	/** Method to edit role of a User
	 * @param idOfRole
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/roles/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_ROLE_MODIFY'})")
	public String editUserRole(@PathVariable(value = "id") Integer idOfRole, Model model) {
		
		Role roleToBeModified = roleService.findByIdLoadedRolePermissions(idOfRole);
		
		model.addAttribute("systemPermissions", Permissions.values());
		model.addAttribute("role", roleToBeModified);
		
		// Put in the map of permissions which are to be checked
		Map<String,Boolean> mapOfPermissions = new HashMap<>();
		for (RolePermission rolePerm : roleToBeModified.getRolePermissions()){
			mapOfPermissions.put(rolePerm.getPermissionname(), true);
		}
		model.addAttribute("rolePermissionMap", mapOfPermissions);
		return "userManagement/editRole";
	}
	
	/** Method to save User role
	 * @param roleId
	 * @param permissions
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/roles/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_ROLE_MODIFY'})")
	public String saveRole(@RequestParam(value="roleId") Integer roleId, @RequestParam(value="permissionsCheckbox", required=false) String[] permissions) {
		
		Role oneRole = roleService.findByIdLoadedRolePermissions(roleId);
		if (oneRole != null){
			Set<RolePermission> newRolePermissionList = new HashSet<>();
			
			// Delete existing permissions
			roleService.delete( oneRole.getRolePermissions());
			
			for (String onePermAsString : permissions) { 
				try{
					RolePermission rp = new RolePermission(oneRole, onePermAsString);
					newRolePermissionList.add(rp);
				} catch (Exception e){
					logger.error("Exception in saveRole(): "+e);
				}
			}			
			roleService.saveRolePermissions(newRolePermissionList);
		}		
		return "redirect:/user/roles/list";
	}

	/** Method to show User profile
	 * @param Id
	 * @param model
	 * @return
	 */
	
	@RequestMapping(value = "/{id}/editprofile", method = RequestMethod.GET)
	public String profileEditUser(Model model,@PathVariable(value = "id") Integer idOfUser){
		User userById = userService.findUserById(idOfUser);
		if(userById.getId()!=null){
			model.addAttribute("UserForm", userById);
		}
		return USER_PROFILE;
	}

	
   /** Method to display user image 
 * @param model
 * @param idOfUser
 * @param response
 */
	@RequestMapping(value = "/{id}/showimage", method = RequestMethod.GET)
	public void image(Model model,@PathVariable(value = "id") Integer idOfUser,HttpServletResponse response){
		
		User userById = userService.findUserById(idOfUser);
		try{
        response.setContentType("image/jpeg,image/png,image/jpg");
        OutputStream o =response.getOutputStream();
        o.write(userById.getPhoto());
        o.flush();
        o.close();
			}
		catch(Exception e){
			logger.error("Exception in showimage(): "+e);
		}
	}
	
	/** Method  to save user profile
	 * @param user
	 * @param model
	 * @param question
	 * @param answer
	 * @param question2
	 * @param answer2
	 * @param fileUpload
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/saveUserProfile", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("UserForm") User currentUser,Model model,@RequestParam(value="question", required=false) String question,@RequestParam(value="answer", required=false) String answer,@RequestParam(value="question2", required=false) String question2,@RequestParam(value="answer2", required=false) String answer2,@RequestParam(value="fileUpload") MultipartFile fileUpload, BindingResult result) {

		//Setting password in user object
		setPassword(currentUser);
		userValidator.validateUserProfile(currentUser, result,fileUpload);
		userValidator.validateQuestionAnswer(question, answer, question2, answer2,result);
		if(result.hasErrors()){
			return USER_PROFILE;	
		}

		try{
			if(currentUser.getId()!=null){
				if(!fileUpload.isEmpty()&& fileUpload.getSize()!=0) {
					byte[] bytes = fileUpload.getBytes();
					currentUser.setPhoto(bytes);
				}else if(currentUser.getPhoto().length==0){
					currentUser.setPhoto(null);
				}
				List<JSONQuestionAnswerMapper> questionAnswermapper = new ArrayList<>();
				questionAnswermapper.add(new JSONQuestionAnswerMapper(question,setAnswer(answer)));
				questionAnswermapper.add(new JSONQuestionAnswerMapper(question2,setAnswer(answer2)));
				userService.save(userService.updateUserObjectForUserProfile(currentUser,questionAnswermapper));

			}
		}
		catch (Exception e){
			logger.error("Exception in saveUserProfile(): "+e);
		}
		return USER_PROFILE;
	}

	/** Method to save answer in encrypt form 
	 * @param answer
	 * @return
	 */
	private String setAnswer(String answer){
		if(answer!=null){
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			
			return passwordEncoder.encode(answer);
		}else
			return "";
	}
	
	@RequestMapping(value="/changePassword", method=RequestMethod.GET)
	public String change(Model model){
		model.addAttribute("ChangePass", new Password());
		return CHANGE_PASS;
	}
	//Method to change password
		@RequestMapping(value = "/change", method = RequestMethod.POST)
	    public String changeUserPassword(@ModelAttribute("ChangePass") Password password,BindingResult result,Model model) {

			User currentUser = userService.findUserById(customUserDetailsService.getCurrentUser().getId());    
			password.setDbPassword(currentUser.getPassword());
			changePasswordValidator.validate(password, result);
			if(result.hasErrors())
			{
				return CHANGE_PASS;
			}
			currentUser.setPassword(password.getNewPassword());
	        setPassword(currentUser);
	        currentUser.setFirstLogin(true);
			userService.updateUserPassword(currentUser);
			model.addAttribute("UserForm", currentUser);
			return "userManagement/userProfile";
	    }
}