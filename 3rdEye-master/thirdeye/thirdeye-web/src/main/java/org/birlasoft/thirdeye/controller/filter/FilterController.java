package org.birlasoft.thirdeye.controller.filter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.search.api.beans.FacetBean;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FilterService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for handling all the requests of the Filter.
 *@author dhruv.sood
 */
@Controller
@RequestMapping(value="/filter")
public class FilterController {

	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;	
	@Autowired
	private FilterService filterService;

	
	/**Method to apply the Asset Types facet in Filter
	 * @author dhruv.sood
	 * @param model
	 * @return
	 */
	@RequestMapping( value = "/assetTypes", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String getAssetTypes(Model model) {
		List<AssetType> listOfAssetTypes = assetTypeService.findAll();
		model.addAttribute("listOfAssetTypes",listOfAssetTypes);		
		return "filter/filterFragment :: assetTypeFragment";
	}
	
	
	/**Method to apply customized facet in filter
	 * @author dhruv.sood
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping( value = "/facets", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String getFacets(Model model,HttpServletRequest request) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		int activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}			

		List<FacetBean> listOfFacetBean = filterService.getFacets(tenantId, activeWorkspaceId,filterMap);
		model.addAttribute("listOfFacetBean", listOfFacetBean);
		
		return "filter/filterFragment :: facetFragment";
	}

}
