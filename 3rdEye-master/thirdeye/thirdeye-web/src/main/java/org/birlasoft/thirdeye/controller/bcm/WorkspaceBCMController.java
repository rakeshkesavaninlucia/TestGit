package org.birlasoft.thirdeye.controller.bcm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.validators.BcmValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for Create BCM template {@code String} , show listOfBCM {@code String}
 */
@Controller
@RequestMapping(value="/wsbcm")
public class WorkspaceBCMController {
	
	private CustomUserDetailsService customUserDetailsService;
	private BCMService bcmService;
	private BCMLevelService bcmLevelService;
	private BcmValidator bcmValidator;

	/**
	 * Constructor to initialize services.
	 * @param customUserDetailsService
	 * @param bcmService
	 * @param bcmValidator
	 * @param bcmLevelService
	 */
	@Autowired
	public WorkspaceBCMController(CustomUserDetailsService customUserDetailsService,
			BCMService bcmService,
			BcmValidator bcmValidator,
			BCMLevelService bcmLevelService) {
		this.customUserDetailsService = customUserDetailsService;
		this.bcmService = bcmService;
		this.bcmValidator = bcmValidator;
		this.bcmLevelService = bcmLevelService;
	}

	/**
	 * Create a new BCM Template. This returns the modal for the form.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public String createWorkspaceBCM (Model model) {		
		model.addAttribute("workspaceBcmForm", new Bcm());
		model.addAttribute("listOfBCMTemplate", bcmService.findByWorkspaceIsNullAndBcmIsNull());		
		return "bcm/bcmFragments :: createWorkspaceBCM";
	}
	
	/**
	 * method to update or save BCM as required
	 * @param incomingWorkspaceBcm
	 * @param idOfBCMTemplate
	 * @param model
	 * @param response
	 * @param result
	 * @return {@code Object}
	 */
	@RequestMapping(value = "/create/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public Object saveWorkspaceBCM (@ModelAttribute("workspaceBcmForm") Bcm incomingWorkspaceBcm,
			@RequestParam("bcmTemplate") Integer idOfBCMTemplate, HttpServletResponse response,
			BindingResult result) {
		
		bcmValidator.validate(incomingWorkspaceBcm, result, idOfBCMTemplate);
		if(result.hasErrors()){
			ModelAndView mav = new ModelAndView("bcm/bcmFragments :: createWorkspaceBCM");
			mav.addObject("listOfBCMTemplate", bcmService.findByWorkspaceIsNullAndBcmIsNull());
			return mav;
		}
		
		Bcm bcmFromDb = bcmService.findOne(idOfBCMTemplate);
		
		// Ask the service to go and update or save as required
		Bcm newBcm = bcmService.saveOrUpdate(createNewBcmObject(incomingWorkspaceBcm, bcmFromDb));
		
		// Copy of Bcm Levels from Bcm template into Workspace Template
		List<BcmLevel> bcmLevels = bcmLevelService.findByBcm(bcmFromDb, false);
		
		if(bcmLevels != null && !bcmLevels.isEmpty()){
			bcmLevelService.createBcmLevel(bcmLevels, newBcm);
		}
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "wsbcm/list");
		
		return JSONView.render(jsonToReturn, response);
	}

	private Bcm createNewBcmObject(Bcm incomingWorkspaceBcm, Bcm bcmFromDb) {
		Bcm newWorkspaceBCM = new Bcm();
		newWorkspaceBCM.setBcm(bcmFromDb);
		newWorkspaceBCM.setBcmName(incomingWorkspaceBcm.getBcmName());
		newWorkspaceBCM.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		newWorkspaceBCM.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		newWorkspaceBCM.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		newWorkspaceBCM.setCreatedDate(new Date());
		newWorkspaceBCM.setUpdatedDate(new Date());
		return newWorkspaceBCM;
	}
	
	/**
	 * Get List of BCM for active Workspace user.
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	public String listWorkspaceBCM (Model model) {		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Bcm> listOfBcms = new ArrayList<>();
		
		if(!listOfWorkspaces.isEmpty()){
			listOfBcms = bcmService.findByWorkspaceIn(listOfWorkspaces, true);
		}
		
		model.addAttribute("listOfBcms", listOfBcms);
		return "bcm/listWorkspceBCM";
	}
	
	/**
	 * Update Bcm status.
	 * @param model
	 * @param idOfWorkspaceBcm
	 * @return {@code boolean}
	 */
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	@ResponseBody
	public boolean updateBCMStatus (Model model, @RequestParam(value = "bcmStatus") Integer idOfWorkspaceBcm) {
		return bcmService.isStatusActiveToShowMessage(idOfWorkspaceBcm);
	}
}
