package org.birlasoft.thirdeye.validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.birlasoft.thirdeye.beans.JSONQuestionAnswerMapper;
import org.birlasoft.thirdeye.entity.Password;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class ForgotValidator implements Validator {
	private static final String ANSWER ="answer";
	private static final String NEW_USER_PASS = "newPassword";
	private static final String CONFIRM_USER_PASS = "confirmPassword";

	@Autowired
	private Environment env;
	@Autowired
	private UserService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return JSONQuestionAnswerMapper.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ANSWER, "error.answer", env.getProperty("error.answer"));
	}
	  
	
    public void validate1(Object target, Errors errors,String username) {
    	JSONQuestionAnswerMapper jsonMapper = (JSONQuestionAnswerMapper)target;
    	 int count=0;
         
         
         List<String> answersFromUser = new ArrayList<String>();
         answersFromUser = Arrays.asList(jsonMapper.getAnswer().split(","));
         
         BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
         User userFromDb = userService.findUserByEmail(username);
         
         List<JSONQuestionAnswerMapper> listOfQuestionAnswer = new ArrayList<JSONQuestionAnswerMapper>();
         listOfQuestionAnswer =Arrays.asList(Utility.convertJSONStringToObject(userFromDb.getQuestionAnswer(), JSONQuestionAnswerMapper[].class));

         for(JSONQuestionAnswerMapper js:listOfQuestionAnswer ){
             
         String encodedAnswer = js.getAnswer();
         String rawAnswer = answersFromUser.get(count);
         if (!passwordEncoder.matches(rawAnswer, encodedAnswer)){
             errors.rejectValue(ANSWER, "error.answer.mismatch", env.getProperty("error.answer.mismatch"));    
            
         }
         count++;
         } 
    }
    
    public void validatePassword(Object target, Errors errors){
    	
    	Password password = (Password)target;
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, NEW_USER_PASS, "error.newPassword", env.getProperty("error.newPassword"));
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CONFIRM_USER_PASS, "error.confirmPassword", env.getProperty("error.confirmPassword"));

        if( !password.getNewPassword().equals(password.getConfirmPassword())){
            errors.rejectValue(CONFIRM_USER_PASS, "error.confirmPassword.mismatch", env.getProperty("error.confirmPassword.mismatch")); 
          }
    	
    }
}
