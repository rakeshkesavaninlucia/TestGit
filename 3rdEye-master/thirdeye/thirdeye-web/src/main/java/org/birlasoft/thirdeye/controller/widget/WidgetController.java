package org.birlasoft.thirdeye.controller.widget;

import java.util.Arrays;

import org.birlasoft.thirdeye.constant.WidgetTypes;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * Controller class for Widget
 *
 */
@Controller
@RequestMapping(value="/widgets")
public class WidgetController {
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	WorkspaceSecurityService workspaceSecurityService;
	
	private static final String UNABLE_TO_ACCESS = "You can not access this widget";
	private static final String UNABLE_TO_DELETE_WIDGET = "Unable to delete widget because it is not on the dashboard provided";
	
	/**
	 * This method returns a widget based on widget id
	 * @param model
	 * @param idOfWidget
	 * @return
	 */
	@RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_VIEW_DASHBOARD','WORKSPACES_MODIFY_DASHBOARD'})")
	public String showAWidget (@PathVariable(value = "id") Integer idOfWidget) {

		Widget widget = dashboardService.findOneWidget(idOfWidget, true);
		
		// Check if the widget belongs to a dashboard which is in the active workspace
		if (!widget.getDashboard().getWorkspace().getId().equals(customUserDetailsService.getActiveWorkSpaceForUser().getId())){
			throw new AccessDeniedException(UNABLE_TO_ACCESS);
		}
		
		// Fetch the widget details and configuration
		WidgetTypes widgetType = WidgetTypes.valueOf(widget.getWidgetType());
		
		String forwardURL = "";	
		if(widgetType.equals(widgetType.FACETGRAPH)){
			forwardURL = "forward:/widgets/facets/load/" + idOfWidget;
		}				
		return forwardURL;
	}
	
	/**
	 * This method deletes a widget from dashboard
	 * @param idOfWidget
	 * @param dashboardId
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteWidgetFromDashboard ( @PathVariable(value = "id") Integer idOfWidget, @RequestParam(value="dashboardId") Integer dashboardId) {
		
		// Get Dashboard form dashboardId
		Dashboard dashboard = dashboardService.findOne(dashboardId);
		
		// Validate Dashboard Access
		workspaceSecurityService.authorizeDashboardAccess(dashboard);
		
		// Delete the widget
		Widget widget = dashboardService.findOneWidget(idOfWidget, true);		
		if (dashboard.getId().equals(widget.getDashboard().getId())){
			Widget [] widgetArray = {widget};
			Arrays.asList(widgetArray);			
			dashboardService.deleteWidget(Arrays.asList(widgetArray));
		} else {
			throw new AccessDeniedException(UNABLE_TO_DELETE_WIDGET);
		}		
	}
}
