package org.birlasoft.thirdeye.controller.aid;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.aid.AIDBlockHelper;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock1Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock2Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock3Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDBlock4Bean;
import org.birlasoft.thirdeye.beans.aid.JSONAIDSubBlockConfig;
import org.birlasoft.thirdeye.constant.aid.SubBlockTabs;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for saveBlock1 {@code List<AidBlock>} , saveSubBlockForSubBlock1 {@code List<AidBlock>} , saveBlock2 {@code List<AidBlock>} ,
 * saveSubBlockForSubBlock2 {@code List<AidBlock>} , saveBlock3 {@code List<AidBlock>} .
 *
 */
@Controller
@RequestMapping(value=AIDReportConfigurationController.BASEURL)
public class AIDBlockEditController {

	private static final String CONFIG_UPDATED = "configUpdated";
	@Autowired
	AIDService aidService;
	
	/**
	 * method to save the Block1{@code List<AidBlock>} 
	 * @param block1Bean
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.BLOCKEDIT_SAVE+"/BLOCK_1", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveBlock1 (@ModelAttribute("blockEditForm") JSONAIDBlock1Bean block1Bean,  HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		AidBlock oneBlock = aidService.fetchOneBlock(block1Bean.getId(), true);
		JSONAIDBlock1Bean block1DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Make the update to the object title only.
		block1DBBean.setBlockTitle(block1Bean.getBlockTitle());
		
		// Serialize the JSON string
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block1DBBean));
		
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * method to save SubBlock For SubBlock1 {@code List<AidBlock>}
	 * @param subBlockBean
	 * @param activeTab
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.SUB_BLOCK_EDIT_SAVE +"/BLOCK_1", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveSubBlockForSubBlock1 (@ModelAttribute("subBlockForm") JSONAIDSubBlockConfig subBlockBean, @RequestParam ("activeTab") String activeTab, HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?		
		// Get the parent block this sub block belongs to
		AidBlock oneBlock = aidService.fetchOneBlock(subBlockBean.getParentBlockId(), true);
		JSONAIDBlock1Bean block1DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:		
		// Based on the sub block identifier you update the appropriate sub-block of the block		
		// Get the sub block from the DB bean
		JSONAIDSubBlockConfig dbSubBlock = block1DBBean.getSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier());
		// Update the db subblock based on the incoming values
		dbSubBlock = updateSubBlockBasedOnActiveTab(subBlockBean, dbSubBlock, activeTab);		
		// Set the sub block back into the bean that is going to go back to the database
		block1DBBean.setSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier(), dbSubBlock);		
		// Serialize and ready for insert
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block1DBBean));		
		// Go for the save.
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}

	/**
	 * method to save Block2 {@code List<AidBlock>}
	 * @param block2Bean
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.BLOCKEDIT_SAVE+"/BLOCK_2", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveBlock2 (@ModelAttribute("blockEditForm") JSONAIDBlock1Bean block2Bean,  HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		AidBlock oneBlock = aidService.fetchOneBlock(block2Bean.getId(), true);
		JSONAIDBlock2Bean block2DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Make the update to the object title only.
		block2DBBean.setBlockTitle(block2Bean.getBlockTitle());
		
		// Serialize the JSON string
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block2DBBean));
		
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * method to save SubBlock For SubBlock2 {@code List<AidBlock>}
	 * @param subBlockBean
	 * @param activeTab
	 * @param response
	 * @return  {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.SUB_BLOCK_EDIT_SAVE +"/BLOCK_2", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveSubBlockForSubBlock2 (@ModelAttribute("subBlockForm") JSONAIDSubBlockConfig subBlockBean, @RequestParam ("activeTab") String activeTab, HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		// Get the parent block this sub block belongs to
		AidBlock oneBlock = aidService.fetchOneBlock(subBlockBean.getParentBlockId(), true);
		JSONAIDBlock2Bean block2DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Based on the sub block identifier you update the appropriate sub-block of the block
		
		// Get the sub block from the DB bean
		JSONAIDSubBlockConfig dbSubBlock = block2DBBean.getSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier());
		// Update the db subblock based on the incoming values
		dbSubBlock = updateSubBlockBasedOnActiveTab(subBlockBean, dbSubBlock, activeTab);
		
		// Set the sub block back into the bean that is going to go back to the database
		block2DBBean.setSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier(), dbSubBlock);
		
		// Serialize and ready for insert
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block2DBBean));
		
		// Go for the save.
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * method to save Block3 {@code List<AidBlock>}
	 * @param block3Bean
	 * @param response
	 * @return
	 */
	@RequestMapping(value = AIDReportConfigurationController.BLOCKEDIT_SAVE+"/BLOCK_3", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveBlock3 (@ModelAttribute("blockEditForm") JSONAIDBlock3Bean block3Bean,  HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		AidBlock oneBlock = aidService.fetchOneBlock(block3Bean.getId(), true);
		JSONAIDBlock3Bean block3DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:		
		// Make the update to the object title only.
		block3DBBean.setBlockTitle(block3Bean.getBlockTitle());		
		// Serialize the JSON string
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block3DBBean));		
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Save SubBlock For SubBlock3
	 * @param subBlockBean
	 * @param activeTab
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.SUB_BLOCK_EDIT_SAVE +"/BLOCK_3", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveSubBlockForSubBlock3 (@ModelAttribute("subBlockForm") JSONAIDSubBlockConfig subBlockBean, @RequestParam ("activeTab") String activeTab, HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?		
		// Get the parent block this sub block belongs to
		AidBlock oneBlock = aidService.fetchOneBlock(subBlockBean.getParentBlockId(), true);
		JSONAIDBlock3Bean block3DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Based on the sub block identifier you update the appropriate sub-block of the block
		
		// Get the sub block from the DB bean
		JSONAIDSubBlockConfig dbSubBlock = block3DBBean.getSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier());
		// Update the db subblock based on the incoming values
		dbSubBlock = updateSubBlockBasedOnActiveTab(subBlockBean, dbSubBlock, activeTab);
		
		// Set the sub block back into the bean that is going to go back to the database
		block3DBBean.setSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier(), dbSubBlock);
		
		// Serialize and ready for insert
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block3DBBean));
		
		// Go for the save.
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * save Block4
	 * @param block4Bean
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.BLOCKEDIT_SAVE+"/BLOCK_4", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveBlock4 (@ModelAttribute("blockEditForm") JSONAIDBlock3Bean block4Bean,  HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		AidBlock oneBlock = aidService.fetchOneBlock(block4Bean.getId(), true);
		JSONAIDBlock4Bean block3DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Make the update to the object title only.
		block3DBBean.setBlockTitle(block4Bean.getBlockTitle());
		
		// Serialize the JSON string
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block3DBBean));
		
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Save SubBlock For SubBlock4.
	 * @param subBlockBean
	 * @param activeTab
	 * @param response
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = AIDReportConfigurationController.SUB_BLOCK_EDIT_SAVE +"/BLOCK_4", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public ModelAndView saveSubBlockForSubBlock4 (@ModelAttribute("subBlockForm") JSONAIDSubBlockConfig subBlockBean, @RequestParam ("activeTab") String activeTab, HttpServletResponse response){
		// TODO How do we ensure that the controller only takes forwarded requests ?
		
		// Get the parent block this sub block belongs to
		AidBlock oneBlock = aidService.fetchOneBlock(subBlockBean.getParentBlockId(), true);
		JSONAIDBlock4Bean block4DBBean = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock); 
		// Validation to be put in else throw him out:
		
		// Based on the sub block identifier you update the appropriate sub-block of the block
		
		// Get the sub block from the DB bean
		JSONAIDSubBlockConfig dbSubBlock = block4DBBean.getSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier());
		// Update the db subblock based on the incoming values
		dbSubBlock = updateSubBlockBasedOnActiveTab(subBlockBean, dbSubBlock, activeTab);
		
		// Set the sub block back into the bean that is going to go back to the database
		block4DBBean.setSubBlockFromIdentifier(subBlockBean.getSubBlockIdentifier(), dbSubBlock);
		
		// Serialize and ready for insert
		oneBlock.setBlockJsonconfig(Utility.convertObjectToJSONString(block4DBBean));
		
		// Go for the save.
		AidBlock [] arrayOfBlocks = {oneBlock};
		aidService.saveBlocks(Arrays.asList(arrayOfBlocks));
		
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, Boolean> jsonToReturn = new HashMap<>();
		jsonToReturn.put(CONFIG_UPDATED, true);
		
		return JSONView.render(jsonToReturn, response);
	}

	/**
	 * Updates the dbSubBlock based on the incoming form bean according to the active tab.
	 * It ensures that it updates the subblock completely.
	 * 
	 * @param subBlockBean
	 * @param dbSubBlock
	 * @param activeTab
	 */
	private JSONAIDSubBlockConfig updateSubBlockBasedOnActiveTab(JSONAIDSubBlockConfig subBlockBean, JSONAIDSubBlockConfig dbSubBlock, String activeTab) {
		dbSubBlock.setBlockTitle(subBlockBean.getBlockTitle());
		if (!StringUtils.isEmpty(activeTab)){
			SubBlockTabs activeTabFromScreen = SubBlockTabs.valueOf(activeTab);
			switch (activeTabFromScreen) {
			case selectQuestion:
				// Update for the question
				dbSubBlock.setQuestionId(subBlockBean.getQuestionId());
				dbSubBlock.setQuestionQEId(subBlockBean.getQuestionQEId());
				dbSubBlock.updateBeanForQuestion();
				break;
			case selectParameter:
				// Update for the Parameter
				dbSubBlock.setParamId(subBlockBean.getParamId());
				dbSubBlock.setParamQEId(subBlockBean.getParamQEId());
				dbSubBlock.updateBeanForParameter();
				break;
			case selectColumn:
				// Update for the Column
				dbSubBlock.setTemplateColumnId(subBlockBean.getTemplateColumnId());
				dbSubBlock.updateBeanForTemplateColumn();
				break;
				
			default:
				break;
			}
		}
		return dbSubBlock;
	}
}
