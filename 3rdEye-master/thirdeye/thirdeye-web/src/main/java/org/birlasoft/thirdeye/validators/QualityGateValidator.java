package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields,
 * logic of {@code validation} is here. It also checks for {@code unique} 
 * {@link QualityGate}
 * @author shaishav.dixit
 *
 */
@Component
public class QualityGateValidator implements Validator {

	private static final String QUALITY_GATE_NAME = "name";
	
	private Environment env;
	private QualityGateService assetQualityGateService;
	private CustomUserDetailsService customUserDetailsService;

	@Autowired
	public QualityGateValidator(Environment env,
			QualityGateService assetQualityGateService,
			CustomUserDetailsService customUserDetailsService) {
		this.env = env;
		this.assetQualityGateService = assetQualityGateService;
		this.customUserDetailsService = customUserDetailsService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return QualityGateBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		QualityGateBean qualityGateBean = (QualityGateBean) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, QUALITY_GATE_NAME, "error.quality.gate.name", env.getProperty("error.quality.gate.name"));
		QualityGate assetQualityGates = assetQualityGateService.findByNameAndWorkspace(qualityGateBean.getName(), customUserDetailsService.getActiveWorkSpaceForUser());
		if (assetQualityGates != null){
			errors.rejectValue(QUALITY_GATE_NAME ,"error.quality.gate.name.unique",env.getProperty("error.quality.gate.name.unique"));
		}
		if (qualityGateBean.getName().length() > 45) {
			errors.rejectValue(QUALITY_GATE_NAME ,"error.quality.gate.name.length",env.getProperty("error.quality.gate.name.length"));
			
		}
	}

}
