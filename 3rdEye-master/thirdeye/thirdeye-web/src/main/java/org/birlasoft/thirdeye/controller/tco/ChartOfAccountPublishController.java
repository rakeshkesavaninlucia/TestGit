package org.birlasoft.thirdeye.controller.tco;

import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for {@code pre}{@code publish} {@code chart of account} and {@code publish} {@code chart of account}.
 */
@Controller
@RequestMapping("/tco")
public class ChartOfAccountPublishController {

	private QuestionnaireService questionnaireService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private TcoResponseService tcoResponseService;
	@Autowired
	private ParameterService parameterService;
	
	/**
	 * Constructor to initialize services 
	 * @param questionnaireService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param parameterFunctionService
	 */
	@Autowired
	public ChartOfAccountPublishController(
			QuestionnaireService questionnaireService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionnaireService = questionnaireService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;

	}

	/**
	 * Create a {@code pre publish} {@code chart of account}
	 * @param chartOfAccountId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/prepublish", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String prePublishChartOfAccount(@PathVariable(value = "id") Integer chartOfAccountId, Model model) {

		Questionnaire coa = questionnaireService.findOneFullyLoaded(chartOfAccountId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		questionnaireQuestionService.refreshQuestionnaireQuestions(coa);
		
		if(coa.getQuestionnaireParameters().isEmpty()){
			List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(coa,true) ;
			// Get root parameters of QE
			List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
			model.addAttribute("error", 1);
			model.addAttribute("questionnaireId", chartOfAccountId);
			model.addAttribute("rootParameterList", listOfRootParameters);
			model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Cost Structure Selection for Chart of Account");
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
			return "questionnaire/questionnaireParameters";
		}		
		List<ChartOfAccountBean> listOfCoa = tcoResponseService.extractChartOfAccounts(coa);
		
		model.addAttribute("questionnaireId", chartOfAccountId);
		model.addAttribute("questionnaire", coa);
		model.addAttribute("listOfCoa", listOfCoa);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Publish Chart of Account");
		
		return "questionnaire/questionnairePrePublish";
	}

	/**
	 * Create a {@code publish} {@code chartOfAccount}
	 * @param chartOfAccountId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/publish", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String publishChartOfAccount(@PathVariable(value = "id") Integer chartOfAccountId) {
		
		Questionnaire coa = questionnaireService.findOne(chartOfAccountId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		
		coa.setStatus(QuestionnaireStatusType.PUBLISHED.name());
		
		questionnaireService.save(coa);
		
		return "redirect:/tco/list";
	}
}
