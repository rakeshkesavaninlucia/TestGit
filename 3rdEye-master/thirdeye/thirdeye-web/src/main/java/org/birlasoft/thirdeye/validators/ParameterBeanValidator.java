package org.birlasoft.thirdeye.validators;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class ParameterBeanValidator implements Validator {
	
	@Autowired
	private Environment env;

	@Autowired
	private ParameterService parameterService;
	
	@Autowired
	private HttpSession session;
	
	@Autowired
	private CustomUserDetailsService userDetailsService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ParameterBean.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
		ParameterBean parameterBean = (ParameterBean) target;
		
		if (parameterBean.getUniqueName() != null && parameterBean.getUniqueName().isEmpty()) {			  
		    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uniqueName", "error.coststructure.uniquename", env.getProperty("error.coststructure.uniquename"));
		} else if (parameterBean.getId() == null){		  
		      List<Parameter> parameters = parameterService.findByWorkspace(userDetailsService.getActiveWorkSpaceForUser());
		      for(Parameter param : parameters){
		    	  if(param.getUniqueName().trim().equalsIgnoreCase(parameterBean.getUniqueName().trim())){
		    		  errors.rejectValue("uniqueName", "error.coststructure.uniquename.alreadyexists",env.getProperty("error.coststructure.uniquename.alreadyexists"));
		    		  break;
		    	  }
		      }
		  }
		
		if (parameterBean.getDisplayName() != null && parameterBean.getDisplayName().isEmpty()) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "error.parameter.displayName", env.getProperty("error.parameter.displayName"));
		}
	}
	
	public void validate(Object target, Errors errors, Map<Integer, BigDecimal> mapOfQorPWeight) {
		  ParameterBean parameterBean = (ParameterBean) target;
		  
		  if (parameterBean.getParamType().equalsIgnoreCase("select")) {			  
			  errors.rejectValue("paramType", "error.parameter.type", env.getProperty("error.parameter.type"));
		  }
		  
		  if (parameterBean.getName() != null && parameterBean.getName().isEmpty()) {			  
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.parameter.name", env.getProperty("error.parameter.name"));
		  }else if(!parameterBean.getName().isEmpty() && (parameterBean.getWorkspaceId() == null || parameterBean.getWorkspaceId() == -1)){		  
		      List<Parameter> parameters = parameterService.findByWorkspaceIsNull();
		      for(Parameter param : parameters){
		    	  if(parameterBean.getId() == null && param.getUniqueName().trim().equalsIgnoreCase(parameterBean.getName().trim())){
		    		  errors.rejectValue("name", "error.parameter.name.unique",env.getProperty("error.parameter.name.unique"));
		    		  break;
		    	  }else if(parameterBean.getId() != null && !parameterBean.getId().equals(param.getId()) && param.getUniqueName().trim().equalsIgnoreCase(parameterBean.getName().trim())){
		    		  errors.rejectValue("name", "error.parameter.name.unique",env.getProperty("error.parameter.name.unique"));
		    		  break;
		    	  }
		      }
		  }else if(!parameterBean.getName().isEmpty() && parameterBean.getWorkspaceId() != null && parameterBean.getWorkspaceId() > 0){		  
		      List<Parameter> parameters = parameterService.findByWorkspace(userDetailsService.getActiveWorkSpaceForUser());
		      for(Parameter param : parameters){
		    	  if(parameterBean.getId() == null && param.getUniqueName().trim().equalsIgnoreCase(parameterBean.getName().trim().replaceAll(" +", " "))){
		    		  errors.rejectValue("name", "error.parameter.name.unique",env.getProperty("error.parameter.name.unique"));
		    		  break;
		    	  }else if(parameterBean.getId() != null && !parameterBean.getId().equals(param.getId()) && param.getUniqueName().trim().equalsIgnoreCase(parameterBean.getName().trim().replaceAll(" +", " "))){
		    		  errors.rejectValue("name", "error.parameter.name.unique",env.getProperty("error.parameter.name.unique"));
		    		  break;
		    	  }
		      }
		  }
		  
		  if (parameterBean.getDisplayName() != null && parameterBean.getDisplayName().isEmpty()) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "error.parameter.displayName", env.getProperty("error.parameter.displayName"));
		  }
		  
		  if (parameterBean.getDisplayName() != null && parameterBean.getDisplayName().length() > 45) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "error.parameter.name.length", env.getProperty("error.parameter.name.length"));
		  }
		  
		  if(!parameterBean.getParamType().equalsIgnoreCase("select") && mapOfQorPWeight != null && mapOfQorPWeight.size() == 0){
			  Set<Parameter> parameters = (HashSet<Parameter>) session.getAttribute("_selectedParameters");
			  Set<Question> questions = (HashSet<Question>) session.getAttribute("_selectedQuestions");
			  FunctionalCoverageParameterConfigBean fcConfigBean = (FunctionalCoverageParameterConfigBean) session.getAttribute("_fcParamConfigBean");
			  if(parameterBean.getParamType().equals(ParameterType.AP.name()) && parameters.size() == 0){
				  errors.rejectValue("error", "error.parameter.addchildparam",env.getProperty("error.parameter.addchildparam"));
			  }else if(parameterBean.getParamType().equals(ParameterType.LP.name()) && questions.size() == 0){
				  errors.rejectValue("error", "error.parameter.addquestions",env.getProperty("error.parameter.addquestions"));
			  }else if(parameterBean.getParamType().equals(ParameterType.FC.name()) && fcConfigBean != null && fcConfigBean.getFunctionalMapId() == null){
				  errors.rejectValue("error", "error.parameter.functionalcoverage",env.getProperty("error.parameter.functionalcoverage"));
			  }
		  }
		  
		  if(mapOfQorPWeight != null && mapOfQorPWeight.size() > 0){
		    BigDecimal addWeight = BigDecimal.valueOf(0.0);
		    boolean isWeightZero = false;
		  	for (BigDecimal weightValue : mapOfQorPWeight.values()) {
		  		if(weightValue.equals(BigDecimal.valueOf(0.0))){
		  			errors.rejectValue("error", "error.parameter.que.weight.notzero",env.getProperty("error.parameter.que.weight.notzero"));
		  			isWeightZero = true;
		  			break;
		  		}
		  		addWeight = addWeight.add(weightValue);
		  	}
		  	if(!isWeightZero && (!addWeight.equals(BigDecimal.valueOf(1.0)))){
		  		errors.rejectValue("error", "error.parameter.que.weight.equalone",env.getProperty("error.parameter.que.weight.equalone"));
		  	}
		  }
	}
}
