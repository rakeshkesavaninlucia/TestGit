package org.birlasoft.thirdeye.controller;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for save a {@code user} in {@code workspace}
 */
@Controller
@RequestMapping(value = "/workspace")
public class UserWorkspaceController {
	
	private WorkspaceService workSpaceService;
	private UserService userService;
	private UserWorkspaceService userWorkspaceService;

	/** Parameterized Constructor 
	 * @param workSpaceService
	 * @param userService
	 * @param userWorkspaceService
	 */
	@Autowired
	public UserWorkspaceController(WorkspaceService workSpaceService,
			UserService userService,
			UserWorkspaceService userWorkspaceService) {
		this.workSpaceService = workSpaceService;
		this.userService = userService;
		this.userWorkspaceService = userWorkspaceService;
}
	
	/** Method to save a user in workspace
	 * save {@code User} in {@code Workspace}  
	 * @param idOfworkspace
	 * @param userId
	 * @return String
	 */
	@RequestMapping(value="/user/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY'})")
	public String saveUserInWorkspace(@RequestParam(value = "workspaceId") Integer idOfworkspace, @RequestParam(value = "userId") Integer userId){
		User user = userService.findUserById(userId);
		
		// No need to use active workspace here
		Workspace workspace = workSpaceService.findOne(idOfworkspace);
		userWorkspaceService.save(userWorkspaceService.createNewUserWorkspaceObject(user, workspace));
		return "redirect:/workspace/view/" + workspace.getId();
	}
}
