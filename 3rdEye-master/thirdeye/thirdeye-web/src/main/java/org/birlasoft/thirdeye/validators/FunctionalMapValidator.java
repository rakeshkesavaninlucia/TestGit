package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * 
 * Functional Map Validator class
 *
 */
@Component
public class FunctionalMapValidator implements Validator {
	
	private static final String FUNCTIONAL_MAP_NAME = "name";
	private static final String FUNCTIONAL_MAP_BCM = "bcmId";
	private static final String FUNCTIONAL_MAP_ASSET_TEMPLATE = "assetTemplateId";
	private static final String FUNCTIONAL_MAP_QUESTION = "questionId";
	private static final String FUNCTIONAL_MAP_TYPE = "type";
	
	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return FunctionalMapBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		FunctionalMapBean functionalMapBean = (FunctionalMapBean) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FUNCTIONAL_MAP_NAME, "error.functionalMap.name", env.getProperty("error.functionalMap.name"));
		if(functionalMapBean.getBcmId() == null){
			errors.rejectValue(FUNCTIONAL_MAP_BCM, "error.functionalMap.bcm");
		}
		if(functionalMapBean.getAssetTemplateId() == null){
			errors.rejectValue(FUNCTIONAL_MAP_ASSET_TEMPLATE, "error.functionalMap.assetTemplate");
		}
		if(functionalMapBean.getQuestionId() == null){
			errors.rejectValue(FUNCTIONAL_MAP_QUESTION, "error.functionalMap.question");
		}
		if(functionalMapBean.getType() == null){
			errors.rejectValue(FUNCTIONAL_MAP_TYPE, "error.functionalMap.type");
		}

	}

}
