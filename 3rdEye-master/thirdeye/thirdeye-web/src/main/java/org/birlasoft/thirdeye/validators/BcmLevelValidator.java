package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary} fields , logic of
 * {@code validation} is here.
 */
@Component
public class BcmLevelValidator implements Validator {

	private static final String BCM_LEVEL_NAME = "bcmLevelName";
	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return BcmLevelBean.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BcmLevelBean newBcmLevelBean = (BcmLevelBean) target;

		if (StringUtils.isEmpty(newBcmLevelBean.getBcmLevelName())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_LEVEL_NAME, "bcmlevel.errors.name.empty");
		} else if (firstCondition(newBcmLevelBean) || secondCondition(newBcmLevelBean) && StringUtils.isEmpty(newBcmLevelBean.getCategory())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "bcmlevel.errors.category.empty");
		}
	}

	// Bcm level validation
	public void validateBcmLevel(Object target, List<BcmLevel> listOfBcmLevel, Errors errors) {
		BcmLevelBean newBcmLevelBean = (BcmLevelBean) target;

		if (StringUtils.isEmpty(newBcmLevelBean.getBcmLevelName())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_LEVEL_NAME, "bcmlevel.errors.name.empty");
		} else if (firstCondition(newBcmLevelBean) || secondCondition(newBcmLevelBean) && StringUtils.isEmpty(newBcmLevelBean.getCategory())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "bcmlevel.errors.category.empty");
		}
		if (newBcmLevelBean.getBcmLevelName() != null && newBcmLevelBean.getBcmLevelName().length() > 45) {
			errors.rejectValue(BCM_LEVEL_NAME, "error.bcmlevel.name.length", env.getProperty("error.questionnaire.name.length"));
		}

		for (BcmLevel oneLevel : listOfBcmLevel) {
			if (oneLevel.getBcmLevelName().equals(newBcmLevelBean.getBcmLevelName()) && !oneLevel.getId().equals(newBcmLevelBean.getId())) {
				errors.rejectValue(BCM_LEVEL_NAME, "bcm.error.bcmlevel.name.unique", env.getProperty("bcm.error.bcmlevel.name.unique"));
				break;
			}
		}
	}

	private boolean secondCondition(BcmLevelBean newBcmLevelBean) {
		return newBcmLevelBean.getId() != null && newBcmLevelBean.getLevelNumber() == 2;
	}

	private boolean firstCondition(BcmLevelBean newBcmLevelBean) {
		return newBcmLevelBean.getId() == null && newBcmLevelBean.getLevelNumber() == 1;
	}
}
