package org.birlasoft.thirdeye.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.format.Formatter;

/**
 * {@code Formatter} , which allows you to define custom parse and print capabilities for just about any type
 */
public class WorkspaceFormatter implements Formatter<Workspace> {

	@Override
	public String print(Workspace workspace, Locale locale) {
		return workspace.getId().toString();
	}

	@Override
	public Workspace parse(String id, Locale locale) throws ParseException {
		Workspace workspace = new Workspace();
		workspace.setId(Integer.parseInt(id));
		return workspace;
	}

}
