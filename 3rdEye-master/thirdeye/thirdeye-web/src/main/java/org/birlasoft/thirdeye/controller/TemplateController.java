package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.asset.AssetBarChartWrapper;
import org.birlasoft.thirdeye.beans.asset.AssetChartRequestParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetParameterHealthBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.beans.tco.TCOSunburstWrapperBean;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetParameterHealthService;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DataCompletnessService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.AssetValidation;
import org.birlasoft.thirdeye.validators.TemplateColumnValidator;
import org.birlasoft.thirdeye.validators.TemplateValidator;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for view {@code template} , create a new {@code template} {@code column} row , edit a {@code column} row
 * save {@code column} row , create new {@code template} , save {@code template} , list od all {@code asset template}
 * view {@code template} {@code data}
 */
@Controller
@RequestMapping(value = "/templates")
public class TemplateController {

	private static final String USER = "User ";
	private static final String ASSET_TEMPLATE_CREATE = "assetTemplate/createTemplate";
	private static final String ONE_ROW = "oneRow";
	private static final String SESSION_ASSET_TEMPLATE = "_currentAssetTemplate";

	private static Logger logger = LoggerFactory.getLogger(TemplateController.class);

	private AssetTypeService assetTypeService;
	private AssetTemplateService assetTemplateService;
	private AssetTemplateColumnService assetTemplateColumnService;
	private AssetService assetService;
	private TemplateValidator templateValidator;
	private TemplateColumnValidator templateColumnValidator;
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private WorkspaceService workspaceService;	
	@Autowired
	private AssetParameterHealthService assetParamHealthService;
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	@Autowired
	private RelationshipTemplateService relationshipTemplateService;
	@Autowired
	private DataCompletnessService dataCompletnessService;
	@Autowired
	private AssetValidation assetValidation;
	
	/**
	 * Constructor autowiring of dependencies
	 * @param assetTypeService
	 * @param assetTemplateService
	 * @param assetTemplateColumnService
	 * @param assetService
	 * @param templateValidator
	 * @param templateColumnValidator
	 * @param customUserDetailsService
	 */
	@Autowired
	public TemplateController(AssetTypeService assetTypeService,
			AssetTemplateService assetTemplateService,
			AssetTemplateColumnService assetTemplateColumnService,
			AssetService assetService,
			TemplateValidator templateValidator,
			TemplateColumnValidator templateColumnValidator,
			CustomUserDetailsService customUserDetailsService) {
		this.assetTypeService = assetTypeService;
		this.assetTemplateService = assetTemplateService;
		this.assetTemplateColumnService = assetTemplateColumnService;
		this.assetService = assetService;
		this.templateValidator = templateValidator;
		this.templateColumnValidator = templateColumnValidator;
		this.customUserDetailsService = customUserDetailsService;
	}

	/**
	 * View template by template Id.
	 * @param model
	 * @param idOfColumn
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_VIEW'})")
	public String viewTemplate(Model model, @PathVariable(value = "id") Integer idOfColumn, HttpSession session) {
		   
		// Validate if the logged in user has the correct privilege (already
		AssetTemplate oneTemplateForViewing = assetTemplateService.findOne(idOfColumn);
		// Check if the user can access this template.
		workspaceSecurityService.authorizeTemplateAccess(oneTemplateForViewing);
		
		// Check if the user can access relationship template.
		assetTemplateService.checkForViewTemplateColumn(oneTemplateForViewing);
		
		oneTemplateForViewing.setAssetTemplateColumns(assetTemplateColumnService.sortTemplateColumnsBySequenceNumber(oneTemplateForViewing.getAssetTemplateColumns()));
		session.setAttribute(SESSION_ASSET_TEMPLATE, oneTemplateForViewing);
		model.addAttribute("oneAssetTemplate", oneTemplateForViewing);
		
		return "assetTemplate/viewTemplate";
	}

	/**
	 * Create new template column. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/newRow", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String createTemplateColumnRow(Model model) {
		model.addAttribute("assetTemplateColBean", new AssetTemplateColumn());
		return "assetTemplate/editTemplateFragments :: createRow";
	}

	/**
	 * Edit asset template column.
	 * @param model
	 * @param idOfColumn
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/editRow/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String editAssetTemplateColumn(Model model, @PathVariable(value = "id") Integer idOfColumn, HttpSession session) {
		// Validate the incoming parameters
		AssetTemplate cachedTemplate = retrieveCachedAssetTemplate(session);
		
		// Check if the user can access this template.
		workspaceSecurityService.authorizeTemplateAccess(cachedTemplate);
		
		// pick up and return the row in the editable form
		for (AssetTemplateColumn oneCol : cachedTemplate.getAssetTemplateColumns()) {
			if (oneCol.getId().equals(idOfColumn)) {
				setTemplateColumnInModel(model, oneCol);
				break;
			}
		}
		model.addAttribute("assetTemplateColBean", new AssetTemplateColumn());
		return "assetTemplate/editTemplateFragments :: editTemplate";
	}

	/**
	 * Save asset template column and temporary save in cache/session.
	 * @param assetTemplateColumn
	 * @param result
	 * @param model
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/saveRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String saveAssetTemplateColumn(@ModelAttribute("assetTemplateColBean") AssetTemplateColumn assetTemplateColumn, BindingResult result, Model model, HttpSession session) {
		AssetTemplate cachedTemplate = retrieveCachedAssetTemplate(session);
		
		workspaceSecurityService.authorizeTemplateAccess(cachedTemplate);
		
		assetTemplateColumn.setAssetTemplate(cachedTemplate);
		templateColumnValidator.validate(assetTemplateColumn, result);
		if (result.hasErrors()) {
			return handleErrorOnTemplateColumnSave(assetTemplateColumn, model, cachedTemplate);
		}

		User currentUser = customUserDetailsService.getCurrentUser();
		// validate the user can edit the object

		// validate the object (should not have the same column name)
		if (cachedTemplate != null) {
			if (assetTemplateColumn.getId() != null) {
				cachedTemplate = updateTemplateColumn(assetTemplateColumn, cachedTemplate, currentUser);
				updateSessionCache(session, cachedTemplate);
				if(logger.isInfoEnabled()) {
					logger.info(USER + currentUser.getId() + " has created a new template column with column name "+assetTemplateColumn.getAssetTemplateColName());//Added by dhruv.sood
				}
			} else {
				// add a row
				// Go and save
				cachedTemplate.getAssetTemplateColumns().add(assetTemplateColumnService.createAssetTemplateColumnObject(assetTemplateColumn, cachedTemplate, currentUser));
				cachedTemplate = assetTemplateService.save(cachedTemplate);
				updateSessionCache(session, cachedTemplate);
				if(logger.isInfoEnabled()) {
					logger.info(USER + currentUser.getId() + " has updated the template column with column name "+assetTemplateColumn.getAssetTemplateColName());//Added by dhruv.sood
				}
			}
		} else {
			// we should return an error response

		}

		// Return in Model
		prepareTemplatecolumnForModel(assetTemplateColumn, model, cachedTemplate);

		return "assetTemplate/editTemplateFragments :: viewOnlyTemplate";
	}

	private void prepareTemplatecolumnForModel(AssetTemplateColumn assetTemplateColumn, Model model,
			AssetTemplate cachedTemplate) {
		if(cachedTemplate != null){			
			for (AssetTemplateColumn oneCol : cachedTemplate.getAssetTemplateColumns()) {
				if (oneCol.getAssetTemplateColName().equalsIgnoreCase(assetTemplateColumn.getAssetTemplateColName())) {
					setTemplateColumnInModel(model, oneCol);
					model.addAttribute("assetTypeName", cachedTemplate.getAssetType().getAssetTypeName());
					break;
				}
			}
		}
	}

	/**
	 * @param assetTemplateColumn
	 * @param model
	 * @param cachedTemplate
	 * @return
	 */
	private String handleErrorOnTemplateColumnSave(AssetTemplateColumn assetTemplateColumn, Model model,
			AssetTemplate cachedTemplate) {
		if(assetTemplateColumn.getId() == null){
			model.addAttribute(ONE_ROW, assetTemplateColumn);
			return "assetTemplate/editTemplateFragments :: createRow";
		}else{
			// pick up and return the row in the editable form
			for (AssetTemplateColumn oneCol : cachedTemplate.getAssetTemplateColumns()) {
				if (oneCol.getId().equals(assetTemplateColumn.getId())) {
					assetTemplateColumn.setDataType(oneCol.getDataType());
					model.addAttribute(ONE_ROW, assetTemplateColumn);
					break;
				}
			}
			return "assetTemplate/editTemplateFragments :: editTemplate";
		}
	}

	/**
	 * Update the template column which has the same ID.
	 * @param assetTemplateColumn
	 * @param cachedTemplate
	 * @param currentUser
	 * @return
	 */
	private AssetTemplate updateTemplateColumn(	AssetTemplateColumn assetTemplateColumn, AssetTemplate cachedTemplate, 
			User currentUser) {
		// check if row exists then update
		// Only pick up certain fields.

		// Update the column which has the same ID

		// pick up and return the row in the editable form
		for (AssetTemplateColumn oneCol : cachedTemplate.getAssetTemplateColumns()) {
			if (oneCol.getId().equals(assetTemplateColumn.getId())) {
				oneCol.setAssetTemplateColName(assetTemplateColumn.getAssetTemplateColName());
				oneCol.setLength(assetTemplateColumn.getLength());
				oneCol.setMandatory(assetTemplateColumn.isMandatory());
				oneCol.setFilterable(assetTemplateColumn.isFilterable());
				oneCol.setUpdatedDate(new Date());
				oneCol.setUserByUpdatedBy(currentUser);
				break;
			}
		}

		return assetTemplateService.save(cachedTemplate);
	}

	/**
	 * Retrieve asset template from cache/session.
	 * @param session
	 * @return {@code AssetTemplate}
	 */
	private AssetTemplate retrieveCachedAssetTemplate(HttpSession session) {
		return (AssetTemplate) session.getAttribute(SESSION_ASSET_TEMPLATE);
	}

	/**
	 * Update cache/session and save.
	 * @param session
	 * @param cachedTemplate
	 */
	private void updateSessionCache(HttpSession session, AssetTemplate cachedTemplate) {
		session.setAttribute(SESSION_ASSET_TEMPLATE, assetTemplateService.save(cachedTemplate));
	}

	/**
	 * Set template column data in model.
	 * @param model
	 * @param oneCol
	 */
	private void setTemplateColumnInModel(Model model, AssetTemplateColumn oneCol) {
		model.addAttribute("colid", oneCol.getId());
		model.addAttribute("assetTemplateColName", oneCol.getAssetTemplateColName());
		model.addAttribute("dataType", oneCol.getDataType());
		model.addAttribute("length", oneCol.getLength());
		model.addAttribute("mandatory", oneCol.isMandatory());
		model.addAttribute("filterable", oneCol.isFilterable());
		model.addAttribute("sequenceNumber", oneCol.getSequenceNumber());
		model.addAttribute(ONE_ROW, oneCol);
	}

	/**
	 * List of all asset template.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_VIEW'})")
	public String getAllAssetTemplates(Model model) {

		// List those templates which the user has access to in the active
		// workspace
		List<AssetTemplateBean> listOfAssetTemplateBeans = assetTemplateService.getListOfTemplateBeanWithHasColumn();

		Map<AssetTemplateBean, Boolean> assetTemplateHashMap = new HashMap<>();
		for (AssetTemplateBean assetTemplateBean : listOfAssetTemplateBeans) {
			if (assetTemplateService.isTemplateExpandable(assetTemplateBean)) {
				assetTemplateHashMap.put(assetTemplateBean, true);
			} else
				assetTemplateHashMap.put(assetTemplateBean, false);
		}

		model.addAttribute("assettemplateHashMap", assetTemplateHashMap);
		return "assetTemplate/listTemplates";
	}

	/**
	 * For the rest of the controller you should handle the ajax request (post)
	 * http://www.thymeleaf.org/doc/articles/layouts.html Fragment inclusion
	 * from Spring @Controller
	 * 
	 * this would allow response of specific fragments.
	 * 
	 * All you need to see is if we have to have a form object somehow within a
	 * row. Then put some plugin for ajax form submit that would easily allow
	 * you to push a row's data into the form and return with validations if
	 * any. The user can undo - cache the row in JS.
	 * 
	 * Pick up the dirty row and submit. form can have hidden params for the row
	 * id. You should check security for whether the last object access (on
	 * screen) has a row id of the particular asset template name as well as if
	 * the user has access to the template.
	 * Create template to set the data in template form.
	 * @param model
	 * @return {@link String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String createTemplate(Model model) {
		setModelAttributesForTemplate(model, new AssetTemplateBean());
		return ASSET_TEMPLATE_CREATE;
	}

	/**
	 * Save {@code Template} data.
	 * @param assetTemplateBean
	 * @param result
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String saveTemplate(@ModelAttribute("assetTemplateBean") AssetTemplateBean assetTemplateBean,BindingResult result, Model model) {
		templateValidator.validate(assetTemplateBean, result);
		if (result.hasErrors()) {
			handleErrorsOnTemplateSave(assetTemplateBean, model);
			return ASSET_TEMPLATE_CREATE;
		}

		User currentUser = customUserDetailsService.getCurrentUser();
		if(assetTemplateBean.getId() == null){
			AssetTemplate assetTemplate = assetTemplateService.save(assetTemplateService.createNewAssetTemplateObject(assetTemplateBean, currentUser));
			if(assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
				relationshipTemplateService.saveOrUpdate(relationshipTemplateService.createNewRelationshipTemplateObject(assetTemplateBean, assetTemplate));
			}
			if(logger.isInfoEnabled()) {				
				logger.info(USER + currentUser.getId() + " has created a new template with template name "+assetTemplateBean.getAssetTemplateName());//Added by dhruv.sood
			}
		}else{
			AssetTemplate assetTemplate = assetTemplateService.save(assetTemplateService.updateAssetTemplateObject(assetTemplateBean, currentUser));
			if(assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name()) && assetTemplateBean.getRelationshipTemplateId() != null && assetTemplateBean.getRelationshipTemplateId() > 0){
				relationshipTemplateService.saveOrUpdate(relationshipTemplateService.updateRelationshipTemplateObject(assetTemplateBean));
			}
			if(logger.isInfoEnabled()) {				
				logger.info(USER + currentUser.getId() + " has update the template with template name "+assetTemplateBean.getAssetTemplateName());//Added by dhruv.sood
			}
		}
		return "redirect:/templates/list";
	}

	/**
	 * Handle errors while saving asset template
	 * @param assetTemplateBean
	 * @param model
	 */
	private void handleErrorsOnTemplateSave(AssetTemplateBean assetTemplateBean, Model model) {
		if(assetTemplateBean.getId() != null){
			AssetType assetType = assetTypeService.findOne(assetTemplateBean.getAssetTypeId());
			assetTemplateBean.setAssetType(assetType);
		}
		setModelAttributesForTemplate(model, assetTemplateBean);
		if(assetTemplateBean.getAssetTypeId() > 0  && assetTypeService.findOne(assetTemplateBean.getAssetTypeId()).getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			setModelAttributesForRelationshipTemplate(model);
		}
	}

	/**
	 * Set model attributes for relationship template.
	 * @param model
	 */
	private void setModelAttributesForRelationshipTemplate(Model model) {
		AssetTypes[] assetTypes = new AssetTypes[]{AssetTypes.RELATIONSHIP};
		model.addAttribute("listOfAssetType", assetTypeService.extractAssetTypes(assetTypes));
		model.addAttribute("listOfRType", relationshipTypeService.findByWorkspaceIsNullOrWorkspace(customUserDetailsService.getActiveWorkSpaceForUser()));
		model.addAttribute("isRelationshipAssetType", true);
	}

	/**
	 * Set model attributes for asset template.
	 * @param model
	 * @param assetTemplateBean
	 */
	private void setModelAttributesForTemplate(Model model, AssetTemplateBean assetTemplateBean) {
		model.addAttribute("assetTemplateBean", assetTemplateBean);
		model.addAttribute("userWorkspaceList", workspaceService.getUserWorkspaceList(customUserDetailsService.getCurrentUser()));
		model.addAttribute("assetTypeList", assetTypeService.findAll());
	}

	/**
	 * Populate the Asset template form dynamically based on saved data.
	 * @param model
	 * @param idOfColumn
	 * @return {@code String}
	 */
	@RequestMapping(value = "/populateTemplateForm/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String populateTemplateForm(Model model, @PathVariable(value = "id") Integer idOfColumn) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(idOfColumn);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		
		assetTemplate.setAssetTemplateColumns(assetTemplateColumnService.sortTemplateColumnsBySequenceNumber(assetTemplate.getAssetTemplateColumns()));
		model.addAttribute("assetTemplateModel", assetTemplate);
		
		populateRelationshipRelatedData(assetTemplate, model);
		
		return "assetTemplate/populateTemplateForm";
	}

	/**
	 * Populate asset entry form for relationship template.
	 * @param assetTemplate
	 * @param model
	 */
	private void populateRelationshipRelatedData(AssetTemplate assetTemplate, Model model) {
		if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			
			List<RelationshipTemplate> listOfRelationshipTemplate = relationshipTemplateService.findByAssetTemplate(assetTemplate);
			
			if(listOfRelationshipTemplate.size() == 1){
				RelationshipTemplate relationshipTemplate = listOfRelationshipTemplate.get(0);
				List<Workspace> listOfWorkspace = new ArrayList<>();
				listOfWorkspace.add(customUserDetailsService.getActiveWorkSpaceForUser());
				List<AssetBean> listOfParentAssets = assetService.findAllAssetsInWorkspaceByAssetType(listOfWorkspace, assetTypeService.findOne(relationshipTemplate.getAssetTypeByParentAssetTypeId().getId()));
				List<AssetBean> listOfChildAssets = assetService.findAllAssetsInWorkspaceByAssetType(listOfWorkspace, assetTypeService.findOne(relationshipTemplate.getAssetTypeByChildAssetTypeId().getId()));
				// Dump all the assets in the workspace into the model
				model.addAttribute("listOfParentAssets", listOfParentAssets);
				model.addAttribute("listOfChildAssets", listOfChildAssets);
			}
		}
	}

	/**
	 * Save {@code Asset} object.
	 * @param model
	 * @param request
	 * @param templateId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/form/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String saveTemplateForm(HttpServletRequest request, @RequestParam(Constants.TEMPLATE_ID) Integer templateId, Model model) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(templateId);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		
		// TODO we have to put validations - you would need to validate the case for the interfaces
		// also.
		
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		
		Iterator<String> iter = requestParameterNames.iterator();
		while(iter.hasNext()){
		    if(iter.next().equalsIgnoreCase(Constants.REQUEST_PARAM_CSRF))
		        iter.remove();
		}
		
		Asset asset = createAssetObject(request, assetTemplate, requestParameterNames);	
		//validation for same asset name
				if(assetValidation.isAssetDuplicate(asset)){
					 
					  assetTemplate.setAssetTemplateColumns(assetTemplateColumnService.sortTemplateColumnsBySequenceNumber(assetTemplate.getAssetTemplateColumns()));
						model.addAttribute("assetTemplateModel", assetTemplate);
						model.addAttribute("error",1);
						populateRelationshipRelatedData(assetTemplate, model);
						
						return "assetTemplate/populateTemplateForm";
				  }
	
		Asset savedAsset = assetService.save(asset);
		
		// Go and save relationship related data if required
		if (savedAsset.getId() > 0 && assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			try{
			Asset parentAsset = assetService.findOne(Integer.parseInt(StringUtils.trimAllWhitespace(request.getParameter(Constants.PARENT_ASSET))));
			Asset childAsset = assetService.findOne(Integer.parseInt(StringUtils.trimAllWhitespace(request.getParameter(Constants.CHILD_ASSET))));
			
			relationshipTemplateService.createNewRelationshipAssetDataObject(savedAsset, parentAsset, childAsset);
			} catch (NumberFormatException nfe){
				logger.error("Unable to save relationship related data", nfe);
			} catch (IllegalArgumentException e) {
				logger.error("Unable to retrieve information for the relationship", e);
			}
			
		}
				
		return "redirect:/templates/populateTemplateForm/"+templateId;
	}

	/**
	 * View asset template data by template id. 
	 * @param model
	 * @param idOfColumn
	 * @return {@code String}
	 */
	@RequestMapping(value = "/data/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String viewTemplateData(Model model, @PathVariable(value = "id") Integer idOfColumn) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(idOfColumn);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		List<AssetTemplateColumn> assetTemplateColumns = assetTemplateColumnService.getSortedListOfTemplateColumn(assetTemplate.getAssetTemplateColumns());
		Set<Asset> assets = assetService.findByAssetTemplate(assetTemplate);
		// fetch relationships between assets.
		Map<Integer, RelationshipAssetDataBean> mapOfRelationshipAssetDataBean = assetService.getAssetsRelationship(assetTemplate, assets);
		
		model.addAttribute("mapOfRelationshipAssetDataBean", mapOfRelationshipAssetDataBean);
		model.addAttribute("assets", assetService.getListOfAssetBean(assets));
		model.addAttribute("assetTemplate", assetTemplate);
		model.addAttribute("templateData", assetTemplateColumns);
		return "assetTemplate/viewTemplateData";
	}

	/**
	 * Create new {@code Asset} Object.
	 * @param request
	 * @param templateId
	 * @param requestParameterNames
	 * @return {@code Asset}
	 */
	private Asset createAssetObject(HttpServletRequest request, AssetTemplate assetTemplate, List<String> requestParameterNames) {
		Asset asset = assetService.createNewAssetObject(assetTemplate, customUserDetailsService.getCurrentUser());
		// get tenant id,asset type,asset template id,asset name for uid
		String assetType = assetTemplate.getAssetType().getAssetTypeName();
		Integer assetTemplateId = assetTemplate.getId();
		String tenantId = currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		// check relationship template has template column or not.
		boolean hasTemplateColumn = relationshipTemplateService.isHasTemplateColumn(assetTemplate);
		if(hasTemplateColumn){
			asset.setAssetDatas(createSetOfAssetData(request, requestParameterNames, asset));
		}
		
		String assetName = "";
		if(assetType.equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			if(hasTemplateColumn){
				assetName = assetService.getAssetNameForUID(asset);
			} 
			if(StringUtils.trimAllWhitespace(assetName).isEmpty() || !hasTemplateColumn){
				assetName = assetService.getAssetNameForUID(assetService.findOne(Integer.parseInt(StringUtils.trimAllWhitespace(request.getParameter(Constants.PARENT_ASSET))))) + "_" + assetService.getAssetNameForUID(assetService.findOne(Integer.parseInt(StringUtils.trimAllWhitespace(request.getParameter(Constants.CHILD_ASSET)))));
			}
		} else{
			assetName = assetService.getAssetNameForUID(asset);
		}

		// Generate UId for a asset
		asset.setUid(Utility.generateUid(tenantId,assetType, assetTemplateId, assetName));
		return asset;
	}

	/**
	 * Create set of asset data.
	 * @param request
	 * @param requestParameterNames
	 * @param asset
	 * @return {@code Set<AssetData>}
	 */
	private Set<AssetData> createSetOfAssetData(HttpServletRequest request, List<String> requestParameterNames, Asset asset) {
		Set<AssetData> assetDatas = new HashSet<>();
		for (String parameterName : requestParameterNames) {
			if (!parameterName.equalsIgnoreCase(Constants.TEMPLATE_ID) && !parameterName.equalsIgnoreCase(Constants.PARENT_ASSET) && !parameterName.equalsIgnoreCase(Constants.CHILD_ASSET)) {
				AssetData newData = assetService.createAssetDataObject(request.getParameter(parameterName), parameterName, asset);
				if (newData != null){
					assetDatas.add(newData);
				}
		}
		}
		return assetDatas;
	}

	
	/**
	 * Edit asset template
	 * @param model
	 * @param idOfTemplate
	 * @return
	 */
	@RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String editTemplate(Model model, @PathVariable(value = "id") Integer idOfTemplate) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(idOfTemplate);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		
		AssetTemplateBean assetTemplateBean = new AssetTemplateBean(assetTemplate);
		if(assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			setModelAttributesForRelationshipTemplate(model);
			assetTemplateBean = relationshipTemplateService.setRelationshipIntoBean(assetTemplate, assetTemplateBean);
		}
		setModelAttributesForTemplate(model, assetTemplateBean);
		
		return ASSET_TEMPLATE_CREATE;
	}

	/**
	 * Delete asset template column
	 * @param idOfTemplateColumn
	 * @param session
	 */
	@RequestMapping(value = "/columns/deleteRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteTemplateColumn(@RequestParam(value="id") Integer idOfTemplateColumn, HttpSession session) {
		assetTemplateColumnService.deleteAssetData(idOfTemplateColumn);
		AssetTemplateColumn assetTemplateColumn = assetTemplateColumnService.findOneLoadedAssetTemplate(idOfTemplateColumn);
		assetTemplateColumnService.delete(assetTemplateColumn);
		List<AssetTemplateColumn> assetTemplateColumns = assetTemplateColumnService.findByAssetTemplate(assetTemplateColumn.getAssetTemplate());
		assetTemplateColumnService.updateSequenceNumber(assetTemplateColumns);
		AssetTemplate cachedAssetTemplate = retrieveCachedAssetTemplate(session);
		cachedAssetTemplate.setAssetTemplateColumns(new HashSet<AssetTemplateColumn>(assetTemplateColumns));
		updateSessionCache(session, cachedAssetTemplate);
	}
	
	/**
	 * update the asset template column sequence
	 * @param displaySeqAsStrings
	 * @param result
	 * @param model
	 * @param session
	 */
	@RequestMapping(value = "/columns/updateSequence", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateAssetTemplateColumnSequence(@ModelAttribute("sequence") List<String> displaySeqAsStrings, HttpSession session) {
		AssetTemplate cachedTemplate = retrieveCachedAssetTemplate(session);
		
		workspaceSecurityService.authorizeTemplateAccess(cachedTemplate);
		
		Map<Integer, Integer> displaySequenceMap = new HashMap<>();
		for (short i = 0; i < displaySeqAsStrings.size(); i++) {
			displaySequenceMap.put(Integer.valueOf(displaySeqAsStrings.get(i)), (int) (i + 1));
		}
		
		if (cachedTemplate != null) {
			for (AssetTemplateColumn oneCol : cachedTemplate.getAssetTemplateColumns()) {
				if(displaySequenceMap.get(oneCol.getId()) != null){
					oneCol.setSequenceNumber(displaySequenceMap.get(oneCol.getId()));
				}
			}
		}
		
		cachedTemplate = assetTemplateService.save(cachedTemplate);
		updateSessionCache(session, cachedTemplate);
	}
	
	/**
	 * Get the complete asset view. Asset details, data completeness, relationships.
	 * @param model
	 * @param idOfAsset
	 * @return
	 */
	@RequestMapping(value = "/asset/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String viewAssetData(Model model, @PathVariable(value = "id") Integer idOfAsset) {
		Asset asset = assetService.findOne(idOfAsset);
		
		
		//List Of Asset
		List<AssetBean> listOfasset = getListOfAssetforDropDown(asset);
		
		List<RelationshipAssetDataBean> listOfRelationships = relationshipTemplateService.getRootRelationshipsOfAsset(asset);
		//get the list of bean for asset parameter health
		List<AssetParameterHealthBean> assetParamHealthBeanList = assetParamHealthService.getAssetViewHealthParameter(asset);
		assetParamHealthBeanList.sort((obj1, obj2) -> obj1.getParameterValue().compareTo(obj2.getParameterValue()));
	    Collections.reverse(assetParamHealthBeanList);
	    
	    getDetailsForDataCompletenessView(model, asset);
		model.addAttribute("asset", new AssetBean(asset));
		model.addAttribute("listofAsset", listOfasset);
		model.addAttribute("relationshipList", listOfRelationships);
		model.addAttribute("assetTemplateId", asset.getAssetTemplate().getId());
		model.addAttribute("assetViewHealthParameterList",assetParamHealthBeanList);		
		return "assetTemplate/viewAsset";
	}

	/**
	 * @param asset
	 * @return list of asset for drop down in view asset page
	 */
	private List<AssetBean> getListOfAssetforDropDown(Asset asset) {
		//get list of asset for drop down
		AssetTemplate assetTemplate = asset.getAssetTemplate();
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		Set<Asset> assets = assetService.findByAssetTemplate(assetTemplate);
		List<AssetBean> listOfasset =  assetService.getListOfAssetBean(assets);
		listOfasset.sort((asset1,asset2)->asset1.getShortName().compareTo(asset2.getShortName()));
		return listOfasset;
	}

	/**
	 * Get details for data completeness view.</br>
	 * Get no of Questions for the Asset and no of questions responded
	 * @param model
	 * @param asset
	 */
	private void getDetailsForDataCompletenessView(Model model, Asset asset) {
		int noOfResponses = 0;
		Set<QuestionnaireQuestion> setOfQuestionnaireQuestions = dataCompletnessService.getQuestionsForAsset(asset);
		//Check if setOfQQ is empty
		if(!setOfQuestionnaireQuestions.isEmpty()){
			noOfResponses = dataCompletnessService.getNumberOfQuestionsResponded(setOfQuestionnaireQuestions);
		}

		model.addAttribute("NumberOfQuestions", setOfQuestionnaireQuestions.size());
		model.addAttribute("NumberOfResponses", noOfResponses);
		model.addAttribute("completionPercentage", dataCompletnessService.getDataCompletionViewForAsset(setOfQuestionnaireQuestions.size(), noOfResponses));
	}
	
	/**
	 * Delete AssetTemplate after checking if template is used
	 * @param idOfTemplate
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteTemplate(@RequestParam(value = "templateId") Integer idOfTemplate){
		AssetTemplate templateToDelete = assetTemplateService.findOne(idOfTemplate);
		// Check if the user can access this template.
		workspaceSecurityService.authorizeTemplateAccess(templateToDelete);

		if (!assetTemplateService.isTemplateExpandable(templateToDelete)) {
			if(templateToDelete.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
				relationshipTemplateService.deleteInBatch(relationshipTemplateService.findByAssetTemplate(templateToDelete));
			}
			List<AssetTemplateColumn> listOfAssetTemplateColumnsToBeDeleted = assetTemplateColumnService.findByAssetTemplate(templateToDelete);
			assetTemplateColumnService.deleteInBatch(listOfAssetTemplateColumnsToBeDeleted);
			assetTemplateService.deleteByAssetTemplateId(templateToDelete);
		}
	}
	
	/**
	 * edit AssetTemplate after checking if template is used
	 * @param idOfTemplate
	 */
	@RequestMapping(value = "/getassetdetails/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public String getAssetDetails(Model model, @PathVariable(value = "id") Integer idOfAsset){
		Asset asset = assetService.findOne(idOfAsset);
		model.addAttribute("asset", new AssetBean(asset));
	return "assetTemplate/assetFragment::assetinformation";
	}
	
	/**
	 * edit AssetTemplate after checking if template is used
	 * @param idOfTemplate
	 */
	@RequestMapping(value = "/editAssetDetails/{id}", method = RequestMethod.GET)
	//@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public String editAssetDetails(Model model, @PathVariable(value = "id") Integer idOfAsset){

		Asset asset = assetService.findOne(idOfAsset);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(asset.getAssetTemplate());
		Map<Integer,String> mapOfColIdAndData =	new HashMap<>();
    	asset.getAssetDatas().forEach(e->mapOfColIdAndData.put(e.getAssetTemplateColumn().getId(),e.getData()));
    	
		model.addAttribute("assetTemplateModel", asset.getAssetTemplate());
		model.addAttribute("assetid", asset.getId());
		model.addAttribute("mapOfColIdAndData", mapOfColIdAndData);
	    return "assetTemplate/assetFragment :: editAssetDetails";
	}
	
	/**
	 * edit AssetTemplate after checking if template is used
	 * @param idOfTemplate
	 */
	@RequestMapping(value = "form/saveAssetDetails", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	@ResponseStatus(value = HttpStatus.OK)
	public String saveAssetDetails(HttpServletRequest request,@RequestParam(Constants.TEMPLATE_ID) Integer templateId,
			@RequestParam(value = "id") Integer assetid ,Model model){
		
		AssetTemplate assetTemplate = assetTemplateService.findOne(templateId);
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);
		Asset asset  = assetService.updateAsset(request,assetTemplate,assetid);
		model.addAttribute("asset", new AssetBean(asset));
		return "assetTemplate/assetFragment::assetinformation";
	}
	@RequestMapping( value = "/getAssetBarChartWrapper/{id}", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	@ResponseBody
	public List<AssetBarChartWrapper> getAssetBarChartWrapper(@PathVariable(value = "id") Integer idOfAsset) {
		return assetService.getAssetBarChartWrapper(idOfAsset);
	}
	
	
	@RequestMapping(value = "/showSpider", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public  String showSpiderChart(@ModelAttribute("assetBarChartWrapper") AssetBarChartWrapper assetBarChartWrapper, Model model){
         model.addAttribute("assetBarChartWrapper",assetBarChartWrapper);
		return "assetTemplate/assetParameterBarChart :: spiderchart";
	}
	
}
