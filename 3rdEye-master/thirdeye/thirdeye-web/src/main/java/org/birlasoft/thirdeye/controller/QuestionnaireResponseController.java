package org.birlasoft.thirdeye.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.QuestionnaireExcelService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for show the {@code parameter} page , list of {@code questionnaire} {@code response}{@code status} 
 * and save {@code questionnaire} {@code question} {@code response} 
 *
 */
@Controller
@RequestMapping(value="/questionnaire")
public class QuestionnaireResponseController {
	
	private QuestionnaireService questionnaireService;
	private CustomUserDetailsService customUserDetailsService;
	private ResponseService responseService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private ParameterFunctionService parameterFunctionService;
	private ResponseDataService responseDataService;
	private static final String QUESTIONNAIRE_ID = "questionnaireId";
	private static final String ACTIVE_RESPONSE_ID = "activeResponseId";
	
	@Autowired
	private QuestionnaireExcelService questionnaireExcelService;
	
	private static Logger logger = LoggerFactory.getLogger(QuestionnaireResponseController.class);
	
	/**
	 * Constructor for initialize services
	 * @param questionnaireService
	 * @param customUserDetailsService
	 * @param responseService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param parameterFunctionService
	 * @param responseDataService
	 */
	@Autowired
	public QuestionnaireResponseController(QuestionnaireService questionnaireService,
			CustomUserDetailsService customUserDetailsService,
			ResponseService responseService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService,
			ParameterFunctionService parameterFunctionService,
			ResponseDataService responseDataService) {
		this.questionnaireService = questionnaireService;
		this.customUserDetailsService = customUserDetailsService;
		this.responseService = responseService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.parameterFunctionService = parameterFunctionService;
		this.responseDataService = responseDataService;
	}

	/**
	 * Show parameter page.
	 * @param model
	 * @param questionnaireId
	 * @param httpSession
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/response", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	public String respondToQuestionnaire(Model model,  @PathVariable(value = "id") Integer questionnaireId, HttpSession httpSession) {
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);		
		// Is there already any response for this qe. Currently only one response per qe.
		// we can add the ability to add more later.
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(qe, false);		
		if (isOnlyOneResponseAllowed(qe)){
			// Currently we take only one.
			if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
				Response newResponse = responseService.save(responseService.createNewResponseObject(qe, customUserDetailsService.getCurrentUser()));
				// put it in the session
				httpSession.setAttribute(ACTIVE_RESPONSE_ID, newResponse.getId());
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
				// put the response id into the session because only one response is allowed
				// per questionnaire.
				httpSession.setAttribute(ACTIVE_RESPONSE_ID, listOfResponseAvailable.get(0).getId());
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
				// this is an error scenario in the given application
				return null;
			}
		} else {
			// We can put in a new response id into the database
			// and then put it in the session
		}
		
		List<QuestionnaireQuestionBean> questionnaireQuestionBeans = questionnaireQuestionService.listOfQuestionsForQuestionnaire(qe);
		List<ParameterFunction> parameterFunctions = parameterFunctionService.findByMandatoryQuestion(true);
		// Make map for mandatory questions in questionnaire. 
		Map<Integer, Integer> mapOfMandatoryQuestion = questionnaireQuestionService.createMapForMandatoryQuestion(questionnaireQuestionBeans, parameterFunctions);
		
		//Get the map of FmId and Parameter name
		Map<Integer,String> fcFmMap = questionnaireQuestionService.getFcParameterList(qe);
		
		if(questionnaireQuestionBeans.isEmpty() && !fcFmMap.isEmpty()){
			return "redirect:/questionnaire/"+questionnaireId+"/functionalCoverage";
		}
		if(!fcFmMap.isEmpty()){
			model.addAttribute("fcExists",true);
		}else{
			model.addAttribute("fcExists",false);
		}
		
		model.addAttribute(QUESTIONNAIRE_ID, questionnaireId);
		model.addAttribute("questionnaire", qe);
		model.addAttribute("listOfQuestions", questionnaireQuestionBeans);
		model.addAttribute("mapOfMandatoryQuestion", mapOfMandatoryQuestion);
		
		return "questionnaire/questionnaireResponse";
	}

	/** 
	 * Check only one response allowed.
	 * @param qe
	 * @return {@code boolean}
	 */
	private boolean isOnlyOneResponseAllowed (Questionnaire qe) {
		//TODO: Check only one response is allowed for Questionnaire
		return true;
	}

	/**
	 * List of questionnaire response status.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/response/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_VIEW'})")
	public String listQuestionnaireResponseStatus(Model model) {
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Questionnaire> listOfQuestionnaires = questionnaireService.findByQuestionnaireTypeAndStatusAndWorkspaceIn(QuestionnaireType.DEF.toString(), QuestionnaireStatusType.PUBLISHED.toString(), listOfWorkspaces, true);
	
		model.addAttribute("completionStatus", null);
		model.addAttribute("completionPercentage", responseService.getMapForCompletionPercentageOfQe(listOfQuestionnaires));
		model.addAttribute("listOfquestionnaires", listOfQuestionnaires);
		model.addAttribute("answeredMandatoryQuestion", questionnaireQuestionService.createMapForAnsweredMandatoryQuestion(listOfQuestionnaires));
		return "questionnaire/listQuestionnaireResponse";
	}
	
	/**
	 * Save questionnaire question response
	 * @param request
	 * @param model
	 * @param session
	 * @param questionnaireId
	 * @param otherOptionText
	 */
	@RequestMapping(value = "/{id}/response/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveOneQQResponse(HttpServletRequest request, 
			HttpSession session, @PathVariable(value = "id") Integer questionnaireId,
			@RequestParam(value = "otherOptionText", required = false) String otherOptionText) {
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		Integer responseId = (Integer) session.getAttribute(ACTIVE_RESPONSE_ID);
		
		Response activeResponse = saveOneQQResponse(request, responseId, otherOptionText);
		responseService.save(activeResponse);
	}

	
	/**
	 * Save questionnaire question response
	 * @param request
	 * @param responseId
	 * @param otherOptionText 
	 * @param otherOption 
	 * @return {@code Response}
	 */
	private Response saveOneQQResponse(HttpServletRequest request, Integer responseId, String otherOptionText) {
		// Pull the fully loaded response from the data base
		Response activeResponse = responseService.findByIdLoadedResponseDatas(responseId);
		// This will have the response data and QQ loaded		
		// Quick check on the qqid's which are already available
		// The map is of QQID and the associated response data
		Map<Integer, ResponseData> qqMap = new HashMap<>();
		for (ResponseData oneResponseData : activeResponse.getResponseDatas()){
			qqMap.put(oneResponseData.getQuestionnaireQuestion().getId(),oneResponseData);
		}		
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		for(String oneParam : requestParameterNames){
			if (oneParam.startsWith("qq_")){
				// Find out which qq the user is answering
				String cleansedParam = oneParam.replaceAll("qq_", "");
				// The cleansed param is an Integer
				
				Integer idOfQQ = Integer.parseInt(cleansedParam);
				if (qqMap.containsKey(idOfQQ)){
					// We already have the response data and that has to be updated.
					ResponseData alreadyRespondedData = qqMap.get(idOfQQ);
					
					List<ResponseData> listOfAlreadyRespondedData= responseService.getListOfResponseData(request.getParameterValues(oneParam), idOfQQ, alreadyRespondedData, otherOptionText);
					 responseDataService.save(listOfAlreadyRespondedData);
				} else {
					// create a new response data
					ResponseData newResponseData = new ResponseData();
					List<ResponseData> listOfResponseData = responseService.getListOfResponseData(request.getParameterValues(oneParam), idOfQQ, newResponseData, otherOptionText);
					
					for(ResponseData responseData :listOfResponseData ){
					responseData.setResponse(activeResponse);
					activeResponse.getResponseDatas().add(responseData);
					}
					responseDataService.save(listOfResponseData);
				}
			}
		}
		
		activeResponse.setUpdatedDate(new Date());
		return activeResponse;
	}
	
	/**
	 * List of questionnaire to submit a response.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/submit/response/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_VIEW'})")
	public String listQuestionnaireToSubmitResponse(Model model) {
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Questionnaire> listOfQuestionnaires = questionnaireService.findByQuestionnaireTypeAndStatusAndWorkspaceIn(QuestionnaireType.DEF.toString(), QuestionnaireStatusType.PUBLISHED.toString(), listOfWorkspaces, true);
	
		model.addAttribute("questionnaireType", QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.DEF.getAction());
		model.addAttribute("listOfQuestionnaires", listOfQuestionnaires);
		model.addAttribute("completionPercentage", responseService.getMapForCompletionPercentageOfQe(listOfQuestionnaires));
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Response Status");
		return "questionnaire/listQuestionnaireSubmitResponse";
	}
	
	
	/**
	 * @author dhruv.sood
	 * show export import model
	 * @param model
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "{id}/exportImport/fetchModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	public String fetchModalForExportImport(Model model, @PathVariable(value = "id") Integer questionnaireId) {
		
		model.addAttribute(QUESTIONNAIRE_ID, questionnaireId);
		model.addAttribute("activeTab", "exportTab");
		return "questionnaire/modalFragment :: exportImportQuestionnaire";
	}
	
	
	/**
	 * method to download Questionnaire response into Excel
	 * @author dhruv.sood
	 * @param questionnaireId
	 * @param response
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/response/exportQuestionnaire", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	@ResponseBody
	public void downloadRespondToQuestionnaireExcel(@PathVariable(value = "id") Integer questionnaireId, HttpServletResponse response) {
		
		Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(questionnaireId);
		
		// Check if the user can access this questionnaire.
		if(questionnaireId != null){
			workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		} 			
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(questionnaire, false);		
		Response qResponse = checkResponse(questionnaire, listOfResponseAvailable);
		questionnaireExcelService.exportQuestionnaireTemplate(response,questionnaire, qResponse);
		
	}
		
	/**
	 * method to upload response for Questionnaire from excel
	 * @author dhruv.sood
	 * @param model
	 * @param idOfQuestionnaire
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "{id}/import", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	public String uploadResponseToQuestionnaireExcel(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire, @RequestParam(value="fileImport") MultipartFile file) {
		try {			
			Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);			
			// Check if the user can access this questionnaire.
			if(idOfQuestionnaire != null){
				workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
			}	
			List<ExcelCellBean> listOfErrorCells = new ArrayList<>();	
			boolean isFileExist = questionnaireExcelService.isFileExist(file , listOfErrorCells);
			
			if(isFileExist){
			InputStream in = file.getInputStream();			
			XSSFWorkbook workbook = new XSSFWorkbook(in);	
			questionnaireExcelService.importQuestionnaireResponseExcel(workbook, questionnaire, listOfErrorCells);
			workbook.close();
			in.close();	
			}
			//set list of errors in model if exists
			model.addAttribute("listOfErrors", listOfErrorCells);
			model.addAttribute(QUESTIONNAIRE_ID, idOfQuestionnaire);
			model.addAttribute("activeTab", "importTab");
			
		} catch (IOException e) {
			logger.info("Exception thrown in QuestionnaireResponseController :: uploadResponseToQuestionnaireExcel()"+e);			
		}
		return "forward:/questionnaire/"+idOfQuestionnaire+"/response";
	}

	/**
	 * @param questionnaire
	 * @param listOfResponseAvailable
	 * @param response
	 * @return
	 */
	private Response checkResponse(Questionnaire questionnaire,List<Response> listOfResponseAvailable) {
		Response response = new Response();
		if (isOnlyOneResponseAllowed(questionnaire)){
			// Currently we take only one.
			
			if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
				response = responseService.save(responseService.createNewResponseObject(questionnaire, customUserDetailsService.getCurrentUser()));
				
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
				response =listOfResponseAvailable.get(0);
								
			} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
				// this is an error scenario in the given application
				response= null;
			}
		} else {
			// We can put in a new response id into the database
			// and then put it in the session
		}
		return response;
	}
	
	/**
	 * Method to view all the FC Parameters corresponding to a Questionnaire
	 * @author dhruv.sood
	 * @param model
	 * @param questionnaireId	
	 * @return
	 */
	@RequestMapping(value = "{id}/functionalCoverage", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	public String viewFc(Model model, @PathVariable(value = "id") Integer questionnaireId) {
		//Get the questionnaire from questionnaireId
		Questionnaire questionnaire = questionnaireService.findOne(questionnaireId);
		
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		
		//Get the list of all the FC Parameters on a Questionnaire
		Map<Integer,String> fcFmMap = questionnaireQuestionService.getFcParameterList(questionnaire);
		
		model.addAttribute(QUESTIONNAIRE_ID, questionnaireId);
		model.addAttribute("fcFmMap", fcFmMap);
		return "questionnaire/viewFunctionalCoverage";
	}
	
}

