package org.birlasoft.thirdeye.controller.bcm;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.BcmLevelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Constructor for showBCMLevelsPage {@code string} , getModalContentForAddingLevel {@code string} ,addANewBcmLevel {@code String},
 * updateBcmlevelObject {@code BCMlevel}, deleteNodeFromTree
 */
@Controller
@RequestMapping(value="/bcmlevel")
public class BCMLevelController {
	
	private BCMService bcmService;
	private BCMLevelService bcmLevelService;
	private BcmLevelValidator bcmLevelValidator;
	private static final  String BCM_ID = "bcmId";
	
	/**
	 * Constructor to initialize service
	 * @param bcmService
	 * @param customUserDetailsService
	 * @param bcmLevelService
	 * @param bcmLevelValidator
	 */
	@Autowired
	public BCMLevelController(BCMService bcmService,
			BCMLevelService bcmLevelService,
			BcmLevelValidator bcmLevelValidator) {
		this.bcmService = bcmService;
		this.bcmLevelService = bcmLevelService;
		this.bcmLevelValidator = bcmLevelValidator;		
	}

	/**
	 * Show root BCM levels.
	 * @param model
	 * @param idOfBCM
	 * @return {@code String}
	 */
	@RequestMapping(value="/{id}/levels")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public String showBCMLevelsPage(Model model, @PathVariable(value="id") Integer idOfBCM){
		// TODO: Security can he view this page		
		// Prepare the view for the parameters that are on the questionnaire.
		Bcm bcm = bcmService.findOne(idOfBCM);
		// TODO: Check if the user can access this questionnaire.
		
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcm(bcm, false);
		
		// Need to load the Parameters into the tree!
		List<BcmLevelBean> rootBcmLevels = bcmLevelService.generateLevelTreeFromBcmLevels(listOfBcmLevel);
		model.addAttribute("bcmTemplateForm", bcm);
		model.addAttribute("rootBcmLevelList", rootBcmLevels);
		model.addAttribute(BCM_ID, idOfBCM);
		return "bcm/bcmLevels";
	}
	
	/**
	 * method to get the model content for adding bcm levels.
	 * @param model
	 * @param bcmId
	 * @param parentLevelId
	 * @param operationOnLevel
	 * @return {@code String}
	 */
	@RequestMapping(value="/{id}/level/fetchModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public String getModalContentForAddingLevel(Model model, @PathVariable(value = "id") Integer bcmId, 
			@RequestParam(value="parentLevel",required=false) Integer parentLevelId,
			@RequestParam(value="operation",required=false) String operationOnLevel){
		BcmLevelBean bcmLevelBean = null;
		if(parentLevelId == null || (operationOnLevel != null  && operationOnLevel.equalsIgnoreCase(Constants.BCM_LEVEL_ADD))){
			if(parentLevelId != null){
				BcmLevel bcmLevel = bcmLevelService.findOneLoadedBcmLevel(parentLevelId);
				bcmLevelBean =  new BcmLevelBean(bcmLevel.getLevelNumber(), bcmLevel.getBcmLevel());
			}else{
				bcmLevelBean =  new BcmLevelBean(-1, null);
			}
		}else if(operationOnLevel != null && operationOnLevel.equalsIgnoreCase(Constants.BCM_LEVEL_EDIT)){
			BcmLevel bcmLevel = bcmLevelService.findOne(parentLevelId);
			bcmLevelBean = new BcmLevelBean(bcmLevel);
		}
		model.addAttribute("bcmLevelForm", bcmLevelBean);
		model.addAttribute(BCM_ID, bcmId); 
		model.addAttribute("parentLevel", parentLevelId);
		model.addAttribute("operation", operationOnLevel);
		return "bcm/bcmModalFragment :: bcmLevelModal";
	}
	
	/**
	 * method to Add a new bcm level . 
	 * @param bcmLevelBean
	 * @param result
	 * @param model
	 * @param bcmId
	 * @param parentLevelId
	 * @param operationOnLevel
	 * @return {@code String}
	 */
	@RequestMapping(value="/{id}/level/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public String addANewBcmLevel(@ModelAttribute("bcmLevelForm") BcmLevelBean bcmLevelBean, BindingResult result, Model model, @PathVariable(value = "id") Integer bcmId, 
			@RequestParam(value="parentLevel",required=false) Integer parentLevelId,
			@RequestParam(value="operation",required=false) String operationOnLevel) {
		
		// If parentLevelId is 4, stop creating next levels
		if(parentLevelId != null && bcmLevelBean.getId() == null && bcmLevelService.findOne(parentLevelId).getLevelNumber() == 4){
				return null;
		}	
		
		// Fetch existing levels
				Bcm bcm = bcmService.findOne(bcmId);
				List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcm(bcm, false); 
				
		// Validate Bcm Level Bean
	
		bcmLevelValidator.validateBcmLevel(bcmLevelBean, listOfBcmLevel,result);
		if(result.hasErrors()){
			prepareModelData(bcmLevelBean, model, bcmId, parentLevelId,operationOnLevel);
			return "bcm/bcmModalFragment :: bcmLevelModal";
		}		
		
		BcmLevel savedBcmLevel = bcmLevelService.saveBcmLevel(bcmLevelBean, bcmId, parentLevelId);
		
		return addBcmLevel(model, parentLevelId, operationOnLevel,savedBcmLevel);
	}

	/**
	 * Add a new BCM level into existing  tree view .
	 * @param model
	 * @param parentLevelId
	 * @param operationOnLevel
	 * @param savedBcmLevel
	 * @return {@code String}
	 */
	private String addBcmLevel(Model model, Integer parentLevelId,
			String operationOnLevel, BcmLevel savedBcmLevel) {
		// Edit name of Level
		if(operationOnLevel != null && operationOnLevel.equalsIgnoreCase(Constants.BCM_LEVEL_EDIT)){
			model.addAttribute("oneBcmLevelBean", new BcmLevelBean(savedBcmLevel));
			return "bcm/bcmLevelFragements :: oneAnchor(oneLevel=${oneBcmLevelBean})";	
		}
		
		List<BcmLevelBean> bcmLevels = new ArrayList<>();
		bcmLevels.add(new BcmLevelBean(savedBcmLevel));		
		model.addAttribute("oneLevel", bcmLevels);
		
		// Assume no siblings
		boolean siblingIndicator = isSibling(savedBcmLevel);
		
		if(parentLevelId != null && !siblingIndicator){
			return "bcm/bcmLevelFragements :: oneLevelWithWrapper(listOfLevels=${oneLevel})";	
		}		
		// Append to an existing tree view
		return "bcm/bcmLevelFragements :: oneLevelAdded(listOfLevels=${oneLevel})";
	}

	/**
	 * validate there is any siblings.
	 * @param savedBcmLevel
	 * @return {@code boolean}
	 */
	private boolean isSibling(BcmLevel savedBcmLevel) {
		boolean siblingIndicator = false;		
		List<BcmLevel> levels = bcmLevelService.findByBcmLevel(savedBcmLevel.getBcmLevel());
		if(levels !=null && levels.size() > 1){
			siblingIndicator = true;
		}
		return siblingIndicator;
	}

	/**
	 * method to prepare model data after error.
	 * @param bcmLevelBean
	 * @param model
	 * @param bcmId
	 * @param parentLevelId
	 * @param operationOnLevel
	 */
	private void prepareModelData(BcmLevelBean bcmLevelBean, Model model,
			Integer bcmId, Integer parentLevelId, String operationOnLevel) {
		if(parentLevelId == null || (operationOnLevel != null  && operationOnLevel.equalsIgnoreCase(Constants.BCM_LEVEL_ADD))){
			if(parentLevelId != null){
				BcmLevel bcmLevel = bcmLevelService.findOneLoadedBcmLevel(parentLevelId);
				bcmLevelBean.setAddIconTitle(Utility.getTitleName(bcmLevel.getLevelNumber()));
				bcmLevelBean.setLevelNumber(bcmLevel.getLevelNumber());
				bcmLevelBean.setSuperNodeBcmLevel(bcmLevel.getBcmLevel());
			} else {
				bcmLevelBean.setAddIconTitle(Constants.BCM_LEVEL_TITLE_VALUE_CHAIN);
			}
		} else if(operationOnLevel != null && operationOnLevel.equalsIgnoreCase(Constants.BCM_LEVEL_EDIT)){
			bcmLevelBean.setTitleOnEdit(Utility.getTitleOnEdit(bcmLevelBean.getLevelNumber()));
		}
		model.addAttribute(BCM_ID, bcmId); 
		model.addAttribute("bcmLevelForm", bcmLevelBean);
		model.addAttribute("parentLevel", parentLevelId);
		model.addAttribute("operation", operationOnLevel);
	}
	
	/**
	 * To delete level BCM level Tree.
	 * @param model
	 * @param idOfBcm
	 * @param parentLevelId
	 * @param levelId
	 */
	@RequestMapping(value = "{id}/level/deleteNode", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteNodeFromTree( @PathVariable(value = "id") Integer idOfBcm,
									@RequestParam(value = "levelId" , required=false) Integer levelId){
		
		// TODO: check if the logged in user has privilege to edit the given questionnaire
		
		// Based on the parameters validate whether it is a parameter delete or whether it is a question delete
		if (levelId != null){
			bcmLevelService.deleteLevel(idOfBcm, levelId);
		}
	}
	
	@RequestMapping(value = "{id}/updateNode", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateNodeFromTree( @PathVariable(value = "id") Integer idOfBcm, @RequestParam(value = "parent") Integer parentId,
									@RequestParam(value = "position") Integer position){
		
		BcmLevel bcmLevel = bcmLevelService.findOneLoadedBcmLevel(idOfBcm);
		BcmLevel parentBcmLevel = bcmLevelService.findOneLoadedBcmLevel(parentId);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, position+1);
	}
	
}