package org.birlasoft.thirdeye.controller.home;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Password;
import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.HomePageService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *Controller for handling all the requests of the Home Page.
 *@author dhruv.sood
 */
@Controller
public class HomeController {

	@Autowired
	private CustomUserDetailsService customUserDetailsService;	
	@Autowired
	private HomePageService homePageService;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private ParameterService parameterService;
	@Autowired 
	private UserService userService;
	



	/**
	 * Validate the {@code current User} credentials, if valid then show {@code Home} otherwise show {@code Login} page
	 * @param model
	 * @return {@code home} page
	 * @author dhruv.sood
	 */
	@RequestMapping( value = "/home", method = RequestMethod.GET ) 
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String loginHome(Model model) {
		
		if (userService.findUserById(customUserDetailsService.getCurrentUser().getId()).isFirstLogin() == false) {
			return "redirect:user/changePassword";
		} else
			return "home/homeLanding";
	}
	
	@RequestMapping( value = "/homeAssetClasses", method = RequestMethod.GET ) 
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String getAssetClasses(Model model, HttpServletRequest request) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		int activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();

		Map<String, List<String>> filterMap = extractFilterMapFromRequestParam(request);

		List<AssetClassSearchBean> listAssetClassSearchBean = homePageService.home(tenantId, activeWorkspaceId, filterMap);
		model.addAttribute("listAssetClassSearchBean", listAssetClassSearchBean);
		return "home/homeFragment :: homeAssetClasses";
	}


	/**
	 * Handling the click on parameters of the home page
	 * @author dhruv.sood
	 * @param model
	 * @param assetTypeName
	 * @param parameterName
	 * @param parameterId
	 * @return
	 */
	@RequestMapping( value = "/homeParameterTiles/{assetClassName}/{parameterId}", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String homeTiles(Model model, @PathVariable(value = "assetClassName") String assetTypeName,
			@PathVariable(value = "parameterId") Integer parameterId, HttpServletRequest request) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		int activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		
		Map<String, List<String>> filterMap = extractFilterMapFromRequestParam(request);
		
		//Validate if a user can access this parameter
		workspaceSecurityService.authorizeParameterAccess(parameterService.findOne(parameterId));

		ParameterSearchBean parameterSearchBean = homePageService.homeParameterTiles(tenantId, activeWorkspaceId, assetTypeName, parameterId, filterMap);

		model.addAttribute("paramBean", parameterSearchBean);

		return "home/homeParameterFragment :: parameterFragment";
	}

	/**
	 * Create a filter map from request parameters
	 * @param request
	 * @return
	 */
	private Map<String, List<String>> extractFilterMapFromRequestParam(HttpServletRequest request) {
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		return filterMap;
	}


	/** @author dhruv.sood
	 * @param model
	 * @param parameterId
	 * @return
	 */
	@RequestMapping( value = "/assetParameterChart/{parameterId}", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String parameterClick(Model model,@PathVariable(value = "parameterId") Integer parameterId) {
		
		//Now, fetch the parameter based on parameterId
		Parameter parameter = parameterService.findOne(parameterId);
		
		//Validate if a user can access this parameter
		workspaceSecurityService.authorizeParameterAccess(parameter);
		
		model.addAttribute("parameterName", parameter.getDisplayName());
		model.addAttribute("parameterId", parameter.getId());
		return "assetTemplate/assetParameterMapping";
	}


	/**
	 * This controller is responsible for providing the json to for showing the asset parameter chart
	 * @author dhruv.sood
	 * @param parameterId
	 * @return
	 */
	@RequestMapping( value = "/assetParameterWrapper/{parameterId}", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	@ResponseBody
	public AssetParameterWrapper getAssetParameterWrapper(@PathVariable(value = "parameterId") Integer parameterId, HttpServletRequest request) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		int activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		
		Map<String, List<String>> filterMap = extractFilterMapFromRequestParam(request);
		
		return homePageService.assetParameterWrapper(tenantId, activeWorkspaceId, parameterId, filterMap);
	}



}
