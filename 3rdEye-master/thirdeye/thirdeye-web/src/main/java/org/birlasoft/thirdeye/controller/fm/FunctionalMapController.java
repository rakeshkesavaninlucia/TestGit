/**
 * 
 */
package org.birlasoft.thirdeye.controller.fm;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Category;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.FunctionalMapValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Functional Map Controller Class
 * @author shaishav.dixit
 *
 */
@Controller
@RequestMapping("/fm")
public class FunctionalMapController {
	
	private static Logger logger = LoggerFactory.getLogger(FunctionalMapController.class);
	
	private CustomUserDetailsService customUserDetailsService;
	private BCMService bcmService;
	private AssetTemplateService assetTemplateService;
	private QuestionService questionService;
	private FunctionalMapValidator functionalMapValidator;
	private FunctionalMapService functionalMapService;
	private WorkspaceSecurityService workspaceSecurityService;
	private QuestionnaireService questionnaireService;
	
	private static final String CREATE_FUNCTIONAL_MAP = "fm/createFunctionalMap";
	private static final String FUNCTIONAL_MAP_FORM = "functionalMapForm";
	
	/**
	 * Parameterized Constructor 
	 * @param customUserDetailsService
	 * @param bcmService
	 * @param assetTemplateService
	 * @param questionService
	 * @param functionalMapValidator
	 * @param functionalMapService
	 * @param workspaceSecurityService
	 * @param questionnaireService
	 */
	@Autowired
	public FunctionalMapController(CustomUserDetailsService customUserDetailsService,
			BCMService bcmService,
			AssetTemplateService assetTemplateService,
			QuestionService questionService,
			FunctionalMapValidator functionalMapValidator,
			FunctionalMapService functionalMapService,
			WorkspaceSecurityService workspaceSecurityService,
			QuestionnaireService questionnaireService) {
		this.customUserDetailsService = customUserDetailsService;
		this.bcmService = bcmService;
		this.assetTemplateService = assetTemplateService;
		this.questionService = questionService;
		this.functionalMapValidator = functionalMapValidator;
		this.functionalMapService = functionalMapService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.questionnaireService = questionnaireService;
	}
	
	/**
	 * Method to Create Functional Map
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	public String createFunctionalMap(Model model){
		setModelAttributeForFunctionalMap(model);
		model.addAttribute(FUNCTIONAL_MAP_FORM, new FunctionalMapBean());
		return CREATE_FUNCTIONAL_MAP;
	}

	/**
	 * Method to set attribute for functional Map
	 * @param model
	 */
	private void setModelAttributeForFunctionalMap(Model model) {
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Bcm> listOfBcms = new ArrayList<>();
		if(!listOfWorkspaces.isEmpty()){
			listOfBcms = bcmService.findByWorkspaceIn(listOfWorkspaces, true);
		}
		
		//map of asset templates
		Map<AssetType, List<AssetTemplate>> mapOfAssetTemplate = assetTemplateService.getMapOfTemplateByTypeForActiveWorkspace();
		
		//map of question
		Map<Category, List<Question>> mapOfQuestions = questionService.getMapOfQuestionByCategory(customUserDetailsService.getActiveWorkSpaceForUser());
		
		model.addAttribute("mapOfQuestions", mapOfQuestions);
		model.addAttribute("listOfBcms", listOfBcms);
		model.addAttribute("mapOfAssetTemplate", mapOfAssetTemplate);
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
	}

	/**
	 * Method to View Functional Map
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_VIEW'})")
	public String viewFunctionalMaps(Model model){
		List<FunctionalMap> listOfFm = functionalMapService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser(), true);
		model.addAttribute("listOfFm", listOfFm);
		return "fm/viewFunctionalMaps";
	}
	
	/**
	 * Method to save functional Map
	 * @param functionalMapBean
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	public String saveFunctionalMap(@ModelAttribute(FUNCTIONAL_MAP_FORM) FunctionalMapBean functionalMapBean,BindingResult result, Model model){
		
		if(functionalMapBean.getId() != null){
			workspaceSecurityService.authorizeFunctionalMapAccess(functionalMapBean);
		}
		
		functionalMapValidator.validate(functionalMapBean, result);
		if(result.hasErrors()){
			setModelAttributeForFunctionalMap(model);
			model.addAttribute(FUNCTIONAL_MAP_FORM, functionalMapBean);
			return CREATE_FUNCTIONAL_MAP;
		}
		
		User currentUser = customUserDetailsService.getCurrentUser();
		if(functionalMapBean.getId() == null){			
			FunctionalMap fm = functionalMapService.createFunctionalMapObject(functionalMapBean, currentUser);
			functionalMapService.save(fm);
		} else {
			FunctionalMap fm = functionalMapService.updateFunctionalMapObject(functionalMapBean, currentUser);
			functionalMapService.save(fm);
		}
		
		return "redirect:/fm/view";
	}
	
	/**
	 * Method to edit functional Map
	 * @param model
	 * @param idOfFunctionalMap
	 * @return
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	public String editFunctionalMap(Model model, @PathVariable(value="id")Integer idOfFunctionalMap){
		FunctionalMap fm = functionalMapService.findOne(idOfFunctionalMap, true);
		FunctionalMapBean fmBean = new FunctionalMapBean(fm);
		if(fmBean.getId() != null){
			workspaceSecurityService.authorizeFunctionalMapAccess(fmBean);
		}
		setModelAttributeForFunctionalMap(model);
		model.addAttribute(FUNCTIONAL_MAP_FORM, fmBean);
		return CREATE_FUNCTIONAL_MAP;
	}
	
	/**
	 * Method to display modal for export and Import of Functional Map Excel
	 * @param model
	 * @param idOfFunctionalMap
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "{id}/exportImport/fetchModal/{qid}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	public String fetchModalForExportImport(Model model, @PathVariable(value = "id") Integer idOfFunctionalMap,@PathVariable(value= "qid") Integer questionnaireId) {
		
		model.addAttribute("questionnaireId", questionnaireId);
		model.addAttribute("fmId", idOfFunctionalMap);
		model.addAttribute("activeTab", "exportTab");
		return "fm/fmExportImportModal :: exportImportFm";
	}
	
	/**
	 * Method to export functional map excel
	 * @param model
	 * @param idOfFunctionalMap
	 * @param response
	 */
	@RequestMapping(value = "{id}/exportTemplate", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	@ResponseBody
	public void exportTemplate(@PathVariable(value = "id") Integer idOfFunctionalMap, HttpServletResponse response) {
		FunctionalMap fm = functionalMapService.findOne(idOfFunctionalMap, false);
		FunctionalMapBean fmBean = new FunctionalMapBean(fm);
		
		if(fmBean.getId() != null){
			workspaceSecurityService.authorizeFunctionalMapAccess(fmBean);
		}  
		
		functionalMapService.exportFmTemplate(response,fm);
	}
	
	/**
	 * Method to import functional map excel
	 * @author sunil1.gupta
	 * @param model
	 * @param idOfFunctionalMap
	 * @param file
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "{id}/import/{qid}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'FUNCTIONAL_MAP_MODIFY'})")
	public String uploadFunctionalMapData(Model model, @PathVariable(value = "id") Integer idOfFunctionalMap, @PathVariable(value= "qid") Integer questionnaireId, @RequestParam(value="fileImport") MultipartFile file) {
		try {
			
			FunctionalMap fm = functionalMapService.findOne(idOfFunctionalMap,false);
			FunctionalMapBean fmBean = new FunctionalMapBean(fm);
			if (fmBean.getId() != null) {
				workspaceSecurityService.authorizeFunctionalMapAccess(fmBean);
			}
			InputStream in = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(in);		
			List<ExcelCellBean> listOfErrorCells = new ArrayList<>();
			//import functional map excel
			functionalMapService.importFunctionalMapExcel(workbook,fm,listOfErrorCells, questionnaireId);
			workbook.close();
			in.close();
			//set list of errors in model if exists
			model.addAttribute("listOfErrors", listOfErrorCells);
			model.addAttribute("fmId", idOfFunctionalMap);
			model.addAttribute("activeTab", "importTab");
			
		} catch (IOException e) {			
			logger.error("Exception thrown in FunctionalMapController : : uploadFunctionalMapData() "+e);
		}
		return "fm/fmExportImportModal :: exportImportFm";
	}
	
	/**
	 * Method to fetch functional map data
	 * @param model
	 * @param idOfFunctionalMap
	 * @param idOfQuestionnaire
	 * @return
	 */
	@RequestMapping(value="/fetchFMDataModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String fetchFunctionalMapData(Model model, @RequestParam(value = "fmId") Integer idOfFunctionalMap,
			@RequestParam(value = "qeId") Integer idOfQuestionnaire){
        List<FunctionalMapDataBean> listOfFMDataBean = new ArrayList<>();
		FunctionalMap fm = functionalMapService.findOne(idOfFunctionalMap,false);
		Questionnaire qe = questionnaireService.findOne(idOfQuestionnaire);
		FunctionalMapBean fmBean = new FunctionalMapBean(fm);
		if (fmBean.getId() != null) {
			workspaceSecurityService.authorizeFunctionalMapAccess(fmBean);
			listOfFMDataBean = functionalMapService.fetchFunctionalMapData(fm, qe);
		}
		model.addAttribute("fmName", fmBean.getName());
		model.addAttribute("listOfFMDataBean", listOfFMDataBean);
	    return "fm/viewFunctionalMapData :: functionalMapData";
	}
}
