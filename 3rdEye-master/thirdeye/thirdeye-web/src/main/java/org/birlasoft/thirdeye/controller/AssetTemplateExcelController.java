package org.birlasoft.thirdeye.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateExcelService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.ExportLogService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.impl.FunctionalMapServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for asset template excel {@code download} and {@code upload}
 * @author shaishav.dixit
 *
 */
@Controller
@RequestMapping(value = "/templates")
public class AssetTemplateExcelController {

	private static Logger logger = LoggerFactory.getLogger(AssetTemplateExcelController.class);

	private AssetTemplateService assetTemplateService;
	private WorkspaceSecurityService workspaceSecurityService;
	private AssetService assetService;
	private FunctionalMapServiceImpl functionalMapServiceImpl;
	private ExportLogService exportLogService;
	private AssetTemplateExcelService assetTemplateExcelService;

	/**
	 * Constructor autowiring of dependencies
	 * @param assetTemplateService
	 * @param workspaceSecurityService
	 * @param assetService
	 * @param functionalMapServiceImpl
	 * @param exportLogService
	 * @param assetTemplateExcelService
	 */
	@Autowired
	public AssetTemplateExcelController(AssetTemplateService assetTemplateService,
			WorkspaceSecurityService workspaceSecurityService,
			AssetService assetService,
			FunctionalMapServiceImpl functionalMapServiceImpl,
			ExportLogService exportLogService,
			AssetTemplateExcelService assetTemplateExcelService) {
		this.assetService = assetService;
		this.assetTemplateService = assetTemplateService;
		this.exportLogService = exportLogService;
		this.functionalMapServiceImpl = functionalMapServiceImpl;
		this.workspaceSecurityService = workspaceSecurityService;
		this.assetTemplateExcelService = assetTemplateExcelService;
	}

	/**
	 * Get the modal fragment for template export/import
	 * @param model
	 * @param idOfAssetTemplate
	 * @return
	 */
	@RequestMapping(value = "{id}/exportImport/fetchModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String fetchModalForExportImport(Model model, @PathVariable(value = "id") Integer idOfAssetTemplate) {

		model.addAttribute(Constants.TEMPLATE_ID, idOfAssetTemplate);
		model.addAttribute("activeTab", "exportTab");
		return "assetTemplate/editTemplateFragments :: exportImportInventory";
	}

	/**
	 * Export asset template excel sheet.
	 * @param idOfAssetTemplate
	 * @param response
	 */
	@RequestMapping(value = "{id}/exportTemplate", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	@ResponseBody
	public void exportTemplate(@PathVariable(value = "id") Integer idOfAssetTemplate, HttpServletResponse response) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(idOfAssetTemplate);
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);

		assetTemplateExcelService.exportTemplate(response,assetTemplate);		
	}

	/**
	 * Upload downloaded excel sheet for asset template. Data from excel sheet will be validated. If any validation fails, 
	 * the whole sheet will be rejected.
	 * @param model
	 * @param idOfAssetTemplate
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "{id}/import", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public String uploadInventoryData(Model model, @PathVariable(value = "id") Integer idOfAssetTemplate, @RequestParam(value="fileImport") MultipartFile file) {
		AssetTemplate assetTemplate = assetTemplateService.findOne(idOfAssetTemplate);
		List<Asset> listOfAssets = new ArrayList<>();
		List<ExcelCellBean> listOfErrorCells = new ArrayList<>();
		try {
			InputStream in = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(in);
			boolean isValidate = assetTemplateExcelService.validateSheetSecurity(workbook,assetTemplate,listOfErrorCells);
			if(isValidate){
				assetTemplateExcelService.readExcel(assetTemplate, workbook, listOfAssets, listOfErrorCells);
			}

			if (listOfErrorCells.isEmpty()) {
				//Now, save the assets
				 assetService.saveAssetAndRelationship(listOfAssets,assetTemplate);
				//
				//get exportLog by  hashValue getting from workbook 
				ExportLog exportLog = functionalMapServiceImpl.getExportLogByHashValue(workbook);
				exportLogService.updateExportLog(exportLog);			
			}
			workbook.close();
			in.close();
		} catch (IOException e) {
			logger.error("Error on excel upload", e);
		}	

		model.addAttribute("listOfErrors", listOfErrorCells);
		model.addAttribute(Constants.TEMPLATE_ID, idOfAssetTemplate);
		model.addAttribute("activeTab", "importTab");
		return "assetTemplate/editTemplateFragments :: exportImportInventory";
	}
}
