package org.birlasoft.thirdeye.controller;

import java.util.List;



import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.WorkspaceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code workspace} , {@code List} of all
 * {@code workspaces}, save {@code workspace}, {@code List} of all {@code Users}
 * belongs to same {@code workspace} and delete also.
 */
@Controller
@RequestMapping(value = "/workspace")
public class WorkSpaceController {

	private WorkspaceService workSpaceService;
	private WorkspaceValidator createWorkspaceValidator;
	private UserWorkspaceService userWorkspaceService;
	private CustomUserDetailsService customUserDetailsService;
	private UserService userService;

	/**
	 * Parameterized Constructor
	 * 
	 * @param workSpaceService
	 * @param userService
	 * @param createWorkspaceValidator
	 * @param userWorkspaceService
	 * @param customUserDetailsService
	 * @param userService
	 */
	@Autowired
	public WorkSpaceController(WorkspaceService workSpaceService, WorkspaceValidator createWorkspaceValidator,
			UserWorkspaceService userWorkspaceService, CustomUserDetailsService customUserDetailsService,
			UserService userService) {
		this.workSpaceService = workSpaceService;
		this.createWorkspaceValidator = createWorkspaceValidator;
		this.userWorkspaceService = userWorkspaceService;
		this.customUserDetailsService = customUserDetailsService;
		this.userService = userService;
	}

	/**
	 * This Method Create a {@code new} {@code workspace}
	 * 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String createWorkSpace(Model model) {
		model.addAttribute("workspace", new Workspace());
		return "workspace/createWorkSpace";
	}

	/**
	 * {@code save} and {@code validate} {@code workspace}, if found
	 * {@code error} then show {@code error message}
	 * 
	 * @param workSpace
	 * @param model
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String saveWorkSpace(@ModelAttribute("workspace") Workspace workSpace, BindingResult result, Model model) {
		createWorkspaceValidator.validate(workSpace, result);
		if (result.hasErrors()) {
			return "workspace/createWorkSpace";
		}
		Workspace workspacesave = workSpaceService
				.save(workSpaceService.createWorkspaceObject(workSpace, customUserDetailsService.getCurrentUser()));
		model.addAttribute("workspacesave", workspacesave);
		return "redirect:/workspace/view/" + workspacesave.getId();
	}

	/**
	 * List of all {@code workspace}
	 * 
	 * @param model
	 *            to add {@code workspace} Object and pass to jsp for
	 *            manipulation
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_VIEW'})")
	public String listAllWorkspaces(Model model) {
		model.addAttribute("allworkspaces", workSpaceService.findAll());
		return "workspace/listAllWorkspaces";
	}

	/**
	 * View {@code workspace} with give {@code Id} and {@code manipulate}
	 * {@code User} for this {@code workspace}
	 * 
	 * @param model
	 * @param idOfworkspace
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_VIEW'})")
	public String viewWorkspace(Model model, @PathVariable(value = "id") Integer idOfworkspace) {
		Workspace oneWorkspace = workSpaceService.findByIdLoadedUserWorspaces(idOfworkspace);
		List<User> userList = workSpaceService.getUserListByWorkspace(oneWorkspace);
		model.addAttribute("workspaceId", idOfworkspace);
		model.addAttribute("userListInWorkspace", userList);
		model.addAttribute("viewWorkspace", oneWorkspace);
		return "workspace/viewWorkspaceWithId";
	}

	/**
	 * Method to Delete a user from workspace
	 * 
	 * @param idOfuser
	 * @param idOfWorkspace
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String deleteUserFromWorkspace(@RequestParam(value = "userId") Integer idOfuser,
			@RequestParam(value = "workspaceId") Integer idOfWorkspace) {
		Workspace activeWorkspace = customUserDetailsService.getActiveWorkSpaceForUser();
		User currentUser = customUserDetailsService.getCurrentUser();
		User userToDelete = userService.findUserById(idOfuser);
		if(activeWorkspace.getId().equals(idOfWorkspace) && currentUser.getId().equals(idOfuser)) {
			return "redirect:/logout";
		}
		if(userWorkspaceService.findByUser(userToDelete).size() == 1) {
			return "redirect:/logout";
		}
		Workspace workspace = workSpaceService.findOneLoaded(idOfWorkspace,true);
		userWorkspaceService.deleteUserFromWorkspace(workspace, idOfuser);
		return "redirect:/workspace/view/" + workspace.getId();
	}
}
