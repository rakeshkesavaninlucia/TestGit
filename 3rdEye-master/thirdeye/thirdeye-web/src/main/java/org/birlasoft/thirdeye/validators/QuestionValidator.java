package org.birlasoft.thirdeye.validators;

import java.util.Arrays;
import java.util.List;

import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class QuestionValidator implements Validator {
    private static final String QUESTION_TITLE="title";
    private static final String QUESTION_TYPE="questionType";
    private static final String QUESTION_MODE="questionMode";
    private static final String QUESTION_DISPLAY_NAME="displayName";
    private static final String QUESTION_TYPE_TEXT="queTypeText";
    private static final String QUESTION_DISPLAY_UNIQUE_ERROR = "question.displayName.unique.error";

    @Autowired
	private Environment env;
    @Autowired
    private QuestionService questionService;
	@Override
	public boolean supports(Class<?> clazz) {
		return Question.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, QUESTION_TITLE, "question.title.error", env.getProperty("question.title.error"));
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, QUESTION_DISPLAY_NAME, "question.displayName.error", env.getProperty("question.displayName.error"));
	Question question = (Question)target;
	if (question.getQuestionType()!= null && ("-1").equals(question.getQuestionType())) {
		errors.rejectValue(QUESTION_TYPE, "question.type.error",env.getProperty("question.type.error"));
	   }
	if (question.getQuestionMode()!= null && ("-1").equals(question.getQuestionMode())) {
		errors.rejectValue(QUESTION_MODE, "question.mode.error",env.getProperty("question.mode.error"));
	   }
	 if(question.getDisplayName()!= null && question.getDisplayName().length() > 45){
			
		 errors.rejectValue(QUESTION_DISPLAY_NAME, "error.question.name.length", env.getProperty("error.question.name.length")); 
	 }
	if (question.getDisplayName()!= null){
		List<Question> listOfQuestions = questionService.findByWorkspaceAndDisplayName(question.getWorkspace(), question.getDisplayName());
			for (Question oneQuestion : listOfQuestions) {
				if (question.getId() == null || !question.getId().equals(oneQuestion.getId())){
					errors.rejectValue(QUESTION_DISPLAY_NAME, QUESTION_DISPLAY_UNIQUE_ERROR,env.getProperty(QUESTION_DISPLAY_UNIQUE_ERROR));
					break;
				}
	        }			
		}
	}
	
	/**
	 * method to validate Quantifiers
	 * @param quantifiers
	 * @param errors
	 */
	public void validateQuantifiers(List<Double> quantifiers, Errors errors) {

		for (Double i : quantifiers) {
			if (i != null && i != -1) {
				if (i < 1 || i > 10) {
					errors.rejectValue(QUESTION_TYPE_TEXT, "question.quantifiers.error",
							env.getProperty("question.quantifiers.error"));
				}
			} else {
				if (i == null)
					errors.rejectValue(QUESTION_TYPE_TEXT,
							"question.quantifiers.empty.error",
							env.getProperty("question.quantifiers.empty.error"));
			}
		}

	}
	
	/**
	 * method to validate question type text
	 * @param queTypeText
	 * @param errors
	 */
	public void validateQueTypeText(String queTypeText, Errors errors) {
		if(queTypeText != null && !queTypeText.isEmpty()){			
			List<String> queTypeTextList = Arrays.asList(queTypeText.split(","));
			if(queTypeTextList.contains("NA") || queTypeTextList.contains("N/A") || queTypeTextList.contains("N\\A")){
				errors.rejectValue(QUESTION_TYPE_TEXT, "question.queTypeText.error",
						env.getProperty("question.queTypeText.error"));
			}
			
		}

	}
	
	/**
	 * Validate bechmark for MCQ question.
	 * @param bqBean
	 * @return {@code boolean}
	 */
	public boolean validateBenchmark(BenchmarkQuestionBean bqBean) {
		boolean hasErrors = false;
		bqBean.putKeysInMap();
		for (String bqBeanProp : bqBean.getMapOfBenchmarkParam().keySet()) {
			switch (bqBeanProp) {
			case "benchmarkId":
				if(bqBean.getBenchmarkId() == -1){
					bqBean.putInMap(BenchmarkQuestionBean.BENCHMARK_ID, "Please select Benchmark.");
					hasErrors = true;
				}
				break;
			case "benchmarkItem":
				if(bqBean.getBenchmarkId() != -1 && bqBean.getBenchmarkItem() != null && bqBean.getBenchmarkItem().isEmpty()){
					bqBean.putInMap(BenchmarkQuestionBean.BENCHMARK_ITEM, "Please select Benchmark Item.");
					hasErrors = true;
				}
				break;
			default:
				break;
			}
		}
		return hasErrors;
	}

	public void validateMinMax(Integer min, Integer max, Errors errors) {
		if(min == null && max == null){
			errors.rejectValue(QUESTION_TYPE_TEXT, "question.queTypeText.minmax.error",
					env.getProperty("question.queTypeText.minmax.error"));
		}
		else if( min > max )			
		{
			errors.rejectValue(QUESTION_TYPE_TEXT, "question.queTypeText.minmaxcompare.error",
					env.getProperty("question.queTypeText.minmaxcompare.error"));
		}
		
	}
	
	public void validateCostElementModel(Object target, Errors errors) {
		Question question = (Question)target;
		if (question.getTitle() != null && question.getTitle().isEmpty()) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.costelement.title", env.getProperty("error.costelement.title "));
		}
	}
}
