package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 * unique asset name
 */
@Component
public class AssetValidation implements Validator {

	private static final String UID = "uid";

	@Autowired
	private Environment env;

	@Autowired
	private AssetService assetService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Asset.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors){

		Asset asset = (Asset) target;
		if(asset.getUid() != null){			
			Asset assetUid  = assetService.findByUid(asset.getUid());	
			if(assetUid != null) {
				errors.rejectValue(UID,"error.asset.name.unique", env.getProperty("error.asset.name.unique"));	
			}
		}
	}
	

	/**
	 * @param target asset
	 * @return boolean(true/false)
	 */
	public boolean isAssetDuplicate(Object target){

		Asset asset = (Asset) target;
		if(asset.getUid() != null){			
			Asset assetUid  = assetService.findByUid(asset.getUid());	
			if(assetUid != null) {
				return true;
			}
		}
		return false;
	}
}