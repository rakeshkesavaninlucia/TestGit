-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP table relationship_attribute_data;

DROP table relationship_asset;

DROP table relationship_suggestion_attribute;

DROP table relationship_suggestion;

delete FROM functional_map_data where questionnaireId is null;

ALTER TABLE `functional_map_data` 
DROP FOREIGN KEY `functional_map_data_ibfk_1`;
ALTER TABLE `functional_map_data` 
CHANGE COLUMN `questionnaireId` `questionnaireId` INT(11) NOT NULL AFTER `functionalMapId`;
ALTER TABLE `functional_map_data` 
ADD CONSTRAINT `functional_map_data_ibfk_1`
  FOREIGN KEY (`questionnaireId`)
  REFERENCES `questionnaire` (`id`);

DROP TABLE `interface`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;