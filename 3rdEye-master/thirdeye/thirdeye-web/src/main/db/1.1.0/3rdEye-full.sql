CREATE DATABASE  IF NOT EXISTS `3rdi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `3rdi`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aid`
--

DROP TABLE IF EXISTS `aid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `aidTemplate` varchar(25) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_AID_Name_WorkspaceId` (`name`,`workspaceId`),
  KEY `FK_AID_WORKSPACEID_idx` (`workspaceId`),
  KEY `FK_AID_ASSETTEMPLATID_idx` (`assetTemplateId`),
  KEY `FK_CREATEDBY_USERID_idx` (`createdBy`),
  KEY `FK_UPDATEDBY_USERID_idx` (`updatedBy`),
  CONSTRAINT `FK_AID_ASSETTEMPLATID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AID_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CREATEDBY_USERID` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UPDATEDBY_USERID` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid`
--

LOCK TABLES `aid` WRITE;
/*!40000 ALTER TABLE `aid` DISABLE KEYS */;
INSERT INTO `aid` VALUES (1,'Test AID','this is the test AID',1,'DEFAULT',1,2,'2016-04-26 06:03:25',2,'2016-04-26 06:03:25');
/*!40000 ALTER TABLE `aid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aid_block`
--

DROP TABLE IF EXISTS `aid_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aidId` int(11) NOT NULL,
  `aidBlockType` varchar(20) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `blockJSONConfig` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AIDID_AID_idx` (`aidId`),
  CONSTRAINT `FK_AIDID_AID` FOREIGN KEY (`aidId`) REFERENCES `aid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid_block`
--

LOCK TABLES `aid_block` WRITE;
/*!40000 ALTER TABLE `aid_block` DISABLE KEYS */;
INSERT INTO `aid_block` VALUES (1,1,'BLOCK_4',1,'{\"blockTitle\":\"\",\"subBlock1\":{\"templateColumnId\":0,\"blockTitle\":\"Test Sub block 1\",\"paramQEId\":0,\"paramId\":0,\"questionId\":1,\"questionQEId\":1},\"subBlock2\":{\"templateColumnId\":3,\"blockTitle\":\"Test Sub block 2\",\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0}}'),(2,1,'BLOCK_1',2,'');
/*!40000 ALTER TABLE `aid_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `uid` varchar(256) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,1,'1',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(2,1,'2',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(3,1,'3',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(4,1,'4',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(5,2,'5',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(6,2,'6',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(7,3,'7',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(8,3,'8',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(9,3,'9',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(10,3,'10',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(11,3,'11',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(12,3,'12',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(13,3,'13',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(14,3,'14',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(15,1,'15',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(16,6,'16',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(17,6,'17',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(18,6,'18',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(19,6,'19',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(20,6,'20',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(21,6,'21',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(22,6,'22',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(23,6,'23',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(24,6,'24',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(25,6,'25',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(26,6,'26',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(27,6,'27',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(28,4,'28',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(29,6,'29',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(30,6,'30',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(31,6,'31',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(32,6,'32',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(33,6,'33',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(34,6,'34',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(35,1,'35',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(36,2,'36',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(37,6,'37',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(38,6,'38',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(39,6,'39',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(40,1,'40',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(41,4,'41',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(42,2,'42',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(43,12,'123::Asset::Relationship::12::',0,2,'2016-07-25 08:57:49',2,'2016-07-25 08:57:49'),(45,15,'123::Asset::Relationship::15::',0,2,'2016-07-25 09:08:34',2,'2016-07-25 09:08:34'),(48,14,'123::Asset::Relationship::14::',0,2,'2016-07-25 10:53:34',2,'2016-07-25 10:53:34'),(50,14,'123::Asset::Relationship::14::Nexia',0,2,'2016-07-25 12:34:54',2,'2016-07-25 12:34:54'),(51,2,'123::Asset::Server::2::Google',0,2,'2016-07-25 12:37:52',2,'2016-07-25 12:37:52'),(53,2,'123::Asset::Server::2::Google12',0,2,'2016-07-25 12:38:21',2,'2016-07-25 12:38:21'),(54,19,'123::Asset::Relationship::19::Google',0,2,'2016-07-25 12:38:49',2,'2016-07-25 12:38:49'),(57,19,'123::Asset::Relationship::19::Test App',0,2,'2016-07-25 13:36:58',2,'2016-07-25 13:36:58'),(59,19,'123::Asset::Relationship::19::EWDF',0,2,'2016-07-25 13:37:22',2,'2016-07-25 13:37:22'),(60,17,'123::Asset::Relationship::17::sdc',0,2,'2016-07-26 05:34:21',2,'2016-07-26 05:34:21'),(61,20,'123::Asset::Relationship::20::sfdffdf',0,2,'2016-07-26 12:15:10',2,'2016-07-26 12:15:10'),(62,20,'123::Asset::Relationship::20::Name2',0,2,'2016-07-26 12:41:59',2,'2016-07-26 12:41:59'),(64,14,'123::Asset::Relationship::14::ISSPL',0,2,'2016-07-26 12:56:05',2,'2016-07-26 12:56:05'),(65,14,'123::Asset::Relationship::14::Test App',0,2,'2016-07-26 12:56:30',2,'2016-07-26 12:56:30'),(66,14,'123::Asset::Relationship::14::stoleman',0,2,'2016-07-26 12:57:48',2,'2016-07-26 12:57:48'),(67,19,'123::Asset::Relationship::19::xcbg',0,2,'2016-07-26 13:09:42',2,'2016-07-26 13:09:42'),(68,19,'123::Asset::Relationship::19::wadrwe',0,2,'2016-07-27 07:08:53',2,'2016-07-27 07:08:53'),(69,2,'123::Asset::Server::2::Name 1',0,2,'2016-07-28 12:23:54',2,'2016-07-28 12:23:54'),(70,19,'123::Asset::Relationship::19::Name1',0,2,'2016-07-28 12:24:24',2,'2016-07-28 12:24:24'),(72,19,'123::Asset::Relationship::19::stoleman',0,2,'2016-07-28 12:33:30',2,'2016-07-28 12:33:30'),(73,19,'123::Asset::Relationship::19::xcvcx',0,2,'2016-07-28 12:33:54',2,'2016-07-28 12:33:54'),(75,19,'123::Asset::Relationship::19::HT',0,2,'2016-07-28 12:35:42',2,'2016-07-28 12:35:42'),(79,19,'123::Asset::Relationship::19::weferg_Test Application',0,2,'2016-07-29 12:09:49',2,'2016-07-29 12:09:49'),(80,19,'123::Asset::Relationship::19::Google_Google1',0,2,'2016-07-29 12:11:25',2,'2016-07-29 12:11:25'),(81,1,'123::Asset::Application::1::dfsdf',0,2,'2016-07-29 12:12:19',2,'2016-07-29 12:12:19'),(82,20,'123::Asset::Relationship::20::Deep',0,2,'2016-08-02 05:43:02',2,'2016-08-02 05:43:02'),(83,21,'123::Asset::Relationship::21::Google_Google',0,2,'2016-08-02 06:23:58',2,'2016-08-02 06:23:58'),(84,22,'123::Asset::Relationship::22::Google_sdc',0,2,'2016-08-02 06:38:37',2,'2016-08-02 06:38:37'),(85,17,'123::Asset::Relationship::17::Google_Google1',0,2,'2016-08-26 10:51:52',2,'2016-08-26 10:51:52');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'Google Inc',3,2),(2,'Google',1,1),(3,'',2,2),(4,'Google Inc1',3,1),(5,'',2,1),(6,'Google1',1,2),(7,'Naukri.com',3,3),(8,'Naukri',1,3),(9,'',2,3),(10,'Naukri1',1,4),(11,'',2,4),(12,'Naukri.com',3,4),(13,'Tomcat',4,5),(14,'Tomcat1',4,6),(15,'it is app server',5,5),(16,'it is app server1',5,6),(17,'3rdEye',6,7),(18,'FuelSignal',6,8),(19,'DVVNL',6,9),(20,'ISSPL',6,10),(21,'HT',6,11),(22,'Nexia',6,12),(23,'Edusoft',6,13),(24,'stoleman',6,14),(25,'dffff',9,15),(26,'dfdf',2,15),(27,'fdfdff',3,15),(28,'ferf',1,15),(29,NULL,10,16),(30,'sfdffdf',8,16),(31,'2016-08-07',10,17),(32,'gfhngh',8,17),(33,'2016-06-30',10,18),(34,'wadrwe',8,18),(35,'2016-04-29',10,19),(36,'tfghgf',8,19),(37,NULL,10,20),(38,'sdgffdg',8,20),(39,'2016-06-30',10,21),(40,'xcvcx',8,21),(41,'xcvcx',8,22),(42,'2016-06-30',10,22),(43,NULL,10,23),(44,'EWDF',8,23),(45,'EWDF',8,24),(46,NULL,10,24),(47,NULL,10,25),(48,'dfgdf',8,25),(49,'2016-06-22',10,26),(50,'xcbg',8,26),(51,'2016-06-22',10,27),(52,'sdfgdsffgdf',8,27),(53,'Test App',7,28),(54,'Test Application',8,29),(55,'2016-07-01',10,29),(56,'Test Application',8,30),(57,'2016-07-01',10,30),(58,'Name1',8,31),(59,'2016-07-02',10,31),(60,'2016-07-13',10,32),(61,'Name2',8,32),(62,'2016-07-02',10,33),(63,'Name2',8,33),(64,'weferg',8,34),(65,'2016-07-08',10,34),(66,'GIC Db',2,35),(67,'Google',1,35),(68,'dffff',9,35),(69,'5',3,35),(70,'dfdd',5,36),(71,'sdc',4,36),(72,'2016-07-04',10,37),(73,'EWDF',8,37),(74,'gfhngh',8,38),(75,'2016-07-15',10,38),(76,'2016-07-14',10,39),(77,'EWDF',8,39),(78,'Google',1,40),(79,'Samar',3,40),(80,'gdfdfg',2,40),(81,'fgh',9,40),(82,'Google',7,41),(83,'Google',4,42),(84,'test',5,42),(87,'Google',4,51),(88,'Google desc',5,51),(89,'Google desc',5,53),(90,'Google12',4,53),(91,'456',17,54),(92,'fgfd',16,54),(93,'sdg',16,57),(94,'4',17,57),(95,'4',17,59),(96,'sdg',16,59),(97,'Deep',18,61),(98,'Deep1',18,62),(99,'56',17,67),(100,'vgbngc',16,67),(101,'gh',16,68),(102,'56',17,68),(103,'Name 1',4,69),(104,'Desc ',5,69),(105,'Rela 2',16,70),(106,'3',17,70),(107,'4',17,72),(108,'Rela1324',16,72),(109,'655',17,73),(110,'trtrt',16,73),(111,'4',17,75),(112,'rtgrddf',16,75),(113,'Rela45455',16,79),(114,'5',17,79),(115,'4erwer',16,80),(116,'4',17,80),(117,'dfsdf',1,81),(118,'dfgdfg',3,81),(119,'dfgdfg',2,81),(120,'gdfgdfg',9,81),(121,'Deep',18,82);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (1,'template test',1,0,'',1,2,'2016-04-20 10:26:19',2,'2016-06-06 10:09:26'),(2,'Server Inventory',2,0,'Server Inventory desc',1,2,'2016-04-20 10:32:53',2,'2016-06-06 11:09:23'),(3,'App Inventory',1,0,'',1,2,'2016-05-27 06:44:17',2,'2016-06-06 11:09:04'),(4,'template tes',1,0,'fbgdfgfgggfgfg',1,2,'2016-06-06 09:29:28',2,'2016-06-06 11:01:23'),(6,'application Inventory',1,0,'',1,2,'2016-06-06 11:08:51',2,'2016-06-06 11:09:45'),(7,'template test rela',8,0,'sefs',1,2,'2016-07-06 08:21:44',2,'2016-08-02 06:35:45'),(9,'template test rela1',8,0,'sefs',1,2,'2016-07-06 08:28:29',2,'2016-07-06 08:28:29'),(10,'template test rela2',8,0,'sdc',1,2,'2016-07-06 08:31:48',2,'2016-07-06 08:31:48'),(11,'template test dsfd',8,0,'',1,2,'2016-07-06 11:31:40',2,'2016-07-06 11:31:40'),(12,'template test bfgghfgh',8,0,'',1,2,'2016-07-06 11:36:42',2,'2016-07-06 11:36:42'),(13,'erggerg',8,0,'dfdfgdf',1,2,'2016-07-06 13:28:41',2,'2016-07-06 13:28:41'),(14,'wdedryuyuuy',8,0,'',1,2,'2016-07-06 13:35:51',2,'2016-07-25 08:48:30'),(15,'template test',8,0,'',1,2,'2016-07-07 05:41:27',2,'2016-07-07 05:41:27'),(16,'sdcdsda',8,0,'',1,2,'2016-07-07 08:42:45',2,'2016-07-07 08:42:45'),(17,'template test98',8,0,'sa',1,2,'2016-07-07 08:48:00',2,'2016-07-19 10:05:29'),(18,'template test456',1,0,'retg',1,2,'2016-07-07 09:18:58',2,'2016-07-07 09:18:58'),(19,'Test Relationship Template',8,0,'Test Relationship Template',1,2,'2016-07-07 09:19:40',2,'2016-08-01 09:39:57'),(20,'Relat',8,0,'',1,2,'2016-07-26 12:13:37',2,'2016-07-26 12:13:37'),(21,'Test Relationship Template1',8,0,'',1,2,'2016-08-02 06:23:31',2,'2016-08-02 06:23:31'),(22,'Test Relationship Template2',8,0,'',1,2,'2016-08-02 06:38:24',2,'2016-08-02 06:38:24'),(23,'App Test',1,1,'',1,2,'2016-08-02 07:41:57',2,'2016-08-02 07:41:57'),(24,'gdrt',2,0,'drfgd',1,2,'2016-08-02 10:09:45',2,'2016-08-02 10:09:45'),(25,'sdf',8,0,'',1,2,'2016-08-02 10:17:37',2,'2016-08-02 10:17:37'),(26,'rftj',8,1,'rt',1,2,'2016-08-02 10:25:26',2,'2016-08-02 10:25:26'),(27,'Relationship template test',8,1,'WeWE',1,2,'2016-08-03 10:44:39',2,'2016-08-03 10:44:39'),(28,'template test relationship ',8,0,'',1,2,'2016-08-03 10:55:52',2,'2016-08-03 10:55:52'),(29,'test temp 112',1,1,'',1,2,'2016-08-03 10:58:33',2,'2016-08-03 10:58:33'),(30,'template test123',8,0,'gdrty',1,2,'2016-08-12 07:23:39',2,'2016-08-12 07:23:52'),(31,'Test server',8,0,'',1,2,'2016-08-12 10:37:09',2,'2016-08-12 10:37:35'),(32,'RLs_db&Inter',8,0,'',1,2,'2016-08-12 10:39:29',2,'2016-08-12 10:39:29'),(33,'samar 1',1,0,'',1,2,'2016-08-22 11:41:34',2,'2016-08-22 11:41:34');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (1,'Name','TEXT',100,1,0,1,1,2,'2016-04-20 10:26:19',2,'2016-04-20 10:26:19'),(2,'Description','TEXT',200,1,0,0,2,2,'2016-04-20 10:26:49',2,'2016-04-20 10:26:49'),(3,'Owner','TEXT',50,1,0,1,3,2,'2016-04-20 10:27:21',2,'2016-04-20 10:27:21'),(4,'Name','TEXT',100,2,0,1,1,2,'2016-04-20 10:32:53',2,'2016-04-20 10:32:53'),(5,'Desc','TEXT',200,2,0,0,2,2,'2016-04-20 10:33:03',2,'2016-04-20 10:33:03'),(6,'Name','TEXT',100,3,0,1,1,2,'2016-05-27 06:44:17',2,'2016-05-27 06:44:17'),(7,'Name','TEXT',100,4,0,1,1,2,'2016-06-06 09:29:28',2,'2016-06-06 09:29:28'),(8,'Name','TEXT',100,6,0,1,1,2,'2016-06-06 11:08:51',2,'2016-06-06 11:08:51'),(9,'Test','TEXT',10,1,0,0,4,2,'2016-06-08 10:30:52',2,'2016-06-08 10:30:52'),(10,'Date test','DATE',0,6,0,1,2,2,'2016-06-30 09:26:27',2,'2016-06-30 13:16:20'),(13,'Name','TEXT',100,10,0,1,1,2,'2016-07-06 08:31:48',2,'2016-07-06 08:31:48'),(14,'Name','TEXT',100,11,0,1,1,2,'2016-07-06 11:31:40',2,'2016-07-06 11:31:40'),(15,'Name','TEXT',100,18,0,1,1,2,'2016-07-07 09:18:58',2,'2016-07-07 09:18:58'),(16,'Relationship Name','TEXT',100,19,0,1,1,2,'2016-07-20 12:58:41',2,'2016-07-20 12:58:41'),(17,'Protocol','NUMBER',23,19,0,0,2,2,'2016-07-20 12:59:09',2,'2016-07-20 12:59:09'),(18,'Name','TEXT',10,20,0,0,1,2,'2016-07-26 12:14:03',2,'2016-07-26 12:14:03'),(19,'Name','TEXT',100,9,0,1,1,2,'2016-07-06 08:28:29',2,'2016-07-06 08:28:29'),(37,'Name','TEXT',100,24,0,1,1,2,'2016-08-02 10:09:45',2,'2016-08-02 10:09:45'),(38,'Desc','TEXT',200,20,0,0,2,2,'2016-08-03 10:26:46',2,'2016-08-03 10:26:46'),(39,'Protocoal','TEXT',20,20,0,1,4,2,'2016-08-03 10:27:09',2,'2016-08-03 10:27:09'),(40,'Title','TEXT',100,20,0,0,3,2,'2016-08-03 10:27:40',2,'2016-08-03 10:27:40'),(41,'w1','NUMBER',1,23,0,0,1,2,'2016-08-03 10:43:42',2,'2016-08-03 10:43:42'),(52,'Name','TEXT',100,27,0,1,1,2,'2016-08-03 10:54:40',2,'2016-08-03 10:54:40'),(53,'Desc','TEXT',200,27,0,0,2,2,'2016-08-03 10:55:04',2,'2016-08-03 10:55:04');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (1,'Application',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(2,'Server',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(3,'Database',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(4,'Person',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(5,'Business Services',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(6,'IT Services',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(7,'Interfaces',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(8,'Relationship',0,1,'2016-06-23 06:45:08',1,'2016-06-23 06:45:08');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type_style`
--

DROP TABLE IF EXISTS `asset_type_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL,
  `asset_colour` varchar(45) NOT NULL,
  `asset_image` blob,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeId_workspace_uq` (`asset_type_id`,`workspaceId`),
  KEY `assetTypeId_fk_idx` (`asset_type_id`),
  KEY `id_idx` (`asset_type_id`),
  KEY `assetTypeStyle_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `assetTypeId_fk` FOREIGN KEY (`asset_type_id`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeStyle_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type_style`
--

LOCK TABLES `asset_type_style` WRITE;
/*!40000 ALTER TABLE `asset_type_style` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_type_style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm`
--

DROP TABLE IF EXISTS `bcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bcmId` int(11) DEFAULT NULL,
  `bcmName` varchar(255) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_workspaceId_idx` (`workspaceId`),
  KEY `bcm_createdBy_idx` (`createdBy`),
  KEY `bcm_updatedBy_idx` (`updatedBy`),
  KEY `bcm_bcmId_idx` (`bcmId`),
  CONSTRAINT `bcm_bcmId` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm`
--

LOCK TABLES `bcm` WRITE;
/*!40000 ALTER TABLE `bcm` DISABLE KEYS */;
INSERT INTO `bcm` VALUES (1,NULL,'Test',NULL,2,'2016-04-20 12:35:57',2,'2016-04-20 12:35:57'),(2,1,'Test BCM1',1,2,'2016-04-20 12:37:37',2,'2016-05-23 10:37:48'),(4,NULL,'BCM Template',NULL,2,'2016-05-27 06:52:58',2,'2016-05-27 06:52:58'),(5,4,'Test BCM',1,2,'2016-06-03 08:54:42',2,'2016-06-03 08:54:42'),(6,4,'Test BCM',1,2,'2016-06-03 08:54:42',2,'2016-06-03 08:54:42'),(7,NULL,'Test BCM 6',NULL,2,'2016-06-06 10:00:59',2,'2016-06-06 10:00:59');
/*!40000 ALTER TABLE `bcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm_level`
--

DROP TABLE IF EXISTS `bcm_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentBcmLevelId` int(11) DEFAULT NULL,
  `bcmId` int(11) NOT NULL,
  `levelNumber` int(2) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `bcmLevelName` varchar(60) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_level_parentBcmLevelId_fk_idx` (`parentBcmLevelId`),
  KEY `bcm_level_bcmId_fk_idx` (`bcmId`),
  KEY `bcm_level_createdBy_fk_idx` (`createdBy`),
  KEY `bcm_level_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `bcm_level_bcmId_fk` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_parentBcmLevelId_fk` FOREIGN KEY (`parentBcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm_level`
--

LOCK TABLES `bcm_level` WRITE;
/*!40000 ALTER TABLE `bcm_level` DISABLE KEYS */;
INSERT INTO `bcm_level` VALUES (1,NULL,1,0,1,'L1','',2,'2016-04-20 12:36:05',2,'2016-04-20 12:36:05'),(3,1,1,1,1,'L2','Strategy',2,'2016-04-20 12:36:23',2,'2016-04-20 12:36:23'),(4,1,1,1,2,'L21','Strategy',2,'2016-04-20 12:36:40',2,'2016-04-20 12:36:40'),(6,3,1,2,1,'L2','',2,'2016-04-20 12:36:58',2,'2016-04-20 12:36:58'),(8,NULL,2,0,1,'Legal','',2,'2016-04-20 12:37:37',2,'2016-05-27 06:55:13'),(9,8,2,1,1,'Legal Strategy','Strategy',2,'2016-04-20 12:37:38',2,'2016-05-27 06:55:29'),(11,9,2,2,1,'L3','',2,'2016-04-20 12:37:38',2,'2016-05-20 07:05:13'),(13,8,2,1,2,'Policy Formulation','Strategy',2,'2016-04-20 12:37:38',2,'2016-05-27 06:55:45'),(16,11,2,3,1,'L4','',2,'2016-05-20 07:05:37',2,'2016-05-20 07:05:37'),(17,NULL,2,0,2,'Finance','',2,'2016-05-20 07:06:12',2,'2016-05-27 06:56:25'),(18,17,2,1,1,'Financial Planning & Forecasting','Strategy',2,'2016-05-20 07:06:27',2,'2016-05-27 06:56:39'),(19,18,2,2,1,'L3 New','',2,'2016-05-20 07:06:37',2,'2016-05-20 07:06:37'),(20,19,2,3,1,'L4 New','',2,'2016-05-20 07:06:45',2,'2016-05-20 07:06:45'),(21,13,2,2,1,'L3 L21','',2,'2016-05-23 10:26:19',2,'2016-05-23 10:26:19'),(22,21,2,3,1,'L4 L21','',2,'2016-05-23 10:28:05',2,'2016-05-23 10:28:05'),(23,19,2,3,2,'L4 1 New','',2,'2016-05-23 10:28:34',2,'2016-05-23 10:28:34'),(24,18,2,2,2,'L3 1 New','',2,'2016-05-23 10:28:57',2,'2016-05-23 10:28:57'),(25,17,2,1,2,'Licensing / Targeting Acquisitions Management','Strategy',2,'2016-05-23 10:30:13',2,'2016-05-27 06:56:56'),(26,13,2,2,2,'L3 1 L21','',2,'2016-05-23 10:30:48',2,'2016-05-23 10:30:48'),(27,NULL,4,0,1,'Legal','',2,'2016-05-27 06:54:00',2,'2016-05-27 06:54:00'),(28,27,4,1,1,'Legal Strategy','Strategy',2,'2016-05-27 06:54:17',2,'2016-05-27 06:54:17'),(29,8,2,1,3,'Contract Framework','Strategy',2,'2016-05-27 06:56:07',2,'2016-05-27 06:56:07'),(30,NULL,2,0,3,'Business Development','',2,'2016-05-27 07:08:42',2,'2016-05-27 07:08:42'),(31,30,2,1,1,'Business Strategy','Strategy',2,'2016-05-27 07:09:03',2,'2016-05-27 07:09:03'),(32,30,2,1,2,'Product Lifecycle Planning','Strategy',2,'2016-05-27 07:09:49',2,'2016-05-27 07:09:49'),(33,31,2,2,1,'BS process 1','',2,'2016-05-27 07:10:19',2,'2016-05-27 07:10:19'),(34,31,2,2,2,'BS process 2','',2,'2016-05-27 07:10:35',2,'2016-05-27 07:10:35'),(35,33,2,3,1,'BS activity 1','',2,'2016-05-27 07:10:49',2,'2016-05-27 07:10:49'),(36,33,2,3,2,'BS activity 2','',2,'2016-05-27 07:11:05',2,'2016-05-27 07:11:05'),(37,32,2,2,1,'PLP process 1','',2,'2016-05-27 07:11:51',2,'2016-05-27 07:11:51'),(38,32,2,2,2,'PLP process 2','',2,'2016-05-27 07:12:03',2,'2016-05-27 07:12:03'),(39,37,2,3,1,'PLP activity 1','',2,'2016-05-27 07:12:15',2,'2016-05-27 07:12:15'),(40,34,2,3,1,'BS 2 activity 1','',2,'2016-05-27 07:13:00',2,'2016-05-27 07:13:00'),(41,38,2,3,1,'PLP 2 activity 1','',2,'2016-05-27 07:13:20',2,'2016-05-27 07:13:20'),(46,NULL,7,0,1,'L0','',2,'2016-06-06 10:01:06',2,'2016-06-06 10:01:06'),(47,NULL,2,0,4,'Sequence Number','',2,'2016-06-15 05:35:08',2,'2016-06-15 05:35:08'),(48,NULL,2,0,5,'Sequence Number','',2,'2016-06-15 05:35:09',2,'2016-06-15 05:35:09'),(49,47,2,1,1,'L1ew','Strategy',2,'2016-06-15 05:35:42',2,'2016-06-15 05:35:42'),(50,47,2,1,2,'L2','njk',2,'2016-06-15 05:36:09',2,'2016-06-15 05:36:09'),(51,47,2,1,3,'L1 New','Strategy',2,'2016-06-15 05:36:22',2,'2016-06-15 05:36:22'),(52,49,2,2,1,'L2','',2,'2016-06-15 05:37:15',2,'2016-06-15 05:37:15'),(53,52,2,3,1,'L3','',2,'2016-06-15 05:37:30',2,'2016-06-15 05:37:30'),(54,50,2,2,1,'L2','',2,'2016-06-15 05:37:46',2,'2016-06-15 05:37:46'),(55,47,2,1,4,'L2 New','Strategy',2,'2016-06-15 05:38:07',2,'2016-06-15 05:38:07');
/*!40000 ALTER TABLE `bcm_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Functional Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(2,'Application Evolution',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(3,'Technology Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(4,'Application Management',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(5,'Interface Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(6,'Server Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(7,'Cloud Readiness',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(8,'Business Benefit',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(9,'Maintenance Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(10,'Criticality Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(11,'Sourcing Alignment Index',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(12,'Utilization Quotient for Servers',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(13,'Infra Health Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(14,'Governance Quality',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(16,'',NULL,2,'2016-05-27 06:36:18',2,'2016-05-27 06:36:18'),(17,'TCO',NULL,1,'2016-08-29 09:06:30',1,'2016-08-29 09:06:30');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboardName` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DASHBOARD_WORKSPACE_idx` (`workspaceId`),
  CONSTRAINT `FK_DASHBOARD_WORKSPACE` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_log`
--

DROP TABLE IF EXISTS `export_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `purpose` varchar(45) NOT NULL,
  `refId` int(11) NOT NULL,
  `timeDown` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `export_userId_fk_idx` (`userId`),
  KEY `export_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `export_userId_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `export_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_log`
--

LOCK TABLES `export_log` WRITE;
/*!40000 ALTER TABLE `export_log` DISABLE KEYS */;
INSERT INTO `export_log` VALUES (2,'342d5ffbbf641bd47434bd1440b56386',1,2,'INV',1,'2016-04-20 10:27:45',NULL),(3,'07598197d906749f03e810bfb35795bb',1,2,'FM',2,'2016-04-20 12:39:24',NULL),(4,'a0b1b87b273daa490912d00225f32b8e',1,2,'FM',2,'2016-05-12 07:07:15',NULL),(5,'eb0418317fb6c67bdf1b5252754dccf7',1,2,'FM',2,'2016-05-12 07:27:02',NULL),(7,'b868a534ca3073af8a49764e15c10ac8',1,2,'FM',2,'2016-05-20 07:08:29',NULL),(9,'43717f975e6c1f601ac3aea77d242015',1,2,'FM',2,'2016-05-20 07:11:09','2016-05-20 07:20:14'),(11,'bea84aeeb65cb7caa9a8718c581041ea',1,2,'FM',2,'2016-05-23 10:31:16',NULL),(13,'f57997487dadcb17b114b81e2fa0c853',1,2,'FM',2,'2016-05-23 10:32:43','2016-05-23 10:34:08'),(14,'af5da77516f0778440e6750993185d18',1,2,'FM',2,'2016-05-27 07:16:45','2016-05-27 07:23:33'),(15,'7ce31f902675491ee47a4916cb724a09',1,2,'FM',2,'2016-05-27 09:44:13','2016-05-27 09:47:39'),(16,'ff9a1c8348b6b15ea78a5d7d305314a2',1,2,'QE',5,'2016-06-01 07:37:59',NULL),(17,'7fa3c45163f4ed0616276d5fb2b0153a',1,2,'INV',1,'2016-07-07 11:11:50','2016-07-07 11:13:34'),(18,'dfa1f8c1d33e3d6cfa91476bf5caa866',1,2,'INV',15,'2016-07-07 11:17:20',NULL),(19,'3ac8d5166e5e5a26b498ea4dba23b4c3',1,2,'INV',4,'2016-07-07 11:17:38','2016-07-07 11:18:05'),(20,'b54f2e4a18807775c01870d7ddd4f18a',1,2,'INV',2,'2016-07-07 11:23:21','2016-07-07 11:23:53'),(21,'4bb4537b60aaae4b743f0fbc100b7f7e',1,2,'INV',20,'2016-07-26 12:15:58',NULL),(22,'f6b77cc80c3091ccefb7bfd23937dea2',1,2,'INV',20,'2016-07-26 12:15:59',NULL),(23,'7518939446631226f89d9025b4cab653',1,2,'FM',1,'2016-08-10 07:49:55',NULL);
/*!40000 ALTER TABLE `export_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map`
--

DROP TABLE IF EXISTS `functional_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `bcmId` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fm_bcm_idx` (`bcmId`),
  KEY `fm_createdBy_idx` (`createdBy`),
  KEY `fm_updatedBy_idx` (`updatedBy`),
  KEY `fm_workspaceId_idx` (`workspaceId`),
  KEY `fm_assetTemplate_idx` (`assetTemplateId`),
  KEY `fm_question_idx` (`questionId`),
  CONSTRAINT `fm_assetTemplate` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_bcm` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_question` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map`
--

LOCK TABLES `functional_map` WRITE;
/*!40000 ALTER TABLE `functional_map` DISABLE KEYS */;
INSERT INTO `functional_map` VALUES (1,'Test FM',2,'L2',1,2,4,2,'2016-04-20 12:38:15',2,'2016-04-20 12:38:15'),(2,'Test FM1',2,'L4',1,3,4,2,'2016-04-20 12:38:15',2,'2016-05-27 07:13:46'),(3,'Test by Samar',2,'L1',1,3,5,2,'2016-06-08 06:10:15',2,'2016-06-08 06:10:34'),(4,'Test by Sunil',2,'L1',1,3,5,2,'2016-06-08 06:10:15',2,'2016-06-08 06:10:15');
/*!40000 ALTER TABLE `functional_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map_data`
--

DROP TABLE IF EXISTS `functional_map_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionalMapId` int(11) NOT NULL,
  `bcmLevelId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `data` text,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questionnaireId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fmd_bmfId_idx` (`functionalMapId`),
  KEY `fmd_bcmLevel_idx` (`bcmLevelId`),
  KEY `fmd_asset_idx` (`assetId`),
  KEY `fmd_userId_fk_idx` (`updatedBy`),
  KEY `questionnaireId` (`questionnaireId`),
  CONSTRAINT `fmd_asset_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_bcmLevel_fk` FOREIGN KEY (`bcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_fmId_fk` FOREIGN KEY (`functionalMapId`) REFERENCES `functional_map` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_userId_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `functional_map_data_ibfk_1` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map_data`
--

LOCK TABLES `functional_map_data` WRITE;
/*!40000 ALTER TABLE `functional_map_data` DISABLE KEYS */;
INSERT INTO `functional_map_data` VALUES (93,2,35,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(94,2,35,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(95,2,35,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(96,2,35,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(97,2,35,11,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(98,2,35,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(99,2,35,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(100,2,35,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(101,2,20,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(102,2,20,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(103,2,20,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(104,2,20,8,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(105,2,20,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(106,2,20,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(107,2,20,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(108,2,20,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(109,2,36,7,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(110,2,36,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(111,2,36,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(112,2,36,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(113,2,36,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(114,2,36,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(115,2,36,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(116,2,36,14,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(117,2,22,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(118,2,22,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(119,2,22,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(120,2,22,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(121,2,22,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(122,2,22,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(123,2,22,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(124,2,22,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(125,2,23,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(126,2,23,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(127,2,23,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(128,2,23,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(129,2,23,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(130,2,23,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(131,2,23,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(132,2,23,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(133,2,39,7,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(134,2,39,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(135,2,39,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(136,2,39,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(137,2,39,11,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(138,2,39,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(139,2,39,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(140,2,39,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(141,2,40,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(142,2,40,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(143,2,40,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(144,2,40,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(145,2,40,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(146,2,40,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(147,2,40,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(148,2,40,14,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(149,2,41,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(150,2,41,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(151,2,41,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(152,2,41,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(153,2,41,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(154,2,41,10,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(155,2,41,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(156,2,41,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(157,2,16,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(158,2,16,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(159,2,16,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(160,2,16,8,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(161,2,16,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(162,2,16,10,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(163,2,16,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(164,2,16,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL);
/*!40000 ALTER TABLE `functional_map_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_asset_template`
--

DROP TABLE IF EXISTS `graph_asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHID_ASSETTEMPLATEID` (`graphId`,`assetTemplateId`),
  KEY `FK_GRAPHID_GRAPHID_idx` (`graphId`),
  KEY `FK_ASSETTEMPLATEID_ASSETTEMPLATEID_idx` (`assetTemplateId`),
  CONSTRAINT `FK_ASSETTEMPLATEID_ASSETTEMPLATEID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GRAPHID_GRAPHID` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_asset_template`
--

LOCK TABLES `graph_asset_template` WRITE;
/*!40000 ALTER TABLE `graph_asset_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph_asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceAssetId` int(11) NOT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `interfaceDirection` varchar(45) NOT NULL,
  `dataType` varchar(20) DEFAULT NULL,
  `frequency` varchar(20) DEFAULT NULL,
  `communicationMethod` varchar(20) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_INTERFACE_SRC_DEST` (`interfaceAssetId`,`assetSourceId`,`assetDestId`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `FK_INTERFACEASSETID_ASSETID_idx` (`interfaceAssetId`),
  CONSTRAINT `FK_INTERFACEASSETID_ASSETID` FOREIGN KEY (`interfaceAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueName` varchar(45) NOT NULL,
  `displayName` varchar(250) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_uniqueName_workspaceId_uq` (`uniqueName`,`workspaceId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_parentParameterId_idx` (`parentParameterId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentParameterId` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (1,'Test Parameter-1','Test Param-1','This is the description of test que','LP',1,NULL,2,'2016-04-19 12:02:31',2,'2016-04-19 12:05:44'),(2,'Test Param-2','Display Test Parameter-2','desc of Test Param-2','LP',1,NULL,2,'2016-04-19 12:05:34',2,'2016-04-19 12:05:39'),(3,'Test Param-2 - 04-19 17:37:13','Display Test Parameter-2','desc of Test Param-2','LP',1,2,2,'2016-04-19 12:07:16',2,'2016-04-19 12:07:16'),(4,'Test Param-3','Display Test Param-3','Desc Test Param-3','LP',1,NULL,2,'2016-04-19 12:08:50',2,'2016-04-19 12:08:50'),(5,'IT Portfolio Survey - 1','dispaly test','rtret','AP',1,NULL,1,'2016-04-20 06:54:14',1,'2016-04-20 06:54:14'),(6,'Test Param-2 - 04-20 15:34:51 2nd','Display Test Parameter-2','desc of Test Param-2','LP',1,2,2,'2016-04-20 10:05:16',2,'2016-04-20 10:05:16'),(7,'IT Portfolio Survey - 1 - 04-20 15:36:35','dispaly test','rtret','AP',1,5,2,'2016-04-20 10:06:40',2,'2016-04-20 10:06:40'),(8,'IT Portfolio Survey - 1 - 04-20 15:36:351','dispaly test','rtret','AP',1,5,2,'2016-04-20 10:06:47',2,'2016-05-04 11:29:40'),(9,'Test T3 module createParam','Test T3 module createParam','Test T3 module createParam','LP',1,NULL,2,'2016-04-26 10:11:32',2,'2016-04-26 10:11:32'),(10,'Test for Shaishav','wef','df','AP',1,NULL,2,'2016-04-29 11:47:27',2,'2016-05-03 07:46:24'),(11,'tEst by samar','jgyj','ghhjghgj','AP',1,NULL,2,'2016-04-29 13:25:43',2,'2016-04-29 13:25:43'),(12,'tEst by samarerfdef','jgyj','ghhjghgj','AP',1,NULL,2,'2016-04-29 13:25:56',2,'2016-05-04 09:27:16'),(13,'Test to initiate','Test to initiate dispaly','Test to initiate desc','AP',1,NULL,2,'2016-05-02 06:32:40',2,'2016-05-02 06:53:39'),(14,'Test to initiate1','Test to initiate dispaly','Test to initiate desc','AP',1,NULL,2,'2016-05-02 06:36:24',2,'2016-05-02 06:36:24'),(23,'First Test world param','First Test world param display name','First Test world param description','LP',1,NULL,2,'2016-05-04 09:53:52',2,'2016-05-04 09:54:44'),(25,'First Test world param - 05-045','First Test world param display name','First Test world param description','LP',1,23,2,'2016-05-04 10:08:32',2,'2016-05-05 12:37:09'),(26,'First Test world param - 05-04 15:39:21','First Test world param display name','First Test world param description','LP',1,23,2,'2016-05-04 10:09:28',2,'2016-05-05 12:37:33'),(28,'IT Portfolio Survey - gdfgdfgdfg','fgfgdfg','gdfgfdg','AP',1,NULL,2,'2016-05-04 11:20:09',2,'2016-05-04 11:20:09'),(32,'dfgdfgfdggdfghrtyy567','dfddfg','ghfghfghfg','AP',1,NULL,2,'2016-05-04 11:29:15',2,'2016-05-05 12:37:42'),(35,'First for lp1','display lp test','eferg','LP',1,NULL,2,'2016-05-04 11:58:13',2,'2016-05-19 06:09:13'),(36,'Test for AP','ap diplau1','12','AP',1,NULL,2,'2016-05-04 12:00:05',2,'2016-05-04 12:02:46'),(37,'Test for AP - 05-04 17:30:51','ap diplau1','12','AP',1,36,2,'2016-05-04 12:01:09',2,'2016-08-05 07:11:26'),(39,'Level type','disp','desc','FC',1,NULL,2,'2016-05-05 11:40:37',2,'2016-05-05 12:49:15'),(40,'dsfsdffg','dfgfdfgdf','gdfgfdgdfg','FC',1,NULL,2,'2016-05-05 12:18:51',2,'2016-05-05 12:48:54'),(42,'Test by Samardeep','ghjghj1','ghjghj1','FC',1,NULL,2,'2016-05-05 12:22:08',2,'2016-05-06 10:46:05'),(43,'test23','dfdfgfh','','FC',1,NULL,2,'2016-05-05 12:23:03',2,'2016-05-05 12:39:23'),(44,'First Test world param - 05-04 15:39:21 - 05-','First Test world param display name','First Test world param description','LP',1,26,2,'2016-05-05 12:37:50',2,'2016-05-05 12:37:50'),(45,'dfdf','rgtdfgdfg','','FC',1,NULL,2,'2016-05-05 12:39:06',2,'2016-05-05 12:49:08'),(46,'dfgdfgbfg','hbfghfgh','hfghgfhgfh','FC',1,NULL,2,'2016-05-05 12:49:28',2,'2016-08-09 11:18:23'),(47,'Test by Ananta','Test by Ananta','Test by Ananta','LP',1,NULL,2,'2016-05-06 07:03:05',2,'2016-05-06 07:03:05'),(48,'test 3','dfv','cvcv','LP',1,NULL,2,'2016-05-06 07:04:43',2,'2016-05-06 07:04:43'),(49,'test 31','dfv','cvcv','LP',1,NULL,2,'2016-05-06 07:04:46',2,'2016-05-06 07:04:46'),(50,'test by ananta sethi','test by ananta sethi','test by ananta sethi','LP',1,NULL,2,'2016-05-06 07:12:44',2,'2016-05-06 07:12:44'),(51,'Test FC','Display Test FC','','FC',1,NULL,2,'2016-05-06 10:34:19',2,'2016-05-06 10:34:19'),(52,'Test for FC evaluation','Test for FC evaluation','Test for FC evaluation','FC',1,NULL,2,'2016-05-25 07:47:56',2,'2016-05-25 07:47:56'),(53,'Add FC in AP','Add FC in AP','Add FC in AP','AP',1,NULL,2,'2016-05-25 07:49:56',2,'2016-05-25 07:49:56'),(54,'IT tryrtytyrtSurvey - 1','rtrty','tyty','AP',1,NULL,2,'2016-05-27 12:05:00',2,'2016-05-27 12:05:21'),(55,'Test by Sunil','First FC param','sax','FC',1,NULL,2,'2016-05-30 06:39:22',2,'2016-05-30 06:39:39'),(56,'AP','AP','AP','AP',1,NULL,2,'2016-06-01 07:37:07',2,'2016-08-09 11:18:18'),(57,'IT Portfolio Survey - 1ghgh','dispaly test','gdfgfg','FC',1,NULL,2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(58,'Parameter Tree View','Parameter Tree View','Parameter Tree View','LP',1,NULL,2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(59,'Parameter Tree View - 06-15 10:37:09','Parameter Tree View','Parameter Tree View','LP',1,58,2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(61,'Parameter Tree View - 06-15 10:37:17','Parameter Tree View','Parameter Tree View','LP',1,58,2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(62,'Test by Samar1','Test by Samar display','','AP',1,NULL,2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:46'),(63,'dfgdfgfdggdfghrtyy567 - 08-05 12:44:44','dfddfg','ghfghfghfg','AP',1,32,2,'2016-08-05 07:14:47',2,'2016-08-09 11:18:35'),(64,'First for lp1 - 08-05 12:44:52','display lp test','eferg','LP',1,35,2,'2016-08-05 07:15:11',2,'2016-08-05 07:15:11'),(65,'LP parameter to check weight add','LP parameter to check weight addition display','LP parameter to check weight addition discr','LP',1,NULL,2,'2016-08-10 06:22:00',2,'2016-08-10 06:22:01'),(66,'LP to test1','dispaly test434','','LP',1,NULL,2,'2016-08-10 06:47:40',2,'2016-08-10 07:11:36'),(67,'45yt45y','yrtyrty','','LP',1,NULL,2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(68,'test1-lp1','ttestt','','LP',1,NULL,2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(69,'Test-LP2','test -lp2','','LP',1,NULL,2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(70,'FC test by Samar','FC test by Samar','','FC',1,NULL,2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(71,'TCO-A','TCO-A','TCO-A','TCOA',1,NULL,2,'2016-09-01 10:16:32',2,'2016-09-01 10:16:32'),(72,'Test TCO_A','Test TCO_A','Test TCO_A','TCOA',1,NULL,2,'2016-09-01 10:18:20',2,'2016-09-01 10:18:20'),(73,'Test TCO_A1','Test TCO_A1','Test TCO_A1','TCOA',1,NULL,2,'2016-09-01 10:19:45',2,'2016-09-01 10:19:45'),(74,'Test TCO_A3','Test TCO_A3','Test TCO_A3','TCOA',1,NULL,2,'2016-09-01 12:30:57',2,'2016-09-01 12:30:57'),(75,'Test TCO_A4','Test TCO_A4','Test TCO_A4','TCOA',1,NULL,2,'2016-09-01 12:35:15',2,'2016-09-01 12:35:15'),(76,'Test TCO_A5','Test TCO_A5','Test TCO_A5','TCOA',1,NULL,2,'2016-09-01 12:36:48',2,'2016-09-01 12:36:48'),(79,'Test TCO_A6','Test TCO_A5','Test TCO_A5','TCOA',1,NULL,2,'2016-09-01 12:40:52',2,'2016-09-01 12:40:52'),(80,'Test TCO_A7','Test TCO_A7','Test TCO_A7','TCOA',1,NULL,2,'2016-09-01 12:50:15',2,'2016-09-01 12:50:15'),(81,'Test TCO_A8','Test TCO_A8','Test TCO_A8','TCOA',1,NULL,2,'2016-09-01 12:51:25',2,'2016-09-01 12:51:25'),(82,'45yt45y - 09-01 18:43:39','yrtyrty','','LP',1,67,2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55'),(83,'Test TCO_A11','Test TCO_A11','Test TCO_A11','TCOA',1,NULL,2,'2016-09-01 13:33:28',2,'2016-09-01 13:33:28'),(84,'Test TCO_A12','Test TCO_A12','Test TCO_A12','TCOA',1,NULL,2,'2016-09-02 05:24:41',2,'2016-09-02 05:24:41'),(85,'Test TCO_A13','Test TCO_A13','Test TCO_A13','TCOA',1,NULL,2,'2016-09-02 05:25:44',2,'2016-09-02 05:25:44'),(86,'Test TCO_A14','Test TCO_A14','Test TCO_A14','TCOA',1,NULL,2,'2016-09-02 05:26:55',2,'2016-09-02 05:26:55'),(87,'Hello JSTree','Hello JSTree','Hello JSTree','TCOA',1,NULL,2,'2016-09-02 10:26:30',2,'2016-09-02 10:26:30');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_config`
--

DROP TABLE IF EXISTS `parameter_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `parameterConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pc_parameterId_idx` (`parameterId`),
  CONSTRAINT `pc_parameterId` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_config`
--

LOCK TABLES `parameter_config` WRITE;
/*!40000 ALTER TABLE `parameter_config` DISABLE KEYS */;
INSERT INTO `parameter_config` VALUES (9,39,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(10,40,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L2\"}'),(11,42,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(12,43,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L2\"}'),(13,45,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(14,46,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}'),(15,51,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(16,52,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}'),(17,55,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L1\"}'),(18,70,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}');
/*!40000 ALTER TABLE `parameter_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL DEFAULT '1',
  `questionId` int(11) DEFAULT NULL,
  `mandatoryQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `childparameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pf_parentParam_que_childParam_uq` (`parentParameterId`,`questionId`,`childparameterId`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_function`
--

LOCK TABLES `parameter_function` WRITE;
/*!40000 ALTER TABLE `parameter_function` DISABLE KEYS */;
INSERT INTO `parameter_function` VALUES (4,2,0.200,0,1,1,NULL),(5,2,0.300,0,2,0,NULL),(6,1,0.100,0,1,0,NULL),(7,3,0.200,0,1,1,NULL),(8,3,0.300,0,2,0,NULL),(9,4,0.200,0,2,1,NULL),(10,5,0.100,0,NULL,0,1),(11,5,0.200,0,NULL,0,2),(12,6,0.300,0,1,1,NULL),(13,6,0.500,0,2,0,NULL),(14,7,0.100,0,NULL,0,1),(15,7,0.200,0,NULL,0,2),(16,8,0.400,0,NULL,0,1),(17,8,0.600,0,NULL,0,2),(18,9,0.200,0,1,1,NULL),(21,11,0.200,0,NULL,0,2),(22,11,0.300,0,NULL,0,3),(25,14,0.200,0,NULL,0,1),(26,14,0.300,0,NULL,0,2),(27,13,0.200,0,NULL,0,3),(28,13,0.300,0,NULL,0,4),(29,10,0.200,0,NULL,0,1),(30,10,0.300,0,NULL,0,2),(31,12,0.200,0,NULL,0,2),(32,12,0.300,0,NULL,0,3),(33,23,0.700,0,1,0,NULL),(34,23,0.200,0,2,1,NULL),(37,25,0.900,0,1,0,NULL),(38,25,0.100,0,2,1,NULL),(39,26,0.600,0,1,0,NULL),(40,26,0.400,0,2,1,NULL),(43,28,0.200,0,NULL,0,2),(44,28,0.200,0,NULL,0,3),(45,32,0.500,0,NULL,0,3),(46,32,0.500,0,NULL,0,7),(49,35,0.200,0,1,0,NULL),(50,35,0.800,0,2,1,NULL),(51,36,0.200,0,NULL,0,1),(52,36,0.300,0,NULL,0,2),(53,36,0.200,0,NULL,0,3),(54,36,0.300,0,NULL,0,6),(55,37,0.600,0,NULL,0,1),(56,37,0.100,0,NULL,0,2),(57,37,0.100,0,NULL,0,3),(58,37,0.200,0,NULL,0,6),(61,44,0.600,0,1,0,NULL),(62,44,0.400,0,2,1,NULL),(63,47,1.000,0,2,1,NULL),(64,48,1.000,0,4,0,NULL),(65,49,1.000,0,4,0,NULL),(66,50,1.000,0,5,1,NULL),(67,53,0.200,0,NULL,0,35),(68,53,0.300,0,NULL,0,36),(69,53,0.500,0,NULL,0,52),(70,54,0.600,0,NULL,0,3),(71,54,0.400,0,NULL,0,4),(72,56,1.000,0,NULL,0,51),(73,58,0.500,0,1,1,NULL),(74,58,0.500,0,3,0,NULL),(75,59,0.500,0,1,1,NULL),(76,59,0.500,0,3,0,NULL),(77,62,0.500,0,NULL,0,1),(78,62,0.500,0,NULL,0,2),(79,63,0.600,0,NULL,0,3),(80,63,0.400,0,NULL,0,7),(81,64,0.400,0,1,0,NULL),(82,64,0.600,0,2,1,NULL),(83,65,0.600,0,1,1,NULL),(84,65,0.100,0,2,0,NULL),(85,65,0.300,0,3,0,NULL),(86,66,0.600,0,1,0,NULL),(87,66,0.100,0,2,0,NULL),(88,66,0.300,0,3,0,NULL),(89,67,0.300,0,1,0,NULL),(90,67,0.600,0,2,0,NULL),(91,67,0.100,0,3,0,NULL),(92,68,0.600,0,2,0,NULL),(93,68,0.300,0,3,0,NULL),(94,68,0.100,0,4,0,NULL),(95,69,0.300,0,2,0,NULL),(96,69,0.300,0,3,0,NULL),(97,69,0.300,0,4,0,NULL),(98,69,0.100,0,5,0,NULL),(99,82,0.300,0,1,0,NULL),(100,82,0.600,0,2,0,NULL),(101,82,0.100,0,3,0,NULL);
/*!40000 ALTER TABLE `parameter_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_quality_gate`
--

DROP TABLE IF EXISTS `parameter_quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `qualityGateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `qualityGate_parameter_fk_idx` (`parameterId`),
  KEY `qgp_qualityGate_fk_idx` (`qualityGateId`),
  CONSTRAINT `qgp_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgp_qualityGate_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_quality_gate`
--

LOCK TABLES `parameter_quality_gate` WRITE;
/*!40000 ALTER TABLE `parameter_quality_gate` DISABLE KEYS */;
INSERT INTO `parameter_quality_gate` VALUES (1,57,13),(2,58,14),(3,59,15),(4,61,16),(5,62,17),(6,63,18),(7,64,19),(8,65,20),(9,66,21),(10,67,22),(11,68,23),(12,69,24),(13,70,25),(14,82,26);
/*!40000 ALTER TABLE `parameter_quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate`
--

DROP TABLE IF EXISTS `quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qg_assetType_parameter_workspace_uq` (`workspaceId`,`name`),
  KEY `qg_workspace_fk_idx` (`workspaceId`),
  KEY `qg_createdBy_fk_idx` (`createdBy`),
  KEY `qg_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qg_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_workspace_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate`
--

LOCK TABLES `quality_gate` WRITE;
/*!40000 ALTER TABLE `quality_gate` DISABLE KEYS */;
INSERT INTO `quality_gate` VALUES (10,'Test QG',1,2,'2016-05-09 04:35:01',2,'2016-05-09 04:35:01'),(11,'tej test',1,2,'2016-05-26 09:32:50',2,'2016-05-26 09:32:50'),(12,'lknjkl',1,2,'2016-05-27 07:37:47',2,'2016-05-27 07:37:47'),(13,'QG_IT Portfolio Survey - 1ghgh',1,2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(14,'QG_Parameter Tree View',1,2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(15,'QG_Parameter Tree View - 06-15 10:37:09',1,2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(16,'QG_Parameter Tree View - 06-15 10:37:17',1,2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(17,'QG_Test by Samar1',1,2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:17'),(18,'QG_dfgdfgfdggdfghrtyy567 - 08-05 12:44:44',1,2,'2016-08-05 07:14:47',2,'2016-08-05 07:14:47'),(19,'QG_First for lp1 - 08-05 12:44:52',1,2,'2016-08-05 07:15:11',2,'2016-08-05 07:15:11'),(20,'QG_LP parameter to check weight add',1,2,'2016-08-10 06:22:01',2,'2016-08-10 06:22:01'),(21,'QG_LP to test1',1,2,'2016-08-10 06:47:41',2,'2016-08-10 06:47:41'),(22,'QG_45yt45y',1,2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(23,'QG_test1-lp1',1,2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(24,'QG_Test-LP2',1,2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(25,'QG_FC test by Samar',1,2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(26,'QG_45yt45y - 09-01 18:43:39',1,2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55');
/*!40000 ALTER TABLE `quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate_condition`
--

DROP TABLE IF EXISTS `quality_gate_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityGateId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `weight` decimal(10,3) DEFAULT NULL,
  `gateCondition` varchar(45) NOT NULL,
  `conditionValues` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qgc_parameter_fk_idx` (`parameterId`),
  KEY `qgc_qg_fk_idx` (`qualityGateId`),
  KEY `qgc_createdBy_fk_idx` (`createdBy`),
  KEY `qgc_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qgc_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_qg_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate_condition`
--

LOCK TABLES `quality_gate_condition` WRITE;
/*!40000 ALTER TABLE `quality_gate_condition` DISABLE KEYS */;
INSERT INTO `quality_gate_condition` VALUES (1,10,2,0.400,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-09 05:12:58',2,'2016-05-09 05:12:58'),(2,10,2,0.300,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-09 05:13:44',2,'2016-05-09 05:13:44'),(3,11,53,1.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":3.0},{\"color\":\"#ffffbf\",\"value\":6.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-26 09:34:37',2,'2016-05-26 09:34:37'),(4,11,35,3.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-26 09:38:32',2,'2016-05-26 09:38:32'),(5,12,6,1.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":1.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-27 07:38:15',2,'2016-05-27 07:38:15'),(6,13,57,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(7,14,58,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(8,15,59,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(9,16,61,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(10,17,62,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:17'),(11,18,63,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-05 07:14:47',2,'2016-08-05 07:14:47'),(12,19,64,NULL,'LESS','{\"values\":[{\"color\":\"#d7191c\",\"value\":1.0},{\"color\":\"#fdae61\",\"value\":1.0},{\"color\":\"#a6d96a\",\"value\":3.0},{\"color\":\"#1a9641\",\"value\":null}]}',2,'2016-08-05 07:15:11',2,'2016-08-05 07:35:05'),(13,20,65,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 06:22:01',2,'2016-08-10 06:22:01'),(14,21,66,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 06:47:41',2,'2016-08-10 06:47:41'),(15,22,67,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(16,23,68,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(17,24,69,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(18,25,70,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(19,26,82,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55');
/*!40000 ALTER TABLE `quality_gate_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentQuesId` int(11) DEFAULT NULL,
  `questionMode` varchar(20) NOT NULL,
  `title` text NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ques_workspace_displayName_uq` (`workspaceId`,`displayName`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  KEY `ques_quesId_fk_idx` (`parentQuesId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ques_quesId_fk` FOREIGN KEY (`parentQuesId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,NULL,'FIXED','This is title of test que','This is the test question?','Testing Que',1,'TEXT',NULL,0,1,2,'2016-04-19 12:00:37',2,'2016-04-19 12:00:37'),(2,NULL,'FIXED','This is the test question-1','Test Question - 1','',1,'PARATEXT',NULL,0,2,2,'2016-04-19 12:04:26',2,'2016-04-19 12:04:26'),(3,NULL,'FIXED','Another Test Widget','dispaly test','This is Test',1,'TEXT',NULL,0,2,1,'2016-04-20 06:53:24',1,'2016-04-20 06:53:24'),(4,NULL,'MODIFIABLE','This is test que 1','This is test que','This is test que 2',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,2,2,'2016-05-27 09:43:18',2,'2016-06-14 11:28:49'),(5,NULL,'MODIFIABLE','test by ananta','test by ananta','test by ananta',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,1,2,'2016-05-06 07:12:00',2,'2016-05-12 09:21:11'),(6,NULL,'Fixed','Marketing','Marketing','Marketing',1,'CURRENCY',NULL,0,NULL,2,'2016-09-01 11:12:58',2,'2016-09-01 11:12:58');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  KEY `questionnaire_assetTypeId_fk_idx` (`assetTypeId`),
  CONSTRAINT `questionnaire_assetTypeId_fk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (1,'Test by Sunil','this is the test',1,'EDITABLE',1,2,'2016-09-05 07:07:28',2,'2016-09-05 07:07:28'),(2,'Test for Dhruv','',1,'EDITABLE',1,2,'2016-09-05 07:07:28',2,'2016-09-05 07:07:28'),(5,'Tej 1447','',1,'CLOSED',1,2,'2016-09-05 07:07:28',2,'2016-09-05 07:07:28'),(6,'Test for FC Evaluation','',1,'EDITABLE',1,2,'2016-09-05 07:07:28',2,'2016-09-05 07:07:28'),(7,'Twest saf','',1,'CLOSED',1,2,'2016-09-05 07:07:28',2,'2016-09-05 07:07:28');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_asset`
--

LOCK TABLES `questionnaire_asset` WRITE;
/*!40000 ALTER TABLE `questionnaire_asset` DISABLE KEYS */;
INSERT INTO `questionnaire_asset` VALUES (1,1,2),(3,1,3),(2,2,1),(16,5,5),(17,5,9),(8,6,7),(9,6,8),(15,6,9),(10,6,10),(11,6,11),(12,6,12),(13,6,13),(14,6,14);
/*!40000 ALTER TABLE `questionnaire_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_parameter`
--

DROP TABLE IF EXISTS `questionnaire_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `rootParameterId` int(11) NOT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId`,`parameterId`,`parentParameterId`,`rootParameterId`),
  KEY `qp_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `qp_parameterId_fk_idx` (`parameterId`),
  KEY `qp_rootParameterId_fk_idx` (`rootParameterId`),
  KEY `qp_parentParameterId_fk_idx` (`parentParameterId`),
  CONSTRAINT `qp_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_rootParameterId_fk` FOREIGN KEY (`rootParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_parameter`
--

LOCK TABLES `questionnaire_parameter` WRITE;
/*!40000 ALTER TABLE `questionnaire_parameter` DISABLE KEYS */;
INSERT INTO `questionnaire_parameter` VALUES (41,1,1,36,36),(31,1,1,53,36),(43,1,2,36,36),(29,1,2,53,36),(42,1,3,36,36),(32,1,3,53,36),(40,1,6,36,36),(30,1,6,53,36),(38,1,35,35,NULL),(35,1,35,53,53),(39,1,36,36,NULL),(33,1,36,53,53),(34,1,52,53,53),(28,1,53,53,NULL),(13,2,6,6,NULL),(15,2,6,6,NULL),(16,2,23,23,NULL),(58,5,2,2,NULL),(59,5,3,3,NULL),(60,5,9,9,NULL),(57,5,50,50,NULL),(47,6,1,53,36),(48,6,2,53,36),(46,6,3,53,36),(45,6,6,53,36),(52,6,35,35,NULL),(51,6,35,53,53),(49,6,36,53,53),(50,6,52,53,53),(44,6,53,53,NULL);
/*!40000 ALTER TABLE `questionnaire_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `questionnaireParameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qq_QEId_QId_QAId_QPId_uq` (`questionnaireId`,`questionId`,`questionnaireAssetId`,`questionnaireParameterId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `qq_questionnaireParameterId_fk_idx` (`questionnaireParameterId`),
  CONSTRAINT `qq_questionnaireParameterId_fk` FOREIGN KEY (`questionnaireParameterId`) REFERENCES `questionnaire_parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
INSERT INTO `questionnaire_question` VALUES (19,2,1,2,13,1),(20,2,2,2,13,2),(21,2,1,2,15,3),(22,2,2,2,15,4),(23,2,1,2,16,5),(24,2,2,2,16,6),(35,1,1,1,35,1),(37,1,2,1,35,2),(38,1,1,1,29,3),(39,1,2,1,29,4),(40,1,1,1,30,5),(41,1,2,1,30,6),(42,1,1,1,31,7),(43,1,1,1,32,8),(44,1,2,1,32,9),(45,1,1,3,35,10),(46,1,2,3,35,11),(47,1,1,3,29,12),(48,1,2,3,29,13),(49,1,1,3,30,14),(50,1,2,3,30,15),(51,1,1,3,31,16),(52,1,1,3,32,17),(53,1,2,3,32,18),(63,1,1,1,38,28),(64,1,2,1,38,29),(65,1,1,1,40,30),(66,1,2,1,40,31),(67,1,1,1,41,32),(68,1,1,1,42,33),(69,1,2,1,42,34),(70,1,1,1,43,35),(71,1,2,1,43,36),(72,1,1,3,38,37),(73,1,2,3,38,38),(74,1,1,3,40,39),(75,1,2,3,40,40),(76,1,1,3,41,41),(77,1,1,3,42,42),(78,1,2,3,42,43),(79,1,1,3,43,44),(80,1,2,3,43,45),(104,6,1,8,51,1),(105,6,2,8,51,2),(106,6,1,8,52,3),(107,6,2,8,52,4),(108,6,1,8,45,5),(109,6,2,8,45,6),(110,6,1,8,46,7),(111,6,2,8,46,8),(112,6,1,8,47,9),(113,6,1,8,48,10),(114,6,2,8,48,11),(115,6,1,9,51,12),(116,6,2,9,51,13),(117,6,1,9,52,14),(118,6,2,9,52,15),(119,6,1,9,45,16),(120,6,2,9,45,17),(121,6,1,9,46,18),(122,6,2,9,46,19),(123,6,1,9,47,20),(124,6,1,9,48,21),(125,6,2,9,48,22),(126,6,1,10,51,23),(127,6,2,10,51,24),(128,6,1,10,52,25),(129,6,2,10,52,26),(130,6,1,10,45,27),(131,6,2,10,45,28),(132,6,1,10,46,29),(133,6,2,10,46,30),(134,6,1,10,47,31),(135,6,1,10,48,32),(136,6,2,10,48,33),(137,6,1,11,51,34),(138,6,2,11,51,35),(139,6,1,11,52,36),(140,6,2,11,52,37),(141,6,1,11,45,38),(142,6,2,11,45,39),(143,6,1,11,46,40),(144,6,2,11,46,41),(145,6,1,11,47,42),(146,6,1,11,48,43),(147,6,2,11,48,44),(148,6,1,12,51,45),(149,6,2,12,51,46),(150,6,1,12,52,47),(151,6,2,12,52,48),(152,6,1,12,45,49),(153,6,2,12,45,50),(154,6,1,12,46,51),(155,6,2,12,46,52),(156,6,1,12,47,53),(157,6,1,12,48,54),(158,6,2,12,48,55),(159,6,1,13,51,56),(160,6,2,13,51,57),(161,6,1,13,52,58),(162,6,2,13,52,59),(163,6,1,13,45,60),(164,6,2,13,45,61),(165,6,1,13,46,62),(166,6,2,13,46,63),(167,6,1,13,47,64),(168,6,1,13,48,65),(169,6,2,13,48,66),(170,6,1,14,51,67),(171,6,2,14,51,68),(172,6,1,14,52,69),(173,6,2,14,52,70),(174,6,1,14,45,71),(175,6,2,14,45,72),(176,6,1,14,46,73),(177,6,2,14,46,74),(178,6,1,14,47,75),(179,6,1,14,48,76),(180,6,2,14,48,77),(181,6,1,15,51,78),(182,6,2,15,51,79),(183,6,1,15,52,80),(184,6,2,15,52,81),(185,6,1,15,45,82),(186,6,2,15,45,83),(187,6,1,15,46,84),(188,6,2,15,46,85),(189,6,1,15,47,86),(190,6,1,15,48,87),(191,6,2,15,48,88),(192,5,5,17,57,1),(193,5,5,16,57,2);
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset`
--

DROP TABLE IF EXISTS `relationship_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionId` int(11) NOT NULL,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `relationship_asset_relationshipSuggestionId_fk_idx` (`relationshipSuggestionId`),
  KEY `relationship_asset_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `relationship_asset_childAssetId_fk_idx` (`childAssetId`),
  KEY `relationship_asset_createdBy_fk_idx` (`createdBy`),
  KEY `relationship_asset_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset`
--

LOCK TABLES `relationship_asset` WRITE;
/*!40000 ALTER TABLE `relationship_asset` DISABLE KEYS */;
INSERT INTO `relationship_asset` VALUES (1,1,2,5,2,'2016-06-14 11:00:32',2,'2016-06-14 11:00:32'),(2,1,2,5,2,'2016-06-14 11:00:32',2,'2016-06-14 11:00:32');
/*!40000 ALTER TABLE `relationship_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset_data`
--

DROP TABLE IF EXISTS `relationship_asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rad_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `rad_childAssetId_fk_idx` (`childAssetId`),
  KEY `rad_assetId_fk_idx` (`assetId`),
  CONSTRAINT `rad_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset_data`
--

LOCK TABLES `relationship_asset_data` WRITE;
/*!40000 ALTER TABLE `relationship_asset_data` DISABLE KEYS */;
INSERT INTO `relationship_asset_data` VALUES (1,1,3,48),(2,12,9,50),(3,1,2,54),(4,28,4,57),(5,37,4,59),(6,36,12,60),(7,16,6,61),(8,32,42,62),(9,10,40,64),(10,28,13,65),(11,14,31,66),(12,26,14,67),(13,18,14,68),(14,31,33,70),(15,14,9,72),(16,21,14,73),(17,11,12,75),(18,34,30,79),(19,1,2,80),(20,33,51,82),(21,1,42,83),(22,1,36,84),(23,51,2,85);
/*!40000 ALTER TABLE `relationship_asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_attribute_data`
--

DROP TABLE IF EXISTS `relationship_attribute_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_attribute_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionAttributeId` int(11) NOT NULL,
  `relationshipAssetId` int(11) NOT NULL,
  `attributeData` text,
  PRIMARY KEY (`id`),
  KEY `rad_relationshipSuggestionAttributeId_fk_idx` (`relationshipSuggestionAttributeId`),
  KEY `rad_relationshipAssetId_fk_idx` (`relationshipAssetId`),
  CONSTRAINT `rad_relationshipAssetId_fk` FOREIGN KEY (`relationshipAssetId`) REFERENCES `relationship_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_relationshipSuggestionAttributeId_fk` FOREIGN KEY (`relationshipSuggestionAttributeId`) REFERENCES `relationship_suggestion_attribute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_attribute_data`
--

LOCK TABLES `relationship_attribute_data` WRITE;
/*!40000 ALTER TABLE `relationship_attribute_data` DISABLE KEYS */;
INSERT INTO `relationship_attribute_data` VALUES (1,2,1,'GIC Db'),(2,1,1,'Google'),(3,2,2,'GIC Db'),(4,1,2,'Google');
/*!40000 ALTER TABLE `relationship_attribute_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion`
--

DROP TABLE IF EXISTS `relationship_suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rs_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `rs_parentAssetTypeId_idx` (`parentAssetTypeId`),
  KEY `rs_childAssetTypeId_idx` (`childAssetTypeId`),
  KEY `rs_relationshipTypeId_idx` (`relationshipTypeId`),
  KEY `rs_workspaceId_idx` (`workspaceId`),
  KEY `rs_createdBy_idx` (`createdBy`),
  KEY `rs_updatedBy_idx` (`updatedBy`),
  CONSTRAINT `rs_childAssetTypeId` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_parentAssetTypeId` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_relationshipTypeId` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion`
--

LOCK TABLES `relationship_suggestion` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion` DISABLE KEYS */;
INSERT INTO `relationship_suggestion` VALUES (1,1,1,2,'Application Runs :: Runs on Server',1,0,2,'2016-04-20 10:31:12',2,'2016-04-20 10:31:12'),(2,1,1,1,'Application Runs :: Runs on Application',1,0,2,'2016-06-13 13:44:00',2,'2016-06-13 13:44:00'),(3,1,1,4,'Application Runs :: Runs on Person',1,0,2,'2016-06-14 10:55:52',2,'2016-06-14 10:55:52'),(4,1,1,5,'Application Runs :: Runs on Business Services',1,0,2,'2016-06-15 05:06:36',2,'2016-06-15 05:06:36');
/*!40000 ALTER TABLE `relationship_suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion_attribute`
--

DROP TABLE IF EXISTS `relationship_suggestion_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributeName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(5) DEFAULT NULL,
  `relationshipSuggestionId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ra_uq_attributeName_rSuggestionId1` (`relationshipSuggestionId`,`attributeName`),
  KEY `ra_relationshipSuggestionId_idx` (`relationshipSuggestionId`),
  KEY `ra_createdBy_fk_idx` (`createdBy`),
  KEY `ra_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion_attribute`
--

LOCK TABLES `relationship_suggestion_attribute` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion_attribute` DISABLE KEYS */;
INSERT INTO `relationship_suggestion_attribute` VALUES (1,'Name','TEXT',100,1,0,1,1,2,'2016-04-20 10:31:12',2,'2016-04-20 10:31:12'),(2,'Desc','TEXT',200,1,0,0,2,2,'2016-04-20 10:31:37',2,'2016-04-20 10:31:37'),(3,'Name','TEXT',100,2,0,1,1,2,'2016-06-13 13:44:00',2,'2016-06-13 13:44:00'),(4,'Name','TEXT',100,3,0,1,1,2,'2016-06-14 10:55:52',2,'2016-06-14 10:55:52'),(5,'Name','TEXT',100,4,0,1,1,2,'2016-06-15 05:06:36',2,'2016-06-15 05:06:36');
/*!40000 ALTER TABLE `relationship_suggestion_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_template`
--

DROP TABLE IF EXISTS `relationship_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `hasAttribute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_template_displayName_assetTemplateId_uq` (`displayName`,`assetTemplateId`),
  UNIQUE KEY `assetTemplateId_UNIQUE` (`assetTemplateId`),
  KEY `relationship_template_parentAssetTypeId_fk_idx` (`parentAssetTypeId`),
  KEY `relationship_template_childAssetTypeId_fk_idx` (`childAssetTypeId`),
  KEY `relationship_template_relationshipTypeId_fk_idx` (`relationshipTypeId`),
  KEY `relationship_template_assetTemplateId_fk_idx` (`assetTemplateId`),
  CONSTRAINT `relationship_template_assetTemplateId_fk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_childAssetTypeId_fk` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_parentAssetTypeId_fk` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_relationshipTypeId_fk` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_template`
--

LOCK TABLES `relationship_template` WRITE;
/*!40000 ALTER TABLE `relationship_template` DISABLE KEYS */;
INSERT INTO `relationship_template` VALUES (2,1,1,2,'Application Runs :: Runs on Server',10,1),(3,2,1,5,'Server Runs :: Runs on Business Services',11,0),(4,4,1,5,'Person Runs :: Runs on Business Services',12,1),(5,6,1,8,'IT Services Runs :: Runs on Relationship',13,1),(6,1,1,1,'Application Runs :: Runs on Application',14,0),(7,5,1,4,'Business Services Runs :: Runs on Person',15,0),(8,1,1,3,'Application Runs :: Runs on Database',16,1),(9,2,1,1,'Server Runs :: Runs on Application',17,0),(10,1,1,2,'Application Runs :: Runs on Server',19,1),(11,1,1,2,'Application Runs :: Runs on Server',20,1),(12,1,1,2,'Application Runs :: Runs on Server',21,1),(13,1,1,2,'Application Runs :: Runs on Server',22,0),(16,1,1,2,'Application Runs :: Runs on Server',30,1),(17,1,2,4,'Application Allocated from :: Allocated to Person',31,1),(18,3,5,7,'Database Use :: Used by Interfaces',32,0);
/*!40000 ALTER TABLE `relationship_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_type`
--

DROP TABLE IF EXISTS `relationship_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentDescriptor` varchar(50) NOT NULL,
  `childDescriptor` varchar(50) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `displayName` varchar(150) NOT NULL,
  `direction` varchar(4) NOT NULL DEFAULT 'UNI',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_type_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `relationship_type_createdBy_idx` (`createdBy`),
  KEY `relationship_type_updatedBy_idx` (`updatedBy`),
  KEY `relationship_type_workspaceId_idx` (`workspaceId`),
  CONSTRAINT `relationship_type_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_type`
--

LOCK TABLES `relationship_type` WRITE;
/*!40000 ALTER TABLE `relationship_type` DISABLE KEYS */;
INSERT INTO `relationship_type` VALUES (1,'Runs','Runs on',1,0,'Runs :: Runs on','UNI',2,'2016-07-18 11:11:05',2,'2016-07-18 11:11:05'),(2,'Allocated from','Allocated to',1,0,'Allocated from :: Allocated to','UNI',2,'2016-07-07 07:14:59',2,'2016-07-07 07:14:59'),(3,'Allocated to','Runs on',1,0,'Allocated to :: Runs on','UNI',2,'2016-07-18 11:11:05',2,'2016-07-18 11:11:05'),(4,'Runs','Runs on',NULL,0,'Runs :: Runs on','UNI',2,'2016-08-12 07:05:21',2,'2016-08-12 07:05:21'),(5,'Use','Used by',NULL,0,'Use :: Used by','BIDI',2,'2016-08-12 10:38:56',2,'2016-08-12 10:38:56');
/*!40000 ALTER TABLE `relationship_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (1,1,2,'2016-04-26 05:12:59',2,'2016-06-17 07:01:30'),(2,2,2,'2016-05-06 07:00:52',2,'2016-06-14 11:15:41'),(3,6,2,'2016-05-27 07:14:37',2,'2016-05-27 07:47:55'),(4,5,2,'2016-06-01 07:37:58',2,'2016-06-14 11:28:04');
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
INSERT INTO `response_data` VALUES (1,19,2,'{\"responseText\":\"test5\",\"score\":null}'),(2,20,2,'{\"responseParaText\":\"ggfgdfg\",\"score\":null}'),(14,104,3,'{\"responseText\":\"jio\",\"score\":1.0}'),(15,105,3,'{\"responseParaText\":\"hih\",\"score\":2.0}'),(16,106,3,'{\"responseText\":\"ih\",\"score\":3.0}'),(17,107,3,'{\"responseParaText\":\"ihi\",\"score\":6.0}'),(18,108,3,'{\"responseText\":\"jhih\",\"score\":6.0}'),(19,109,3,'{\"responseParaText\":\"igh\",\"score\":2.0}'),(20,110,3,'{\"responseText\":\"uhg\",\"score\":8.0}'),(21,111,3,'{\"responseParaText\":\"ydt\",\"score\":9.0}'),(22,112,3,'{\"responseText\":\"dh\",\"score\":2.0}'),(23,113,3,'{\"responseText\":\"hiou\",\"score\":4.0}'),(24,114,3,'{\"responseParaText\":\"iugg\",\"score\":7.0}'),(25,115,3,'{\"responseText\":\"ug\",\"score\":2.0}'),(26,116,3,'{\"responseParaText\":\"iuuuu\",\"score\":8.0}'),(27,117,3,'{\"responseText\":\"gtyfg\",\"score\":4.0}'),(28,118,3,'{\"responseParaText\":\"guih\",\"score\":1.0}'),(29,119,3,'{\"responseText\":\"uf\",\"score\":8.0}'),(30,120,3,'{\"responseParaText\":\"fu\",\"score\":8.0}'),(31,121,3,'{\"responseText\":\"ghguy\",\"score\":3.0}'),(32,122,3,'{\"responseParaText\":\"ftf\",\"score\":4.0}'),(33,123,3,'{\"responseText\":\"8\",\"score\":8.0}'),(34,124,3,'{\"responseText\":\"ufgyt\",\"score\":9.0}'),(35,125,3,'{\"responseParaText\":\"f\",\"score\":3.0}'),(36,126,3,'{\"responseText\":\"hu\",\"score\":6.0}'),(37,127,3,'{\"responseParaText\":\"fyf\",\"score\":6.0}'),(38,128,3,'{\"responseText\":\"y\",\"score\":3.0}'),(39,129,3,'{\"responseParaText\":\"tuf\",\"score\":3.0}'),(40,130,3,'{\"responseText\":\"yf\",\"score\":7.0}'),(41,131,3,'{\"responseParaText\":\"uyuf\",\"score\":8.0}'),(42,132,3,'{\"responseText\":\"\",\"score\":9.0}'),(43,133,3,'{\"responseParaText\":\"ioiu\",\"score\":6.0}'),(44,134,3,'{\"responseText\":\"ufuy\",\"score\":2.0}'),(45,135,3,'{\"responseText\":\"huy8\",\"score\":6.0}'),(46,136,3,'{\"responseParaText\":\"yg\",\"score\":2.0}'),(47,137,3,'{\"responseText\":\"ug\",\"score\":6.0}'),(48,138,3,'{\"responseParaText\":\"tyfuyguyf\",\"score\":4.0}'),(49,139,3,'{\"responseText\":\"yguyg\",\"score\":7.0}'),(50,140,3,'{\"responseParaText\":\"fguy\",\"score\":1.0}'),(51,141,3,'{\"responseText\":\"g7f\",\"score\":9.0}'),(52,142,3,'{\"responseParaText\":\"uyg\",\"score\":4.0}'),(53,143,3,'{\"responseText\":\"fgy\",\"score\":4.0}'),(54,144,3,'{\"responseParaText\":\"guy\",\"score\":null}'),(55,145,3,'{\"responseText\":\"g7yf\",\"score\":null}'),(56,146,3,'{\"responseText\":\"uyug\",\"score\":null}'),(57,147,3,'{\"responseParaText\":\"uyg\",\"score\":null}'),(58,148,3,'{\"responseText\":\"uyf\",\"score\":null}'),(59,149,3,'{\"responseParaText\":\"uyg\",\"score\":null}'),(60,150,3,'{\"responseText\":\"yf\",\"score\":null}'),(61,151,3,'{\"responseParaText\":\"tyf\",\"score\":null}'),(62,152,3,'{\"responseText\":\"yg\",\"score\":null}'),(63,153,3,'{\"responseParaText\":\"yf\",\"score\":null}'),(64,154,3,'{\"responseText\":\"f\",\"score\":null}'),(65,155,3,'{\"responseParaText\":\"uyt\",\"score\":null}'),(66,156,3,'{\"responseText\":\"t7rf\",\"score\":null}'),(67,157,3,'{\"responseText\":\"yg\",\"score\":null}'),(68,158,3,'{\"responseParaText\":\"uyf\",\"score\":null}'),(69,159,3,'{\"responseText\":\"7yg\",\"score\":null}'),(70,160,3,'{\"responseParaText\":\"y8g\",\"score\":null}'),(71,161,3,'{\"responseText\":\"tf\",\"score\":null}'),(72,162,3,'{\"responseParaText\":\"uyg\",\"score\":null}'),(73,163,3,'{\"responseText\":\"uyf\",\"score\":null}'),(74,164,3,'{\"responseParaText\":\"tyg\",\"score\":null}'),(75,165,3,'{\"responseText\":\"g\",\"score\":null}'),(76,166,3,'{\"responseParaText\":\"t7fy8\",\"score\":null}'),(77,167,3,'{\"responseText\":\"ghy8\",\"score\":null}'),(78,168,3,'{\"responseText\":\"ftyf\",\"score\":null}'),(79,169,3,'{\"responseParaText\":\"y8\",\"score\":null}'),(80,170,3,'{\"responseText\":\"gt7r\",\"score\":null}'),(81,171,3,'{\"responseParaText\":\"t\",\"score\":null}'),(82,172,3,'{\"responseText\":\"gyg\",\"score\":null}'),(83,173,3,'{\"responseParaText\":\"t7r\",\"score\":null}'),(84,174,3,'{\"responseText\":\"t6f\",\"score\":null}'),(85,175,3,'{\"responseParaText\":\"7yg\",\"score\":null}'),(86,176,3,'{\"responseText\":\"t7f\",\"score\":null}'),(87,177,3,'{\"responseParaText\":\"t7f\",\"score\":null}'),(88,178,3,'{\"responseText\":\"yg\",\"score\":null}'),(89,179,3,'{\"responseText\":\"7rt76\",\"score\":null}'),(90,180,3,'{\"responseParaText\":\"7yg\",\"score\":null}'),(91,181,3,'{\"responseText\":\"uyg\",\"score\":null}'),(92,182,3,'{\"responseParaText\":\"tdt6\",\"score\":null}'),(93,183,3,'{\"responseText\":\"fyg\",\"score\":null}'),(94,184,3,'{\"responseParaText\":\"uyr\",\"score\":null}'),(95,185,3,'{\"responseText\":\"57r\",\"score\":null}'),(96,186,3,'{\"responseParaText\":\"6g\",\"score\":null}'),(97,187,3,'{\"responseText\":\"y8f\",\"score\":null}'),(98,188,3,'{\"responseParaText\":\"r6d\",\"score\":null}'),(99,189,3,'{\"responseText\":\"tg\",\"score\":null}'),(100,190,3,'{\"responseText\":\"ygt\",\"score\":null}'),(101,191,3,'{\"responseParaText\":\"ty7\",\"score\":null}'),(102,42,1,'{\"responseText\":\"sdvgdfgfgdfg\",\"score\":null}'),(103,35,1,'{\"responseText\":\"ffgdfgdfgdf\",\"score\":null}'),(109,37,1,'{\"responseParaText\":\"dfgdfgdfgdfggfg\",\"score\":null}'),(110,38,1,'{\"responseText\":\"fdgdfgdfg\",\"score\":null}'),(116,39,1,'{\"responseParaText\":\"fgdfgfdg\",\"score\":null}'),(122,40,1,'{\"responseText\":\"ghfghgfh\",\"score\":null}'),(124,21,2,'{\"responseText\":\"dfgdfg\",\"score\":null}'),(130,22,2,'{\"responseParaText\":\"fgfddgfdg\",\"score\":null}'),(136,23,2,'{\"responseText\":\"fgdfgfdg\",\"score\":null}'),(142,24,2,'{\"responseParaText\":\"dfdgdfgfdg\",\"score\":null}'),(148,192,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(150,193,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(156,41,1,'{\"responseParaText\":\"bwsbhdkwj\",\"score\":null}'),(157,44,1,'{\"responseParaText\":\"vgvbcvbcvbcvb\",\"score\":null}'),(161,45,1,'{\"responseText\":\"\",\"score\":null}');
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_APP_ADMIN',NULL),(2,'ROLE_APP_USER',NULL),(3,'ROLE_CONSULTANT',NULL),(4,'ROLE_CUSTOMER',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (231,'AID_MODIFY',1),(207,'AID_VIEW',1),(230,'APP_LOGIN',1),(218,'ASSET_FILLDETAILS',1),(221,'BCM_MODIFY',1),(210,'BCM_VIEW',1),(234,'COLOR_SCHEME_MODIFY',1),(226,'COLOR_SCHEME_VIEW',1),(238,'FUNCTIONAL_MAP_MODIFY',1),(239,'FUNCTIONAL_MAP_VIEW',1),(229,'INDEX_CREATION',1),(227,'MODIFY_TCO',1),(228,'PARAMETER_MODIFY',1),(222,'PARAMETER_VIEW',1),(232,'QUESTIONNAIRE_MODIFY',1),(237,'QUESTIONNAIRE_RESPOND',1),(225,'QUESTIONNAIRE_VIEW',1),(209,'QUESTION_MODIFY',1),(214,'QUESTION_VIEW',1),(215,'RELATIONSHIP_MODIFY',1),(217,'RELATIONSHIP_VIEW',1),(219,'REPORT_ASSET_GRAPH',1),(206,'REPORT_FACET_GRAPH',1),(240,'REPORT_FUNCTIONAL_REDUNDANCY',1),(236,'REPORT_PHP',1),(216,'REPORT_SCATTER_GRAPH',1),(224,'TEMPLATE_MODIFY',1),(223,'TEMPLATE_VIEW',1),(241,'USER_MODIFY',1),(211,'USER_ROLE_MODIFY',1),(233,'USER_ROLE_VIEW',1),(205,'USER_VIEW',1),(220,'VIEW_TCO',1),(208,'WORKSPACES_MODIFY',1),(235,'WORKSPACES_MODIFY_DASHBOARD',1),(213,'WORKSPACES_VIEW',1),(212,'WORKSPACES_VIEW_DASHBOARD',1);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','Admin','thirdeye-admin@birlasoft.com','$2a$10$2E3ud9YmM9U4EAcJd16lmusYswSzmx5DGhQ3tk6L9g4A/5eyvxCpO',0,1,1,0,0,-1,'2016-04-18 10:34:44',-1,'2016-04-18 10:34:44'),(2,'Samar','Gupta','samar.gupta@birlasoft.com','$2a$10$VKZyaHTwz2nYfx0PkUU.CuXqxeuy.10hkNG.aiDEoBfKZ0/0TEFDm',0,0,0,0,0,1,'2016-04-19 11:57:28',1,'2016-04-19 11:57:28');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (1,1,1),(3,2,1);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetType` varchar(20) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `widgetConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_WIDGET_DASHBOARD_idx` (`dashboardId`),
  CONSTRAINT `FK_WIDGET_DASHBOARD` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'MyCust','Workspace for MyCust',0,1,'2016-04-18 10:34:45',1,'2016-04-18 10:34:45');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace_dashboard`
--

DROP TABLE IF EXISTS `workspace_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceId` int(11) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_WORKSPACE_DASHBOARD` (`workspaceId`,`dashboardId`),
  KEY `FK_WORKSPACE_ID_idx` (`workspaceId`),
  KEY `FK_DASHBOARD_ID_idx` (`dashboardId`),
  CONSTRAINT `FK_DASHBOARD_ID` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_ID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace_dashboard`
--

LOCK TABLES `workspace_dashboard` WRITE;
/*!40000 ALTER TABLE `workspace_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `workspace_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database '3rdi'
--
/*!50003 DROP FUNCTION IF EXISTS `GetParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GetParameterTree`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        
        IF queue_length = 1 THEN
            SET front_id = FORMAT(queue,0);
            SET queue = '';
        ELSE
			SET front_id = SUBSTR(queue,1,LOCATE(',',queue)-1);
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(childparameterId) qc
        FROM parameter_function WHERE parentParameterId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getParameterTree`(IN GivenID int, OUT rv VARCHAR(1024))
BEGIN

    set rv = getParameterTree(GivenID);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-05 12:48:18
