/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-questionnaireAsset',function(context){
	'use strict';
	var $;
	var properties = {
			 "bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"iDisplayLength": 30,
				"columnDefs": [ {
			          "targets": 'no-sort',
			          "orderable": false,
			    }]
            };
	 
	return {

		init : function(){
		
			$ = context.getGlobal('jQuery');				
			
			$('#assetList').dataTable(properties);
			
			//var oTable;
			//oTable = $('#assetList').DataTable();
  		    //var column = oTable.column(2);
			//	var select = $('#assetFilter').on( 'change', function () {
           //          $.fn.DataTable.util.escapeRegex($(this).val());                     
            //         column.search(this.value).draw();
           //      } );
				
				//select.append('<option value=""></option>')
				//column.data().unique().sort().each( function (d) {
				//	var s = d.substring(6, d.length-7);
				//	select.append( '<option value="'+s+'">'+s+'</option>' )
				//} );				
				$('form[name=assetQuesList]').submit(function(){ //replace 'yourformsnameattribute' with the name of your form
					  $($('#assetList').dataTable().fnGetHiddenNodes()).find('input:checked').appendTo(this).hide(); //this is what passes any hidden nodes to your form when a user clicks SUBMIT on your FORM
					} );
				
				$('#asset-select-all').change( function() { //this is the function that will mark all your checkboxes when the input with the .checkall class is clicked
					$('input', $('#assetList').dataTable().fnGetFilteredNodes()).prop('checked',this.checked); //note it's calling fnGetFilteredNodes() - this is so it will mark all nodes whether they are filtered or not
				} );
				
			}

		}
		
});