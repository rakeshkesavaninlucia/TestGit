/**
 * @author: dhruv.sood
 */
Box.Application.addModule('module-createAssetTemplate', function(context) {
	'use strict';
	var $, commonUtils;
	var dataObj = {};
	// private methods here

	return {
		behaviors : [ 'behavior-select2' ],
		
	    init: function (){
	    	// retrieve a reference to jQuery
	        $ = context.getGlobal('jQuery');
	        commonUtils = context.getService('commonUtils');
	      },
	      
	      onchange: function(event, element, elementType) {
	    		  if(elementType === "assetTypeRelationship"){
	    			  var $selectedElementValue = element.value;
	    			  dataObj.assetTypeId = $selectedElementValue;
	    			  var postRequest = $.post( commonUtils.getAppBaseURL("relationshipTemplate/create"), dataObj);
	    			  postRequest.done(function(incomingData){
	    				  $(".relationshipTemplateContainer").empty();
	    				  $(".relationshipTemplateContainer").append(incomingData);
	    			  })
	    		  }
	      }
	};
});
