/**
 * @author: samar.gupta
 * Behavior to show Parameter Tree
 */
Box.Application.addBehavior('behaviour-showsParamTree', function(context) {

		var $;
		var dataObj = {};

		return {
			
			init: function() {

	            // retrieve a reference to jQuery
	            $ = context.getGlobal('jQuery');
	        },
		
			onclick: function(event, element, elementType) {
				if (elementType === "viewParameterTree"){
					var paramTreeViewDivClass = $(".paramTreeModalModule");
					var paramTreeViewModule = paramTreeViewDivClass.data('module');
					dataObj.parameterId = $(element).data("parameterid");		
					Box.Application.startAll();
					context.broadcast('pm::showTree', dataObj);
				}
			}
		};
});
