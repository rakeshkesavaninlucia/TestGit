/**This module will display the Asset Parameter Chart
 * and once you click on any bar of the chart it will take you to specific asset view page.
*
*@author: dhruv.sood  
*/

Box.Application.addModule('module-assetParameterMap', function(context) {
	
	 'use strict';

	// private methods here
	var $, d3, nv, commonUtils, jsonService,colourBar;
	var dataObj={};
	var spinner = null;
	
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		var $moduleDiv = $(context.getElement());
		//Calling the service
		spinner = spinService.getSpinner($moduleDiv,show,spinner);   	
	 }
		
	//function to decide the bar color
	function generateBarColour(multiColour){
		if(multiColour){
			colourBar=d3.scale.category20().range();
		}else{
			colourBar=["#99ddff"];
		}	
	}
		
	//Method to get the AssetParameterWrapper
	function getParameterName(){		
		dataObj.parameterName= $('.assetParameter').data("parametername");
		dataObj.parameterId=$('.assetParameter').data("parameterid");
	}	
	 
	
	//Method to get the Asset Parameter Wrapper and Show the Chart after generation
	function getAssetParameterWrapper(){
		var filterURL = commonUtils.readCookie("filterURL");
		var pageUrl = commonUtils.getAppBaseURL("assetParameterWrapper/" + dataObj.parameterId);
		if(filterURL !== undefined && filterURL !== null){			
			var url = filterURL.split('?').pop();
			pageUrl = pageUrl + "?" + url;
		}
		var deffMassage = $.Deferred();
		var dataPromise = jsonService.getJsonResponse(pageUrl); 
		dataPromise.done ( function (data) {
	        // need to massage it and then sending the data to the map
			deffMassage.resolve (generateMap(data));    
	    })
	    
	    .fail (function (error) {	       
	        deffMassage.reject(error);
	    });	
	  
	}
	
	
	//Method to generate the Asset Parameter chart
	function generateMap(data){
		
				$("#assetParameterMap").css("width", (data.values.length)*20+50);
		      	nv.addGraph(function() {
                var chart = nv.models.discreteBarChart()
               	   .margin({bottom: 110, left: 70})
                   .x(function(d) { return (d.assetName).substr(0, 5) + '...' + (d.assetName).substr((d.assetName).length-9, (d.assetName).length)})
                   .y(function(d) { return d.parameterValue })
                   .forceY([0,10])
                   .color(colourBar)
                   .showValues(true)
                   .duration(300);
	
	   			//Labeling y-axis
	   	        chart.yAxis
   	            .axisLabel("Parameter Name Values")
   	            .axisLabelDistance(-10)
   	            .showMaxMin(false);
	   	        
	   	          //Now populating the data on the chart
                  d3.select('#assetParameterMap svg').datum([data]).call(chart);
                   
                 //Adding pointer to all the bars
      	         d3.selectAll(".discreteBar").attr("class","clickable");
                   
                 //Making chart responsive  
                 nv.utils.windowResize(chart.update);
                   
                //Rotating the labels of x-axis by -45 degree
                var xLabels = d3.select('.nv-x.nv-axis > g').selectAll('g');
                xLabels.selectAll('text').attr('transform', function() { return 'translate (-25, 45) rotate(-60,0,0) ' });
               
                //Customizing the popover
               chart.tooltip.contentGenerator(function (object) { 
            	   return "<table><div class='smallRectangle' style='background-color:"+object.color+";'></div><tr><td style='text-align: right;'><strong>Asset:</strong></td><td>"+object.data.assetName+"</td></tr><tr><td style='text-align: right;'><strong>Parameter: </strong></td><td>"+data.parameter+"</td></tr><tr><td style='text-align: right;'><strong>Value: </strong></td><td>"+object.data.parameterValue+"</td></tr></table>";
    	        });
    	            	          	        
    	        //Capturing click on bars of chart
				chart.discretebar.dispatch.on("elementClick", function(obj) {					
				var url = 'templates/asset/view/'+obj.data.assetId;
				window.location.href = commonUtils.getAppBaseURL(url);				
				});    	        		    	        
                return chart;
                
               });                 
		 
	}

	return {
		 init: function (){
			 
			// Retrieving the references
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			jsonService = context.getService('jsonService');
			d3 = context.getGlobal('d3');
			nv= context.getGlobal('nv');
			
			//Multicoloured bar or Single coloured
			generateBarColour(false);
			
			//Get the Parameter Name
			getParameterName();
			
			//Showing loading image till chart loads
			showLoading(true);	
			
         	//Method to get the AssetParameterWrapper
			getAssetParameterWrapper();
			
			//Now stop the loading image
			showLoading(false);
						
		}
	
	};
});