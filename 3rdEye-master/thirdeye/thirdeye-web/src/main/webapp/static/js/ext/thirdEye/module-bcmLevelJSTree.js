/**
 * @author sunil.gupta
 */

Box.Application.addModule('module-bcmLevelJSTree', function(context) {
	 'use strict';
	 
	 var $, notifyService;
     var commonUtils;    
     var modalId;
     var $moduleDiv;
     function customMenu(node) {
    	    // The default custom menu for bcmLevels
    	    var items = {
    	      editValueChain: { 
    	            label: "Edit Value Chain",
    	            action: function (node) {    	            	
    	            	var dataObj = {};
    	            	dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "edit";
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        deleteValueChain: { 
    	            label: "Delete Value Chain",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.levelId = node.reference[0].id;
    	            	dataObj.operation = "delete";
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	handleDelete(dataObj);
    	            }
    	        },    	        
    	        addBusinessFunction: {
    	            label: "Add Business Function",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "add";
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        editBusinessFunction: {
    	            label: "Edit Business Function",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "edit";    	
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        deleteBusinessFunction: { 
    	            label: "Delete Business Function",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.levelId = node.reference[0].id;
    	            	dataObj.operation = "delete"; 
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	handleDelete(dataObj);
    	            }
    	        },
    	        addBusinessProcess: { 
    	            label: "Add Business Process",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "add";  
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },    	        
    	        editBusinessProcess: { 
    	            label: "Edit Business Process",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "edit";  
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        deleteBusinessProcess: { 
    	            label: "Delete Business Process",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.levelId = node.reference[0].id;
    	            	dataObj.operation = "delete"; 
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	handleDelete(dataObj);
    	            }
    	        },
    	        addActivity: { 
    	            label: "Add Activity",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "add";
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        editActivity: {
    	            label: "Edit Activity",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.parentLevel = node.reference[0].id;
    	            	dataObj.operation = "edit";  
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	populateModel(dataObj);
    	            }
    	        },
    	        deleteActivity: { 
    	            label: "Delete Activity",
    	            action: function (node) {
    	            	var dataObj = {};
						dataObj.levelId = node.reference[0].id;
    	            	dataObj.bcmid = $("#container").data("bcmid");
    	            	handleDelete(dataObj);
    	            }
    	        }
    	    };
    	    
    	   if (node.li_attr.class === "1") {    	    	
    	        
    	        delete items.editBusinessFunction;
    	        delete items.deleteBusinessFunction;
    	        
    	        delete items.addBusinessProcess;
    	        delete items.editBusinessProcess;
    	        delete items.deleteBusinessProcess;
    	        
    	        delete items.addActivity;
    	        delete items.editActivity;
    	        delete items.deleteActivity;
    	    } else if (node.li_attr.class === "2") {
    	    	
    	        delete items.editValueChain;
    	        delete items.deleteValueChain;
    	        
    	        delete items.addBusinessFunction;
    	        
    	        delete items.editBusinessProcess;
    	        delete items.deleteBusinessProcess;
    	        
    	        delete items.addActivity;
    	        delete items.editActivity;
    	        delete items.deleteActivity;
    	    
    	    }else if (node.li_attr.class === "3") {
    	    	
    	        delete items.editValueChain;
    	        delete items.deleteValueChain;
    	        
    	        delete items.addBusinessFunction;
    	        delete items.editBusinessFunction;
    	        delete items.deleteBusinessFunction;
    	        
    	        delete items.addBusinessProcess;
    	        
    	        delete items.editActivity;
    	        delete items.deleteActivity;
    	        
    	    }else if (node.li_attr.class === "4") {
    	    	
    	        delete items.editValueChain;
    	        delete items.deleteValueChain;
    	        
    	        delete items.addBusinessFunction;
    	        delete items.editBusinessFunction;
    	        delete items.deleteBusinessFunction;
    	        
    	        delete items.addBusinessProcess;
    	        delete items.editBusinessProcess;
    	        delete items.deleteBusinessProcess;
    	        
    	        delete items.addActivity;
    	        
    	    }

    	    return items;
    	}
      
        function handleFormSubmitForBcmLevelCreation(modalId, htmlResponse){

    		var modalPromise= commonUtils.populateModalWithContent(modalId,htmlResponse);
    		// Try and see whether you have received valid modal data 
    		modalPromise.done ( function (data) {
    			$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
    				handleFormSubmitForBcmLevelCreation(modalId, htmlResponse);
    			});
    		})

    		// This means that the modal is no longer needed
    		.fail (function (htmlContent) {    			
    			$("#"+modalId).modal('hide');
    			window.location.reload(true);    			
    		});
    	}
        
        function populateModalForAddValueChain(data){
        	commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL("bcmlevel/"+data.bcmid+"/level/fetchModal"))
			.then(function(){
				// Bind the ajax form
				$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
					handleFormSubmitForBcmLevelCreation(modalId, htmlResponse);
				}); 

				// Show the modal
				$("#"+modalId).modal({
					keyboard: true
				})
			});       	
        	
        }
        
        function populateModel(data){
        	commonUtils.populateModalWithURL(modalId,commonUtils.getAppBaseURL("bcmlevel/"+data.bcmid+"/level/fetchModal"),data)
			.then(function(){
				// Bind the ajax form
				$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
					handleFormSubmitForChildLevelCreation(modalId, htmlResponse, data);
				}); 

				// Show the modal
				$("#"+modalId).modal({
					keyboard: true
				})
			});
        }
        
        function handleFormSubmitForChildLevelCreation(modalId, htmlResponse, dataObj){
    		var modalPromise= commonUtils.populateModalWithContent(modalId,htmlResponse);
    		// Try and see whether you have received valid modal data 
    		modalPromise.done ( function (data) {
    			$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
    				handleFormSubmitForChildLevelCreation(modalId, htmlResponse, dataObj);
    			});
    		})

    		// This means that the modal is no longer needed
    		.fail (function (htmlContent) {
    			// Unbind the clicks
    			$("#"+modalId).modal('hide');	
    			window.location.reload(true);
    		});
    	}
        
        function handleDelete(data){

    		var deferred = $.Deferred();
    		var postRequest = $.post( commonUtils.getAppBaseURL("bcmlevel/"+data.bcmid+"/level/deleteNode"), data);
    		postRequest.done(function(incomingData){
    			deferred.resolve();
    			notifyService.setNotification("Node Deleted", "", "success");
    		}).fail(function(){
    			notifyService.setNotification("Something went wrong", "", "error");
    		});
    			window.location.reload(true);
    		
    		return deferred.promise();
    	} 
        function bindBlurToBcmName(){					

    		$(".editableBcmName").blur(function(event){
    			var $editedParam = $(this);
    			var $formForParam = $editedParam.closest("form.editableForm");


    			$formForParam.ajaxForm(function(htmlResponse) {
    			}); 

    			$formForParam.submit();

    		})
    	}// EOF
    	
        function updateNode(data) {
        	var dataObj = {};
        	dataObj.parent = data.new_instance._model.data[data.parent].a_attr.id;
        	dataObj.position = data.position;
    		var postRequest = $.post( commonUtils.getAppBaseURL("bcmlevel/" + data.node.a_attr.id + "/updateNode"), dataObj);
    		postRequest.done(function(){
    			notifyService.setNotification("Node Updated", "", "success");
    		}).fail(function(){
    			notifyService.setNotification("Something went wrong", "", "error");
    		});
        }
     
     return {
    	 
	        init: function (){
	              // retrieve a reference to jQuery
	            $ = context.getGlobal('jQuery');
	            modalId = "pageModal";
	            $moduleDiv= context.getElement()
	            commonUtils = context.getService('commonUtils');
	            notifyService = context.getService('service-notify');
	            $(function() {
	              $( "div.containerJSTreeView" ).find( "li" ).attr("data-jstree", '{"opened" : true }');
	          	  $('#container').jstree({
	          		"core" : {
   		  			 //"check_callback" : true
   		  			 "check_callback" : function(operation, node, node_parent, node_position, more) {
   		  				 // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'

   		  				 if (operation == 'move_node') {
   		  					 if (node_parent.li_attr.class === "4") {
   		  						 return false;
   		  					 } else if(node.li_attr.class === "3" && (node_parent.li_attr.class === "4" || (node_parent.li_attr.class === "3" && node.children.length > 0))) {
   		  						 return false;
   		  					 } else if(node.li_attr.class === "2" && (node_parent.li_attr.class === "4" || (node_parent.li_attr.class === "3" && node.children.length > 0) || (node_parent.li_attr.class === "2" && node.children.length > 0))) {
   		  						 return false;
   		  					 } else if(node.li_attr.class === "1" && ((node_parent.li_attr.class === "1" && node.children.length > 0) || (node_parent.li_attr.class === "2" && node.children.length > 0) || (node_parent.li_attr.class === "3" && node.children.length > 0) || node_parent.li_attr.class === "4")) {
   		  						 return false;
   		  					 } else {
   		  						 return true; 
   		  					 }
   		  				 }
   		  			 }
   		  		 },
   		  		 "types": {
	   		  		"root": {
	   		  	      "icon" : "fa fa-folder bcmFolderIcon"
	   		  	    },
	   		  	    "child": {
	   		  	      "icon" : "fa fa-folder bcmFolderIcon"
	   		  	    },
	   		  	    "default" : {
	   		  	   "icon" : "fa fa-folder bcmFolderIcon"
	   		  	    } 
   		  		 },
   		  		 "dnd" : {
   		  			 /*"is_draggable" : function(node) {
   		  				 if (node[0].li_attr.class === "1" || node[0].li_attr.class === "2") {
   		  					 return false;
   		  				 }
   		  				 return true;
   		  			 },*/
   		  			 "inside_pos" : "last"
   		  		 },
   		  		 "plugins" : ["contextmenu", "dnd","types"], contextmenu: {items: customMenu}
	          	  }).bind("move_node.jstree", function(e, data) {
		   		  		 updateNode(data);

		          	});
	          	});
	            bindBlurToBcmName();
	            
	        },	       		
			onclick: function(event, element, elementType) {
				if(elementType==="addValueChain" ){
					var dataObj = {};
					dataObj.bcmid = $(element).data("bcmid");
					populateModalForAddValueChain(dataObj)
					handleDelete(data)
				}
			},
			onmouseover:function(event, element, elementType){
				$('[data-toggle="popover"]', $moduleDiv).popover();
			}
     	}
});