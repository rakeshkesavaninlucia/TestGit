/**
 * 
 */
Box.Application.addModule('charting-dependency', function(context) {

	var $, d3, commonUtils, jsonService;
	var graph       = {},
	selected    = {},
	highlighted = null,
	isIE        = false;

	var config;

	var spinner = null;

	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		var $moduleDiv = $(context.getElement());
		//Calling the service
		spinner = spinService.getSpinner($moduleDiv,show,spinner);   	
	}

	function drawGraph() {
		$('#graph').empty();

		graph.margin = {
				top    : 20,
				right  : 20,
				bottom : 20,
				left   : 20
		};

		var display = $('#graph').css('display');
		$('#graph')
		.css('display', 'block')
		.css('height', config.graph.height + 'px');
		graph.width  = $('#graph').width()  - graph.margin.left - graph.margin.right;
		graph.height = $('#graph').height() - graph.margin.top  - graph.margin.bottom;
		$('#graph').css('display', display);

		for (var name in graph.data.data) {
			var obj = graph.data.data[name];
			obj.positionConstraints = [];
			obj.linkStrength        = 1;

			config.constraints.forEach(function(c) {
				for (var k in c.has) {
					if (c.has[k] !== obj[k]) {
						return true;
					}
				}

				switch (c.type) {
				case 'position':
					obj.positionConstraints.push({
						weight : c.weight,
						x      : c.x * graph.width,
						y      : c.y * graph.height
					});
					break;

				case 'linkStrength':
					obj.linkStrength *= c.strength;
					break;
				}
			});
		}

		graph.links = [];
		for (var name in graph.data.data) {
			var obj = graph.data.data[name];
			for (var depIndex in obj.depends) {
				var link = {
						source : search(obj.depends[depIndex].asset, graph.data.data),  //graph.data.data[obj.depends[depIndex]],
						target : obj,
						name : obj.depends[depIndex].relationshipName,
				};
				if(typeof link.source !== "undefined"){
					link.strength = (link.source.linkStrength || 1)
					* (link.target.linkStrength || 1)

					graph.links.push(link);
				}
			}
		}

		graph.categories = {};
		for (var name in graph.data.data) {
			var obj = graph.data.data[name],
			key = obj.type + ':' + (obj.group || ''),
			cat = graph.categories[key];

			obj.categoryKey = key;
			if (!cat) {
				cat = graph.categories[key] = {
						key      : key,
						type     : obj.type,
						typeName : (config.types[obj.type]
						? config.types[obj.type].short
								: obj.type),
								group    : obj.group,
								count    : 0,
								imageurl : obj.imageUrl
				};
			}
			cat.count++;
		}
		graph.categoryKeys = d3.keys(graph.categories);

		graph.colors = colorbrewer.Set3[config.graph.numColors];

		function getColorScale(darkness) {
			return d3.scale.ordinal()
			.domain(graph.categoryKeys)
			.range(graph.colors.map(function(c) {
				return d3.hsl(c).darker(darkness).toString();
			}));
		}

		graph.strokeColor = getColorScale( 0.7);
		graph.fillColor   = getColorScale(-0.1);

		graph.nodeValues = d3.values(graph.data.data);

		graph.force = d3.layout.force()
		.nodes(graph.nodeValues)
		.links(graph.links)
		.linkStrength(function(d) { return d.strength; })
		.size([graph.width, graph.height])
		.linkDistance(config.graph.linkDistance)
		.charge(config.graph.charge)
		.on('tick', tick);

		graph.svg = d3.select('#graph').append('svg')
		.attr('width' , graph.width  + graph.margin.left + graph.margin.right)
		.attr('height', graph.height + graph.margin.top  + graph.margin.bottom)
		.attr('id', 'graphDown')
		.append('g')
		.attr('transform', 'translate(' + graph.margin.left + ',' + graph.margin.top + ')');

		//Bind the change on the slider
		d3.select("#internalScale").on("change", rescale);
		d3.select("#internalTranslateRight").on("click", panRight);
		d3.select("#internalTranslateLeft").on("click", panLeft);

		function rescale(d) {
			var scale=this.value;
			graph.svg.attr("transform", "scale(" + scale + ")");
		}

		function panRight(d){
			var t = d3.transform(d3.select("#graph svg g").attr("transform")),
			x = t.translate[0] + 20,
			y = t.translate[1] + 0;
			graph.svg.attr("transform", "translate(" + x + "," + y + ")scale(" + $('#internalScale').val() + ")");
			graph.legend.attr("transform", "translate("+ -t.translate[0] +","+$("#graph-container").scrollTop()+")");
		}

		function panLeft(d){
			var t = d3.transform(d3.select("#graph svg g").attr("transform")),
			x = t.translate[0] - 20,
			y = t.translate[1] + 0;
			graph.svg.attr("transform", "translate(" + x + "," + y + ")scale(" + $('#internalScale').val() + ")");
			graph.legend.attr("transform", "translate("+ (-x+20) +","+$("#graph-container").scrollTop()+")");
		}


		//markers(arrow) to be shown on end of line. 

		graph.svg.append("defs").selectAll("marker")
		.data(["arrow"])
		.enter().append("marker")
		.attr("id", "markerEnd")
		.attr("viewBox", "0 -5 10 10")
		.attr("refX", 10)
		.attr("refY", -0,7)
		.attr("markerWidth", 6)
		.attr("markerHeight", 6)
		.attr("orient", "auto")
		.append("path")
		.attr("d", "M0,-5L10,0L0,5");

		graph.svg.append("defs").selectAll("marker")
		.data(["arrow"])
		.enter().append("marker")
		.attr("id", "markerStart")
		.attr("viewBox", "0 -5 10 10")
		.attr("refX", 0)
		.attr("refY", -0,7)
		.attr("markerWidth", 6)
		.attr("markerHeight", 6)
		.attr("orient", "auto")
		.append("path")
		.attr("d", "M0,0L10,-5L10,5Z");

		// adapted from http://stackoverflow.com/questions/9630008
		// and http://stackoverflow.com/questions/17883655

		var glow = graph.svg.append('filter')
		.attr('x'     , '-50%')
		.attr('y'     , '-50%')
		.attr('width' , '200%')
		.attr('height', '200%')
		.attr('id'    , 'blue-glow');

		glow.append('feColorMatrix')
		.attr('type'  , 'matrix')
		.attr('values', '0 0 0 0  0 '
				+ '0 0 0 0  0 '
				+ '0 0 0 0  .7 '
				+ '0 0 0 1  0 ');

		glow.append('feGaussianBlur')
		.attr('stdDeviation', 3)
		.attr('result'      , 'coloredBlur');

		glow.append('feMerge').selectAll('feMergeNode')
		.data(['coloredBlur', 'SourceGraphic'])
		.enter().append('feMergeNode')
		.attr('in', String);

		graph.legend = graph.svg.append('g')
		.attr('class', 'legend')
		.attr('x', 0)
		.attr('y', 0)
		.selectAll('.category')
		.data(d3.values(graph.categories))
		.enter().append('g')
		.attr('class', 'category');

		graph.legendConfig = {
				rectWidth   : 12,
				rectHeight  : 12,
				xOffset     : -10,
				yOffset     : 30,
				xOffsetText : 20,
				yOffsetText : 10,
				lineHeight  : 15
		};
		graph.legendConfig.xOffsetText += graph.legendConfig.xOffset;
		graph.legendConfig.yOffsetText += graph.legendConfig.yOffset;

		graph.legend.append("svg:image")
		.attr('x', graph.legendConfig.xOffset)
		.attr('y', function(d, i) {
			return graph.legendConfig.yOffset + i * graph.legendConfig.lineHeight;
		})
		.attr('height', graph.legendConfig.rectHeight)
		.attr('width' , graph.legendConfig.rectWidth)
		/*.attr('fill'  , function(d) {
	            return graph.fillColor(d.key);
	        })
	        .attr('stroke', function(d) {
	            return graph.strokeColor(d.key);
	        })*/
		.attr("xlink:href",function(d){
			return commonUtils.getAppBaseURL(d.imageurl);
		});

		graph.legend.append('text')
		.attr('x', graph.legendConfig.xOffsetText)
		.attr('y', function(d, i) {
			return graph.legendConfig.yOffsetText + i * graph.legendConfig.lineHeight;
		})
		.text(function(d) {
			return d.typeName + (d.group ? ': ' + d.group : '');
		});

		$('#graph-container').on('scroll', function() {
			var t = d3.transform(d3.select("#graph svg g").attr("transform"));
			var x = t.translate[0] - 20
			graph.legend.attr('transform', 'translate('+ -x +',' + $(this).scrollTop() + ')');
		});


		graph.line = graph.svg.append('g').selectAll('.link')
		.data(graph.force.links())
		.enter()
		.append('line')
		.attr('class', 'link')
		.each(function(data){
			var header = d3.select(this);
			var x = data.target.depends.filter(function(v) {return v.asset === data.source.name ;});
			if(x[0].direction != "UNI") {
	    		header.attr("marker-start", "url(#markerStart)")
	    		header.attr("marker-end", "url(#markerEnd)")
	    	}else{
	    		header.attr("marker-start", "url(#markerStart)")
	    	}
		});
		

		graph.draggedThreshold = d3.scale.linear()
		.domain([0, 0.1])
		.range([5, 20])
		.clamp(true);

		function dragged(d) {
			var threshold = graph.draggedThreshold(graph.force.alpha()),
			dx        = d.oldX - d.px,
			dy        = d.oldY - d.py;
			if (Math.abs(dx) >= threshold || Math.abs(dy) >= threshold) {
				d.dragged = true;
			}
			return d.dragged;
		}

		graph.drag = d3.behavior.drag()
		.origin(function(d) { return d; })
		.on('dragstart', function(d) {
			d.oldX    = d.x;
			d.oldY    = d.y;
			d.dragged = false;
			d.fixed |= 2;
		})
		.on('drag', function(d) {
			d.px = d3.event.x;
			d.py = d3.event.y;
			if (dragged(d)) {
				if (!graph.force.alpha()) {
					graph.force.alpha(.025);
				}
			}
		})
		.on('dragend', function(d) {
			if (!dragged(d)) {
				selectObject(d, this);
			}
			d.fixed &= ~6;
		});

		$('#graph-container').on('click', function(e) {
			if (!$(e.target).closest('.node').length) {
				deselectObject();
			}
		});

		graph.node = graph.svg.selectAll('.node')
		.data(graph.force.nodes())
		.enter().append('g')
		.attr('class', 'node')	        
		.call(graph.drag)
		.on('mouseover', function(d) {
			if (!selected.obj) {
				if (graph.mouseoutTimeout) {
					clearTimeout(graph.mouseoutTimeout);
					graph.mouseoutTimeout = null;
				}
				highlightObject(d);
			}
		})
		.on('mouseout', function() {
			if (!selected.obj) {
				if (graph.mouseoutTimeout) {
					clearTimeout(graph.mouseoutTimeout);
					graph.mouseoutTimeout = null;
				}
				graph.mouseoutTimeout = setTimeout(function() {
					highlightObject(null);
				}, 300);
			}
		});
		// rect not used now 

		/*graph.nodeRect = graph.node.append('rect')
	        .attr('rx', 5)
	        .attr('ry', 5)
	        .attr('stroke', function(d) {
	            return graph.strokeColor(d.categoryKey);
	        })
	        .attr('fill', function(d) {
	            return graph.fillColor(d.categoryKey);
	        })
	        .attr('width' , 120)
	        .attr('height', 30);*/

		//1.Append an Image
		graph.nodeImage = graph.node.append("svg:image")

		.attr('stroke', function(d) {
			return graph.strokeColor(d.categoryKey);
		})
		.attr('fill', function(d) {
			return graph.fillColor(d.categoryKey);
		})
		//2.Attribute -add SVG Image Link
		.attr("xlink:href",function(d){
			return commonUtils.getAppBaseURL(d.imageUrl);
		})  

		.attr("width", 30)
		.attr("height", 25)
		.attr("transform","translate(-15,0)");
		//1.Append a circle at corner of image.

		graph.nodeCircle = graph.node.append("svg:circle")
		.attr("cx",30)
		.attr("cy",20)
		.attr("r",3)	    
		.style("fill","#99ddff")
		//2.Attribute transform specifies a translation by x and y,if y not defined it is assumed to be zero.
		.attr("transform","translate(-14,0)");


		graph.node.each(function(d) {
			var node  = d3.select(this),
			//   rect  = node.select('rect'),
			image = node.select('image'),
			circle = node.select('circle'),
			lines = wrap(d.name),
			ddy   = 1.1,
			dy    = -ddy * lines.length / 2 + .5;

			lines.forEach(function(line) {
				var text = node.append('text')
				.text(line)
				.attr('x',5)
				.attr('y',35)
				.attr('dy', dy + 'em');
				dy += ddy;
			});
		});

		setTimeout(function() {
			graph.node.each(function(d) {
				var node   = d3.select(this),
				text   = node.selectAll('text'),
				bounds = {},
				first  = true;

				text.each(function() {
					var box = this.getBBox();
					if (first || box.x < bounds.x1) {
						bounds.x1 = box.x;
					}
					if (first || box.y < bounds.y1) {
						bounds.y1 = box.y;
					}
					if (first || box.x + box.width > bounds.x2) {
						bounds.x2 = box.x + box.width;
					}
					if (first || box.y + box.height > bounds.y2) {
						bounds.y2 = box.y + box.height;
					}
					first = false;
				}).attr('text-anchor', 'middle');	            

				var padding  = config.graph.labelPadding,
				margin   = config.graph.labelMargin,
				oldWidth = bounds.x2 - bounds.x1;

				bounds.x1 -= oldWidth / 2;
				bounds.x2 -= oldWidth / 2;

				bounds.x1 -= padding.left;
				bounds.y1 -= padding.top;
				bounds.x2 += padding.left + padding.right;
				bounds.y2 += padding.top  + padding.bottom;

				node.select('rect')
				.attr('x', bounds.x1)
				.attr('y', bounds.y1)
				.attr('width' , bounds.x2 - bounds.x1)
				.attr('height', bounds.y2 - bounds.y1);	              

				d.extent = {
						left   : bounds.x1 - margin.left,
						right  : bounds.x2 + margin.left + margin.right,
						top    : bounds.y1 - margin.top,
						bottom : bounds.y2 + margin.top  + margin.bottom
				};

				d.edge = {
						left   : new geo.LineSegment(bounds.x1, bounds.y1, bounds.x1, bounds.y2),
						right  : new geo.LineSegment(bounds.x2, bounds.y1, bounds.x2, bounds.y2),
						top    : new geo.LineSegment(bounds.x1, bounds.y1, bounds.x2, bounds.y1),
						bottom : new geo.LineSegment(bounds.x1, bounds.y2, bounds.x2, bounds.y2)
				};
			});

			graph.numTicks = 0;
			graph.preventCollisions = false;
			graph.force.start();
			for (var i = 0; i < config.graph.ticksWithoutCollisions; i++) {
				graph.force.tick();
			}
			graph.preventCollisions = true;
			$('#graph-container').css('visibility', 'visible');
		});
		
		//scale and translate graph to fit the svg div
		var t = d3.transform(d3.select("#graph svg g").attr("transform")),
		sx = t.translate[0] + 20 + 120,
		sy = t.translate[1] + 0;
		graph.svg.attr("transform", "translate(" + sx + "," + sy + ")scale(" + graph.width/graph.height + ")");
		$("#internalScale").val(graph.width/graph.height);
	}

	function search(key, array){
		for (var i=0; i < array.length; i++) {
			if (array[i].name === key) {
				return array[i];
			}
		}
	}

	var maxLineChars = 26,
	wrapChars    = ' /_-.'.split('');

	function wrap(text) {
		if (text.length <= maxLineChars) {
			return [text];
		} else {
			for (var k = 0; k < wrapChars.length; k++) {
				var c = wrapChars[k];
				for (var i = maxLineChars; i >= 0; i--) {
					if (text.charAt(i) === c) {
						var line = text.substring(0, i + 1);
						return [line].concat(wrap(text.substring(i + 1)));
					}
				}
			}
			return [text.substring(0, maxLineChars)]
			.concat(wrap(text.substring(maxLineChars)));
		}
	}

	function preventCollisions() {
		var quadtree = d3.geom.quadtree(graph.nodeValues);

		for (var name in graph.data.data) {
			var obj = graph.data.data[name],
			ox1 = obj.x + obj.extent.left,
			ox2 = obj.x + obj.extent.right,
			oy1 = obj.y + obj.extent.top,
			oy2 = obj.y + obj.extent.bottom;

			quadtree.visit(function(quad, x1, y1, x2, y2) {
				if (quad.point && quad.point !== obj) {
					// Check if the rectangles intersect
					var p   = quad.point,
					px1 = p.x + p.extent.left,
					px2 = p.x + p.extent.right,
					py1 = p.y + p.extent.top,
					py2 = p.y + p.extent.bottom,
					ix  = (px1 <= ox2 && ox1 <= px2 && py1 <= oy2 && oy1 <= py2);
					if (ix) {
						var xa1 = ox2 - px1, // shift obj left , p right
						xa2 = px2 - ox1, // shift obj right, p left
						ya1 = oy2 - py1, // shift obj up   , p down
						ya2 = py2 - oy1, // shift obj down , p up
						adj = Math.min(xa1, xa2, ya1, ya2);

						if (adj == xa1) {
							obj.x -= adj / 2;
							p.x   += adj / 2;
						} else if (adj == xa2) {
							obj.x += adj / 2;
							p.x   -= adj / 2;
						} else if (adj == ya1) {
							obj.y -= adj / 2;
							p.y   += adj / 2;
						} else if (adj == ya2) {
							obj.y += adj / 2;
							p.y   -= adj / 2;
						}
					}
					return ix;
				}
			});
		}
	}

	function tick(e) {
		graph.numTicks++;

		for (var name in graph.data.data) {
			var obj = graph.data.data[name];

			obj.positionConstraints.forEach(function(c) {
				var w = c.weight * e.alpha;
				if (!isNaN(c.x)) {
					obj.x = (c.x * w + obj.x * (1 - w));
				}
				if (!isNaN(c.y)) {
					obj.y = (c.y * w + obj.y * (1 - w));
				}
			});
		}

		if (graph.preventCollisions) {
			preventCollisions();
		}

		graph.line
		.attr('x1', function(d) {
			return d.source.x;
		})
		.attr('y1', function(d) {
			return d.source.y;
		})
		.each(function(d) {
			if (isIE) {
				// Work around IE bug regarding paths with markers
				// Credit: #6 and http://stackoverflow.com/a/18475039/106302
				this.parentNode.insertBefore(this, this);
			}

			var x    = d.target.x,
			y    = d.target.y,
			line = new geo.LineSegment(d.source.x, d.source.y, x, y);

			for (var e in d.target.edge) {
				var ix = line.intersect(d.target.edge[e].offset(x, y));
				if (ix.in1 && ix.in2) {
					x = ix.x;
					y = ix.y;
					break;
				}
			}

			d3.select(this)
			.attr('x2', x)
			.attr('y2', y);
		})
		.append('svg:title')
		.text(function(d){ return d.name});

		graph.node
		.attr('transform', function(d) {
			return 'translate(' + d.x + ',' + d.y + ')';
		});
	}

	function selectObject(obj, el) {
		var node;
		if (el) {
			node = d3.select(el);
		} else {
			graph.node.each(function(d) {
				if (d === obj) {
					node = d3.select(el = this);
				}
			});
		}
		if (!node) return;

		if (node.classed('selected')) {
			deselectObject();
			return;
		}
		deselectObject(false);

		selected = {
				obj : obj,
				el  : el
		};

		highlightObject(obj);

		node.classed('selected', true);

		// Tej I think we can potentially pull this from the server
		//$('#docs').html($.get(commonUtils.getAppBaseURL("add/getAddDoc/"+obj.id,null)));
		$.get( commonUtils.getAppBaseURL("add/getAddDoc/"+obj.id,null), function( data ) {
			$('#docs').html(data);
		});
		$('#docs-container').scrollTop(0);
		resize(true);

		// Figure out how much to scroll up or down
		var $graph   = $('#graph-container'),
		nodeRect = {
			left   : obj.x + obj.extent.left + graph.margin.left,
			top    : obj.y + obj.extent.top  + graph.margin.top,
			width  : obj.extent.right  - obj.extent.left,
			height : obj.extent.bottom - obj.extent.top
		},
		graphRect = {
			left   : $graph.scrollLeft(),
			top    : $graph.scrollTop(),
			width  : $graph.width(),
			height : $graph.height()
		};
		if (nodeRect.left < graphRect.left ||
				nodeRect.top  < graphRect.top  ||
				nodeRect.left + nodeRect.width  > graphRect.left + graphRect.width ||
				nodeRect.top  + nodeRect.height > graphRect.top  + graphRect.height) {

			$graph.animate({
				scrollLeft : nodeRect.left + nodeRect.width  / 2 - graphRect.width  / 2,
				scrollTop  : nodeRect.top  + nodeRect.height / 2 - graphRect.height / 2
			}, 500);
		}
	}

	function deselectObject(doResize) {
		if (doResize || typeof doResize == 'undefined') {
			resize(false);
		}
		graph.node.classed('selected', false);
		selected = {};
		highlightObject(null);
	}

	function highlightObject(obj) {
		if (obj) {
			if (obj !== highlighted) {
				graph.node.classed('inactive', function(d) {
					var dependsName = [];
					d.depends.forEach(function (x) {dependsName.push(x.asset)});
					return (obj !== d
							&& dependsName.indexOf(obj.name) == -1
							&& d.dependedOnBy.indexOf(obj.name) == -1);
				});
				graph.line.classed('inactive', function(d) {
					return (obj !== d.source && obj !== d.target);
				});
			}
			highlighted = obj;
		} else {
			if (highlighted) {
				graph.node.classed('inactive', false);
				graph.line.classed('inactive', false);
			}
			highlighted = null;
		}
	}

	var showingDocs       = false,
	docsClosePadding  = 8,
	desiredDocsHeight = 300;

	function resize(showDocs) {
		var docsHeight  = 0,
		graphHeight = 0,
		$docs       = $('#docs-container'),
		$graph      = $('#graph-container');
		//$close      = $('#docs-close');

		if (typeof showDocs == 'boolean') {
			showingDocs = showDocs;
			$docs[showDocs ? 'show' : 'hide']();
		}

		if (showingDocs) {
			docsHeight = desiredDocsHeight;
			$docs.css('height', docsHeight + 'px');
		}

		graphHeight = window.innerHeight - docsHeight;
		$graph.css('height', graphHeight + 'px');

		/* $close.css({
	        top   : graphHeight + docsClosePadding + 'px',
	        right : $docs.width() - $docs[0].clientWidth + docsClosePadding + 'px'
	    });*/
	}

	function fetchDataForGraph(){
		var _this = this;
		var deffMassage = $.Deferred();
		var url = getUrlForAdd();

		var dataPromise = jsonService.getJsonResponse(url,null);  //dataQuery(null);
		dataPromise.done ( function (data) {
			showLoading(false);
			// need to massage it
			deffMassage.resolve (dataMassage(data));    
		})

		.fail (function (error) {
			console.log(error);
			deffMassage.reject(error);
		});

		return deffMassage.promise();
	}

	function getUrlForAdd() {
		var filterURL = commonUtils.readCookie("filterURL");
		var url = commonUtils.getAppBaseURL("add/json/graphData/" + $("[data-graphid]").data("graphid"));
		if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				url = url + "?" + urlData;
			}
		}
		return url;
	}

	function dataMassage(data){
		graph.data = data;
	}

	return {

		messages: ['filterSelected'],

		init : function(){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			jsonService = context.getService('jsonService');
			d3 = context.getGlobal('d3');
			config = context.getConfig();
			resize();

			isIE = $.browser.msie;
			showLoading(true);
			$(fetchDataForGraph().then(function(){
				drawGraph();
			}));
			/*d3.json(config.jsonUrl, function(data) {
	             if (data.errors.length) {
	                 alert('Data error(s):\n\n' + data.errors.join('\n'));
	                 return;
	             }

	             graph.data = data.data;
	             drawGraph();
	         });*/

			// TEJ This needs to access a service based on the URL configuration setup.
			// For the datastructure of the JSON coming back check the files.
			/*graph.data = {"db_view_1":{"type":"view","name":"db_view_1","depends":["db_table_15","db_table_14","db_table_1"],"dependedOnBy":[],"docs":"<h2>db&#95;view&#95;1 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-15\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_15\">db&#95;table&#95;15<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<li><a href=\"#obj-db-table-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_1\">db&#95;table&#95;1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_view_2":{"type":"view","name":"db_view_2","depends":["db_table_4","db_table_14"],"dependedOnBy":["query9.sql","query11.sql"],"docs":"<h2>db&#95;view&#95;2 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query11-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query11.sql\">query11.sql<\/a><\/li>\n<\/ul>\n"},"db_view_3":{"type":"view","name":"db_view_3","depends":["db_table_5","db_table_14"],"dependedOnBy":["query12.sql","query28.sql"],"docs":"<h2>db&#95;view&#95;3 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_5\">db&#95;table&#95;5<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query12-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query12.sql\">query12.sql<\/a><\/li>\n<li><a href=\"#obj-query28-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query28.sql\">query28.sql<\/a><\/li>\n<\/ul>\n"},"db_view_4":{"type":"view","name":"db_view_4","depends":["db_table_10","db_table_14"],"dependedOnBy":["query7.sql","query9.sql","query13.sql","query22.sql"],"docs":"<h2>db&#95;view&#95;4 <em>Database view<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This view contains miscellaneous other data for the current period.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query7-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query7.sql\">query7.sql<\/a><\/li>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query13-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query13.sql\">query13.sql<\/a><\/li>\n<li><a href=\"#obj-query22-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query22.sql\">query22.sql<\/a><\/li>\n<\/ul>\n"},"db_view_5":{"type":"view","name":"db_view_5","depends":["db_table_10","db_table_14"],"dependedOnBy":["query22.sql"],"docs":"<h2>db&#95;view&#95;5 <em>Database view<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This view contains miscellaneous other data for the previous period.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query22-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query22.sql\">query22.sql<\/a><\/li>\n<\/ul>\n"},"db_view_6":{"type":"view","name":"db_view_6","depends":["db_table_10"],"dependedOnBy":["db_view_7"],"docs":"<h2>db&#95;view&#95;6 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_7\">db&#95;view&#95;7<\/a><\/li>\n<\/ul>\n"},"db_view_7":{"type":"view","name":"db_view_7","depends":["db_view_6","db_table_14"],"dependedOnBy":["query9.sql","query17.sql"],"docs":"<h2>db&#95;view&#95;7 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_6\">db&#95;view&#95;6<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query17-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query17.sql\">query17.sql<\/a><\/li>\n<\/ul>\n"},"db_view_8":{"type":"view","name":"db_view_8","depends":["db_table_15","db_table_14"],"dependedOnBy":["query27.sql","query31.sql"],"docs":"<h2>db&#95;view&#95;8 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-15\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_15\">db&#95;table&#95;15<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query27-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query27.sql\">query27.sql<\/a><\/li>\n<li><a href=\"#obj-query31-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query31.sql\">query31.sql<\/a><\/li>\n<\/ul>\n"},"db_view_9":{"type":"view","name":"db_view_9","depends":["db_table_18","db_table_14"],"dependedOnBy":["query5.sql","query6.sql","query7.sql","query8.sql","query9.sql","query14.sql","query15.sql","query18.sql","query20.sql","query21.sql","query25.sql","query26.sql","query31.sql"],"docs":"<h2>db&#95;view&#95;9 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query5-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query5.sql\">query5.sql<\/a><\/li>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<li><a href=\"#obj-query7-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query7.sql\">query7.sql<\/a><\/li>\n<li><a href=\"#obj-query8-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query8.sql\">query8.sql<\/a><\/li>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query14-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query14.sql\">query14.sql<\/a><\/li>\n<li><a href=\"#obj-query15-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query15.sql\">query15.sql<\/a><\/li>\n<li><a href=\"#obj-query18-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query18.sql\">query18.sql<\/a><\/li>\n<li><a href=\"#obj-query20-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query20.sql\">query20.sql<\/a><\/li>\n<li><a href=\"#obj-query21-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query21.sql\">query21.sql<\/a><\/li>\n<li><a href=\"#obj-query25-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query25.sql\">query25.sql<\/a><\/li>\n<li><a href=\"#obj-query26-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query26.sql\">query26.sql<\/a><\/li>\n<li><a href=\"#obj-query31-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query31.sql\">query31.sql<\/a><\/li>\n<\/ul>\n"},"db_view_10":{"type":"view","name":"db_view_10","depends":["db_table_18","db_table_14"],"dependedOnBy":["query20.sql","query21.sql"],"docs":"<h2>db&#95;view&#95;10 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query20-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query20.sql\">query20.sql<\/a><\/li>\n<li><a href=\"#obj-query21-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query21.sql\">query21.sql<\/a><\/li>\n<\/ul>\n"},"db_view_11":{"type":"view","name":"db_view_11","depends":["db_table_24","db_table_14"],"dependedOnBy":["query16.sql","query29.sql"],"docs":"<h2>db&#95;view&#95;11 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-24\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_24\">db&#95;table&#95;24<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query16-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query16.sql\">query16.sql<\/a><\/li>\n<li><a href=\"#obj-query29-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query29.sql\">query29.sql<\/a><\/li>\n<\/ul>\n"},"db_view_12":{"type":"view","name":"db_view_12","depends":["db_table_28","db_table_14"],"dependedOnBy":["query6.sql","query30.sql"],"docs":"<h2>db&#95;view&#95;12 <em>Database view<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-28\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_28\">db&#95;table&#95;28<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<li><a href=\"#obj-query30-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query30.sql\">query30.sql<\/a><\/li>\n<\/ul>\n"},"db_table_1":{"type":"table","group":"Mapping","name":"db_table_1","depends":[],"dependedOnBy":["db_view_1"],"docs":"<h2>db&#95;table&#95;1 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_1\">db&#95;view&#95;1<\/a><\/li>\n<\/ul>\n"},"db_table_2":{"type":"table","name":"db_table_2","depends":[],"dependedOnBy":["query10.sql"],"docs":"<h2>db&#95;table&#95;2 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query10-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query10.sql\">query10.sql<\/a><\/li>\n<\/ul>\n"},"db_table_3":{"type":"table","name":"db_table_3","depends":["SASProject.egp"],"dependedOnBy":["db_table_4"],"docs":"<h2>db&#95;table&#95;3 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-SASProject-egp\" class=\"select-object\" data-type=\"select-object\" data-name=\"SASProject.egp\">SASProject.egp<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<\/ul>\n"},"db_table_4":{"type":"table","name":"db_table_4","depends":["SASProject.egp","db_table_3"],"dependedOnBy":["db_view_2","query2.sql","query3.sql","query4.sql"],"docs":"<h2>db&#95;table&#95;4 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-SASProject-egp\" class=\"select-object\" data-type=\"select-object\" data-name=\"SASProject.egp\">SASProject.egp<\/a><\/li>\n<li><a href=\"#obj-db-table-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_3\">db&#95;table&#95;3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_2\">db&#95;view&#95;2<\/a><\/li>\n<li><a href=\"#obj-query2-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query2.sql\">query2.sql<\/a><\/li>\n<li><a href=\"#obj-query3-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query3.sql\">query3.sql<\/a><\/li>\n<li><a href=\"#obj-query4-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query4.sql\">query4.sql<\/a><\/li>\n<\/ul>\n"},"db_table_5":{"type":"table","name":"db_table_5","depends":["db_table_7","ETL process 1"],"dependedOnBy":["db_view_3"],"docs":"<h2>db&#95;table&#95;5 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_7\">db&#95;table&#95;7<\/a><\/li>\n<li><a href=\"#obj-ETL-process-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 1\">ETL process 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_3\">db&#95;view&#95;3<\/a><\/li>\n<\/ul>\n"},"db_table_6":{"type":"table","name":"db_table_6","depends":["ETL process 1"],"dependedOnBy":[],"docs":"<h2>db&#95;table&#95;6 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 1\">ETL process 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_table_7":{"type":"table","name":"db_table_7","depends":["ETL process 1"],"dependedOnBy":["db_table_5"],"docs":"<h2>db&#95;table&#95;7 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 1\">ETL process 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_5\">db&#95;table&#95;5<\/a><\/li>\n<\/ul>\n"},"db_table_8":{"type":"table","name":"db_table_8","depends":["query5.sql"],"dependedOnBy":["query19.sql"],"docs":"<h2>db&#95;table&#95;8 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query5-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query5.sql\">query5.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query19-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query19.sql\">query19.sql<\/a><\/li>\n<\/ul>\n"},"db_table_9":{"type":"table","name":"db_table_9","depends":[],"dependedOnBy":["query1.sql"],"docs":"<h2>db&#95;table&#95;9 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query1-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query1.sql\">query1.sql<\/a><\/li>\n<\/ul>\n"},"db_table_10":{"type":"table","name":"db_table_10","depends":["db_table_12","ETL process 2"],"dependedOnBy":["db_view_4","db_view_5","db_view_6"],"docs":"<h2>db&#95;table&#95;10 <em>Database table<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This table contains miscellaneous other data for current and historical\nperiods.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_12\">db&#95;table&#95;12<\/a><\/li>\n<li><a href=\"#obj-ETL-process-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 2\">ETL process 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-view-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_5\">db&#95;view&#95;5<\/a><\/li>\n<li><a href=\"#obj-db-view-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_6\">db&#95;view&#95;6<\/a><\/li>\n<\/ul>\n"},"db_table_11":{"type":"table","name":"db_table_11","depends":["ETL process 2"],"dependedOnBy":[],"docs":"<h2>db&#95;table&#95;11 <em>Database table<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This table contains miscellaneous other data for all periods.  Where\n<a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a> contains the latest data for each period, this\ntable stores all updates to the miscellaneous items data.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 2\">ETL process 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_table_12":{"type":"table","name":"db_table_12","depends":["ETL process 2"],"dependedOnBy":["db_table_10"],"docs":"<h2>db&#95;table&#95;12 <em>Database table<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This table temporarily stores the miscellaneous other data for the current\nperiod while the data is being extracted and loaded to the database.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 2\">ETL process 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a><\/li>\n<\/ul>\n"},"db_table_13":{"type":"table","name":"db_table_13","depends":[],"dependedOnBy":["query18.sql","query19.sql","query31.sql"],"docs":"<h2>db&#95;table&#95;13 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query18-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query18.sql\">query18.sql<\/a><\/li>\n<li><a href=\"#obj-query19-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query19.sql\">query19.sql<\/a><\/li>\n<li><a href=\"#obj-query31-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query31.sql\">query31.sql<\/a><\/li>\n<\/ul>\n"},"db_table_14":{"type":"table","name":"db_table_14","depends":[],"dependedOnBy":["db_view_1","db_view_2","db_view_3","db_view_4","db_view_5","db_view_7","db_view_8","db_view_9","db_view_10","db_view_11","db_view_12","query5.sql","query6.sql","query10.sql","query18.sql","query19.sql","query20.sql","query21.sql","query22.sql","query23.sql","query24.sql","query31.sql"],"docs":"<h2>db&#95;table&#95;14 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_1\">db&#95;view&#95;1<\/a><\/li>\n<li><a href=\"#obj-db-view-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_2\">db&#95;view&#95;2<\/a><\/li>\n<li><a href=\"#obj-db-view-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_3\">db&#95;view&#95;3<\/a><\/li>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-view-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_5\">db&#95;view&#95;5<\/a><\/li>\n<li><a href=\"#obj-db-view-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_7\">db&#95;view&#95;7<\/a><\/li>\n<li><a href=\"#obj-db-view-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_8\">db&#95;view&#95;8<\/a><\/li>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_10\">db&#95;view&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-view-11\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_11\">db&#95;view&#95;11<\/a><\/li>\n<li><a href=\"#obj-db-view-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_12\">db&#95;view&#95;12<\/a><\/li>\n<li><a href=\"#obj-query5-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query5.sql\">query5.sql<\/a><\/li>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<li><a href=\"#obj-query10-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query10.sql\">query10.sql<\/a><\/li>\n<li><a href=\"#obj-query18-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query18.sql\">query18.sql<\/a><\/li>\n<li><a href=\"#obj-query19-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query19.sql\">query19.sql<\/a><\/li>\n<li><a href=\"#obj-query20-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query20.sql\">query20.sql<\/a><\/li>\n<li><a href=\"#obj-query21-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query21.sql\">query21.sql<\/a><\/li>\n<li><a href=\"#obj-query22-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query22.sql\">query22.sql<\/a><\/li>\n<li><a href=\"#obj-query23-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query23.sql\">query23.sql<\/a><\/li>\n<li><a href=\"#obj-query24-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query24.sql\">query24.sql<\/a><\/li>\n<li><a href=\"#obj-query31-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query31.sql\">query31.sql<\/a><\/li>\n<\/ul>\n"},"db_table_15":{"type":"table","name":"db_table_15","depends":["db_table_17","ETL process 3"],"dependedOnBy":["db_view_1","db_view_8","query23.sql"],"docs":"<h2>db&#95;table&#95;15 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-17\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_17\">db&#95;table&#95;17<\/a><\/li>\n<li><a href=\"#obj-ETL-process-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 3\">ETL process 3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_1\">db&#95;view&#95;1<\/a><\/li>\n<li><a href=\"#obj-db-view-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_8\">db&#95;view&#95;8<\/a><\/li>\n<li><a href=\"#obj-query23-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query23.sql\">query23.sql<\/a><\/li>\n<\/ul>\n"},"db_table_16":{"type":"table","name":"db_table_16","depends":["ETL process 3"],"dependedOnBy":[],"docs":"<h2>db&#95;table&#95;16 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 3\">ETL process 3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_table_17":{"type":"table","name":"db_table_17","depends":["ETL process 3"],"dependedOnBy":["db_table_15"],"docs":"<h2>db&#95;table&#95;17 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 3\">ETL process 3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-15\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_15\">db&#95;table&#95;15<\/a><\/li>\n<\/ul>\n"},"db_table_18":{"type":"table","name":"db_table_18","depends":["db_table_20","ETL process 4"],"dependedOnBy":["db_view_9","db_view_10","query9.sql","query18.sql","query19.sql"],"docs":"<h2>db&#95;table&#95;18 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-20\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_20\">db&#95;table&#95;20<\/a><\/li>\n<li><a href=\"#obj-ETL-process-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 4\">ETL process 4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_10\">db&#95;view&#95;10<\/a><\/li>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query18-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query18.sql\">query18.sql<\/a><\/li>\n<li><a href=\"#obj-query19-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query19.sql\">query19.sql<\/a><\/li>\n<\/ul>\n"},"db_table_19":{"type":"table","name":"db_table_19","depends":["ETL process 4"],"dependedOnBy":[],"docs":"<h2>db&#95;table&#95;19 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 4\">ETL process 4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_table_20":{"type":"table","name":"db_table_20","depends":["ETL process 4"],"dependedOnBy":["db_table_18"],"docs":"<h2>db&#95;table&#95;20 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 4\">ETL process 4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<\/ul>\n"},"db_table_21":{"type":"table","group":"Mapping","name":"db_table_21","depends":[],"dependedOnBy":["query31.sql"],"docs":"<h2>db&#95;table&#95;21 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query31-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query31.sql\">query31.sql<\/a><\/li>\n<\/ul>\n"},"db_table_22":{"type":"table","name":"db_table_22","depends":[],"dependedOnBy":["query1.sql"],"docs":"<h2>db&#95;table&#95;22 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query1-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query1.sql\">query1.sql<\/a><\/li>\n<\/ul>\n"},"db_table_23":{"type":"table","name":"db_table_23","depends":[],"dependedOnBy":["query1.sql"],"docs":"<h2>db&#95;table&#95;23 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query1-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query1.sql\">query1.sql<\/a><\/li>\n<\/ul>\n"},"db_table_24":{"type":"table","name":"db_table_24","depends":["query6.sql"],"dependedOnBy":["db_view_11"],"docs":"<h2>db&#95;table&#95;24 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-11\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_11\">db&#95;view&#95;11<\/a><\/li>\n<\/ul>\n"},"db_table_25":{"type":"table","group":"Mapping","name":"db_table_25","depends":[],"dependedOnBy":["query6.sql"],"docs":"<h2>db&#95;table&#95;25 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<\/ul>\n"},"db_table_26":{"type":"table","group":"Mapping","name":"db_table_26","depends":[],"dependedOnBy":["query6.sql"],"docs":"<h2>db&#95;table&#95;26 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<\/ul>\n"},"db_table_27":{"type":"table","group":"Mapping","name":"db_table_27","depends":[],"dependedOnBy":["query6.sql","query16.sql"],"docs":"<h2>db&#95;table&#95;27 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-query6-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query6.sql\">query6.sql<\/a><\/li>\n<li><a href=\"#obj-query16-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query16.sql\">query16.sql<\/a><\/li>\n<\/ul>\n"},"db_table_28":{"type":"table","name":"db_table_28","depends":["db_table_30","ETL process 5"],"dependedOnBy":["db_view_12","query24.sql"],"docs":"<h2>db&#95;table&#95;28 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-30\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_30\">db&#95;table&#95;30<\/a><\/li>\n<li><a href=\"#obj-ETL-process-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 5\">ETL process 5<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_12\">db&#95;view&#95;12<\/a><\/li>\n<li><a href=\"#obj-query24-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query24.sql\">query24.sql<\/a><\/li>\n<\/ul>\n"},"db_table_29":{"type":"table","name":"db_table_29","depends":["ETL process 5"],"dependedOnBy":[],"docs":"<h2>db&#95;table&#95;29 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 5\">ETL process 5<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"db_table_30":{"type":"table","name":"db_table_30","depends":["ETL process 5"],"dependedOnBy":["db_table_28"],"docs":"<h2>db&#95;table&#95;30 <em>Database table<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-ETL-process-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"ETL process 5\">ETL process 5<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-28\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_28\">db&#95;table&#95;28<\/a><\/li>\n<\/ul>\n"},"query1.sql":{"type":"query","group":"Management","name":"query1.sql","depends":["db_table_9","db_table_23","db_table_22"],"dependedOnBy":[],"docs":"<h2>query1.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_9\">db&#95;table&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-table-23\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_23\">db&#95;table&#95;23<\/a><\/li>\n<li><a href=\"#obj-db-table-22\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_22\">db&#95;table&#95;22<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query2.sql":{"type":"query","group":"Data1","name":"query2.sql","depends":["db_table_4"],"dependedOnBy":["Data store 1"],"docs":"<h2>query2.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n"},"query3.sql":{"type":"query","group":"Data1","name":"query3.sql","depends":["db_table_4"],"dependedOnBy":["Data store 1"],"docs":"<h2>query3.sql <em>Database query<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This query returns some data needed for <a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a>.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n"},"query4.sql":{"type":"query","group":"Data1","name":"query4.sql","depends":["db_table_4"],"dependedOnBy":["Data store 1"],"docs":"<h2>query4.sql <em>Database query<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This query returns some data needed for <a href=\"#obj-Report-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 6\">Report 6<\/a>.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n"},"query5.sql":{"type":"query","group":"Miscellaneous","name":"query5.sql","depends":["db_view_9","db_table_14"],"dependedOnBy":["db_table_8"],"docs":"<h2>query5.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_8\">db&#95;table&#95;8<\/a><\/li>\n<\/ul>\n"},"query6.sql":{"type":"query","group":"Miscellaneous","name":"query6.sql","depends":["db_view_12","db_table_25","db_table_26","db_table_27","db_view_9","db_table_14"],"dependedOnBy":["db_table_24"],"docs":"<h2>query6.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_12\">db&#95;view&#95;12<\/a><\/li>\n<li><a href=\"#obj-db-table-25\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_25\">db&#95;table&#95;25<\/a><\/li>\n<li><a href=\"#obj-db-table-26\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_26\">db&#95;table&#95;26<\/a><\/li>\n<li><a href=\"#obj-db-table-27\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_27\">db&#95;table&#95;27<\/a><\/li>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-24\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_24\">db&#95;table&#95;24<\/a><\/li>\n<\/ul>\n"},"query7.sql":{"type":"query","group":"Data2","name":"query7.sql","depends":["db_view_4","db_view_9"],"dependedOnBy":["Data store 2"],"docs":"<h2>query7.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query8.sql":{"type":"query","group":"Data2","name":"query8.sql","depends":["db_view_9"],"dependedOnBy":["Data store 2"],"docs":"<h2>query8.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query9.sql":{"type":"query","group":"Data2","name":"query9.sql","depends":["db_view_9","db_view_4","db_view_2","db_view_7","db_table_18"],"dependedOnBy":["Data store 2"],"docs":"<h2>query9.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-view-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_2\">db&#95;view&#95;2<\/a><\/li>\n<li><a href=\"#obj-db-view-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_7\">db&#95;view&#95;7<\/a><\/li>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query10.sql":{"type":"query","group":"Data2","name":"query10.sql","depends":["db_table_2","db_table_14"],"dependedOnBy":["Data store 2"],"docs":"<h2>query10.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_2\">db&#95;table&#95;2<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query11.sql":{"type":"query","group":"Data2","name":"query11.sql","depends":["db_view_2"],"dependedOnBy":["Data store 2"],"docs":"<h2>query11.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_2\">db&#95;view&#95;2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query12.sql":{"type":"query","group":"Data2","name":"query12.sql","depends":["db_view_3"],"dependedOnBy":["Data store 2"],"docs":"<h2>query12.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_3\">db&#95;view&#95;3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query13.sql":{"type":"query","group":"Data2","name":"query13.sql","depends":["db_view_4"],"dependedOnBy":["Data store 2"],"docs":"<h2>query13.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query14.sql":{"type":"query","group":"Data2","name":"query14.sql","depends":["db_view_9"],"dependedOnBy":["Data store 2"],"docs":"<h2>query14.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query15.sql":{"type":"query","group":"Data2","name":"query15.sql","depends":["db_view_9"],"dependedOnBy":["Data store 2"],"docs":"<h2>query15.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query16.sql":{"type":"query","group":"Data2","name":"query16.sql","depends":["db_view_11","db_table_27"],"dependedOnBy":["Data store 2"],"docs":"<h2>query16.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-11\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_11\">db&#95;view&#95;11<\/a><\/li>\n<li><a href=\"#obj-db-table-27\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_27\">db&#95;table&#95;27<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query17.sql":{"type":"query","group":"Data2","name":"query17.sql","depends":["db_view_7"],"dependedOnBy":["Data store 2"],"docs":"<h2>query17.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_7\">db&#95;view&#95;7<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query18.sql":{"type":"query","group":"Data2","name":"query18.sql","depends":["db_table_18","db_table_13","db_table_14","db_view_9"],"dependedOnBy":["Data store 2"],"docs":"<h2>query18.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<li><a href=\"#obj-db-table-13\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_13\">db&#95;table&#95;13<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query19.sql":{"type":"query","group":"Data2","name":"query19.sql","depends":["db_table_18","db_table_13","db_table_14","db_table_8"],"dependedOnBy":["Data store 2"],"docs":"<h2>query19.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<li><a href=\"#obj-db-table-13\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_13\">db&#95;table&#95;13<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<li><a href=\"#obj-db-table-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_8\">db&#95;table&#95;8<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query20.sql":{"type":"query","group":"Data2","name":"query20.sql","depends":["db_view_9","db_view_10","db_table_14"],"dependedOnBy":["Data store 2"],"docs":"<h2>query20.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_10\">db&#95;view&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query21.sql":{"type":"query","group":"Data2","name":"query21.sql","depends":["db_view_9","db_view_10","db_table_14"],"dependedOnBy":["Data store 2"],"docs":"<h2>query21.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_10\">db&#95;view&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query22.sql":{"type":"query","group":"Data2","name":"query22.sql","depends":["db_view_4","db_view_5","db_table_14"],"dependedOnBy":["Data store 2"],"docs":"<h2>query22.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_4\">db&#95;view&#95;4<\/a><\/li>\n<li><a href=\"#obj-db-view-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_5\">db&#95;view&#95;5<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n"},"query23.sql":{"type":"query","group":"Data3","name":"query23.sql","depends":["db_table_15","db_table_14"],"dependedOnBy":["Data store 3"],"docs":"<h2>query23.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-15\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_15\">db&#95;table&#95;15<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 3\">Data store 3<\/a><\/li>\n<\/ul>\n"},"query24.sql":{"type":"query","group":"Data4","name":"query24.sql","depends":["db_table_28","db_table_14"],"dependedOnBy":["Data store 4"],"docs":"<h2>query24.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-28\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_28\">db&#95;table&#95;28<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 4\">Data store 4<\/a><\/li>\n<\/ul>\n"},"query25.sql":{"type":"query","group":"Validation","name":"query25.sql","depends":["db_view_9"],"dependedOnBy":[],"docs":"<h2>query25.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query26.sql":{"type":"query","group":"Validation","name":"query26.sql","depends":["db_view_9"],"dependedOnBy":[],"docs":"<h2>query26.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query27.sql":{"type":"query","group":"Validation","name":"query27.sql","depends":["db_view_8"],"dependedOnBy":[],"docs":"<h2>query27.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_8\">db&#95;view&#95;8<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query28.sql":{"type":"query","group":"Validation","name":"query28.sql","depends":["db_view_3"],"dependedOnBy":[],"docs":"<h2>query28.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_3\">db&#95;view&#95;3<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query29.sql":{"type":"query","group":"Validation","name":"query29.sql","depends":["db_view_11"],"dependedOnBy":[],"docs":"<h2>query29.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-11\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_11\">db&#95;view&#95;11<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query30.sql":{"type":"query","group":"Validation","name":"query30.sql","depends":["db_view_12"],"dependedOnBy":[],"docs":"<h2>query30.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_12\">db&#95;view&#95;12<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"query31.sql":{"type":"query","group":"Validation","name":"query31.sql","depends":["db_view_9","db_view_8","db_table_21","db_table_14","db_table_13"],"dependedOnBy":[],"docs":"<h2>query31.sql <em>Database query<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-view-9\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_9\">db&#95;view&#95;9<\/a><\/li>\n<li><a href=\"#obj-db-view-8\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_view_8\">db&#95;view&#95;8<\/a><\/li>\n<li><a href=\"#obj-db-table-21\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_21\">db&#95;table&#95;21<\/a><\/li>\n<li><a href=\"#obj-db-table-14\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_14\">db&#95;table&#95;14<\/a><\/li>\n<li><a href=\"#obj-db-table-13\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_13\">db&#95;table&#95;13<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"SASProject.egp":{"type":"sas","name":"SASProject.egp","depends":[],"dependedOnBy":["db_table_3","db_table_4"],"docs":"<h2>SASProject.egp <em>SAS project<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_3\">db&#95;table&#95;3<\/a><\/li>\n<li><a href=\"#obj-db-table-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_4\">db&#95;table&#95;4<\/a><\/li>\n<\/ul>\n"},"ETL process 1":{"type":"extract","name":"ETL process 1","depends":[],"dependedOnBy":["db_table_5","db_table_6","db_table_7"],"docs":"<h2>ETL process 1 <em>Extract-transform-load process<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_5\">db&#95;table&#95;5<\/a><\/li>\n<li><a href=\"#obj-db-table-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_6\">db&#95;table&#95;6<\/a><\/li>\n<li><a href=\"#obj-db-table-7\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_7\">db&#95;table&#95;7<\/a><\/li>\n<\/ul>\n"},"ETL process 2":{"type":"extract","name":"ETL process 2","depends":[],"dependedOnBy":["db_table_10","db_table_11","db_table_12"],"docs":"<h2>ETL process 2 <em>Extract-transform-load process<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-10\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_10\">db&#95;table&#95;10<\/a><\/li>\n<li><a href=\"#obj-db-table-11\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_11\">db&#95;table&#95;11<\/a><\/li>\n<li><a href=\"#obj-db-table-12\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_12\">db&#95;table&#95;12<\/a><\/li>\n<\/ul>\n"},"ETL process 3":{"type":"extract","name":"ETL process 3","depends":[],"dependedOnBy":["db_table_15","db_table_16","db_table_17"],"docs":"<h2>ETL process 3 <em>Extract-transform-load process<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-15\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_15\">db&#95;table&#95;15<\/a><\/li>\n<li><a href=\"#obj-db-table-16\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_16\">db&#95;table&#95;16<\/a><\/li>\n<li><a href=\"#obj-db-table-17\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_17\">db&#95;table&#95;17<\/a><\/li>\n<\/ul>\n"},"ETL process 4":{"type":"extract","name":"ETL process 4","depends":[],"dependedOnBy":["db_table_18","db_table_19","db_table_20"],"docs":"<h2>ETL process 4 <em>Extract-transform-load process<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-18\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_18\">db&#95;table&#95;18<\/a><\/li>\n<li><a href=\"#obj-db-table-19\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_19\">db&#95;table&#95;19<\/a><\/li>\n<li><a href=\"#obj-db-table-20\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_20\">db&#95;table&#95;20<\/a><\/li>\n<\/ul>\n"},"ETL process 5":{"type":"extract","name":"ETL process 5","depends":[],"dependedOnBy":["db_table_28","db_table_29","db_table_30"],"docs":"<h2>ETL process 5 <em>Extract-transform-load process<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on <em>(none)<\/em><\/h3>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-db-table-28\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_28\">db&#95;table&#95;28<\/a><\/li>\n<li><a href=\"#obj-db-table-29\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_29\">db&#95;table&#95;29<\/a><\/li>\n<li><a href=\"#obj-db-table-30\" class=\"select-object\" data-type=\"select-object\" data-name=\"db_table_30\">db&#95;table&#95;30<\/a><\/li>\n<\/ul>\n"},"Data store 1":{"type":"database","name":"Data store 1","depends":["query2.sql","query3.sql","query4.sql"],"dependedOnBy":["Intermediate step 1","Intermediate step 2","Intermediate step 3"],"docs":"<h2>Data store 1 <em>Access database<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>The database that provides some data for\n<a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a>, <a href=\"#obj-Report-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 6\">Report 6<\/a>, and\n<a href=\"#obj-Intermediate-step-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 3\">Intermediate step 3<\/a>.  It is located at:<\/p>\n\n<pre>\/path\/Data store 1\n<\/pre>\n\n<p>This database contains some simple mapping logic that takes the results of the\n<a href=\"#obj-query2-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query2.sql\">query2.sql<\/a> query and splits\nit into one query for each of the <a href=\"#obj-Intermediate-step-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 3\">Intermediate step 3<\/a> files.<\/p>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query2-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query2.sql\">query2.sql<\/a><\/li>\n<li><a href=\"#obj-query3-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query3.sql\">query3.sql<\/a><\/li>\n<li><a href=\"#obj-query4-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query4.sql\">query4.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Intermediate-step-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 1\">Intermediate step 1<\/a><\/li>\n<li><a href=\"#obj-Intermediate-step-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 2\">Intermediate step 2<\/a><\/li>\n<li><a href=\"#obj-Intermediate-step-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 3\">Intermediate step 3<\/a><\/li>\n<\/ul>\n"},"Data store 2":{"type":"database","name":"Data store 2","depends":["query7.sql","query8.sql","query9.sql","query10.sql","query11.sql","query12.sql","query13.sql","query14.sql","query15.sql","query16.sql","query17.sql","query18.sql","query19.sql","query20.sql","query21.sql","query22.sql"],"dependedOnBy":["Report 1","Report 2","Report 3","Report 4","Upstream report 1","Upstream report 2","Upstream report 3"],"docs":"<h2>Data store 2 <em>Access database<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>The database that provides the data for the reports and the upstream reports.\nIt is located at:<\/p>\n\n<pre>\/path\/Data store 2\n<\/pre>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query7-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query7.sql\">query7.sql<\/a><\/li>\n<li><a href=\"#obj-query8-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query8.sql\">query8.sql<\/a><\/li>\n<li><a href=\"#obj-query9-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query9.sql\">query9.sql<\/a><\/li>\n<li><a href=\"#obj-query10-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query10.sql\">query10.sql<\/a><\/li>\n<li><a href=\"#obj-query11-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query11.sql\">query11.sql<\/a><\/li>\n<li><a href=\"#obj-query12-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query12.sql\">query12.sql<\/a><\/li>\n<li><a href=\"#obj-query13-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query13.sql\">query13.sql<\/a><\/li>\n<li><a href=\"#obj-query14-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query14.sql\">query14.sql<\/a><\/li>\n<li><a href=\"#obj-query15-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query15.sql\">query15.sql<\/a><\/li>\n<li><a href=\"#obj-query16-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query16.sql\">query16.sql<\/a><\/li>\n<li><a href=\"#obj-query17-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query17.sql\">query17.sql<\/a><\/li>\n<li><a href=\"#obj-query18-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query18.sql\">query18.sql<\/a><\/li>\n<li><a href=\"#obj-query19-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query19.sql\">query19.sql<\/a><\/li>\n<li><a href=\"#obj-query20-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query20.sql\">query20.sql<\/a><\/li>\n<li><a href=\"#obj-query21-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query21.sql\">query21.sql<\/a><\/li>\n<li><a href=\"#obj-query22-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query22.sql\">query22.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 1\">Report 1<\/a><\/li>\n<li><a href=\"#obj-Report-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 2\">Report 2<\/a><\/li>\n<li><a href=\"#obj-Report-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 3\">Report 3<\/a><\/li>\n<li><a href=\"#obj-Report-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 4\">Report 4<\/a><\/li>\n<li><a href=\"#obj-Upstream-report-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Upstream report 1\">Upstream report 1<\/a><\/li>\n<li><a href=\"#obj-Upstream-report-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Upstream report 2\">Upstream report 2<\/a><\/li>\n<li><a href=\"#obj-Upstream-report-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Upstream report 3\">Upstream report 3<\/a><\/li>\n<\/ul>\n"},"Data store 3":{"type":"database","name":"Data store 3","depends":["query23.sql"],"dependedOnBy":["Report 5"],"docs":"<h2>Data store 3 <em>Access database<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>The database that provides some data for <a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a>.  It is located at:<\/p>\n\n<pre>\/path\/Data store 3\n<\/pre>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query23-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query23.sql\">query23.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a><\/li>\n<\/ul>\n"},"Data store 4":{"type":"database","name":"Data store 4","depends":["query24.sql"],"dependedOnBy":["Report 6"],"docs":"<h2>Data store 4 <em>Access database<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>The database that provides some data for <a href=\"#obj-Report-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 6\">Report 6<\/a>.  It is located at:<\/p>\n\n<pre>\/path\/Utilization_Triangles\\Data store 3\n<\/pre>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-query24-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query24.sql\">query24.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 6\">Report 6<\/a><\/li>\n<\/ul>\n"},"Intermediate step 1":{"type":"report","group":"Intermediate","name":"Intermediate step 1","depends":["Data store 1"],"dependedOnBy":["Report 6"],"docs":"<h2>Intermediate step 1 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-6\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 6\">Report 6<\/a><\/li>\n<\/ul>\n"},"Intermediate step 2":{"type":"report","group":"Intermediate","name":"Intermediate step 2","depends":["Data store 1"],"dependedOnBy":["Report 5"],"docs":"<h2>Intermediate step 2 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a><\/li>\n<\/ul>\n"},"Report 1":{"type":"report","name":"Report 1","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Report 1 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Report 2":{"type":"report","name":"Report 2","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Report 2 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Report 3":{"type":"report","name":"Report 3","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Report 3 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Report 4":{"type":"report","name":"Report 4","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Report 4 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Report 5":{"type":"report","name":"Report 5","depends":["Data store 3","Intermediate step 2"],"dependedOnBy":["Intermediate step 3"],"docs":"<h2>Report 5 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 3\">Data store 3<\/a><\/li>\n<li><a href=\"#obj-Intermediate-step-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 2\">Intermediate step 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by<\/h3>\n\n<ul>\n<li><a href=\"#obj-Intermediate-step-3\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 3\">Intermediate step 3<\/a><\/li>\n<\/ul>\n"},"Intermediate step 3":{"type":"report","group":"Intermediate","name":"Intermediate step 3","depends":["Report 5","Data store 1"],"dependedOnBy":[],"docs":"<h2>Intermediate step 3 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Report-5\" class=\"select-object\" data-type=\"select-object\" data-name=\"Report 5\">Report 5<\/a><\/li>\n<li><a href=\"#obj-Data-store-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 1\">Data store 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Report 6":{"type":"report","name":"Report 6","depends":["Data store 4","Intermediate step 1"],"dependedOnBy":[],"docs":"<h2>Report 6 <em>Report<\/em><\/h2>\n\n<div class=\"alert alert-warning\">No documentation for this object<\/div>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-4\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 4\">Data store 4<\/a><\/li>\n<li><a href=\"#obj-Intermediate-step-1\" class=\"select-object\" data-type=\"select-object\" data-name=\"Intermediate step 1\">Intermediate step 1<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Upstream report 1":{"type":"report","group":"Reporting","name":"Upstream report 1","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Upstream report 1 <em>Report<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This file is used to report some stuff upstream.  It is located at:<\/p>\n\n<pre>\/path\/Upstream report 1\n<\/pre>\n\n<p>It uses the following queries as data sources:<\/p>\n\n<ul>\n<li><a href=\"#obj-query14-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query14.sql\">query14.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Upstream report 2":{"type":"report","group":"Reporting","name":"Upstream report 2","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Upstream report 2 <em>Report<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This file is used to report some other stuff upstream.  It is located at:<\/p>\n\n<pre>\/path\/Upstream report 2\n<\/pre>\n\n<p>It uses the following queries as data sources:<\/p>\n\n<ul>\n<li><a href=\"#obj-query15-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query15.sql\">query15.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"},"Upstream report 3":{"type":"report","group":"Reporting","name":"Upstream report 3","depends":["Data store 2"],"dependedOnBy":[],"docs":"<h2>Upstream report 3 <em>Report<\/em><\/h2>\n\n<h3>Documentation<\/h3>\n\n<p>This file is used to report even more stuff upstream.  It is located at:<\/p>\n\n<pre>\/path\/Upstream report 3\n<\/pre>\n\n<p>It uses the following queries as data sources:<\/p>\n\n<ul>\n<li><a href=\"#obj-query16-sql\" class=\"select-object\" data-type=\"select-object\" data-name=\"query16.sql\">query16.sql<\/a><\/li>\n<\/ul>\n\n<h3>Depends on<\/h3>\n\n<ul>\n<li><a href=\"#obj-Data-store-2\" class=\"select-object\" data-type=\"select-object\" data-name=\"Data store 2\">Data store 2<\/a><\/li>\n<\/ul>\n\n<h3>Depended on by <em>(none)<\/em><\/h3>\n"}};
	         drawGraph();*/

			/* $('#docs-close').on('click', function() {
	             deselectObject();
	             return false;
	         });*/

			// This has been moved to the onclick handler for the t3 module.
			/*$(document).on('click', '.select-object', function() {
	             var obj = graph.data[$(this).data('name')];
	             if (obj) {
	                 selectObject(obj);
	             }
	             return false;
	         });*/

			$(window).on('resize', resize);
		},

		onclick: function(event, element, elementType) {
			if (elementType === "select-object"){
				var obj = search($(element).data('name'), graph.data.data);
				if (obj) {
					selectObject(obj);
				}
				return false;
			} else if (elementType === "docs-close"){
				deselectObject();
				return false;
			} else if (elementType === "download"){
				saveSvgAsPng(document.getElementById("graphDown"), "dependencyDiagram.png")
				return false;
			}
		},

		onmessage: function(name, data) {
			if(name === 'filterSelected') {
				showLoading(true);
				$(fetchDataForGraph().then(function(){
					drawGraph();
				}));
			}
		}

	}
})