/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-viewScatterGraph', function(context) {
	'use strict';
	// private methods here
	var $, commonUtils, $moduleDiv, title, message;
	var spinner = null;

	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function showScatterGraph(scatterWrapper) {

		var dataToPlot = [];
		$ = context.getGlobal('jQuery');
		$.each(scatterWrapper.series, function(index, oneSeries) {
			var oneSeriesForPlotting = {};
			oneSeriesForPlotting.key = oneSeries.seriesName;
			oneSeriesForPlotting.values = [];
			oneSeriesForPlotting.color = oneSeries.seriesColor;
			$.each(oneSeries.dataPoints, function(index2, oneCoordinate) {
				oneSeriesForPlotting.values.push({
					x : oneCoordinate.x,
					y : oneCoordinate.y,
					size : oneCoordinate.z,
					shape : 'circle',
					label : oneCoordinate.label
				});
			});
			dataToPlot.push(oneSeriesForPlotting);
		});

		if(scatterWrapper.series.length > 0){			
			$(".graphTitle").text(scatterWrapper.graphName);
			title = scatterWrapper.graphName;
			
		}
			if(scatterWrapper.message != null){
				$(".bcmError").text(scatterWrapper.message);
				$(".bcmError").show();
		}else{
			$(".bcmError").hide();
			
		}
			
		nv.addGraph(function() {
			var chart = nv.models.scatterChart().showDistX(false).showDistY(false).color(function(d, i) {
				var colors = dataToPlot.color;
				return colors[i % colors.length - 1];
			});
			//.color(d3.scale.category10().range());

			//Configure how the tooltip looks.
			chart.tooltip.contentGenerator(function(obj) {
				return '<h3>' + obj.point.label + '</h3>';
			});

			chart.tooltip.enabled();
			chart.forceY([ 0, 1 ]);
			chart.forceX([ 0, 1 ]);

			chart.xAxis.tickFormat(d3.format('.02f'));
			chart.yAxis.tickFormat(d3.format('.02f'));

			chart.xAxis.axisLabel(scatterWrapper.xAxisLabel);
			chart.yAxis.axisLabel(scatterWrapper.yAxisLabel);

			d3.select('#chart svg').attr('id', 'graphDown');
			d3.select('#chart svg').datum(dataToPlot).transition().duration(1000).call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});
	}
	

	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,
			success : function(scatterWrapper) {
				showScatterGraph(scatterWrapper);
				showLoading($('.scatterPlot'), false);
			}
		}
		$("#scatterGraphControl").ajaxForm(ajaxFormOptions);
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.scatterPlot'), true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
	}

	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2' ],
		
		messages: ['filterSelected'],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = $(context.getElement());
			initPageBindings();
		},
		onclick: function(event, element, elementType) {
			if (elementType === "download"){
				saveSvgAsPng(document.getElementById("graphDown"), "scatterGraph-" + title + ".png")
			}
		},
		onmessage: function(name, data) {
			 if(name === 'filterSelected') {
				 if($("#scatterGraphControl", $moduleDiv).find("select[name='parameterIds']").val() != -1 && $("#scatterGraphControl", $moduleDiv).find("select[name='questionnaireIds']").val() != -1){					 
					 $('button[type="submit"]').trigger('click');
				 }
			 }
		 }
	}
});