/**
 * @author: ananta.sethi 
 * module for edit workspace-()
 * 
 */
Box.Application.addModule('module-editworkSpace',function(context) {

	var $,commonUtils;


	return{
		// Initialize the page
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("workspace/delete");

			$("#userWorkspaceView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		}


	}


});