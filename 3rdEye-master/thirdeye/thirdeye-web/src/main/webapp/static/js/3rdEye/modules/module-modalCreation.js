/**
 * 
 */
Box.Application.addModule('module-modalCreation', function(context) {
	'use strict';
	
	var $, commonUtils, moduleConfig;
	var params ={};
	
	function getCreateModal(url, modalId) {
		
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL(url), params).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage(modalId, content, true);
			
			bindNewBCMTemplateModalRequirements($newModal, modalId);
			
			$newModal.modal({backdrop:false,show:true});
			params = {};
		});
	}
	
	function bindNewBCMTemplateModalRequirements ($newModal, modalId){
		// Bind the ajax form
		var formId = $newModal.find('form').attr('id');
		$("#"+formId, $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent(modalId, htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewBCMTemplateModalRequirements($replacement);
			}
		}); 
	}
	
	return{
		init: function(){
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			moduleConfig = context.getConfig();
		},
		
		onclick: function(event, element, elementType){
			if(elementType === 'createModal') {
				getCreateModal(moduleConfig.url, moduleConfig.modal);
			} else if(elementType === 'editModal') {
				if($(element).data('id')){
					params["id"] = $(element).data('id');
				}
				getCreateModal(moduleConfig.url, moduleConfig.modal);
			}
		}
	}
});