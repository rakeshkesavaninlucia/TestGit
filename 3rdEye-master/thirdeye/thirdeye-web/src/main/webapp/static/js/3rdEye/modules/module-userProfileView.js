/* 
 *  Author- Shagun.sharma
 *  module for user profile validations and display user image
 */
Box.Application.addModule('module-userProfileView', function(context) {

	'use strict';
	var $;
	// private methods here
	function validateDropDown() {

		$('#question1dropdown').on('change',function(){
			if($(this).val()==='-1'){
				$('#answerTextfield').hide();
			}
			else{
				$('#answerTextfield').show();	
			}
		})

		$('#question2dropdown').on('change',function(){
			if($(this).val()==='-1'){
				$('#answerTextfields').hide();
			}
			else{
				$('#answerTextfields').show();	
			}
		})
	}
	
	function readURL(input){
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#user-img')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	function showImage(){
		$('i').click(function () {
			$("input[type='file']").trigger('click');
		});
		$('input[type="file"]').on('change', function() {
			readURL(this);
		})
	}

	return {
		behaviors : [ 'behavior-select2'],

		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');  
			validateDropDown();
			showImage();
		}
	};
});