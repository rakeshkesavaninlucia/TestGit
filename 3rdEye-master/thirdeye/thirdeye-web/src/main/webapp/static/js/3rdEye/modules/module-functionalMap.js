/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-functionalMap', function(context) {

	var $;

    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            
            $(".dropdown_select").select2({
				  placeholder: "Select",
				  allowClear: true
			});
        }
    };
});