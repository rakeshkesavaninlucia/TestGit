/**
 * @author: akshay.mathur
 */
Box.Application.addModule('module-adminApplicationInterfaceDiagram', function(context) {
	'use strict';
	
	// private methods here
	var $, $moduleDiv, commonUtils;
	
	function getAssetClasses(){

		var aid = $('.appId').data('aid');
		var url = commonUtils.getAppBaseURL("adminaid/" + aid + "/viewAID");

		var filterURL = commonUtils.readCookie("filterURL");

		if (filterURL !== undefined && filterURL !== null) {
			var urlData = filterURL.split('?').pop();
			url = url + "?" + urlData;
		}
		var postRequest = $.get(url);
		postRequest.done(function(incomingData) {
			$moduleDiv.empty().append(incomingData);
			context.broadcast('lazyLoad', urlData);
		});
	}
	
	return {
		behaviors: ['behavior-collapseable','behavior-lazyLoaded'],
		messages: ['filterSelected'],
		
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
		
			$moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');
			getAssetClasses();
			//var _this = this;
			$(".convert").click(
					function() {
						html2canvas($(".box").get(0), {
							onrendered : function(canvas) {
								// Lets not show the canvas and then download it
								// straight away.
								// Ideally iterate over all the Aid and then
								// convert and download.
								//var $canvas = $(canvas);
								//$canvas.hide();
								//$canvas.appendTo(".box");
								//$(".downloadAsImage").attr("href",
								//		canvas.toDataURL("image/jpeg", 1.0));
								//$(".downloadAsImage").attr("download",
								//		"aid.jpg");
								// $(".downloadAsImage").click();
							},
						/*
						 * width: 600, height: 400
						 */
						});
			});
			
			
		},
	
		onmessage : function(name, data){
			if (name == 'filterSelected') {
				getAssetClasses();
			}
		}
	}
});