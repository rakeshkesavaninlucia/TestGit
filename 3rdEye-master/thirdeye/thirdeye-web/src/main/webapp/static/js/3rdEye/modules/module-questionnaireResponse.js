/**
 * @author samar.gupta
 */
Box.Application.addModule('module-questionnaireResponse', function(context) {
	'use strict';
	var $;
	
	// private methods here
	
	function bindBlurToParameters(){
		
		
		$(".editableParameter", $(".userquestionnaire")).blur(function(){
			var $editedParam = $(this);
			var $formForParam = $editedParam.closest("form.editableForm");
			
			
			$formForParam.ajaxForm(function(htmlResponse) {							
				if (htmlResponse.indexOf("error") > -1 ) {								
					 $("#"+htmlResponse.replace("error_", "")).html("Question Score can not be less than 1 and greater than 10.")
	                 .addClass("error_msg");
				}else{
					$("#"+htmlResponse.replace("success_", "")).empty();
				}						
			}); 
			
			$formForParam.submit();
			
		})
	}

	return {
		
		init : function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			bindBlurToParameters();
		}  // end of init
	};
});
