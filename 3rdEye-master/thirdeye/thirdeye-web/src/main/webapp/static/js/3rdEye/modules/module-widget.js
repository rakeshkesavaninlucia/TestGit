/**
 * 
 */
Box.Application.addModule('module-widget', function(context) {
	'use strict';
	
	var $, commonUtils, $moduleDiv;
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner($moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	function fetchQuestionValues(qeId){
		var deferred = $.Deferred();
		
		var urlToHit = commonUtils.getAppBaseURL("widgets/facets/fetchQuestions/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}
	
	function fetchParameterValues(qeId){
		var deferred = $.Deferred();
		
		var urlToHit = commonUtils.getAppBaseURL("widgets/facets/fetchParameters/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
				deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}
	
	function showDiscreteBarGraph($widget,graphData){
		nv.addGraph(function() {
			  var chart = nv.models.discreteBarChart()
                  .x(function(d){ 
			            if(d.label.length > 10){						          		
			                return (d.label).substr(0, 5) + '...' + (d.label).substr((d.label).length-7, (d.label).length)
			    		}
			            else{	
			    			return d.label
			            }
			        })    //Specify the data accessors.
			      .y(function(d) { return d.value })
//                           .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
			      // .tooltips(false)     //Don't show tooltips
			    .showValues(true) 
			      //...instead, show the bar value right on top of each bar.
			      //.transitionDuration(350)
			      ;
			  
			  // important jQuery to D3 conversion
			   chart.xAxis.rotateLabels(-25);
			   chart.margin({bottom: 60});
			   chart.tooltip.contentGenerator(function (d) { 
	             return "<table><div class='smallRectangle' style='background-color:"+d.color+";'></div><tr><td style='text-align: right;'>"+d.data.label+"</td><td style='text-align: right;'>"+d.data.value+"</td></tr></table>";
               });
			   
			   
			   
			  var idOfChart = $(".facetGraph", $widget).attr("id");
			  d3.selectAll("#"+idOfChart +" svg > *").remove();
			  d3.select("#"+idOfChart +" svg")
			      .datum(graphData)
			      .call(chart);

			  nv.utils.windowResize(chart.update);

			  return chart;
			});
	}
	
	function showDonut($widget,graphData){
		nv.addGraph(function() {
			  var chart = nv.models.pieChart()
			      .x(function(d) { return d.label })
			      .y(function(d) { return d.value })
			      .showLabels(true)     //Display pie labels
			      .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
			      .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
			      .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
			      .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
			      ;	
			// important jQuery to D3 conversion
			  var idOfChart = $(".facetGraph", $widget).attr("id");
			  d3.selectAll("#"+idOfChart +" svg > *").remove();
			  d3.select("#"+idOfChart +" svg")
			        .datum(graphData[0].values)
			        .transition().duration(350)
			        .call(chart);

			  return chart;
			});
	}
	
	/*
	 * Loads the widgets on the dashboard based on the .widgetplacement class
	 * 
	 */
	function loadPageWidgets(){
		// Allow each widget to load asynchronously.
		var widgetId = $moduleDiv.data( "widgetid" );
		
		// Lets load the actual widget content
		var loadWidgetPromise = _loadWidgetContent($moduleDiv, widgetId)
		
		loadWidgetPromise.done(function($widgetContainer){
			// Widget setup
			// The widgettype data attribute will tell me which widget to setup
			setupWidget($widgetContainer);
		});
		loadWidgetPromise.fail();
	}
	
	function setupWidget( $widgetContainer) {
		initializeWidget($widgetContainer);
	}
	
	/*
	 * Pull the actual widget content from the server and replace content 
	 * of the placeholder.
	 */
	function _loadWidgetContent($replacementContainer, widgetid){
		var deferredContentLoading =  $.Deferred();
		
		// The URL from which you load the content
		var widgetURL = commonUtils.getAppBaseURL("widgets/load/" + widgetid);
		
		var fetchContentPromise = fetchWidgetHTMLContent(widgetURL);
		
		fetchContentPromise.done (function(newHTMLContent){
			// With the new HTML content
			// You can validate response and then insert into the target
			$replacementContainer = $replacementContainer.empty();
			$replacementContainer = $replacementContainer.append(newHTMLContent);
			
			// Pass the reference for the new widget
			deferredContentLoading.resolve($replacementContainer);
		});
		fetchContentPromise.fail (function(){deferredContentLoading.reject($replacementContainer);});
		
		// Place in the target div
		return deferredContentLoading.promise();
	}
	
	function fetchWidgetHTMLContent(urlToLoadFrom){
		var deferredContentFetch =  $.Deferred();
		$.get( urlToLoadFrom, function( newHTMLContent ) {
			deferredContentFetch.resolve(newHTMLContent);
		});
		return deferredContentFetch.promise();
	}
	
	function deleteWidget() {
		if (confirm("You are about to delete the widget from your dashboard. This operation can not be undone.")){
			var dashboardId =  $moduleDiv.parents('.dashboard').first().data("dashboardid");
			var widgetId = $moduleDiv.data("widgetid");
			
			if (dashboardId && widgetId){
				// Send the delete
				var param = {};
				param["dashboardId"] = dashboardId;
				var postRequest = $.post( commonUtils.getAppBaseURL("widgets/delete/"+widgetId), param);
				postRequest.done(function(){
					$moduleDiv.slideUp();
					$moduleDiv.remove();
				})
		
		}
	}
	}
	function showRequest(formData, jqForm, options) {
		showLoading(true);
		//var url = commonUtils.getAppBaseURL("widgets/facets/plot");
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
      
	}
	
	function dashboardpanel(box){
	   //var box = $('.direct-chat',$widgetContainer);//$widgetContainer.parents('.direct-chat').first();
	    box.toggleClass('direct-chat-contacts-open');
	}
	
	function initializeWidget($widgetContainer){
		var ajaxFormOptions = {
				beforeSubmit : showRequest,
				success: function(dataReturned){
					
					// Make sure we dont save repetitively.
					$('.graphFacetsControl input[name=saveWidget]', $widgetContainer).val("false");
					
					// Depending on selected graph type show the graph
					if (typeof $('input[name=graphType]:checked', $widgetContainer).val() === "undefined") {
						if ($widgetContainer.data("widget-status") === 'loaded'){
							alert("Please select an output graph type");
						} 
					} else {
						$(".facetGraph", $widgetContainer).fadeIn("slow");
						
						if ($('input[name=graphType]:checked', $widgetContainer).val() === 'bar'){
							showDiscreteBarGraph($widgetContainer, dataReturned);
						} else if ($('input[name=graphType]:checked', $widgetContainer).val() === 'pie'){
							showDonut($widgetContainer, dataReturned);
						}
					}
					showLoading(false);
					
					// Lets make the configuration go away.
					if ($widgetContainer.data("widget-status") === 'loaded'){
						$('[data-widget="direct-chat-contacts-open"]', $widgetContainer).trigger( "click" );
//						var box = $('.direct-chat',$widgetContainer);//$widgetContainer.parents('.direct-chat').first();
//					    box.toggleClass('direct-chat-contacts-open');
					} else {
						$widgetContainer.data("widget-status",'loaded') ;
					}
					Box.Application.startAll($('widgetContentWrapper'));
				}
			};
			
			$("input.plotChart",$widgetContainer).on('click', function(){
				$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
				dashboardpanel($('.direct-chat',$widgetContainer));
			});
			
			$("input.saveWidget",$widgetContainer).on('click', function(){
				$('.graphFacetsControl input[name=saveWidget]', $widgetContainer).val("true");
				$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
				dashboardpanel($('.direct-chat',$widgetContainer));
			});
			
			$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
			
			Box.Application.startAll($moduleDiv);
	}
	
	
	
	return{
		behaviors: ['behavior-collapseable'],
		
		messages: ['filterSelected'],
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			$moduleDiv = $(context.getElement());
			
			loadPageWidgets();
			
		},
		
		onmessage : function(name, data){
			if (name == 'filterSelected') {
				loadPageWidgets();
			}
		},
		
		onclick: function(event, element, elementType) {
			if (elementType === 'chat-pane-toggle') {
				dashboardpanel( $(element).parents('.direct-chat').first());
			    
			} else if (elementType === 'deleteWidget') {
				deleteWidget();
			}
		},
		
		onchange: function(event, element, elementType) {
			switch (elementType) {
			case 'ajaxSelector':
				var depName = $(element).data("dependency");
				var depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
				// Scope to the widget
				var $matchingSelectors = $(depSelector, $moduleDiv);
				
				$matchingSelectors.each(function() {
					var $this = $(this);
					if ($this.is(".questionSelector") && $(element).val() > 0){
						fetchQuestionValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
					
					if ($this.is(".paramSelector") && $(element).val() > 0){
						fetchParameterValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
				});
			}
		}
	}
	
	
});