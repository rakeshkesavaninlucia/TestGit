/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-parameterColoring', function(context) {

	var $, commonUtils, colors, moduleEl, notifyService;
	var dataObj = {};

	function addCondition(data){
		var deferred = $.Deferred();
		addRowToTableFromURL("qualityGateCondition",commonUtils.getAppBaseURL("qualityGates/newRow"), data, deferred);
	}
	
	function addRowToTableFromURL(tableId, urlToHit, data, deferred){
		if ($("#"+tableId+" tbody").length === 0){
			$("#"+tableId).append("<tbody></tbody>");
		}
		commonUtils.getHTMLContent( urlToHit, data).then(function( newRowContent ) {
			$("#"+tableId+" tbody").empty();
			$("#"+tableId+" tbody").append(newRowContent);
			deferred.resolve();
		});	
		return deferred.promise();
	}
	
	function saveDescription(data, $trRef){
		commonUtils.getHTMLContent( commonUtils.getAppBaseURL("qualityGates/saveDescription"), data).then(function( incomingData ) {
			if($(incomingData).hasClass('error_msg')){
				$('#descriptionError', moduleEl).empty();
				$('#descriptionError', moduleEl).append(incomingData);
				//notify
				notifyService.setNotification("Field Missing", "", "error");
			} else {
				$('#descriptionError', moduleEl).empty();
				$trRef.replaceWith(incomingData);
				//notify
				if($(incomingData).find('.error_msg').size()>0){
					notifyService.setNotification("Field Missing", "", "error");
				}
				else{
					notifyService.setNotification("Saved", "", "success");	
				}
			}
		});
	}
	
	function saveRow(data, $trRef){
		commonUtils.getHTMLContent( commonUtils.getAppBaseURL("qualityGates/saveRow"), data).then(function( incomingData ) {
			if($(incomingData).hasClass('error_msg')){
				$('#conditionError', moduleEl).empty();
				$('#conditionError', moduleEl).append(incomingData);
			} else {
				$('#conditionError', moduleEl).empty();
				$trRef.replaceWith(incomingData);
			}
		});
	}
	
	function editDescription($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/editDescription"), data);
		postRequest.done(function(incomingData){
			$trRef = $(incomingData).replaceAll($trRef);
			$trRef.addClass("pre-save is-saving");
		});
	}
	
	function editCondition($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/editRow/"+$trRef.attr("id")), data);
		postRequest.done(function(incomingData){
			$trRef = $(incomingData).replaceAll($trRef);
			$trRef.addClass("pre-save is-saving");
		});
	}
	
	function deleteCondition($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/deleteRow"), data);
		postRequest.done(function(){
			$trRef.find('td').hide();
		});
	}
	
	function callQualityGate(){
		var data = {};
		data.parameterId = $('#parameterId').val();
		var scale = $('#colorScale').val();
		if(scale == 2) {
			data.scale = ['#d7191c', '#1a9641'];
		} else {
			data.scale = colors.RdYlGn[scale];
		}
		data.qualityGateId = $('#qualityGate').val();
		addCondition(data);
		data.action1 ="add";
		context.broadcast('qgd::addQualityGateDescription',data);
	}
	
    return {
    	
    	behaviors: ['behavior-lazyLoaded'],
    	//messages: ['qgd::addQualityGateDescription'],
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            colors = context.getGlobal('colorbrewer');
            notifyService = context.getService('service-notify');
            moduleEl = context.getElement();
        },
    
        onclick: function(event, element, elementType) {
        	var scale;
        	var $trRef;
        	if (elementType === "saveDescription") {
        		dataObj = {};
        		$trRef = $(event.target).closest( "tr" );
        		var description = $('.dirtyrow').find("input[name='description']");
        		var descriptioncolors = $('.dirtyrow').find("input[name='descriptionColor']");
        		scale = $('#colorScale').val();
        		dataObj.description = [scale];
        		dataObj.color = [scale];
        		dataObj.qualityGateId = $('#qualityGate').val();
        		$.each(descriptioncolors, function(i){
        			dataObj.color[i] = descriptioncolors[i].value;
        		});
        		$.each(description, function(i){
        			dataObj.description[i] = description[i].value;
        		});
        		saveDescription(dataObj, $trRef);
        	} if (elementType === 'editDescription') {
        		$trRef = $(event.target).closest( "tr" );
        		$trRef.addClass("pre-save");
				$trRef.addClass("is-saving");
        		dataObj.qualityGateId = $('#qualityGate').val();
        		editDescription($trRef, dataObj);
        	}
        	if (elementType === 'saveRow') {
        		$trRef = $(event.target).closest( "tr" );
        		var values = $('.dirtyrow').find("input[name='conditionValue']");
        		var conditioncolors = $('.dirtyrow').find("input[name='conditionColor']");
        		scale = $('#colorScale').val();
        		dataObj.value = [scale];
        		dataObj.color = [scale];
        		$.each(conditioncolors, function(i){
        			dataObj.color[i] = conditioncolors[i].value;
				});
        		$.each(values, function(i){
        			dataObj.value[i] = values[i].value;
				});
        		dataObj.weight = $('#weight').val();
        		saveRow(dataObj, $trRef);
        	} else if (elementType === 'editRow') {
        		$trRef = $(event.target).closest( "tr" );
        		$trRef.addClass("pre-save");
				$trRef.addClass("is-saving");
        		dataObj.id = $trRef.attr("id");
        		editCondition($trRef, dataObj);
        	} else if (elementType === 'deleteRow') {
        		$trRef = $(event.target).closest( "tr" );
        		dataObj.id = $trRef.attr("id");
        		deleteCondition($trRef, dataObj);
        	}
        },
        
        onchange: function(event, element, elementType) {
        	if (elementType === 'colorScaleSelected') {
        		callQualityGate();
        	}
        }
    };
});