/**
 * 
 */

Box.Application.addModule('module-parameterModal', function(context) {

    // private methods here
	var $, commonUtils,moduleEl, _moduleConfig;
	var dataObj = {};
	var pageUrl = "parameterSelector/fetchModal";
	
	function showParameterSelectorModel(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})
		});	
	}
	function hideParameterSelectorModel(){
		$(moduleEl).modal('hide');
	}
	function requestForSaveParam(){		
		var table = $('.paramListTable',moduleEl).DataTable();
		var dataParameter = table.$('.parameterClass:checked').map(function() { return this.value; }).get();
		dataObj.parameter = dataParameter;	
	    context.broadcast('pm::parameterSelected', dataObj)
	}
   
    return {

    	messages: [ 'pm::showParameterSelector','pm::paramProcessComplete'],

        onmessage: function(name, data) {
            if (name === 'pm::showParameterSelector') {
            	if(_moduleConfig.mode === 'questionnaireParameter'){
            		dataObj.questionnaireId = data.questionnaireId;
            		dataObj.questionnaireType = data.questionnaireType;
            		dataObj.mode = _moduleConfig.mode;
            		showParameterSelectorModel(dataObj);
            	}
            } else if (name === 'pm::paramProcessComplete') {
            	hideParameterSelectorModel();            	
            }
        },
        
        onclick: function(event, element, elementType) {        
        	if (elementType === 'saveParameters') {
        		requestForSaveParam();
        		event.preventDefault();
        	}
        },
    
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            _moduleConfig = context.getConfig();
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	_moduleConfig = null;    // clear the reference
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});