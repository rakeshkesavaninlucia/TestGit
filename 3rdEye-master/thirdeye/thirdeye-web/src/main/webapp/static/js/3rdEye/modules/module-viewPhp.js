/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-viewPhp', function(context) {
	'use strict';
	var $,commonUtils, $moduleDiv;
	var spinner = null;
		
	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function showPHPHeatMap(phpWrapper) {
		function heatMap(data) {
			var config = {};
			// If you have multiple sets of data that you want to show - you may
			// have to render the whole
			// thing from scratch. X and Y axis may change
			config.columns = [];
			config.columns.group =  [];
			config.columns.groupsize = []
			for (var i = 0; i < data.parameters.length; i++) {
				config.columns.group.push(data.parameters[i].key);
				config.columns.groupsize.push(data.parameters[i].value.length);
				for (var j = 0; j < data.parameters[i].value.length; j++) {
					config.columns.push(data.parameters[i].value[j]);
				}
			}
			config.rows = [];
			config.rows.group =  [];
			config.rows.groupsize = []
			for (var i = 0; i < data.assets.length; i++) {
				config.rows.group.push(data.assets[i].key);
				config.rows.groupsize.push(data.assets[i].value.length);
				for (var j = 0; j < data.assets[i].value.length; j++) {
					config.rows.push(data.assets[i].value[j]);
				}
			}
			config.colors = colorbrewer.RdYlGn[5];
			config.buckets = config.colors.length;

			var maxGridSize = 38;
			var margin = { top: 80, right: 0, bottom: 100, left: 170 };
			var width = 960 - (margin.left + margin.right);

			if (Math.floor(width / config.columns.length) > maxGridSize){
				config.gridSize = maxGridSize;
			} else {
				config.gridSize = Math.floor(width / config.columns.length);
			}

			// Initial SVG element
			var height = (config.rows.length + 4 ) * config.gridSize;

			// remove any previously loaded svg
			d3.select("svg").remove();

			var title = "";
			config.columns.forEach(function(value) {title = title + value + ", ";})
			if(title == ""){
				$(".mapTitle").text("No Data available");
				return;
			} else {				
				$(".mapTitle").text("Showing portfolio health for "+ title.substring(0,title.length-2));
			}

			var svg = d3.select("#chart").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// Rows to be lined up
			var rowLabels = svg.selectAll(".heatMapRow")
			.data(config.rows)
			.enter().append("text")
			.text(function (d) { return (d.length > 12) ? (d.substring(0,12)).trim() + "..." : d; })
			.attr("x", 0)
			.attr("y", function (d, i) { return i * config.gridSize; })
			.attr("dy", ".01em")
			 .style("text-anchor", "end")
			 .style("font-size","9pt")
			.style("font-family", "Consolas, courier")
			.style("fill", "#aaa")
			.attr("transform", "translate(-4," + config.gridSize / 1.75 + ")")
			// .attr("class", function (d, i) { return ((i >= 0 && i <= 4) ?
			// "dayLabel mono axis axis-workweek" : "dayLabel mono axis"); });
			.attr("class", function (d, i) { return ("heatMapRow mono axis"); });

			svg.selectAll(".heatMapRow").call(wrap, 75);

			var count = 0;
			var rowLabelsGrp = svg.selectAll(".heatMapRowGrp").append("g")
			.data(config.rows.group)
			.enter().append("text")
			.text(function (d) { return d; })
			.attr("x", 0)
			.attr("y", function(d, i) { count = count + config.rows.groupsize[i]; return config.gridSize * (count-config.rows.groupsize[i]);})
			// .attr("dy", ".01")
			 .style("text-anchor", "end")
			 .style("font-size","9pt")
			.style("font-family", "Consolas, courier")
			.style("fill", "#aaa")
			.attr("transform", function(d, i) { return "translate(-120," + config.gridSize * config.rows.groupsize[i]/2  + ")"; })
			// .attr("class", function (d, i) { return ((i >= 0 && i <= 4) ?
			// "dayLabel mono axis axis-workweek" : "dayLabel mono axis"); });
			.attr("class", function (d, i) { return ("heatMapRow mono axis"); }); 

			var inc = 0; 
			var colGrpLabels = svg.selectAll(".heatMapColGrp")
			.data(config.columns.group)
			.enter().append("text")
			.text(function(d) { return d; })
			.attr("x", function(d, i) { inc = inc + config.columns.groupsize[i]; return config.gridSize * (inc-config.columns.groupsize[i]); })
			.attr("y", 0)
			.attr("transform", function(d, i) { return "translate(" + config.gridSize * config.columns.groupsize[i]/2  + ", -20)"; })
			// .attr("class", function(d, i) { return ((i >= 7 && i <= 16) ?
			// "heatMapCol mono axis axis-worktime" : "heatMapCol mono axis");
			// });
			.style("font-size","9pt")
			.style("font-family", "Consolas, courier")
			.style("fill", "#aaa")
			.attr("class", function(d, i) { return ("heatMapColGrp mono axis"); });

			var colLabels = svg.selectAll(".heatMapCol")
			.data(config.columns)
			.enter().append("text")
			.text(function(d) { return (d.length > 4) ? (d.substring(0,4)).trim() + "..." : d;  })
			.attr("x", function(d, i) { return i * config.gridSize; })
			.attr("y", 0)
			.attr("transform", function(d, i) { return "translate(" + config.gridSize / 2 + ", -6)"})
			.style("text-anchor","middle")
			.style("font-size","9pt")
			.style("font-family", "Consolas, courier")
			.style("fill", "#aaa")
			// .attr("class", function(d, i) { return ((i >= 7 && i <= 16) ?
			// "heatMapCol mono axis axis-worktime" : "heatMapCol mono axis");
			// });
			.attr("class", function(d, i) { return ("heatMapCol mono axis"); });

			var heatMapChart = function (config, data) {
				var colorScale = d3.scale.quantile()
				.domain([0, config.buckets - 1, d3.max(data, function (d) { return d.value; })])
				.range(config.colors);

				// This will become each of out RAG box
				var cards = svg.selectAll(".heatIndex").data(data, function(d) {return d.rowDesc + ":" + d.colDesc;});

				cards.append("title");

				cards.enter().append("rect")
				.attr("x", function(d) { 
					var colIndex = config.columns.indexOf(d.colDesc);
					if (colIndex > -1){
						return (colIndex) * config.gridSize;
					}
				})
				.attr("y", function(d) { 
					var rowIndex = config.rows.indexOf(d.rowDesc);
					if (rowIndex > -1){
						return (rowIndex) * config.gridSize;
					}
				})
				.attr("rx", 4)
				.attr("ry", 4)
				.attr("class", "heatIndex bordered")
				.attr("width", config.gridSize)
				.attr("height", config.gridSize)
				.style("stroke", "#E6E6E6")
				.style("stroke-width", "2px")
				.style("fill", config.colors[0]);

				cards.transition().duration(1000)
				.style("fill", function(d) { return d.color; });

				cards.select("title").text(function(d) { 
					return d.value; });

				cards.exit().remove();
			}

			heatMapChart(config,data.wrappers);
		}

		heatMap(phpWrapper[0]);
		
		function wrap(text, width) {
			text.each(function() {
				var text = d3.select(this),
				words = text.text().split(/\s+/).reverse(),
				word,
				line = [],
				lineNumber = 0,
				lineHeight = 1.1, // ems
				y = text.attr("y"),
				dy = parseFloat(text.attr("dy")),
				tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
				while (word = words.pop()) {
					line.push(word);
					tspan.text(line.join(" "));
					if (tspan.node().getComputedTextLength() > width) {
						line.pop();
						tspan.text(line.join(" "));
						line = [word];
						tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
					}
				}
			});
		}
	}

	function fetchAssetTemplateValues(asseturlToHit){
		var deferred = $.Deferred();
		commonUtils.getHTMLContent(asseturlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}// EOF[_fetchAssetTemplateValues]

	function fetchParameterValues(paramUrlToHit){
		var deferred = $.Deferred();
		commonUtils.getHTMLContent(paramUrlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	} //EOF[_fetchParameterValues]
	
	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,
			success: function(phpWrapper){
				generateHrefLink();
				showPHPHeatMap(phpWrapper);
				showLoading($('.phpHeatMap'), false);
			}
		}
		$("#phpHeatMapControl", $moduleDiv).ajaxForm(ajaxFormOptions); 
		$(".ajaxSelector", $moduleDiv).change(function(){
			var $selector = $(this);
			// find value 
			var paramUrlToHit = commonUtils.getAppBaseURL($(this).data("parameterurl")+ $selector.val());
			var asseturlToHit = commonUtils.getAppBaseURL($(this).data("asseturl")+ $selector.val());
			var depName = $selector.data("dependency");
			var depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
			var $matchingSelectors = $(depSelector);

			$matchingSelectors.each(function( index ) {
				var $this = $(this);
				if ($this.is(".assetTemplateSelector")){
					if ($selector.val() > 0){
						fetchAssetTemplateValues(asseturlToHit).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						})
					}
				}

				if ($this.is(".parameterSelector")){
					if ($selector.val() > 0){
						fetchParameterValues(paramUrlToHit).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						})
					}
				}
			});
		});	
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.phpHeatMap'), true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
	}
	
	
	function generateHrefLink(data){
		var url = commonUtils.getAppBaseURL("portfoliohealth/downloadExcel");
		var link = url + '?parameterIds='+$("#phpHeatMapControl", $moduleDiv).find("select[name='parameterIds']").val()+
		                 '&assetTemplateIds=' + $("#phpHeatMapControl", $moduleDiv).find("select[name='assetTemplateIds']").val()+
		                 '&questionnaireId='+ $("#phpHeatMapControl", $moduleDiv).find("select[name='questionnaireId']").val();
		$("#link").attr("href", link);
	}

	return{

		behaviors : [ 'behavior-select2', 'behavior-downloadD3' ],
		
		messages: ['filterSelected'],
		
		init : function(){
			$ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            $moduleDiv = $(context.getElement());
			
            initPageBindings();

		}, // end of init for View PHP
		
		onmessage: function(name, data) {
			 if(name === 'filterSelected') {
				 if($("#phpHeatMapControl", $moduleDiv).find("select[name='parameterIds']").val() != null && $("#phpHeatMapControl", $moduleDiv).find("select[name='assetTemplateIds']").val()
						 && $("#phpHeatMapControl", $moduleDiv).find("select[name='questionnaireId']").val()){					 
					 $('button[type="submit"]').trigger('click');
				 }
			 }
		 }
	}
});