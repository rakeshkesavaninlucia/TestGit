/**
 * @author: ananta.sethi
 * module for applying select on select asset type in Questionnaire and chart of account
 */
Box.Application.addModule('module-createQuestionnaireselect', function(context) {

	// private methods here

	return {
		behaviors : [ 'behavior-select2']
	};
});
