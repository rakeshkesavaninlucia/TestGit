/**
 * @author: ananta.sethi 
 * 
 */
Box.Application.addModule('module-assetSelect2',function(context) {
	'use strict';

	var $,_moduleConfig,commonUtils;

	function assetApplication(id){
		window.location.href = commonUtils.getAppBaseURL("templates/asset/view/"+id);
	}


	function nextAsset(){
		$('#assetselection').change(function(){
			var id =$('#assetselection').val();

			assetApplication(id);
		});
	}

	function nextbuttonAsset(){
		var id = $('#assetselection option:selected').next().val()
		if($("#assetselection option:selected").next().val() ===undefined){
			var idofasset =$('#assetselection').val();
			assetApplication(idofasset);
			$(".rightarrow").attr('disabled', 'disabled'); 
		}
		else{
			assetApplication(id);
		}
	}
	function leftbuttonAsset(){
		var id = $('#assetselection option:selected').prev().val()
		//If current asset is the first one in list ,
		if($("#assetselection option:selected").prev().val() ==="-1"){
			var ids =$('#assetselection').val();
			assetApplication(ids);
			$(".leftarrow").attr('disabled', 'disabled');
		}
		else{
			assetApplication(id);
		}
	}
	return {

		behaviors : [ 'behavior-select2'],

		// Initialize the page
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');  
			_moduleConfig = context.getConfig(); 
			commonUtils = context.getService('commonUtils');
			nextAsset();
		},

		onclick: function(event, element, elementType) {        
			if (elementType === 'edit') {
				var dataobj= {};
				dataobj.assteid= _moduleConfig.assetId;
				dataobj.template =  _moduleConfig.templateid;
				context.broadcast('editasset',dataobj);
			}
			if (elementType === 'save') {
				context.broadcast('saveassetdetail');
			}
			if (elementType === 'rightButton'){
				nextbuttonAsset();
			}
			if (elementType === 'leftButton'){
				leftbuttonAsset();
			}
		}

	}
});