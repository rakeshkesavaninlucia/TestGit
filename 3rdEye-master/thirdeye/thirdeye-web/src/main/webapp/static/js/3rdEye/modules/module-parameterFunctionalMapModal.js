/**
 * @author samar.gupta
 */

Box.Application.addModule('module-parameterFunctionalMapModal', function(context) {

    // private methods here
	var $, commonUtils,moduleEl;
	var dataObj = {};
	var pageUrl = "parameter/fc/showModal";
	
	function showFunctionalMapSelectorModal(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})
		});	
	}
	function hideFunctionalMapSelectorModal(){
		$(moduleEl).modal('hide');
	}
	function saveFunctionalMapTemporary(event){		
		var table = $('#functionalMapListInModal').DataTable();
		var dataFunctionalMap = table.$('.functionalMapClass:checked').map(function() { return this.value; }).get();
		dataObj.functionalMapId = dataFunctionalMap;
		var postRequest = $.post( commonUtils.getAppBaseURL("parameter/fc/saveTemporary"), dataObj);
		postRequest.done(function(htmlResponse){
			$('#selectedFunctionalMapTableId').remove();
			$( htmlResponse ).insertAfter( "#questionParamFooter" );
			$('.errorDiv').remove();
			hideFunctionalMapSelectorModal();
		})
	}
   
    return {

    	messages: [ 'pfmm::showFunctionalMapSelector','pfmm::functionalMapProcessComplete'],

        onmessage: function(name, data) {
            if (name === 'pfmm::showFunctionalMapSelector') {
            		showFunctionalMapSelectorModal(dataObj);
            } else if (name === 'pfmm::functionalMapProcessComplete') {
            	hideFunctionalMapSelectorModal();            	
            }
        },
        
        onclick: function(event, element, elementType) {        
        	if (elementType === 'saveFunctionalMapTemporary') {
        		saveFunctionalMapTemporary(event);
        		event.preventDefault();
        	}
        },
    
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});