/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-viewWaveAnalysis', function(context) {
	'use strict';
	// private methods here
	var $, commonUtils, $moduleDiv;
	var spinner = null;
    var scatterWrapperData = null;
	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function showScatterGraph(scatterWrapper) {

		var dataToPlot = [];
		$ = context.getGlobal('jQuery');
		$.each(scatterWrapper.series, function(index, oneSeries) {
			var oneSeriesForPlotting = {};
			oneSeriesForPlotting.key = oneSeries.seriesName;
			oneSeriesForPlotting.values = [];
			oneSeriesForPlotting.color =  oneSeries.seriesColor;
			$.each(oneSeries.dataPoints, function(index2, oneCoordinate) {
				oneSeriesForPlotting.values.push({
					x : oneCoordinate.x,
					y : oneCoordinate.y,
					size : oneCoordinate.z,
					shape : 'circle',
					label : oneCoordinate.label
				});
			});
			dataToPlot.push(oneSeriesForPlotting);
		});

		if(scatterWrapper.series.length > 0){			
			$(".graphTitle").text(scatterWrapper.graphName);
		}
		nv.addGraph(function() {
			var chart = nv.models.scatterChart().showDistX(false).showDistY(false).color(function(d, i) {
				var colors = dataToPlot.color;
				return colors[i % colors.length - 1];
			});
			//.color(d3.scale.category10().range());

			//Configure how the tooltip looks.
			chart.tooltip.contentGenerator(function(obj) {
				return '<h3>' + obj.point.label + '</h3>';
			});

			chart.tooltip.enabled();
			chart.forceY([ 0, 1 ]);
			chart.forceX([ 0, 1 ]);

			chart.xAxis.tickFormat(d3.format('.02f'));
			chart.yAxis.tickFormat(d3.format('.02f'));

			chart.xAxis.axisLabel(scatterWrapper.xAxisLabel);
			chart.yAxis.axisLabel(scatterWrapper.yAxisLabel);

			d3.select('#chart svg').datum(dataToPlot).transition().duration(1000).call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});
	}
	

	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,		
			success : function(scatterWrapper) {
				showScatterGraph(scatterWrapper);
				showLoading($('.scatterPlot'), false);
				showWavesPoint(scatterWrapper);
			}
		}
		$("#scatterGraphControl").ajaxForm(ajaxFormOptions);
	}
	
	function showWavesPoint(scatterWrapper){
		scatterWrapperData = scatterWrapper;
		$("#waveAnalysis").show();
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.scatterPlot'), true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
	}
	function divideScatterIntoWaveSeries(scatterWrapperData,waveAxis){
		var scatterObj    = prepareScatterObj(scatterWrapperData);
		
		$.each(scatterWrapperData.series, function(index, oneSeries) {
			var waveSeries1 = prepareWaveSeries("Wave 1", "red");
			var waveSeries2 = prepareWaveSeries("Wave 2", "green"); 			
			var waveSeries3 = prepareWaveSeries("Wave 3", "blue"); 		
			$.each(oneSeries.dataPoints, function(index2, oneCoordinate) {
				   var s1 =  checkCoordinatePosition(oneCoordinate,waveAxis.waveOneAxis);
				   if(s1<0){
					   waveSeries1.dataPoints.push(prepareDataPoints(oneCoordinate));
				   }else{
					   var s2 =  checkCoordinatePosition(oneCoordinate,waveAxis.waveTwoAxis);
					   if(s2<0){
						   waveSeries2.dataPoints.push(prepareDataPoints(oneCoordinate));
					   }else{
						   waveSeries3.dataPoints.push(prepareDataPoints(oneCoordinate));
					   }
				   }
			});
			scatterObj.series.push(waveSeries1);
			scatterObj.series.push(waveSeries2);
			scatterObj.series.push(waveSeries3);
		});
		
		return scatterObj;
	}
	function prepareWaveSeries(seriesName,seriesColor){
		var waveSeries = {}; 
		waveSeries.seriesName =seriesName;
		waveSeries.seriesColor = seriesColor;
		waveSeries.dataPoints = [];
		return waveSeries;
	}
	function prepareScatterObj(scatterWrapperData){
		var scatterWrapperObject    = {};
		scatterWrapperObject.series = [];
		scatterWrapperObject.graphName  = scatterWrapperData.graphName;
		scatterWrapperObject.xAxisLabel = scatterWrapperData.xAxisLabel;
		scatterWrapperObject.yAxisLabel = scatterWrapperData.yAxisLabel; 
		scatterWrapperObject.zAxisLabel =	scatterWrapperData.zAxisLabel; 
		return scatterWrapperObject;
	}
	function prepareDataPoints(oneCoordinate){
		   var dpObj = {};
		   dpObj.x =   oneCoordinate.x;
		   dpObj.y =   oneCoordinate.y;
		   dpObj.label =   oneCoordinate.label;
		   return dpObj;
	}
	function checkCoordinatePosition(coordinate,lineCoordinate){
		 return (lineCoordinate.y2 - lineCoordinate.y1) * coordinate.x + (lineCoordinate.x1 - lineCoordinate.x2) * coordinate.y + (lineCoordinate.x2 * lineCoordinate.y1 - lineCoordinate.x1 * lineCoordinate.y2);
		
	}
	function setMessage(message) {
        var messageEl = $moduleDiv.querySelector('.error_msg');
        messageEl.innerText = message;
    }
	function isWaveAxisPointEmpty(waveAxis){
		if(waveAxis.x1 === "" || waveAxis.y1 === "" || waveAxis.x2 === "" || waveAxis.y2 === "" ){
			return true;
		}
		return false;
	}
	function isWaveAxisCoordinateValid(waveAxis){
		if(isCoordinateValid(waveAxis.x1) || isCoordinateValid(waveAxis.y1) || isCoordinateValid(waveAxis.x2) || isCoordinateValid(waveAxis.y2)){
			return true;
		}
		return false;
	}
	function isCoordinateValid(coordinate){
		if(coordinate < 0 || coordinate >10){
			return true;
		}
		return false;
	} 
	function validateWaveAxis(waveAxis) {
		if(isWaveAxisPointEmpty(waveAxis.waveOneAxis) || isWaveAxisPointEmpty(waveAxis.waveTwoAxis)){
			setMessage("Please fill all Wave axis points.");
        	return false;
		}else if(isWaveAxisCoordinateValid(waveAxis.waveOneAxis) || isWaveAxisCoordinateValid(waveAxis.waveTwoAxis)){
			setMessage("Please fill the correct value.");
        	return false;
		}
        setMessage("");
        return true;
    }
	
	function getWaveAxisPoint(){
		var waveAxis = {};
		waveAxis.waveOneAxis = getFirstWaveAxisPoint();
		waveAxis.waveTwoAxis = getSecondWaveAxisPoint();;
		return waveAxis;
	}
    
	function getFirstWaveAxisPoint(){
		var waveOneAxis = {};
		waveOneAxis.x1 = $moduleDiv.querySelector('[name="w1x1"]').value;
		waveOneAxis.y1 = $moduleDiv.querySelector('[name="w1y1"]').value;
		waveOneAxis.x2 = $moduleDiv.querySelector('[name="w1x2"]').value;
		waveOneAxis.y2 = $moduleDiv.querySelector('[name="w1y2"]').value;
		return waveOneAxis;
	}
	
    function getSecondWaveAxisPoint(){
    	var waveTwoAxis = {};
		waveTwoAxis.x1 = $moduleDiv.querySelector('[name="w2x1"]').value;
		waveTwoAxis.y1 = $moduleDiv.querySelector('[name="w2y1"]').value;
		waveTwoAxis.x2 = $moduleDiv.querySelector('[name="w2x2"]').value;
		waveTwoAxis.y2 = $moduleDiv.querySelector('[name="w2y2"]').value;
		return waveTwoAxis;
	}
	
	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2' ],
		
		messages: ['filterSelected'],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = context.getElement();
			initPageBindings();
		},
		onclick:function(event,element,elementType){
			if(elementType === 'applyWaves'){			
				var waveAxis = getWaveAxisPoint();
				if(validateWaveAxis(waveAxis))
				showScatterGraph(divideScatterIntoWaveSeries(scatterWrapperData,waveAxis));
			}
		},
		
		onmessage: function(name, data) {
				 if(name === 'filterSelected' && $("#scatterGraphControl", $moduleDiv).find("select[name='parameterIds']").val() != -1 && $("#scatterGraphControl", $moduleDiv).find("select[name='questionnaireIds']").val() != -1){					 
					 $('button[type="submit"]').trigger('click');
				 }
		 }
	}
});