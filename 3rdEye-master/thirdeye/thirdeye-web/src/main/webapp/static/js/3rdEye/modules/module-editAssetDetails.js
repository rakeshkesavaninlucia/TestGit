/**
 * @author: ananta.sethi 
 * 
 * 
 */
Box.Application.addModule('module-editAssetDetails',function(context) {
	'use strict';
	var $,notifyService,$moduleDiv,commonUtils,_moduleConfig;

	function submitForm(){	
		$("#assetdetailForm",$moduleDiv).ajaxForm(function(htmlResponse){

			notifyService.setNotification("Saved", "", "success");	
		});
		$("#assetdetailForm",$moduleDiv).submit();
	}

	function getAssetDetails(dataobj){
		var url = commonUtils.getAppBaseURL("templates/getassetdetails/"+dataobj);
		var getRequest = $.get(url);

		getRequest.done(function(incomingData){
			$("#box-body1",$moduleDiv).empty();
			$("#box-body1",$moduleDiv).append(incomingData);	 
		})
	}
	function editasset(data){
		var url = commonUtils.getAppBaseURL("templates/editAssetDetails/"+data.assteid);
		var getRequest = $.get(url);

		getRequest.done(function(incomingData){
			$("#box-body1",$moduleDiv).empty();
			$("#box-body1",$moduleDiv).append(incomingData);	 
		});
	}

	return {
		messages: [ 'editasset','saveassetdetail'],

		onmessage: function(name,data) {
			if (name === 'editasset') {
				editasset(data);
			}
			if(name === 'saveassetdetail') {
				submitForm();
			}
		},
		// Initialize the page
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			$moduleDiv = context.getElement();
			commonUtils = context.getService('commonUtils');
			notifyService = context.getService('service-notify');
			_moduleConfig = context.getConfig(); 
			var dataobj= _moduleConfig.assetId;
			getAssetDetails(dataobj); 
		}
	}
});