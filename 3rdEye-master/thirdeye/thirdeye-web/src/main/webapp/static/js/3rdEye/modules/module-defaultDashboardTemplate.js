/** @author: ananta.sethi
 * module-default dashboard template
 * (top right  setting icon-click to edit dashboard,delete dashboard,add widget to dashboard)
 *
 **/
Box.Application.addModule('module-defaultDashboardTemplate', function(context) {
	'use strict';

	// private methods here
	var $, commonUtils;

	function addWidget(dashboardId){
		var params ={};
		if (dashboardId !== undefined){
			params["dashboardId"] = dashboardId;
		}
		// Fetch modal content 
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL("dashboard/getWidgetOptions"), params).then(function(content){
			var $newModal = commonUtils.appendModalToPage("addNewWidgetsModal", content, true);
			bindAddWidgetModalRequirements($newModal);
			$newModal.modal({backdrop:false,show:true});

		});

//		The form submit is simple ajax form submit. If form errors you come back else you go to the new URL 
		function bindAddWidgetModalRequirements ($newModal){
			// Bind the ajax form 
			$('#addNewWidgetsForm', $newModal).ajaxForm(function(htmlResponse) {

				if (htmlResponse.redirect){
					window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
				} else{ 					

					// if you have anything else - inject the widget
					injectWidget(htmlResponse);
					
					$newModal.modal("hide");
				}
			}); 
		}
		// Bind forms bindings
		// When form submits then you give it to  layout engine

		function injectWidget (htmlContent) {
			$(".dashboard .row").first().append(htmlContent);
			Box.Application.startAll($('.dashboard'));
		}
	}

	function deleteDashboard(id){
		if (confirm("You are about to delete this dashboard. This action can not be undone. Are you sure you wish to continue ?")){
			var postRequest = $.post( commonUtils.getAppBaseURL("dashboard/delete/" + id), {});
			postRequest.done(function(){
				window.location.replace(commonUtils.getAppBaseURL("home"));
			})
		}
	}
	
	function editDashboard(id){
		
		var params ={};
		if (id !== undefined){
			params["dashboardId"] = id;
		}
		
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL("dashboard/create"),params).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("createNewDashBoardModal", content, true);
			// Once you get the modal you append it and show it.
			bindNewDashboardModalRequirements($newModal);
			
			$newModal.modal({backdrop:false,show:true});
		});		
	}
	
	// The form submit is simple ajax form submit. If form errors you come back else you go to the new URL 
	function bindNewDashboardModalRequirements ($newModal){
		// Bind the ajax form 
		$('#createNewDashBoardForm', $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("createNewDashBoardModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewDashboardModalRequirements($replacement);
			}
		}); 
	}

	return{
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
		},
		
		onclick: function(event, element, elementType) {
			var id;
			switch (elementType) {
	            case 'addWidget':
	            	id = $(element).data("dashboardid");
	            	addWidget(id);
	                break;
	
	            case 'editDashboard':
	            	id = $(element).data("dashboardid");
	            	editDashboard(id);
	                break;
	            
	            case 'deleteDashboard':
	            	id = $(element).data("dashboardid");
	            	deleteDashboard(id);
	                break;
			}
		}
	}
});