/**
 * Module for bcm template.
 */
Box.Application.addModule('module-bcmTemplate', function(context) {

	// private methods here
	var $,d3,nv,commonUtils,jsonService,paramid,_moduleConfig, qeid;	
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		var $moduleDiv = $(context.getElement());
		//Calling the service
		spinner = spinService.getSpinner($moduleDiv,show,spinner);   	
	 }


	function drawBCM(wrapper, bcmid){

		var bcmId = "bcm-"+bcmid;
		var bcmConfig = {};
		var bcmData = {};

		bcmData.l1List = wrapper.l1List;
		bcmData.categoryList = wrapper.l2CategoryList;
		bcmData.l2List = wrapper.l2List;

		plotBCM(bcmId, bcmConfig, bcmData);

		// http://bl.ocks.org/mbostock/7555321

	}

	function plotBCM(bcmId, bcmConfig,bcmData){
		//get L0 and L1 list from bcmData
		var l1List = bcmData.l1List;
		var categoryList = bcmData.categoryList;
		var l2List = bcmData.l2List.sort(function(seq1, seq2){return seq1.sequenceNumber - seq2.sequenceNumber});
         
		var l1Box = {margin:{left:5, right:5, top:2, bottom:3},width:90,height:35};
		var categoryBox = {padding:{left:2, right:2, top:3, bottom:3}};
		var widthOfOneTower = l1Box.margin.left + l1Box.width + l1Box.margin.right;
		var L1Colors = getCategoryColor();

		var marginRight = 5;
		var marginBottom = 5;
		var categoryMinHeight = 125;
		// Make space on the left for the grouping
		var groupWidthLeft = 0.75 * widthOfOneTower;
		var groupWidthRight = 5;
		var _mapForL1Layout = {};
		var totalHeight=15;

		// Prepare the widths and sizes
		// The key is the category name and the value is an array matching the L1 (keeping the counts of boxes)
		var _categoryHeights = {};
		var _maxCountsInCategory = [];

		// Create the initial array for the usage
		$.each(categoryList, function( index, oneCategory ) {
			_categoryHeights[oneCategory] = [];
			$.each(l1List, function( i2, oneL1 ) { 
				// Initialize an array with all elements set to 0
				_categoryHeights[oneCategory].push(0);
			});
		});

		$.each(l2List, function( index, oneL1 ) {
			if (_categoryHeights[oneL1.categoryDesc]){
				var indexToIncrement = $.inArray(oneL1.l1Desc, l1List);
				_categoryHeights[oneL1.categoryDesc][indexToIncrement] = _categoryHeights[oneL1.categoryDesc][indexToIncrement] + 1;
			}
		});

		$.each(categoryList, function( index, oneCategory ) {
			_maxCountsInCategory.push(d3.max(_categoryHeights[oneCategory], function (d) { return d }));
		});

		_maxCountsInCategory.forEach(function(value){
			var v = value * ((l1Box.height +  l1Box.margin.bottom) + categoryBox.padding.top +categoryBox.padding.bottom);
			totalHeight = totalHeight + ((v <= categoryMinHeight) ? (categoryMinHeight + categoryBox.padding.top +categoryBox.padding.bottom) : v);})

			//Make an SVG Container
			var svgContainer = d3.select("#" + bcmId).append("svg")
			.attr("width",function (d,i) {return groupWidthLeft + (widthOfOneTower + marginRight) * l1List.length +groupWidthRight;})
			.attr("height", totalHeight+4).append("g").attr("class", function(d, i) {
				return ("bcmContainer");
			});
		

		// Start rendering the grouping
		var heightForTheL0Label = 40;
		var L1Categorying = svgContainer.selectAll(".L1Category")
		var heightOfOneGroup = 125;

		L1Categorying = L1Categorying.data(categoryList).enter()
		.append("g").attr("class","L1Category")
		.attr("transform", function(d, i) { return "translate(0,"+ (positionCategory(i) + heightForTheL0Label) +")" });

		L1Categorying.append("rect")
		.attr("y", 0) 
		.attr("x", 0)
		.attr("rx", 4)
		.attr("ry", 4)
		.style("fill", function(d, i) { return L1Colors[i%8];})
		.attr("height", function(d,i){ return evaluateCategoryHeight(i) }) // Height should be on max height of the l0Label text boxes
		.attr("width", (groupWidthLeft + (widthOfOneTower + marginRight) * l1List.length +groupWidthRight));// Should be the width of the whole drawing

		// Put the name of each of the Groups
		L1Categorying.append("g").attr("transform", function(d, i) { return "rotate(-90) translate("+(-1) * evaluateCategoryHeight(i)/2+","+ groupWidthLeft/3 +")" })
		.append("text")
		.attr("x", function(d,i) {return (5);}) // half the width and centre aligned
		.attr("y", 10)
		.attr("dy", ".35em")
		.attr("class","L1CategoryLabel")
		.style("text-anchor", "middle")
		.style("font-weight","bold")
		.text(function(d) {return d; });

		svgContainer.selectAll(".L1CategoryLabel").call(wrap, heightOfOneGroup);

		// Render the towers
		var towerContainer = svgContainer.append("g").attr("transform", function(d, i) { return "translate(" + groupWidthLeft + ",0)" });
		var oneTower = towerContainer.selectAll(".L0").data(l1List).enter()
		.append("g")
		.attr("class", function(d, i) { return ("L0");})
		// This should be width + margin.right
		.attr("transform", function(d, i) { return "translate(" + (i * (widthOfOneTower + marginRight)) + ",0)" });

		oneTower.append("rect")
		.attr("y", 0)
		// The width should be calculated based on the available space in the svg window
		.attr("x", 0) 
		.attr("width", widthOfOneTower)
		// The height should be calculated as max height of each of the boxes
		.attr("height", totalHeight+4).attr("rx", 5)
		.attr("ry", 5)
		.style("fill-opacity",0.5)
		.style("fill", "#3B475B");

		// Need to group the text otherwise facing issues while wrapping and centering
		oneTower.append("g").attr("transform", function(d, i) { return "translate(" + (widthOfOneTower/2 ) + ",0)" }).append("text")
		.attr("x", function(d,i) {return (widthOfOneTower/2);}) // half the width and centre aligned
		.attr("y", 10)
		.attr("dy", ".35em")
		.attr("class","L0Label")
		.style("font-weight"," bold")
		.style("text-anchor"," middle")
		.style("fill"," #ffffff")
	//.style("text-anchor", "middle")
		.text(function(d) { return d; });

		svgContainer.selectAll(".L0Label").call(wrap, widthOfOneTower);

		// Lets make the L1 
		var allL1Boxes = svgContainer.selectAll(".L1")
		.data(l2List)
		.enter()
		.append("g").attr("class","L1")
		.attr("transform", function(d, i) { return "translate("+ positionX(d) +","+ positionY(d) +")" })
        .attr("data-type","bcmLevelArea")
		.attr("data-bcmlevelid",function(d){return d.l2Id;})
		
		allL1Boxes.append("rect")
		.attr("y", 0)
		// The width should be calculated based on the available space in the svg window
		.attr("x", 0)
		.attr("width", l1Box.width)
		// The height should be calculated as max height of each of the boxes
		.attr("height", function(d) { return (d.length <= 45) ? l1Box.height : (l1Box.height)} )
		.attr("rx", 3)
		.attr("ry", 3)		
		.style("cursor","pointer" )
		.style("fill", function(d) {return d.hexColor; });

		// Need to group the text otherwise facing issues while wrapping and centering
		allL1Boxes.append("g").attr("transform", function(d, i) { return "translate(" + (l1Box.width/2 ) + ",0)" })
		.append("text")
		.attr("x", function(d,i) {return (l1Box.width/2);}) // half the width and centre aligned
		.attr("y", 10)
		.attr("dy", ".35em")
		.attr("class","L1Label")
		.style("text-anchor", "middle")
		.style("font-size", "0.7em")
		.style("cursor","pointer" )
		.text(function(d) { return d.value; });

		svgContainer.selectAll(".L1Label").call(wrap, l1Box.width);		
		
		function evaluateCategoryHeight (index){
			var numberOfBoxes = _maxCountsInCategory[index];
			var categoryHeight = numberOfBoxes * (l1Box.height +  l1Box.margin.bottom) + categoryBox.padding.top +categoryBox.padding.bottom;
			if ( categoryHeight <= categoryMinHeight){
				return categoryMinHeight
			}
			return categoryHeight;
		}

		function positionCategory (index){
			var yIndex = 0;
			for (i = 0; i < index; i++){
				yIndex = evaluateCategoryHeight(i) + marginBottom + yIndex;
			}
			return yIndex;
		}

		// Position based on which vertical
		function positionX(dataPoint){
			return (l1List.indexOf(dataPoint.l1Desc) * (widthOfOneTower + marginRight)) + groupWidthLeft + l1Box.margin.left;
		}

		function positionY(dataPoint){
			// This should be in a central map
			var categoryStart = positionCategory(categoryList.indexOf(dataPoint.categoryDesc)) + heightForTheL0Label;
			var boxes = getBoxNumForL1CategoryCombo(dataPoint);
			return categoryStart + categoryBox.padding.top + boxes * (l1Box.height + l1Box.margin.bottom) ;
		}

		function getBoxNumForL1CategoryCombo(dataPoint){
			var objKey = dataPoint.categoryDesc + ":" + dataPoint.l1Desc;
			if (_mapForL1Layout.hasOwnProperty(objKey)){
				_mapForL1Layout[objKey] = _mapForL1Layout[objKey] + 1;
			} else {
				_mapForL1Layout[objKey] = 0;
			}
			return _mapForL1Layout[objKey];
		}
		
		function getCategoryColor(){
			var colorBucket1 = ["#e4c9d8","#c993b1","#af5e8b","#942864"];
			var colorBucket2 = ["#f9ddd0","#f3bba1","#ed9a73","#e77844"];
			var colorBucket3 = [];
			var catLenght = categoryList.length > 8 ? 8 : categoryList.length;
			for(var i=0; i<Math.ceil(catLenght/2); i++){
				colorBucket3.push(colorBucket1[i]);
			}
			for(var j=0; j<(catLenght - Math.ceil(catLenght/2)); j++){
				colorBucket3.push(colorBucket2[j]);
			}
			return colorBucket3;
		}
	}

	function wrap(text, width) {
		text.each(function() {
			var text = d3.select(this),
			words = text.text().split(/\s+/).reverse(),
			word,
			line = [],
			lineNumber = 0,
			lineHeight = 1.1, // ems
			y = text.attr("y"),
			dy = parseFloat(text.attr("dy")),
			tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
				if (tspan.node().getComputedTextLength() > width) {
					line.pop();
					tspan.text(line.join(" "));
					line = [word];
					tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
				}
			}
		});
	}

//	function showLoading(toShow){
//		if (toShow){
//			$(".bcmcontainer .overlay").fadeIn("slow");
//		} else {
//			$(".bcmcontainer .overlay").fadeOut("slow");
//		}
//	}

	return {
		behaviors : [ 'behavior-select2','behavior-downloadD3' ], 
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			d3 = context.getGlobal('d3');	
			 _moduleConfig = context.getConfig();
			jsonService = context.getService('jsonService');
			commonUtils = context.getService('commonUtils');
			var _this = this;				
			_this._loadBcm();	         
			showLoading(true);
		},  

		_loadBcm : function(){			
			var _this = this;
			// Allow each bcm to load asynchronously.
			$( ".bcmplacement" ).each(function() {
				$( this ).empty();
				var bcmId = $( this ).data( "bcmid" );
//			    showLoading(true);
				// Lets load the actual bcm content
				var loadBcmPromise = _this._loadBcmContent($( this ), bcmId)
				loadBcmPromise.done(function($bcmContainer){
					//TODO : 
				});
				loadBcmPromise.fail();
			});				
		},

		_loadBcmContent : function($replacementContainer, bcmid){
			var _this = this;
			var deferredContentLoading =  $.Deferred();
			// The URL from which you load the content
			var dataObj={};
			dataObj.paramid = paramid;
			dataObj.qeid = qeid
			var bcmURL = commonUtils.getAppBaseURL("bcm/"+bcmid+"/view");
			var fetchContentPromise  = jsonService.getJsonResponse(bcmURL,dataObj); 
			//var fetchContentPromise = _this._fetchBcmJSONContent(bcmURL);
			fetchContentPromise.done (function(JSONContent){
				// With the JSON content
				// draw the BCM
				drawBCM(JSONContent, bcmid);
				showLoading(false);

				// Pass the reference for the new bcm
				deferredContentLoading.resolve($replacementContainer);
			});
			fetchContentPromise.fail (function(){deferredContentLoading.reject($replacementContainer);});

			// Place in the target div
			return deferredContentLoading.promise();
		},

		_fetchBcmJSONContent : function (urlToLoadFrom){
			var deferredContentFetch =  $.Deferred();
			$.get( urlToLoadFrom, function( newJSONContent ) {
				deferredContentFetch.resolve(newJSONContent);
			});
			return deferredContentFetch.promise();
		},	
		
		onchange: function(event, element, elementType) {
        	if (elementType === 'parameterOnBcmTempalate') { 
        		paramid = $(element).val();
        		qeid = $('option:selected', $(element)).attr('data-qeid');
        		showLoading(true);
        		this._loadBcm();	
        	}
        },
		
        onclick: function(event, element, elementType) {
			var url;
        	if (elementType === 'bcmLevelArea') {  
        		 if(paramid == undefined || paramid == ""){
        			 alert("Please select parameter");
   				     return false; 
        		 }        		
        		var bcmlevelId = $(element).data("bcmlevelid");
        		if(_moduleConfig.mode === 0){
        			url = _moduleConfig.asseturl +"/"+paramid +"/qe/"+qeid + "/level/" +bcmlevelId;
        		}else if(_moduleConfig.mode === 1){      			
        				  url = _moduleConfig.frurl +"/mode/single/"+paramid +"/qe/"+qeid + "/level/" +bcmlevelId;        			 
        	    }
        		 window.location.href = commonUtils.getAppBaseURL(url); 
        	}
        }
	};
});