/**
 * @author: ananta.sethi
 *  Behavior for loading content
 */
Box.Application.addBehavior('behavior-lazyLoaded', function(context) {
	'use strict';
	var $,commonUtils,moduleEl;
	var urlData;
	
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show, $replacementContainer){
		var spinService = context.getService('service-spinner');
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner($replacementContainer, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	function loadContent($replacementContainer, asyncURL) {
		
		var deferredContentLoading = $.Deferred();
		
		// The URL from which you load the content
		var asyncFullURL = commonUtils.getAppBaseURL(asyncURL);
		var fetchContentPromise = fetchHTMLContent(asyncFullURL);

		fetchContentPromise.done(function(newHTMLContent) {
			// With the new HTML content
			// You can validate response and then insert into the target
			$replacementContainer = $(newHTMLContent).replaceAll($replacementContainer);
			deferredContentLoading.resolve($replacementContainer);
		});
		fetchContentPromise.fail(function() {
			deferredContentLoading.reject($replacementContainer);
		});

		// Place in the target div
		return deferredContentLoading.promise();
	}

	function fetchHTMLContent(urlToLoadFrom) {
		
		var deferredContentFetch = $.Deferred();
		$.get(urlToLoadFrom, function(newHTMLContent) {
			deferredContentFetch.resolve(newHTMLContent);
		});
		return deferredContentFetch.promise();
	}
	
	function lazyLoad() {
		moduleEl = context.getElement();
		$("*[data-loadurl]", $(context.getElement())).each(function() {
			var asyncURL = $(this).data("loadurl");
			if(urlData !== undefined) {
				asyncURL = asyncURL + '?' + urlData;
			}
			 showLoading(true, $(this));		
			
			var loadPromise = loadContent($(this), asyncURL)

			loadPromise.done(function($container) {
				Box.Application.startAll(moduleEl);
			});
			loadPromise.fail();
		});
	}

	return {

		messages: ['lazyLoad'],
		
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			lazyLoad();

		},
		
		onmessage: function(name, data) {
			if(name === 'lazyLoad') {				
				urlData = data;
				lazyLoad();
			}
		}
	}
});