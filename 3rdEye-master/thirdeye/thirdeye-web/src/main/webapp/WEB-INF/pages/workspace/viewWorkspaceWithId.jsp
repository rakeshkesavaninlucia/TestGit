<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.workspacemanagement.workspaceuser.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!-- Latest compiled and minified CSS -->
<link th:href="@{/static/css/ext/bootstrap-3.3.6.min.css}" rel="stylesheet" type="text/css" />
<!-- Latest compiled and minified JavaScript -->
<script	th:src="@{/static/js/ext/jquery/2.1.4/jquery.min.js}"></script>
<script th:src="@{/static/js/ext/bootstrap/3.3.6/bootstrap.min.js}"></script>
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="${viewWorkspace.workspaceName}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="${viewWorkspace.workspaceDescription}"></span></div>
<div class="container"  th:fragment="contentContainer">
<div class="box box-primary">
          <div class="box-body" data-module="module-editworkSpace">
 			<div class="table-responsive"  th:if="${#lists.size(userListInWorkspace) gt 0}" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
				<table class="table table-bordered table-condensed table-hover" id="userWorkspaceView">
					<thead>
						<tr>
							<th><span th:text="#{workspace.fname}"></span></th>
							<th><span th:text="#{workspace.lname}"></span></th>
							<th><span th:text="#{workspace.email}"></span></th>
							<th><span th:text="#{workspace.action}"></span></th>
						</tr>
					</thead>
					<tbody th:each="oneUser : ${userListInWorkspace}">
						<tr th:replace="workspace/editWorkspaceFragments :: detailofusersInWorkspace(oneRow = ${oneUser}, workspaceId = ${viewWorkspace.id})"></tr>					
					</tbody>
				</table>
			</div>
			<div class="row">
				 <div id="addUserInWorkspace" data-module="addUser-module">
	  					<form th:action="@{/workspace/user/save}" method="post" data-type="userAddForm">
							<input type="hidden" name="workspaceId" th:value="${workspaceId}"/>
							<br></br>
					        <div class="form-group">
						        <span class="col-md-6">
						        	<select name="userId" class="select2 autocomplete form-control" th:attr="data-url='autocomplete/usersnotinworkspace/'+${workspaceId}">
										<option value="">Select a user to add to workspace</option>
									</select>
				                </span>
				                <span class="col-md-3">
					               <input type="submit" value="Add User" class="btn btn-primary"/>
				                </span>
			                </div>
					   </form>
				 </div>
			</div>
		</div>
</div>			
	</div>
	
	
  <div th:fragment="scriptsContainer" >
  	<script th:src="@{/static/js/3rdEye/modules/addUser-module.js}"></script>
  	<script	th:src="@{/static/js/3rdEye/modules/module-editworkSpace.js}"></script>
	<script type="text/javascript">
	    
  $('#addUserInWorkspace').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient) 
})


	</script>
</div>

</body>
</html>