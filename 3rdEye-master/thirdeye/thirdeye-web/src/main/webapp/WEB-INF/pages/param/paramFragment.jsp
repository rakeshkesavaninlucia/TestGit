<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="questionsModal">
		<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
					<form id="questionSelectorId">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">Select Questions</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
				          	<b><span th:text="#{question.select.help}"></span></b>
							<table class="table table-bordered table-striped table-condensed table-hover dataTable" id="questionListInModal">
								<thead>
									<tr>
										<th></th>
										<th><span th:text="#{question.category}"></span></th>
										<th><span th:text="#{question.displayName}"></span></th>
										<th><span th:text="#{question.title.table}"></span></th>
										<th><span th:text="#{question.mandatory}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRow : ${listOfQuestions}">
										<td><input type="checkbox" name="question" class="questionClass" th:value="${oneRow.id}"/></td>
										<td><span th:if="${oneRow.category ne null}" th:text="${oneRow.category.name}">Test</span></td>
										<td><span th:text="${oneRow.displayName}">Test</span></td>
										<td><span th:text="${oneRow.title}">Test</span></td>
										<td><input type="checkbox" name="mandatoryQuestion" class="mandatoryQuestionClass" th:value="${oneRow.id}"/></td>
									</tr>
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" data-type="saveQuestionsTemporary" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
	
	<div th:fragment="parametersModal">
		<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
					<form id="parameterSelectorId">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">Select Parameters</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
				          	<b><span th:text="#{parameter.select.help}"></span></b>
							<table class="table table-bordered table-condensed table-hover paramListTable" id="parameterListInModal">
								<thead>
									<tr>
										<th></th>
										<th><span th:text="#{parameter.theader.parent}"></span></th>
										<th><span th:text="#{parameter.theader.uniquename}"></span></th>
										<th><span th:text="#{parameter.theader.description}"></span></th>
										<th><span th:text="#{parameter.theader.displayname}"></span></th>
									</tr>
								</thead>
								<tbody >
									<tr th:each="oneRow : ${listOfParameter}" th:id="${oneRow.id}">
										<td><input type="checkbox" name="parameter" class="parameterClass" th:value="${oneRow.id}"/></td>
										<td><span th:if="${oneRow.parameter ne null}" th:text="${oneRow.parameter.uniqueName}">Test</span></td>
										<td><span th:text="${oneRow.uniqueName}">Test</span></td>
										<td><span th:text="${oneRow.description}">Test</span></td>
										<td><span th:text="${oneRow.displayName}">Test</span></td>
									</tr>			
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" data-type="saveParametersTemporary" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
	
	<div th:fragment="functionalMapModal">
		<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
					<form id="functionalMapSelectorId" th:object="${fcParamConfigBean}">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">Select Functional Map</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
				          	<b><span th:text="#{functionalMap.select.help}"></span></b>
							<table class="table table-bordered table-condensed table-hover" id="functionalMapListInModal">
								<thead>
									<tr>
									    <th></th>
										<th><span th:text="#{functionalMap.name}"></span></th>
										<th><span th:text="#{functionalMap.bcm}"></span></th>
										<th><span th:text="#{functionalMap.asset.template}"></span></th>
										<th><span th:text="#{functionalMap.type}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneFm : ${listOfFunctionalMap}" th:id="${oneFm.id}">
										<td><input type="radio" th:field="*{functionalMapId}" class="functionalMapClass" th:value="${oneFm.id}"/></td>
										<td><span th:text="${oneFm.name}">Test</span></td>
										<td><span th:text="${oneFm.bcm.bcmName}">Test</span></td>
										<td><span th:text="${oneFm.assetTemplate.assetTemplateName}">Test</span></td>
										<td><span th:text="${oneFm.type}">Test</span></td>
									</tr>
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" data-type="saveFunctionalMapTemporary" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
	
	<table th:fragment="selectedQuestion" class="selectedChildEntities table table-bordered table-condensed table-hover" id="selectedQuestionTableId">
		<thead>
			<th>SNo.</th>
			<th>Question</th>
			<th>Weight</th>
		</thead>
		<tbody>
			<tr th:each="oneQue,status : ${selectedQuestions}" th:id="${oneQue.id}">
					<td><span th:text="|${status.count}|"></span></td>
					<td><span th:text="${oneQue.title}"></span></td>
					<td><input type="number" step="0.1" th:name="${oneQue.id}" style="width: 50px;" max="1" min="0" required="required" th:value="${mapOfWeight.get(oneQue.id)}"/></td>
			</tr>
		</tbody>
	</table>
	
	<table th:fragment="selectedParameter" class="selectedChildEntities table table-bordered table-condensed table-hover" id="selectedParameterTableId">
		<thead>
			<th>SNo.</th>
			<th>Parameter</th>
			<th>Weight</th>
		</thead>
		<tbody>
			<tr th:each="oneParam,status : ${selectedParameters}" th:id="${oneParam.id}">
					<td><span th:text="|${status.count}|"></span></td>
					<td><span th:text="${oneParam.uniqueName}"></span></td>
					<td><input type="number" step="0.1" th:name="${oneParam.id}" style="width: 50px;" max="1" min="0" required="required" th:value="${mapOfWeight.get(oneParam.id)}"/></td>
			</tr>
		</tbody>
	</table>
	
	<table th:fragment="selectedFunctionalMap" class="selectedChildEntities table table-bordered table-condensed table-hover" id="selectedFunctionalMapTableId">
		<thead>
			<th>Functional Map</th>
			<th>Level Type</th>
		</thead>
		<tbody>
			<tr>
				<td><span th:text="${selectedFunctionalMap.name}"></span></td>
				<td>
					<select th:if="${selectedFunctionalMap.functionalMapLevelType eq null}" name="functionalMapLevelType" class="form-control">
						<option th:each="oneType : ${mapOfFunctionalMapType}" th:value="${oneType.value}" th:text="${oneType.value}"></option>
					</select>
					<span th:if="${selectedFunctionalMap.functionalMapLevelType ne null}" th:text="${selectedFunctionalMap.functionalMapLevelType}"></span>
				</td>
			</tr>
		</tbody>
	</table>
	
	<a th:fragment="addFunctionalMapButton" class="addFunctionalMap btn btn-primary" data-type="buttonClick">Add Functional Map</a>
	<a th:fragment="addParameterButton" class="addParameter btn btn-primary" data-type="buttonClick">Add Parameter</a>
	<a th:fragment="addQuestionButton" class="addQuestion btn btn-primary" data-type="buttonClick">Add Question</a>
</body>
</html>