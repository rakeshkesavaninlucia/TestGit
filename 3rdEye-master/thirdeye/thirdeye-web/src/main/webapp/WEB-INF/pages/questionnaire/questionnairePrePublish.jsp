<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='questionnaireprepublish',dataAction='',pageTitle=${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.pre.publish}"></span>
	    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.pre.publish}"></span>
	</div>
	<div th:fragment="pageSubTitle">
	    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.publish.help}"></span>
	    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.publish.help}"></span>
	</div>
	
	<div class="container"  th:fragment="contentContainer">
	
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
		                 <h3 class="box-title">		                
		                 <span th:text="#{publish.help}"></span>
		                 </h3>
		            </div>
	                	
	               	<form role="form" th:action="@{/{action}/{questionnaireId}/publish(questionnaireId=${questionnaire.id},action=${action})}" method="post" id="publishForm">
		                  <div class="box-footer">
		                    <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{questionnaireId}/orderquestions(questionnaireId=${questionnaireId},action=${action})}" class="btn btn-default" th:text="#{button.previous}"></a>
		                    <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{questionnaireId}/coststructures(questionnaireId=${questionnaireId},action=${action})}" class="btn btn-default" th:text="#{button.previous}"></a>
		                    <button type="submit" class="btn btn-primary">Publish Now</button>
		                  </div>
	               </form>
		        </div>   
			</div>
		</div>
		<div class="row" >
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			    <div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}"><div th:replace="questionnaire/questionnaireResponse :: displayQuestionnaire(questionnaire=${questionnaire},listOfQuestions = ${listOfQuestions})"></div></div>
			    <div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}"><div th:replace="tco/tcoResponse :: contentContainer"></div></div>
			</div>
		</div>
	</div>
</body>
</html>