<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = 'Change Password')">
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{user.changePassword}"></span>
		</div>
	</div>	
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
					<div class="box-body">
						<div>
						<form th:action="@{/user/change}"  method="post" th:object="${ChangePass}" >
						<div class="form-group">
						<span>Current Password :<span style="color: red;">*</span></span> <input type="password" th:field="*{currentPassword}" class="form-control" />
						<span th:if="${#fields.hasErrors('currentPassword')}" th:errors="*{currentPassword}" style="color: red;">Test</span> 
						<br/>
						<span>New Password :<span style="color: red;">*</span></span> <input type="password" th:field="*{newPassword}" class="form-control" />
						<span th:if="${#fields.hasErrors('newPassword')}" th:errors="*{newPassword}" style="color: red;">Test</span> 
						<br/>
						<span>Confirm Password :<span style="color: red;">*</span></span> <input type="password" th:field="*{confirmPassword}"  class="form-control" />
						<span th:if="${#fields.hasErrors('confirmPassword')}" th:errors="*{confirmPassword}" style="color: red;">Test</span> 
						<br/>
						<div><input class="btn btn-primary" type="submit" value="Change Password"/></div>
						</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>