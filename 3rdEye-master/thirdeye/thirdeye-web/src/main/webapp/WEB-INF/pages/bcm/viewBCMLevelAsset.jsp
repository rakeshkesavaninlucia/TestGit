<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.functionalbcm.view.nav})">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Insert title here</title>

</head>
<body>

	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{bcm.show.assets}"></span><span th:text="${bcmLevel.bcmLevelName}"></span>
		</div>
	</div>	
	<div th:fragment="contentContainer">
		<div data-module="module-viewBCMLevelAsset">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-12 form-group">
					<select class="form-control select2" data-type="qualityGates">
						<option value="">--Select Quality Gate--</option>
						<option th:each="oneQualityGate : ${mapOfQualityGates}"
							th:value="${oneQualityGate.key}" th:text="${oneQualityGate.value}" />
					</select>
				</div>
			</div>
			<div class="row panel panel-default bcmlevelcontainer">
				<div class="box box-primary">
					<div class="box-body col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="bcmLevelName l2"><span th:text="${bcmLevel.bcmLevelName}"></span></div>
					</div>					
					<div class="col-md-9 bcmlevelplace" th:attr="data-bcmlevelid=${bcmLevel.id},data-fcparameterid=${fcParameterId},data-qeid=${qeid}" >
					</div>
					<!-- <div class="overlay" >
							<i class="fa fa-refresh fa-spin"></i>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="row" th:fragment="bcmLevelAssets">						  
	  <div id="assetClick"  class="col-lg-2 col-md-4 col-sm-4 col-xs-6" style="padding-top:12px;padding-bottom:12px;" th:each="oneAsset : ${listOfBcmLevelAsset}">
	     <div id="assetId" data-type="assetName" class="bcmLevelAssets clickable" th:style="'background:'+${oneAsset.hexColor}" th:value="${oneAsset.id}"><span th:text="${oneAsset.shortName} "></span>
	     <a th:href="@{/templates/asset/view/{id}(id=${oneAsset.id})}"></a></div>
	  </div>
	</div>
	<div th:fragment="scriptsContainer" th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-viewBCMLevelAsset.js}"></script>
	</div>
</body>
</html>