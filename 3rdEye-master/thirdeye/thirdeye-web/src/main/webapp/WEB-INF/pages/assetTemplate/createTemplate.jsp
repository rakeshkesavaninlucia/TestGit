<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.createtemplate.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<div th:switch="${assetTemplateBean.id}">
			<span th:case="null" th:text="#{pages.templatemanagement.createtemplate.title}"></span> <span th:case="*" th:text="#{pages.templatemanagement.updatetemplate.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.templatemanagement.definetemplate.subtitle}"></span>
	</div>

	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body" data-module="module-createAssetTemplate">
						<div
							th:replace="assetTemplate/editTemplateFragments :: createTemplateForm"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
  		<script th:src="@{/static/js/3rdEye/modules/module-createAssetTemplate.js}"></script>
	</div>
</body>
</html>