<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tcomanagement.editcoststructure.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pages.tcomanagement.editcoststructure.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{tco.coststructure.helptext}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="containerParameterTreeView box box-primary" id="container" data-module="module-editCostStructure">
			  <ul>
			    <li th:classappend="${T(org.birlasoft.thirdeye.constant.ParameterType).TCOA.name().equalsIgnoreCase(parameterBean.paramType)} ? displayAddChildMenuItemClass : ''" th:id="${parameterBean.id}">
			    <span th:text="${parameterBean.displayName}"></span>
			      <ul th:fragment="oneParameterWithWrapper(listOfParameters)" th:if="${not #lists.isEmpty(listOfParameters)}">
			        <li th:if="${T(org.birlasoft.thirdeye.constant.ParameterType).TCOL.name().equalsIgnoreCase(oneParameter.paramType)}" class="displayCostElementMenuItemClass" th:each="oneParameter : ${#sets.toSet(listOfParameters)}" th:id="${oneParameter.id}">
			        	<span th:text="${oneParameter.displayName}"></span>
			        	<!-- Process children -->
						<div th:replace="tco/editCostStructure :: oneParameterWithWrapper(listOfParameters=${oneParameter.childParameters})" th:if="${not #lists.isEmpty(oneParameter.childParameters)}"></div>
						<div th:replace="tco/editCostStructure :: oneQuestionWithWrapper(listOfQuestions=${oneParameter.childQuestions})" th:if="${not #lists.isEmpty(oneParameter.childQuestions)}"></div>
					</li>
					<li th:if="${T(org.birlasoft.thirdeye.constant.ParameterType).TCOA.name().equalsIgnoreCase(oneParameter.paramType) and not #lists.isEmpty(oneParameter.childParameters)}" class="displayAddChildMenuItemClass" th:each="oneParameter : ${#sets.toSet(listOfParameters)}" th:id="${oneParameter.id}">
			        	<span th:text="${oneParameter.displayName}"></span>
			        	<!-- Process children -->
						<div th:replace="tco/editCostStructure :: oneParameterWithWrapper(listOfParameters=${oneParameter.childParameters})" th:if="${not #lists.isEmpty(oneParameter.childParameters)}"></div>
						<div th:replace="tco/editCostStructure :: oneQuestionWithWrapper(listOfQuestions=${oneParameter.childQuestions})" th:if="${not #lists.isEmpty(oneParameter.childQuestions)}"></div>
					</li>
					<li th:if="${T(org.birlasoft.thirdeye.constant.ParameterType).TCOA.name().equalsIgnoreCase(oneParameter.paramType) and #lists.isEmpty(oneParameter.childParameters)}" th:each="oneParameter : ${#sets.toSet(listOfParameters)}" th:id="${oneParameter.id}">
			        	<span th:text="${oneParameter.displayName}"></span>
			        	<!-- Process children -->
						<div th:replace="tco/editCostStructure :: oneParameterWithWrapper(listOfParameters=${oneParameter.childParameters})" th:if="${not #lists.isEmpty(oneParameter.childParameters)}"></div>
						<div th:replace="tco/editCostStructure :: oneQuestionWithWrapper(listOfQuestions=${oneParameter.childQuestions})" th:if="${not #lists.isEmpty(oneParameter.childQuestions)}"></div>
					</li>
			      </ul>
			      <!-- The fragment for displaying the questions -->
				  <ul th:fragment="oneQuestionWithWrapper(listOfQuestions)" th:if="${not #lists.isEmpty(listOfQuestions)}">
					<li th:each="oneQuestion : ${#sets.toSet(listOfQuestions)}" th:title="${oneQuestion.title}"  data-toggle="popover" data-trigger="hover" data-container="body" data-placement="top" th:id="${oneQuestion.id}" class="notDisplayMenuItemClass">
						<span th:text="${oneQuestion.title}"></span>
					</li>
				</ul>
			    </li>
			  </ul>
			</div>
		</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/ext/thirdEye/module-editCostStructure.js}"></script>
	</div>
</body>
</html>