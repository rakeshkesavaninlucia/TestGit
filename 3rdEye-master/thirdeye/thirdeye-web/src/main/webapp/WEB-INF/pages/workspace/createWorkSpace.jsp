<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.workspacemanagement.createworkspace.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pages.workspacemanagement.createworkspace.title}"></span></div>
	<div class="container"  th:fragment="contentContainer">
		<div th:replace="workspace/editWorkspaceFragments :: createWorkSpaceForm"></div>		
	</div>
</body>
</html>