<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.tco.analysis.nav})">
<head>
<meta charset="utf-8" />
<title>TCO Analysis</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<style th:fragment="onPageStyles">

.axis text {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
  fill-opacity: .9;
}

.x1.axis path {
  display: none;
}

.x1.axis text {
  text-anchor: end !important;
}

.nv-x.nv-axis text {
  text-anchor: end !important;
}

</style>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.tco.analysis.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.tco.analysis.subtitle}"></span>
	</div>
	<div th:fragment="contentContainer">
				
		<div data-module="module-tcoAnalysisGraphs">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body analysisGraph">
							<form role="form" th:action="@{/graph/tco/plot}" method="post" id="tcoAnalysisGraph">
								<table class="table table-bordered table-condensed table-hover row">
									<tr>
										<td class="col-md-2 horizontal"><label>Select Graph</label></td>
										<td colspan="3"><select class="form-control select2" data-type="graphType" name="graphType">
											<option th:each="oneMode : ${T(org.birlasoft.thirdeye.constant.TcoGraphTypes).values()}" th:value="${oneMode}" th:text="${oneMode.value}"></option>
										</select></td>
									</tr>
									<tr>
										<td class="col-md-2 horizontal"><label>Chart of Account</label></td>
										<td colspan="3"><select class="form-control select2" name="chartOfAccount">
											<option value="-1">--Select--</option>
											<option th:each="oneChartOfAccount : ${listOfChartOfAccount}" th:value="${oneChartOfAccount.id}" th:text="${oneChartOfAccount.name}" />
										</select></td>
									</tr>
									<tr id="getFragment">
										<td class="col-md-2 horizontal"><label>Questionnaire</label></td>
										<td class="col-md-4"><select class="form-control ajaxSelector select2" th:attr="data-dependency=qe ,data-url='graph/scatter/fetchRootParameters/'" name="questionnaireId">
											<option value="-1">--Select--</option>
											<option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
										</select></td>
										<td class="col-md-2 horizontal"><label>Parameter</label></td>
										<td class="col-md-4"><select class="form-control ajaxPopulated select2" th:attr="data-dependson=qe" name="parameterId">
											<option value="-1">--Select--</option>
								    		<option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
										</select></td>
									</tr>
								</table>
								<div class="box-footer">
									<div class="pull-right">
										<button type="submit" class="btn btn-primary">Plot</button>
									</div>
								</div>
							</form>
							<!-- <div id="getFragment"></div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary scatterPlot" >
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-type="download" title = "Download">
									<i class="glyphicon glyphicon-download clickable"></i>
								</button>
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body chart-responsive">
							<svg></svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-tcoAnalysisGraphs.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/saveSvgAsPng.min.js}"></script>
	</div>

</body>
</html>