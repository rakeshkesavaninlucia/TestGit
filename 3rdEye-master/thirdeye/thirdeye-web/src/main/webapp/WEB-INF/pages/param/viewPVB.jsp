<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pvb.page.title})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pvb.page.title}"></span></div>
		
	<div class="container"  th:fragment="contentContainer">
	<div data-module="module-pvbAsset">
		<div class="row">
			<div class="col-sm-12">	
				<div class="box box-primary">					
					 <table class="table table-bordered table-condensed table-hover">					
						<tr>
						<td class="col-md-1 horizontal"><label for="assetId" class="control-label" th:text="#{asset}"></label></td>
							<td colspan="3">
							<select class="select2" data-type="assetChanged" th:attr="data-qeid=${questionnaireId}">
								<option value="-1">--Select an Asset--</option>							
								<option th:each="oneAssetBean : ${listOfAssetBean}" th:value="${oneAssetBean.id}" th:text="${oneAssetBean.shortName}"  th:selected="${oneAssetBean.id==assetId}"/>
							</select></td>
						</tr>
					</table>				
				</div>	
			</div>	
		</div>
	</div>
		<div data-module="module-pvb">
			<div th:fragment="parameterFragment">
				<div class="row" th:if="${not #lists.isEmpty(listOfParameterValueBoostBean)}" id="pvbParameters">
			       	<div class="col-sm-12">
			        	<div class="box box-primary">
			                <div class="box-body">	              
								<div class="table-responsive" data-module="common-data-table">
									<table class="table table-bordered table-condensed table-hover">
										<thead>
											<tr>
												<th><span th:text="'Parameter Name'"></span></th>
												<th><span th:text="'Evaluated Value'"></span></th>
												<th><span th:text="'Boost Percentage'"></span></th>	
												<th><span th:text="'Actions'"></span></th>										
											</tr>
										</thead>
										<tbody>
											<tr th:each="onePVB : ${listOfParameterValueBoostBean}" th:id="${onePVB.parameterId}">									
												<td><span th:text="${onePVB.parameterName}"></span></td>
												<td><span th:text="${onePVB.evaluatedValue}"></span></td>
												<td><span th:text="${onePVB.boostPercentage}"></span></td>	
												<td><a class="fa fa-pencil-square-o clickable" th:title="#{icon.title.edit}" th:href="@{/parameter/boost/{qeid}/edit/{aid}/parameter/{pid}(qeid=${onePVB.qeId},aid=${onePVB.assetId},pid=${onePVB.parameterId})}"></a></td>																											
											</tr>	
										</tbody>
									</table>
								</div>		
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	
	
	
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script th:src="@{/static/js/3rdEye/modules/pvb/module-pvb.js}"></script> 
       <script th:src="@{/static/js/3rdEye/modules/pvb/module-pvbAsset.js}"></script>     
    </div>
</body>
</html>