<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventory.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="#{pages.templatemanagement.inventory.viewtemplatedata.title}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="${assetTemplate.assetTemplateName}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="box box-primary">
			<div class="table-responsive" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
				<div class="box-body" style="overflow: auto;">
					<table class="table table-bordered " id="assetList">
						<thead>
							<tr>
								<th th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplate.assetType.assetTypeName)}">Parent Asset</th>
								<th th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplate.assetType.assetTypeName)}">Child Asset</th>
								<th th:each="templateCol : ${templateData}"><span th:text="${templateCol.assetTemplateColName}"></span></th>
							</tr>
						</thead>
						<tbody>
							<tr th:each="asset : ${assets}">
								<td th:if="${not #maps.isEmpty(mapOfRelationshipAssetDataBean) and #maps.containsKey(mapOfRelationshipAssetDataBean, asset.id)}"><a th:href="@{/templates/asset/view/{id}(id=${mapOfRelationshipAssetDataBean.get(asset.id).parentAsset.id})}" th:text="${mapOfRelationshipAssetDataBean.get(asset.id).parentAsset.shortName}"></a></td>
								<td th:if="${not #maps.isEmpty(mapOfRelationshipAssetDataBean) and #maps.containsKey(mapOfRelationshipAssetDataBean, asset.id)}"><a th:href="@{/templates/asset/view/{id}(id=${mapOfRelationshipAssetDataBean.get(asset.id).childAsset.id})}" th:text="${mapOfRelationshipAssetDataBean.get(asset.id).childAsset.shortName}"></a></td>
								<td th:each="assetData : ${asset.assetDatas}">
									<a th:if="${not T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplate.assetType.assetTypeName) and assetData.sequenceNumber eq 1}" th:href="@{/templates/asset/view/{id}(id=${asset.id})}">
										<span th:text="${assetData.data}"></span>
									</a>
									<span th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplate.assetType.assetTypeName) and assetData.sequenceNumber eq 1}" th:text="${assetData.data}"></span>
									<span th:if="${assetData.sequenceNumber ne 1}" th:text="${assetData.data}"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box-footer clearfix">
				<div>
					<a class="btn btn-primary" th:href="@{/templates/populateTemplateForm/{id}(id=${assetTemplate.id})}">Add Asset</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>