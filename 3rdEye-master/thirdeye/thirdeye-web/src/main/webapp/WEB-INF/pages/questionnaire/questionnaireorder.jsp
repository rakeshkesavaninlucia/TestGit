<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='questionnaireorder',dataAction='',pageTitle=#{pages.questionnaire.response.order.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{page.title.question.order}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="col-md-12">
			<div class="box">
					<div class="table-responsive">
						<div class="box-body">
								<table class="table table-bordered table-condensed table-hover" id="templateColumnsofview">
									<thead>
										<tr>
											<th><span th:text="#{questionnairequestion.category.name}"></span></th>
											<th><span th:text="#{questionnairequestion.asset.name }"></span></th>
											<th><span th:text="#{questionnairequestion.question.name}"></span></th>
										</tr>
									</thead>
									<tbody>
										<tr th:each="oneRow : ${listOfRows}">
											<td th:text="${oneRow.question.category}"></td>
											<td th:text="${oneRow.asset.shortName}"></td>
											<td th:text="${oneRow.question.title }"></td>
										</tr>
									</tbody>
								</table>
					   </div>
					   <div class="box-footer">
							<a th:href="@{/questionnaire/{questionnaireId}/parameters(questionnaireId=${questionnaireId})}" class="btn btn-default" th:text="#{button.previous}"></a>
							<a th:href="@{/questionnaire/{questionnaireId}/prepublish(questionnaireId=${questionnaireId})}" class="btn btn-default" th:text="#{button.next}"></a>
						</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
