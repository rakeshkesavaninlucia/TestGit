<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionmanagement.viewquestion.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{question.view}"></span></div>
	<div class="container"  th:fragment="contentContainer">
		<div th:if="${oneQQ.question.mode == T(org.birlasoft.thirdeye.constant.QuestionMode).MODIFIABLE }" align="right"><a th:href="@{/question/{id}/edit(id=${oneQQ.question.id})}" class="fa fa-pencil-square-o" title="Edit" sec:authorize="@securityService.hasPermission({'QUESTION_MODIFY'})"></a>
		<a th:if="${oneQQ.question.parentQuestion eq null}" th:href="@{/question/{id}/createVariant(id=${oneQQ.question.id})}" class="fa fa-plus-square" title="Create Question Variant" sec:authorize="@securityService.hasPermission({'QUESTION_MODIFY'})"></a></div>
		<div th:switch="${oneQQ.question.type}" th:fragment="oneQuestionContainer(oneQQ,iter)">
			<div th:case="TEXT"><div  th:replace="question/questionFragments :: TEXT(oneQQ=${oneQQ}, iter=${iter} )"></div></div>
			<div th:case="PARATEXT"><div  th:replace="question/questionFragments :: PARATEXT(oneQQ=${oneQQ}, iter=${iter})"></div></div>
			<div th:case="MULTCHOICE" ><div th:replace="question/questionFragments :: MULTCHOICE(oneQQ=${oneQQ}, iter=${iter})"></div></div>
			<div th:case="NUMBER" ><div th:replace="question/questionFragments :: NUMBER(oneQQ=${oneQQ}, iter=${iter})"></div></div>
			<div th:case="DATE" ><div th:replace="question/questionFragments :: DATE(oneQQ=${oneQQ}, iter=${iter})"></div></div>
			<div th:case="CURRENCY" ><div th:replace="tco/tcoResponse :: CURRENCY(oneQQ=${oneQQ}, iter=${iter})"></div></div>
		</div>
	</div>
</body>
</html>