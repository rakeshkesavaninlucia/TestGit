<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Bar Chart</title>
</head>
<body>
	<div th:fragment="widgetContentWrapper" class= "widgetContentWrapper">
		<div class="box box-primary widget facet-widget direct-chat" th:attr="data-widgetid=${oneWidget.id}" data-widgettype="widget_facet">
			<div class="box-header with-border ">
                 <h3 class="box-title" th:text="${oneWidget.title}">Widget name</h3>
                 <div class="box-tools pull-right">
                 	<button class="btn btn-box-tool" data-widget="chat-pane-toggle" data-type="chat-pane-toggle"><i class="fa fa-cog"></i></button>
                   <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   <button class="btn btn-box-tool" data-widget="delete-widget" data-type="deleteWidget"><i class="fa fa-times"></i></button>
                 </div>
               </div>
               <div class="box-body chart-responsive ">
               	<div class="row">
               		<div class="col-md-12 direct-chat-messages">
							<div th:id="'widget_facet_'+${oneWidget.id}" class='facetGraph with-3d-shadow with-transitions ' style="display:none;">
								<svg></svg>
							</div>
               		</div>
               		<div class="col-md-6 direct-chat-contacts">
               			<form role="form" th:action="@{/widgets/facets/plot}" method="post" class="graphFacetsControl">
               				<input type="hidden" name="saveWidget"></input>
               				<input type="hidden" name="id" th:value="${oneWidget.id}"/>
	               			<div class="form-group">
									<label>Select the questionnaire</label>
									<select class="form-control ajaxSelector" th:attr="data-dependency=${'graph1QE-' + oneWidget.id}" name="questionnaireId" data-type="ajaxSelector">
							          <option value="-1">--Select--</option>
							          <option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:selected="${oneQuestionnaire.id == oneWidget.questionnaireId}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
							        </select>
					        </div>
					        <div class="form-group" id="questionsListDiv" th:fragment="questionListSelect">
								<label>Select the question</label>
						        <select class="form-control ajaxPopulated questionSelector" th:attr="data-dependson=${'graph1QE-' + oneWidget.id}" name="questionId" th:fragment="questionList(listOfQuestions)" th:remove="${ajaxRequest}? tag : none" >
							         <option value="-1">--Select--</option>
							         <option th:each="oneQuestion : ${listOfQuestions}" th:selected="${oneQuestion.id == oneWidget.questionId}" th:value="${oneQuestion.id}" th:text="${oneQuestion.title}" />
							    </select>
					        </div>
					        <div class="form-group" id="paramListDiv" th:fragment="paramListSelect">
								<label>Select the parameter (Optional)</label>
								<p class="help-block">If you wish to show a view of data over a numerical parameter</p>
						        <select class="form-control ajaxPopulated paramSelector" th:attr="data-dependson=${'graph1QE-' + oneWidget.id}" name="paramId" th:fragment="paramList(listOfParams)" th:remove="${ajaxRequest}? tag : none" >
							         <option value="-1">--Select--</option>
							         <option th:each="oneParam : ${listOfParams}" th:selected="${oneParam.id == oneWidget.paramId}" th:value="${oneParam.id}" th:text="${oneParam.name}" />
							    </select>
					        </div>
					        <div class="form-group" >
							       <div class="radio">
						             <label >
						             	<input type="radio" name="graphType" th:checked="${'bar' == oneWidget.graphType}" value="bar" />
						             	<i class="fa fa-bar-chart fa-2"></i>
						             </label>
						           </div>
						           <div class="radio">
						             <label >
						             	<input type="radio" name="graphType" th:checked="${'pie' == oneWidget.graphType}"  value="pie" />
						             	<i class="fa fa-pie-chart fa-2"></i>
						             </label>
						           </div>
						      </div>
						      <div class = "pull-right">
					              <input type="button" class="btn btn-primary plotChart" value="Plot Chart" data-type="plotChart"/>
					              <input type="button" class="btn btn-primary saveWidget" value="Save Widget" data-type="saveWidget"/>
					          </div>
					       </form>
               		</div>
               	</div>
               	<div class="overlay" style="display: none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
               </div>
		</div>
	</div>
</body>
</html>