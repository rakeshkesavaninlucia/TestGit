<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="assetTypeFragment">
		  <span class="control-sidebar-heading" style="text-decoration: underline;font-size: 14px;" th:text="#{asset.type}"></span>
		  <div style="padding: 0px 5px;">
			  <div th:if="${not #lists.isEmpty(listOfAssetTypes)}" th:each="oneAssetType : ${listOfAssetTypes}">	
		         <label class="control-sidebar-subheading clickable"><input type="checkbox" th:attr="data-type='assetClass='+${oneAssetType.assetTypeName},id='assetClass'+${#strings.replace(oneAssetType.assetTypeName,' ','_')}" class="pull-right clickable" th:text="${oneAssetType.assetTypeName}"/></label>
		      </div>	    
	      </div>
	      <br/>
	</div>
	
     <div th:fragment="facetFragment">
		<div style="padding-bottom: 6px;" th:if="${not #lists.isEmpty(listOfFacetBean)}" th:each="oneFacet : ${listOfFacetBean}">
		  <span class="clickable" style="text-decoration: underline;font-size: 14px;color: white;" th:text="${oneFacet.name}" data-toggle="collapse" data-type="expandCollapse" th:attr="data-target='#'+${oneFacet.facetNumber}"></span>
		  <div th:attr="id=${oneFacet.facetNumber}" class="collapse" style="padding: 0px 5px">
			  <div th:if="${not #lists.isEmpty(oneFacet.listOfFilterBean)}" th:each="oneFilterBean : ${oneFacet.listOfFilterBean}">	
		         <label class="control-sidebar-subheading  clickable"><input type="checkbox"  th:checked="${oneFilterBean.checked}" th:attr="data-type=${oneFacet.name}+'='+${oneFilterBean.name}" class="pull-right clickable" th:text="${oneFilterBean.name}"/><span th:text="'('+${oneFilterBean.count}+')'"></span></label>
		      </div>
	      </div>
	      <br/>
	    </div>
	</div>	
    
</body>
</html>