<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='aidReportConfiguration',dataAction='',pageTitle = #{pages.reports.viewaid.aidreport.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.reports.viewaid.aidreport.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.reports.viewaid.aidreport.subtitle}"></span></div>
		<div th:fragment="contentContainer">
			<div class="row">
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-body" data-module = "module-3rdi-Aid">
							<table class="table" style="width:100%;">
								<tr th:each="elem: ${T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).values()}">
									<td class="col-md-8">
										
										<div th:if="${elem ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_1}">
										    <div th:replace="aid/blocks/BLOCK_1 :: BLOCK_1_view(blockForDisplay=${block1})"></div>
										</div>
										
										<div th:if="${elem ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_2}">
										    <div th:replace="aid/blocks/BLOCK_2 :: BLOCK_2_view(blockForDisplay=${block2})"></div>
										</div>
										
										<div th:if="${elem ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_3}">
										    <div th:replace="aid/blocks/BLOCK_3 :: BLOCK_3_view(blockForDisplay=${block3})"></div>
										</div>
										
										<div th:if="${elem ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_4}">
										    <div th:replace="aid/blocks/BLOCK_4 :: BLOCK_4_view(blockForDisplay=${block4})"></div>
										</div>
									</td>
									<td class="col-md-4">
										<button class="btn btn-block btn-primary addAIDBlock btnAIDreport" th:attr="data-blocktype=${elem.name()}">Add to Report</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-body">
							<div th:replace="aid/default/defaultTemplateFragments :: centralApp(aidWrapper=${centralAsset})"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-footer">
							<a th:href="@{/aid/list}" class="btn btn-default btn-block col-md-1">Done</a>
							<a th:href="@{/aid/{id}/view(id=${aidId})}" class="btn btn-primary btn-block col-md-1">View Report</a>
		                </div>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
		
		</div>	
		
		<div th:fragment="scriptsContainer" th:remove="tag">
		  	<script	th:src="@{/static/js/3rdEye/modules/module-3rdi-Aid.js}"></script>
		</div>
		
		
</body>
</html>