<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.home.portfolio.asset.nav })">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="#{parameter.view}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{parameter.asset.health}"></span>
	</div>
	<div th:fragment="contentContainer">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title" th:text="${parameterName}"></h3>
						</div>	
						<div class="box-body assetParameter" data-module="module-assetParameterMap" th:attr="data-parametername=${parameterName},data-parameterid=${parameterId}">
							<div style="width:700px;overflow-x:auto;">
								<div id="assetParameterMap" class='with-3d-shadow with-transitions' style="height: 420px;">
								    <svg></svg>							 
								</div>	
							</div>											
						</div>
					</div>
				</div>
			</div>
	 </div>
	
	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-assetParameterMap.js}"></script>	
	</div>
</body>
</html>