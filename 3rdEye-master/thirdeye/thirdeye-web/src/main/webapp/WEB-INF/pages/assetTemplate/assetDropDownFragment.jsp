<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryaddasset.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="assetDropDown">
		<div class="row">
			<div data-module="module-assetSelect2">
				<script type="text/x-config" th:inline="javascript">{"assetId":"[[${asset.id}]]","templateid":"[[${assetTemplateId}]]"}</script>

				<div class="col-md-6">
					<div class="col-md-2">
						<a data-type="leftButton"
							class="pull-left fa fa-chevron-left btn btn-default leftarrow"></a>
					</div>
					<div class="col-md-8">
						<select class="select2" id="assetselection">
							<option value="-1">--Select an Asset--</option>
							<option th:each="oneAsset : ${listofAsset}"
								th:value="${oneAsset.id}" th:text="${oneAsset.shortName}"
								th:selected="${oneAsset.id==asset.id}" />
						</select>
					</div>
					<div class="col-md-2">
						<a data-type="rightButton"
							class="pull-right fa fa-chevron-right btn btn-default rightarrow">
						</a>
					</div>
				</div>

				<div class="col-md-6" align="right" style="margin-top:4px;">
					<div>
						<a th:href="@{/templates/populateTemplateForm/{id}(id=${assetTemplateId})}">Create</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a data-type="edit" class="edit clickable">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="#">Delete</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button data-type="save" class="btn btn-primary btn-xs">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>