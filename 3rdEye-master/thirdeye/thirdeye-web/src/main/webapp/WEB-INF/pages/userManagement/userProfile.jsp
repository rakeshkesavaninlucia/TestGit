<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

	<div class="container" th:fragment="contentContainer">
		<div class="col-md-14">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"
						th:text="#{pages.userManagement.userProfile.title}"></h3>
				</div>
				<div class="box-body">
					<div th:fragment="createUserForm">
						<div data-module="module-userProfileView">
							<i class="fa fa-camera fa-2 camera clickable" title="Upload Photo"></i>
							 <img id="user-img" th:if="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() eq null}" style="float: right" height="140px" width="140px" th:src="@{/static/img/user2-160x160.jpg}" class="img-circle" />
							<img id="user-img" style="float: right" height="140px" width="140px" class="img-circle"	th:if="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() ne null}" th:src="@{/user/{id}/showimage(id=${@customUserDetailsServiceImpl.getCurrentUser().getId()})}" />
							<form th:action="@{/user/saveUserProfile?_csrf={_csrf.token}(_csrf.token=${_csrf.token})}" method="post" th:object="${UserForm}" enctype="multipart/form-data">
								<input type="hidden" th:field="*{id}" />
								<input type = "hidden" th:field = "*{photo}"/>
								<div class="form-group">
									<span>First Name :<span style="color: red;">*</span></span> 
									<input style="width: 380px;" type="text" th:field="*{firstName}" class="form-control" placeholder="First Name" />
									 <span th:if="${# fields.hasErrors('firstName')}" th:errors="*{firstName}" style="color: red;">Test</span><br />
									<span>Last Name :<span style="color: red;">*</span></span>
									 <input	style="width: 380px;" type="text" th:field="*{lastName}" class="form-control" placeholder="Last Name" />
									  <span	th:if="${#fields.hasErrors('lastName')}" th:errors="*{lastName}" style="color: red;">Test</span> <br />
									<div style="float: right;">
										<span th:if="${#fields.hasErrors('photo')}"
											th:errors="*{photo}" style="color: red;">Test</span>
									</div>
									<input type="file" class="fake" style="float: right;" name="fileUpload" value="fileUpload" accept="fileUpload/jpeg,fileUpload/png,fileUpload/jpg" />
									 <span>Email:<span style="color: red;">*</span>
									</span> <input type="email" th:field="*{emailAddress}" class="form-control" placeholder="Email" />
									 <span th:if="${#fields.hasErrors('emailAddress')}" th:errors="*{emailAddress}" style="color: red;">Test</span><br />
									<span>Password :<span style="color: red;">*</span></span> 
									<input type="password" th:field="*{password}" class="form-control" placeholder="Password"/>
									 <span th:if="${#fields.hasErrors('password')}"	th:errors="*{password}" style="color: red;">Test</span><br />
									<span> Hint Question1 :<span style="color: red;">*</span></span>
									<select class="form-control select2" id="question1dropdown"	name="question" data-type="selecttype">
										<option value="-1">--Select--</option>
										<option
											th:each="question : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
											th:value="${question.value}" th:text="${question.value}">
										</option>
									</select> <br />
									<div id="answerTextfield" style="display: none;">
										<br /> <span>Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="answer" th:value="${JSONQuestionAnswerMapper}" class="form-control" placeholder="Enter Answer" />
									</div>
									<br /> <span> Hint Question2 :<span style="color: red;">*</span></span>
									<select class="form-control select2" id="question2dropdown"	name="question2" data-type="selecttype">
										<option value="-1">--Select--</option>
										<option
											th:each="question2 : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
											th:value="${question2.value}" th:text="${question2.value}">
										</option>
									</select> <br />
									<div id="answerTextfields" style="display: none;">
										<br /> <span>Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="answer2" th:value="${JSONQuestionAnswerMapper}" class="form-control" id="answerTextfield" placeholder="Enter Answer" />
									</div>
									<br /> <span>Organization Name</span><span style="color: red;">*</span>
									<input type="text" th:field="*{organizationName}" class="form-control" placeholder="Organization Name" /> 
									<span th:if="${#fields.hasErrors('organizationName')}" th:errors="*{organizationName}" style="color: red;">Test</span>
									<br /> <span>Country:<span style="color: red;">*</span></span>
									<select	class="form-control select2" th:field="*{country}" data-type="selecttype" id="dropdown">
										<option value="select">--Select--</option>
										<option
											th:each="country : ${T(org.birlasoft.thirdeye.constant.CountryType).values()}"
											th:value="${country}" th:text="${country.value}"></option>
									</select> <span th:if="${#fields.hasErrors('country')}"
										th:errors="*{country}" style="color: red;">Test</span>
								</div>
								<span th:if="${#fields.hasErrors('questionAnswer')}"
									th:errors="*{questionAnswer}" style="color: red;">Test</span>
								<div class="box-footer">
									<div>
										<input class="btn btn-primary" type="submit" value="Save" />
										<div style="float: right;"><a  th:href="@{/user/changePassword}" class="btn btn-primary" >Change Password</a></div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script
			th:src="@{/static/js/3rdEye/modules/module-userProfileView.js}"></script>
	</div>

</body>
</html>