<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionnaire.response.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.questionnaire.response.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.questionnaire.response.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="box box-primary">
            <div class="box-body">
 			<div class="table-responsive" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th><span th:text="#{questionnaire.name.table}"></span></th>
							<th><span th:text="#{questionnaire.description.table}"></span></th>
							<th><span th:text="#{questionnaire.progress.table}"></span></th>
							<th><span th:text="#{questionnaire.label.table}"></span></th>
							<th><span th:text="#{questionnaire.status.table}"></span></th>
							<th><span th:text="#{tabel.header.action}"></span></th>
						</tr>
					</thead>
					<tbody>
						<tr th:each="oneQuestionnaire : ${listOfquestionnaires}">
							<td><span th:text="${oneQuestionnaire.name}"></span></td>
							<td><span th:text="${oneQuestionnaire.description}"></span></td>
							<td>
								<div class="progress progress-xs progress-striped">
		                          <div class="progress-bar progress-bar-primary" th:style="'width:'+ ${completionPercentage[oneQuestionnaire.id]} + '%'"></div>
		                        </div>
	                        </td>
	                        <td><span class="badge bg-light-blue" th:text="${completionPercentage[oneQuestionnaire.id] + '%'}">55%</span></td>
	                        <td><span th:if="${null ne answeredMandatoryQuestion[oneQuestionnaire.id]}" th:text="${answeredMandatoryQuestion[oneQuestionnaire.id].answered + '/' + answeredMandatoryQuestion[oneQuestionnaire.id].total}"></span></td>
	                        <td>
	                        	<a th:href="@{/questionnaire/{id}/nqrq/response/list(id=${oneQuestionnaire.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY'})"></a>&nbsp;
	                        	<a sec:authorize="@securityService.hasPermission({'BOOST_EVALUATED_VALUE'})" th:href="@{/parameter/boost/{id}/view(id=${oneQuestionnaire.id})}" class="fa fa-bolt" th:title="#{icon.title.boost}"></a>
	                        </td>
						</tr>					
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>