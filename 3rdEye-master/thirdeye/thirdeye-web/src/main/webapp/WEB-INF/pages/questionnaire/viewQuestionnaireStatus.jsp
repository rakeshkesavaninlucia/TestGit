<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionnaire.viewquestionnaire.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<a class="pull-left fa fa-chevron-left btn btn-default" th:href="@{/{action}/list(action=${action})}" style="margin-left: 10px;"></a>
	   <div>
			<span th:text="${questionnaireForm.name}"></span>		
	   </div>
	</div>
	<div class="container"  th:fragment="contentContainer">
	    
		<div class="box box-primary col-md-12"  data-module="module-questionnaireStatus">
		    <div class="box-header with-border">
		        <div class="col-md-6"><h3 class="box-title" style="font-weight: 600" th:text="#{questionnaire.description}"></h3>&nbsp;&nbsp;&nbsp;<span th:text="${questionnaireForm.description}"></span></div>
		        <div class="col-md-6"><h3 class="box-title" style="font-weight: 600" th:text="#{questionnaire.status}"></h3>&nbsp;&nbsp;&nbsp;<span th:text="${questionnaireForm.status}"></span></div>
		    </div>
		    <table class="col-md-12 table-responsive" style=" border: 1px solid black;">
			    <tr>
				    <td><div class="box-header" style="text-align: center;"><h3 class="box-title" style="font-weight: 600" th:text="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)} ? #{questionnaire.root.parameter} : #{tco.root.costofstructure}"></h3></div><div th:replace="questionnaire/viewQuestionnaireStatus :: questionnaireParameterFragment(${rootParameterList})"></div></td>
				    <td style="border: 1px solid black;"><div class="box-header" style="text-align: center;"><h3 class="box-title" style="font-weight: 600" th:text="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)} ? #{questionnaire.category} : #{tco.category}"></h3></div><div th:replace="questionnaire/viewQuestionnaireStatus :: questionnaireCategoryFragment(${mapOfQC})"></div></td>
			    </tr>		    
		    </table>	
		</div>	     
	     <div class="box-footer">
              <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaireForm.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaireForm.status) }" th:href="@{/{action}/{id}/modify/{status}(id=${questionnaireForm.id},status=EDITABLE,action=${action})}" class="btn btn-primary" th:text="#{questionnaire.editable}"></a>
              <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).EDITABLE.name().equalsIgnoreCase(questionnaireForm.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaireForm.status)}"  th:href="@{/{action}/{id}/modify/{status}(id=${questionnaireForm.id},status=PUBLISHED,action=${action})}" class="btn btn-primary" th:text="#{questionnaire.published}"></a>
              <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaireForm.status)}" th:href="@{/{action}/{id}/modify/{status}(id=${questionnaireForm.id},status=REVIEWED,action=${action})}" class="btn btn-primary" th:text="#{questionnaire.review}"></a>
			  <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaireForm.status)}" th:href="@{/{action}/{id}/modify/{status}(id=${questionnaireForm.id},status=CLOSED,action=${action})}" class="btn btn-primary" th:text="#{questionnaire.closed}"></a>
         </div>
		
		
	    <div class="modal fade paramTreeModalModule" data-module="module-paramTreeModal"></div>
	</div>
	<div th:fragment="questionnaireParameterFragment(rootParameterList)" >
		
		    
			<div class="table-responsive" style="padding: 10px;" data-module="common-data-table">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
						<th><span th:text="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)} ? #{questionnaire.parameter.displayname} : #{tco.costofstructure.displayname} "></span></th>
						<th><span th:text="#{questionnaire.parameter.action}"></span></th>
					</thead>
					<tbody>
				      <tr th:each="oneParameter : ${rootParameterList}" th:id="${oneParameter.id}">
			              <td ><span th:text="${oneParameter.displayName}">Parameter</span></td>
			              <td ><a data-type="viewParameterTree" th:attr="data-parameterid=${oneParameter.id}" class="fa fa-tree"  style="cursor: pointer" th:title="#{icon.title.treeview}"></a></td>
		              </tr>
					</tbody>
			    </table>
	         </div>
	   			
	</div>
	<div th:fragment="questionnaireCategoryFragment(mapOfQC)" >	
	 
				<div class="table-responsive" style="padding: 10px;" data-module="common-data-table">
					<table class="table table-bordered table-condensed table-hover">
						<thead>
							<th><span th:text="#{questionnaire.category.name}"></span></th>
							<th><span th:text="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)} ? #{questionnaire.category.noofquestion} : #{tco.category.noofcostelement}"></span></th>
						</thead>
						<tbody>
					      <tr th:each="oneEntry : ${mapOfQC}">
				              <td><span th:text="${oneEntry.key}">Category Name</span></td>
				              <td><span th:text="${oneEntry.value}">No.Of Question</span></td>
			              </tr>
						</tbody>
				    </table>
		         </div>	  			
	</div>
	
	
	<div th:fragment="scriptsContainer"  th:remove="tag">    
	
	   <script th:src="@{/static/js/3rdEye/modules/module-questionnaireStatus.js}"></script>
       <script	th:src="@{/static/js/3rdEye/modules/module-paramTreeModal.js}"></script>
    </div>
</body>
</html>