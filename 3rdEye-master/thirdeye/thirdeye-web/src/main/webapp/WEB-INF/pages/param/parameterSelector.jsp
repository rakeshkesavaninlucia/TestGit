<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="parameterSelector">
		<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
					<form id="parameterSelectorId">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">
				          <span  th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.select.parameters}"></span>
				          <span  th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.select.coststructure}"></span>
				        </h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
				          	<b>
				          	<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{parameter.select.help}"></span>
				          	<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.select.help}"></span>
				          	</b>
							<table class="table table-bordered table-condensed table-hover paramListTable">
								<thead>
									<tr>
										<th></th>
										<th th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}">
										  <span th:text="#{parameter.theader.parent}"></span>
										</th>
										<th><span th:text="#{parameter.theader.uniquename}"></span></th>
										<th><span th:text="#{parameter.theader.description}"></span></th>
										<th><span th:text="#{parameter.theader.displayname}"></span></th>
									</tr>
								</thead>
								<tbody >
									<tr th:each="oneRow : ${listOfParameter}" th:id="${oneRow.id}">
										<td><input type="checkbox" name="parameter" class="parameterClass" th:value="${oneRow.id}"/></td>
										<td th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" ><span th:if="${oneRow.parameter ne null}" th:text="${oneRow.parameter.uniqueName}">Test</span></td>
										<td><span th:text="${oneRow.uniqueName}">Test</span></td>
										<td><span th:text="${oneRow.description}">Test</span></td>
										<td><span th:text="${oneRow.displayName}">Test</span></td>
									</tr>			
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" data-type ="saveParameters" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
</body>
</html>