<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<title>Insert title here</title>
</head>
<body>

    

	<div th:fragment="inboundInterfaces(listOfInboundApps)" th:remove="tag">
		<td class="col-md-4" style="padding-right: 0px;">
			<!-- One inbound Application 1 -->
				<table style="width:100%; margin-bottom: 15px;" th:each="oneInboundAppBean : ${listOfInboundApps}">
					<tr>
						<td class="inboundAsset col-md-5 " th:style="'background:'+${oneInboundAppBean.assetStyle}">
							<span th:text="${oneInboundAppBean.asset }">Name of inbound</span>
						</td>
							
						<td class="col-md-7" style="padding:0px;">
							<table style="width:100%;" >
								<tr th:each="oneInterface : ${oneInboundAppBean.listOfRelationshipAssetData}">
								<td class = "col-md-1" style="padding:1px;margin-bottom: 10px;position: relative;" th:if= "${oneInterface.direction eq 'BIDI' }">
    									<div class="interfaceoutbound"></div>
 								    </td>
									<td class="col-md-5 " style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceWrapper">
											<div class = "interface">
												<div class="interfaceFrequency" th:if="${ oneInterface.frequency ne null}" th:text="${ oneInterface.frequency}">data</div>
												<div class="interfaceDataType" th:if="${ oneInterface.dataType ne null}" th:text="${ oneInterface.dataType}">data</div>
											</div>
										</div>
									</td>
									<td th:if="${oneInterface.displayName != null}" class="col-md-5" style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceType" th:text="${ oneInterface.displayName}">Other</div>
									</td>
									<td class="col-md-2" style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceWrapper">
											<div class = "interface">
												
											</div>
										</div>
									</td>
									<td class = "col-md-1" style="padding:1px;margin-bottom: 10px;position: relative;" th:if= "${oneInterface.direction eq 'UNI' || oneInterface.direction eq 'BIDI' }">
    									<div class="interfaceinbound"></div>
 								    </td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
		</td>
	</div>
	
	<div th:fragment="outboundInterfaces(listOfOutboundApps)" th:remove="tag">
		<td class="col-md-4" style="padding-left: 0px;">
			<!-- One inbound Application 1 -->
				<table style="width:100%; margin-bottom: 15px;" th:each="oneOutputAppBean : ${listOfOutboundApps}">
					<tr>
						
						<td class="col-md-7" style="padding:0px;">
							<table style="width:100%;" >
								<tr th:each="oneInterface : ${oneOutputAppBean.listOfRelationshipAssetData}">
								    <td class = "col-md-1" style="padding:1px;margin-bottom: 10px;position: relative;" th:if= "${oneInterface.direction eq 'UNI' || oneInterface.direction eq 'BIDI' }">
    									<div class="interfaceoutbound"></div>
 								    </td>
									<td class="col-md-5" style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceWrapper">
											
											<div class = "interface">
												<div class="interfaceFrequency" th:if="${ oneInterface.frequency ne null}" th:text="${ oneInterface.frequency}">data</div>
												<div class="interfaceDataType" th:if="${ oneInterface.dataType ne null}" th:text="${ oneInterface.dataType}">data</div>
											</div>
										</div>
									</td>
									<td th:if="${oneInterface.displayName != null}" class="col-md-5" style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceType" th:text="${ oneInterface.displayName}">Other</div>
									</td>
									<td class="col-md-2" style="padding:0px;margin-bottom: 10px;position: relative;">
										<div class="interfaceWrapper">
											<div class = "interface">
												
											</div>
										</div>
									</td>
									<td class = "col-md-1" style="padding:1px;margin-bottom: 10px;position: relative;" th:if= "${oneInterface.direction eq 'BIDI' }">
												<div class="interfaceinbound"></div>
											
 								    </td>
									</tr>
							</table>
						</td>
						<td class="outboundAsset col-md-5 " th:style="'background:'+${oneOutputAppBean.assetStyle}">
							<span th:text="${oneOutputAppBean.asset}">Name of inbound</span>
						</td>
					</tr>
				</table>
		</td>
	</div>

	<div class="col-lg-12" th:fragment="aidContentWrapper">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title" th:text="${oneAid.centralAsset.centralAssetBean}">Asset Name</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body" style="width: 100%; overflow-x:scroll;overflow-y: hidden;">
				<table style="width: 100%;">
					<thead>
						<tr>
							<th class="col-md-4" style="padding-right: 0px;">
								<!-- Inbound Interfaces -->
								<span>Inbound Interfaces</span>
							</th>
							<th class="col-md-4" style="text-align: center; padding-bottom: 5px;">
								<span th:text="${oneAid.centralAsset.centralAssetBean}">Application Details</span>
							</th>
							<th class="col-md-4">
								<span>Outbound Interfaces</span>
							</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td th:replace="aid/aidFragments :: inboundInterfaces(listOfInboundApps=${oneAid.listOfInboundAssets})"></td>
							<td class="col-md-4 " style="padding: 0px !important">
								<div th:replace="aid/default/defaultTemplateFragments :: centralApp(aidWrapper=${oneAid.centralAsset})"></div>
							</td>
							<td th:replace="aid/aidFragments :: outboundInterfaces(listOfOutboundApps=${oneAid.listOfOutboundAssets})"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	
	<div th:fragment="applInterface">		
		<div class="row oneAid" th:each="oneAsset: ${aidAssetSet}">
				<div class="col-lg-12" >
					<div class="box box-primary applicationID"  th:attr="data-loadurl=${'aid/view/' + oneAsset}">
						<div class="box-body">
						</div>
					</div>
				</div>			
		</div>
		<div class="box box-primary" th:if="${#lists.isEmpty(aidAssetSet)}">
			<div class="box-header with-border">
				<h3 class="box-title">No Data available</h3>
			</div>
		</div>		
	</div>

</body>
</html>