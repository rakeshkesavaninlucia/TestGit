<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionmanagement.createquestion.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<div th:switch="${questionForm.id}">
			<span th:case="null" th:text="#{pages.questionmanagement.createquestion.title}"></span>
			<span th:case="*" th:text="#{pages.questionmanagement.updatequestion.title}"></span>
		</div>
	</div>
	<div class="container"  th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
		    		<div class="box-body">
						<div th:replace="question/questionTemplateFragement :: createQuestionForm"></div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div th:fragment="scriptsContainer">
  		<script th:src="@{/static/js/3rdEye/modules/module-createQuestion.js}"></script>
  		<script th:src="@{/static/js/3rdEye/modules/module-benchmarkQuestion.js}"></script>
	</div>
</body>
</html>