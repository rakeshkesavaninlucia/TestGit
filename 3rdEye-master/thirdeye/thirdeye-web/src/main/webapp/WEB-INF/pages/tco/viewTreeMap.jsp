<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tco.reports.treeMap.nav})">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{tco.reports.treemap.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{tco.reports.treemap.subtitle}"></span>
	</div>

	<div th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary" data-module="module-tcoTree">
					<div class="box-header with-border">
						<h3 class="box-title" th:text="${coaName}"></h3>
						<div class="box-tools pull-right">
							<i data-type="tree" th:attr="data-coaid=${chartOfAccountId}" class="fa fa-tree treeicon clickable" th:title="${coaName} + #{icon.title.treeview}"></i>&nbsp;&nbsp;
						</div>
						 <div class="box-tools pull-right">
					            <a th:href="@{/tco/{id}/response/exportCOA(id=${chartOfAccountId})}" class="fa fa-file-excel-o clickable excelDownload" th:title = "${coaName} + #{icon.title.excel}" ></a>
						   	 </div>	  
						</div>
						<div class="box-body">
							<p>Click to zoom to the next level. Click on the top grey
								band to zoom out.</p>
							<div id="treemap" class="clickable"	style="height: 35em; width: 100%; overflow-x: scroll; white-space: nowrap;"
								th:attr="data-coaid=${chartOfAccountId}"></div>
						</div>
						
						<script type="text/x-config" th:inline="javascript">{"pngimagename":"[[${coaName}]]"}</script>
						
						<div id="assetDetail-container">
							<div id="tcoasset" class="tcoasset"
								style="height: 25em; width: 100%; overflow-x: scroll; white-space: nowrap"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="pageModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" style="display: none;"></div>
		</div>

		<div th:fragment="scriptsContainer" th:remove="tag">
			<link th:href="@{/static/css/3rdEye/zoomableTreeMap.css}" rel="stylesheet" type="text/css" />
			<script th:src="@{/static/js/ext/d3/v2/d3v2.js}"></script>
			<script th:src="@{/static/js/3rdEye/modules/module-tcoTree.js}"></script>
		</div>
</body>
</html>