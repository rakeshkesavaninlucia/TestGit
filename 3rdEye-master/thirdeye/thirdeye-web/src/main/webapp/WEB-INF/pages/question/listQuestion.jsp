
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionmanagement.listquestion.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.questionmanagement.viewquestion.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.questionmanagement.viewquestion.subtitle}"></span>
	</div> 
	<div class="container" th:fragment="contentContainer">
        <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">	        		 
	                <div class="box-body">
	                	<div class="table-responsive" data-module="common-data-table">
									<div class="overlay">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
							<table class="table table-bordered table-striped table-condensed table-hover dataTable" id="questionList">
								<thead>
									<tr>
										<th class="sorting"><span th:text="#{question.category}" class="col-sm-2"></span></th>
										<th class="sorting"><span th:text="#{question.title.table}" class="col-sm-4"></span></th>
										<th><span th:text="#{question.help.text.table}" class="col-sm-4"></span></th>
										<th class="sorting"><span th:text="#{question.level.text}" class="col-sm-2"></span></th>
										<th><span th:text="#{question.date}" class="col-sm-2"></span></th>
										<th><span th:text="#{question.action}" class="col-sm-2"></span></th>
									</tr>
								</thead>
								<tbody >
									<tr th:each="oneRow : ${listOfQuestion}" th:id="${oneRow.id}">
										<td><span th:if="${oneRow.category ne null}" th:text="${oneRow.category.name}">Test</span></td>
										<td class="col-sm-4"><a th:href="@{/question/{id}/view(id=${oneRow.id})}"><span th:text="${oneRow.title}">Test</span></a></td>
										<td class="col-sm-4"><span th:text="${oneRow.helpText}">Test</span></td>
										<td><span th:text="${oneRow.level}">Test</span></td>
										<td class="col-sm-2"><span th:text="${#dates.format(oneRow.updatedDate, 'dd-MMM-yyyy')}">Test</span></td>
										<td class="col-sm-2">
										  <div th:if="${oneRow.questionMode ne null}"  th:switch="${oneRow.questionMode}">
											  <div th:case="FIXED"> <a th:href="@{/question/{id}/view(id=${oneRow.id})}" class="fa fa-search" th:title="#{icon.title.view.question}"></a></div>
											  <div th:case="MODIFIABLE"><a th:href="@{/question/{id}/edit(id=${oneRow.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'QUESTION_MODIFY'})"></a><a th:href="@{/question/{id}/view(id=${oneRow.id})}" class="fa fa-search" th:title="#{icon.title.view.question}"></a></div>									      										     
										  </div>										 
										</td>
									</tr>			
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
	                  <div>
				          <a class="btn btn-primary" th:href="@{/question/create}">Create Question</a>
				        </div>
	                </div>
				</div>
			</div>
		</div>
	</div>
	</body>
	</html>