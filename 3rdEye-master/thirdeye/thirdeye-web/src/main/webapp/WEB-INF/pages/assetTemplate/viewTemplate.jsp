<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.viewtemplate.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

	<div th:fragment="pageTitle"><span th:text="${oneAssetTemplate.assetTemplateName}">View Template name</span></div>
	<div th:fragment="pageSubTitle"><span th:text=" 'Asset Type : '  + ${oneAssetTemplate.assetType.assetTypeName}"></span><br/>
	<span th:text=" ' Description : ' +${oneAssetTemplate.description}">page subtitle</span></div>
	
	<div class="container"  th:fragment="contentContainer">
		<div class="box box-primary">
        <div class="box-body">
        	<b th:text="${assetTemplateName}"></b>
			<div class="table-responsive" data-module="module-viewAssetTemplate">
				<form:hidden id="sequenceNumber" />
				<table class="table table-bordered table-condensed table-hover" id="templateColumns">
					<thead>
						<tr>
							<th>Column Name</th>
							<th>Data type</th>
							<th>Length</th>
							<th>Mandatory</th>
							<th>Filterable</th>
							<th sec:authorize="@securityService.hasPermission({'TEMPLATE_MODIFY'})">Actions</th>
						</tr>
					</thead>
					<tbody >
						  <tr th:each="oneCol : ${oneAssetTemplate.assetTemplateColumns}" th:include="assetTemplate/editTemplateFragments :: viewOnlyTemplate"
						  	th:with="colid=${oneCol.id},assetTemplateColName=${oneCol.assetTemplateColName}, dataType=${oneCol.dataType}, length=${oneCol.length}, mandatory=${oneCol.mandatory},filterable=${oneCol.filterable},
						  	sequenceNumber=${oneCol.sequenceNumber},assetTypeName=${oneAssetTemplate.assetType.assetTypeName}"/>
					</tbody>
				</table>
				<span sec:authorize="@securityService.hasPermission({'TEMPLATE_MODIFY'})"><button id="addRowButton" class="btn btn-primary" >Add Row</button></span>
			</div>
        </div>
        </div>
	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/ext/jquery.tablednd/0.8/jquery.tablednd.min.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-viewAssetTemplate.js}"></script>
	</div>
</body>
</html>