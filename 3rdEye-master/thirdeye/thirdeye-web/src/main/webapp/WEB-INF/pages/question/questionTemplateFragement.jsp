<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="createQuestionForm" data-module="module-createQuestion">
		<form th:action="@{/question/save}" method="post" th:object="${questionForm}">
		<input type="hidden" th:field="*{id}" />
		<input type="hidden" name="parentQuestionId" th:if="${questionForm.question}" th:value="${questionForm.question.id }"/>
			<div class="form-group">
				<table class="table table-bordered table-condensed table-hover">
					<tr>
						<td class="col-md-2 horizontal"><label for="workspace" class="control-label" th:text="#{question.workspace}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null}">
								<select id="userWorkSpaceId" name="workspace" class="form-control select2">
									<option th:selected="${questionForm.id eq null or (questionForm.workspace ne null and (questionForm.workspace.id eq activeWorkspace.id))}" th:value="${activeWorkspace.id}" th:text="${activeWorkspace.workspaceName}" />
									<option th:selected="${questionForm.id gt 0 and questionForm.workspace eq null}" value="" th:text="#{option.systemlevel}">Independent from workspace</option>
								</select> 
							</div>
							<div th:if="${questionForm.id ne null}">
								<span th:if="${questionForm.workspace ne null}" th:text="${questionForm.workspace.workspaceName}"></span>
								<span th:if="${questionForm.workspace eq null}" th:text="#{option.systemlevel}"></span>
								<input th:if="${questionForm.workspace ne null}" type="hidden" th:field="*{workspace}" />
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="questionMode" class="control-label" th:text="#{question.mode}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null and questionForm.question eq null}">
								<select th:field="*{questionMode}" class="form-control select2">
									<option value="-1" selected="selected">--Select--</option>
									<option th:each="oneMode : ${T(org.birlasoft.thirdeye.constant.QuestionMode).values()}" th:value="${oneMode}" th:text="${oneMode}"></option>
								</select>
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}">
								<span th:if="${questionForm.questionMode ne null}" th:text="${questionForm.questionMode}"></span>
								<input th:if="${questionForm.questionMode ne null}" type="hidden" th:field="*{questionMode}" />
							</div>
							<span th:if="${#fields.hasErrors('questionMode')}" th:errors="*{questionMode}" class="error_msg">Test</span>
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="category" class="control-label" th:text="#{question.category}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null and questionForm.question eq null}" >
							<select class="form-control select2 allowOptionAddition" name="questionCategory">
								<option value="" selected="selected">--Select--</option>
								<option th:each="oneCategory : ${listOfCategory}" th:text="${oneCategory.name}"></option>
							</select>
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}">
								<span th:if="${questionForm.category ne null}" th:text="${questionForm.category.name}"></span>
								<input th:if="${questionForm.category ne null}" type="hidden" name="questionCategory" th:value="${questionForm.category.name}" />
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="displayName" class="control-label" th:text="#{question.displayName}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null and questionForm.question eq null}">
							    <input type="text" th:field="*{displayName}" class="form-control" maxlength="45"  />							
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
							    <div th:case="FIXED">
							       <span th:if="${questionForm.displayName ne null}" th:text="${questionForm.displayName}"></span>
							       <input th:if="${questionForm.displayName ne null}" type="hidden" th:field="*{displayName}" />
							    </div>
							    <div th:case="MODIFIABLE">							           
							        <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
							            <span th:if="${questionForm.displayName ne null}" th:text="${questionForm.displayName}"></span>
							            <input th:if="${questionForm.displayName ne null}" type="hidden" th:field="*{displayName}" />
							        </div>
							        <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">
							          <input type="text" th:field="*{displayName}" class="form-control" maxlength="45"  />
							       </div>
							    </div>							    							
							</div>						  
						    <span th:if="${#fields.hasErrors('displayName')}" th:errors="*{displayName}" class="error_msg">Test</span>						 
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="title" class="control-label" th:text="#{question.title}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null and questionForm.question eq null}">
							     <input type="text" th:field="*{title}" class="form-control" />
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
							    <div th:case="FIXED">
							       <span th:if="${questionForm.title ne null}" th:text="${questionForm.title}"></span>
							       <input th:if="${questionForm.title ne null}" type="hidden" th:field="*{title}" />
							    </div>
							    <div th:case="MODIFIABLE">
							       <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
							         <span th:if="${questionForm.title ne null}" th:text="${questionForm.title}"></span>
							         <input th:if="${questionForm.title ne null}" type="hidden" th:field="*{title}" />							       
							       </div>
							       <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">					        
							         <input type="text" th:field="*{title}" class="form-control" />
							       </div>
							    </div>							    							
							</div>	
							<span th:if="${#fields.hasErrors('title')}" th:errors="*{title}" class="error_msg">Test</span>
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="helpText" class="control-label" th:text="#{question.help.text}"></label></td>
						<td class="col-md-10">
						    <div th:if="${questionForm.id eq null and questionForm.question eq null}">
							    <input type="text" class="form-control" th:field="*{helpText}"></input>
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
							    <div th:case="FIXED">
							       <span th:if="${questionForm.helpText ne null}" th:text="${questionForm.helpText}"></span>
							       <input th:if="${questionForm.helpText ne null}" type="hidden" th:field="*{helpText}" />
							    </div>
							    <div th:case="MODIFIABLE">
							       <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
								      <span th:if="${questionForm.helpText ne null}" th:text="${questionForm.helpText}"></span>
								      <input th:if="${questionForm.helpText ne null}" type="hidden" th:field="*{helpText}" />
							       </div>
							        <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">
							          <input type="text" th:field="*{helpText}" class="form-control" />
							       </div>
							    </div>							    							
							</div>							   
						</td>
					</tr>
					<tr>
						<td class="col-md-2 horizontal"><label for="questionType" class="control-label" th:text="#{question.type}"></label></td>
						<td class="col-md-10">
							<div th:if="${questionForm.id eq null and questionForm.question eq null}">
								<select th:field="*{questionType}" class="form-control select2" data-type="questiontype">
									<option value="-1" selected="selected">--Select--</option>
									<option th:each="listOfQuestionType : ${T(org.birlasoft.thirdeye.constant.QuestionType).values()}" th:value="${listOfQuestionType}" th:text="${listOfQuestionType.value}"></option>
								</select>
							</div>
							<div th:if="${questionForm.id ne null or questionForm.question ne null}">
								<span th:if="${questionForm.questionType ne null}" th:text="${questionForm.questionType}"></span>
								<input th:if="${questionForm.questionType ne null}" type="hidden" th:field="*{questionType}" />
							</div>
							<div id="divCheckbox" style="display: none;" data-module="module-benchmarkQuestion">
								<label><input type="checkbox" class="benchmarkCheckboxMCQ" name="benchmarkMCQ" data-type="fetchBenchmark" th:checked="${benchmarkQuestionBean ne null and benchmarkQuestionBean.benchmarkCheckboxMCQ eq true}"/><span th:text="#{question.mcq.benchmark}"></span></label>
							</div>
							<span th:if="${#fields.hasErrors('questionType')}" th:errors="*{questionType}" class="error_msg">Test</span>
							<br/>
							<div id="getFragment">	
							    <span th:if="${#fields.hasErrors('queTypeText')}" th:errors="*{queTypeText}" class="error_msg">Test</span>
								<br/>
								<div th:switch="${questionForm.questionType}">
									<div th:case="TEXT"><div th:replace="question/questionTemplateFragement :: TEXT"></div></div>
									<div th:case="PARATEXT"><div th:replace="question/questionTemplateFragement :: PARATEXT"></div></div>
									<div th:case="MULTCHOICE">
										<div th:if="${questionForm.benchmark eq null}" th:replace="question/questionTemplateFragement :: MULTICHOICE(${decodedArrayOfOption},${questionForm})"></div>
										<div th:if="${questionForm.benchmark ne null}">
											<input type="hidden" name="benchmarkMCQ" value="true" />
											<input type="hidden" name="benchmarkId" th:value="${questionForm.benchmark.id}"/>
											<span th:text="${questionForm.benchmark.name}"></span>
										</div>
										<div th:if="${benchmarkQuestionBean.benchmarkCheckboxMCQ eq true}"><div th:replace="benchmark/benchmarkFragment :: viewBenchmarks"></div></div>
										<br/>
										<div th:if="${questionForm.benchmark ne null or benchmarkQuestionBean.benchmarkId ne null}" th:replace="benchmark/benchmarkFragment :: viewBenchmarkItems"></div>
									</div>
									<div th:case="NUMBER"><div th:replace="question/questionTemplateFragement :: NUMBER(${questionForm})"></div></div>
									<div th:case="DATE"><div th:replace="question/questionTemplateFragement :: DATE"></div></div>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<input class="btn btn-primary" style="margin-top: 10px" type="submit" value="Done"/>
			</div>
		</form>
	</div>
	
	<div th:fragment="MULTICHOICE(decodedArrayOfOption,questionForm)" th:if="${not #lists.isEmpty(decodedArrayOfOption)}">
		<div class="col-sm-4 input-group input-group-sm">
			<span class="input-group-addon">
			<input type="radio" disabled="disabled"/>
			</span>
			<input type="hidden" name="queTypeTextHidden"/>
			<div th:if="${questionForm.id eq null and questionForm.question eq null}">
				<input type="text" name="queTypeText" th:value="${decodedArrayOfOption.get(0).text}" placeholder="Option" required="required" class="form-control"/>
				<input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(0).quantifier}" required="required" class="form-control"/>
			</div>
		    <div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
				 <div th:case="FIXED">
				   <span th:text="${decodedArrayOfOption.get(0).text}" class="form-control"></span>
				   <span th:text="${decodedArrayOfOption.get(0).quantifier}" class="form-control"></span>
				   <input type="hidden" name="queTypeText" th:value="${decodedArrayOfOption.get(0).text}" placeholder="Option" required="required" />
				   <input type="hidden" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(0).quantifier}" required="required" />
			    </div>
			     <div th:case="MODIFIABLE">
          		   <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
          		      <span th:text="${decodedArrayOfOption.get(0).text}" class="form-control"></span>          		           
				      <input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(0).quantifier}" required="required" class="form-control"/>
			          <input type="hidden" name="queTypeText" th:value="${decodedArrayOfOption.get(0).text}" placeholder="Option" required="required" />	
			       </div>
			       <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">		
				     <input type="text" name="queTypeText" th:value="${decodedArrayOfOption.get(0).text}" placeholder="Option" required="required" class="form-control"/>
				     <input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(0).quantifier}" required="required" class="form-control"/>
			       </div>
			    </div>
			</div>
		</div>
		<div th:if="${#lists.size(decodedArrayOfOption) gt 1 }" th:each="optionVar : ${#numbers.sequence(1, #lists.size(decodedArrayOfOption)-1)}" class="col-sm-4 input-group input-group-sm">
		<span class="input-group-addon">
			<input type="radio" disabled="disabled"/>
			</span>
			<input type="hidden" name="queTypeTextHidden"/>
			<div th:if="${questionForm.id eq null and questionForm.question eq null}">
				<input type="text" name="queTypeText" th:value="${decodedArrayOfOption.get(optionVar).text}" placeholder="Option" required="required" class="form-control"/>
				<input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(optionVar).quantifier}" required="required" class="form-control"/>
				<a data-type="removeOption"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
		    </div>
		    <div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
				<div th:case="FIXED">
				  <span th:text="${decodedArrayOfOption.get(optionVar).text}" class="form-control"></span>
				  <span th:text="${decodedArrayOfOption.get(optionVar).quantifier}" class="form-control"></span>
				  <input  type="hidden" name="queTypeText" th:value="${decodedArrayOfOption.get(optionVar).text}" placeholder="Option" required="required"/>
				  <input  type="hidden" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(optionVar).quantifier}" required="required"/>
				</div>
			    <div th:case="MODIFIABLE">
			     <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
			      <span th:text="${decodedArrayOfOption.get(optionVar).text}" class="form-control"></span>				  
				  <input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(optionVar).quantifier}" required="required" class="form-control"/>
				  <input  type="hidden" name="queTypeText" th:value="${decodedArrayOfOption.get(optionVar).text}" placeholder="Option" required="required"/>
				 </div>
			     <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">	
				  <input type="text" name="queTypeText" th:value="${decodedArrayOfOption.get(optionVar).text}" placeholder="Option" required="required" class="form-control"/>
				  <input type="number" min="1" max="10" step=".1" name="quantifiable" th:value="${decodedArrayOfOption.get(optionVar).quantifier}" required="required" class="form-control"/>
				  <a data-type="removeOption"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
			     </div>
			    </div>
			</div>
		</div>
		<div id="getOption"></div>
		<div th:if="${questionForm.id eq null and questionForm.question eq null}">
			<div class="col-sm-4 input-group input-group-sm">
					<span class="input-group-addon">
				<input type="radio" disabled="disabled" />
				</span>
				<input type="text" placeholder="Click to add options" data-type="addOption" readonly="readonly" class="form-control" />
			</div>
			<div>
				<label><input type="checkbox" name="otherOption" th:checked="${otherOption}"/><span th:text="#{question.multichoice.addoption}">Add Option</span></label>
			</div>
			<div>
			    <span th:text="#{question.queTypeText}"></span>
		    </div>
		</div>
		  <div th:if="${questionForm.id ne null or questionForm.question ne null}" th:switch="${questionForm.questionMode}">
			<div th:case="FIXED">
				<div class="col-sm-4 input-group input-group-sm">
					<span class="input-group-addon">
					<input type="radio" disabled="disabled" />
					</span>
					<input type="text" disabled="disabled" placeholder="Click to add options" data-type="addOption" readonly="readonly" class="form-control" />
				</div>
				<div>
					<label><input type="checkbox" disabled="disabled" name="otherOption" th:checked="${otherOption}"/><span th:text="#{question.multichoice.addoption}">Add Option</span></label>
				</div>
				<div>
			        <span th:text="#{question.queTypeText}"></span>
		        </div>
			</div>
			<div th:case="MODIFIABLE">
				<div class="col-sm-4 input-group input-group-sm">
					<span class="input-group-addon">
					<input type="radio" disabled="disabled" />
					</span>
					 <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
					    <input type="text" disabled="disabled" placeholder="Click to add options" data-type="addOption" readonly="readonly" class="form-control" />
				     </div>
				      <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">
					     <input type="text" placeholder="Click to add options" data-type="addOption" readonly="readonly" class="form-control" />
				     </div>
				</div>
				<div>
				   <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
					<label><input type="checkbox" disabled="disabled" name="otherOption" th:checked="${otherOption}"/><span th:text="#{question.multichoice.addoption}">Add Option</span></label>
				   </div>
				    <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">
					<label><input type="checkbox" name="otherOption" th:checked="${otherOption}"/><span th:text="#{question.multichoice.addoption}">Add Option</span></label>
				   </div>
				</div>
				<div>
			      <span th:text="#{question.queTypeText}"></span>
		        </div>
			</div>
		</div>
	</div>	
	<div th:fragment="TEXT">
		<input type="text" class="form-control" disabled="disabled" value="Their answer" />
	</div>
	
	<div th:fragment="PARATEXT">
		<textarea rows="3" class="form-control" disabled="disabled" placeholder="Their longer answer"></textarea>
	</div>
	
	<div th:fragment="MULTIPLECHOICE">
		<div class="col-sm-4 input-group input-group-sm">
				<span class="input-group-addon">
			<input type="radio" disabled="disabled"/>
			</span>
			<input type="hidden" name="queTypeTextHidden"/>
			<input type="text" name="queTypeText" placeholder="Option" required="required" class="form-control"/>
			<input type="number" min="1" max="10" step=".1" name="quantifiable" required="required" class="form-control"/>
		</div>
		<div id="getOption"></div>
		<div class="col-sm-4 input-group input-group-sm">
				<span class="input-group-addon">
			<input type="radio" disabled="disabled" />
			</span>
			<input type="text" placeholder="Click to add options" data-type="addOption" readonly="readonly" class="form-control" />
		</div>
		 <div>
			<label><input type="checkbox" name="otherOption" /><span th:text="#{question.multichoice.addoption}"></span></label>
		</div>
		<div>
			<span th:text="#{question.queTypeText}"></span>
		</div>
	</div>
	<div th:fragment="addOption">
		<div class="col-sm-4 input-group input-group-sm">
			<span class="input-group-addon">
				<input type="radio" disabled="disabled"/>
			</span>
			<input type="hidden" name="queTypeTextHidden"/>
			<input type="text" name="queTypeText" placeholder="Option" required="required" class="form-control"/>
			<input type="number" min="1" max="10" step=".1" name="quantifiable" required="required" class="form-control"/>
			<a data-type="removeOption"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
		</div>
	</div>
	
	<div th:fragment="NUMBER(questionForm)">
		<input type="number" class="form-control" disabled="disabled"/>
		<span th:text="#{question.limitToRange}"></span>
		<div th:if="${questionForm eq null}">
			<div th:replace="question/questionTemplateFragement :: NNUMBER"></div>
		</div>
		<div th:if="${questionForm ne null}" th:switch="${questionForm.questionMode}">
			<div th:case="FIXED">
				<div th:if="${questionForm.id eq null}">
			    	<div th:replace="question/questionTemplateFragement :: NNUMBER"></div>
				</div>
				<div th:if="${questionForm.id ne null}">	
					<div th:replace="question/questionTemplateFragement :: FMNUMBER"></div>
				</div>
			</div>
			<div th:case="MODIFIABLE">
			  <div th:if="${#sets.size(questionForm.parameterFunctions) gt 0}">
			     <div th:replace="question/questionTemplateFragement :: FMNUMBER"></div>
			   </div>
			   <div th:if="${#sets.isEmpty(questionForm.parameterFunctions)}">	
				 <div th:replace="question/questionTemplateFragement :: NNUMBER"></div>
			  </div>
			</div>
			<div th:case="-1">
				<div th:replace="question/questionTemplateFragement :: NNUMBER"></div>
			</div>
		</div>
	</div>	
	<div th:fragment="DATE">
		<input type="date" class="form-control" disabled="disabled"/>
	</div>
	<div th:fragment="FMNUMBER">
		 <input type="checkbox" th:checked="${jsonNumberQuestionMapper.limitTo}" disabled="disabled" />
		 <input type="number" disabled="disabled"  th:value="${jsonNumberQuestionMapper.min}" placeholder="min"/>
		 <input type="number" disabled="disabled"  th:value="${jsonNumberQuestionMapper.max}" placeholder="max"/>
		 <input type="hidden" th:value="${jsonNumberQuestionMapper.limitTo}" name="limitTo"/>
		 <input type="hidden" name="min" th:value="${jsonNumberQuestionMapper.min}" />
		 <input type="hidden" name="max" th:value="${jsonNumberQuestionMapper.max}" />
	</div>
	<div th:fragment="NNUMBER">
		<input type="checkbox" th:checked="${jsonNumberQuestionMapper.limitTo}" name="limitTo"/>
		<input type="number" name="min" th:value="${jsonNumberQuestionMapper.min}" placeholder="min"/>
		<input type="number" name="max" th:value="${jsonNumberQuestionMapper.max}" placeholder="max"/>
	</div>
	
</body>
</html>