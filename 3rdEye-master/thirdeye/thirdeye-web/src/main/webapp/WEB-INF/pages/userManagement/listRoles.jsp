<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.userroles.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.usermanagement.userroles.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.usermanagement.userroles.subtitle}"></span></div> 
	<div class="container" th:fragment="contentContainer">
		       <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
	              	  <div class="table-responsive" data-module="common-data-table">
									<div class="overlay">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
									<table class="table table-bordered table-condensed table-hover" id="userManagementId">
											<thead>
												<tr>
													<th><span >Role Name</span></th>
													<th><span >Role Description</span></th>
													<th><span >Actions</span></th>
												</tr>
											</thead>
											<tbody >
												<tr th:each="oneRole : ${listOfRoles}">
													<td><span th:text="${oneRole.roleName}">Test</span></td>
													<td><span th:text="${oneRole.roleDescription}">Test</span></td>
													<td><a th:href="@{/user/roles/{id}/edit(id=${oneRole.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'USER_ROLE_MODIFY'})"></a>
								        			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}" sec:authorize="@securityService.hasPermission({'USER_ROLE_MODIFY'})"></a></td>
											    </tr>
											</tbody>
									</table>
							</div>
			          </div>
			          <div class="box-footer clearfix">
	                  <!-- <div>
				          <a class="btn btn-primary" th:href="@{/user/roles/create}">Create Role</a>
				        </div> -->
	                </div>
			          </div>
			          </div>
			          </div>
			
	</div>
	
</body>
	</html>