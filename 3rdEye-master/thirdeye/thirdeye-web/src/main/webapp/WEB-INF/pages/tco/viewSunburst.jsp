<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tco.reports.sunburst})">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{tco.reports.sunburst.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{tco.reports.sunburst.subtitle}+${coaName}"></span>
	</div>
	<div th:fragment="contentContainer">		
		<div class="row">
		    <div class="col-md-12">
			   <div class="box box-primary"  data-module="module-viewTCOSunburst">	
					    <div class="box-header with-border">
					         <h3 class="box-title" th:text="${coaName}"></h3>
					         <div class="box-tools pull-right">
					            <i data-type = "tree" th:attr="data-coaid=${chartOfAccountId}" class="fa fa-tree treeicon clickable" th:title="${coaName} + #{icon.title.treeview}"></i>
						   	 </div>
						   	  <div class="box-tools pull-right">
					            <a th:href="@{/tco/{id}/response/exportCOA(id=${chartOfAccountId})}" class="fa fa-file-excel-o clickable excelDownload" th:title = "${coaName} + #{icon.title.excel}" ></a>
						   	 </div>	  
					    </div>
						<div class="box-body" style="font-family: sans-serif;">
						<div id="breadcumb"></div>
						<div id="sunburst" class="clickable" style="height: 40em; width: 100%; overflow-x:scroll; white-space: nowrap;" th:attr="data-coaid=${chartOfAccountId}"></div>
				   </div>
				   <script type="text/x-config" th:inline="javascript">{"pngimagename":"[[${coaName}]]"}</script>
				   <div id="assetDetail-container"  >
							<div id="tcoasset" class="tcoasset" style="height: 25em; width: 100%; overflow-x:scroll;white-space: nowrap"></div>
						</div>
	         </div>
	      </div>
	  </div>	
	  <div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;"></div>
	  </div>
	  
	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/v3/d3.v3.min.js}"></script>		
		<script th:src="@{/static/js/3rdEye/modules/module-viewTCOSunburst.js}"></script>
	</div>
	
</body>

</html>