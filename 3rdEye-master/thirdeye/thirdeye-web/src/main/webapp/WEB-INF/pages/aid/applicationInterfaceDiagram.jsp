<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.viewaid.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>AID Report</title>
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.reports.viewaid.aidreportview.title}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
</div>


	<div th:fragment="contentContainer">

		<div>
			<!-- Filter Implementation -->
			<div class="row" style="padding-bottom: 20px; padding-left: 20px;">
				<div style="padding-top: 15px;" data-module="module-filterTags">
					<div class="col-md-12">
						<div id="tags"></div>
					</div>
				</div>
			</div>

			<aside class="control-sidebar control-sidebar-dark"
				style="margin-top: 101px; padding-top: 0px;">
				<div class="tab-content" style="height: 39em; overflow-y: auto;">
					<div id="control-sidebar-theme-demo-options-tab"
						class="tab-pane active">
						<div style="text-align: center; text-decoration: underline;">
							<h4 th:text="#{filter.panel}"></h4>
						</div>
						<div data-module="module-assetTypeFacet"></div>
						<div data-module="module-facetUpdate"></div>
					</div>
					<!-- /.tab-pane -->
				</div>
			</aside>
			<!-- Filter Implementation End -->

			<div class="appId" th:attr="data-aid=${aid}" data-module="module-applicationInterfaceDiagram"></div>
			<div class="page-break"></div>

		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		  	<script	th:src="@{/static/js/ext/html2canvas/0.5.0-alpha1/html2canvas.min.js}"></script>
		  	<script	th:src="@{/static/js/3rdEye/modules/module-applicationInterfaceDiagram.js}"></script>
		</div>
</body>
</html>