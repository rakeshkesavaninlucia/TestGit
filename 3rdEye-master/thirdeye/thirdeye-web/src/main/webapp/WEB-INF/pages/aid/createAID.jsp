<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.createaid.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pages.reports.createaid.title}"></span></div>
	<div class="container"  th:fragment="contentContainer">
		<form th:action="@{/aid/save}" method="post" th:object="${aid}">
				<div class="form-group" data-module="module-createAID">					
					<span>AID Name :<span style="color: red;">*</span></span> <input type="text" th:field="*{name}" class="form-control" />
					<span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" style="color: red;">Test</span> 
					<br/>
					<span>Description :</span>
					<textarea rows="3" class="form-control" th:field="*{description}"></textarea>
					<br/>
					<span>Asset Template :<span style="color: red;">*</span></span>
					<select name="assetTemplateId" class="form-control select2">
						<option value="-1">--Select--</option>
						<option th:each="assetTemplate : ${listOfAssetTemplate}"
						 th:value="${assetTemplate.id}" th:text="${assetTemplate.assetTemplateName}"></option>
					</select>
					<br/>
					<span th:if="${#fields.hasErrors('assetTemplate')}" th:errors="*{assetTemplate}" style="color: red;">Test</span>
					<br/>
					<input class="btn btn-primary" type="submit" value="Create AID"/>
				</div>
		</form>		
	</div>
	<div th:fragment="scriptsContainer">
  		<script th:src="@{/static/js/3rdEye/modules/module-createAID.js}"></script>
	</div>	
</body>
</html>