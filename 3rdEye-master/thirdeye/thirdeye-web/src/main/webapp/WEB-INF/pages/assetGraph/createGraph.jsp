<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.assetgraph.creategraph.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>
<body>
	<div th:fragment="pageTitle">
		<div th:switch="${graph.id}">
			<span th:case="null" th:text="#{pages.reports.assetgraph.creategraph.title}"></span>
			<span th:case="*" th:text="#{pages.reports.assetgraph.updategraph.title}"></span>
		</div>
	</div>
	
	<div class="container"  th:fragment="contentContainer">
		<div th:replace="assetLinkGraphTemplate/editAssetLinkFragments :: createGraphForm"></div>		
	</div>
</body>
</html>