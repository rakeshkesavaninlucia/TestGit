<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.colorscheme.qualitygates.qgfrag.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style th:fragment="onPageStyles">
</style>
<title>Fragments for Quality Gates</title>
</head>
<body>
	<div th:fragment="createQualityGate">
		<div class="modal fade" id="createQualityGateModal" tabindex="-1"
			role="dialog" aria-labelledby="createQualityGateModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form th:action="@{/qualityGates/save}" method="post"
						id="createQualityGateForm" th:object="${qualityGateForm}">
						<input type="hidden" th:field="*{id}" />
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title"
								th:text="#{color.quality.gates.modal.title}"></h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="name" class="control-label"
									th:text="#{color.quality.gates.name}"></label> <span
									class="asterisk">*</span> <input type="text"
									class="form-control" th:field="*{name}" /> <span
									th:if="${#fields.hasErrors('name')}" th:errors="*{name}"
									class="error_msg">Test</span>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit"
								th:value="*{id != null }? 'Edit Quality Gate' : 'Create Quality Gate'"
								class="btn btn-primary"></input>
						</div>
					</form>
				</div>                          
			</div>
		</div>
	</div>
	<tbody>
		<tr th:fragment="colorDescription" class="dirtyrow colorDescription" data-type="colorDescription">
		     <td class="col-md-2 horizontal">
		         <label th:text="#{color.quality.gates.colorDescription}"></label>
		     </td>
		     <div class ="description" th:each="oneColor : ${colorScale}">
			 <td class="col-md-6 horizontal" th:style="'text-align:center; background:'+${oneColor}+'; width:84px;padding:.5em;'">
				 <input type="hidden" th:value="${oneColor}" name="descriptionColor" /> 
				 <input style="width: 60px;" type="text" value="" name="description"  />
			</td>
			</div>
			<td class="col-md-2 horizontal">
		   </td>
			<td class="col-md-2 horizontal">
			    <a class="saveRow fa fa-check-square-o" title="Save" data-type="saveDescription"></a>
		   </td>
		</tr>
	</tbody>
	<tbody>
		<tr th:fragment="viewDescription" class="dirtyrow colorDescription" >
		     <td class="col-md-2 horizontal">
		         <label th:text="#{color.quality.gates.colorDescription}"></label>
		     </td>
		    <div th:each="oneColor : ${qualityGateDescription}">
				<td class="col-md-6 horizontal" th:style="'text-align:center; background:'+${oneColor.color}+'; width:84px;padding:.5em;'">
					<span th:text="${oneColor.description}"></span>
				</td>
			</div>
			<td class="col-md-2 horizontal">
		   </td>
			<td class="col-md-2 horizontal">
			  <a class="editRow fa fa-pencil-square-o" title="Edit" style="cursor: pointer;" data-type="editDescription"></a> 
		   </td>
		</tr>
	</tbody>
	<tbody>
		<tr th:fragment="editDescription" class="dirtyrow colorDescription"  >
		     <td class="col-md-2 horizontal">
		         <label th:text="#{color.quality.gates.colorDescription}"></label>
		     </td>
		    <div class ="description" th:each="oneColor : ${qualityGateDescription}">
				<td class="col-md-6 horizontal" th:style="'text-align:center; background:'+${oneColor.color}+'; width:84px;padding:.5em;'">
					<input type="hidden" th:value="${oneColor.color}" name="descriptionColor"  />
					<input style="width: 60px;" type="text" 
					th:value="${oneColor.description}" name="description" />
				</td>
			</div>
			<td class="col-md-2 horizontal">
		   </td>
			<td class="col-md-2 horizontal">
			  <a class="saveRow fa fa-check-square-o" title="Save" data-type="saveDescription"></a>
		   </td>
		</tr>
	</tbody>
	
	<span th:fragment="descriptionError" class="errorCondition error_msg" th:if="${descriptionError ne null}" th:text="${descriptionError}"></span>

	<tbody>
		<tr th:fragment="createRow" class="dirtyrow"
			th:object="${qualityGateCondition}">
			<input type="hidden" th:field="*{qualityGateId}" />
			<td class="col-md-2 horizontal"><span
				th:text="${parameter.displayName}"></span></td>
			<div th:if="${qualityGateCondition.valueMapper eq null}"
				th:each="oneColor,iterStat : ${colorScale }">
				<td class="col-md-6 horizontal"
					th:style="'text-align:center; background:'+${ oneColor}+'; width:84px;padding:1em;'">
					<input type="hidden" th:value="${oneColor }" name="conditionColor" />
					<input th:if="${#lists.size(colorScale) ne iterStat.index+1}"
					style="width: 60px;" type="number" value="" name="conditionValue" />
				</td>
			</div>
			<div th:if="${qualityGateCondition.valueMapper ne null}"
				th:each="oneColor,iterStat : ${qualityGateCondition.valueMapper }">
				<td
					th:style="'text-align:center; background:'+${ oneColor.color}+'; width:84px;padding:1em;'">
					<input type="hidden" th:value="${oneColor.color }"
					name="conditionColor" /> <input
					th:if="${#lists.size(qualityGateCondition.valueMapper) ne iterStat.index+1}"
					style="width: 60px;" type="number" th:value="${oneColor.value }"
					name="conditionValue" />
				</td>
			</div>
			<td class="col-md-2 horizontal" th:if="${qualityGateCondition.weighted}"><span
				th:text="#{color.quality.gates.weight }"></span>&nbsp;<input
				style="width: 60px;" type="number" max="1" min="0" step="0.1"
				th:field="*{weight}" required="required" placeholder="0" /> <span
				th:if="${#fields.hasErrors('weight')}" th:errors="*{weight}"
				class="error_msg">Test</span></td>
			<td class="col-md-2 horizontal"><a class="saveRow fa fa-check-square-o" title="Save"
				data-type="saveRow"></a></td>
		</tr>

		<tr th:fragment="editRow" class="dirtyrow"
			th:object="${qualityGateCondition}">
			<input type="hidden" th:field="*{qualityGateId}" />
			<input type="hidden" th:field="*{id}" />
			<td><span th:text="${qualityGateCondition.parameterName}"></span></td>
			<div
				th:each="oneValue,iterStat : ${qualityGateCondition.valueMapper }">
				<td
					th:style="'text-align:center; background:'+${ oneValue.color}+'; width:84px;padding:1em;'">
					<input type="hidden" th:value="${oneValue.color }"
					name="conditionColor" /> <input
					th:if="${#lists.size(qualityGateCondition.valueMapper) ne iterStat.index+1}"
					style="width: 60px;" type="number" th:value="${oneValue.value }"
					name="conditionValue" />
				</td>
			</div>
			<td class="col-md-2 horizontal" th:if="${qualityGateCondition.weighted}"><span
				th:text="#{color.quality.gates.weight }"></span>&nbsp;<input
				style="width: 60px;" type="number" max="1" min="0" step="0.1"
				th:field="*{weight}" required="required" placeholder="0" /> <span
				th:if="${#fields.hasErrors('weight')}" th:errors="*{weight}"
				class="error_msg">Test</span></td>
			<td class="col-md-2 horizontal"><a class="saveRow fa fa-check-square-o" title="Save"
				data-type="saveRow"></a></td>
		</tr>
	</tbody>

	<tbody th:fragment="viewOnlyCondition" th:remove="tag">
		<tr th:id="${qgCondition.id}">
			<input type="hidden" th:value="${qgCondition.id }" />
			<td class="col-md-2 horizontal"><span
				th:text="${qgCondition.parameterName}">Test</span></td>
			<div th:each="oneValue : ${qgCondition.valueMapper }">
				<td class="col-md-6"
					th:style="'text-align:center; background:'+${oneValue.color}+'; width:84px;padding:1em;'">
					<span th:text="${oneValue.value}"></span>
				</td>
			</div>
			<td class="col-md-2 horizontal" th:if="${qgCondition.weighted}"><span
				th:text="#{color.quality.gates.weight }"></span>&nbsp;-&nbsp;<span
				th:text="${qgCondition.weight }"></span></td>
			<td class="col-md-2 horizontal"><a class="editRow fa fa-pencil-square-o"
				title="Edit" style="cursor: pointer;" data-type="editRow"></a> <a
				class="deleteRow  fa fa-trash-o" th:if="${qgCondition.weighted}"
				th:title="#{icon.title.delete}" style="cursor: pointer;"
				data-type="deleteRow"></a></td>
		</tr>
	</tbody>

	<span th:fragment="parameterError" class="errorParam error_msg"
		th:if="${paramError ne null}" th:text="${paramError}"></span>

	<span th:fragment="conditionError" class="errorCondition error_msg"
		th:if="${conditionError ne null}" th:text="${conditionError}"></span>

	<div class="row" th:fragment="parameterColor">
		<div th:if="${qualityGateId ne null}" class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Parameter Color</h3>
				</div>
				<div class="box-body" data-module="module-qualityGateCondition">
					<table class="table table-bordered table-condensed table-hover">
						<tr>
							<input type="hidden" th:value="${paramId}" id="parameterId" />
							<input type="hidden" th:value="${qualityGateId}" id="qualityGate" />
							<td class="col-md-2 horizontal"><label for="colorScale"
								class="control-label required"
								th:text="#{color.quality.gates.colorScale}"></label></td>
							<td><select class="form-control" id="colorScale"
								data-type="colorScaleSelected">
									<option value=""></option>
									<option
										th:each="oneScale : ${T(org.birlasoft.thirdeye.colorscheme.constant.ParameterColorScale).values()}"
										th:value="${oneScale.value}" th:text="${oneScale.value}"
										th:selected="${oneScale.value == colorScaleCount}"></option>
							</select></td>
							<td class="col-md-2 horizontal"><label
								class="control-label required"
								th:text="#{color.quality.gates.condition}"></label></td>
							<td><span
								th:text="${T(org.birlasoft.thirdeye.colorscheme.constant.Condition).LESS.value}"></span></td>
						</tr>
					</table>
					<table class="table table-bordered table-condensed table-hover">
						<tbody>
						    <tr class="colorDescription"></tr>
						</tbody>
					</table>
					<table class="table table-bordered table-condensed table-hover"
						id="qualityGateCondition">
						<tbody>
							<tr th:each="oneCondition : ${listOfConditions}"
								th:include="qualityGates/qualityGateFragments :: viewOnlyCondition"
								th:with="qgCondition=${oneCondition}" />
						</tbody>
					</table>
					<div id="descriptionError"></div>
					<div id="conditionError"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
