
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>
<body>
<table>
	<tbody th:fragment="viewOnlyTemplate" th:remove="tag" >
		<tr th:if="${not T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTypeName) and sequenceNumber eq 1}" th:id="${colid}" class="nodrag nodrop">
			<td><span th:text="${assetTemplateColName}">Test</span>
			<span class="mandatory" th:text="#{mandatory.column}">test</span></td>
			<td><span th:text="${dataType}">Test</span></td>
			<td><span th:text="${length}">Test</span></td>
			<td style="text-align: center; "><input type="checkbox" th:checked="${mandatory}" disabled="disabled"></input></td>
			<td sec:authorize="@securityService.hasPermission({'TEMPLATE_MODIFY'})"><a th:if="${sequenceNumber gt 1}" class="editRow fa fa-pencil-square-o" title="Edit" style="cursor: pointer;"></a>
			<a th:if="${sequenceNumber gt 1}" class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a></td>
		</tr>
		<tr th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTypeName) or sequenceNumber ne 1}" th:id="${colid}">
			<td><span th:text="${assetTemplateColName}">Test</span></td>
			<td><span th:text="${dataType}">Test</span></td>
			<td><span th:text="${length}">Test</span></td>
			<td style="text-align: center; "><input type="checkbox" th:checked="${mandatory}" disabled="disabled"></input></td>
			<td style="text-align: center; "><input type="checkbox" th:checked="${filterable}" disabled="disabled"></input></td>
			<td sec:authorize="@securityService.hasPermission({'TEMPLATE_MODIFY'})"><a th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTypeName) or sequenceNumber gt 1}" class="editRow fa fa-pencil-square-o" id="editButton" title="Edit" data-type="edittype" style="cursor: pointer;"></a>
			<a th:if="${T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTypeName) or sequenceNumber gt 1}" class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a></td>
		</tr>
	</tbody>
<tbody>
	<tr th:fragment="editTemplate(oneRow)" class="dirtyrow" th:id="${oneRow.id}" th:object="${assetTemplateColBean}">
			<td><input type="text" name="assetTemplateColName" th:value="${oneRow.assetTemplateColName}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('assetTemplateColName')}" th:errors="*{assetTemplateColName}" class="error_msg">Test</span>
			</td>
			<td><input type="hidden" name="dataType" id="datatype" th:value="${oneRow.dataType}"/><span th:text="${oneRow.dataType}">Test</span></td>
			<td><input type="number"  step="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="lengthField" data-type="lengthType" name="length" th:value="${oneRow.length}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('length')}" th:errors="*{length}" class="error_msg">Test</span>
			</td>
			<td style="text-align: center; "><input type="checkbox" th:name="mandatory" th:checked="${oneRow.mandatory}" ></input></td>
			<td style="text-align: center; "><input type="checkbox" th:name="filterable" th:checked="${oneRow.filterable}"></input></td>
			<td><a class="saveRow fa fa-check-square-o" title="Save" ></a>
			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}"></a></td>
	</tr>
	
	<tr th:fragment="createRow" class="dirtyrow" th:object="${assetTemplateColBean}">
		<td><input type="text" th:field="*{assetTemplateColName}"></input><span class="asterisk">*</span>
		<span th:if="${#fields.hasErrors('assetTemplateColName')}" th:errors="*{assetTemplateColName}" class="error_msg">Test</span></td>
		
		<td><select th:field="*{dataType}" data-type="selecttype" id = "dropdown">
				<option value="select">--Select--</option>
				<option th:each="dataType : ${T(org.birlasoft.thirdeye.constant.DataType).values()}"
				        th:value="${dataType}" th:text="${dataType.value}">
				</option>
			</select><span class="asterisk">*</span>
		<span th:if="${#fields.hasErrors('dataType')}" th:errors="*{dataType}" class="error_msg">Test</span>
		</td>
		<td><input type="number" step="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" th:field="*{length}" data-type="lengthType" ></input><span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('length')}" th:errors="*{length}" class="error_msg">Test</span>
		</td>
		<td style="text-align: center; "><input type="checkbox" name="mandatory"></input></td>
		<td style="text-align: center; "><input type="checkbox" name="filterable"></input></td>
		<td><a class="saveRow fa fa-check-square-o" title="Save"></a></td>

	</tr>
	
</tbody>
</table>
	<div th:fragment="createTemplateForm">
		<form th:action="@{/templates/save}" method="post" th:object="${assetTemplateBean}">
				<input type="hidden" th:field="*{id}" />
				<input th:if="${assetTemplateBean.id ne null}" type="hidden" th:field="*{assetTypeId}" />
				<div class="form-group">
					<span th:text="#{asset.type}"></span>
					<span th:if="${assetTemplateBean.id ne null}" th:text="${assetTemplateBean.assetType.assetTypeName}"></span>
					<select th:if="${assetTemplateBean.id eq null}" id="assetTypeListId" th:field="*{assetTypeId}" class="form-control select2" data-type="assetTypeRelationship">
						<option value="-1">--Select--</option>
						<option th:each="assetType : ${assetTypeList}" th:value="${assetType.id}" th:text="${assetType.assetTypeName}" />
					</select>
					<br/>
					<span th:if="${#fields.hasErrors('assetTypeId')}" th:errors="*{assetTypeId}" style="color: red;">Test</span>
					<br/>
					<span th:text="#{asset.template}"></span> <input type="text" th:field="*{assetTemplateName}"  class="form-control" /> 
					<span th:if="${#fields.hasErrors('assetTemplateName')}" th:errors="*{assetTemplateName}" style="color: red;">Name Error</span>
					<br/>
					<span th:text="#{asset.desc}"></span>
					<textarea rows="3" class="form-control" th:field="*{description}"></textarea>
					<br/>
					<div class="relationshipTemplateContainer"><div th:replace="relationship/relationshipTemplateFragments :: createRelationshipTemplate"></div></div>
					<input class="btn btn-primary" type="submit" value="SAVE"/>
				</div>
		</form>
	</div>
	
	<div th:if="${#sets.size(assetTemplateModel.assetTemplateColumns) gt 0 or T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplateModel.assetType.assetTypeName)}" th:fragment="populateAssetDataForm">
		<form th:action="@{/templates/form/save}" method="post">
		<input type="hidden" name="templateId" th:value="${assetTemplateModel.id}" />
			<div class="form-group" data-module="module-assetDataEntry">
			<div >
			<a class="fa fa-chevron-left btn btn-default" th:href="@{/templates/data/view/{id}(id=${assetTemplateModel.id})}" style="margin-left: 10px;"></a>
			<input class="btn btn-primary" style="float: right;" type="submit" value="Save And Add More" id="submitButton" th:if="${assetTemplateModel.assetTemplateColumns ne null}"/>
			</div>
				<table class="table table-bordered table-condensed table-hover">				
					<tr th:each="templatecol : ${assetTemplateModel.assetTemplateColumns}">
						<td class="col-md-4 horizontal"><span th:text="${templatecol.assetTemplateColName}"></span></td>
						<td class="col-md-8">
							<input th:if="${templatecol.dataType ne T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" th:type="${templatecol.dataType}" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" ></input>
							<input th:if="${templatecol.dataType eq T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" type="date" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" ></input>
						</td>
						</tr>
				</table>
				<span th:if="${error eq 1}" style="color: red;">Name already exists</span>
				
				<table th:if="${listOfParentAssets ne null and listOfChildAssets ne null}" class="table table-bordered table-condensed table-hover">
					<tr>
						<td class="col-md-4 horizontal"><span>Parent Asset</span></td>
						<td class="col-md-8">
							<select class="form-control select2 parentassetdropdown" name="parentAsset" required="required">
								<option value="">--Select--</option>
					          	<option th:each="oneAsset : ${listOfParentAssets}" th:value="${oneAsset.id}" th:text="${oneAsset.assetTypeName} + ' :: ' + ${oneAsset.shortName}" />
					        </select>
						</td>
					</tr>
					<tr>
						<td class="col-md-4 horizontal"><span>Child Asset</span></td>
						<td class="col-md-8">
							<select class="form-control select2 childassetdropdown" name="childAsset" required="required">
								<option value="">--Select--</option>
					          	<option th:each="oneAsset : ${listOfChildAssets}" th:value="${oneAsset.id}" th:text="${oneAsset.assetTypeName} + ' :: ' + ${oneAsset.shortName}" />
					        </select>
					        <br/>
					        <div id="errorParentChild" style="display: none;">Child asset name cannot be same as Parent asset</div>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
	
	<div th:fragment="exportImportInventory">
		<div class="modal fade" id="exportImportInventoryModal" tabindex="-1" role="dialog" aria-labelledby="exportImportInventoryModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" th:text="#{export.modal.title.inventory}">Test</h4>
					</div>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs" style="width: 100%">
							<li th:class="${activeTab == 'exportTab' ? 'active' : ''} ">
								<a href="#selectExport" data-toggle="tab" aria-expanded="true" th:text="#{export.tab}"> Export </a>
							</li>
							<li th:class="${activeTab == 'importTab' ? 'active' : ''} ">
								<a href="#selectImport" data-toggle="tab" aria-expanded="false" th:text="#{import.tab}"> Import </a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" th:classappend="${activeTab == 'exportTab' ? 'active' : ''} " id="selectExport">
								<h3 th:text="#{export.tab.heading}">Test</h3>
								<a class="btn btn-primary" th:href="@{/templates/{id}/exportTemplate(id=${templateId})}" th:text="#{export.button}"></a>
								<h4>Please Note:</h4>
								<ol>
									<li>Only you can upload from this template.</li>
									<li>Please do not make any modifications to the structure of this file.</li>
								</ol>
							</div><!-- /.tab-pane -->
							<div class="tab-pane" th:classappend="${activeTab == 'importTab' ? 'active' : ''} " id="selectImport">
								<h3 th:text="#{import.tab.heading}">Test</h3>
								<form th:action="@{/templates/{id}/import?_csrf={_csrf.token}(id=${templateId},_csrf.token=${_csrf.token})}" method="post" id="importInventoryForm" enctype="multipart/form-data">
									<input type="file" name="fileImport" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
									<input type="submit" value="Upload"/>
									<div th:if="${activeTab == 'importTab'}">
										<div th:if="${#lists.isEmpty(listOfErrors)}">
											<div class="alert alert-success alert-dismissable">
												<h4><i class="icon fa fa-check"></i> Alert!</h4>
												<span th:text="#{import.success}"></span>
											</div>
										</div>
										<div th:if="${not #lists.isEmpty(listOfErrors)}">
											<div th:replace="assetTemplate/editTemplateFragments :: importInventoryError"></div>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div th:fragment="importInventoryError">
		<div class="alert alert-danger alert-dismissable">
			<h4><i class="icon fa fa-ban"></i> Alert!</h4>
			<ul th:each="errorCell : ${listOfErrors}">
				<li><span th:if="${errorCell.row ne null and errorCell.column ne null}">Column: <span th:text="${errorCell.column }"></span> Row: <span th:text="${errorCell.row }"></span></span></li>
				<ul th:each="error : ${errorCell.errors}">				
					<li><span th:text="${error}"></span></li>
				</ul>
				
			</ul>
		</div>
	</div>
</body>
</html>