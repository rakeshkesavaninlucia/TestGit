<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Fragments for dashboards</title>
</head>
<body>
	<div th:fragment="createNewDashboard">
		<div class="modal fade" id="createNewDashBoardModal" tabindex="-1" role="dialog" aria-labelledby="createNewDashBoardModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/dashboard/create/save}" method="post" th:object="${dashboardForm}"  id="createNewDashBoardForm">
								<input type="hidden" th:field="*{id}"/>
								
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title">Create / Edit Dashboard</h4>
						      </div>
						      <div class="modal-body">
						          <div class="form-group">
						            <label for="dashboardName" class="control-label">Name:</label>
						            <input type="text" class="form-control" th:field="*{dashboardName}" />
			            			<span th:if="${#fields.hasErrors('dashboardName')}" th:errors="*{dashboardName}" style="color: red;">Test</span>
						          </div>
						          <!-- <div class="form-group">
						            <label for="parameter-description" class="control-label">Description:</label>
						            <textarea class="form-control"></textarea>
						          </div> -->
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Edit Dashboard' : 'Create Dashboard'" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
	
	<div th:fragment="addNewWidgets">
		<div class="modal fade" id="addNewWidgetsModal" tabindex="-1" role="dialog" aria-labelledby="addNewWidgetsModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/dashboard/addWidget}" method="post" th:object="${addNewWidgetsForm}"  id="addNewWidgetsForm">
							<input type="hidden" th:field="*{id}"/>
							<input type="hidden" th:field="*{dashboardId}"/>
								
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title">Add a widget to your dashboard</h4>
						      </div>
						      <div class="modal-body">
						          <div class="form-group">
						            <label for="title" class="control-label">Name for your widget:</label>
						            <input type="text" class="form-control" th:field="*{title}" />
			            			<span th:if="${#fields.hasErrors('title')}" th:errors="*{title}" style="color: red;">Test</span>
						          </div>
						          
						          <div class="form-group">
						            <label for="dashboardName" class="control-label">Please select the type of widget</label>
						            <div class="radio" th:each="elem: ${T(org.birlasoft.thirdeye.constant.WidgetTypes).values()}">
							             <label >
							             <input type="radio" th:field="*{widgetType}" th:value="${elem.name()}" th:text="${elem.description}"/>
							             </label>
							           </div>
						          </div>
						          
						      </div>
						      <div class="modal-footer">
						        <input type="submit" value="Add Widget" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
</body>
</html>