<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.waveanalysis.nav})">
<head>
<meta charset="utf-8" />
<title></title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<style th:fragment="onPageStyles">
#chart svg {
	height: 400px;
}
</style>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.waveanalysis.title}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.waveanalysis.subtitle}"></span>
	</div>
	<div th:fragment="contentContainer">
		<div class="row" style="padding-bottom: 20px; padding-left: 20px;">
			<div style="padding-top: 15px;" data-module="module-filterTags">
				<div class="col-md-12">
					<div id="tags"></div>
				</div>
			</div>
		</div>

		<aside class="control-sidebar control-sidebar-dark" style="margin-top: 101px; padding-top: 0px;">
			<div class="tab-content" style="height: 39em; overflow-y: auto;">
				<div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
					<div style="text-align: center; text-decoration: underline;">
						<h4 th:text="#{filter.panel}"></h4>
					</div>
					<div data-module="module-assetTypeFacet"></div>
					<div data-module="module-facetUpdate"></div>
				</div>
			</div>
		</aside>
		
		<div data-module="module-viewWaveAnalysis">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div th:replace="scatter/scatterGraphFragment :: waveFragment"></div>
							<div id="waveAnalysis" style="display:none;">
							   <div th:replace="waveAnalysis/waveAnalysisFragment :: wavePointFragment"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary scatterPlot" >
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body chart-responsive">
							<div id="chart" class='with-3d-shadow with-transitions'>
								<svg>
								 
                    			</svg>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewWaveAnalysis.js}"></script>
	</div>

</body>
</html>