<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.functionalmaps.create.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<a class="pull-left fa fa-chevron-left btn btn-default" th:href="@{/fm/view}" style="margin-left: 10px;"></a>
		<div th:switch="${functionalMapForm.id}">
			<span th:case="null" th:text="#{functionalMap.create}"></span>
			<span th:case="*" th:text="#{functionalMap.update}"></span>
		</div>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
	                <div class="box-body" data-module="module-functionalMap">
	                	<form th:action="@{/fm/save}" method="post" id="createFunctionalMapForm" th:object="${functionalMapForm}">
	                		<input type="hidden" th:field="*{id}" />
	                		<input type="hidden" th:field="*{workspaceId}" />
	                		<div class="form-group">
	                			<table class="table table-bordered table-condensed table-hover">
	                				<tr>
	                					<td class="col-md-2 horizontal"><label for="name" class="control-label required" th:text="#{functionalMap.name}"></label></td>
	                					<td colspan="3">
	                						<input type="text" th:field="*{name}" class="form-control" />
					            			<span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" class="error_msg">Test</span>
	                					</td>
	                				</tr>
	                				<tr>
	                					<td class="col-md-2 horizontal"><label for="bcmId" class="control-label required" th:text="#{functionalMap.bcm}"></label></td>
	                					<td colspan="3">
	                						<select th:field="*{bcmId}" class="form-control dropdown_select">
												<option value=""></option>
												<option th:each="oneBcm : ${listOfBcms}" th:value="${oneBcm.id}" th:text="${oneBcm.bcmName}" />
											</select>
					            			<span th:if="${#fields.hasErrors('bcmId')}" th:errors="*{bcmId}" class="error_msg">Test</span>
	                					</td>
	                				</tr>
	                				<tr>
	                					<td class="col-md-2 horizontal"><label for="assetTemplateId" class="control-label required" th:text="#{functionalMap.asset.template}"></label></td>
	                					<td class="col-md-4">
	                						<select th:field="*{assetTemplateId}" class="form-control dropdown_select">
	                							<option value=""></option>
												<optgroup th:each="assetType : ${mapOfAssetTemplate} " th:label="${assetType.key.assetTypeName }">
													<option th:each="oneAssetTemplate : ${assetType.value}" th:value="${oneAssetTemplate.id}" th:text="${oneAssetTemplate.assetTemplateName}" />
												</optgroup>
											</select>
					            			<span th:if="${#fields.hasErrors('assetTemplateId')}" th:errors="*{assetTemplateId}" class="error_msg">Test</span>
	                					</td>
	                					<td class="col-md-2 horizontal"><label for="type" class="control-label required" th:text="#{functionalMap.type}"></label></td>
	                					<td class="col-md-4">
	                						<select th:field="*{type}" class="form-control dropdown_select">
												<option value="" selected="selected"></option>
												<option th:each="oneType : ${T(org.birlasoft.thirdeye.constant.FunctionalMapType).values()}" th:value="${oneType}" th:text="${oneType}"></option>
											</select>
					            			<span th:if="${#fields.hasErrors('type')}" th:errors="*{type}" class="error_msg">Test</span>
	                					</td>
	                				</tr>
	                				<tr>
	                					<td class="col-md-2 horizontal"><label for="questionId" class="control-label required" th:text="#{functionalMap.question}"></label></td>
	                					<td colspan="3">
	                						<select th:field="*{questionId}" class="form-control dropdown_select">
												<option value=""></option>
												<optgroup th:each="quesCat : ${mapOfQuestions} " th:if="${quesCat.key eq null }">
													<option th:each="oneQues : ${quesCat.value}" th:value="${oneQues.id}" th:text="${oneQues.title}" />
												</optgroup>
												<optgroup th:each="quesCat : ${mapOfQuestions} " th:if="${quesCat.key ne null }" th:label="${quesCat.key.name }">
													<option th:each="oneQues : ${quesCat.value}" th:value="${oneQues.id}" th:text="${oneQues.title}" />
												</optgroup>
											</select>
					            			<span th:if="${#fields.hasErrors('questionId')}" th:errors="*{questionId}" class="error_msg">Test</span>
	                					</td>
	                				</tr>
	                			</table>
	                		</div>
	                		<input class="btn btn-primary" type="submit" value="SAVE"/>
	                	</form>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script th:src="@{/static/js/3rdEye/modules/module-functionalMap.js}"></script>
	</div>
</body>
</html>