<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = ${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<div th:fragment="pageTitle">
	<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.questionnaire.submitresponse.title}"></span>
    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.tco.submitresponse.title}"></span>
</div>
<div th:fragment="pageSubTitle"><span ></span></div>
<body>
	<div class="container" th:fragment="contentContainer">
		<div class="box box-primary">
            <div class="box-body">
	             <div class="table-responsive" data-module="common-data-table">
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
		            <table class="table table-bordered">
						<thead>
							<tr>
								<th><span th:text="#{questionnaire.name.table}"></span></th>
								<th><span th:text="#{questionnaire.description.table}"></span></th>
								<th><span th:text="#{questionnaire.progress.table}"></span></th>
								<th><span th:text="#{tabel.header.action}"></span></th>
							</tr>
						</thead>
						<tbody>
							<tr th:each="oneQuestionnaire : ${listOfQuestionnaires}">
								<td><span th:text="${oneQuestionnaire.name}"></span></td>
								<td><span th:text="${oneQuestionnaire.description}"></span></td>
								<td>
									<div class="progress progress-xs progress-striped">
			                          <div class="progress-bar progress-bar-primary" th:style="'width:'+ ${completionPercentage[oneQuestionnaire.id]} + '%'"></div>
			                        </div>
		                        </td>
		                        <td data-module="module-exportImportCOA">
		                        	<a th:href="@{/{action}/{id}/response(id=${oneQuestionnaire.id},action=${action})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO','QUESTIONNAIRE_RESPOND'})"></a>&nbsp;	
		                        	<a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" class="fa fa-download exportImport clickable" data-type="exportImportCOA" th:attr="data-coaid=${oneQuestionnaire.id}" th:title="#{icon.title.export/import}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"></a>	                        	
		                        </td>
							</tr>					
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>	
	<div th:fragment="scriptsContainer"  th:remove="tag">
     	<script	th:src="@{/static/js/3rdEye/modules/module-exportImportCOA.js}"></script>      
    </div>
</body>
</html>