<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='editRoles',dataAction='',pageTitle = #{pages.usermanagement.editrole.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="${role.roleName}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.usermanagement.editrole.subtitle} + ' Role :'+${role.roleName}"></span></div> 
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
					<form role="form" th:action="@{/user/roles/save}" method="post" id="scatterGraphControl">
						<input type="hidden" name="roleId" id="roleId" th:value="${role.id}" />
						<div class="box-body">
							<div class="checkbox" th:each="onePermission : ${systemPermissions}">
					          <label>
					            <input type="checkbox" th:checked="${rolePermissionMap.containsKey(onePermission.name())}" class = "editableParameter" th:name="permissionsCheckbox" th:value="${onePermission.name()}" th:text="${onePermission.permissionDescription}"/>
					          </label>
					        </div>
						</div>
						<div class="box-footer clearfix">
							<div class="pull-right">
								<button type="submit" class="btn btn-primary">Update Permissions</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>

</body>
</html>