
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="wavePointFragment">
	
		<div class="row">
		  <table class="table table-bordered table-condensed table-hover row">
		      <tr>
		        <td class="col-md-2 horizontal"></td>
		        <td class="col-md-5">
		           <div>	
		             <label>Point 1 (x,y)</label>
				   </div>
		       </td>
		         <td class="col-md-5">
		          <div>	
		             <label>Point 2 (x,y)</label>
		          </div>
		       </td>
		      </tr>
		      <tr>
		       <td class="col-md-2 horizontal"><label>Wave 1</label></td>
		       <td class="col-md-5">
		          <div>	
		          <input type="number" min="0" max="10" step=".1" name="w1x1" required="required" />
		          <input type="number" min="0" max="10" step=".1" name="w1y1" required="required"/>
		          </div>
		       </td>
		         <td class="col-md-5">
		          <div>	
		          <input type="number" min="0" max="10" step=".1" name="w1x2" required="required" />
		          <input type="number" min="0" max="10" step=".1" name="w1y2" required="required"/>
		          </div>
		       </td>
		      </tr>
		      <tr>
		       <td class="col-md-2 horizontal"><label>Wave 3</label></td>
		       <td class="col-md-5">
		          <div>	
			          <input type="number" min="0" max="10" step=".1" name="w2x1" required="required" />
			          <input type="number" min="0" max="10" step=".1" name="w2y1" required="required" />
		          </div>
		       </td>
		       <td class="col-md-5">
		          <div>	
		          <input type="number" min="0" max="10" step=".1" name="w2x2" required="required" />
		          <input type="number" min="0" max="10" step=".1" name="w2y2" required="required"/>
		          </div>
		       </td>
		      </tr>
	      </table>
        </div>
        <div>
		       <span class="error_msg"></span>
	    </div>
        <div class = "pull-right">
		        <button type="submit" class="btn btn-primary" data-type="applyWaves">Apply Waves</button>
	    </div>
	
	</div>
</body>
</html>