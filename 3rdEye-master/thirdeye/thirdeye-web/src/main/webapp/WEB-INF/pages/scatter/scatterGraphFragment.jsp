<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="viewQuestionnaireList(nameOfAxis)">
		<div class="form-group">
			<label>Select the questionnaire</label>
			<select class="form-control ajaxSelector select2" th:attr="data-dependency=${nameOfAxis},data-url='graph/scatter/fetchRootParameters/'" name="questionnaireIds">
	          <option value="-1">--Select--</option>
	          <option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
	        </select>
        </div>
	</div>
	
	<div th:fragment="viewParamList(nameOfAxis)">
		<div class="form-group">
			<label>Select the parameter</label>
			<select class="form-control ajaxPopulated select2" th:attr="data-dependson=${nameOfAxis}" name="parameterIds">
	             <option value="-1">--Select--</option>
	             <option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	        </select>
        </div>
	</div>
	
		<select th:fragment="optionList(listOfParameters)" th:remove="tag">
	         <option value="-1">--Select--</option>
	         <option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	    </select>

	<div th:fragment="combineQuestionnaireAndParam(oneAxis)">
		<div class="col-md-4">
			<div th:replace="scatter/scatterGraphFragment :: viewQuestionnaireList(nameOfAxis=${oneAxis})"></div>
			<div th:replace="scatter/scatterGraphFragment :: viewParamList(nameOfAxis=${oneAxis})"></div>
		</div>
	</div>
	
	<div th:fragment="qualityGateSelectorFragment">
		<table class="table table-bordered table-condensed table-hover row">
			<tr>
				<td class="col-md-2 horizontal"><label>Select Color Scheme</label></td>
				<td colspan="3"><select class="form-control select2" name="qualityGateId">
						<option value=""> --Select--</option>
						<option th:each="oneQualityGate : ${listOfQualityGate}" th:value="${oneQualityGate.id}" th:text="${oneQualityGate.name}" />
				</select></td>
			</tr>
		</table>
	</div>
	
	<div th:fragment ="bcmlevelFragments">
	<div class="col-md-3 form-group">
			<label>Select BCMLevel 1</label>
			<select class="form-control ajaxSelector select2" th:attr="data-dependency=level1,data-url='graph/scatter/fetchbcmlevel/'" name ="idsOfLevel1" multiple="multiple" >
	          <option value="-1">--Select--</option>
	          <option th:each="oneLevel : ${bcmLevelList}" th:value = "${oneLevel.id}" th:text="${oneLevel.bcmLevelName}" />
	        </select>
	        
	       
        </div>
    
	   <div class="col-md-3 form-group">
         <label>Select BCMLevel 2</label>
			<select class="form-control ajaxPopulated ajaxSelector  select2" th:attr="data-dependson=level1,data-dependency=level2,data-url='graph/scatter/fetchbcmlevel/'"  name ="idsOfLevel2" multiple="multiple" >
	          <option value=""> </option>
	        </select>
	         
	       
        </div>
        <div class="col-md-3 form-group">
         <label>Select BCMLevel 3</label>
			<select class="form-control ajaxPopulated ajaxSelector  select2" th:attr="data-dependson=level2,data-dependency=level3,data-url='graph/scatter/fetchbcmlevel/'"  name ="idsOfLevel3" multiple="multiple" >
	          <option value=""></option>
	        </select>
	         
	       
        </div>
         <div class="col-md-3 form-group">
         <label>Select BCMLevel 4</label>
			<select class="form-control ajaxPopulated  select2" th:attr="data-dependson=level3"  name ="idsOfLevel4" multiple="multiple" >
	          <option value=""></option>
	        </select>
        </div>
       
        <select th:fragment="optionList(listOfLevels)" th:remove="tag">
	         <option th:each="oneLevel : ${listOfLevels}" th:value ="${oneLevel.id}" th:text="${oneLevel.bcmLevelName}" />
	    </select>
        </div>
        
        <div class="col-md-6" th:fragment = "showBcmErrorMessage">
        <h5 class = "bcmError" style="color:red;"></h5>
        </div> 
       
	<div th:fragment="createFullFragment">
		
			<form role="form" th:action="@{/graph/scatter/plot}" method="post" id="scatterGraphControl">
				<div class="row">
					<div th:replace="scatter/scatterGraphFragment :: combineQuestionnaireAndParam(oneAxis='x-axis')"></div>
					<div th:replace="scatter/scatterGraphFragment :: combineQuestionnaireAndParam(oneAxis='y-axis')"></div>
					<div th:replace="scatter/scatterGraphFragment :: combineQuestionnaireAndParam(oneAxis='z-axis')"></div>
					<div th:replace="scatter/scatterGraphFragment :: bcmlevelFragments"></div>
					    
					                                                
				
				</div>
				<div class="row">
				<div th:replace="scatter/scatterGraphFragment :: showBcmErrorMessage"></div>  
					</div>
				<div th:replace="scatter/scatterGraphFragment :: qualityGateSelectorFragment"></div>
			
				<div class="box-footer">
					<div class = "pull-right">
						<button type="submit" class="btn btn-primary">Plot Graph</button>
					</div>
					
	            </div>
			</form>
	</div>
	<div th:fragment="waveFragment">
		
			<form role="form" th:action="@{/graph/scatter/plot}" method="post" id="scatterGraphControl">
				<div class="row">
					<div th:replace="scatter/scatterGraphFragment :: combineQuestionnaireAndParam(oneAxis='x-axis')"></div>
					<div th:replace="scatter/scatterGraphFragment :: combineQuestionnaireAndParam(oneAxis='y-axis')"></div>
				</div>
				<div class="box-footer">
					<div class = "pull-right">
						<button type="submit" class="btn btn-primary">Plot Graph</button>
					</div>
	            </div>
			</form>
	</div>
</body>
</html>