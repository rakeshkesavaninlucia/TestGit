<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.functionalmaps.viewbcmtemplateslevel.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{bcm.level.pageTitle}"></span></div>
	<div class="container"  th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-6">
				<div class="levelTree box box-primary" >
				<form th:action="@{/bcm/{id}/update(id=${bcmTemplateForm.id})}" method="post" class="editableForm">
					<div class="form-group">
				      <input type="text" class="editableBcmName form-control" name="bcmName" th:value="${bcmTemplateForm.bcmName}"/>
				    </div>
			    </form>	
				   <div data-module="module-bcmLevelJSTree">
					<div class="containerJSTreeView " id="container" th:attr="data-bcmid=${bcmId}">
					<ul>
						<div th:replace="bcm/bcmLevelFragements :: oneLevelAdded(listOfLevels=${rootBcmLevelList})"></div>
						</ul>	                   
	                </div>
	                <div class="box-footer" >
	                    <a th:if="${bcmTemplateForm.workspace eq null}" th:href="@{/bcm/list}" class="btn btn-default" th:text="#{button.back}"></a>
	                   	<a th:if="${bcmTemplateForm.workspace ne null}" th:href="@{/wsbcm/list}" class="btn btn-default" th:text="#{button.back}"></a>
						<button id="AddL0" data-type="addValueChain" th:attr="data-bcmid=${bcmId}" class="btn btn-primary" th:text="#{bcm.level.addValueChain.btn}">Add Value Chain</button>
	                </div>
	               </div>
				</div>
			</div>
			
		</div>
		<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;">
	</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/ext/thirdEye/module-bcmLevelJSTree.js}"></script>
    </div>
</body>
</html>
