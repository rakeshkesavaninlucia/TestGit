<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.functionalredundancy.viewfr.nav })">
  <head>

	
  </head>
<body>
<div th:fragment="pageTitle">
	<span th:text="#{pages.reports.functionalredundancyview.title}"></span>
	<span data-toggle="control-sidebar" style="float:right;">
		<i class="glyphicon glyphicon-filter clickable btn btn-primary"></i>
	</span>
</div>
<div class="container" th:fragment="contentContainer">
	 <div class="row">
	 	<!-- Filter Implementation -->
			<div class="row" style="padding-bottom: 20px; padding-left: 20px;">
				<div style="padding-top: 15px;" data-module="module-filterTags">
					<div class="col-md-12">
						<div id="tags"></div>
					</div>
				</div>
			</div>

			<aside class="control-sidebar control-sidebar-dark"
				style="margin-top: 101px; padding-top: 0px;">
				<div class="tab-content" style="height: 39em; overflow-y: auto;">
					<div id="control-sidebar-theme-demo-options-tab"
						class="tab-pane active">
						<div style="text-align: center; text-decoration: underline;">
							<h4 th:text="#{filter.panel}"></h4>
						</div>
						<div data-module="module-assetTypeFacet"></div>
						<div data-module="module-facetUpdate"></div>
					</div>
					<!-- /.tab-pane -->
				</div>
			</aside>
			<!-- Filter Implementation End -->
        <div class="col-sm-12">
        <div class="nav-tabs-custom" data-module="module-viewFunctionalRedundancy" >
			<ul class="nav nav-tabs pull-right">
				<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                      View <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a role="menuitem" tabindex="-1" data-type="single">Selected</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" data-type="full">Complete</a></li>
                    </ul>
                  </li>
                  </ul>
	      <div class="box box-primary">
		     <div class="box-body assetMappingMainDiv" th:attr="data-fcid=${fcid},data-levelid=${levelid},data-qeid=${qeid},data-mode=${mode},data-filtermap=${filterMap}" style="padding-top:15px;">
	     			<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				   <div id='chart1' style="font-size:0.8em;margin-right:0px !important; padding-right:0px;" class="levelTitle  col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<table id="bcmLevelDetails" class="table table-bordered" style="width:100%;border-collapse:collapse;table-layout: fixed;"></table>
				  </div>
				  <div class="graphViewPort col-lg-8 col-md-8 col-sm-8 col-xs-8">
						<div class="graphsContainer assetMapping">
							<div id='newID' style="width: 84px;font-size:0.8em; float:left;" >
								<table id="assetMappingTable" class="table table-bordered" style="width:100%;border-collapse:collapse;">
								</table>
							</div>
						</div>
				  </div>
				  
				<script id="bcmLevelTemplate" type="text/x-handlebars-template">
				 {{#each bcmDetails}}
						{{#each children}}
					<tr>
							{{#if @first}}
    							<td class="parentLevelTitle" rowspan="{{../children.length}}" data-toggle="tooltip"  title="{{../parentLevel}}" data-container="body" data-placement="top"><div style="display:block;"><span class="rotate90" style="font-size:0.8em;">{{link ../parentLevel ../parentLevelId}}</span></div></td>
							{{/if}}
								{{#if dataExist}}
									<td  align="center" class="levelTitle" data-toggle="tooltip" title="{{name}}" data-container="body" data-placement="top">{{link name id}}</td>
								{{else}}
									<td  align="center" class="levelTitle" data-toggle="tooltip" title="{{name}}" data-container="body" data-placement="top">{{name}}</td>
								{{/if}}
					</tr>
						{{/each}}
  				 {{/each}}
		       </script>
				
				<script id="assetMappingTemplate" type="text/x-handlebars-template">
				     {{#each bcmDetails}}
						{{#each children}}
						 <tr>
				           {{#each assetValues}}
				 		      <td class="assetMapping">
				 			    <div class="progress progress-xs assetMapping" data-toggle="popover" data-trigger="hover" title="{{../this.name}}" data-content="{{this}}%"  data-container="body" data-placement="top">
	                            <div class="progress-bar progress-bar-primary" style="width: {{this}}%;">{{this}}%</div>
	                            </div>
				 		      </td>
							{{/each}}
			            </tr>						
						{{/each}}
  				    {{/each}}
		       </script>
				
				<script id="assetTitlesMappingTemplate" type="text/x-handlebars-template">
			<tr>
			{{#each assetDetails}}
				 	<td class="assetNameTitle clickable" data-type="assetTitle" id="{{id}}">
				 		{{name}}
				 	</td>
			{{/each}}
			</tr>
		   </script>
			 
			</div>

		</div>
		</div>
		</div>
	</div>
</div><!-- /.container -->
     	<div th:fragment="scriptsContainer"  th:remove="tag">
			<script	th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
			<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
			<script	th:src="@{/static/js/3rdEye/modules/module-viewFunctionalRedundancy.js}"></script>
			<script th:src="@{/static/js/ext/handlebars/4.0.5/handlebars.js}"></script>
		</div>
   </body>
</html>
