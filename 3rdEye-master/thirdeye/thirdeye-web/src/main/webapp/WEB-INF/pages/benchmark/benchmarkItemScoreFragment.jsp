<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<!-- Sunil code start benchmark item score creation -->
	
	<div th:fragment="createBenchmarkItemScore"  class="table-responsive" th:object="${benchmarkItemBean}">
	      <div class="form-group" >
				<table class="table table-bordered table-condensed table-hover">
						    <tr>
								<td class="col-md-4 horizontal"><label class="control-label" th:text="#{benchmark.item.name}"></label></td>
								<td class="col-md-8">
									<div class="form-group">
									    <input type="hidden" name="benchmarkItemId" th:field="*{id}" />	 
										<span th:text="${benchmarkItemBean.displayName}">Test</span>
									</div>
								</td>
							</tr>
				</table>
		 </div>
		 <table class="table table-bordered table-condensed table-hover" id="benchmarkItemScoreColumns">
			<thead>
		    	<tr>
					<th>Start Year</th>
					<th>End Year</th>
					<th>Score</th>					
					<th sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})">Actions</th>
				</tr>
			</thead>
			<tbody >
		 	  <tr th:each="oneCol : ${benchmarkItemBean.benchmarkItemScores}" th:include="benchmark/benchmarkItemScoreFragment :: viewBenchmarkItemScore"
		  	  	  th:with="colid=${oneCol.id},startYear=${oneCol.startYear},endYear=${oneCol.endYear},score=${oneCol.score},benchmarkItemId=${oneCol.benchmarkItem.id}"/>
			</tbody>
		</table>	
		<span sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"><button id="addRowButton"  th:attr="data-benchmarkItemId=${benchmarkItemBean.id}" class="btn btn-primary" >Add Benchmark Item Score</button></span>
	</div>
	
	<div th:fragment="viewBenchmarkItemScore" th:remove="tag" >
		<tr th:id="${colid}" class="nodrag nodrop">
			<td><span th:text="${#dates.format(startYear, 'MM/dd/yyyy')}">Test</span>	</td>		
			<td><span th:text="${#dates.format(endYear, 'MM/dd/yyyy')}">Test</span></td>
			<td><span th:text="${#numbers.formatDecimal(score, 0, 1)}">Test</span></td>
			<td sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"><a class="editRow fa fa-pencil-square-o" title="Edit" style="cursor: pointer;"></a>
			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a></td>
		</tr>
	</div>
	
	<tr th:fragment="editBenchmarkItemScore(oneRow)" class="dirtyrow" th:id="${oneRow.id}" th:object="${benchmarkItemScoreBean}">
	        <input type="hidden"  name="benchmarkItem.id" th:value="${oneRow.benchmarkItem.id}" />		  
			<td><input type="date" name="startYear" th:value="${oneRow.startYear} ? ${#dates.format(oneRow.startYear, 'yyyy-MM-dd')}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('startYear')}" th:errors="*{startYear}" class="error_msg">Test</span>
			</td>
			
			<td><input type="date" name="endYear" th:value="${oneRow.endYear} ? ${#dates.format(oneRow.endYear, 'yyyy-MM-dd')}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('endYear')}" th:errors="*{endYear}" class="error_msg">Test</span>
			</td>
			<td>
			  <input type="number" th:min="1" th:max="10" step=".1"  name="score" th:value="${#numbers.formatDecimal(oneRow.score, 0, 1)}"></input>
			   <span class="asterisk">*</span>
			   <span th:if="${#fields.hasErrors('score')}" th:errors="*{score}" class="error_msg">Test</span>
			</td>
			<td><a class="saveRow fa fa-check-square-o" title="Save"></a>
			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}"></a></td>
	</tr>
	
	<tr th:fragment="createRow" class="dirtyrow" th:object="${benchmarkItemScoreBean}">
		    <input type="hidden" th:field="*{benchmarkItem.id}" />		
		    <td>		         
		      <input type="date" th:field="*{startYear}" placeholder="Date"></input>
			  <span class="asterisk">*</span>
			  <span th:if="${#fields.hasErrors('startYear')}" th:errors="*{startYear}" class="error_msg">Test</span>
			</td>			
			<td>
			  <input type="date" th:field="*{endYear}" placeholder="Date" ></input>
			  <span class="asterisk">*</span>
			  <span th:if="${#fields.hasErrors('endYear')}" th:errors="*{endYear}" class="error_msg">Test</span>
			</td>
			<td>
			   <input  type="number" th:min="1" th:max="10" step=".1" th:field="*{score}"></input>
			   <span class="asterisk">*</span>
			   <span th:if="${#fields.hasErrors('score')}" th:errors="*{score}" class="error_msg">Test</span>
			</td>
			<td><a class="saveRow fa fa-check-square-o" title="Save"></a></td>
	</tr>
	
	<!-- Sunil code end -->
</body>
</html>