<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.dashboard.nav}+ '-'+${dashboard.dashboardName})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
</head>
<body>
		<div th:fragment="pageTitle">
			<span th:text="${dashboard.dashboardName}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
			<div class="dropdown pull-right" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})"  data-module="module-defaultDashboardTemplate" style = "right:3px;">
			  <button class="btn btn-default dropdown-toggle" style = "float:left;" type="button" id="dashboardActionDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <i class="fa fa-cog"></i>
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dashboardActionDropDown">
			    <li><a class="editDashboard clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="editDashboard">Edit Dashboard</a></li>
			    <li><a class="addWidget clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="addWidget">Add Widget to Dashboard</a></li>
			    <li><a class="deleteDashboard clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="deleteDashboard">Delete Dashboard</a></li>
			  </ul>
			</div>
		
		</div>
		
		<div th:fragment="contentContainer">
		<!-- Filter Impelmentation -->
				<div class="row" style="padding-bottom: 20px;padding-left: 20px;">
				<div style="padding-top:15px;" data-module="module-filterTags">
					<div class="col-md-12">					
						<div id="tags"></div>
					</div>
				</div>
				</div>
				
			      <aside class="control-sidebar control-sidebar-dark" style="margin-top: 101px;padding-top: 0px;">                
			        <div class="tab-content" style="height: 39em;overflow-y: auto;">            
			          <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active"> 
			          <div style="text-align: center;text-decoration: underline;"><h4 th:text="#{filter.panel}"></h4></div>      
						<div data-module="module-assetTypeFacet"></div>
						<div data-module="module-facetUpdate"></div>         
			          </div><!-- /.tab-pane -->          
			        </div>
			      </aside>     
	<!-- Filter Impelmentation End -->
		
			<div class="dashboard" th:attr="data-dashboardid=${dashboard.id}">
				<div class="row">
					<div class="col-md-6 widgetcontainer" th:fragment="widgetcontainer(widgets)" th:each="oneWidget : ${widgets}">
						<div class="widgetplacement" th:attr="data-widgetid=${oneWidget.id}" data-module="module-widget">
							
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<!-- <button class="btn btn-block btn-primary addWidget">Add a widget</button> -->
					</div>
				</div>
			</div>
		</div>	
		
	
	 <div th:fragment="scriptsContainer" th:remove="tag">
	  	<script	th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
	  	<script	th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
	  	<script	th:src="@{/static/js/3rdEye/modules/module-defaultDashboardTemplate.js}"></script>
	  	<script	th:src="@{/static/js/3rdEye/modules/module-widget.js}"></script>
	</div>
</body>
</html>